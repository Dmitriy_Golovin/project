function ajaxAddPersonalData(callback) {
	tab.xjscsrf(function() {
		var data  = new FormData(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			avatarInputElem = document.querySelector('input[name=avatar]'),
			noticeElem = document.querySelector('.notice_elem_ava'),
			firstNameValue = document.querySelector('p.show_hide_firstname').innerHTML,
			lastNameValue = document.querySelector('p.show_hide_lastname').innerHTML,
			countryId = document.querySelector('p.show_hide_country').getAttribute('data-countryId'),
			cityId = document.querySelector('p.show_hide_city').getAttribute('data-cityId'),
			listP = document.querySelectorAll('.qqq'),
			buttonSendData = document.querySelector('.button_edit_profile_submit'),
			waitingBoxSendData = document.querySelector('.waiting_icon');

		data.append('first_name', firstNameValue);
		data.append('last_name', lastNameValue);

		if (countryId) {
			data.append('country_id', countryId);
		}

		if (cityId) {
			data.append('city_id', cityId);
		}

		data.append('csrf_test_name', csrfVal.getAttribute('content'));

		var xhr = new XMLHttpRequest();
		xhr.onload = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();
				
				if (result['res'] == 1) {
					var dataForTabSync = {
						'ws_tab_sync_to_do': 'change_personal_data',
						'firstName': result.user.first_name,
						'lastName': result.user.last_name,
						'country': result.user.countryTitle,
						'city': result.user.cityTitle,
					}
					tab.send(tab.tabSync.collectData(dataForTabSync));
					tab.tabSync.run(dataForTabSync)
					callback();
				}
			}
		};

		xhr.open('POST', '/user/editPersonalData');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}