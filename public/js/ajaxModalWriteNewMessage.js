function ajaxModalWriteNewMessage(from, to) {

	let xhr = new XMLHttpRequest(),
		modaLBlock = document.querySelector('.modal_content_box'),
		data = 'writeNewMessage=' + true + '&' + 'from=' + from + '&' + 'to=' + to + '&' + 'ajax=' + true;

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			let result = JSON.parse(xhr.responseText);
			if (result['res'] == 1) {
				modaLBlock.innerHTML = result['content'];
				//animationPushIcon({icon: '.edit_icon'});
				
				let modalWin = document.querySelector('.modal_profile'),
					profileBox = document.querySelector('.profile_box'),
					messageToFullnameEl = document.querySelector('.message_to_user_fullname'),
					messageToUserLink = document.querySelector('.message_to_user_link'),
					writeMessageField = document.querySelector('.write_message_block>textarea'),
					sendMessageButton = document.querySelector('.send_message_to_user_button'),
					waitingBox = document.querySelector('.waiting_icon');
					
				noScrollOnModalWin(modalWin, profileBox);
				
				messageToUserLink.setAttribute('href', '/profile/id' + to);
				messageToFullnameEl.innerHTML = result['messageToFullName'];
				writeMessageField.focus();
				
				writeMessageField.addEventListener('keyup', function() {
					if (this.value) {
						sendMessageButton.classList.remove('not_active_element');
					} else if (!this.value && !sendMessageButton.classList.contains('not_active_element')) {
						sendMessageButton.classList.add('not_active_element');
					}
				});
				
				sendMessageButton.addEventListener('click', function() {
					let data = {
						'ws_tab_sync_to_do': 'send_message_to_user',
						'actionDB': 'addNewMessage',
						'fromUserID': from,
						//'user_from_fullname': document.querySelector('.user_full_name').innerHTML,
						//'ava_from': document.querySelector('.avatar_block > img').getAttribute('src'),
						'toUserID': to,
						'text': writeMessageField.value,
						'server_response': 'message_succesfully_sent',
						'timeOffsetSeconds': new Date().getTimezoneOffset() * (-60),
					}
					tab.waitForServerResponse = 'message_succesfully_sent';
					tab.send(tab.tabSync.collectData(data));
					activateIconWaiting(sendMessageButton, waitingBox);
				});
				
				setTimeout(function() {
					profileBox.classList.add('modal_active');
					modalWin.classList.add('modal_box_active');
				}, 10);
					
			} else if (result['res'] == 0){
				modaLBlock.innerHTML = 'error';
			}
		}
	};

	xhr.open('POST', '/user_panel/getBlockWriteNewMessage');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(data);

}

/* function breakTextIntoParagraphs(text) {
	let textEdit = text.split('\n'),
		result = '';

	for (let i = 0; i < textEdit.length; i++) {
		if (textEdit[i] !== '') {
			result += '<p class="message_paragraph">' + textEdit[i] + '</p>';
		}
	}
	return result;
} */