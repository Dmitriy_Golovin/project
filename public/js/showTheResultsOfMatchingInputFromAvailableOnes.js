function showMatchingResult(inputField, elToSearch, elClass) {
	inputField.addEventListener('keyup', function(e) {
		let res = getItemList(elToSearch),
			inputValue = inputField.value.toLowerCase();

		for (let i = 0; i < res.length; i++) {
			let elValue = '',
				elValueArr = [];
			if (!res[i].querySelector('.' + elClass).children[0]) {
				elValue = res[i].querySelector('.' + elClass).innerHTML.toLowerCase();
				elValueArr = elValue.split(' ');
			} else {
				elValue = res[i].querySelector('.' + elClass).children[0].innerHTML.toLowerCase().trim() + ' ' + res[i].querySelector('.' + elClass).children[1].innerHTML.toLowerCase().trim();
				elValueArr.push(res[i].querySelector('.' + elClass).children[0].innerHTML.toLowerCase().trim())
				elValueArr.push(res[i].querySelector('.' + elClass).children[1].innerHTML.toLowerCase().trim())
			}
			
			if (inputValue == elValue.slice(0, inputValue.length) || inputValue == elValueArr[0].slice(0, inputValue.length) || inputValue == elValueArr[1].slice(0, inputValue.length)) {
				res[i].classList.remove('item_box_user_not_active');
			} else {
				res[i].classList.add('item_box_user_not_active');
			}
		}
	});
}

function getItemList(el) {
	if (el.children.length < 1) {
		return [];
	}

	let list = [];
	
	for (let elem of el.children) {
		if (elem.classList.contains('input_search_item')) {
			list.push(elem);
		} else {
			for (let elemChild of elem.children) {
				list.push(elemChild)
			}
		}
	}

	return list;
}