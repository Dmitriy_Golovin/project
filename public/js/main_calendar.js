/*jshint esversion: 6 */

const FILE_TYPE_IMAGE = 1;
const FILE_TYPE_VIDEO = 3;
const FILE_TYPE_DOCUMENT = 4;

var closeNewWinElement = document.querySelector('.close_win_add_new_event'),
	targetPic = document.querySelector('.close_pic'),
	containerForElement = document.querySelector('.add_new_win_container'),
	elemInsideWin = document.querySelectorAll('.inside_win'),
	winForNewEvent = document.querySelector('.add_new_win'),
	formFieldsetBox = document.querySelector('.fieldset_box'),
	formLogin = document.querySelector('form[name=login_form]'),
	userLoginField = document.querySelector('input[name=user_login]'),
	userPasswordField = document.querySelector('input[name=user_password]'),
	buttonLogin = document.querySelector('.button_login_start_page'),
	calendarMenuIcon = document.querySelector('.calendar_menu_icon_block'),
	calendarMenuItemBlock = document.querySelector('.calendar_menu_item_block'),
	chekBoxCalendSettingList = document.querySelectorAll('.calendar_menu_list>li>input'),
	addNewEventItemList = document.querySelectorAll('.add_new_event_item>div'),
	eventContainer = document.querySelector('#event'),
	meetingContainer = document.querySelector('#meeting'),
	taskContainer = document.querySelector('#task'),
	invitedAssignSelf = document.querySelector('.invited_assign_self'),
	videoFileList = new DataTransfer(),
	imageFileList = new DataTransfer(),
	documentFileList = new DataTransfer(),
	taskFileVideoField = document.querySelector('#task_load_file_video'),
	taskFileImageField = document.querySelector('#task_load_file_image'),
	taskFileDocumentField = document.querySelector('#task_load_file_document'),
	calendar = new Calendar(),
	userListObjData,
	scrollUserList = true,
	timeOutVideoMediaControl,
   	fullScreen = false;

window.addEventListener('load', function() {
	setTableDimension();
	setNavHeaderContainerDimensions();
	setGreetingBoxDimention();
	calendar.show(calendar.getDifferenceMonth());
    createWindowForNewEvent({parent: 'table', header: 'header', child: '.inside-cell'});

	showInviteContainerEventMeeting(eventContainer);
	showInviteContainerEventMeeting(meetingContainer);
	showInviteContainerTask(taskContainer);

	showAddFileContainer(taskContainer)
});

window.addEventListener('popstate', function() {
	calendar.show(calendar.getDifferenceMonth());
});

window.addEventListener('resize', function() {
	setTableDimension();
	setNavHeaderContainerDimensions();
	setGreetingBoxDimention();
	if (userLoginField) {
		removeNotice(formFieldsetBox, userLoginField, userPasswordField);
	}
});

if (closeNewWinElement) {
	closeNewWinElement.addEventListener('click', function(e) {
			
		if (e.target === closeNewWinElement || e.target === targetPic) {
			closeWinForNewEvent(containerForElement);
			if (userLoginField) {
				removeNotice(formFieldsetBox, userLoginField, userPasswordField);
			}
		}
	});
}

document.body.addEventListener('click', function(e) {

	for (var i = 0; i < elemInsideWin.length; i++) {
		if (e.target === elemInsideWin[i]) {
			return;
		}
	}
	
	if (winForNewEvent.classList.contains('active')) {
		closeWinForNewEvent(containerForElement);
		if (userLoginField) {
			removeNotice(formFieldsetBox, userLoginField, userPasswordField);
		}
	}

	if (e.target.classList.contains('task_assign_user_button')) {
		let assignedUserContainer = e.target.closest('.container_create_new_event').querySelector('.assigned_user_container'),
			assignedUserBlock = assignedUserContainer.querySelector('.assigned_user_block'),
			invitedContainer = e.target.closest('.invited_container'),
			target = e.target.parentElement.parentElement,
			cancelButton = target.querySelector('.task_cancel_user_button'),
			inviteButton = e.target.closest('.container_create_new_event').querySelector('.invite_button');

		inviteButton.classList.add('not_active');
		e.target.classList.add('not_active');
		cancelButton.classList.remove('not_active');
		target.classList.remove('box_bottom_border');
		target.classList.add('task_assigned_user');
		assignedUserContainer.classList.remove('not_active');
		assignedUserBlock.append(target);
		invitedContainer.classList.add('not_active_opacity');
	}
	
	if (e.target.classList.contains('task_cancel_user_button')) {
		let inviteContainer = document.querySelector('#task').querySelector('.invited_container'),
			invitedContentBlock = inviteContainer.querySelector('.invited_content_block'),
			inviteButton = document.querySelector('#task').querySelector('.invite_button');

		activeInviteContainer(inviteContainer, invitedContentBlock, 'assign');
	}

	if (e.target.classList.contains('invited_added_users_button')) {
		let invitedContainer = e.target.closest('.invited_container'),
			addedUserContainer = invitedContainer.querySelector('.invited_added_users_container'),
			invitedContentContainer = invitedContainer.querySelector('.invited_content_block'),
			showUsersButton = invitedContainer.querySelector('.invited_show_users_button'),
			backButton = invitedContainer.querySelector('.back_button'),
			invitedFilterBlock = invitedContainer.querySelector('.invited_filter_block'),
			invitedSearchBlock = invitedContainer.querySelector('.invited_search_block'),
			titleInvitedContainer = invitedContainer.querySelector('.title_invited_container'),
			titleInvitedUsersContainer = invitedContainer.querySelector('.title_invited_users_container');

		invitedContentContainer.classList.add('not_active');
		invitedFilterBlock.classList.add('not_active');
		invitedSearchBlock.classList.add('not_active');
		backButton.classList.add('not_active');
		titleInvitedContainer.classList.add('not_active');
		showUsersButton.classList.remove('not_active');
		addedUserContainer.classList.remove('not_active');
		titleInvitedUsersContainer.classList.remove('not_active');
	}

	if (e.target.classList.contains('invited_show_users_button')) {
		let invitedContainer = e.target.closest('.invited_container'),
			addedUserContainer = invitedContainer.querySelector('.invited_added_users_container'),
			invitedContentContainer = invitedContainer.querySelector('.invited_content_block'),
			showUsersButton = invitedContainer.querySelector('.invited_show_users_button'),
			backButton = invitedContainer.querySelector('.back_button'),
			invitedFilterBlock = invitedContainer.querySelector('.invited_filter_block'),
			invitedSearchBlock = invitedContainer.querySelector('.invited_search_block'),
			titleInvitedContainer = invitedContainer.querySelector('.title_invited_container'),
			titleInvitedUsersContainer = invitedContainer.querySelector('.title_invited_users_container'),
			userQuerySearch = invitedContainer.querySelector('.user_query_search'),
			userContainerChildren = Array.from(userQuerySearch.children);

		userContainerChildren.sort(function(a, b) {
			return a.getAttribute("data-rownum") - b.getAttribute("data-rownum");
		});

		userQuerySearch.innerHTML = '';

		for (let i = 0; i < userContainerChildren.length; i++) {
			userQuerySearch.append(userContainerChildren[i]);
		}

		invitedContentContainer.classList.remove('not_active');
		invitedFilterBlock.classList.remove('not_active');
		invitedSearchBlock.classList.remove('not_active');
		backButton.classList.remove('not_active');
		titleInvitedContainer.classList.remove('not_active');
		showUsersButton.classList.add('not_active');
		addedUserContainer.classList.add('not_active');
		titleInvitedUsersContainer.classList.add('not_active');
	}

	if (e.target.classList.contains('event_meeting_invite_user_button')) {
		let container = e.target.closest('.container_create_new_event'),
			userContainer = e.target.closest('.user_result_item'),
			invitedContainer = e.target.closest('.invited_container'),
			invitedContentBlock = invitedContainer.querySelector('.invited_content_block'),
			addedUserContainer = invitedContainer.querySelector('.invited_added_users_container'),
			invitedAddedUsersButton = invitedContainer.querySelector('.invited_added_users_button'),
			inviteUserButon = container.querySelector('.invite_user_buton'),
			cancelInviteButton = userContainer.querySelector('.event_meeting_cancel_user_button'),
			userQuerySearch = e.target.closest('.user_query_search'),
			userCount = userQuerySearch.getAttribute('data-usercount'),
			timestamp = userQuerySearch.getAttribute('data-timestamp'),
			existUserCount = userQuerySearch.children.length/*(addedUserContainer.children.length > 0) ? userQuerySearch.children.length + addedUserContainer.children.length : userQuerySearch.children.length*/,
			rowNum = userQuerySearch.children[userQuerySearch.children.length - 1].getAttribute('data-rowNum');

		for (let el of addedUserContainer.children) {
			if (el.hasAttribute('data-query_timestamp') && el.getAttribute('data-query_timestamp') == userQuerySearch.getAttribute('data-timestamp')) {
				existUserCount += 1;
			}
		}

		if ((userQuerySearch.parentElement.scrollHeight - userContainer.offsetHeight) <= userQuerySearch.parentElement.clientHeight && existUserCount < userCount) {
			fetchGetUserList(userListObjData, invitedContentBlock, timestamp, rowNum);
		}

		userContainer.setAttribute('data-query_timestamp', userQuerySearch.getAttribute('data-timestamp'));
		addedUserContainer.append(userContainer);
		e.target.classList.add('not_active');
		cancelInviteButton.classList.remove('not_active');
		invitedAddedUsersButton.innerHTML = invitedAddedUsersButton.innerHTML.replace(/[0-9]/g, '') + ' ' + addedUserContainer.children.length;
		inviteUserButon.innerHTML = inviteUserButon.innerHTML.replace(/[0-9]/g, '') + ' ' + addedUserContainer.children.length;
	}

	if (e.target.classList.contains('event_meeting_cancel_user_button')) {
		let container = e.target.closest('.container_create_new_event'),
			userContainer = e.target.closest('.user_result_item'),
			invitedContainer = e.target.closest('.invited_container'),
			addedUserContainer = invitedContainer.querySelector('.invited_added_users_container'),
			userQuerySearchContainer = invitedContainer.querySelector('.user_query_search'),
			invitedAddedUsersButton = invitedContainer.querySelector('.invited_added_users_button'),
			inviteUserButon = container.querySelector('.invite_user_buton'),
			inviteUserItemButton = userContainer.querySelector('.event_meeting_invite_user_button'),
			nothingFound = userQuerySearchContainer.querySelector('.nothing_found');

		if (nothingFound) {
			nothingFound.remove();
		}

		userContainer.removeAttribute('data-query_timestamp');
		userQuerySearchContainer.append(userContainer);
		e.target.classList.add('not_active');
		inviteUserItemButton.classList.remove('not_active');
		invitedAddedUsersButton.innerHTML = (addedUserContainer.children.length > 0) ? invitedAddedUsersButton.innerHTML.replace(/[0-9]/g, '') + ' ' + addedUserContainer.children.length : invitedAddedUsersButton.innerHTML.replace(/[0-9]/g, '');
		inviteUserButon.innerHTML = (addedUserContainer.children.length > 0) ? inviteUserButon.innerHTML.replace(/[0-9]/g, '') + ' ' + addedUserContainer.children.length : inviteUserButon.innerHTML.replace(/[0-9]/g, '');
	}

	if (e.target.classList.contains('invited_search_button')) {
		let data = getDataForInviteUsers(e.target.closest('.invited_container')),
			invitedContentBlock = e.target.closest('.invited_container').querySelector('.invited_content_block');

		fetchGetUserList(data, invitedContentBlock);
	}

	if (e.target.classList.contains('add_button_event')) {
		let container = e.target.closest('.container_create_new_event');

		if (checkBaseEventdata(container)) {
			let data = getDataForEvent(container);
			fetchSaveBaseEvent(data)
				.then(function(result) {
					if (result.res) {
						let title = container.querySelector('.title_create_new_event'),
							contentWrapper = container.querySelector('.wrapper_create_new_event'),
							addButtonBlock = container.querySelector('.add_button_block');
							calendarDateList = calendar.getDataForCalendarEvent();

						tab.checkToken(function() {
							ajaxGetContentInCell(calendarDateList.dateList, calendarDateList.dateOfFirstEl, calendarDateList.dateOfLastEl, calendarDateList.cellList, calendarDateList.yearList, calendarDateList.monthList);
						});

						closeWinForAnyNewEvent(container, contentWrapper, addButtonBlock, title);
					}

					if (!result.res) {
						let errorEl = container.querySelector('.event_form_error'),
							saveButton = container.querySelector('.add_button_event');

						errorEl.innerHTML = result.errors;
						errorEl.classList.add('event_form_error_active');
						saveButton.classList.add('not_active_opacity');

						container.onclick = function(e) {
							if (!e.target.classList.contains('add_button_event')) {
								removeErrors(container);
							}
						};
					}
				});
		}
	}

	if (e.target.classList.contains('video_template_delete_file_container') ||
		e.target.closest('.video_template_delete_file_container')) {
		let fileContainerPreload = e.target.closest('.file_container_preload'),
			labelContainer = e.target.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value'),
			parent = fileContainerPreload.parentElement,
			fieldPayloadState = fileContainerPreload.getAttribute('data-field_payload_state'),
			field = e.target.closest('.create_event_file_field_row').querySelector('input[type=file]');

		for (let i = 0; i < videoFileList.files.length; i++) {
			if (videoFileList.files[i].index === Number(fieldPayloadState)) {
				videoFileList.items.remove(i);
			}
		}

		field.files = videoFileList.files;
		fileContainerPreload.remove();

		if (parent.children.length < 1) {
			labelContainer.classList.remove('not_active');
		}
	}

	if (e.target.classList.contains('image_template_delete_file_container') ||
		e.target.closest('.image_template_delete_file_container')) {
		let fileContainerTemplate = e.target.closest('.image_file_container_template'),
			labelContainer = e.target.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value'),
			parent = fileContainerTemplate.parentElement,
			fieldPayloadState = fileContainerTemplate.getAttribute('data-field_payload_state'),
			field = e.target.closest('.create_event_file_field_row').querySelector('input[type=file]');

		for (let i = 0; i < imageFileList.files.length; i++) {
			if (imageFileList.files[i].index === Number(fieldPayloadState)) {
				imageFileList.items.remove(i);
			}
		}

		field.files = imageFileList.files;
		fileContainerTemplate.remove();

		if (parent.children.length < Number(field.dataset.max_value)) {
			labelContainer.classList.remove('not_active');
		}

		if (parent.children.length <= 0) {
			parent.classList.remove('mb_10');
		} else {
			parent.classList.add('mb_10');
		}
	}

	if (e.target.classList.contains('document_template_delete_file_container') ||
		e.target.closest('.document_template_delete_file_container')) {
		let fileContainerTemplate = e.target.closest('.document_file_container_template'),
			labelContainer = e.target.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value'),
			parent = fileContainerTemplate.parentEl,
			fieldPayloadState = fileContainerTemplate.getAttribute('data-field_payload_state'),
			field = e.target.closest('.create_event_file_field_row').querySelector('input[type=file]');

		parent = fileContainerTemplate.parentElement;

		for (let i = 0; i < documentFileList.files.length; i++) {
			if (documentFileList.files[i].index === Number(fieldPayloadState)) {
				documentFileList.items.remove(i);
			}
		}

		field.files = documentFileList.files;
		fileContainerTemplate.remove();

		if (parent.children.length < Number(field.dataset.max_value)) {
			labelContainer.classList.remove('not_active');
		}

		if (parent.children.length <= 0) {
			parent.classList.remove('mb_10');
		} else {
			parent.classList.add('mb_10');
		}
	}
});

document.body.addEventListener('mouseover', function(e) {
	if (e.target.classList.contains('notice_icon')) {
		let noticeLabelMessage = e.target.parentElement.querySelector('.notice_label_message');

		noticeLabelMessage.classList.remove('not_active');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active_opacity');
			noticeLabelMessage.setAttribute('data-notice_label_state', 'active');
		}, 5);
	}
});

document.body.addEventListener('mouseout', function(e) {
	if (e.target.classList.contains('notice_icon')) {
		let noticeLabelMessage = e.target.parentElement.querySelector('.notice_label_message');

		noticeLabelMessage.classList.add('not_active_opacity');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active');
		}, 5);
	}
});

calendarMenuIcon.addEventListener('mouseover', function() {
	calendarMenuItemBlock.classList.add('menu_box_active');
	calendarMenuItemBlock.addEventListener('transitionend', function() {
		calendarMenuItemBlock.children[0].classList.add('calendar_menu_list_active');
	});
});

calendarMenuIcon.addEventListener('mouseout', function(e) {
	calendarMenuItemBlock.classList.remove('menu_box_active');
	calendarMenuItemBlock.addEventListener('transitionend', function() {
		calendarMenuItemBlock.children[0].classList.remove('calendar_menu_list_active');
	});
});

for (let el of chekBoxCalendSettingList) {
	el.addEventListener('change', function() {
		let partOfIdChangeEl = this.id.slice(0, this.id.lastIndexOf('_'));
		tab.checkToken(function() {
			ajaxSendDataSettings(chekBoxCalendSettingList, partOfIdChangeEl);
		});
	});
}

for (let el of addNewEventItemList) {
	el.addEventListener('click', function() {
		let dateOfCell = winForNewEvent.querySelector('.date_title_new_event').innerHTML;
		showBlockForCreateNewEventByElAttr(winForNewEvent, this, dateOfCell);
	});
}

invitedAssignSelf.addEventListener('click', function() {
	let container = this.closest('.container_create_new_event'),
		assignedUserContainer = container.querySelector('.assigned_user_container'),
		assignedUserBlock = container.querySelector('.assigned_user_block'),
		invitedContainer = this.closest('.invited_container'),
		inviteButton = container.querySelector('.invite_button'),
		userAssignedContainer = document.createElement('div'),
		avaBox = document.createElement('div'),
		avaLink = document.createElement('a'),
		avaImg = document.createElement('img'),
		fullNameBox = document.createElement('div'),
		buttonBox = document.createElement('div'),
		cancelButton = document.createElement('span');

	userAssignedContainer.classList.add('user_result_item');
	userAssignedContainer.setAttribute('data-profile_id', tab.tabSync.selfID);
	userAssignedContainer.classList.add('task_assigned_user');
	avaBox.classList.add('ava_box_result_item');
	avaLink.href = '/profile/' +  tab.tabSync.selfID;
	avaImg.src = tab.tabSync.selfPathToAva;
	avaImg.classList.add('ava_result_item');
	fullNameBox.classList.add('fullname_box_result_item');
	fullNameBox.innerHTML = tab.tabSync.selfFullName;
	buttonBox.classList.add('button_box_result_item');
	cancelButton.classList.add('task_cancel_user_button');
	cancelButton.innerHTML = 'отменить';

	avaLink.append(avaImg);
	avaBox.append(avaLink);
	buttonBox.append(cancelButton);
	userAssignedContainer.append(avaBox);
	userAssignedContainer.append(fullNameBox);
	userAssignedContainer.append(buttonBox);
	assignedUserBlock.append(userAssignedContainer);
	assignedUserContainer.classList.remove('not_active');
	inviteButton.classList.add('not_active');
	invitedContainer.classList.add('not_active_opacity');
});

taskFileVideoField.addEventListener('change', function() {
	checkAndUploadFile(this, FILE_TYPE_VIDEO, videoFileList);
});

taskFileImageField.addEventListener('change', function() {
	checkAndUploadFile(this, FILE_TYPE_IMAGE, imageFileList);
});

taskFileDocumentField.addEventListener('change', function() {
	checkAndUploadFile(this, FILE_TYPE_DOCUMENT, documentFileList);
});

taskFileVideoField.addEventListener('click', function() {
	clearErrorFileInput(this);
});

taskFileImageField.addEventListener('click', function() {
	clearErrorFileInput(this);
});

taskFileDocumentField.addEventListener('click', function() {
	clearErrorFileInput(this);
});

function checkMaxItemField(field, existList, maxValue) {
	let result = {'error': ''};

	if (Number(field.files.length) + Number(existList.files.length) > Number(maxValue)) {
		result.error = field.dataset.error_max_value;
	}

	return result;
}

function checkAndUploadFile(field, fileType, fileList) {
	let checkResult = checkUploadFile(field, fileType),
		checkMax = checkMaxItemField(field, fileList, Number(field.dataset.max_value)),
		container = field.closest('.container_create_new_event');

	if (checkResult.error) {
		let noticeElem = field.closest('.create_event_file_field_row').querySelector('.notice_elem');
		noticeElem.innerHTML = checkResult.error;
		noticeElem.classList.add('notice_elem_full_width_active');
	} else if (checkMax.error) {
		let noticeElem = field.closest('.create_event_file_field_row').querySelector('.notice_elem');
		noticeElem.innerHTML = checkMax.error;
		noticeElem.classList.add('notice_elem_full_width_active');
	} else {
		insertLoadFile(container, field, fileType);
	}
}

function clearErrorFileInput(field) {
	let noticeElem = field.closest('.create_event_file_field_row').querySelector('.notice_elem');

	if (noticeElem.classList.contains('notice_elem_full_width_active')) {
		noticeElem.classList.remove('notice_elem_full_width_active');
		noticeElem.innerHTML = '';
	}
}

function insertLoadFile(container, field, fileType) {
	let insertContainer,
		templateContainer;

	if (fileType === FILE_TYPE_VIDEO) {
		insertContainer = container.querySelector('.create_event_video_container');
		templateContainer = document.querySelector('.video_file_template_container');
	}

	if (fileType === FILE_TYPE_IMAGE) {
		insertContainer = container.querySelector('.create_event_image_container');
		templateContainer = document.querySelector('.image_file_template_container');
	}

	if (fileType === FILE_TYPE_DOCUMENT) {
		insertContainer = container.querySelector('.create_event_document_container');
		templateContainer = document.querySelector('.document_file_template_container');
	}

	for (let i = 0; i < field.files.length; i++) {
		let cloneTemplate = templateContainer.firstElementChild.cloneNode(true);

		if (fileType === FILE_TYPE_VIDEO) {
			let videoEl = cloneTemplate.querySelector('.video_el');

			videoEl.src = URL.createObjectURL(field.files[i]);
			videoFileList.items.add(field.files[i]);
			new VideoFileExistElement(cloneTemplate);

			for (let j = 0; j < videoFileList.files.length; j++) {
				if (videoFileList.files[j] === field.files[i]) {
					videoFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}

			insertContainer.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value').
				classList.add('not_active');
		}

		if (fileType === FILE_TYPE_IMAGE) {
			let imageEl = cloneTemplate.querySelector('.image_file');

			imageEl.src = URL.createObjectURL(field.files[i]);
			imageFileList.items.add(field.files[i]);

			for (let j = 0; j < imageFileList.files.length; j++) {
				if (imageFileList.files[j] === field.files[i]) {
					imageFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}
		}

		if (fileType === FILE_TYPE_DOCUMENT) {
			let nameEl = cloneTemplate.querySelector('.document_name_block_template');

			nameEl.innerHTML = field.files[i].name;
			documentFileList.items.add(field.files[i]);

			for (let j = 0; j < documentFileList.files.length; j++) {
				if (documentFileList.files[j] === field.files[i]) {
					documentFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}
		}

		cloneTemplate.setAttribute('data-load_type', 'preload');
		insertContainer.prepend(cloneTemplate);
	}

	if (fileType === FILE_TYPE_VIDEO) {
		field.files = videoFileList.files;
	}

	if (fileType === FILE_TYPE_IMAGE) {
		if (insertContainer.children.length > 0 && insertContainer.children.length < Number(field.dataset.max_value)) {
			insertContainer.classList.add('mb_10');
		}

		if (insertContainer.children.length === Number(field.dataset.max_value) && insertContainer.classList.contains('mb_10')) {
			insertContainer.classList.remove('mb_10');
		}

		field.files = imageFileList.files;

		if (insertContainer.children.length >= Number(field.dataset.max_value)) {
			insertContainer.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value').
				classList.add('not_active');
		}
	}

	if (fileType === FILE_TYPE_DOCUMENT) {
		if (insertContainer.children.length > 0 && insertContainer.children.length < Number(field.dataset.max_value)) {
			insertContainer.classList.add('mb_10');
		}

		if (insertContainer.children.length === Number(field.dataset.max_value) && insertContainer.classList.contains('mb_10')) {
			insertContainer.classList.remove('mb_10');
		}

		field.files = documentFileList.files;

		if (insertContainer.children.length >= Number(field.dataset.max_value)) {
			insertContainer.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value').
				classList.add('not_active');
		}
	}
}