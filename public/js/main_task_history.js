/*jshint esversion: 6 */

let mainBox = document.querySelector('main_box'),
	eventMenuContainer = document.querySelector('.event_menu_container'),
	eventContentContainer = document.querySelector('.event_content_container'),
	taskContentList = document.querySelector('.task_content_list'),
	eventMenuCloseContainer = document.querySelector('.event_menu_close_container'),
	eventMenuCloseIcon = document.querySelector('.event_menu_close_icon'),
	eventMenuIcon = document.querySelector('.event_menu_icon'),
	calendarContainer = document.querySelector('.calendar_container'),
	calendarBox = document.querySelector('.calendar_box'),
	calendarArrow = document.querySelector('.calendar_box_arrow'),
	calendarDateTitle = document.querySelector('.calendar_date_title'),
	calendarPrevArrow = calendarBox.querySelector('.calendar_left_arrow'),
	calendarNextArrow = calendarBox.querySelector('.calendar_right_arrow'),
	calendarTable = calendarBox.querySelector('table'),
	dateFromField = document.querySelector('.filter_date_item_value_from'),
	dateToField = document.querySelector('.filter_date_item_value_to'),
	typeField = document.querySelector('#filter_task_history_status_field'),
	filterApplyButton = document.querySelector('.filter_apply_button'),
	taskId = taskContentList.dataset.task_id,
	sourceId = taskContentList.dataset.source_id,
	scrollEventList = true,
	calendarClass = new CalendarClass({
		'titleDateEl': calendarDateTitle,
		'tableEl': calendarTable,
		'prevArrow': calendarPrevArrow,
		'nextArrow': calendarNextArrow,
		'type': CalendarClass.typeAny,
	}),
	timeOutVideoMediaControl,
   	activeTimeout = false,
   	fullScreen = false,
   	modalSliderImage;

calendarClass.init();

window.addEventListener('load', function() {
	fetchGetTaskHistoryList(getDataTaskHistoryObj());
});

window.addEventListener('resize', function() {
	closeCalendarBox();
	if (document.body.offsetWidth > 820 && eventMenuContainer.classList.contains('menu_container_active')) {
		eventMenuContainer.classList.remove('menu_container_active');

		if (!eventMenuCloseContainer.classList.contains('not_active')) {
			eventMenuCloseContainer.classList.add('not_active');
		}

		if (eventMenuIcon.classList.contains('event_menu_container_not_active')) {
			eventMenuIcon.classList.remove('event_menu_container_not_active');
		}

		if (document.body.classList.contains('scroll_none')) {
			document.body.classList.remove('scroll_none');
		}
	}
});

eventMenuIcon.addEventListener('click', function() {
	eventMenuCloseContainer.classList.remove('not_active');
	eventMenuContainer.classList.add('menu_container_active');
	eventMenuIcon.classList.add('event_menu_container_not_active');
});

eventMenuCloseIcon.addEventListener('click', function() {
	eventMenuContainer.classList.remove('menu_container_active');

	setTimeout(function() {
		eventMenuCloseContainer.classList.add('not_active');
		eventMenuIcon.classList.remove('event_menu_container_not_active');
	}, 220);
});

filterApplyButton.addEventListener('click', function() {
	fetchGetTaskHistoryList(getDataTaskHistoryObj());
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('filter_date_icon')) {
		let container = e.target.parentElement;

		container.append(calendarBox);
		calendarBox.classList.add('calendar_box_position_left');

		setTimeout(function() {
			calendarBox.classList.add('calendar_box_active');
		}, 10);
	}

	if ((!e.target.classList.contains('filter_date_icon') && !e.target.closest('.calendar_box')) ||
		e.target.classList.contains('calendar_container_close_icon') ||
		e.target.classList.contains('calendar_container_close')) {
		closeCalendarBox();
	}

	if (e.target.tagName === 'TD' && e.target.closest('.calendar_box>table')) {
		let container = e.target.closest('.filter_date_item'),
			elValue = container.firstElementChild,
			clearEl = container.querySelector('.filter_date_clear_icon');

		elValue.innerHTML = e.target.getAttribute('data-filter_date');
		clearEl.classList.remove('not_active');
		closeCalendarBox();
	}

	if (e.target.classList.contains('filter_date_clear_icon')) {
		let container = e.target.closest('.filter_date_item'),
			elValue = container.firstElementChild,
			clearEl = container.querySelector('.filter_date_clear_icon');

		elValue.innerHTML = '';
		clearEl.classList.add('not_active');
	}

	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}

	if (e.target.closest('.video_file_preview_container')) {
		let videoContainer = e.target.closest('.video_file_preview_container');
		fetchGetVideoViewModal({'page': 0}, videoContainer);
	}

	if (e.target.closest('.image_file_container_preview')) {
		let imageContainer = e.target.closest('.image_file_container_preview'),
			taskHistoryContainer = imageContainer.closest('.task_history_item_container'),
			taskHistoryId = taskHistoryContainer.dataset.task_history_id,
			fieldDeleteFileIdList = document.querySelector('input[name="deleteFileIdList"]'),
			// exceptionIdList = (fieldDeleteFileIdList.value) ?
			// 	fieldDeleteFileIdList.value.split(',') :
			// 	'',
			dataObj = {
				'type': TYPE_SHOW,
				'entity_id': taskHistoryId,
				'current_file_id': imageContainer.dataset.file_id,
				// 'exceptionIdList': exceptionIdList,
				'entityAction': 'taskHistorySlider',
			};

		fetchGetImageSliderViewModal(dataObj);
	}

	if (e.target.classList.contains('dot_menu_container_pic')) {
		let menuList = e.target.parentElement.querySelector('.file_menu_list');
		menuList.classList.add('menu_box_active');
	}

	if (!e.target.classList.contains('dot_menu_container_pic')) {
		let sliderImage = document.querySelector('.modal_image_slider'),
			modalVideo = document.querySelector('.modal_video');

		if (sliderImage) {
			let activeMenuList = sliderImage.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}

		if (modalVideo) {
			let activeMenuList = modalVideo.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}
	}
});

function closeCalendarBox() {
	if (calendarBox.classList.contains('calendar_box_position_left') || calendarBox.classList.contains('calendar_box_active')) {
		calendarBox.classList.remove('calendar_box_active');

		setTimeout(function() {
			calendarBox.classList.remove('calendar_box_position_left');
			calendarContainer.append(calendarBox);
			calendarClass.count = 0;
			calendarClass.init();
		}, 200);
	}
}

function closeEventItemMenu() {
	let openMenu = document.querySelector('.show_event_menu_container_open');

	if (openMenu) {
		let upMenuItem = openMenu.querySelector('.up_menu_box_item'),
			middleMenuItem = openMenu.querySelector('.middle_menu_box_item'),
			bottomMenuItem = openMenu.querySelector('.bottom_menu_box_item');

		openMenu.classList.remove('show_event_menu_container_open');
		upMenuItem.classList.remove('up_menu_box_item_open');
		middleMenuItem.classList.remove('middle_menu_box_item_open');
		bottomMenuItem.classList.remove('bottom_menu_box_item_open');
	}
}