/*jshint esversion: 6 */

class VideoFileExistElement
{
	constructor(fileContainer)
	{
		this.fileContainer = fileContainer;
		this.videoElement = this.fileContainer.querySelector('video');
		this.currentTimeBlock = this.fileContainer.querySelector('.video_current_time_block');
		this.videoContainer = this.fileContainer.querySelector('.video_in_message_block');
		this.videoControlMediaBlock = this.fileContainer.querySelector('.video_control_media_block');
		this.playIcon = this.fileContainer.querySelector('.video_play_icon');
		this.timeBar = this.fileContainer.querySelector('.video_time_bar');
		this.fullScreenIcon = this.fileContainer.querySelector('.fullscreen_video_icon');
		Object.assign(this, MediaElementControlMixin);
		this.playIconPath = '/img/play_video_icon.png';
		this.pauseIconPath = '/img/pause_video_icon.png';
		this.init();
	}

	init()
	{
		this.fileContainer.setAttribute('media_init', 1);
		
		this.videoElement.onloadeddata = function() {
			this.currentTimeBlock.innerHTML = this.prepareTimeForEl(this.videoElement.duration);
		}.bind(this);

		this.videoContainer.onmousemove= function() {
			if (!this.videoElement.paused) {
				this.showVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videoElement);
				this.hideVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videoElement);
			}
		}.bind(this);

		this.playIcon.onclick = function() {
			this.hideVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videoElement);
			this.playOrPauseVideoOrAudio(this.videoElement, this.playIcon, 'video', this.playIconPath, this.pauseIconPath);
		}.bind(this);

		this.videoElement.onclick = function() {
			this.hideVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videoElement);
			this.playOrPauseVideoOrAudio(this.videoElement, this.playIcon, 'video', this.playIconPath, this.pauseIconPath);
		}.bind(this);

		this.timeBar.onclick = function(e) {
			this.changeCurrentTimeVideoOrAudio(e, this.timeBar, 'video', this.videoElement);
		}.bind(this);

		this.videoElement.onended = function() {
			this.endVideoOrAudio(this.playIcon, this.playIconPath, 'video', this.videoContainer);
			this.showVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videoElement);
		}.bind(this);

		this.videoElement.ontimeupdate = function() {
			this.updateCurrentTime(this.videoElement, this.videoContainer, 'video', this.videoElement.duration);
		}.bind(this);

		this.fullScreenIcon.onclick = function() {
			if (!this.fullScreenIcon.hasAttribute('data-action_video_fullsreen_icon') || this.fullScreenIcon.getAttribute('data-action_video_fullsreen_icon') === 'not_full') {
				fullScreen = true;
				this.videoContainer.requestFullscreen();
				this.videoElement.classList.add('video_el_full_screen');
				this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'full');
				this.fullScreenIcon.src = '/img/smallscreen_video_icon.png';
			} else if (this.fullScreenIcon.getAttribute('data-action_video_fullsreen_icon') === 'full') {
				document.exitFullscreen();
				this.videoElement.classList.remove('video_el_full_screen');
				this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'not_full');
				this.fullScreenIcon.src = '/img/fullscreen_video_icon.png';
				setTimeout(function() {
					fullScreen = false;
				}, 100);
			}
		}.bind(this);

		this.videoContainer.onfullscreenchange = function(e) {
			if (document.fullscreenElement == null) {
				this.videoElement.classList.remove('video_el_full_screen');
				this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'not_full');
				this.fullScreenIcon.src = '/img/fullscreen_video_icon.png';
				// dialogOrChatContent.scrollTop = this.dialogOrChatScrollTop
				setTimeout(function() {
					fullScreen = false;
				}, 100);
			}
		}.bind(this)
	}
}