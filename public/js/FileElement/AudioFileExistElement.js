/*jshint esversion: 6 */

class AudioFileExistElement
{
	constructor(fileContainer)
	{
		this.fileContainer = fileContainer;
		this.audioElement = this.fileContainer.querySelector('audio');
		this.audioContainer = this.fileContainer.querySelector('.select_audio_message_block');
		this.playIcon = this.fileContainer.querySelector('.audio_play_icon');
		this.timeBar = this.fileContainer.querySelector('.audio_time_bar');
		this.currentTimeBlock = this.fileContainer.querySelector('.audio_current_time_block');
		this.playIconPath = '/img/play_icon.png';
		this.pauseIconPath = '/img/pause_icon.png';
		Object.assign(this, MediaElementControlMixin);
		this.init();
	}

	init()
	{
		this.fileContainer.setAttribute('media_init', 1);

		this.audioElement.onloadeddata = function() {
			this.currentTimeBlock.innerHTML = this.prepareTimeForEl(this.audioElement.duration);
		}.bind(this);

		this.playIcon.onclick = function() {
			this.playOrPauseVideoOrAudio(this.audioElement, this.playIcon, 'audio', this.playIconPath, this.pauseIconPath);
		}.bind(this);

		this.timeBar.onclick = function(e) {
			this.changeCurrentTimeVideoOrAudio(e, this.timeBar, 'audio', this.audioElement);
		}.bind(this);

		this.audioElement.onended = function() {
			this.endVideoOrAudio(this.playIcon, this.playIconPath, 'audio', this.audioContainer);
		}.bind(this);

		this.audioElement.ontimeupdate = function() {
			this.updateCurrentTime(this.audioElement, this.audioContainer, 'audio', this.audioElement.duration);
		}.bind(this);
	}
}