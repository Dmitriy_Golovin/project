class FileElement {
	
	container;
	fileControlBlock

	constructor(data) {
		this.parentEl = data['parentEl'];
		this.type = data['type'];
	}

	init() {
		this.createContainer();
	}

	createContainer() {
		this.container = document.createElement('div');

		if (this.type === 'prepareForMessage') {
			this.container.classList.add('file_container_for_message');
			this.createFileControlBlock();
			this.container.append(this.fileControlBlock);
		}

		if (this.type === 'inMessage') {
			this.container.classList.add('file_container_in_message');
		}
	}

	createFileControlBlock() {
		this.fileControlBlock = document.createElement('div');

		if (this.type === 'prepareForMessage') {
			this.fileControlBlock.classList.add('select_file_control_block');
		}
	}

	static createPrivateInputElement(privacyStatus) {
		let iconPrivacyBlock = document.createElement('div'),
		iconPrivacy = document.createElement('img');

		iconPrivacyBlock.classList.add('private_public_icon_message_block');
		iconPrivacy.classList.add('private_public_icon_message');

		if (Number(privacyStatus) === 2) {
			iconPrivacy.src = '/img/private_icon.png';
		} else {
			iconPrivacy.src = '/img/public_icon.png';
		}

		iconPrivacyBlock.append(iconPrivacy);

		return iconPrivacyBlock;
	}

	static createIconSelectElement(selected) {
		let iconSelectBlock = document.createElement('div'),
		iconSelect = document.createElement('img');

		iconSelectBlock.classList.add('select_icon_message_block');
		iconSelect.classList.add('select_icon_message');

		if (selected) {
			iconSelect.src = '/img/selected_icon.png';
			iconSelect.classList.add('selected_file');
		} else {
			iconSelect.src = '/img/not_selected_icon.png';
		}

		iconSelectBlock.append(iconSelect);

		return iconSelectBlock;
	}

	static clearContainerForFilesOfMessage() {
		let container = document.querySelector('.available_file_message_block'),
			countFileBlock = document.querySelector('.count_of_files_block');

		for (let el of container.children) {
			el.innerHTML = '';
		}

		container.classList.remove('available_file_message_block_active');
		countFileBlock.innerHTML = '';
		countFileBlock.classList.remove('count_of_files_block_active');
		this.setHeightForMessagesBlock();
	}

	static setHeightForMessagesBlock() {
		let block = document.querySelector('.messages_block');

		// if (block && block.nextElementSibling.classList.contains('write_message_box')) {
		// 	block.style.height = block.parentElement.offsetHeight - fileMessageBlock.parentElement.offsetHeight - 44 + 'px';
		// 	new ResizeObserver(scrollDownElement.bind(null, block)).observe(block.nextElementSibling);
		// }
	}

	static showAccessDenied(parentEl) {
		let element = document.createElement('span');

		element.classList.add('file_access_denied');
		element.innerHTML = 'Доступ к файлу запрещен';
		parentEl.append(element);
	}
}