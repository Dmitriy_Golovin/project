class ImageFileElement extends FileElement {

	imageContainer;
	imageElement;

	static fileType = 1;
	static mainCssClassForContainer = 'main_file_image_container';

	constructor(data, fileData) {
		super(data);
		this.fileId = fileData['fileId'];
		this.url = fileData['url'];
		this.privacyStatus = fileData['privacyStatus'] || null;
		this.selected = fileData['selected'] || null;
		this.previewHeight = fileData['previewHeight'] || null;
		this.previewWidth = fileData['previewWidth'] || null;
		this.insertType = fileData['insertType'] || 'append';
		this.init();
	}

	init() {
		super.init();

		if (this.insertType == 'append') {
			this.parentEl.append(this.container);
		}

		if (this.insertType == 'prepend') {
			this.parentEl.prepend(this.container);
		}

		this.container.setAttribute('data-file_id', this.fileId);
		this.container.setAttribute('data-file_type', ImageFileElement.fileType);
		this.container.classList.add(ImageFileElement.mainCssClassForContainer);

		this.createImageContainer();

		if (this.type === 'prepareForMessage') {
			this.container.classList.add('image_file_message_container');
			this.imageContainer.classList.add('select_image_message_block');
			this.fileControlBlock.append(ImageFileElement.createPrivateInputElement(this.privacyStatus));
			this.fileControlBlock.append(ImageFileElement.createIconSelectElement(this.selected));
			this.imageElement.classList.add('change_ava_available_image');
			this.imageElement.classList.add('available_file_for_message');

			if (this.selected) {
				selectedFileList.push(this.container);
			}
		}

		if (this.type === 'inMessage') {
			this.imageContainer.classList.add('image_file_in_message_block');
			this.imageElement.classList.add('image_file_in_message');

			this.imageElement.addEventListener('click', function(e) {
				modalSliderImage.show(e.target);
			})
		}

		if (this.type === 'slider') {
			if (this.parentEl.classList.contains('slider_image_container')) {
				this.container.classList.add('image_file_slider_container');
			}

			if (this.parentEl.classList.contains('slider_image_container_mobile')) {
				this.container.classList.add('image_file_slider_container_mobile');
			}

			this.imageContainer.classList.add('image_file_in_slider_block');
			this.imageElement.classList.add('image_file_in_slider');
		}
	}

	createImageContainer() {
		this.imageContainer = document.createElement('div');

		this.createImageElement();
		this.imageContainer.append(this.imageElement);
		this.container.append(this.imageContainer);
	}

	createImageElement() {
		this.imageElement = document.createElement('img');
		this.imageElement.src = this.url;

		if (this.previewHeight) {
			this.imageElement.style.height = this.previewHeight + 'px';
		}
	}
}