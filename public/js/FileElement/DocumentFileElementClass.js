class DocumentFileElement extends FileElement {
	
	fileType = 4;
	mainCssClassForContainer = 'main_file_document_container';
	documentContainer;
	iconBlock;
	iconEl;
	nameDateContainer;
	nameBlock;
	dateBlock;
	downloadAnchor;

	constructor(data, fileData) {
		super(data);
		this.fileId = fileData['fileId'];
		this.url = fileData['url'];
		this.privacyStatus = fileData['privacyStatus'];
		this.selected = fileData['selected'];
		this.name = fileData['name'];
		this.date = fileData['date'];
		this.init();
	}

	init() {
		super.init();
		this.container.setAttribute('data-file_id', this.fileId);
		this.container.setAttribute('data-file_type', this.fileType);
		this.container.classList.add(this.mainCssClassForContainer);

		this.createDocumentContainer();
		this.parentEl.append(this.container);

		if (this.type === 'prepareForMessage') {
			this.createNameDateContainer();
			this.documentContainer.append(this.nameDateContainer);
			this.container.classList.add('document_file_message_container');
			this.documentContainer.classList.add('select_document_message_block');
			this.fileControlBlock.append(DocumentFileElement.createPrivateInputElement(this.privacyStatus));
			this.fileControlBlock.append(DocumentFileElement.createIconSelectElement(this.selected));

			if (this.selected) {
				selectedFileList.push(this.container);
			}
		}

		if (this.type === 'inMessage') {
			this.createDownloadAnchor();
			this.createNameBlock();
			this.downloadAnchor.append(this.documentContainer);
			this.container.append(this.downloadAnchor);
			this.container.classList.add('document_file_in_message_container');
			this.documentContainer.classList.add('document_file_message_block');
			this.documentContainer.append(this.nameBlock);
		}

		this.setBoxBottomBorder();
	}

	createDocumentContainer() {
		this.documentContainer = document.createElement('div');

		this.createIconBlock();
		this.documentContainer.append(this.iconBlock);

		if (this.type === 'prepareForMessage') {
			this.container.append(this.documentContainer);
		}

		if (this.type === 'inMessage') {
			
		}
	}

	createDownloadAnchor() {
		this.downloadAnchor = document.createElement('a');
		this.downloadAnchor.classList.add('document_download_anchor');
		this.downloadAnchor.href = this.url;
	}

	createIconBlock() {
		this.iconBlock = document.createElement('div');
		this.iconEl = document.createElement('img');

		if (this.type === 'prepareForMessage') {
			this.iconBlock.classList.add('document_icon_block');
			this.iconEl.classList.add('document_icon_el');
		}

		if (this.type === 'inMessage') {
			this.iconBlock.classList.add('document_icon_block_in_message');
			this.iconEl.classList.add('document_icon_el_in_message');
		}

		this.iconEl.src = '/img/document_icon.png';

		this.iconBlock.append(this.iconEl);
	}

	createNameDateContainer() {
		this.nameDateContainer = document.createElement('div');

		this.nameDateContainer.classList.add('name_date_document_container');

		if (this.type === 'prepareForMessage') {
			this.createNameBlock();
			this.createDateBlock();
			this.nameDateContainer.append(this.nameBlock);
			this.nameDateContainer.append(this.dateBlock);
		}
	}

	createNameBlock() {
		this.nameBlock = document.createElement('div');
		this.nameBlock.innerHTML = this.name;

		if (this.type === 'prepareForMessage') {
			this.nameBlock.classList.add('document_name_block');
		}

		if (this.type === 'inMessage') {
			this.nameBlock.classList.add('document_name_block_in_message');
		}
	}

	createDateBlock() {
		this.dateBlock = document.createElement('div');
		this.dateBlock.classList.add('document_date_block');
		this.dateBlock.innerHTML = this.date;
	}

	setBoxBottomBorder() {
		let previousEl = this.container.previousElementSibling;

		if (previousEl && previousEl.classList.contains(this.mainCssClassForContainer)) {
			previousEl.classList.add('box_bottom_border');
		}
	}
}