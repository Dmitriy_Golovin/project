/*jshint esversion: 6 */

let MediaElementControlMixin = {

	prepareTimeForEl(time) {
		let mainTime =  Math.floor(time),
			minutes = mainTime / 60,
			seconds = mainTime % 60;

		if (String(minutes).length < 2) {
			minutes = Number(String(0) + minutes);
		}

		if (String(seconds).length < 2) {
			seconds = String(0) + seconds;
		}

		return Math.floor(minutes) + ':' + seconds;
	},

	playOrPauseVideoOrAudio(mediaContent, playIconEl, elemName, playIcon, pauseIcon) {
		if (!playIconEl.hasAttribute('data-action_media_icon') || playIconEl.getAttribute('data-action_media_icon') === 'play') {
			this.setPausedForActiveMediaElem();
			mediaContent.play();
			playIconEl.setAttribute('data-action_media_icon', 'pause');
			playIconEl.src = pauseIcon;
		} else if (playIconEl.getAttribute('data-action_media_icon') === 'pause') {
			mediaContent.pause();
			playIconEl.setAttribute('data-action_media_icon', 'play');
			playIconEl.src = playIcon;

			if (elemName == 'video') {
				let mediaContainer = playIconEl.closest('.file_container_for_message') ||
					playIconEl.closest('.file_container_in_message') ||
					playIconEl.closest('.file_container_preload') ||
					playIconEl.closest('.file_container_in_modal');
				clearTimeout(timeOutVideoMediaControl);
				this.showVideoControlContainer(
					mediaContainer.querySelector('.video_control_media_block'),
					mediaContainer.querySelector('.select_video_message_block') || mediaContainer.querySelector('.video_in_message_block')/*,
					mediaEl*/
				);
			}
		}
	},

	setPausedForActiveMediaElem() {
		let playIcon = document.querySelector('img[data-action_media_icon="pause"]');

		if (playIcon) {
			let mediaContainer = playIcon.closest('.file_container_for_message') || playIcon.closest('.file_container_in_message'),
				mediaEl = mediaContainer.querySelector('.media_el');

			mediaEl.pause();
			playIcon.setAttribute('data-action_media_icon', 'play');

			if (mediaEl.tagName === 'AUDIO') {
				playIcon.src = '/img/play_icon.png';
			}

			if (mediaEl.tagName === 'VIDEO') {
				playIcon.src = '/img/play_video_icon.png';
				clearTimeout(timeOutVideoMediaControl);
				this.showVideoControlContainer(
					mediaContainer.querySelector('.video_control_media_block'),
					mediaContainer.querySelector('.select_video_message_block') || mediaContainer.querySelector('.video_in_message_block')/*,
					mediaEl*/
				);
			}
		}
	},

	changeCurrentTimeVideoOrAudio(e, el, elemName, content) {
		let clickCoordX = e.offsetX,
			width = el.getBoundingClientRect().width,
			widthInner = clickCoordX * 100 / width;

		el.querySelector('.' + elemName + '_time_bar_inner').style.width = widthInner + '%';
		content.currentTime = widthInner * content.duration / 100;
	},

	endVideoOrAudio(playIconEl, playIcon, elemName, container) {
		playIconEl.src = playIcon;
		playIconEl.removeAttribute('data-action_media_icon');
		container.querySelector('.' + elemName + '_time_bar_inner').style.width = 0 + '%';
		
		let mediaContainer = playIconEl.closest('.file_container_for_message') ||
			playIconEl.closest('.file_container_in_message') ||
			playIconEl.closest('.file_container_preload') ||
			playIconEl.closest('.file_container_in_modal'),
			mediaEl = mediaContainer.querySelector('.media_el');

		if (mediaEl.tagName === 'VIDEO') {
			clearTimeout(timeOutVideoMediaControl);
			this.showVideoControlContainer(
				mediaContainer.querySelector('.video_control_media_block'),
				mediaContainer.querySelector('.select_video_message_block') || mediaContainer.querySelector('.video_in_message_block')/*,
				mediaEl*/
			);
		}
	},

	updateCurrentTime(el, container, elemName, duration) {
		let currentTime = el.currentTime;

		container.querySelector('.' + elemName + '_time_bar_inner').style.width = (currentTime * 100 / el.duration) + '%';
		container.querySelector('.' + elemName + '_current_time_block').innerHTML = this.prepareTimeForEl(currentTime);
	},

	showVideoControlContainer(controlContainer, videoBlock,/* videoEl*/) {
		controlContainer.classList.remove('not_active');
		videoBlock.classList.remove('cursor_none');
	},

	hideVideoControlContainer(controlContainer, videoBlock, videoEl) {
		clearTimeout(timeOutVideoMediaControl);
		if (!videoEl.paused) {
			timeOutVideoMediaControl = setTimeout(function() {
				controlContainer.classList.add('not_active');
				videoBlock.classList.add('cursor_none');
			}, 2000);
		}
	}
}