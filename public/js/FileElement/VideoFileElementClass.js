/*jshint esversion: 6 */

class VideoFileElement extends FileElement {

	fileType = 3;
	mainCssClassForContainer = 'main_file_video_container';
	playIconPath = '/img/play_video_icon.png';
	pauseIconPath = '/img/pause_video_icon.png';
	videoContainer;
	videolement;
	videoControlMediaBlock;
	playBlock;
	playIcon;
	stateBlock;
	timeBar;
	timeBarInner;
	currentTimeBlock;
	fullScreenBlock;
	fullScreenIcon;
	nameBlock;

	constructor(data, fileData) {
		super(data);
		this.fileId = fileData.fileId;
		this.url = fileData.url;
		this.privacyStatus = fileData.privacyStatus;
		this.selected = fileData.selected;
		this.name = fileData.name;
		Object.assign(this, MediaElementControlMixin);
		this.init();
	}

	init() {
		super.init();
		this.parentEl.append(this.container);
		this.container.setAttribute('data-file_id', this.fileId);
		this.container.setAttribute('data-file_type', this.fileType);
		this.container.classList.add(this.mainCssClassForContainer);

		this.createVideoContainer();

		if (this.type === 'prepareForMessage') {
			this.container.classList.add('video_file_message_container');
			this.videoContainer.classList.add('select_video_message_block');
			this.videoContainer.setAttribute('data-css_eskiz-class', 'video_eskiz_block');
			this.fileControlBlock.append(VideoFileElement.createPrivateInputElement(this.privacyStatus));
			this.fileControlBlock.append(VideoFileElement.createIconSelectElement(this.selected));

			if (this.selected) {
				selectedFileList.push(this.container);
			}
		}

		if (this.type === 'inMessage') {
			this.container.classList.add('video_file_in_message_container');
			this.videoContainer.classList.add('video_in_message_block');
		}

		this.videolement.onloadeddata = function() {
			this.currentTimeBlock.innerHTML = this.prepareTimeForEl(this.videolement.duration);
		}.bind(this);

		this.videoContainer.onmousemove= function() {
			if (!this.videolement.paused) {
				this.showVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videolement);
				this.hideVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videolement);
			}
		}.bind(this);

		this.playIcon.onclick = function() {
			this.hideVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videolement);
			this.playOrPauseVideoOrAudio(this.videolement, this.playIcon, 'video', this.playIconPath, this.pauseIconPath);
		}.bind(this);

		this.videolement.onclick = function() {
			this.hideVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videolement);
			this.playOrPauseVideoOrAudio(this.videolement, this.playIcon, 'video', this.playIconPath, this.pauseIconPath);
		}.bind(this);

		this.timeBar.onclick = function(e) {
			this.changeCurrentTimeVideoOrAudio(e, this.timeBar, 'video', this.videolement);
		}.bind(this);

		this.videolement.onended = function() {
			this.endVideoOrAudio(this.playIcon, this.playIconPath, 'video', this.videoContainer);
			this.showVideoControlContainer(this.videoControlMediaBlock, this.videoContainer, this.videolement);
		}.bind(this);

		this.videolement.ontimeupdate = function() {
			this.updateCurrentTime(this.videolement, this.videoContainer, 'video', this.videolement.duration);
		}.bind(this);

		this.fullScreenIcon.onclick = function() {
			if (!this.fullScreenIcon.hasAttribute('data-action_video_fullsreen_icon') || this.fullScreenIcon.getAttribute('data-action_video_fullsreen_icon') === 'not_full') {
				// this.dialogOrChatScrollTop = dialogOrChatContent.scrollTop;
				this.videoContainer.requestFullscreen();
				this.videoElement.classList.add('video_el_full_screen');
				this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'full');
				this.fullScreenIcon.src = '/img/smallscreen_video_icon.png';
			} else if (this.fullScreenIcon.getAttribute('data-action_video_fullsreen_icon') === 'full') {
				document.exitFullscreen();
				this.videoElement.classList.remove('video_el_full_screen');
				this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'not_full');
				this.fullScreenIcon.src = '/img/fullscreen_video_icon.png';
			}
		}.bind(this);

		this.videoContainer.onfullscreenchange = function(e) {
			if (document.fullscreenElement == null) {
				this.videoElement.classList.remove('video_el_full_screen');
				this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'not_full');
				this.fullScreenIcon.src = '/img/fullscreen_video_icon.png';
				// dialogOrChatContent.scrollTop = this.dialogOrChatScrollTop
			}
		}.bind(this)
	}

	createVideoContainer() {
		this.videoContainer = document.createElement('div');

		this.createVideoElement();
		this.createVideoControlMediaBlock();
		this.createNameBlock();
		this.videoContainer.append(this.videolement);
		this.videoContainer.append(this.videoControlMediaBlock);
		this.container.append(this.videoContainer);
		this.container.append(this.nameBlock);
	}

	createVideoElement() {
		this.videolement = document.createElement('video');
		this.videolement.src = this.url;
		this.videolement.setAttribute('controlsList', 'nodownload');
		this.videolement.classList.add('video_el');
		this.videolement.classList.add('media_el');
	}

	createVideoControlMediaBlock() {
		this.videoControlMediaBlock = document.createElement('div');

		this.videoControlMediaBlock.classList.add('video_control_media_block');

		this.createPlayBlock();
		this.createStateBlock();
		this.createCurrentTimeBlock();
		this.createFullScreenBlock();
		this.videoControlMediaBlock.append(this.playBlock);
		this.videoControlMediaBlock.append(this.stateBlock);
		this.videoControlMediaBlock.append(this.currentTimeBlock);
		this.videoControlMediaBlock.append(this.fullScreenBlock);
	}

	createPlayBlock() {
		this.playBlock = document.createElement('div');
		this.playIcon = document.createElement('img');

		this.playBlock.classList.add('video_play_block');
		this.playIcon.classList.add('video_play_icon');
		this.playIcon.src = this.playIconPath;
		this.playIcon.setAttribute('data-action_media_icon', 'play');

		this.playBlock.append(this.playIcon);
	}

	createStateBlock() {
		this.stateBlock = document.createElement('div');
		this.timeBar = document.createElement('div');
		this.timeBarInner = document.createElement('div');

		this.stateBlock.classList.add('video_state_block');
		this.timeBar.classList.add('video_time_bar');
		this.timeBarInner.classList.add('video_time_bar_inner');

		this.timeBar.append(this.timeBarInner);
		this.stateBlock.append(this.timeBar);
	}

	createCurrentTimeBlock() {
		this.currentTimeBlock = document.createElement('div');

		this.currentTimeBlock.classList.add('video_current_time_block');
	}

	createFullScreenBlock() {
		this.fullScreenBlock = document.createElement('div');
		this.fullScreenIcon = document.createElement('img');

		this.fullScreenBlock.classList.add('fullscreen_video_block');
		this.fullScreenIcon.classList.add('fullscreen_video_icon');
		this.fullScreenIcon.src = '/img/fullscreen_video_icon.png';
		this.fullScreenIcon.setAttribute('data-action_video_fullsreen_icon', 'not_full');

		this.fullScreenBlock.append(this.fullScreenIcon);
	}

	createNameBlock() {
		this.nameBlock = document.createElement('div');

		this.nameBlock.classList.add('video_name_block');
		this.nameBlock.innerHTML = this.name.slice(0, this.name.lastIndexOf('.'));
	}
}