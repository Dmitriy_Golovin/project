class AudioFileElement extends FileElement {

	fileType = 2;
	mainCssClassForContainer = 'main_file_audio_container';
	playIconPath = '/img/play_icon.png';
	pauseIconPath = '/img/pause_icon.png';
	audioContainer;
	audiolement;
	playBlock;
	playIcon;
	contentBlock;
	currentTimeBlock;
	timeBar;

	constructor(data, fileData) {
		super(data);
		this.fileId = fileData['fileId'];
		this.url = fileData['url'];
		this.privacyStatus = fileData['privacyStatus'];
		this.selected = fileData['selected'];
		this.name = fileData['name'];
		Object.assign(this, MediaElementControlMixin);
		this.init();
	}

	init() {
		super.init();
		this.parentEl.append(this.container);
		this.container.setAttribute('data-file_id', this.fileId);
		this.container.setAttribute('data-file_type', this.fileType);
		this.container.classList.add(this.mainCssClassForContainer);

		this.createAudioContainer();

		if (this.type === 'prepareForMessage') {
			this.container.classList.add('audio_file_message_container');
			this.audioContainer.classList.add('select_audio_message_block');
			this.fileControlBlock.append(AudioFileElement.createPrivateInputElement(this.privacyStatus));
			this.fileControlBlock.append(AudioFileElement.createIconSelectElement(this.selected));

			if (this.selected) {
				selectedFileList.push(this.container);
			}
		}

		if (this.type === 'inMessage') {
			this.container.classList.add('audio_file_in_message_container');
			this.audioContainer.classList.add('select_audio_message_block');
		}

		let previousEl = this.container.previousElementSibling;

		if (previousEl && previousEl.classList.contains(this.mainCssClassForContainer)) {
			previousEl.classList.add('box_bottom_border');
		}

		this.audiolement.onloadeddata = function() {
			this.currentTimeBlock.innerHTML = this.prepareTimeForEl(this.audiolement.duration);
		}.bind(this);

		this.playIcon.onclick = function() {
			this.playOrPauseVideoOrAudio(this.audiolement, this.playIcon, 'audio', this.playIconPath, this.pauseIconPath);
		}.bind(this);

		this.timeBar.onclick = function(e) {
			this.changeCurrentTimeVideoOrAudio(e, this.timeBar, 'audio', this.audiolement);
		}.bind(this);

		this.audiolement.onended = function() {
			this.endVideoOrAudio(this.playIcon, this.playIconPath, 'audio', this.audioContainer);
		}.bind(this);

		this.audiolement.ontimeupdate = function() {
			this.updateCurrentTime(this.audiolement, this.audioContainer, 'audio', this.audiolement.duration);
		}.bind(this);
	}

	createAudioContainer() {
		this.audioContainer = document.createElement('div');

		this.createAudioElement();
		this.createPlayBlock();
		this.createContentBlock();
		this.audioContainer.append(this.playBlock);
		this.audioContainer.append(this.contentBlock);
		this.audioContainer.append(this.audiolement);
		this.container.append(this.audioContainer);
	}

	createAudioElement() {
		this.audiolement = document.createElement('audio');
		this.audiolement.src = this.url;
		this.audiolement.setAttribute('preload', 'metadata');
		this.audiolement.classList.add('media_el');
	}

	createPlayBlock() {
		this.playIcon = document.createElement('img');

		this.playBlock = document.createElement('div');
		this.playBlock.classList.add('audio_play_block');
		this.playBlock.classList.add('audio_play_icon');
		this.playIcon.src = '/img/play_icon.png';
		this.playIcon.setAttribute('data-action_media_icon', 'play');
		this.playIcon.classList.add('audio_play_icon');

		this.playBlock.append(this.playIcon);
	}

	createContentBlock() {
		this.timeBar = document.createElement('div');
		let nameBlock = document.createElement('div'),
			stateBlock = document.createElement('div'),
			timeBarInner = document.createElement('div');

		this.contentBlock = document.createElement('div');
		this.contentBlock.classList.add('audio_content_block');
		nameBlock.classList.add('audio_name_block');
		stateBlock.classList.add('audio_state_block');
		this.timeBar.classList.add('audio_time_bar');
		timeBarInner.classList.add('audio_time_bar_inner');
		nameBlock.innerHTML = this.name.slice(0, this.name.lastIndexOf('.'));
		nameBlock.setAttribute('data-file_url', this.url);

		this.createCurrentTimeBlock();
		this.timeBar.append(timeBarInner);
		stateBlock.append(this.timeBar);
		this.contentBlock.append(nameBlock);
		this.contentBlock.append(stateBlock);
		this.contentBlock.append(this.currentTimeBlock);
	}

	createCurrentTimeBlock() {
		this.currentTimeBlock = document.createElement('div');

		this.currentTimeBlock.classList.add('audio_current_time_block');
	}
}