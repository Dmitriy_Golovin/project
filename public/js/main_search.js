/*jshint esversion: 6 */

var searchOptionsButton = document.querySelector('.search_options_button'),
	searchOptionsLeftArrow = document.querySelector('.search_options_left_arrow'),
	searchOptionsRightArrow = document.querySelector('.search_options_right_arrow'),
	mainFieldsSearchOptions = document.querySelector('.main_fields_search_options'),
	minorFieldsSearchOptions = document.querySelector('.minor_fields_search_options'),
	minorFieldsSearchChildrenList = minorFieldsSearchOptions.children,
	mainSearchSelect = document.querySelector('.main_search_select'),
	searchFieldsHolidaysBlock = document.querySelector('.f_holidays'),
	searchFieldsUsersBlock = document.querySelector('.f_users'),
	searchUserCountrySelect = document.querySelector('.search_user_country'),
	searchUserCitySelect = document.querySelector('.search_user_city'),
	searchHolidayCountrySelect = document.querySelector('.search_holiday_country'),
	searchHolidayNameSelect = document.querySelector('.search_holiday_value'),
	startSearchButton = document.querySelector('.start_search_button'),
	mainFieldQuery = document.querySelector('.search_field_block>input[type="search"]'),
	emojiContainer = document.querySelector('.emoji_block'),
	writeMessageField = document.querySelector('.write_message_block>textarea'),
	sendMessageButton = document.querySelector('.send_message_to_user_button'),
	cursorPositionInMessageField = 0,
	scrollSearch = false;

window.addEventListener('load', function() {
	if (writeMessageField) {
		addEmojiToMessageFieldModal(emojiContainer, writeMessageField, sendMessageButton);
	}
});

searchOptionsButton.addEventListener('click', function() {
	searchOptionsLeftArrow.classList.toggle('search_options_left_arrow_open');
	searchOptionsRightArrow.classList.toggle('search_options_right_arrow_open');
	mainFieldsSearchOptions.classList.toggle('fields_block_search_options_active');
	
	if (searchOptionsButton.innerHTML === 'показать параметры поиска') {
		searchOptionsButton.innerHTML = 'скрыть параметры поиска';
	} else if (searchOptionsButton.innerHTML === 'скрыть параметры поиска') {
		searchOptionsButton.innerHTML = 'показать параметры поиска';
		hideMinorBlockFields();
		mainSearchSelect.selectedIndex = 0;
	}
});

mainSearchSelect.addEventListener('change', function() {
	searchUserCountrySelect.selectedIndex = 0;
	searchHolidayCountrySelect.selectedIndex = 0;
	searchUserCitySelect.selectedIndex = 0;
	if (mainSearchSelect.value === 'f_holidays') {
		searchFieldsHolidaysBlock.classList.add('holiday_fields_block_search_options_active');
		hideMinorBlock('holiday_fields_block_search_options_active');
	} else if (mainSearchSelect.value === 'f_users') {
		searchFieldsUsersBlock.classList.add('user_fields_block_search_options_active');
		hideMinorBlock('user_fields_block_search_options_active');
	}
});

searchUserCountrySelect.addEventListener('change', function() {
	var countryList = searchUserCountrySelect.children;
	for (var i = 0; i < countryList.length; i++) {
		if (searchUserCountrySelect.value == countryList[i].value) {
			tab.checkToken(function() {	
				ajaxGetCitiesForSearch(countryList[i].getAttribute('data-countryId'));
			});
			break;
		}
	}
});

function hideMinorBlockFields() {
	searchUserCitySelect.innerHTML = '';
	for (var i = 0; i < minorFieldsSearchChildrenList.length; i++) {
		minorFieldsSearchChildrenList[i].classList.remove('user_fields_block_search_options_active');
		minorFieldsSearchChildrenList[i].classList.remove('holiday_fields_block_search_options_active');
		for (var j = 0; j < minorFieldsSearchChildrenList[i].children.length; j++) {
			if (minorFieldsSearchChildrenList[i].children[j].tagName === 'SELECT') {
				minorFieldsSearchChildrenList[i].children[j].selectedIndex = 0;
			}
			minorFieldsSearchChildrenList[i].children[j].classList.remove('minor_field_for_search_active');
		}
	}
}

function hideMinorBlock(classOfEl) {
	searchUserCitySelect.innerHTML = '';
	var classArr = ['user_fields_block_search_options_active', 'holiday_fields_block_search_options_active'];
	for (var i = 0; i < minorFieldsSearchChildrenList.length; i++) {
		for (var j = 0; j < classArr.length; j++) {
			if (classArr[j] !== classOfEl) {
				minorFieldsSearchChildrenList[i].classList.remove(classArr[j]);
			}
		}
	}
}

startSearchButton.addEventListener('click', function() {
	var link = '/search?',
		obj = getFieldsValue();
	
	for (var key in obj) {
		if (obj[key] && obj[key] !== 'Выберите город' ) {
			link += key + '=' + obj[key] + '&';
		}
	}
	
	link = link.slice(0, -1);
	
	history.pushState(null, null, link);
	tab.checkToken(function() {
		getDataForSearch(getDataForAjax(link));
	});
});

window.addEventListener('popstate', function() {
	tab.checkToken(function() {
		getDataForSearch(getDataForAjax('/search' + location['search']));
	});
});

window.addEventListener('load', function() {
	tab.checkToken(function() {	
		getDataForSearch(getDataForAjax('/search' + location['search']));
	});
});

function getFieldsValue() {
	var searchValueObj = {
		'q': mainFieldQuery.value,
		'm_v': mainSearchSelect.value,
		'u_co': searchUserCountrySelect.options[searchUserCountrySelect.selectedIndex].getAttribute('data-countryId'),
		'u_ci': (searchUserCitySelect.options.length) ? searchUserCitySelect.options[searchUserCitySelect.selectedIndex].getAttribute('data-cityId') : '',
		'h_co': searchHolidayCountrySelect.options[searchHolidayCountrySelect.selectedIndex].getAttribute('data-countryId'),
	};
	
	return searchValueObj;
}

function getDataForAjax(str) {
	var obj = {};
	if (str.indexOf('?') !== -1) {
		var res = str.slice(str.indexOf('?') + 1).split('&');

		for (var i = 0; i < res.length; i++) {
			var key = res[i].slice(0, res[i].indexOf('='));
			obj[key] = res[i].slice(res[i].indexOf('=') + 1);
		}
	}
	return obj;
}

document.body.addEventListener('click', function(e) {
	if (!e.target.closest('.emoji_block') && e.target !== writeMessageField) {
		cursorPositionInMessageField = 0;
	}

	if (e.target.closest('.emoji_block') !== emojiContainer) {
		emojiContainer.querySelector('.emoji_box').classList.remove('menu_box_active');
		emojiContainer.querySelector('.emoji_box').parentElement.classList.remove('menu_box_active');
		writeMessageField.classList.remove('message_field_modal_emoji_active');
	}
	
	if (e.target === document.querySelector('.main_query_h3_holiday_search_result')) {
		var url = location['href'],
			addParam = 'm_v=' + document.querySelector('.main_query_h3_holiday_search_result').getAttribute('f');

		if (url.indexOf(addParam) >= 0) {
			return;
		}

		if (url.indexOf('?') >= 0) {
			history.pushState(null, null, url + '&' + addParam);
		} else {
			history.pushState(null, null, url + '?' + addParam);
		}

		getDataForSearch(getDataForAjax('/search' + location.search));
	}

	if (e.target === document.querySelector('.main_query_h3_user_search_result')) {
		let url = location.href,
			addParam = 'm_v=' + document.querySelector('.main_query_h3_user_search_result').getAttribute('f');

		if (url.indexOf(addParam) >= 0) {
			return;
		}

		if (url.indexOf('?') >= 0) {
			history.pushState(null, null, url + '&' + addParam);
		} else {
			history.pushState(null, null, url + '?' + addParam);
		}
		
		getDataForSearch(getDataForAjax('/search' + location.search));
	}

	if (e.target.classList.contains('send_message_button')) {
		let userContainer = e.target.closest('.friend_result_item'),
			from = tab.userID,
			to = userContainer.dataset.profile_id,
			userFullnameBlock = userContainer.querySelector('.user_name_block_friend_result_item>a'),
			toFullName = userFullnameBlock.innerHTML;

		activateWriteNewMessageBox(from, to, toFullName);
	}

	if (e.target.classList.contains('invite_button_friend_result_item')) {
		let profileId = e.target.closest('.friend_result_item').dataset.profile_id;
		activateInviteModal(profileId);
	}
});

document.addEventListener('scroll', function(e) {
	let documetScroll = document.documentElement.scrollTop,
		holidayContainer = document.querySelector('.holiday_query_search'),
		userContainer = document.querySelector('.user_query_search');

	if (holidayContainer) {
		let holidayCount = holidayContainer.getAttribute('data-holidayCount'),
			timestamp = holidayContainer.getAttribute('data-timestamp'),
			existHolidayCount = holidayContainer.children.length;
			rowNum = holidayContainer.children[holidayContainer.children.length - 1].getAttribute('data-rowNum');

		if (!scrollSearch && documetScroll + document.documentElement.clientHeight > document.documentElement.scrollHeight - 50 && holidayCount > existHolidayCount) {
			scrollSearch = true;
			getDataForSearch(getDataForAjax('/search' + location['search']), timestamp, rowNum);
		}
	}

	if (userContainer) {
		let userCount = userContainer.getAttribute('data-usercount'),
			timestamp = userContainer.getAttribute('data-timestamp'),
			existUserCount = userContainer.children.length;
			rowNum = userContainer.children[userContainer.children.length - 1].getAttribute('data-rowNum');

		if (!scrollSearch && documetScroll + document.documentElement.clientHeight > document.documentElement.scrollHeight - 50 && userCount > existUserCount) {
			scrollSearch = true;
			getDataForSearch(getDataForAjax('/search' + location['search']), timestamp, rowNum);
		}
	}
});

if (writeMessageField) {
	writeMessageField.addEventListener('keyup', function() {
		cursorPositionInMessageField = this.selectionStart;
	});

	writeMessageField.addEventListener('click', function() {
		cursorPositionInMessageField = this.selectionStart;
	});
}