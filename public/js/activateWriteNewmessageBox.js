/*jshint esversion: 6 */

let intetvalPreloader,
	count = 0,
	writeMessageBoxModal = document.querySelector('.write_message_box_modal'),
	fileMessageBlock = document.querySelector('.available_file_message_block'),
	countOfFilesBlock = document.querySelector('.count_of_files_block'),
	selectedFileList = [];
	selectedFileIdList = [];

var timeOutVideoMediaControl,
	activeTimeout = false;

function activateWriteNewMessageBox(from, to, toFullName) {
	fileMessageBlock.classList.remove('available_file_message_block_active');

	if (countOfFilesBlock) {
		countOfFilesBlock.remove();
	}

	for (let el of fileMessageBlock.children) {
		el.innerHTML = '';
	}

	let modal = document.querySelector('.modal'),
		modalBox = document.querySelector('.modal_box'),
		closePic = writeMessageBoxModal.querySelector('.close_win_modal'),
		userLink = writeMessageBoxModal.querySelector('.message_to_user_link'),
		elFullName = writeMessageBoxModal.querySelector('.message_to_user_fullname'),
		writeMessageField = writeMessageBoxModal.querySelector('.write_message_block>textarea'),
		buttonBox = document.querySelector('.send_message_button_box'),
		sendMessageButton = writeMessageBoxModal.querySelector('.send_message_to_user_button'),
		// emjBlock = document.querySelector('.emoji_block'),
		emjBox = document.querySelector('.emoji_box'),
		// emjElList = emjBox.querySelectorAll('li'),
		waitingBox = document.querySelector('.waiting_icon'),
		inputUploadFile = buttonBox.querySelector('input[type=file]'),
		iconButtonList = writeMessageBoxModal.querySelectorAll('.add_file_icon');

	modal.classList.add('modal_active');
	modalBox.classList.add('modal_box_active');
	userLink.setAttribute('href', '/profile/' + to);
	elFullName.innerHTML = toFullName;
	writeMessageField.focus();
	
	writeMessageField.addEventListener('keyup', function() {
		let availableFileEl = fileMessageBlock.querySelector('.file_container_for_message');
		if (this.value) {
			sendMessageButton.classList.remove('not_active_element');
		} else if (!this.value && !availableFileEl && !sendMessageButton.classList.contains('not_active_element')) {
			sendMessageButton.classList.add('not_active_element');
		}
	});

	for (let item of iconButtonList) {
		item.onclick = function() {
			setPausedForActiveMediaElem();
			let itemBlock = document.querySelector('.' + this.getAttribute('data-block_modal')),
				availableFileMessageBlock = document.querySelector('.' + this.getAttribute('data-message_selected_file')),
				fileType = Number(item.getAttribute('data-file_type'));

			for (let el of availableFileMessageBlock.children) {
				selectedFileIdList.push(el.getAttribute('data-file_id'));
			}

			preloaderAction('add');
			switchModalBlock(itemBlock, fileType);
		}
	}
	
	sendMessageButton.onclick = function() {
		if (!writeMessageField.value && !isEmptyFileContainer()) {
			return false;
		}
	
		let fileList = getFileIdList(),
			data;

		activateIconWaiting(buttonBox, waitingBox);

		tab.checkToken(function() {
			tab.xjscsrf(function() {
				data = 'user_to=' + to + '&text=' + writeMessageField.value;
				data += '&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
				data += '&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60);

				if (fileList) {
					for (let i = 0; i < fileList.length; i++) {
						data += '&fileList[]=' + fileList[i];
					}
				}

				fetch('/message/sendDialogMessage', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
				.then(response => response.json())
				.then(response => {
					document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
					.then(response => {
						if (response.res) {
							let messageListDataForLS = {
								'ws_tab_sync_to_do': 'update_dialog_message_list_item',
								'dialogId': response.dialogId,
								'user_to': response.user_to,
								'user_from': response.user_from,
							};
							tab.sendToLS(messageListDataForLS);
							
							let messageDetailsDataForLS = {
								'ws_tab_sync_to_do': 'update_dialog_message_details_item',
								'message_id': response.message_id,
								'user_to': response.user_to,
								'user_from': response.user_from,
							};
							tab.sendToLS(messageDetailsDataForLS);

							let waitingSuccesfullySendMessageBlock = document.querySelector('.waiting_succesfully_send_message'),
								waitingIconlist = waitingSuccesfullySendMessageBlock.querySelectorAll('.waiting_icon_span'),
								buttonBox = document.querySelector('.send_message_button_box'),
								sendMessageButton = document.querySelector('.send_message_to_user_button'),
								modal = document.querySelector('.modal'),
								modalBox = document.querySelector('.modal_box'),
								writeMessageField = modalBox.querySelector('.write_message_block>textarea'),
								succesfullyReport = document.querySelector('.succesfully_report');

							for (let node of waitingIconlist) {
								node.classList.add('not_active');
							}
							
							succesfullyReport.classList.remove('not_active');
							
							setTimeout(function() {
								modalBox.classList.remove('modal_box_active');
								writeMessageField.value = '';
								
								setTimeout(function() {
									modal.classList.remove('modal_active');
								}, 300);
								
								for (let node of waitingIconlist) {
									node.classList.remove('not_active');
								}
								
								succesfullyReport.classList.add('not_active');
								sendMessageButton.classList.add('not_active_element');
								disActivateIconWaiting(buttonBox, waitingSuccesfullySendMessageBlock)
							}, 750);
						} else {
							console.log(response.errors);
						}
					})
					.catch((error) => {
						console.log(error);
				});
			});
		});
	};
	
	closePic.onclick = function() {
		sendMessageButton.classList.add('not_active_element');
		modalBox.classList.remove('modal_box_active');
		writeMessageField.value = '';
		fileMessageBlock.classList.remove('available_file_message_block_active');
		selectedFileList.length = 0;
		selectedFileIdList.length = 0;
		cursorPositionInMessageField = 0;

		for (let child of fileMessageBlock.children) {
			child.innerHTML = '';
		}
		
		setTimeout(function() {
			modal.classList.remove('modal_active');
		}, 300);

		getCountOfFiles();
	};
}

function switchModalBlock(itemBlock, fileType) {
	let closeItemEl = itemBlock.querySelector('.close_win_modal'),
		containerFile = itemBlock.querySelector('.file_container'),
		acceptButton = itemBlock.querySelector('.select_file_accept_button'),
		loadFileInput = itemBlock.querySelector('.load_file');

	writeMessageBoxModal.classList.add('not_active_opacity');

	ajaxGetUserPublicFiles(fileType, function(imageList) {
		for (let i = 0; i < imageList.length; i++) {
			for (let j = 0; j < selectedFileIdList.length; j++) {
				if (imageList[i]['file_id'] == selectedFileIdList[j]) {
					imageList[i]['selected'] = true;
				}
			}
		}

		selectedFileIdList.length = 0;
		selectedFileList.length = 0;

		showFileList(imageList, containerFile, fileType);
		
		writeMessageBoxModal.ontransitionend = function() {
			if (this.classList.contains('not_active_opacity')) {
				this.classList.add('modal_block_not_active');
				itemBlock.classList.remove('modal_block_not_active');
				
				itemBlock.ontransitionend = function() {
					preloaderAction('remove')
					if (!itemBlock.classList.contains('modal_block_not_active')) {
						itemBlock.classList.remove('not_active_opacity');
					}
				}
			}
		}

		closeItemEl.onclick = function() {
			closeSubModalWin(itemBlock, containerFile, function() {
				acceptButton.classList.add('not_active_element');
			});
		}

		loadFileInput.onchange = function() {
			let checkResult = checkUploadFile(this, fileType);

			if (checkResult['error']) {
				let noticeElem = itemBlock.querySelector('.notice_elem_modal_message');
				noticeElem.innerHTML = checkResult['error'];
				noticeElem.classList.add('notice_elem_modal_message_active');
				this.value = '';

				itemBlock.onclick = function() {
					if (noticeElem.classList.contains('notice_elem_modal_message_active')) {
						noticeElem.classList.remove('notice_elem_modal_message_active');
						noticeElem.innerHTML = '';
					}
				}
			} else {
				ajaxLoadFile(loadFileInput, fileType, function(data) {
					showFileList(data, containerFile, fileType);
				});
			}
		}
	})
}

function closeSubModalWin(itemBlock, containerFile, callback) {
	itemBlock.classList.add('not_active_opacity');
	preloaderAction('add');
	containerFile.innerHTML = '';

	itemBlock.ontransitionend = function() {
		if (this.classList.contains('not_active_opacity')) {
			this.classList.add('modal_block_not_active');
			writeMessageBoxModal.classList.remove('modal_block_not_active');
			
			writeMessageBoxModal.ontransitionend = function() {
				callback();
				preloaderAction('remove');
				toggleSendMessageButton();
				if (!writeMessageBoxModal.classList.contains('modal_block_not_active')) {
					writeMessageBoxModal.classList.remove('not_active_opacity');
				}
			}
		}
	}
}

function showFileList(imageList, parentEl, fileType) {
	if (imageList.length <= 0 && parentEl.children.length <= 0) {
		let noFileEl = document.createElement('h3');
		noFileEl.classList.add('no_file_el_h3');

		if (fileType === 1) {
			noFileEl.innerHTML = 'У вас нет доступных изображений';
		} else if (fileType === 2) {
			noFileEl.innerHTML = 'У вас нет доступных аудио файлов';
		} else if (fileType === 3) {
			noFileEl.innerHTML = 'У вас нет доступных видео файлов';
		} else if (fileType === 4) {
			noFileEl.innerHTML = 'У вас нет доступных файлов';
		}

		parentEl.append(noFileEl);
		return;
	} else {
		existsnoFileEl = document.querySelector('.no_file_el_h3');

		if (existsnoFileEl) {
			existsnoFileEl.remove();
		}
	}

	for (let i = 0; i < imageList.length; i++) {
		let notLast = true;

		if (Number(fileType) === 1) {
			new ImageFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
				}
			);
		} else if (Number(fileType) === 2) {
			new AudioFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
					'name': imageList[i]['name'],
				}
			);
		} else if (Number(fileType) === 3) {
			new VideoFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
					'name': imageList[i]['name'],
				}
			);
		} else if (Number(fileType) === 4) {
			new DocumentFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
					'name': imageList[i]['name'],
					'date': imageList[i]['date'],
				}
			);
		}
	}

	selectFile(parentEl, fileType, true);
}

// function getSelectedElement() {
// 	let img = document.createElement('img');
// 	img.classList.add('select_file_icon');
// 	img.src = '/img/selected_icon.png';

// 	return img;
// }

// function selectFile(parentEl, fileType) {
// 	let selectIconList = document.querySelectorAll('.select_icon_message'),
// 		acceptButton = parentEl.parentElement.querySelector('.select_file_accept_button');

// 	toggleAcceptButton(parentEl, acceptButton);

// 	for (let item of selectIconList) {
// 		item.onclick = function() {
// 			if (item.classList.contains('selected_file')) {
// 				item.classList.remove('selected_file');
// 				item.src = '/img/not_selected_icon.png';
// 				removeFromSelectedFileList(item);
// 			} else {
// 				item.classList.add('selected_file');
// 				item.src = '/img/selected_icon.png';
// 				addElToSelectedFileList(item);
// 			}

// 			toggleAcceptButton(parentEl, acceptButton);
// 		}
// 	}

// 	acceptButton.onclick = function() {
// 		closeSubModalWin(parentEl.parentElement.parentElement, parentEl, function() {
// 			if (selectedFileList.length < 1 && acceptButton.classList.contains('not_active_element')) {
// 				return;
// 			}

// 			acceptButton.classList.add('not_active_element');

// 			let container = fileMessageBlock.querySelector('.' + acceptButton.getAttribute('data-insert_block'));
// 			container.parentElement.classList.add('available_file_message_block_active');
// 			container.innerHTML = '';

// 			for (let i = 0; i < selectedFileList.length; i++) {
// 				let controlBlock = selectedFileList[i].querySelector('.select_file_control_block'),
// 					closeEl = document.createElement('span'),
// 					closePic = document.createElement('img'),
// 					divCollection = selectedFileList[i].querySelectorAll('div');

// 				controlBlock.innerHTML = '';
// 				closeEl.classList.add('remove_file_block');
// 				closePic.src = '/img/close_.png';
// 				closePic.classList.add('remove_file_pic');

// 				for (let el of divCollection) {
// 					if (el.hasAttribute('data-css_eskiz-class')) {
// 						el.classList.add(el.getAttribute('data-css_eskiz-class'));
// 					}
// 				}

// 				if (selectedFileList[i].classList.contains('video_file_message_container')) {
// 					clearTimeout(timeOutVideoMediaControl);
// 					let videoControl = selectedFileList[i].querySelector('.video_control_media_block'),
// 						videoBlock = selectedFileList[i].querySelector('.select_video_message_block');

// 					videoControl.classList.remove('not_active');
// 					videoBlock.classList.remove('cursor_none');
// 				}

// 				closeEl.onclick = function() {
// 					this.closest('.file_container_for_message').remove();
// 					toggleSendMessageButton();
// 					let availableItem = container.parentElement.querySelector('.file_container_for_message');

// 					if (!availableItem) {
// 						container.parentElement.classList.remove('available_file_message_block_active');
// 					}

// 					if (container.children.length < 1) {
// 						container.classList.remove('box_bottom_border');
// 						removeBorderBottomForChildrenEl(container.parentElement);
// 					}
// 				}

// 				if (selectedFileList[i].classList.contains('box_bottom_border') && i == selectedFileList.length - 1) {
// 					selectedFileList[i].classList.remove('box_bottom_border')
// 				}

// 				closeEl.append(closePic);
// 				controlBlock.append(closeEl);
// 				container.append(selectedFileList[i]);
// 			}

// 			setBorderBottomForChildrenEl(container.parentElement)
// 			selectedFileList.length = 0;
// 		});
// 	}
// }

// function setBorderBottomForChildrenEl(parentEl) {
// 	let elList = parentEl.children;

// 	for (let i = 0; i < elList.length; i++) {
// 		if (elList[i].children.length > 0 && !elList[i].classList.contains('box_bottom_border') && i < elList.length - 1) {
// 			for (let j = i + 1; j < elList.length; j++) {
// 				if (elList[j].children.length > 0 && !elList[i].classList.contains('box_bottom_border')) {
// 					elList[i].classList.add('box_bottom_border')
// 				}
// 			}
// 		}
// 	}
// }

// function removeBorderBottomForChildrenEl(parentEl) {
// 	let elList = parentEl.children,
// 		hasChildList = [];

// 	for (let el of elList) {
// 		if (el.children.length > 0) {
// 			hasChildList.push(el);
// 		}
// 	}

// 	if (hasChildList.length < 2) {
// 		for (let el of elList) {
// 			el.classList.remove('box_bottom_border')
// 		}
// 	}
// }

// function toggleAcceptButton(parentEl, acceptButton) {
// 	let selectedIcon = parentEl.querySelector('.selected_file');

// 	if (selectedIcon) {
// 		acceptButton.classList.remove('not_active_element');
// 	} else {
// 		acceptButton.classList.add('not_active_element');
// 	}
// }

// function toggleSendMessageButton() {
// 	let sendMessageButton = document.querySelector('.send_message_to_user_button'),
// 		writeMessageField = writeMessageBoxModal.querySelector('.write_message_block>textarea'),
// 		availableFileEl = fileMessageBlock.querySelector('.file_container_for_message');

// 	if (availableFileEl) {
// 		sendMessageButton.classList.remove('not_active_element');
// 	} else if (!availableFileEl && !writeMessageField.value) {
// 		sendMessageButton.classList.add('not_active_element');
// 	}
// }

// function addElToSelectedFileList(item) {
// 	let container = item.closest('.file_container_for_message');

// 	selectedFileList.push(container);
// }

// function removeFromSelectedFileList(item) {
// 	let container = item.closest('.file_container_for_message'),
// 		fileId = container.getAttribute('data-file_id');

// 	for (let i = 0; i < selectedFileList.length; i++) {
// 		let elFileId = selectedFileList[i].getAttribute('data-file_id');

// 		if (elFileId === fileId) {
// 			selectedFileList.splice(i, 1);
// 		}
// 	}
// }