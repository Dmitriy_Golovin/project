function appendInputCR(form) {
	let inpHidden = document.createElement('input'),
		metaCSRF = document.querySelector('meta[name=X-CSRF-TOKEN]');
		
	inpHidden.setAttribute('type', 'hidden');
	inpHidden.setAttribute('name', 'csrf_test_name');
	inpHidden.value = metaCSRF.getAttribute('content');
	form.appendChild(inpHidden);
}