/*jshint esversion: 6 */

function activateInviteModal(profileId) {
	let modal = document.querySelector('.modal_invite'),
		modalBox = modal.querySelector('.modal_invite_box'),
		modalMainBox = modal.querySelector('.modal_invite_event_meeting_box'),
		closeModalButton = modal.querySelector('.close_win_modal'),
		chooseEntity = modal.querySelector('.invite_choose_entity'),
		entityBox = modal.querySelector('.invite_entity_box'),
		entityBoxTitle = entityBox.querySelector('.invite_entity_title'),
		entityBoxContent = entityBox.querySelector('.invite_entity_content');

	modal.classList.add('modal_active');
	modalBox.classList.add('modal_box_active');

	for (let el of chooseEntity.children) {
		el.onclick = function() {
			let type = this.dataset.main_type,
				dataObj = {
					'profileId': profileId,
					'type': type,
					'chooseEntity': chooseEntity,
					'modalMainBox': modalMainBox,
					'entityBox': entityBox,
					'entityBoxTitle': entityBoxTitle,
					'entityBoxContent': entityBoxContent,
				};
			
			fetchGetInviteEntityList(dataObj);
		};
	}

	closeModalButton.onclick = function() {
		modalBox.classList.remove('modal_box_active');

		setTimeout(function() {
			modal.classList.remove('modal_active');
			chooseEntity.classList.remove('not_active');
			chooseEntity.classList.remove('not_active');
			modalMainBox.classList.remove('p_10');
			entityBox.classList.remove('modal_box_active');
			entityBoxTitle.classList.remove('h_30');
			entityBoxTitle.innerHTML = '';
			entityBoxContent.innerHTML = '';
		}, 300);
	};
}

function fetchGetInviteEntityList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&timeOffsetSeconds=' + timeOffsetSeconds +
					'&profile_id=' + dataObj.profileId;


				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/commonEventList/getInviteEntityList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertInviteEntityList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertInviteEntityList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			dataObj.entityBoxContent.innerHTML = '';
			dataObj.chooseEntity.classList.add('not_active');
			dataObj.modalMainBox.classList.add('p_10');
			dataObj.entityBox.classList.add('modal_box_active');
			dataObj.entityBoxTitle.classList.add('h_30');
			dataObj.entityBoxTitle.insertAdjacentHTML('beforeend', data.htmlTitle);
			dataObj.entityBoxContent.insertAdjacentHTML('beforeend', data.htmlContent);
			dataObj.entityBoxContent.scrollTop = 0;

			addEventForInvite(dataObj.entityBoxContent, dataObj.profileId);
			loadInviteEntitiesByScroll(dataObj.entityBoxContent, dataObj);
		}

		if (data.insert_type === 'add') {
			dataObj.entityBoxContent.firstElementChild.insertAdjacentHTML('beforeend', data.htmlContent);
		}

		setTimeout(function() {
			scrollInviteEntityList = true;
		}, 200);
	} else {
		console.log(data.errors);
	}
}

function loadInviteEntitiesByScroll(entityBoxContent, dataObj) {
	let container = entityBoxContent.querySelector('.invite_entity_query_search');

	entityBoxContent.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			inviteEntityCount = container.getAttribute('data-entitycount'),
			timestamp = container.getAttribute('data-timestamp'),
			existInviteEntityCount = container.children.length,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		if (scrollInviteEntityList &&
			containerRect.bottom <= window.screen.height - 20 &&
			inviteEntityCount > existInviteEntityCount) {
			scrollInviteEntityList = false;
			fetchGetInviteEntityList(dataObj, timestamp, rowNum);
		}
	};
}

function addEventForInvite(parentElement, profileId)
{
	parentElement.onclick = function(e) {
		eventMeetingInviteUser(e, profileId);
	};
}

function eventMeetingInviteUser(e, profileId) {
	if (e.target.classList.contains('invite_entity_button')) {
		let container = e.target.closest('.invite_entity_result_item'),
			type = container.dataset.type,
			entityId = container.dataset.entity_id;
		
		tab.checkToken(function() {
			tab.xjscsrf(function() {
				let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
					timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
					data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
						'&type=' + type +
						'&timeOffsetSeconds=' + timeOffsetSeconds +
						'&profile_id=' + profileId +
						'&entity_id=' + entityId;

					fetch('/commonEventList/entityInviteUser', {
						method: 'POST',
						body: data,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
						.then(response => response.json())
						.then(response => {
							csrfVal.setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
			  				if (response.res) {
				  				let buttonBox = e.target.closest('.invite_entity_result_item[data-entity_id="' + response.entityId + '"]')
				  					.querySelector('.invite_entity_button_box');

				  				buttonBox.innerHTML = response.html;
				  			} else {
								console.log(response.errors);
							}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			});
		});
	}
}