var closeNewWinElement = document.querySelector('.close_win_add_new_event'),
	targetPic = document.querySelector('.close_pic'),
	containerForElement = document.querySelector('.add_new_win_container'),
	elemInsideWin = document.querySelectorAll('.inside_win'),
	winForNewEvent = document.querySelector('.add_new_win'),
	userLoginField = document.querySelector('input[name=email]'),
	userPasswordField = document.querySelector('input[name=password]'),
	formFieldsetBox = document.querySelector('.fieldset_box'),
	formLogin = document.querySelector('form[name=login_form]'),
	buttonLogin = document.querySelector('.button_login_start_page')
	userMenuList = document.querySelectorAll('.menu_user_item'),
	iconBurgerMenu = document.querySelector('.burger_menu')
	elemOnTheRight = document.querySelector('.autorized_or_personal_block'),
	burgerMenuHeader = document.querySelector('.burger_menu_header_box'),
	mainMenu = document.querySelector('.menu_user_main'),
	burgerMenu = document.querySelector('.menu_user_burger'),
	searchSubmitutton = document.querySelector('.search_submit'),
	searchField = document.querySelector('.search_field'),
	calendar = new Calendar(),
	winWidth = document.documentElement.offsetWidth,

	url = 'localhost:8080',
	userID = false;
	
let tab = new BroadcastLS(url, userID);
console.log(tab);

window.addEventListener('load', function() {
	setDataAttributePositionForUsermenuItem(userMenuList);
	showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
	setTableDimension();
	setNavHeaderContainerDimensions();
	setGreetingBoxDimention();
	calendar.show(0);
    createWindowForNewEvent({parent: 'table', header: 'header', child: '.inside-cell', startPage: true});
});

window.addEventListener('resize', function() {
	if (winWidth !== document.documentElement.offsetWidth) {
		winWidth = document.documentElement.offsetWidth;
		showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
		//hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e, burgerMenu);
	}
	
	setTableDimension();
	setNavHeaderContainerDimensions();
	setGreetingBoxDimention();
	if (userLoginField) {
		removeNotice(formFieldsetBox, userLoginField, userPasswordField);
	};
});

if (buttonLogin) {
	buttonLogin.addEventListener('click', function() {
		if (!userLoginField.value || !userPasswordField.value) {
			checkFillingFormLogin(formFieldsetBox, userLoginField, userPasswordField);
			return false;
		} else if (userLoginField.value || userPasswordField.value) {
			//checkLoginPasswordAjax(userLoginField, userPasswordField);
			appendInputCR(formLogin);
			formLogin.submit();
		}
	});
	
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (userLoginField == document.activeElement || userPasswordField == document.activeElement) {
				if (!userLoginField.value || !userPasswordField.value) {
					checkFillingFormLogin(formFieldsetBox, userLoginField, userPasswordField);
					return false;
				} else if (userLoginField.value || userPasswordField.value) {
					//checkLoginPasswordAjax(userLoginField, userPasswordField);
					appendInputCR(formLogin);
					formLogin.submit();
				}
			}
		}
	})
	
};

if (userLoginField) {
	userLoginField.addEventListener('focus', function() {
		removeNotice(formFieldsetBox, userLoginField, userPasswordField);
	});

	userPasswordField.addEventListener('focus', function() {
		removeNotice(formFieldsetBox, userLoginField, userPasswordField);
	});
}

if (closeNewWinElement) {
	closeNewWinElement.addEventListener('click', function(e) {
			
		if (e.target === closeNewWinElement || e.target === targetPic) {
			closeWinForNewEvent(containerForElement);
			if (userLoginField) {
				removeNotice(formFieldsetBox, userLoginField, userPasswordField);
			};
		};
	});
}

iconBurgerMenu.addEventListener('click', function() {
	showBurgerMenu(burgerMenuHeader, iconBurgerMenu, burgerMenu)
});

document.body.addEventListener('click', function(e) {

	for (var i = 0; i < elemInsideWin.length; i++) {
		if (e.target === elemInsideWin[i]) {
			return;
		};
	};
	
	if (winForNewEvent.classList.contains('active')) {
		closeWinForNewEvent(containerForElement);
		if (userLoginField) {
			removeNotice(formFieldsetBox, userLoginField, userPasswordField);
		};
	};
	
	hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e, burgerMenu);
});

if (searchSubmitutton) {
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (searchField == document.activeElement) {
				if (searchField.value !== '') {
					var url = encodeURI('/search?q=' + searchField.value);
					location.href = url;
				}
				searchField.value = '';
			}
		}
	})
	
	searchSubmitutton.addEventListener('click', function() {
		if (searchField.value !== '') {
			var url = encodeURI('/search?q=' + searchField.value);
			location.href = url;
		}
		searchField.value = '';
	});
}