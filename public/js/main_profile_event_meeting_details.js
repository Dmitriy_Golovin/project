/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	profileId = mainBox.dataset.profile_id,
	entityId = mainBox.dataset.entity_id,
	type = mainBox.dataset.type,
	profileEventContentBlock = mainBox.querySelector('.event_content_block'),
	scrollProfileEventParticipantsList = true;

window.addEventListener('load', function() {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + type +
					'&entity_id=' + entityId +
					'&timeOffsetSeconds=' + timeOffsetSeconds +
					'&profile_id=' + profileId;

				fetch('/profile/getMainEventDetails', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				profileEventContentBlock.insertAdjacentHTML('afterbegin', response.html);
		  			})
		  			.then(response => {
		  				let dataObj = {
		  					'profileId': profileId,
		  					'entityId': entityId,
		  					'type': type,
		  					'contentBlock': profileEventContentBlock.querySelector('.profile_entity_guest_content'),
		  					'scrollEl': document.body,
		  				};

		  				fetchGetParticipantsProfileMainEventList(dataObj);
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
});