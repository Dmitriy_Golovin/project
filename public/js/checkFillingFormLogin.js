function checkFillingFormLogin(fieldsetBox, loginField, passwordField) {
	var noticeElem = document.querySelector('.notice_elem_start_page') || document.querySelector('.notice_elem');
	
	if (noticeElem.offsetWidth < 175) {
		noticeElem.classList.add('notice_elem_large_active1');
	}
	
	if (!loginField.value && !passwordField.value) {
		loginField.classList.add('error_input');
		passwordField.classList.add('error_input');
		noticeElem.innerHTML = 'Введите email и пароль';
		noticeElem.classList.add('notice_elem_active');
	} else if (!loginField.value) {
		loginField.classList.add('error_input');
		noticeElem.innerHTML = 'Введите email';
		noticeElem.classList.add('notice_elem_active');
	} else if (!passwordField.value) {
		noticeElem.innerHTML = 'Введите пароль';
		passwordField.classList.add('error_input');
		noticeElem.classList.add('notice_elem_active');
	}
	
}

function removeNotice(fieldsetBox, loginField, passwordField) {
	var noticeElem = document.querySelector('.notice_elem') || document.querySelector('.notice_elem_start_page');
	
	loginField.classList.remove('error_input');
	passwordField.classList.remove('error_input');

	noticeElem.innerHTML = '';
	noticeElem.classList.remove('notice_elem_active');
	noticeElem.classList.remove('notice_elem_large_active1');
}

function removeNoticePassRecovery(loginField) {
	var noticeElem = document.querySelector('.notice_elem');
	
	loginField.classList.remove('error_input');

	noticeElem.innerHTML = '';
	noticeElem.classList.remove('notice_elem_active');
	noticeElem.classList.remove('notice_elem_large_active1');
}