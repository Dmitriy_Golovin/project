/*jshint esversion: 6 */

function ajaxGetContentInCell(dateList, dateOfFirstEl, dateOfLastEl, tdList, yearList, monthList) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			year = document.querySelector('#show-date').innerText.slice(-4),
			data = 'timeOffsetSeconds=' + timeOffsetSeconds + '&' + 'dateList=' + dateList + '&' + 'dateOfFirstEl=' + dateOfFirstEl + '&' + 'dateOfLastEl=' + dateOfLastEl + '&' + 'year=' + year + '&' + 'yearList=' + yearList + '&' + 'monthList=' + monthList  + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);

				if (result.res == 1) {
					csrfVal.setAttribute('content', result.csrf);
					tab.sendCSRFToLS();

					for (let el of tdList) {
						el.removeAttribute('data-event_id_list');
						el.removeAttribute('data-meeting_id_list');
						el.removeAttribute('data-task_id_list');
					}

					showCountHoliday(tdList, result.data.holidayList);
					showCountAnyEvent(tdList, result.data.selfEventList, 'event');
					showCountAnyEvent(tdList, result.data.meetingList, 'meeting');
					showCountAnyEvent(tdList, result.data.taskList, 'task');
					changeMonth = true;
				} else if (result.res == 2) {
					console.log(result.errors);
				}
			}
		};

		xhr.open('POST', 'calendar/getContentInCell');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function showCountHoliday(tdList, holidayListObj) {
	var spanCountHolidayList = document.querySelectorAll('.count_holiday');
	
	for (var i = 0; i < tdList.length; i++) {
		var spanHoliday = tdList[i].querySelector('.holiday_cell');
		spanHoliday.classList.add('not_active');
		spanHoliday.classList.remove('count_holiday');
		spanHoliday.innerHTML = '';
		for (var key in holidayListObj) {
			
			if (tdList[i].getAttribute('data-holiday_date_for_cell') == key || tdList[i].getAttribute('data-var_holiday_date_for_cell') == key || tdList[i].getAttribute('data-last_holiday_date_for_cell') == key || tdList[i].getAttribute('data-holiday_date_for_cell') + '-' + tdList[i].getAttribute('data-date_for_cell').substr(tdList[i].getAttribute('data-date_for_cell').lastIndexOf('.') + 1) == key) {
				var simple = holidayListObj[tdList[i].getAttribute('data-holiday_date_for_cell')] || 0,
					variable = holidayListObj[tdList[i].getAttribute('data-var_holiday_date_for_cell')] || 0,
					last = holidayListObj[tdList[i].getAttribute('data-last_holiday_date_for_cell')] || 0,
					holidayCount = simple + variable + last + (holidayListObj[tdList[i].getAttribute('data-holiday_date_for_cell') + '-' + tdList[i].getAttribute('data-date_for_cell').substr(tdList[i].getAttribute('data-date_for_cell').lastIndexOf('.') + 1)] || 0);
				if (holidayCount) {
					spanHoliday.classList.add('count_holiday');
					spanHoliday.classList.remove('not_active');
					spanHoliday.innerHTML = holidayCount;
				}
			}
			
		}
	}
}

function showCountAnyEvent(tdList, selfEventList, partOfClass) {
	for (let el of tdList) {
		let spanEl = el.querySelector('.' + partOfClass + '_cell');

		if (spanEl) {
			innerCount = (spanEl.innerHTML) ? Number(spanEl.innerHTML) : 0;
			spanEl.classList.add('not_active');
			spanEl.classList.remove('count_' + partOfClass);
			spanEl.innerHTML = '';
			for (let key in selfEventList) {
				showCountAnyEventIncell(el, key, selfEventList[key], selfEventList[key].length, innerCount, partOfClass, false);
			}
		}
	}
}

function showCountAnyEventIncell(el, date, idList, count, innerCount, partOfClass, add) {

	let spanEl = el.querySelector('.' + partOfClass + '_cell');
	
	if (el.getAttribute('data-date_for_cell') == date) {
		spanEl.classList.remove('not_active');
		spanEl.classList.add('count_' + partOfClass);
		spanEl.innerHTML = (add) ? innerCount + Number(count) : count;

		if (partOfClass === 'event') {
			setEventAttributeIdList(el, 'data-event_id_list', idList);
		}

		if (partOfClass === 'meeting') {
			setEventAttributeIdList(el, 'data-meeting_id_list', idList);
		}

		if (partOfClass === 'task') {
			setEventAttributeIdList(el, 'data-task_id_list', idList);
		}
	}
}

function setEventAttributeIdList(el, attribute, idList) {
	el.setAttribute(attribute, idList);
}