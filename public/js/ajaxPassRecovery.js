function sendEmailUserForPassRecovery(email, formBox, reportBox) {
	
	var xhr = new XMLHttpRequest(),
		noticeElem = document.querySelector('.notice_elem'),
		buttonPassRec = document.querySelector('.button_pass_recovery'),
		waitingBox = document.querySelector('.waiting_icon'),
		data = 'email=' + email.value + '&' + 'ajax=' + true;

	xhr.onreadystatechange = function() {
		activateIconWaiting(buttonPassRec, waitingBox);
		if (xhr.readyState == 4 && xhr.status == 200) {
			var result = JSON.parse(xhr.responseText);
			if (result['res'] == 0) {
				formBox.classList.add('not_active');
				reportBox.classList.remove('not_active');
			} else if (result['res'] == 1){
				disActivateIconWaiting(buttonPassRec, waitingBox);
				noticeElem.innerHTML = 'email не зарегистрирован';
				noticeElem.classList.add('notice_elem_active');
				email.classList.add('error_input');
				email.classList.remove('valid_input');
			}

		}
	};

	xhr.open('POST', '/ajax/getEmail');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(data);
}