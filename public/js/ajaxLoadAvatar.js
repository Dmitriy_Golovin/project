/*jshint esversion: 6 */

function ajaxLoadAvatar(avatarInputElem, callback) {
	tab.xjscsrf(function() {
		let data = new FormData(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			avatar = avatarInputElem.files[0],
			currentAva = document.querySelector('.current_ava_container>img');
		
		data.append('avatar', avatar);
		data.append('csrf_test_name', csrfVal.getAttribute('content'));

		let xhr = new XMLHttpRequest();
		xhr.onload = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				let result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();
				
				if (result.res == 1) {
					currentAva.src = result.data.path_to_ava;
					let dataForTabSync = {
						'ws_tab_sync_to_do': 'change_personal_data',
						'firstName': false,
						'lastName': false,
						'country': false,
						'city': false,
						'pathToAva': result.data.path_to_ava
					};

					tab.send(tab.tabSync.collectData(dataForTabSync));
					tab.tabSync.run(dataForTabSync);
					callback();
				} else {
					console.log(result.errors);
				}
			}
		};

		xhr.open('POST', '/user/loadAvatar');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}