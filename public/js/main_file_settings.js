/*jshint esversion: 6 */

const TYPE_IMAGE = 1;
const TYPE_VIDEO = 3;
const TYPE_DOCUMENT = 4;

const PUBLIC_STATUS = 1;
const PRIVATE_STATUS = 2;

let mainBox = document.querySelector('.main_box'),
	fileId = mainBox.dataset.file_id,
	type = mainBox.dataset.file_type,
	privacyStatus = mainBox.dataset.file_privacy_status,
	fileContainer = mainBox.querySelector('.file_settings_file_container'),
	timeOutVideoMediaControl,
   	activeTimeout = false,
   	fullScreen = false,
   	scrollUserList = true,
   	scrollPermissionUserList,
   	modalAddGuestUserBox,
   	userModal,
   	userModalBox,
   	invitedContentBlock;

if (Number(privacyStatus) === PRIVATE_STATUS) {
	modalAddGuestUserBox = document.querySelector('.modal_add_guest_user_box');
	userModal = modalAddGuestUserBox.closest('.modal');
	userModalBox = modalAddGuestUserBox.closest('.modal_box');
	invitedContentBlock = userModal.querySelector('.invited_content_block');
}

window.addEventListener('load', function() {
	if (Number(type) === TYPE_IMAGE) {
		let image = document.querySelector('.image_file_in_settings'),
			modalSliderImage = new ModalSliderImage({
				'modalCssClass': 'modal_slider_image',
				'availableImageListContainer': fileContainer,
			});

		image.addEventListener('click', function() {
			modalSliderImage.show(this);
		});
	}

	if (Number(type) === TYPE_VIDEO) {
		let videoContainer = mainBox.querySelector('.main_file_video_container');

		new VideoFileExistElement(videoContainer);
	}

	fetchGetFileSettingsData({'fileId': fileId});

	if (Number(privacyStatus) === PRIVATE_STATUS) {
		let userFileAccessContent = mainBox.querySelector('.user_file_access_content'),
			dataObj = {
				'fileId': fileId,
				'contentBlock': userFileAccessContent,
			};

		fetchGetFilePermissionUserList(dataObj);
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('add_user_file_access_but')) {
		fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
		userModal.classList.add('modal_active');
		userModalBox.classList.add('modal_box_active');
	}

	if ((e.target.classList.contains('close_pic_modal') || e.target.classList.contains('close_win_modal')) &&
		e.target.closest('.modal_add_guest_user_box')) {

		userModalBox.classList.remove('modal_box_active');

		setTimeout(function() {
			userModal.classList.remove('modal_active');
		}, 300);
	}

	if (e.target.classList.contains('invited_search_button') && e.target.closest('.modal_add_guest_user_box')) {
		fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
	}

	if (e.target.classList.contains('file_access_add_user_button')) {
		let userContainer = e.target.closest('.user_result_item'),
			userContainerParent = userContainer.parentElement,
			profileId = userContainer.dataset.profile_id;

		tab.checkToken(function() {
			tab.xjscsrf(function() {
				let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
					timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
					data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
						'&file_id=' + fileId + 
						'&profile_id=' + profileId;

					fetch('/file/addUserFilePermission', {
						method: 'POST',
						body: data,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
						.then(response => response.json())
						.then(response => {
							csrfVal.setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
			  				if (response.res) {
			  					let userFileAccessContent = mainBox.querySelector('.user_file_access_content'),
			  						userQuerySearch = userFileAccessContent.querySelector('.user_query_search');

			  					userQuerySearch.insertAdjacentHTML('afterbegin', response.html);
			  					userContainerParent.dataset.usercount = Number(userContainerParent.dataset.usercount) - 1;
			  					userContainer.remove();
			  				} else {
			  					console.log(response.errors);
			  				}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			});
		});
	}

	if (e.target.classList.contains('file_access_cancel_user_button')) {
		let userContainer = e.target.closest('.user_result_item'),
			userContainerParent = userContainer.parentElement,
			profileId = userContainer.dataset.profile_id,
			exsistDataRowNum = userContainer.hasAttribute('data-rowNum');

		tab.checkToken(function() {
			tab.xjscsrf(function() {
				let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
					timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
					data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
						'&file_id=' + fileId + 
						'&profile_id=' + profileId;

					fetch('/file/removeUserFilePermission', {
						method: 'POST',
						body: data,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
						.then(response => response.json())
						.then(response => {
							csrfVal.setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
			  				if (response.res) {
			  					if (exsistDataRowNum) {
			  						userContainerParent.dataset.usercount = Number(userContainerParent.dataset.usercount) - 1;
			  					}

			  					userContainer.remove();
			  				} else {
			  					console.log(response.errors);
			  				}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			});
		});
	}
});

function getDataForInviteUsers() {
	let invitedFilterBlock = modalAddGuestUserBox.querySelector('.invited_filter_block'),
		searchField = modalAddGuestUserBox.querySelector('.invited_search_field'),
		onlyFriends = invitedFilterBlock.querySelector('.invited_only_friends').checked,
		invitedSelectGroup = invitedFilterBlock.querySelector('select[name="invited_select_group"]'),
		result = {};

	if (searchField.value) {
		result.searchString = searchField.value;
	}

	result.only_friends = Number(onlyFriends);
	result.group_id = invitedSelectGroup.options[invitedSelectGroup.selectedIndex].getAttribute('data-group_id');
	result.type = 'file-permission-add';
	result.exceptionEntityId = fileId;
	result.exceptionEntity = 1;
	// result.within_group = 0;

	// if (inviteContainer.querySelector('.within_group') && !inviteContainer.querySelector('.within_group').classList.contains('not_active')) {
	// 	result.within_group = inviteContainer.querySelector('.within_group').value;
	// }

	return result;
}