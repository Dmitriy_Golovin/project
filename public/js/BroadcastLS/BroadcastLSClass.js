/*jshint esversion: 6 */

class BroadcastLS {
	
	constructor() {
		this.tabID = Date.now();
		this.myStorageLS = window.localStorage;
		this.status = false;
		this.userID = this._getUserID();
		this.ws = false;
		this.sockets = false;
		this._setStatusTab();
		this.sendOneTime = false;
		this.waitForServerResponse = false;
		this.tabSync = new TabSync(this.userID);
	}
	
	_setStatusTab() {
		let primary = this.myStorageLS.getItem('primary');
		if (!primary) {
			this._addPrimaryTabInStorage();
		} else if (primary) {
			this._addSecondaryTabInStorage();
		}

		document.addEventListener('visibilitychange', function() {
			if (document.visibilityState === 'visible' && this.userID) {
				this.checkToken(function() {
					if (this.status === 'primary') {
						if (this.ws.readyState !== 1) {
							this._connectToWS();
						}
					} else {
						this.myStorageLS.setItem('visibility_connectWS', JSON.stringify({'cookie': true}));
						this.myStorageLS.removeItem('visibility_connectWS');

					}	
				}.bind(this));
			}
		}.bind(this));
		
		window.addEventListener('storage', function(e) {		
			let primaryAvalaible = this.myStorageLS.getItem('primary');
			let secondaryTabList = JSON.parse(this.myStorageLS.getItem('secondary'));
			
			if (e.key === 'visibility_connectWS' && this.status === 'primary') {
				let data = JSON.parse(e.newValue);
				
				if (data) {
					if (this.ws.readyState !== 1) {
						this.checkToken(this._connectToWS.bind(this));
					}
				}
			}
			
			if (e.key === 'send_to_ws') {
				let sendToWS = JSON.parse(e.newValue);
				this._readSentDataFromStorage(sendToWS, e);
			}
			
			if (e.key === 'server_response') {
				let response = JSON.parse(e.newValue);
				if (response) {
					this._processingServerResponseAfterWaiting(response);
				}
			}
			
			if (e.key === 'tab_sync_self') {
				let syncData = JSON.parse(e.newValue);
				if (syncData) {
					this.tabSync.run(syncData.secondary_send);
				}
			}
			
			if (e.key === 'checkPrimary' && this.status === 'primary') {
				this.myStorageLS.removeItem('checkPrimary');
			}
			
			if (e.key === 'checkSecondary' && this.status === 'secondary') {
				let checkSecondaryTabListDel = JSON.parse(this.myStorageLS.getItem('checkSecondary'));
				this._deleteItemFromList(checkSecondaryTabListDel, this.tabID);
				if (checkSecondaryTabListDel != null) this.myStorageLS.setItem('checkSecondary', JSON.stringify(checkSecondaryTabListDel));
			}

			if (!primaryAvalaible) {
				if (secondaryTabList && secondaryTabList.length > 0) {
					if (secondaryTabList[0] !== this.tabID) {
						return
					} else {
						let secondaryTabListDel = JSON.parse(this.myStorageLS.getItem('secondary'));
						this.status = 'primary';
						this.myStorageLS.setItem('primary', this.tabID);
						this._deleteItemFromList(secondaryTabListDel, this.tabID);
						this.myStorageLS.setItem('secondary', JSON.stringify(secondaryTabListDel));
						if (this.userID) this.checkToken(this._connectToWS.bind(this));
					}
				}
			}
		}.bind(this))
	}
	
	_addPrimaryTabInStorage() {
		this.myStorageLS.setItem('primary', this.tabID);
		this.status = 'primary';
		if (this.userID) {
			this._connectToWS();
		}
		
		this._checkSecondary(function() {
			setTimeout(function() {
				let checkSecondaryTabList = JSON.parse(this.myStorageLS.getItem('checkSecondary')),
					secondaryTabListDel = JSON.parse(this.myStorageLS.getItem('secondary'));
				
				this._deleteItemFromList(checkSecondaryTabList, this.tabID);
				
				if (checkSecondaryTabList != null) {
					for (let i = 0; i < checkSecondaryTabList.length; i++) {
						this._deleteItemFromList(secondaryTabListDel, checkSecondaryTabList[i]);
					}
				}

				if (secondaryTabListDel && secondaryTabListDel.length > 0) {
					this.myStorageLS.setItem('secondary', JSON.stringify(secondaryTabListDel));
				} else {
					this.myStorageLS.removeItem('secondary');
				}
				
				this.myStorageLS.removeItem('checkSecondary');
			}.bind(this), 500)
		}.bind(this))
		
		window.addEventListener('beforeunload', function() {
			this.ws.close;
			this.myStorageLS.removeItem('primary');
		}.bind(this));
	}
	
	_addSecondaryTabInStorage() {
		
		let secondaryList = this.myStorageLS.getItem('secondary');
		if (!secondaryList) {
			let arr = [];
			arr.push(this.tabID);
			this.myStorageLS.setItem('secondary', JSON.stringify(arr));
			this.status = 'secondary';
		} else {
			let secondaryAvailableList = JSON.parse(secondaryList);
			secondaryAvailableList.push(this.tabID);
			this.myStorageLS.setItem('secondary', JSON.stringify(secondaryAvailableList));
			this.status = 'secondary';
		}

		this._checkPrimary(this.tabID, function() {
			setTimeout(function() {
				let checkPrimary = this.myStorageLS.getItem('checkPrimary');
				
				if (checkPrimary && this.tabID == checkPrimary) {
					let secondaryTabListDel = JSON.parse(this.myStorageLS.getItem('secondary'));
					this.status = 'primary';
					this.myStorageLS.setItem('primary', this.tabID);
					this._deleteItemFromList(secondaryTabListDel, this.tabID);
					this.myStorageLS.setItem('secondary', JSON.stringify(secondaryTabListDel));
					if (this.userID) this.checkToken(this._connectToWS.bind(this));
					this.myStorageLS.removeItem('checkPrimary');
					
					this._checkSecondary(function() {
						setTimeout(function() {
							let checkSecondaryTabList = JSON.parse(this.myStorageLS.getItem('checkSecondary')),
								secondaryTabListDel = JSON.parse(this.myStorageLS.getItem('secondary'));
							
							this._deleteItemFromList(checkSecondaryTabList, this.tabID);
							
							if (checkSecondaryTabList != null) {
								for (let i = 0; i < checkSecondaryTabList.length; i++) {
									this._deleteItemFromList(secondaryTabListDel, checkSecondaryTabList[i]);
								}
							}
							
							if (secondaryTabListDel && secondaryTabListDel.length > 0) {
								this.myStorageLS.setItem('secondary', JSON.stringify(secondaryTabListDel));
							} else {
								this.myStorageLS.removeItem('secondary');
							}
							
							this.myStorageLS.removeItem('checkSecondary');
						}.bind(this), 500)
					}.bind(this))
				}
			}.bind(this), 500);
		}.bind(this))
		
		window.addEventListener('beforeunload', function() {
			this.ws.close;
			if (this.tabID == this.myStorageLS.getItem('primary')) this.myStorageLS.removeItem('primary');
			let secondaryListForDel = JSON.parse(this.myStorageLS.getItem('secondary'));
			if (secondaryListForDel) {
				this._deleteItemFromList(secondaryListForDel, this.tabID)
				if (secondaryListForDel.length > 0) {
					this.myStorageLS.setItem('secondary', JSON.stringify(secondaryListForDel));
				} else {
					this.myStorageLS.removeItem('secondary');
				}
			}
		}.bind(this));
		
	}
	
	_checkPrimary(tabID, callback) {
		this.myStorageLS.setItem('checkPrimary', tabID);
		callback();
	}
	
	_checkSecondary(callback) {
		let secondaryList = this.myStorageLS.getItem('secondary');
		if (secondaryList && secondaryList.length > 0) this.myStorageLS.setItem('checkSecondary', secondaryList);
		callback();
	}

	_getCookie() {
		return document.cookie;
	}
	
	_connectToWS() {
		let timeOffsetSeconds = new Date().getTimezoneOffset() * (-60);
		this.ws = new WebSocket('wss://calendaric.fun:8080?timeOffsetSeconds=' + timeOffsetSeconds);
		
		this._readWS();
		
		this.ws.onopen = function() {
			this.sockets = true;
			// console.log('Соединение с сокет-сервером Установленно');
		}.bind(this);

		this.ws.onclose = function() {
			this.sockets = false;
		}.bind(this);	
	}
	
	_pingWS() {
		
		if (this.ws.readyState !== 1) {
			this.checkToken(this._connectToWS.bind(this));
		}
	}

	
	_deleteItemFromList(list, item) {
		if (list != null) {
			for (let i = 0; i < list.length; i++) {
				if (list[i] == item) list.splice(i, 1);
			}
		}
		
	}
	
	primarySendToWS(data) {
		if (data.send_one_time && !this.sendOneTime) {
			this.sendOneTime = true;
			delete data.send_one_time;
			let dataWS = JSON.stringify(data);
			this.ws.send(dataWS);
		} else if (!data.send_one_time && this.sendOneTime) {
			this.sendOneTime = false;
			let dataWS = JSON.stringify(data);
			this.ws.send(dataWS);
		} else if (!data.send_one_time && !this.sendOneTime) {
			let dataWS = JSON.stringify(data);
			this.ws.send(dataWS);
		}
	}

	sendToLS(data) {
		let dataForLS = {
			'secondary_send': data
		};

		this.myStorageLS.setItem('tab_sync_self', JSON.stringify(dataForLS));
		this.myStorageLS.removeItem('tab_sync_self');
	}
	
	send(data) {
		if (this.status == 'primary') {
			this.primarySendToWS(data);
			let dataForLS = {
				'primary_send': data,
				'tabID': this.tabID,
			};
			if (!data.actionDB) {
				this.myStorageLS.setItem('send_to_ws', JSON.stringify(dataForLS));
				this.myStorageLS.removeItem('send_to_ws');
			}
		} else {
			let dataForLS = {
				'secondary_send': data,
				'tabID': this.tabID,
			};

			this.myStorageLS.setItem('send_to_ws', JSON.stringify(dataForLS));
			this.myStorageLS.removeItem('send_to_ws');
		}
	}
	
	_addToLSWaitingServerResponse(tabID) {
		this.myStorageLS.setItem('wait_for_server_response', JSON.stringify(tabID));	
	}
	
	_readSentDataFromStorage(sendToWS, e) {
		if (sendToWS) {
			if (this.status == 'secondary' && sendToWS.primary_send) {
				this.tabSync.run(sendToWS.primary_send);
			} else if (sendToWS.secondary_send && this.tabID !== sendToWS.tabID && !sendToWS.secondary_send.actionDB) {
				this.tabSync.run(sendToWS.secondary_send);
			}
			
			if (this.status == 'primary' && sendToWS.secondary_send && this.tabID !== sendToWS.tabID) {
				this.primarySendToWS(sendToWS.secondary_send);
			}
		}
	}
	
	_addSendDataInstorage(key, data) {
		if (this.myStorageLS.getItem(key)) {
			let storageList = JSON.parse(this.myStorageLS.getItem(key));
			storageList.push(data);
			this.myStorageLS.setItem(key, JSON.stringify(storageList));
		} else {
			let storageListNew = [];
			storageListNew.push(data);
			this.myStorageLS.setItem(key, JSON.stringify(storageListNew));
		}
	}
	
	_readWS() {	
		this.ws.onmessage = function(e) {
			this.sendOneTime = false;
			let message = JSON.parse(e.data);

			if (message.ws_tab_sync_to_do && !message.resultDB) {
				this._playNotification(message.ws_tab_sync_to_do);
				this.tabSync.run(message);
				let dataForLS = {
					'primary_send': message,
					'tabID': this.tabID,
				};
				this.myStorageLS.setItem('send_to_ws', JSON.stringify(dataForLS));
				this.myStorageLS.removeItem('send_to_ws');
			} else if (message.resultDB) {
				if (!this.waitForServerResponse) {
					this._addToLSServerResponse(message);
				} else {
					this._processingServerResponseAfterWaiting(message);
				}
			} else if (message === 'upgradeToken') {
				this._upgradeToken(function(result) {
					if (result.res === 'good') {
						console.log('send_ws_upgrade');
						this.ws.send(JSON.stringify({'ws_tab_sync_to_do': 'upgrade_token', 'upgradeToken': this._getCookie()}));
					} else if (result.res === 'logout') {
						console.log(result);
						document.location.href = '/';
						//this.ws.close();
					}
				}.bind(this));
			} else if (message.action === 'logout' || message == 'logout') {
				console.log(message);
				//document.location.href = '/logout';
			}
		}.bind(this);
	}
	
	_addToLSServerResponse(response) {
		this.myStorageLS.setItem('server_response', JSON.stringify(response));
		
	}
	
	_playNotification(action) {
		let pathToNotification = {
			'subscribe': '/audio/notification/new_friend_request.wav',
			'send_message_to_user': '/audio/notification/new_friend_request.wav'
		}
		
		if (pathToNotification[action]) {
			let audio = new Audio(pathToNotification[action]);

			audio.play();
		}
	}
	
	_processingServerResponseAfterWaiting(response) {
		if (response && this.waitForServerResponse === response['resultDB']) {
			let action = '_run_' + response['resultDB'];
			this[action](response);
			this.waitForServerResponse = false;
			this.myStorageLS.removeItem('server_response');
			delete response['resultDB'];
			let dataForLS = {
				'secondary_send': response,
				'tabID': this.tabID,
			};
			this.myStorageLS.setItem('tab_sync_self', JSON.stringify(dataForLS));
			this.myStorageLS.removeItem('tab_sync_self');
		}
	
	}
	
	_run_message_succesfully_sent(data) {
		let waitingSuccesfullySendMessageBlock = document.querySelector('.waiting_succesfully_send_message'),
			waitingIconlist = waitingSuccesfullySendMessageBlock.querySelectorAll('.waiting_icon_span'),
			buttonBox = document.querySelector('.send_message_button_box'),
			sendMessageButton = document.querySelector('.send_message_to_user_button'),
			modal = document.querySelector('.modal'),
			modalBox = document.querySelector('.modal_box'),
			writeMessageField = modalBox.querySelector('.write_message_block>textarea'),
			succesfullyReport = document.querySelector('.succesfully_report');

		for (let node of waitingIconlist) {
			node.classList.add('not_active');
		}
		
		succesfullyReport.classList.remove('not_active');
		
		setTimeout(function() {
			modalBox.classList.remove('modal_box_active');
			writeMessageField.value = '';
			
			setTimeout(function() {
				modal.classList.remove('modal_active');
			}, 300);
			
			for (let node of waitingIconlist) {
				node.classList.remove('not_active');
			}
			
			succesfullyReport.classList.add('not_active');
			sendMessageButton.classList.add('not_active_element');
			disActivateIconWaiting(buttonBox, waitingSuccesfullySendMessageBlock)
		}, 750);
	}
	
	_run_message_succesfully_sent_dialog_chat(data) {
		FileElement.clearContainerForFilesOfMessage();
		this.tabSync._minorAddMessageOnDialogOrChatPage(data['fromUserID'], data['ava_from'], data['user_from_fullname'], data['date'], data['time'], data['message_text'], data['file_list'], data['important'], data['message_id'], true);
		this.tabSync._scrollDownTheBlockWithMessages();
	}
	
	sendCSRFToLS() {
		let dataForLS = {
			'secondary_send': {
				'csrfVal': document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content'),
				'ws_tab_sync_to_do': 'syncCSRF'
			}
		};

		this.myStorageLS.setItem('tab_sync_self', JSON.stringify(dataForLS));
		this.myStorageLS.removeItem('tab_sync_self');
	}
	
	xjscsrf(callback) {
		let xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				let result = JSON.parse(xhr.responseText),
					csrfMeta = document.querySelector('meta[name=X-CSRF-TOKEN]');
				csrfMeta.setAttribute('content', result['token']);
				callback();
			}
		};

		xhr.open('GET', '/cs');
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.setRequestHeader("X-DATA-CS", "get");
		xhr.send();
	}
	
	_getUserID() {
		let cookie = document.cookie.split(';'),
			token = '',
			userID = false;
			
		if (cookie && cookie.length > 0) {
			for (let i = 0; i < cookie.length; i++) {
				if (cookie[i].trim().slice(0, 4) === 'sta=') {
					token = cookie[i].slice(4)
				}
			}
		}

		if (token) {
			userID = JSON.parse(atob(unescape(token.split('.')[1]))).userID;
		}
		
		return userID;
	}
	
	checkToken(callback) {
		let cookie = document.cookie.split(';'),
			token = '',
			exp;
			
		if (cookie && cookie.length > 0) {
			for (let i = 0; i < cookie.length; i++) {
				if (cookie[i].slice(0, 4) === 'sta=') {
					token = cookie[i].slice(4)
				}
			}
		}
		
		if (token) {
			exp = Number(JSON.parse(atob(unescape(token.split('.')[1]))).exp + '000');
			if (exp < Date.now() - 7000) {
				this._upgradeToken(callback);
			} else {
				callback();
			}
		} else {
			callback();
		}
	}
	
	_upgradeToken(callback) {
		let xhr = new XMLHttpRequest();

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				let result = JSON.parse(xhr.responseText);
				console.log(result);
				callback(result);
			}
		};

		xhr.open('GET', '/auth/strUpgrade');
		xhr.setRequestHeader('Content-Type', 'application/json');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.setRequestHeader("X-DATA-STR", "up");
		xhr.send();
	}
}
