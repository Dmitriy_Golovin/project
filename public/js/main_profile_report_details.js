/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	profileId = mainBox.dataset.profile_id,
	entityId = mainBox.dataset.entity_id,
	reportId = mainBox.dataset.report_id,
	type = mainBox.dataset.type,
	profileEventContentBlock = mainBox.querySelector('.event_content_block'),
	modalShowGuestUserBox = document.querySelector('.modal_show_guest_user_box'),
	guestUserModal = modalShowGuestUserBox.closest('.modal_show_guest'),
	guestUserModalBox = modalShowGuestUserBox.closest('.modal_show_guest_box'),
	closeGuestUserButton = guestUserModal.querySelector('.close_win_modal'),
	scrollProfileEventParticipantsList = true,
	commentQuerySearch,
	timeOutVideoMediaControl,
	commentFieldContainer,
	fullScreen = false,
	modalSliderClass;

window.addEventListener('load', function() {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + type +
					'&entity_id=' + entityId +
					'&timeOffsetSeconds=' + timeOffsetSeconds +
					'&profile_id=' + profileId +
					'&report_id=' + reportId;

				fetch('/profile/getReportDetails', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				profileEventContentBlock.insertAdjacentHTML('afterbegin', response.html);
		  			})
		  			.then(response => {
		  				commentQuerySearch = mainBox.querySelector('.comment_query_search');
						commentFieldContainer = mainBox.querySelector('.comment_field_container');

						setPaddingBottomForListContainer(commentFieldContainer);

						if (commentFieldContainer) {
							showCommentWriteField(commentFieldContainer);
						}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
});

window.addEventListener('scroll', function() {
	if (commentFieldContainer) {
		showCommentWriteField(commentFieldContainer);
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.closest('.video_file_preview_container')) {
		let videoContainer = e.target.closest('.video_file_preview_container');
		fetchGetVideoViewModal({'page': 2}, videoContainer);
	}

	if (e.target.closest('.image_file_container_preview')) {
		let imageContainer = e.target.closest('.image_file_container_preview'),
			dataObj = {
				'type': TYPE_SHOW,
				'entity_id': reportId,
				'current_file_id': imageContainer.dataset.file_id,
				'entityAction': 'reportSlider',
			};

		fetchGetImageSliderViewModal(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}

	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('profile_entity_show_guest_button')) {
		let dataObj = {
				'profileId': profileId,
				'entityId': entityId,
				'type': type,
				'contentBlock': modalShowGuestUserBox.querySelector('.profile_entity_guest_content'),
				'scrollEl': modalShowGuestUserBox.querySelector('.profile_entity_guest_content')
			};

		fetchGetParticipantsProfileMainEventList(dataObj);
		guestUserModal.classList.add('modal_active');
		guestUserModalBox.classList.add('modal_box_active');
	}
});

closeGuestUserButton.addEventListener('click', function() {
	guestUserModalBox.classList.remove('modal_box_active');

	setTimeout(function() {
		guestUserModal.classList.remove('modal_active');
	}, 300);
});