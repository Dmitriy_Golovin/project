/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	type = mainBox.dataset.type,
	friendContentBlock = mainBox.querySelector('.friend_content_block'),
	emojiContainer = document.querySelector('.emoji_block'),
	writeMessageField = document.querySelector('.write_message_block>textarea'),
	sendMessageButton = document.querySelector('.send_message_to_user_button'),
	searchFriendsField = document.querySelector('.search_friends_field'),
	scrollFriendList = true,
	cursorPositionInMessageField = 0;

window.addEventListener('load', function() {
	let friendData = {
		'type' : type,
	};

	fetchGetFriendListSelf(friendData);
	addEmojiToMessageFieldModal(emojiContainer, writeMessageField, sendMessageButton);
});

searchFriendsField.addEventListener('input', function() {
	let friendData = {
		'type' : type,
		'searchString': this.value,
	};

	fetchGetFriendListSelf(friendData);
});

document.body.addEventListener('click', function(e) {
	if (!e.target.closest('.emoji_block') && e.target !== writeMessageField) {
		cursorPositionInMessageField = 0;
	}

	if (e.target.closest('.emoji_block') !== emojiContainer) {
		emojiContainer.querySelector('.emoji_box').classList.remove('menu_box_active');
		emojiContainer.querySelector('.emoji_box').parentElement.classList.remove('menu_box_active');
		writeMessageField.classList.remove('message_field_modal_emoji_active');
	}

	if (e.target.classList.contains('send_message_button')) {
		var from = tab.userID,
			userContainer = e.target.closest('.friend_result_item'),
			to = userContainer.dataset.profile_id,
			userFullnameBlock = userContainer.querySelector('.user_name_block_friend_result_item>a'),
			toFullName = userFullnameBlock.innerHTML;
			
		activateWriteNewMessageBox(from, to, toFullName);
	}

	if (e.target.classList.contains('menu_block_friend_result_item') ||
		e.target.closest('.menu_block_friend_result_item')) {
		closeFriendMenuContainer();
		let container = e.target.closest('.menu_block_friend_result_item') || e.target;
		container.querySelector('.burger_menu_friend_result_item').classList.add('menu_box_active');
	}

	if (!e.target.classList.contains('menu_block_friend_result_item') &&
		!e.target.closest('.menu_block_friend_result_item')) {
		closeFriendMenuContainer();
	}

	if (e.target.classList.contains('burger_user_friend_menu_item')) {
		let dataObj = {
				'profileId': e.target.dataset.profile_id,
				'type': e.target.dataset.ws_tab_sync_to_do,
			};

		fetchSendFriendRequest(dataObj, function(response) {
			tab.tabSync['_' + response.action]({
				'htmlData': response.htmlData,
				'userFromId': response.userFromId,
				'userToId': response.userToId,
				'newUserAction': response.newUserAction,
				'newSubscriberAction': response.newSubscriberAction,
			});

			tab.sendToLS({
				'ws_tab_sync_to_do': response.action,
				'htmlData': response.htmlData,
				'userFromId': response.userFromId,
				'userToId': response.userToId,
				'newUserAction': response.newUserAction,
				'newSubscriberAction': response.newSubscriberAction,
			});
		});
	}

	if (e.target.classList.contains('friend_button')) {
		let noticeModal = document.body.querySelector('.modal_notice'),
			dataObj = {
				'profileId': e.target.dataset.profile_id,
				'type': e.target.dataset.ws_tab_sync_to_do,
			};

		if (noticeModal) {
			noticeModal.querySelector('.modal_box').classList.remove('modal_box_active');
							
			setTimeout(function() {
				noticeModal.remove();
			}, 300);
		}


		fetchSendFriendRequest(dataObj, function(response) {
			tab.tabSync['_' + response.action]({
				'htmlData': response.htmlData,
				'userFromId': response.userFromId,
				'userToId': response.userToId,
				'newUserAction': response.newUserAction,
			});

			tab.sendToLS({
				'ws_tab_sync_to_do': 'subscribe',
				'htmlData': response.htmlData,
				'userFromId': response.userFromId,
				'userToId': response.userToId,
			});
		});
	}

	if (e.target.classList.contains('invite_button_friend_result_item')) {
		let profileId = e.target.closest('.friend_result_item').dataset.profile_id;
		activateInviteModal(profileId);
	}
});

function closeFriendMenuContainer() {
	let menu = document.querySelector('.friend_query_search').querySelector('.menu_box_active');

	if (menu) {
		menu.classList.remove('menu_box_active');
	}
}