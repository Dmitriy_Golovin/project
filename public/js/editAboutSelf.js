function editAboutSelf(aboutSelfBlock, editBlock) {
	var result = '';
	if (aboutSelfBlock.children.length > 0) {
		for(var i = 0; i < aboutSelfBlock.children.length; i++) {
			result += aboutSelfBlock.children[i].innerHTML;
			if (i < aboutSelfBlock.children.length - 1) {
				result += '\n';
			}
		}
	}
	return result;
}