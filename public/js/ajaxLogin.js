function checkLoginPasswordAjax(email, password, emailExists) {
	var xhr = new XMLHttpRequest(),
		noticeElem = document.querySelector('.notice_elem_start_page') || document.querySelector('.notice_elem'),
		data = 'email=' + email.value + '&' + 'password=' + password.value + '&' + 'ajax=' + true;

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var result = JSON.parse(xhr.responseText);

			if (result['res'] == 1) {
				noticeElem.innerHTML = 'неверные данные';
				noticeElem.classList.add('notice_elem_active');
				email.classList.add('error_input');
				email.classList.remove('valid_input');
				password.classList.add('error_input');
				password.classList.remove('valid_input');
			} else if (result['res'] == 0){
				document.location.href = '/cabinet';
			}
		}
	};

	xhr.open('POST', '/ajax/checkLoginData');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(data);
}