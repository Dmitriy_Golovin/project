/*jshint esversion: 6 */

const monthList = {
		'январь': 1,
		'февраль': 2,
		'март': 3,
		'апрель': 4,
		'май': 5,
		'июнь': 6,
		'июль': 7,
		'август': 8,
		'сентябрь': 9,
		'октябрь': 10,
		'ноябрь': 11,
		'декабрь': 12
	};

function addEventOnInviteElemList(container) {
	
	let inviteBoxContentWrapper = container.querySelector('.invite_wrapper_box_create_new_event'),
		inviteBoxEl = container.querySelector('.invite_box_create_new_event'),
		eventElList = inviteBoxEl.querySelectorAll('.invite_item'),
		textElFriends = inviteBoxEl.children[1].innerHTML,
		textElUsers = inviteBoxEl.children[2].innerHTML,
		textElByEmail = inviteBoxEl.children[3].innerHTML;
	
	for (let el of eventElList) {
		el.addEventListener('click', function() {
			removeActiveClassForInviteItem();
			el.classList.add('invite_item_active');
			if (el.getAttribute('data-invite') == 'friends') {
				let textInnerItem = textElFriends;
				addInviteContentForFriends(container, inviteBoxContentWrapper, textInnerItem, inviteObj['friends'], this, 'показать друзей');
			} else if (el.getAttribute('data-invite') == 'users') {
				let textInnerItem = textElUsers;
				addInviteContentForUsers(container, inviteBoxContentWrapper, textInnerItem, inviteObj['users'], this, 'показать пользователей');
			} else if (el.getAttribute('data-invite') == 'email') {
				let textInnerItem = textElByEmail;
				addInviteContentForEmail(container, inviteBoxContentWrapper, textInnerItem, inviteObj['email'], this);
			}
		});
	}
	
}

function showInviteContainerEventMeeting(container) {
	let inviteButton = container.querySelector('.invite_user_buton'),
		inviteContainer = container.querySelector('.invited_container'),
		backButton = inviteContainer.querySelector('.back_button'),
		invitedContentBlock = inviteContainer.querySelector('.invited_content_block');

	inviteButton.addEventListener('click', function() {
		activeInviteContainer(inviteContainer, invitedContentBlock, 'invite');
	});

	backButton.addEventListener('click', function() {
		inviteContainer.classList.add('not_active_opacity');
	});

	inviteContainer.addEventListener('transitionend', function(e) {
		if (this.getAttribute('data-active') == 0 && inviteContainer.classList.contains('not_active_opacity') && !inviteContainer.classList.contains('not_active_transform_x')) {
			inviteContainer.classList.remove('not_active_opacity');
			this.setAttribute('data-active', 1);
		}

		if (this.getAttribute('data-active') == 1 && inviteContainer.classList.contains('not_active_opacity') && !inviteContainer.classList.contains('not_active_transform_x')) {
			inviteContainer.classList.add('not_active_transform_x');
			this.setAttribute('data-active', 0);
		}
	});
}

function showInviteContainerTask(container) {
	let inviteButton = container.querySelector('.invite_button'),
		inviteContainer = container.querySelector('.invited_container'),
		backButton = inviteContainer.querySelector('.back_button'),
		invitedContentBlock = inviteContainer.querySelector('.invited_content_block'),
		assignedUserContainer = container.querySelector('.assigned_user_container');

	inviteButton.addEventListener('click', function() {
		removeErrors(container);
		activeInviteContainer(inviteContainer, invitedContentBlock, 'assign');
	});

	backButton.addEventListener('click', function() {
		inviteContainer.classList.add('not_active_opacity');
		scrollUserList = true;
	});

	inviteContainer.addEventListener('transitionend', function(e) {
		if (this.getAttribute('data-active') == 0 && inviteContainer.classList.contains('not_active_opacity') && !inviteContainer.classList.contains('not_active_transform_x')) {
			inviteContainer.classList.remove('not_active_opacity');
			this.setAttribute('data-active', 1);

			setTimeout(function() {
				assignedUserContainer.querySelector('.assigned_user_block').innerHTML = '';
				inviteButton.classList.remove('not_active');
				assignedUserContainer.classList.add('not_active');
			}, 200);
		}

		if (this.getAttribute('data-active') == 1 && inviteContainer.classList.contains('not_active_opacity') && !inviteContainer.classList.contains('not_active_transform_x')) {
			inviteContainer.classList.add('not_active_transform_x');
			this.setAttribute('data-active', 0);
		}
	});
}

function showAddFileContainer(container) {
	let addFileButton = container.querySelector('.file_button_create_new_event'),
		fileContainer = container.querySelector('.add_file_container'),
		backButton = fileContainer.querySelector('.back_button');

	addFileButton.addEventListener('click', function() {
		removeErrors(container);
		fileContainer.classList.remove('not_active_transform_x');
	});

	backButton.addEventListener('click', function() {
		let fileContainerList = container.querySelectorAll('.create_event_file_container'),
			fileCount = 0;

		for (let el of fileContainerList) {
			fileCount += Number(el.children.length);
		}

		if (fileCount > 0) {
			addFileButton.innerHTML = addFileButton.innerHTML.replace(/[0-9]/g, '') + ' ' + fileCount;
		}

		fileContainer.classList.add('not_active_opacity');
	});

	fileContainer.addEventListener('transitionend', function(e) {
		if (this.getAttribute('data-active') == 0 && this.classList.contains('not_active_opacity') && !this.classList.contains('not_active_transform_x')) {
			this.classList.remove('not_active_opacity');
			this.setAttribute('data-active', 1);
		}

		if (this.getAttribute('data-active') == 1 && this.classList.contains('not_active_opacity') && !this.classList.contains('not_active_transform_x')) {
			this.classList.add('not_active_transform_x');
			this.setAttribute('data-active', 0);
		}
	});
}

function closeWinForAnyNewEvent(container, contentWrapper, addButtonBlock, title) {
	contentWrapper.classList.add('wrapper_create_new_event_transparent');
	addButtonBlock.classList.add('wrapper_create_new_event_transparent');
	title.classList.remove('title_create_new_event_pos_rel');
	title.classList.add('title_create_new_event_transition');
	container.classList.add('not_active');
	container.classList.remove('container_create_new_event_active_left');
	container.classList.remove('container_create_new_event_active_right');
	container.classList.remove('container_create_new_task_active_left');
	container.classList.remove('container_create_new_task_active_right');
	containerForElement.children[0].prepend(container);
	document.querySelector('.content_wrapper').classList.remove('not_clickable');
	returnContainerItemsToTheirOriginalState(container);
	removeCountInviteItemEl(container);

	removeErrors(container);
	removeTaskAssignedUser(container);

	if (container.querySelector('.invited_added_users_container')) {
		container.querySelector('.invited_added_users_container').innerHTML = '';
	}

	if (container.id == 'event' || container.id == 'meeting') {
		let invitedAddedUsersButton = container.querySelector('.invited_added_users_button');

		invitedAddedUsersButton.innerHTML = invitedAddedUsersButton.innerHTML.replace(/[0-9]/g, '');
	}

	videoFileList = new DataTransfer();
	imageFileList = new DataTransfer();
	documentFileList = new DataTransfer();

	let fileFieldList = container.querySelectorAll('.load_file'),
		fileContainerList = container.querySelectorAll('.create_event_file_container'),
		fileFiledValueList = container.querySelectorAll('.create_event_file_field_value'),
		addFileButton = container.querySelector('.file_button_create_new_event');

	if (addFileButton) {
		addFileButton.innerHTML = addFileButton.innerHTML.replace(/[0-9]/g, '');
	}

	for (let el of fileFieldList) {
		el.value = '';
	}

	for (let el of fileContainerList) {
		el.innerHTML = '';
	}

	for (let el of fileFiledValueList) {
		el.classList.remove('not_active');
	}
}

function activeInviteContainer(inviteContainer, invitedContentBlock, type) {
	let data = {
			'type': type,
			'only_friends': 0,
			'within_group': 0,
		},
		container = inviteContainer.closest('.container_create_new_event');

	if (container.id == 'event' || container.id == 'meeting') {
		let container = inviteContainer.closest('.container_create_new_event'),
			invitedAddedUsersContainer = container.querySelector('.invited_added_users_container');

		if (invitedAddedUsersContainer.children.length > 0) {
			let userIdList = [];

			for (let el of invitedAddedUsersContainer.children) {
				userIdList.push(el.getAttribute('data-profile_id'));
			}

			data.exceptionUserIdList = userIdList;
		}
	}

	fetchGetUserList(data, invitedContentBlock)
	inviteContainer.classList.remove('not_active_transform_x')
}

function removeTaskAssignedUser(container) {
	if (container.querySelector('.task_assigned_user')) {
		container.querySelector('.task_assigned_user').remove();
	}

	if (container.querySelector('.invite_button')) {
		container.querySelector('.invite_button').classList.remove('not_active');
	}

	if (container.querySelector('.assigned_user_container')) {
		container.querySelector('.assigned_user_container').classList.add('not_active');
	}
}

function returnContainerItemsToTheirOriginalState(container) {
	let selectList = container.querySelectorAll('select'),
		privacyBox = container.querySelector('.privacy_box_create_new_event');
		inputList = container.querySelectorAll('input[type=text]'),
		descriptionEl = container.querySelector('.description_create_new_event');
	
	for (let el of selectList) {
		el.selectedIndex = 0;
	}
	
	for (let el of inputList) {
		el.value = '';
	}
	
	privacyBox.children[0].checked = true;
	descriptionEl.value = '';
}

function removeCountInviteItemEl(container) {
	let elemList = container.querySelectorAll('.invite_item');
	
	for (let el of elemList) {
		if (el.innerHTML.indexOf(' ') == -1) continue;
		el.innerHTML = el.innerHTML.slice(0, el.innerHTML.indexOf(' '));
	}
}

function getDataForInviteUsers(inviteContainer) {
	let container = inviteContainer.closest('.container_create_new_event'),
		searchString = inviteContainer.querySelector('.invited_search_field').value,
		onlyFriends = inviteContainer.querySelector('.invited_only_friends').checked,
		invitedSelectGroup = inviteContainer.querySelector('select[name="invited_select_group"]'),
		addedUserContainer = inviteContainer.querySelector('.invited_added_users_container'),
		result = {};

	if (searchString) {
		result.searchString = searchString;
	}

	result.only_friends = Number(onlyFriends);
	// result.group_id = invitedSelectGroup.options[invitedSelectGroup.selectedIndex].getAttribute('data-group_id');
	// result.within_group = 0;

	// if (inviteContainer.querySelector('.within_group') && !inviteContainer.querySelector('.within_group').classList.contains('not_active')) {
	// 	result.within_group = inviteContainer.querySelector('.within_group').value;
	// }

	if (container.id === 'event' || container.id === 'meeting') {
		result.type = 'invite';
	}

	if (container.id === 'task') {
		result.type = 'assign';
	}

	if (addedUserContainer && addedUserContainer.children.length > 0) {
		let exceptionUserIdList = [];

		for (let el of addedUserContainer.children) {
			exceptionUserIdList.push(el.getAttribute('data-profile_id'));
		}

		result.exceptionUserIdList = exceptionUserIdList;
	}

	return result;
}

function getDataForEvent(container) {
	let dataObj = {},
		dayEl = container.querySelector('.select_day'),
		monthEl = container.querySelector('.select_month'),
		yearEl = container.querySelector('.select_year'),
		durationEl = container.querySelector('.select_end_time'),
		publicEl = container.querySelector('.privacy_box_create_new_event').children[0],
		periodicityEl = container.querySelector('.periodicity_select'),
		reminderEl = container.querySelector('.reminder_select'),
		descriptionEl = container.querySelector('.description_create_new_event'),
		dayValue = (dayEl.value.length < 2) ? '0' + dayEl.value : dayEl.value,
		fileFieldList = container.querySelectorAll('.load_file');

	for (let el of fileFieldList) {
		if (el.files.length > 0) {
			dataObj[el.getAttribute('name')] = el.files;
		}
	}
	
	if (container.id === 'task') {
		dataObj.assigned_user_id = container.querySelector('.assigned_user_container').querySelector('.user_result_item').getAttribute('data-profile_id');
	}

	if (container.id === 'event' || container.id === 'meeting')  {
		let invitedAddedUsersContainer = container.querySelector('.invited_added_users_container');

		if (invitedAddedUsersContainer.children.length > 0) {
			let userIdList = [];

			for (let el of invitedAddedUsersContainer.children) {
				userIdList.push(el.getAttribute('data-profile_id'));
			}

			dataObj.guestUserIdList = JSON.stringify(userIdList);
		}

		if (container.querySelector('.input_new_event_location').value) {
			dataObj.location = container.querySelector('.input_new_event_location').value;
		}
	}

	if (reminderEl.options[reminderEl.selectedIndex].getAttribute('data-reminder')) {
		dataObj.reminder = reminderEl.options[reminderEl.selectedIndex].getAttribute('data-reminder');
	}

	if (descriptionEl.value) {
		dataObj.description = descriptionEl.value;
	}

	dataObj.name = container.querySelector('.input_new_event_name').value;
	dataObj.time = (Number(container.querySelector('.select_start_time_hour').value) * (60 * 60)) + (Number(container.querySelector('.select_start_time_minute').value) * 60);
	dataObj.duration = durationEl.options[durationEl.selectedIndex].getAttribute('data-duration');
	dataObj.privacy = (publicEl.checked) ? 0 : 1;
	dataObj.periodicity = periodicityEl.options[periodicityEl.selectedIndex].getAttribute('data-periodicity');
	dataObj.date = new Date(yearEl.value, monthEl.options[monthEl.selectedIndex].getAttribute('data-month') - 1, dayValue).getTime() / 1000;
	dataObj.type = container.id;

	return dataObj;
}

function getStrDataForAjax(obj) {
	let result = '';
	for (let key in obj) {
		result += key + '=' + obj[key] + '&';
	}
	return result.slice(0, -1);
}

function showBlockForCreateNewEventByElAttr(parent, itemEl, dateOfCell) {
	let dateArr = dateOfCell.split('.'),
		containerForPaste = document.querySelector('.content_wrapper'),
		container = document.querySelector('#' + itemEl.getAttribute('data-content')),
		closeSpan = container.querySelector('.span_close_win_add_new_event'),
		title = container.children[1],
		contentWrapper = container.querySelector('.wrapper_create_new_event'),
		addButtonBlock = container.querySelector('.add_button_block'),
		containerForPasterect = containerForPaste.getBoundingClientRect(),
		parentRect = parent.getBoundingClientRect(),
		itemRect = itemEl.getBoundingClientRect(),
		itemStyle = getComputedStyle(itemEl);

	// if (parentRect.left >= document.documentElement.clientWidth / 2) {
	// 	container.style.right = document.documentElement.clientWidth - parentRect.right + 'px';
	// } else {
		container.style.left = parentRect.left + 'px';
	// }
		
	title.style.top = containerForPaste.offsetTop - 6 + 'px';
	title.style.left = itemRect.left - parentRect.left + 'px';
	title.style.width = itemRect.width - 6 + 'px';
	title.style.fontSize = itemStyle.fontSize;
	container.style.maxHeight = parentRect.height + 'px';
	container.style.width = parentRect.width + 'px';
	container.style.top = parentRect.top + 'px';
	
	container.classList.remove('not_active');
	closeWinForNewEvent(containerForElement);
	containerForPaste.appendChild(container);
	container.getBoundingClientRect();
	
	if (itemEl.getAttribute('data-content') == 'event' || itemEl.getAttribute('data-content') == 'meeting') {
		getContentForEventOrMeetingBlock(container, dateArr);
		if (parentRect.left <= document.documentElement.clientWidth / 2) {
			container.classList.add('container_create_new_event_active_left');
		} else {
			container.classList.add('container_create_new_event_active_right');
		}
	} else if (itemEl.getAttribute('data-content') == 'task') {
		getContentForTaskBlock(container, dateArr);
		if (parentRect.left <= document.documentElement.clientWidth / 2) {
			container.classList.add('container_create_new_task_active_left');
		} else {
			container.classList.add('container_create_new_task_active_right');
		}
	}
	
	title.classList.add('title_create_new_event_active');
	
	container.addEventListener('transitionend', function(e) {
		container.removeAttribute('style');

		if (e.propertyName === 'max-height') {
			contentWrapper.classList.remove('wrapper_create_new_event_transparent');
			addButtonBlock.classList.remove('wrapper_create_new_event_transparent');
		}

	});
	title.addEventListener('transitionend', function() {
		title.classList.remove('title_create_new_event_transition');
		title.removeAttribute('style');
		title.classList.add('title_create_new_event_pos_rel');
		title.classList.remove('title_create_new_event_active');
	});
	
	containerForPaste.classList.add('not_clickable');
	
	closeSpan.addEventListener('click', closeBlockForCreateNewEvent);
	
	function closeBlockForCreateNewEvent(e) {
		if (e.target == this || closeSpan.contains(e.target)) {
			closeWinForAnyNewEvent(container, contentWrapper, addButtonBlock, title);
			closeSpan.removeEventListener('click', closeBlockForCreateNewEvent);
		}
	}
}

function getContentForEventOrMeetingBlock(container, dateArr) {
	let selectYear = container.querySelector('.select_year'),
		selectMonth = container.querySelector('.select_month'),
		selectDay = container.querySelector('.select_day'),
		selectHour = container.querySelector('.select_start_time_hour'),
		selectMinute = container.querySelector('.select_start_time_minute')
		dayOfWeek = container.querySelector('.day_of_week_create_new_event');
	
	getYearElementList(selectYear, dateArr[2]);
	getNameOfMonthElementList(selectMonth, dateArr[1])
	getDayElementList(selectDay, dateArr[0], dateArr[1], dateArr[2]);
	getNameOfWeek(dayOfWeek, dateArr[0], dateArr[1], dateArr[2]);
	changeCountDayInMonth(container);
	preparePeriodicityReminderByDate(container, selectYear, selectMonth, selectDay, selectHour, selectMinute);
}

function getContentForTaskBlock(container, dateArr) {
	let selectYear = container.querySelector('.select_year'),
		selectMonth = container.querySelector('.select_month'),
		selectDay = container.querySelector('.select_day'),
		dayOfWeek = container.querySelector('.day_of_week_create_new_event'),
		date = new Date(dateArr[2], Number(dateArr[1]) - 1, dateArr[0]),
		dateTimestamp = date.getTime(),
		nowTimestamp = Date.now() + 3600000;

	if (dateTimestamp <= nowTimestamp) {
		let dateNow = new Date(nowTimestamp);

		getYearElementList(selectYear, dateNow.getFullYear());
		getNameOfMonthElementList(selectMonth, dateNow.getMonth() + 1);
		getDayElementList(selectDay, dateNow.getDate(), dateNow.getMonth() + 1, dateNow.getFullYear());
		getNameOfWeek(dayOfWeek, dateNow.getDate(), dateNow.getMonth() + 1, dateNow.getFullYear());
		setHours(container, dateNow.getHours());
		setMinutes(container, dateNow.getMinutes());
	} else {
		getYearElementList(selectYear, dateArr[2]);
		getNameOfMonthElementList(selectMonth, dateArr[1])
		getDayElementList(selectDay, dateArr[0], dateArr[1], dateArr[2]);
		getNameOfWeek(dayOfWeek, dateArr[0], dateArr[1], dateArr[2]);
	}

	changeCountDayInMonth(container);
}

function setHours(container, value) {
	let selectHour = container.querySelector('.select_start_time_hour');
	selectHour.selectedIndex = value;
}

function setMinutes(container, value) {
	let selectMinute = container.querySelector('.select_start_time_minute');
	selectMinute.selectedIndex = value;
}

function getNameOfMonthElementList(parent, numOfMonth) {
	let monthList = ['январь', 'февраль', 'март', 'апрель', 'май', 'июнь', 'июль', 'август', 'сентябрь', 'октябрь', 'ноябрь', 'декабрь']
	parent.selectedIndex = numOfMonth - 1;
}

function getYearElementList(parent, currentYear) {
	let selectedIndex = 0;
	parent.innerHTML = '';

	for (let i = - 100; i <= 100; i++) {
		let optionEl = document.createElement('option');
		optionEl.classList.add('option_create_new_event');
		optionEl.innerHTML = Number(currentYear) + i;
		parent.appendChild(optionEl);
		if (i === 0) {
			console.log(Number(currentYear) + i)
		}
		if (Number(i) === 0) {
			parent.selectedIndex = selectedIndex;
		}

		selectedIndex++;
	}
}

function getDayElementList(parent, numOfDay, numOfMonth, currentYear) {
	let curMonthFirstDay = new Date(currentYear, numOfMonth - 1, 1),
		nextMonthFirstDay = new Date(currentYear, numOfMonth, 1),
		dayCount = Math.round((nextMonthFirstDay - curMonthFirstDay) / 1000 / 3600 / 24);

	parent.innerHTML = '';
	for (let i = 1; i <= dayCount; i++) {
		let optionEl = document.createElement('option');
		optionEl.classList.add('option_create_new_event');
		optionEl.innerHTML = i;
		parent.appendChild(optionEl);
	}
	parent.selectedIndex = numOfDay - 1;
}

function getNameOfWeek(parent, numOfDay, numOfMonth, currentYear) {
	let day = new Date(currentYear, numOfMonth - 1, numOfDay).getDay(),
		nameOfDayWeekList = ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'];
	parent.innerHTML = nameOfDayWeekList[day];
}

function changeCountDayInMonth(container) {
	let dayEl = container.querySelector('.select_day'),
		monthEl = container.querySelector('.select_month'),
		yearEl = container.querySelector('.select_year'),
		dayOfWeekEl = container.querySelector('.day_of_week_create_new_event'),
		hourEl = container.querySelector('.select_start_time_hour'),
		minuteEl = container.querySelector('.select_start_time_minute');
	
	monthEl.addEventListener('change', function() {
		getDayElementList(dayEl, 1, monthList[monthEl.value], Number(yearEl.value));
		getNameOfWeek(dayOfWeekEl, Number(dayEl.value), monthList[monthEl.value], Number(yearEl.value));
		preparePeriodicityReminderByDate(container, yearEl, monthEl, dayEl, hourEl, minuteEl)
	});

	yearEl.addEventListener('change', function() {
		getDayElementList(dayEl, 1, monthList[monthEl.value], Number(yearEl.value));
		getNameOfWeek(dayOfWeekEl, Number(dayEl.value), monthList[monthEl.value], Number(yearEl.value));
		preparePeriodicityReminderByDate(container, yearEl, monthEl, dayEl, hourEl, minuteEl)
	});

	dayEl.addEventListener('change', function() {
		getNameOfWeek(dayOfWeekEl, Number(dayEl.value), monthList[monthEl.value], Number(yearEl.value));
		preparePeriodicityReminderByDate(container, yearEl, monthEl, dayEl, hourEl, minuteEl)
	});

	hourEl.addEventListener('change', function() {
		preparePeriodicityReminderByDate(container, yearEl, monthEl, dayEl, hourEl, minuteEl)
	});

	minuteEl.addEventListener('change', function() {
		preparePeriodicityReminderByDate(container, yearEl, monthEl, dayEl, hourEl, minuteEl)
	});
}

function preparePeriodicityReminderByDate(container, yearEl, monthEl, dayEl, hourEl, minuteEl) {
	let date = new Date(yearEl.value, monthList[monthEl.value] - 1, dayEl.value, hourEl.value, minuteEl.value),
		dateTimestamp = date.getTime(),
		nowTimestamp = Date.now(),
		reminderEl = container.querySelector('.reminder_select'),
		periodicityEl = container.querySelector('.periodicity_select'),
		inviteBox = container.querySelector('.invite_box_create_new_event');
	
	removeErrors(container);

	if (reminderEl && periodicityEl) {
		for (let el of reminderEl.children) {
			if (typeof Number(el.getAttribute('data-duration')) === 'number' && dateTimestamp - Number(el.getAttribute('data-duration') + '000') <= nowTimestamp) {
				if (el.getAttribute('data-duration') === reminderEl.options[reminderEl.selectedIndex].getAttribute('data-duration')) {
					reminderEl.selectedIndex = 0;
				}

				el.classList.add('not_active');
			} else {
				el.classList.remove('not_active');
			}
		}

		if (dateTimestamp <= nowTimestamp) {
			reminderEl.parentElement.classList.add('not_active_element');
			periodicityEl.parentElement.classList.add('not_active_element');

			if (inviteBox) {
				inviteBox.classList.add('not_active_element');
			}

			removeCountInviteItemEl(container);
		} else {
			reminderEl.parentElement.classList.remove('not_active_element');
			periodicityEl.parentElement.classList.remove('not_active_element');
			
			if (inviteBox) {
				inviteBox.classList.remove('not_active_element');
			}
		}
	}

	if (container.id === 'task') {
		if (dateTimestamp <= nowTimestamp) {
			let errorEl = container.querySelector('.event_form_error'),
				saveButton = container.querySelector('.add_button_event'),
				dayEl = container.querySelector('.select_day'),
				monthEl = container.querySelector('.select_month'),
				yearEl = container.querySelector('.select_year'),
				hourEl = container.querySelector('.select_start_time_hour'),
				minuteEl = container.querySelector('.select_start_time_minute');

			saveButton.classList.add('not_active_opacity');
			errorEl.innerHTML = 'дата и время не могут быть в прошлом';
			errorEl.classList.add('event_form_error_active');
			dayEl.classList.add('error_input');
			monthEl.classList.add('error_input');
			yearEl.classList.add('error_input');
			hourEl.classList.add('error_input');
			minuteEl.classList.add('error_input');
		}
	}
}

function checkBaseEventdata(container) {
	let inputNewEventName = container.querySelector('.input_new_event_name'),
		errorEl = container.querySelector('.event_form_error'),
		saveButton = container.querySelector('.add_button_event');

	if (!inputNewEventName.value) {
		inputNewEventName.onfocus = function() {
			removeErrors(container);
		};

		inputNewEventName.classList.add('error_input');
		errorEl.innerHTML = 'заполните название';
		errorEl.classList.add('event_form_error_active');
		saveButton.classList.add('not_active_opacity');
		return false;
	}

	if (container.id === 'task') {
		if (!container.querySelector('.assigned_user_container').querySelector('.user_result_item')) {
			let inviteButtonContainer = container.querySelector('.invite_button_container');

			inviteButtonContainer.classList.add('error_input');
			errorEl.innerHTML = 'назначьте исполнителя';
			errorEl.classList.add('event_form_error_active');
			saveButton.classList.add('not_active_opacity');
			return false;
		}
	}

	return true;
}

function removeErrors(container) {
	let errorEl = container.querySelector('.event_form_error'),
		saveButton = container.querySelector('.add_button_event'),
		errorInputList = container.querySelectorAll('.error_input');

	if (errorEl.classList.contains('event_form_error_active')) {
		errorEl.classList.remove('event_form_error_active');
		saveButton.classList.remove('not_active_opacity');

		for (let el of errorInputList) {
			el.classList.remove('error_input');
		}
		
		errorEl.ontransitionend = function() {
			if (!errorEl.classList.contains('event_form_error_active')) {
				errorEl.innerHTML = '';
			}
		}
	}
}