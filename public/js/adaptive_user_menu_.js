function setDataAttributePositionForUsermenuItem(elemList) {
	
	var firstEl = elemList[0],
		firstPosition = 0
		elComputedStyle = getComputedStyle(firstEl),
		elComputedStyleParentPrevious = getComputedStyle(firstEl.parentElement.parentElement.previousElementSibling),
		elCoords = firstEl.getBoundingClientRect();
		elFullWidth = elCoords.width + Number(elComputedStyle.marginRight.replace(/\D+/g, '')) + Number(elComputedStyle.marginLeft.replace(/\D+/g, ''));
	firstPosition = Number(elComputedStyleParentPrevious.width.replace(/\D+/g, '')) + Number(elComputedStyleParentPrevious.marginRight.replace(/\D+/g, '')) + 20 + elFullWidth;
	var position = firstPosition;

	[].forEach.call(elemList, function(el, index, arr) {
		var elComputedStyle = getComputedStyle(el),
			elCoords = el.getBoundingClientRect(),
			elFullWidth = elCoords.width + Number(elComputedStyle.marginRight.replace(/\D+/g, '')) + Number(elComputedStyle.marginLeft.replace(/\D+/g, ''));

			if (index > 0) position += elFullWidth;
			if (index == arr.length - 1) position = position - 26;
		el.setAttribute('data-right_side_el', position);
	});
}

function showHideElemMenuList(elemList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu, errorMargin) {
	var burgerMenuSizes = getComputedStyle(iconBurgerMenu),
		rightMarginOfParent = 20,
		widthElemOnTheRight;

	errorMargin = errorMargin || 0;

	if (document.documentElement.clientWidth <= 377) {
		rightMarginOfParent = 5;
	}

	if (elemOnTheRight) {
		var elOnTheRightCoords = elemOnTheRight.getBoundingClientRect();
		widthElemOnTheRight = elOnTheRightCoords.width + Number(burgerMenuSizes.width.replace(/\D+/g, '')) + Number(burgerMenuSizes.marginLeft.replace(/\D+/g, '')) + Number(burgerMenuSizes.marginRight.replace(/\D+/g, '')) + rightMarginOfParent + 10;
	} else {
		widthElemOnTheRight = Number(burgerMenuSizes.width.replace(/\D+/g, '')) + Number(burgerMenuSizes.marginLeft.replace(/\D+/g, '')) + Number(burgerMenuSizes.marginRight.replace(/\D+/g, '')) + rightMarginOfParent + 1;
	}

	widthElemOnTheRight += errorMargin;

	[].forEach.call(elemList, function(el) {
		var elPosition = el.getAttribute('data-right_side_el');
		if (elPosition > (document.documentElement.clientWidth - widthElemOnTheRight)) {
			movingItemInBurgerMenu(el, burgerMenu);
		} else if (elPosition < (document.documentElement.clientWidth - widthElemOnTheRight)) {
			movingItemInMainMenu(el, iconBurgerMenu);
		}
	});
	
	if (elemList[elemList.length - 1].getAttribute('data-right_side_el') > (document.documentElement.clientWidth - widthElemOnTheRight)) {
		iconBurgerMenu.classList.remove('not_active');
	} else {
		iconBurgerMenu.classList.add('not_active');
	}

	showNotificationAmountInBurgerMenuElem(iconBurgerMenu, burgerMenu);
}

function movingItemInBurgerMenu(el, burgerMenu) {
	burgerMenu.append(el);
	el.classList.remove('menu_user_item');
	el.classList.add('active_burger_menu');
}

function movingItemInMainMenu(el, iconBurgerMenu) {
	iconBurgerMenu.before(el);
	el.classList.remove('active_burger_menu');
	el.classList.add('menu_user_item');
}

function showBurgerMenu(menu, icon, burgerMenu) {
	var iconCoords = icon.getBoundingClientRect();
	menu.classList.remove('burger_menu_header_box_position_left');
	menu.classList.remove('burger_menu_header_box_position_right');
	menu.classList.toggle('menu_box_active');
	
	if (iconCoords.left <= 150) {
		menu.classList.add('burger_menu_header_box_position_left');
	} else {
		menu.classList.add('burger_menu_header_box_position_right');
	}
	
	showNotificationAmountInBurgerMenuElem(icon, burgerMenu, 'toggle');
}

function hideBurgerMenu(menu, icon, e, burgerMenu) {
	var searchBlockList = document.querySelectorAll('.search_elem');
	
	if (e.target === icon || e.target === icon.children[0]) return;
	
	for (var i = 0; i < searchBlockList.length; i++) {
		if (e.target === searchBlockList[i]) return;
	}

	menu.classList.remove('menu_box_active');
	
	showNotificationAmountInBurgerMenuElem(icon, burgerMenu, 'remove')
}

function hideBurgerSubMenu() {
	let activeSubmenuList = document.getElementsByClassName('menu_user_item_submenu_container menu_box_active');
	if (activeSubmenuList.length > 0) {
		activeSubmenuList[0].parentElement.dataset.active_submenu = 0;
		activeSubmenuList[0].classList.remove('opacity_active');
		activeSubmenuList[0].classList.remove('menu_box_active');
	}
	
}

function showNotificationAmountInBurgerMenuElem(iconBurgerMenu, burgerMenu, action) {
	var notificationAmount = 0,
		mainAmountEl = document.querySelector('.main_notification_amount');
	
	if (burgerMenu) {
		for (var i = 0; i < burgerMenu.children.length; i++) {
			if (burgerMenu.children[i].children.length > 0 &&
				burgerMenu.children[i].children[0].children[1] &&
				burgerMenu.children[i].children[0].children[1].tagName == 'SPAN') {
				var notificationAmountItem = burgerMenu.children[i].children[0].children[1].innerHTML;
				notificationAmount += Number(notificationAmountItem);
			}
		}
	}

	if (mainAmountEl) {
		mainAmountEl.innerHTML = notificationAmount;

		if (action === 'toggle') {
			if (notificationAmount > 0) {
				mainAmountEl.classList.toggle('not_active');
			}
		} else if (action === 'add') {
			if (notificationAmount > 0) {
				mainAmountEl.classList.remove('not_active');
			}
		} else {
			if (notificationAmount > 0) {
				mainAmountEl.classList.remove('not_active');
			} else if (notificationAmount < 1) {
				mainAmountEl.classList.add('not_active');
			}
		}
	}
}

function showOrHideNotificationAmountInBurgerMenuElemByEventClick(iconBurgerMenu, burgerMenu, action) {
	
}
