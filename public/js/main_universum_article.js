/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	articleSectionId = mainBox.dataset.article_section_id,
	articleId = mainBox.dataset.article_id,
	aticleDetailsBlock = mainBox.querySelector('.aticle_details_block'),
	commentQuerySearch,
	commentFieldContainer;

window.addEventListener('load', function() {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&article_id=' + articleId +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				fetch('/universum/getArticleCommentData', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						
						if (response.res) {
							aticleDetailsBlock.insertAdjacentHTML('beforeend', response.html);

							commentQuerySearch = mainBox.querySelector('.comment_query_search');
							commentFieldContainer = mainBox.querySelector('.comment_field_container');

							setPaddingBottomForListContainer(commentFieldContainer);

							if (commentFieldContainer) {
								showCommentWriteField(commentFieldContainer);
							}
						} else {
							console.log(response.errors);
						}
					})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
});

window.addEventListener('scroll', function() {
	if (commentFieldContainer) {
		showCommentWriteField(commentFieldContainer);
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}
})