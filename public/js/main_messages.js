/*jshint esversion: 6 */

let searchMessagesBlock = document.querySelector('.search_messages_block'),
	searchDialogOrChatField = document.querySelector('.search_messages_field'),
	dialogOrChatContent = document.querySelector('.messages_block'),
	writeMessageBlock = document.querySelector('.write_message_box'),
	backToAllCorrespondenceButton = document.querySelector('.back_to_all_correspondence_button'),
	correspondenceFullnameOrChatRoom = document.querySelector('.correspondence_fullname_or_chat_room'),
	writeMessageTextField = document.querySelector('.text_message'),
	writeMessageSendMessageButton = document.querySelector('.write_message_send_message_button'),
	emojiContainer = document.querySelector('.emoji_block'),
	cursorPositionInMessageField = 0,
	modalSliderImage = new ModalSliderImage({
		'modalCssClass': 'modal_slider_image',
		'availableImageListContainer': dialogOrChatContent,
	});

window.addEventListener('load', function() {
	tab.checkToken(function() {
		ajaxShowСorrespondence(dialogOrChatContent, getLastPartOfURL());
	});
	if (searchDialogOrChatField) {
		showMatchingResult(searchDialogOrChatField, dialogOrChatContent, 'user_name_box_dialog_chat');
	}
	addEmojiToMessageField(emojiContainer, writeMessageTextField, writeMessageSendMessageButton, 'messagePage');
	activateFileToMessageBlockModal();
});

window.addEventListener('resize', function() {
	dialogOrChatContent.style.height = '';
});

window.addEventListener('popstate', function() {
	tab.checkToken(function() {
		ajaxShowСorrespondence(dialogOrChatContent, getLastPartOfURL());
	});
});

new ResizeObserver(function(){
	scrollDownElement(fileMessageBlock);
	dialogOrChatContent.style.height = dialogOrChatContent.parentElement.offsetHeight - searchMessagesBlock.offsetHeight - writeMessageBlock.offsetHeight - 20 + 'px';
	dialogOrChatContent.style.setProperty('--transitionEl', '0s');
}).observe(writeMessageBlock);

writeMessageTextField.addEventListener('paste', function(e) {
    let paste = (event.clipboardData || window.clipboardData).getData('text');

    const selection = window.getSelection();
    if (!selection.rangeCount) return false;
    selection.deleteFromDocument();
    selection.getRangeAt(0).insertNode(document.createTextNode(paste));

    e.preventDefault();
});

document.body.addEventListener('click', function(e) {
	if (!e.target.closest('.emoji_block') && e.target !== writeMessageTextField) {
		cursorPositionInMessageField = 0;
	}

	if (e.target.classList.contains('delete_correspondence') || e.target.classList.contains('delete_correspondence_pic')) {
		let container = e.target.closest('.dialog_chat_item');

		if (container.hasAttribute('data-type') && container.getAttribute('data-type') === 'chat') {
			let chatId = container.getAttribute('data-chat_id');
			fetchLeaveChat(chatId);
		}

		if (container.hasAttribute('data-type') && container.getAttribute('data-type') === 'dialog') {
			let withUserId = container.getAttribute('data-profile_id');
			fetchDeleteDialogAllMessages(withUserId);
		}
	}
});

writeMessageTextField.addEventListener('click', function() {
	cursorPositionInMessageField = getCaretPosition(this);
});

writeMessageTextField.addEventListener('keyup', function() {
	cursorPositionInMessageField = getCaretPosition(this);
});

writeMessageTextField.addEventListener('keypress', function(e) {
    if (e.keyCode !== 13) {
    	return false;
    }

    if (writeMessageTextField.innerHTML || isEmptyFileContainer()) {
		let data = {
			'ws_tab_sync_to_do': 'send_message_to_user',
			'actionDB': 'addNewMessage',
			'fromUserID': tab.userID,
			'toUserID': correspondenceFullnameOrChatRoom.getAttribute('data-profile_id'),
			'text': writeMessageTextField.innerHTML,
			'fileIdList': getFileIdList(),
			'server_response': 'message_succesfully_sent_dialog_chat',
			'timeOffsetSeconds': new Date().getTimezoneOffset() * (-60),
		};
		writeMessageTextField.innerHTML = '';
		tab.waitForServerResponse = 'message_succesfully_sent_dialog_chat';
		tab.send(tab.tabSync.collectData(data));
	}

    e.preventDefault();
});

writeMessageSendMessageButton.addEventListener('click', function() {
	if (writeMessageTextField.innerHTML || isEmptyFileContainer()) {
		let data = {
			'ws_tab_sync_to_do': 'send_message_to_user',
			'actionDB': 'addNewMessage',
			'fromUserID': tab.userID,
			'toUserID': correspondenceFullnameOrChatRoom.getAttribute('data-profile_id'),
			'text': writeMessageTextField.innerHTML,
			'fileIdList': getFileIdList(),
			'server_response': 'message_succesfully_sent_dialog_chat',
			'timeOffsetSeconds': new Date().getTimezoneOffset() * (-60),
		};

		writeMessageTextField.innerHTML = '';
		// tab.waitForServerResponse = 'message_succesfully_sent_dialog_chat';
		// tab.send(tab.tabSync.collectData(data));
	}
});

function getLastPartOfURL() {
	let urlArr = location.pathname.split('/'),
		lastPartOfURL = urlArr[urlArr.length - 1],
		lastPartOfURLObj = {},
		act = '',
		id = '';
		
	for (let i = 0; i < lastPartOfURL.length; i++) {
		if (isFinite(lastPartOfURL[i])) {
			id += lastPartOfURL[i];
		} else {
			act += lastPartOfURL[i];
		}
	}

	lastPartOfURLObj.act = act;
	lastPartOfURLObj.id = id;
	return lastPartOfURLObj;
}