function showHidePersonalMenu(elemInsideMenuBox, e) {
	
	for (var i = 0; i < elemInsideMenuBox.length; i++) {
		if (e.target === elemInsideMenuBox[i]) {
			return;
		};
	};
	
	var menuBox = document.querySelector('.menu_box'),
	    arrow = document.querySelector('.pers_data_arrow');
		
	menuBox.classList.toggle('menu_box_active');
	arrow.classList.toggle('pers_data_arrow_active');

}

function removePersonalMenu(elemInsideMenuBox, e) {
	
	/* for (var i = 0; i < elemInsideMenuBox.length; i++) {
		if (e.target === elemInsideMenuBox[i]) {
			return;
		};
	}; */
	
	var menuBox = document.querySelector('.menu_box'),
	    arrow = document.querySelector('.pers_data_arrow');
		
	menuBox.classList.remove('menu_box_active');
	arrow.classList.remove('pers_data_arrow_active');

}