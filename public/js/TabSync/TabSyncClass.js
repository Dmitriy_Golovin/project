/*jshint esversion: 6 */

class TabSync {
	
	constructor(selfID) {

		this.selfID = selfID;
		if (this.selfID) {
			if (document.querySelector('.avatar')) {
				this.selfPathToAva = document.querySelector('.avatar').getAttribute('src');
			}

			if (document.querySelector('.user_full_name')) {
				this.selfFullName = document.querySelector('.user_full_name').innerHTML;
			}
		}
		
	}
	
	run(data) {

		let action = '_' + data.ws_tab_sync_to_do;
		this[action](data);
		
	}

	// новое уведомление

	_new_notofication(data) {
		let notificationMenuUserItem = document.querySelector('.notification_menu_user_item');

		if (!notificationMenuUserItem || this.selfID !== data.userId) {
			return;
		}

		this._minorPlusOneAmountNewNotification(notificationMenuUserItem);
		let nothingFoundEl = notificationMenuUserItem.querySelector('.nothing_found'),
			notificationContentBox = notificationMenuUserItem.querySelector('.modal_notification_content_box');

		nothingFoundEl.classList.add('not_active');
		notificationContentBox.insertAdjacentHTML('beforeend', data.html);
	}

	// увеличиваем количество новых приглашений в хеадере

	_update_new_invite_count_plus_one(data) {
		let mainMenuCabinetItem = document.querySelector('.cabinet_menu_user_item'),
			url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (mainMenuCabinetItem && this.selfID === data.userId) {
			this._minorPlusOneAmountNewSubscriber(mainMenuCabinetItem);
		}

		if (lastPartlUrl === 'cabinet' && this.selfID === data.userId) {
			let parentEl = document.querySelector('.invites_' + data.type + '_item_count');

			if (parentEl) {
				let value = parentEl.firstChild.textContent;
				parentEl.firstChild.textContent = Number(value) + 1;
				this._minorPlusOneNewInvite(parentEl);
			}
		}
	}

	//увеличиваем количество новых задач в хеадере
	
	_update_new_task_count_plus_one(data) {
		let mainMenuCabinetItem = document.querySelector('.cabinet_menu_user_item'),
			url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (mainMenuCabinetItem && this.selfID === data.userId) {
			this._minorPlusOneAmountNewSubscriber(mainMenuCabinetItem);
		}

		if (lastPartlUrl === 'cabinet' && this.selfID === data.userId) {
			let parentEl = document.querySelector('.tasks_' + data.subType + '_item_count');

			if (parentEl) {
				let value = parentEl.firstChild.textContent;

				if (!data.onlyNew) {
					parentEl.firstChild.textContent = Number(value) + 1;
				}

				this._minorPlusOneNewInvite(parentEl);
			}
		}
	}

	// уменьшаем количество новых приглашений

	_update_new_invite_count_minus_one(data) {
		let mainMenuCabinetItem = document.querySelector('.cabinet_menu_user_item'),
			url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (mainMenuCabinetItem && this.selfID === data.userId) {
			this._minorMinusOneAmountNewSubscriber(mainMenuCabinetItem, 1);
		}

		if (lastPartlUrl === 'cabinet' && this.selfID === data.userId) {
			let parentEl = document.querySelector('.invites_' + data.type + '_item_count');

			if (parentEl) {
				this._minorMinusOneNewInvite(parentEl, 1);
			}
		}

		if (firstPartUrl === data.type && this.selfID === data.userId) {
			let parentEl = document.querySelector('.invitations_span_count');

			if (parentEl) {
				this._minorMinusOneNewInvite(parentEl, 1);
			}

		}
	}

	// обновляем список приглашений события
	
	_update_event_invitations_list_item(data) {
		let url = location.pathname.split('/'),
			firstPartUrl = url[1],
			lastPartlUrl = url[url.length - 1];

		if (firstPartUrl === 'event' &&
			(lastPartlUrl === 'invitations' ||
				lastPartlUrl === 'completed' ||
				lastPartlUrl === 'scheduled' ||
				lastPartlUrl === 'event')) {

			let parentEl = document.querySelector('.invitations_span_count'),
				value = parentEl.firstChild.textContent;console.log(data)
			parentEl.firstChild.textContent = Number(value) + 1;
			this._minorPlusOneNewInvite(parentEl);

		}

		if (firstPartUrl === 'event' && lastPartlUrl === 'invitations') {
			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let dataFetch = 'entity_id=' + data[data.type + '_id'] +
						'&invite_from_id=' + data.invite_from_id +
						'&type=' + data.type +
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
					
					fetch('/commonEventList/getInviteListItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
			  			if (response.res) {
			  				let parentContainer = document.querySelector('.event_query_search'),
			  					existEl = document.querySelector('div[data-event_id="' + response.entity_id + '"]'),
			  					noEventsTitleEl = parentContainer.querySelector('.no_events_title');

			  				if (existEl) {
			  					existEl.remove();
			  				}

			  				if (noEventsTitleEl) {
			  					noEventsTitleEl.remove();
			  				}

			  				parentContainer.insertAdjacentHTML('afterbegin', response.html);
			  			} else {
			  				console.log(response.errors);
			  			}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	// помечаем входящее приглашение как принятое
	
	_setIncominInviteAsAccept(data) {
		let url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (data.statusNotViewedBeforeAction) {
			this._update_new_invite_count_minus_one(data);
		}

		if (firstPartUrl === data.type && lastPartlUrl === 'invitations' && this.selfID === data.userId) {
			let eventContainer = document.querySelector('.event_result_item[data-event_id="' + data.id + '"]');

			if (eventContainer) {
				let itemMenuContainer = eventContainer.querySelector('.event_item_menu ul'),
					guestStatusEl = eventContainer.querySelector('.invite_status_value');

				guestStatusEl.classList.remove('invite_status_viewed');
				guestStatusEl.classList.remove('invite_status_decline');
				guestStatusEl.classList.add(data.statusCssClass);
				guestStatusEl.innerHTML = data.statusValue;

				itemMenuContainer.innerHTML = '';
				itemMenuContainer.insertAdjacentHTML('afterBegin', data.listDeclineBut);
				eventContainer.classList.remove('not_viewed_invite');
			}

		}

		if (url === '/' + data.type + '/invitation/' + data.id) {
			let entityButtonContainer = document.querySelector('.entity_button_container'),
				inviteStatusValue = document.querySelector('.invite_status_value');

			entityButtonContainer.innerHTML = '';
			entityButtonContainer.insertAdjacentHTML('afterBegin', data.detailsDeclineBut);
			inviteStatusValue.classList.remove('invite_status_viewed');
			inviteStatusValue.classList.remove('invite_status_decline');
			inviteStatusValue.classList.add(data.statusCssClass);
			inviteStatusValue.innerHTML = data.statusValue;
		}
	}

	// помечаем входящее приглашение как отклоненное
	
	_setIncominInviteAsDecline(data) {
		let url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (data.statusNotViewedBeforeAction) {
			this._update_new_invite_count_minus_one(data);
		}

		if (firstPartUrl === data.type && lastPartlUrl === 'invitations' && this.selfID === data.userId) {
			let eventContainer = document.querySelector('.event_result_item[data-event_id="' + data.id + '"]');

			if (eventContainer) {
				let itemMenuContainer = eventContainer.querySelector('.event_item_menu ul'),
					guestStatusEl = eventContainer.querySelector('.invite_status_value');

				guestStatusEl.classList.remove('invite_status_viewed');
				guestStatusEl.classList.remove('invite_status_accept');
				guestStatusEl.classList.add(data.statusCssClass);
				guestStatusEl.innerHTML = data.statusValue;

				itemMenuContainer.innerHTML = '';
				itemMenuContainer.insertAdjacentHTML('afterBegin', data.listAcceptBut);
				eventContainer.classList.remove('not_viewed_invite');
			}

		}

		if (url === '/' + data.type + '/invitation/' + data.id) {
			let entityButtonContainer = document.querySelector('.entity_button_container'),
				inviteStatusValue = document.querySelector('.invite_status_value');

			entityButtonContainer.innerHTML = '';
			entityButtonContainer.insertAdjacentHTML('afterBegin', data.detailsAcceptBut);
			inviteStatusValue.classList.remove('invite_status_viewed');
			inviteStatusValue.classList.remove('invite_status_accept');
			inviteStatusValue.classList.add(data.statusCssClass);
			inviteStatusValue.innerHTML = data.statusValue;
		}
	}

	// помечаем входящее приглашение как просмотренное
	
	_setIncominInviteAsView(data) {
		let mainMenuCabinetItem = document.querySelector('.cabinet_menu_user_item'),
			url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		this._update_new_invite_count_minus_one(data);

		if (firstPartUrl === data.type && lastPartlUrl === 'invitations' && this.selfID === data.userId) {
			let eventContainer = document.querySelector('.event_result_item[data-event_id="' + data.id + '"]');

			if (eventContainer) {
				let guestStatusEl = eventContainer.querySelector('.invite_status_value');
				guestStatusEl.className = data.className;
				guestStatusEl.innerHTML = data.guestStatusValue;
				eventContainer.classList.remove('not_viewed_invite');
			}

		}
	}

	// меняем статус исходящего приглашения
	
	_changeOutgoingInviteStatus(data) {
		let url = location.pathname;

		if (url === '/' + data.type + /details/ + data.id && this.selfID === data.userId) {
			let userContainer = document.querySelector('.user_result_item[data-profile_id="' + data.inviteRecipientId + '"]');

			if (userContainer) {
				let guestStatusEl = userContainer.querySelector('.guest_status');
				guestStatusEl.className = data.className;
				guestStatusEl.innerHTML = data.guestStatusValue;
			}
		}
	}

	// меняем статус задачи "просмотрено ли исполнителем"
	
	_change_outgoing_viewed_by_assigned_status(data) {
		let url = location.pathname;

		if (url === '/task/owner-details/' + data.id && this.selfID === data.userId) {
			let elValue = document.querySelector('.entity_data_row_view_by_assigned_status').
				querySelector('.entity_data_row_item_value');

			if (elValue) {
				elValue.className = 'entity_data_row_item_value ' + data.className;
				elValue.innerHTML = data.statusValue;
			}
		}
	}

	// обновляем список приглашений встречи
	
	_update_meeting_invitations_list_item(data) {
		let url = location.pathname.split('/'),
			firstPartUrl = url[1],
			lastPartlUrl = url[url.length - 1];

		if (firstPartUrl === 'meeting' &&
			(lastPartlUrl === 'invitations' ||
				lastPartlUrl === 'completed' ||
				lastPartlUrl === 'scheduled' ||
				lastPartlUrl === 'meeting')) {

			let parentEl = document.querySelector('.invitations_span_count'),
				value = parentEl.firstChild.textContent;
			parentEl.firstChild.textContent = Number(value) + 1;
			this._minorPlusOneNewInvite(parentEl);
		}

		if (firstPartUrl === 'meeting' && lastPartlUrl === 'invitations') {
			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let dataFetch = 'entity_id=' + data[data.type + '_id'] +
						'&invite_from_id=' + data.invite_from_id +
						'&type=' + data.type +
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
					
					fetch('/commonEventList/getInviteListItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
			  			if (response.res) {
			  				let parentContainer = document.querySelector('.event_query_search'),
			  					existEl = document.querySelector('div[data-meeting_id="' + response.entity_id + '"]'),
			  					noEventsTitleEl = parentContainer.querySelector('.no_events_title');

			  				if (existEl) {
			  					existEl.remove();
			  				}

			  				if (noEventsTitleEl) {
			  					noEventsTitleEl.remove();
			  				}

			  				parentContainer.insertAdjacentHTML('afterbegin', response.html);
			  			} else {
			  				console.log(response.errors);
			  			}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	// обновляем список новых задач
	
	_update_task_list_item(data) {
		let url = location.pathname.split('/'),
			firstPartUrl = url[1],
			lastPartlUrl = url[url.length - 1];

		if (firstPartUrl === 'task' &&
			(lastPartlUrl === 'overdue' ||
				lastPartlUrl === 'completed' ||
				lastPartlUrl === 'scheduled' ||
				lastPartlUrl === 'created' ||
				lastPartlUrl === 'task')) {

			let parentEl = document.querySelector('.' + data.subType + '_task_span_count'),
				value = parentEl.firstChild.textContent;
			parentEl.firstChild.textContent = Number(value) + 1;
			this._minorPlusOneNewInvite(parentEl);
		}

		let subType = data.subType;

		if (subType === 'current') {
			subType = 'task';
		}

		if (firstPartUrl === 'task' && lastPartlUrl === subType) {
			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let dataFetch = 'task_id=' + data.task_id +
						'&subType=' + data.subType + 
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
					
					fetch('/task/getTaskListItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
			  			if (response.res) {
			  				let parentContainer = document.querySelector('.event_query_search'),
			  					existEl = document.querySelector('div[data-task_id="' + response.entity_id + '"]'),
			  					noEventsTitleEl = parentContainer.querySelector('.no_events_title');

			  				if (existEl) {
			  					existEl.remove();
			  				}

			  				if (noEventsTitleEl) {
			  					noEventsTitleEl.remove();
			  				}

			  				parentContainer.insertAdjacentHTML('afterbegin', response.html);
			  			} else {
			  				console.log(response.errors);
			  			}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	// установка статуса задачи - завершена для создателя задачи
	
	_set_task_complete_status_for_owner(data) {
		let url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (firstPartUrl === 'task' &&
			(lastPartlUrl === 'overdue' ||
				lastPartlUrl === 'completed' ||
				lastPartlUrl === 'scheduled' ||
				lastPartlUrl === 'created' ||
				lastPartlUrl === 'task') && data.scenario === 'create') {

			let parentEl = document.querySelector('.' + data.subType + '_task_span_count'),
				value = parentEl.firstChild.textContent;

			if (!data.onlyNew) {
				parentEl.firstChild.textContent = Number(value) + 1;
			}

			this._minorPlusOneNewInvite(parentEl);
		}

		if (firstPartUrl === 'task' && lastPartlUrl === data.subType &&
			data.scenario === 'create') {
			let listContainer = document.querySelector('.event_query_search'),
				notViewedItemList = listContainer.querySelectorAll('.not_viewed_invite'),
				lastNotViewedItem = '',
				existItem = listContainer.querySelector('.event_result_item[data-task_id="' + data.taskId + '"]');

			if (notViewedItemList.length > 0) {
				lastNotViewedItem = notViewedItemList[notViewedItemList.length - 1];
			}

			if (!lastNotViewedItem || existItem || (lastNotViewedItem && !existItem &&
				Number(listContainer.dataset.itemcount) > Number(lastNotViewedItem.dataset.rownum) &&
				Number(lastNotViewedItem.dataset.rownum) < listContainer.children.length) ||
				(lastNotViewedItem && !existItem && Number(listContainer.dataset.itemcount) > Number(lastNotViewedItem.dataset.rownum) &&
				lastNotViewedItem === listContainer.children[listContainer.children.length - 1] &&
				Number(lastNotViewedItem.dataset.date_timestamp) < Number(data.dateTimestamp))) {

				tab.checkToken(function() {
					tab.xjscsrf(function() {
						let dataFetch = 'task_id=' + data.taskId +
							'&subType=' + data.subType + 
							'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
							'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
						
						fetch('/task/getTaskListItem', {
							method: 'POST',
							body: dataFetch,
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'X-Requested-With': 'XMLHttpRequest',
							}
						})
						.then(response => response.json())
						.then(response => {
							document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
				  			if (response.res) {
				  				let parentContainer = document.querySelector('.event_query_search');

				  				if (existItem) {
				  					existItem.remove();
				  				}

				  				if (!lastNotViewedItem) {
				  					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				  				} else {
				  					let afterElList = [];

				  					for (let el of notViewedItemList) {
				  						if (Number(el.dataset.date_timestamp) < Number(data.dateTimestamp)) {
				  							afterElList.push(el);
				  						}
				  					}

				  					if (afterElList.length > 0) {
				  						afterElList.sort(function(a, b) {
											return a.getAttribute("data-rownum") - b.getAttribute("data-rownum");
										});

										afterElList[0].insertAdjacentHTML('beforebegin', response.html);
				  					} else {
				  						if (lastNotViewedItem !== parentContainer.children[parentContainer.children.length - 1]) {
				  							lastNotViewedItem.insertAdjacentHTML('afterend', response.html);
				  						}
				  					}
				  				}

				  				let rowNum = 1;

				  				for (let el of parentContainer.children) {
				  					el.dataset.rownum = rowNum;
				  					rowNum++;
				  				}
				  			} else {
				  				console.log(response.errors);
				  			}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			  		});
				});
			}
		}

		if (url === '/task/history/' + data.taskId && this.selfID === data.userId) {
			let fetchData = {
				'task_id': data.taskId,
				'task_history_id': data.task_history_id,
				'scenario': data.scenario,
				'type': 'owner',
			};

			fetchGetTaskHistoryListItem(fetchData, function(response) {
				let parentContainer = document.querySelector('.task_history_query_search');

				if (response.scenario === 'create') {
					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				}

				if (response.scenario === 'edit') {
					let existEl = parentContainer.querySelector('.task_history_item_container[data-task_history_id="' + response.task_history_id + '"]');
					
					if (existEl) {
						existEl.innerHTML = response.html;
					}
				}
			});
		}

		if (url === '/task/owner-details/' + data.taskId && this.selfID === data.userId) {
			let fetchData = {
				'task_id': data.taskId,
				'task_history_id': data.task_history_id,
				'scenario': data.scenario,
				'type': 'owner',
			};

			fetchGetTaskHistoryListItem(fetchData, function(response) {
				let parentContainer = document.querySelector('.task_history_content');

				if (response.scenario === 'create') {
					let entityButtonContainer = document.querySelector('.entity_button_container'),
						statusElValue = document.querySelector('.entity_data_row_status').querySelector('.entity_data_row_item_value'),
						reviewStatusElValue = document.querySelector('.entity_data_row_review_status').querySelector('.entity_data_row_item_value');
					entityButtonContainer.innerHTML = '';
					entityButtonContainer.insertAdjacentHTML('afterbegin', response.taskDetailsButton);
					statusElValue.className = 'entity_data_row_item_value' + response.statusDetailsData.statusCssClass;
					statusElValue.innerHTML = response.statusDetailsData.statusValue;
					reviewStatusElValue.className = 'entity_data_row_item_value' + response.statusDetailsData.reviewStatusCssClass;
					reviewStatusElValue.innerHTML = response.statusDetailsData.reviewStatusValue;
					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				}

				if (response.scenario === 'edit') {
					let existEl = parentContainer.querySelector('.task_history_item_container[data-task_history_id="' + response.task_history_id + '"]');
					
					if (existEl) {
						existEl.innerHTML = '';
						existEl.insertAdjacentHTML('afterbegin', response.html);
					}
				}
			});
		}
	}

	// установка статуса задачи - принята для исполнителя
	
	_set_task_accept_status_for_assigned(data) {
		let url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (firstPartUrl === 'task' && lastPartlUrl === data.subType &&
			data.scenario === 'create') {
			let listContainer = document.querySelector('.event_query_search'),
				existItem = listContainer.querySelector('.event_result_item[data-task_id="' + data.taskId + '"]');

			if (existItem) {
				let rowNum = existItem.dataset.rownum;

				tab.checkToken(function() {
					tab.xjscsrf(function() {
						let dataFetch = 'task_id=' + data.taskId +
							'&subType=' + data.subType + 
							'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
							'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
						
						fetch('/task/getTaskListItem', {
							method: 'POST',
							body: dataFetch,
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'X-Requested-With': 'XMLHttpRequest',
							}
						})
						.then(response => response.json())
						.then(response => {
							document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
				  			if (response.res) {
				  				if (existItem.previousElementSibling) {
				  					existItem.previousElementSibling.insertAdjacentHTML('afterend', response.html);
				  				} else if (existItem.nextElementSibling) {
				  					existItem.nextElementSibling.insertAdjacentHTML('beforebegin', response.html);
				  				} else if (!existItem.previousElementSibling && !existItem.nextElementSibling) {
				  					listContainer.insertAdjacentHTML('afterbegin', response.html);
				  				}

				  				existItem.remove();

				  				let newItem = listContainer.querySelector('.event_result_item[data-task_id="' + data.taskId + '"]');
				  				newItem.setAttribute('data-rowNum', rowNum);
				  			} else {
				  				console.log(response.errors);
				  			}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			  		});
				});
			}
		}

		if (url === '/task/history/' + data.taskId && this.selfID === data.userId) {
			let fetchData = {
				'task_id': data.taskId,
				'task_history_id': data.task_history_id,
				'scenario': data.scenario,
				'type': 'assigned',
			};

			fetchGetTaskHistoryListItem(fetchData, function(response) {
				let parentContainer = document.querySelector('.task_history_query_search');

				if (response.scenario === 'create') {
					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				}

				if (response.scenario === 'edit') {
					let existEl = parentContainer.querySelector('.task_history_item_container[data-task_history_id="' + response.task_history_id + '"]');
					
					if (existEl) {
						existEl.innerHTML = response.html;
					}
				}
			});
		}

		if (url === '/task/assigned-details/' + data.taskId && this.selfID === data.userId) {
			let fetchData = {
				'task_id': data.taskId,
				'task_history_id': data.task_history_id,
				'scenario': data.scenario,
				'type': 'assigned',
			};

			fetchGetTaskHistoryListItem(fetchData, function(response) {
				let parentContainer = document.querySelector('.task_history_content');

				if (response.scenario === 'create') {
					let entityButtonContainer = document.querySelector('.entity_button_container'),
						statusElValue = document.querySelector('.entity_data_row_review_status').querySelector('.entity_data_row_item_value');
					entityButtonContainer.innerHTML = '';
					entityButtonContainer.insertAdjacentHTML('afterbegin', response.taskDetailsButton);
					statusElValue.className = 'entity_data_row_item_value' + response.statusDetailsData.reviewStatusCssClass;
					statusElValue.innerHTML = response.statusDetailsData.reviewStatusValue;
					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				}

				if (response.scenario === 'edit') {
					let existEl = parentContainer.querySelector('.task_history_item_container[data-task_history_id="' + response.task_history_id + '"]');
					
					if (existEl) {
						existEl.innerHTML = '';
						existEl.insertAdjacentHTML('afterbegin', response.html);
					}
				}
			});
		}
	}

	// установка статуса задачи - отклонена для исполнителя задачи

	_set_task_reject_status_for_assigned(data) {
		let url = location.pathname,
			urlArr = url.split('/'),
			firstPartUrl = urlArr[1],
			lastPartlUrl = urlArr[urlArr.length - 1];

		if (firstPartUrl === 'task' &&
			(lastPartlUrl === 'overdue' ||
				lastPartlUrl === 'completed' ||
				lastPartlUrl === 'scheduled' ||
				lastPartlUrl === 'created' ||
				lastPartlUrl === 'task') && data.scenario === 'create') {

			let parentEl = document.querySelector('.' + data.subType + '_task_span_count'),
				value = parentEl.firstChild.textContent,
				completedParentEl = document.querySelector('.completed_task_span_count'),
				completedValue = completedParentEl.firstChild.textContent;

			if (!data.onlyNew) {
				parentEl.firstChild.textContent = Number(value) + 1;
			}

			completedParentEl.firstChild.textContent = Number(completedValue) - 1;
			this._minorPlusOneNewInvite(parentEl);
		}

		if (firstPartUrl === 'task' && lastPartlUrl === 'completed' &&
			data.scenario === 'create') {
			let listContainer = document.querySelector('.event_query_search'),
				existItem = listContainer.querySelector('.event_result_item[data-task_id="' + data.taskId + '"]');

			if (existItem) {
				existItem.remove();
				listContainer.dataset.itemcount = Number(listContainer.dataset.itemcount) - 1;

				let rowNum = 1;

  				for (let el of listContainer.children) {
  					el.dataset.rownum = rowNum;
  					rowNum++;
  				}
			}
		}

		if (lastPartlUrl === 'cabinet' && this.selfID === data.userId &&
			data.scenario === 'create') {
			let parentEl = document.querySelector('.tasks_completed_item_count');

			if (parentEl) {
				let value = parentEl.firstChild.textContent;

				if (!data.onlyNew) {
					parentEl.firstChild.textContent = Number(value) - 1;
				}
			}
		}

		if (firstPartUrl === 'task' && lastPartlUrl === data.subType &&
			data.scenario === 'create') {
			let listContainer = document.querySelector('.event_query_search'),
				notViewedItemList = listContainer.querySelectorAll('.not_viewed_invite'),
				lastNotViewedItem = '';

			if (notViewedItemList.length > 0) {
				lastNotViewedItem = notViewedItemList[notViewedItemList.length - 1];
			}

			if (!lastNotViewedItem || (lastNotViewedItem &&
				Number(listContainer.dataset.itemcount) > Number(lastNotViewedItem.dataset.rownum) &&
				Number(lastNotViewedItem.dataset.rownum) < listContainer.children.length) ||
				(lastNotViewedItem && Number(listContainer.dataset.itemcount) > Number(lastNotViewedItem.dataset.rownum) &&
				lastNotViewedItem === listContainer.children[listContainer.children.length - 1] &&
				Number(lastNotViewedItem.dataset.date_timestamp) < Number(data.dateTimestamp))) {

				tab.checkToken(function() {
					tab.xjscsrf(function() {
						let dataFetch = 'task_id=' + data.taskId +
							'&subType=' + data.subType + 
							'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
							'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
						
						fetch('/task/getTaskListItem', {
							method: 'POST',
							body: dataFetch,
							headers: {
								'Content-Type': 'application/x-www-form-urlencoded',
								'X-Requested-With': 'XMLHttpRequest',
							}
						})
						.then(response => response.json())
						.then(response => {
							document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
				  			if (response.res) {
				  				let parentContainer = document.querySelector('.event_query_search'),
				  					noEventsTitleEl = parentContainer.querySelector('.no_events_title');

				  				if (!lastNotViewedItem) {
				  					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				  				} else {
				  					let afterElList = [];

				  					for (let el of notViewedItemList) {
				  						if (Number(el.dataset.date_timestamp) < Number(data.dateTimestamp)) {
				  							afterElList.push(el);
				  						}
				  					}

				  					if (afterElList.length > 0) {
				  						afterElList.sort(function(a, b) {
											return a.getAttribute("data-rownum") - b.getAttribute("data-rownum");
										});

										afterElList[0].insertAdjacentHTML('beforebegin', response.html);
				  					} else {
				  						if (lastNotViewedItem !== parentContainer.children[parentContainer.children.length - 1]) {
				  							lastNotViewedItem.insertAdjacentHTML('afterend', response.html);
				  						}
				  					}
				  				}

				  				if (noEventsTitleEl) {
				  					noEventsTitleEl.remove();
				  				}

				  				parentContainer.dataset.itemcount = Number(parentContainer.dataset.itemcount) - 1;
				  				let rowNum = 1;

				  				for (let el of parentContainer.children) {
				  					el.dataset.rownum = rowNum;
				  					rowNum++;
				  				}
				  			} else {
				  				console.log(response.errors);
				  			}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			  		});
				});
			}
		}

		if (url === '/task/history/' + data.taskId && this.selfID === data.userId) {
			let fetchData = {
				'task_id': data.taskId,
				'task_history_id': data.task_history_id,
				'scenario': data.scenario,
				'type': 'assigned',
			};

			fetchGetTaskHistoryListItem(fetchData, function(response) {
				let parentContainer = document.querySelector('.task_history_query_search');

				if (response.scenario === 'create') {
					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				}

				if (response.scenario === 'edit') {
					let existEl = parentContainer.querySelector('.task_history_item_container[data-task_history_id="' + response.task_history_id + '"]');
					
					if (existEl) {
						existEl.innerHTML = response.html;
					}
				}
			});
		}

		if (url === '/task/assigned-details/' + data.taskId && this.selfID === data.userId) {
			let fetchData = {
				'task_id': data.taskId,
				'task_history_id': data.task_history_id,
				'scenario': data.scenario,
				'type': 'assigned',
			};

			fetchGetTaskHistoryListItem(fetchData, function(response) {
				let parentContainer = document.querySelector('.task_history_content');

				if (response.scenario === 'create') {
					let entityButtonContainer = document.querySelector('.entity_button_container'),
						reviewStatusElValue = document.querySelector('.entity_data_row_review_status').querySelector('.entity_data_row_item_value'),
						statusElValue = document.querySelector('.entity_data_row_status').querySelector('.entity_data_row_item_value');
					entityButtonContainer.innerHTML = '';
					entityButtonContainer.insertAdjacentHTML('afterbegin', response.taskDetailsButton);
					reviewStatusElValue.className = 'entity_data_row_item_value' + response.statusDetailsData.reviewStatusCssClass;
					reviewStatusElValue.innerHTML = response.statusDetailsData.reviewStatusValue;
					statusElValue.className = 'entity_data_row_item_value' + response.statusDetailsData.statusCssClass;
					statusElValue.innerHTML = response.statusDetailsData.statusValue;
					parentContainer.insertAdjacentHTML('afterbegin', response.html);
				}

				if (response.scenario === 'edit') {
					let existEl = parentContainer.querySelector('.task_history_item_container[data-task_history_id="' + response.task_history_id + '"]');
					
					if (existEl) {
						existEl.innerHTML = '';
						existEl.insertAdjacentHTML('afterbegin', response.html);
					}
				}
			});
		}
	}

	//обновляем количество новых соощений в хеадере

	_update_new_message_count_plus_one(data) {
		let mainMenuMessagesItem = document.querySelector('.messages_menu_user_item');

		if (mainMenuMessagesItem && this.selfID === data.userId) {
			this._minorPlusOneAmountNewSubscriber(mainMenuMessagesItem);
		}
	}

	_update_new_message_count_minus_count(data) {
		let mainMenuMessagesItem = document.querySelector('.messages_menu_user_item');

		if (mainMenuMessagesItem && this.selfID === data.userId) {
			this._minorMinusOneAmountNewSubscriber(mainMenuMessagesItem, data.count);
		}
	}

	//обновляем детали чата

	_update_chat_message_details_item(data) {
		let url = location.pathname.split('/');

		if (url[2] && url[2] === 'cr' && url[3] && Number(url[3]) === Number(data.chat_id)) {
			if (document.querySelector('div[data-chat_message_id="' + data.chat_message_id + '"]')) {
				return;
			}

			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
						dataFetch = 'chat_id=' + data.chat_id +
						'&chat_message_id=' + data.chat_message_id +
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + csrfVal.getAttribute('content');

					fetch('/message/getChatMessageDetailsItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				let parentContainer = document.querySelector('.message_query_search'),
		  					messagesBlock = parentContainer.parentElement;

		  				if (parentContainer) {
		  					parentContainer.insertAdjacentHTML('beforeend', response.html);
		  					parentContainer.parentElement.scrollTo(0, parentContainer.parentElement.scrollHeight);
		  				}

		  				let newMediaElList = parentContainer.querySelectorAll('div[media_init="0"]');

		  				for (let mediaEl of newMediaElList) {
		  					if (Number(mediaEl.getAttribute('data-file_type')) === 2) {
		  						new AudioFileExistElement(mediaEl);
		  					}

		  					if (Number(mediaEl.getAttribute('data-file_type')) === 3) {
		  						new VideoFileExistElement(mediaEl);
		  					}
		  				}

		  				if (messagesBlock.scrollHeight <= messagesBlock.offsetHeight) {
		  					markMessageIdList.push(data.chat_message_id);
		  					fetchMarkChatMessageAsRead(data.chat_id);
		  				}

		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	//обновляем детали диалога

	_update_dialog_message_details_item(data) {
		let url = location.pathname.split('/'),
			withUserId = (this.selfID === data.user_to) ? data.user_from : data.user_to;

		if (url[2] && url[2] === 'd' && url[3] && Number(url[3]) === Number(withUserId)) {
			if (document.querySelector('div[data-message_id="' + data.message_id + '"]')) {
				return;
			}

			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
						dataFetch = 'message_id=' + data.message_id +
						'&user_to=' + data.user_to +
						'&user_from=' + data.user_from +
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + csrfVal.getAttribute('content');

					fetch('/message/getDialogMessageDetailsItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				let parentContainer = document.querySelector('.message_query_search'),
		  					messagesBlock = parentContainer.parentElement;

		  				if (parentContainer) {
		  					parentContainer.insertAdjacentHTML('beforeend', response.html);
		  					parentContainer.parentElement.scrollTo(0, parentContainer.parentElement.scrollHeight);
		  				}

		  				let newMediaElList = parentContainer.querySelectorAll('div[media_init="0"]');

		  				for (let mediaEl of newMediaElList) {
		  					if (Number(mediaEl.getAttribute('data-file_type')) === 2) {
		  						new AudioFileExistElement(mediaEl);
		  					}

		  					if (Number(mediaEl.getAttribute('data-file_type')) === 3) {
		  						new VideoFileExistElement(mediaEl);
		  					}
		  				}

		  				if (messagesBlock.scrollHeight <= messagesBlock.offsetHeight) {
		  					markMessageIdList.push(data.message_id);
		  					fetchMarkDialogMessageAsRead(withUserId);
		  				}

		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	//обновляем список сообщений (чат)

	_update_chat_message_list_item(data) {
		let url = location.pathname.split('/'),
			lastPartlUrl = url[url.length - 1];

		if (lastPartlUrl === 'messages') {
			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let dataFetch = 'chat_id=' + data.chat_id +
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
					
					fetch('/message/getChatMessageListItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				let parentContainer = document.querySelector('.messages_block'),
		  					existEl = document.querySelector('div[data-chat_id="' + response.chatId + '"]');

		  				if (existEl) {
		  					existEl.remove();
		  				}

		  				parentContainer.insertAdjacentHTML('afterbegin', response.html);
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	//обновляем список сообщений (диалог)
	
	_update_dialog_message_list_item(data) {
		let url = location.pathname.split('/'),
			lastPartlUrl = url[url.length - 1],
			withUserId = (this.selfID === data.user_to) ? data.user_from : data.user_to;

		if (lastPartlUrl === 'messages') {
			tab.checkToken(function() {
				tab.xjscsrf(function() {
					let dataFetch = 'user_from=' + data.user_from +
						'&user_to=' + data.user_to +
						'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
						'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
					
					fetch('/message/getDialogMessageListItem', {
						method: 'POST',
						body: dataFetch,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
					.then(response => response.json())
					.then(response => {
						document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				let parentContainer = document.querySelector('.messages_block'),
		  					existEl = document.querySelector('.dialog_chat_item[data-profile_id="' + withUserId + '"]');

		  				if (existEl) {
		  					existEl.remove();
		  				}

		  				parentContainer.insertAdjacentHTML('afterbegin', response.html);
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		  		});
			});
		}
	}

	//обновляем количество пользователей которые просмотрели сообщение чата

	_update_view_chat_message_count(data) {
		let url = location.pathname.split('/');

		if (url[2] && url[2] === 'cr' && url[3] && Number(url[3]) === Number(data.chat_id)) {
			for (let i = 0; i < data.messageDataList.length; i++) {
				let messageContainer = document.querySelector('.message_item_box[data-chat_message_id="' + data.messageDataList[i].id + '"]');

				if (!messageContainer) {
					continue;
				}

				let viewStatisticEl = messageContainer.querySelector('.view_statistic_count');
				viewStatisticEl.innerHTML = data.messageDataList[i].viewCount;
			}
		}
	}

	//обновляем реакции на сообщение в чате
	
	_update_chat_message_reaction(data) {
		let url = location.pathname.split('/');

		if (url[2] && url[2] === 'cr' && url[3] && Number(url[3]) === Number(data.chat_id)) {
			let messageContainer = document.querySelector('.message_item_box[data-chat_message_id="' + data.chat_message_id + '"]');

			if (!messageContainer) {
				return;
			}

			messageContainer.querySelector('.reaction_message_block').innerHTML = data.reactionHtml;

			// let viewStatisticEl = messageContainer.querySelector('.view_statistic_count');
			// viewStatisticEl.innerHTML = data.messageDataList[i].viewCount;
		}
	}

	// обновляем количество новых сообщений на элементе переписки
	
	_update_correspondence_item_new_message_count(data) {
		let url = location.pathname.split('/'),
			lastPartlUrl = url[url.length - 1];

		if (lastPartlUrl === 'messages') {
			if (data.type === 'dialog') {
				let container = document.querySelector('.dialog_chat_item[data-profile_id="' + data.dataElId + '"]');

				if (container) {
					let textBox = container.querySelector('.text_box_dialog_chat'),
						newMessagesCountEl = document.querySelector('.new_messages_count');

					if (newMessagesCountEl) {
						let newMessageCount = newMessagesCountEl.innerHTML.replace('+', ''),
							currentMessageCount = Number(newMessageCount) - Number(data.countNewMessage);

						if (currentMessageCount <= 0) {
							newMessagesCountEl.remove();
							textBox.classList.remove('dialog_chat_item_not_read_message');
						} else {
							newMessagesCountEl.innerHTML = '+' + currentMessageCount;
						}
					}
				}
			}

			if (data.type === 'chat') {
				let container = document.querySelector('.dialog_chat_item[data-chat_id="' + data.dataElId + '"]');

				if (container) {
					let textBox = container.querySelector('.text_box_dialog_chat'),
						newMessagesCountEl = document.querySelector('.new_messages_count');

					if (newMessagesCountEl) {
						let newMessageCount = newMessagesCountEl.innerHTML.replace('+', ''),
							currentMessageCount = Number(newMessageCount) - Number(data.countNewMessage);

						if (currentMessageCount <= 0) {
							newMessagesCountEl.remove();
							textBox.classList.remove('dialog_chat_item_not_read_message');
						} else {
							newMessagesCountEl.innerHTML = '+' + currentMessageCount;
						}
					}
				}
			}
		}
	}

	//помечаем сообщение как прочитанное для отправителя (диалог)
	
	_mark_message_as_read_for_sender(data) {
		let url = location.pathname.split('/'),
			lastPartlUrl = url[url.length - 1];

		if (url[2] && url[2] === 'd' && url[3] && Number(url[3]) === Number(data.dialogId)) {
			for (let i = 0; i < data.messageDataList.length; i++) {
				let messageContainer = document.querySelector('.message_item_box[data-message_id="' + data.messageDataList[i].id + '"]');

				// if () {
					messageContainer.classList.remove('self_message_not_read');
				// }
			}
		}

		if (lastPartlUrl === 'messages' && data.newMessageForSenderCount <= 0) {
			let messageContainer = document.querySelector('.dialog_chat_item[data-profile_id="' + data.dialogId + '"]');

			if (messageContainer) {
				messageContainer.querySelector('.text_box_dialog_chat').classList.remove('dialog_chat_item_not_read_message');
			}
		}
	}

	//посылаем сообщение пользователю
	
	_send_message_to_user(data) {
		
		let url = location.pathname.split('/'),
			lastPartlUrl = url[url.length - 1],
			mainMenuMessagesItem = document.querySelector('.messages_menu_user_item');
			
		if (mainMenuMessagesItem && this.selfID === data['toUserID']) {
			this._minorPlusOneAmountNewSubscriber(mainMenuMessagesItem)
		}
		
		if (lastPartlUrl === 'd' + data['toUserID'] && this.selfID === data['fromUserID']) {
			this._minorAddMessageOnDialogOrChatPage(data['fromUserID'], data['ava_from'], data['user_from_fullname'], data['date'], data['time'], data['message_text'], data['file_list'], data['important'], data['messages_id'], true);
			this._scrollDownTheBlockWithMessages();
		} else if (lastPartlUrl === 'd' + data['fromUserID'] && this.selfID === data['toUserID']) {
			this._minorAddMessageOnDialogOrChatPage(data['fromUserID'], data['ava_from'], data['user_from_fullname'], data['date'], data['time'], data['message_text'], data['file_list'], data['important'], data['messages_id'], false);
			data = {
				'ws_tab_sync_to_do': 'mark_messages_as_read',
				'actionDB': 'markIncomingMessagesFromUserAsRead',
				'fromUserID': data['toUserID'],
				'toUserID': data['fromUserID'],
				'send_one_time': true
			};
			tab.send(tab.tabSync.collectData(data));
			this._scrollDownTheBlockWithMessages();
		} else if (lastPartlUrl === 'messages' && this.selfID === data['toUserID']) {
			this._minorUpdateOrAddCorrespondence(data['fromUserID'], data['toUserID'], data['ava_from'], data['ava_to'], data['user_from_fullname'], data['user_to_fullname'], data['date'], data['time'], data['message_text'], data['file_list'], false, true);
		} else if (lastPartlUrl === 'messages' && this.selfID === data['fromUserID']) {
			this._minorUpdateOrAddCorrespondence(data['fromUserID'], data['toUserID'], data['ava_from'], data['ava_to'], data['user_from_fullname'], data['user_to_fullname'], data['date'], data['time'], data['message_text'], data['file_list'], true, false);
		}
		
	}
	
	//второстепенные методы для отправки сообщения пользователю
	
	_scrollDownTheBlockWithMessages() {
		let block = document.querySelector('.messages_block_with_text_field');

		if (block) {
			let scrollBlock = block.scrollHeight - block.getBoundingClientRect().height;
			block.scrollTop = scrollBlock + 1000;
		}
	}
	
	_minorAddMessageOnDialogOrChatPage(userFrom, userAva, userFullname, date, time, text, fileList, important, messageId, selfMessageNotread, rowNum, actInsert) {
		let parentEl = document.querySelector('.messages_block_with_text_field'),
			emptyTitle = document.querySelector('.empty_title');

		if (!parentEl && emptyTitle) {
			parentEl = document.querySelector('.messages_block');
			let writeMessageBlock = document.querySelector('.write_message_box');

			emptyTitle.remove();
			parentEl.classList.add('messages_block_with_text_field');
			writeMessageBlock.classList.remove('not_active');
		}

		let item = document.createElement('div'),
			messageTitle = document.createElement('div'),
			linkAva = document.createElement('a'),
			linkUserName = document.createElement('a'),
			avaBox = document.createElement('div'),
			avaEl = document.createElement('img'),
			userNameTimeBox = document.createElement('div'),
			userNameEl = document.createElement('span'),
			timeEl = document.createElement('span'),
			messageBox = document.createElement('div'),
			menuContainer = document.createElement('div'),
			menuIcon = document.createElement('img');

		item.setAttribute('data-profile_id', userFrom);
		item.setAttribute('data-important_message', important);
		item.setAttribute('data-message_id', messageId);
		item.classList.add('message_item_box');
		linkAva.setAttribute('href', '/profile/id' + userFrom);
		linkUserName.setAttribute('href', '/profile/' + userFrom);
		messageTitle.classList.add('message_title');
		avaBox.classList.add('message_ava_box');
		avaEl.classList.add('message_ava');
		userNameTimeBox.classList.add('user_name_time_box');
		messageBox.classList.add('message_box');
		avaEl.setAttribute('src', userAva);
		userNameEl.innerHTML = userFullname;
		menuContainer.classList.add('menu_message_container');
		menuIcon.classList.add('menu_message_icon');
		menuIcon.setAttribute('src', '/img/icon_burger_message_menu.png');

		if (rowNum) {
			item.setAttribute('data-rowNum', rowNum);
		}
		
		if (selfMessageNotread) {
			item.classList.add('dialog_chat_item_not_read_message');
		}

		timeEl.innerHTML = date + ' ' + time;
		
		if (text) {
			var messageEl = document.createElement('div');
			messageEl.classList.add('message_text');
			text = text.replace(/\n/g, '<br>');
			messageEl.innerHTML = text;
		}

		if (fileList && fileList.length > 0) {
			var fileListContainer = document.createElement('div');
			fileListContainer.classList.add('massage_file_container');

			for (let i = 0; i < fileList.length; i++) {
				if (fileList[i] == 'notAllowed') {
					FileElement.showAccessDenied(fileListContainer);
				}

				if (Number(fileList[i]['type']) === 1) {
					new ImageFileElement(
						{
							'parentEl': fileListContainer,
							'type': 'inMessage',
						},
						{
							'fileId': fileList[i]['file_id'],
							'url': fileList[i]['url'],
							'privacyStatus': fileList[i]['privacy_status'],
							'selected': fileList[i]['selected'],
							'previewHeight': (fileList[i]['preview_height']) ? fileList[i]['preview_height'] : false,
							'previewWidth': (fileList[i]['preview_width']) ? fileList[i]['preview_width'] : false,
						}
					);
				} else if (Number(fileList[i]['type']) === 2) {
					new AudioFileElement(
						{
							'parentEl': fileListContainer,
							'type': 'inMessage',
						},
						{
							'fileId': fileList[i]['file_id'],
							'url': fileList[i]['url'],
							'privacyStatus': fileList[i]['privacy_status'],
							'selected': fileList[i]['selected'],
							'name': fileList[i]['name'],
						}
					);
				} else if (Number(fileList[i]['type']) === 3) {
					new VideoFileElement(
						{
							'parentEl': fileListContainer,
							'type': 'inMessage',
						},
						{
							'fileId': fileList[i]['file_id'],
							'url': fileList[i]['url'],
							'privacyStatus': fileList[i]['privacy_status'],
							'selected': fileList[i]['selected'],
							'name': fileList[i]['name'],
						}
					);
				} else if (Number(fileList[i]['type']) === 4) {
					new DocumentFileElement(
						{
							'parentEl': fileListContainer,
							'type': 'inMessage',
						},
						{
							'fileId': fileList[i]['file_id'],
							'url': fileList[i]['url'],
							'privacyStatus': fileList[i]['privacy_status'],
							'selected': fileList[i]['selected'],
							'name': fileList[i]['name'],
							'date': fileList[i]['date'],
						}
					);
				}
			}
		}

		this._createMessageMenuList(menuContainer, important);

		menuContainer.addEventListener('mouseover', function() {
			this.querySelector('.menu_message_list_container').classList.add('menu_box_active');
		});

		menuContainer.addEventListener('mouseout', function() {
			this.querySelector('.menu_message_list_container').classList.remove('menu_box_active');
		});
		
		menuContainer.append(menuIcon);
		linkAva.appendChild(avaEl);
		avaBox.appendChild(linkAva);
		messageTitle.appendChild(avaBox);
		linkUserName.appendChild(userNameEl);
		userNameTimeBox.appendChild(linkUserName);
		userNameTimeBox.appendChild(timeEl);
		messageTitle.appendChild(userNameTimeBox);
		messageTitle.append(menuContainer);

		if (text) {
			messageBox.appendChild(messageEl);
		}

		if (fileList && fileList.length > 0) {
			messageBox.appendChild(fileListContainer);
		}

		item.appendChild(messageTitle);
		item.appendChild(messageBox);
		
		if ((actInsert === 'append' || !actInsert) && parentEl) {
			parentEl.appendChild(item);
		} else if (actInsert === 'prepend' && parentEl) {
			parentEl.prepend(item);
		}
		
		if (important) {
			let imoprtantEl = TabSync._createImportantElement(item, menuContainer);
			menuContainer.insertAdjacentElement('afterend', imoprtantEl);
		}
	}

	_createMessageMenuList(parentEl, important) {
		let container = document.createElement('div'),
			list = document.createElement('ul'),
			importantItem = document.createElement('li'),
			deleteItem = document.createElement('li');

		container.classList.add('menu_message_list_container');
		list.classList.add('menu_message_list');
		importantItem.classList.add('menu_message_item');
		importantItem.classList.add('menu_message_important_item');
		deleteItem.classList.add('menu_message_item');
		deleteItem.classList.add('menu_message_delete_item');
		importantItem.innerHTML = (important) ? 'убрать из важных' : 'отметить важным';
		deleteItem.innerHTML = 'удалить ';

		importantItem.addEventListener('click', function() {
			let messageItemBox = parentEl.closest('.message_item_box'),
				important = messageItemBox.getAttribute('data-important_message'),
				messageId = messageItemBox.getAttribute('data-message_id');
			fetchToggleImportantMessage(important, messageId, function(result) {
				parentEl.querySelector('.menu_message_list_container').classList.remove('menu_box_active');

				if (result.res && result.important == 1) {
					let imoprtantEl = TabSync._createImportantElement();
					parentEl.insertAdjacentElement('afterend', imoprtantEl);
					importantItem.innerHTML = result.buttonText;
					messageItemBox.setAttribute('data-important_message', result.important);
				} else if (result.res && result.important == 0) {
					let existImportantEl = messageItemBox.querySelector('.important_message_container');

					if (existImportantEl) {
						existImportantEl.remove();
					}

					importantItem.innerHTML = result.buttonText;
					messageItemBox.setAttribute('data-important_message', result.important);
				}
			});
		});

		deleteItem.addEventListener('click', function() {
			let messageItem = this.closest('.message_item_box');
			fetchDeleteDialogMessage(messageItem.getAttribute('data-message_id'));
		});

		list.append(importantItem);
		list.append(deleteItem);
		container.append(list);
		parentEl.append(container);
	}

	static _createImportantElement() {
		let importantContainer = document.createElement('div'),
			importantIcon = document.createElement('img');

		importantContainer.classList.add('important_message_container');
		importantIcon.classList.add('important_message_icon');
		importantIcon.src = '/img/important_message_icon_exp.png';

		importantContainer.append(importantIcon);
		return importantContainer;
	}
	
	_minorUpdateOrAddCorrespondence(userFrom, userTo, avaFrom, avaTo, userFromFullname, userToFullname, date, time, text, fileList, selfMessageNotread, newMessage) {
		let itemList = document.querySelectorAll('.dialog_chat_item'),
			parentEl = document.querySelector('.messages_block'),
			idToSearch = userFrom;

		if (selfMessageNotread) idToSearch = userTo;
		
		if (itemList.length === 0) {
			this._minorAddCorespondence(parentEl, userFrom, userTo, avaFrom, avaTo, userFromFullname, userToFullname, date, time, text, fileList, selfMessageNotread, newMessage)
			return;
		}
		
		for (let elem of itemList) {
			if (elem.getAttribute('data-profile_id') == idToSearch) {
				this._minorUpdateCorespondence(userFrom, userTo, avaFrom, userFromFullname, date, time, text, fileList, elem, selfMessageNotread)
				break;
			} else if (elem.getAttribute('data-profile_id') !== idToSearch && elem === itemList[itemList.length - 1]) {
				this._minorAddCorespondence(parentEl, userFrom, userTo, avaFrom, avaTo, userFromFullname, userToFullname, date, time, text, fileList, selfMessageNotread, newMessage);
			}
		}
	}
	
	_minorUpdateCorespondence(userFrom, userTo, avaFrom, userFromFullname, date, time, text, fileList, elem, selfMessageNotread) {
		let textBox = elem.querySelector('.text_box_dialog_chat'),
			countNewMessagesEl = elem.querySelector('.new_messages_count'),
			countNewFileEl = elem.querySelector('.new_attachments_count'),
			dateBoxDialogChat = elem.querySelector('.date_box_dialog_chat'),
			textEl = elem.querySelector('.text_self_dialog_chat'),
			newCountContainer = elem.querySelector('.new_count_container'),
			textElParent = textEl.parentElement;

		text = text.replace(/\n/g, '<br>');
		textBox.classList.add('dialog_chat_item_not_read_message');
		textEl.innerHTML = text;
		dateBoxDialogChat.children[0].innerHTML = date;
		dateBoxDialogChat.children[1].innerHTML = time;

		if (countNewFileEl && fileList && fileList.length > 0) {
			let countNewFileEl = document.createElement('span'),
				br = document.createElement('br');
			countNewFileEl.classList.add('new_attachments_count');
			countNewFileEl.innerHTML = fileList[0].attachments + ': ' + fileList.length;
			textEl.prepend(br);
			textEl.prepend(countNewFileEl);
		}

		if (selfMessageNotread) {
			let avaSelf = elem.querySelector('.ava_self_message_dialog_chat');

			textElParent.classList.remove('.text_not_self_dialog_chat_container');
			textElParent.classList.add('.text_self_dialog_chat_container');

			if (!avaSelf) {
				let avaSelf = document.createElement('img');
				avaSelf.classList.add('ava_self_message_dialog_chat');
				avaSelf.setAttribute('src', avaFrom);
				textBox.prepend(avaSelf);
			}

		} else {
			textElParent.classList.remove('.text_self_dialog_chat_container');
			textElParent.classList.add('.text_not_self_dialog_chat_container');

			if (countNewMessagesEl) {
				let textInner = countNewMessagesEl.innerHTML,
					newTextInner = textInner.slice(0, 1) + (Number(textInner.slice(1)) + 1);
				countNewMessagesEl.innerHTML = newTextInner;
			} else {
				let countNewMessagesEl = document.createElement('span');
				countNewMessagesEl.innerHTML = '+1';
				countNewMessagesEl.classList.add('new_messages_count');
				newCountContainer.append(countNewMessagesEl);
			}
		}
	}
	
	_minorAddCorespondence(parentEl, userFrom, userTo, avaFrom, avaTo, userFromFullname, userToFullname, date, time, text, fileList, selfMessageNotread, newMessage) {
		let item = document.createElement('div'),
			avaBox = document.createElement('div'),
			ava = document.createElement('img'),
			userNameAndDateBox = document.createElement('div'),
			userNameBox = document.createElement('div'),
			firstNameEl = document.createElement('span'),
			lastNameEl = document.createElement('span'),
			dateBox = document.createElement('div'),
			dateEl = document.createElement('span'),
			timeEl = document.createElement('span'),
			textBox = document.createElement('div'),
			textEl = document.createElement('span'),
			deleteEl = document.createElement('span'),
			deletePic = document.createElement('img'),
			newCountContainer = document.createElement('div'),
			textElContainer = document.createElement('div');

			
		item.classList.add('dialog_chat_border');
		item.classList.add('dialog_chat_item');
		item.classList.add('input_search_item');
		deleteEl.classList.add('delete_correspondence');
		deleteEl.classList.add('not_active_opacity');
		deletePic.classList.add('delete_correspondence_pic');
		deletePic.src = '/img/close_.png';
		newCountContainer.classList.add('new_count_container');
		textEl.classList.add('text_self_dialog_chat');

		text = text.replace(/\n/g, '<br>');

		if (selfMessageNotread) {
			let avaSelf = document.createElement('img');
			avaSelf.classList.add('ava_self_message_dialog_chat');
			avaSelf.setAttribute('src', avaFrom);
			item.setAttribute('data-profile_id', userTo);
			ava.setAttribute('src', avaTo);
			firstNameEl.innerHTML = userToFullname.slice(0, userToFullname.indexOf(' '));
			lastNameEl.innerHTML = userToFullname.slice(userToFullname.indexOf(' '));
			// textBox.classList.add('dialog_chat_item_not_read_message');
			textElContainer.classList.add('text_self_dialog_chat_container');
			textBox.appendChild(avaSelf);
		} else {
			item.setAttribute('data-profile_id', userFrom);
			ava.setAttribute('src', avaFrom);
			firstNameEl.innerHTML = userFromFullname.slice(0, userFromFullname.indexOf(' '));
			lastNameEl.innerHTML = userFromFullname.slice(userFromFullname.indexOf(' '));
			textElContainer.classList.add('text_not_self_dialog_chat_container');
		}

		textElContainer.append(textEl);
		textEl.innerHTML = text;

		if (fileList && fileList.length > 0) {
			let newAttachmentsCount = document.createElement('span');
			newAttachmentsCount.classList.add('new_attachments_count');
			newAttachmentsCount.innerHTML = fileList[0].attachments + ': ' + fileList.length;
			textEl.prepend(newAttachmentsCount);
		}

		if (newMessage) {
			let newMessagesCountEl = document.createElement('span');
			newMessagesCountEl.classList.add('new_messages_count');
			newMessagesCountEl.innerHTML = '+' + 1;
			newCountContainer.append(newMessagesCountEl);
		}

		let existItemList = document.querySelectorAll('.dialog_chat_item');
		if (existItemList) {
			let emptyTitle = document.querySelector('.empty_title');

			if (emptyTitle) {
				emptyTitle.remove();
			}
		}

		deleteEl.append(deletePic);
		textBox.classList.add('dialog_chat_item_not_read_message');
		dateEl.innerHTML = date;
		timeEl.innerHTML = time;
		avaBox.classList.add('ava_box_dialog_chat');
		ava.classList.add('ava_dialog_chat');
		userNameAndDateBox.classList.add('user_name_and_date_box_dialog_chat');
		userNameBox.classList.add('user_name_box_dialog_chat');
		dateBox.classList.add('date_box_dialog_chat');
		avaBox.appendChild(ava);
		textBox.classList.add('text_box_dialog_chat');
		userNameBox.appendChild(firstNameEl);
		userNameBox.appendChild(lastNameEl);
		dateBox.appendChild(dateEl);
		dateBox.appendChild(timeEl);
		userNameAndDateBox.appendChild(userNameBox);
		userNameAndDateBox.appendChild(dateBox);
		textBox.appendChild(textElContainer);
		item.appendChild(deleteEl);
		item.appendChild(avaBox);
		item.appendChild(userNameAndDateBox);
		item.appendChild(textBox);
		item.append(newCountContainer);
		parentEl.prepend(item);

		// deleteEl.addEventListener('click', function() {
		// 	let dialogItem = this.closest('.dialog_chat_item'),
		// 		newItemCountEl = document.querySelector('.new_messages_count');
		// 	fetchDeleteDialogAllMessages(dialogItem.getAttribute('data-profile_id'));

		// 	if (newItemCountEl) {
		// 		let count = newItemCountEl.innerHTML.slice(1),
		// 			mainNewMessagesCountEl = document.querySelector('.messages_menu_user_item .new_subscriber_amount'),
		// 			mainNewMessagesCountElValue = mainNewMessagesCountEl.innerHTML.slice(1),
		// 			newMainNewMessagesCountElValue = Number(mainNewMessagesCountElValue) - count;

		// 		if (mainNewMessagesCountElValue <= 0) {
		// 			mainNewMessagesCountEl.remove();
		// 		} else {
		// 			mainNewMessagesCountEl.innerHTML = newMainNewMessagesCountElValue
		// 		}
				
		// 	}
		// });
		
		item.addEventListener('click', function(e) {
			if (this.hasAttribute('data-profile_id') && e.target !== document.querySelector('.delete_correspondence') &&
				e.target !== document.querySelector('.delete_correspondence_pic')) {
				let elOfUrl = location.href + '/d' + this.getAttribute('data-profile_id');
				history.pushState(null, null, elOfUrl);
				ajaxShowСorrespondence(parentEl, getLastPartOfURL());
			}
		});
	}

	//дружеское взаимодействие пользователей
	

	//подписаться (отправить запрос в друзья)
	
	_subscribe(data) {
		let url = location.pathname;

		if (url === '/friends-subscriptions') {
			this._add_user_item_in_list({
				'friendUserItem': data.htmlData.friendUserItem,
				'userId': data.userToId,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriptions = document.querySelector('.profile_submenu_button_subscriptions'),
				count = parseInt(submenuButtonSubscriptions.firstChild.textContent.match(/\d+/));

			count++;
			submenuButtonSubscriptions.firstChild.textContent = submenuButtonSubscriptions.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + count;
		}

		if (url === '/profile/' + data.userToId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}
	}

	//добавить нового подписчика
	
	_add_new_subscriber(data) {
		let url = location.pathname,
			mainMenuFriendsItem = document.querySelector('.friends_menu_user_item');

		if (mainMenuFriendsItem && this.selfID === data.userToId) {
			this._minorPlusOneAmountNewSubscriber(mainMenuFriendsItem);
		}

		if (url === '/friends-subscribers') {
			this._add_user_item_in_list({
				'friendUserItem': data.htmlData.friendUserItem,
				'userId': data.userFromId,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriber = document.querySelector('.profile_submenu_button_subscriber'),
				count = parseInt(submenuButtonSubscriber.firstChild.textContent.match(/\d+/));

			count++;
			submenuButtonSubscriber.firstChild.textContent = submenuButtonSubscriber.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + count;
			this._minorPlusOneNewInvite(submenuButtonSubscriber);
		}

		if (url === '/profile/' + data.userFromId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}
	}

	//отписаться (отменить запрос в друзья)
	
	_unsubscribe(data) {
		let url = location.pathname;

		if (url === '/friends-subscriptions') {
			this._delete_user_item_from_list({
				'userId': data.userToId,
				'nothingFound': data.htmlData.nothingFound,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriptions = document.querySelector('.profile_submenu_button_subscriptions'),
				count = parseInt(submenuButtonSubscriptions.firstChild.textContent.match(/\d+/));

			count--;
			submenuButtonSubscriptions.firstChild.textContent = submenuButtonSubscriptions.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + count;
		}

		if (url === '/profile/' + data.userToId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}
	}

	//удалить подписчика
	
	_delete_subscriber(data) {
		let url = location.pathname,
			mainMenuFriendsItem = document.querySelector('.friends_menu_user_item');

		if (mainMenuFriendsItem && this.selfID === data.userToId && data.newSubscriberAction === 'remove') {
			this._minorMinusOneAmountNewSubscriber(mainMenuFriendsItem, 1);
		}

		if (url === '/friends-subscribers') {
			this._delete_user_item_from_list({
				'userId': data.userFromId,
				'nothingFound': data.htmlData.nothingFound,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriber = document.querySelector('.profile_submenu_button_subscriber'),
				count = parseInt(submenuButtonSubscriber.firstChild.textContent.match(/\d+/));

			count--;
			submenuButtonSubscriber.firstChild.textContent = submenuButtonSubscriber.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + count;
			
			if (data.newSubscriberAction === 'remove') {
				this._minorMinusOneNewInvite(submenuButtonSubscriber, 1);
			}
		}

		if (url === '/profile/' + data.userFromId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}
	}

	//принять запрос на добавление в друзья
	
	_acceptFriendship(data) {
		let url = location.pathname,
			mainMenuFriendsItem = document.querySelector('.friends_menu_user_item');

		if (mainMenuFriendsItem && this.selfID === data.userFromId && data.newSubscriberAction === 'remove') {
			this._minorMinusOneAmountNewSubscriber(mainMenuFriendsItem, 1);
		}

		if (url === '/friends-subscribers') {
			this._delete_user_item_from_list({
				'userId': data.userToId,
				'nothingFound': data.htmlData.nothingFound,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriber = document.querySelector('.profile_submenu_button_subscriber'),
				subMenuButtonFriend = document.querySelector('.profile_submenu_button_friends'),
				countSubscriber = parseInt(submenuButtonSubscriber.firstChild.textContent.match(/\d+/)),
				countFriend = parseInt(subMenuButtonFriend.firstChild.textContent.match(/\d+/));

			countSubscriber--;
			countFriend++;
			submenuButtonSubscriber.firstChild.textContent = submenuButtonSubscriber.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countSubscriber;
			subMenuButtonFriend.firstChild.textContent = subMenuButtonFriend.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countFriend;
			
			if (data.newSubscriberAction === 'remove') {
				this._minorMinusOneNewInvite(submenuButtonSubscriber, 1);
			}
		}

		if (url === '/profile/' + data.userToId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}

		if (url === '/friends') {
			this._add_user_item_in_list({
				'friendUserItem': data.htmlData.friendUserItem,
				'userId': data.userToId,
			});
		}
	}

	//добавить пользователя в друзья из подписок
	
	_add_friend_from_subcriptions(data) {
		let url = location.pathname;

		if (url === '/friends') {
			this._add_user_item_in_list({
				'friendUserItem': data.htmlData.friendUserItem,
				'userId': data.userFromId,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscription = document.querySelector('.profile_submenu_button_subscriptions'),
				subMenuButtonFriend = document.querySelector('.profile_submenu_button_friends'),
				countSubscription = parseInt(submenuButtonSubscription.firstChild.textContent.match(/\d+/)),
				countFriend = parseInt(subMenuButtonFriend.firstChild.textContent.match(/\d+/));

			countSubscription--;
			countFriend++;
			submenuButtonSubscription.firstChild.textContent = submenuButtonSubscription.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countSubscription;
			subMenuButtonFriend.firstChild.textContent = subMenuButtonFriend.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countFriend;
		}

		if (url === '/profile/' + data.userFromId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}

		if (url === '/friends-subscriptions') {
			this._delete_user_item_from_list({
				'userId': data.userFromId,
				'nothingFound': data.htmlData.nothingFound,
			});
		}
	}

	//оставить пользователя в подписчиках
	
	_rejectFriendship(data) {
		let url = location.pathname,
			mainMenuFriendsItem = document.querySelector('.friends_menu_user_item');

		if (mainMenuFriendsItem && this.selfID === data.userFromId && data.newSubscriberAction === 'remove') {
			this._minorMinusOneAmountNewSubscriber(mainMenuFriendsItem, 1);
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriber = document.querySelector('.profile_submenu_button_subscriber');
			
			if (data.newSubscriberAction === 'remove') {
				this._minorMinusOneNewInvite(submenuButtonSubscriber, 1);
			}
		}

		if (url === '/friends-subscribers') {
			let userContainer = document.querySelector('.friend_result_item[data-profile_id="' + data.userToId + '"]'),
				exsistDataRowNum = (userContainer.hasAttribute('data-rowNum')) ? true : false;

			if (userContainer) {
				let parentElement = userContainer.closest('.friend_query_search'),
					parentLastChild = parentElement.children[parentElement.children.length - 1];

				userContainer.remove();

				let firstNotNewUserContainer = parentElement.querySelector('.friend_result_item:not(.new_subscriber_result_item)');

				if (firstNotNewUserContainer) {
					firstNotNewUserContainer.insertAdjacentHTML('beforebegin', data.htmlData.friendUserItem);
				} else {
					parentElement.insertAdjacentHTML('beforeend', data.htmlData.friendUserItem);
				}

				if (exsistDataRowNum) {
					let rowNum = 1;

					for (let el of parentElement.children) {
						el.dataset.rowNum = rowNum;
						rowNum++;
					}
				} else if (!exsistDataRowNum && !firstNotNewUserContainer) {
					let newUserItem = parentElement.children[parentElement.children.length - 1];

					if (parentLastChild) {
						newUserItem.setAttribute('data-rowNum', parentLastChild.dataset.rownum);
						parentLastChild.removeAttribute('data-rowNum');
					}
				}
			}
		}
	}

	//удалить пользователя из списка друзей

	_removeFriend(data) {
		let url = location.pathname;

		if (url === '/friends') {
			this._delete_user_item_from_list({
				'userId': data.userToId,
				'nothingFound': data.htmlData.nothingFound,
			});
		}

		if (url === '/friends-subscribers') {
			let parentElement = document.querySelector('.friend_query_search'),
				firstNotNewUserContainer = parentElement.querySelector('.friend_result_item:not(.new_subscriber_result_item)'),
				parentLastChild = parentElement.children[parentElement.children.length - 1];

			if (firstNotNewUserContainer) {
				firstNotNewUserContainer.insertAdjacentHTML('beforebegin', data.htmlData.friendUserItem);
			} else {
				parentElement.insertAdjacentHTML('beforeend', data.htmlData.friendUserItem);

				// if (parentLastChild) {
				// 	let newUserItem = parentElement.children[parentElement.children.length - 1];
				// 	newUserItem.setAttribute('data-rowNum', parentLastChild.dataset.rownum);
				// 	parentLastChild.removeAttribute('data-rowNum');
				// }
			}
		}

		if (url === '/profile/' + data.userToId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscriber = document.querySelector('.profile_submenu_button_subscriber'),
				subMenuButtonFriend = document.querySelector('.profile_submenu_button_friends'),
				countSubscriber = parseInt(submenuButtonSubscriber.firstChild.textContent.match(/\d+/)),
				countFriend = parseInt(subMenuButtonFriend.firstChild.textContent.match(/\d+/));

			countSubscriber++;
			countFriend--;
			submenuButtonSubscriber.firstChild.textContent = submenuButtonSubscriber.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countSubscriber;
			subMenuButtonFriend.firstChild.textContent = subMenuButtonFriend.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countFriend;
		}
	}

	//перенести пользователя из списка друзей в подписки
	
	_transfer_friend_to_subcriptions(data) {
		let url = location.pathname;

		if (url === '/friends') {
			this._delete_user_item_from_list({
				'userId': data.userFromId,
				'nothingFound': data.htmlData.nothingFound,
			});
		}

		if (url === '/friends-subscriptions') {
			this._add_user_item_in_list({
				'friendUserItem': data.htmlData.friendUserItem,
				'userId': data.userFromId,
			});
		}

		if (url === '/profile/' + data.userFromId) {
			this._update_friend_button_userProfile({
				'friendButtonUserProfile': data.htmlData.friendButtonUserProfile,
			});
		}

		if (url === '/friends' || url === '/friends-subscribers' || url === '/friends-subscriptions') {
			let submenuButtonSubscription = document.querySelector('.profile_submenu_button_subscriptions'),
				subMenuButtonFriend = document.querySelector('.profile_submenu_button_friends'),
				countSubscription = parseInt(submenuButtonSubscription.firstChild.textContent.match(/\d+/)),
				countFriend = parseInt(subMenuButtonFriend.firstChild.textContent.match(/\d+/));

			countSubscription++;
			countFriend--;
			submenuButtonSubscription.firstChild.textContent = submenuButtonSubscription.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countSubscription;
			subMenuButtonFriend.firstChild.textContent = subMenuButtonFriend.firstChild.textContent.replace(/[0-9]/g, '') + ' ' + countFriend;
		}
	}

	//добавить элемент html контейнер пользователя в соответствующий список

	_add_user_item_in_list(data) {
		let parentContainer = document.querySelector('.friend_query_search'),
			noEventsTitleEl = parentContainer.querySelector('.no_events_title');

		if (noEventsTitleEl) {
			noEventsTitleEl.remove();
		}

		if (!document.querySelector('.friend_result_item[data-profile_id="' + data.userId + '"]')) {
			parentContainer.insertAdjacentHTML('afterbegin', data.friendUserItem);
		}
	}

	//удалить элемент html контейнер пользователя из соответствующего списка
	
	_delete_user_item_from_list(data) {
		let userContainer = document.querySelector('.friend_result_item[data-profile_id="' + data.userId + '"]'),
			parentElement = userContainer.closest('.friend_query_search'),
			exsistDataRowNum = (userContainer.hasAttribute('data-rowNum')) ? true : false;

		if (userContainer) {
			userContainer.remove();

			if (exsistDataRowNum) {
				parentElement.dataset.itemcount = Number(parentElement.dataset.itemcount) - 1;
				let rowNum = 1;

				for (let el of parentElement.children) {
					// el.dataset.rowNum = rowNum;
					el.setAttribute('data-rowNum', rowNum);
					rowNum++;
				}
			}

			if (parentElement.children.length <= 0) {
				parentElement.insertAdjacentHTML('afterbegin', data.nothingFound);
			}
		}
	}
	
	//обновить кнопку дружеского взаимодействия на странице профиля пользователя

	_update_friend_button_userProfile(data) {
		let friendButton = document.querySelector('.friend_button');

		if (!friendButton) {
			return;
		}

		let previousEl = friendButton.previousElementSibling,
			parentEl = friendButton.parentElement;

		friendButton.remove();

		if (previousEl) {
			previousEl.insertAdjacentHTML('afterend', data.friendButtonUserProfile);
		} else {
			parentEl.insertAdjacentHTML('afterbegin', data.friendButtonUserProfile);
		}
	}
	
	//изменяем настройки пользователя на сайте

	_changeSettings(data) {
		let url = location.pathname.split('/'),
			firstPartUrl = url[1];
	
		if (firstPartUrl.match(/^(january|february|march|april|may|june|july|august|september|october|november|december)[0-9]{1,6}/) && this.selfID === data['fromUserID']) {
			this._minorShowOrHideEventsOncalendar(data);
		}
	}
	
	//второстепенные методы для изменений настроек пользователя на сайте
	
	_minorShowOrHideEventsOncalendar(data) {
		let checkBoxEl = document.querySelector('#' + data['changes']['applyFor'] + '_check'),
			calendarDateList = calendar.getDataForCalendarEvent();
		if (data['changes']['action'] == 'add') {
			checkBoxEl.checked = true;
			ajaxGetContentInCell(calendarDateList['dateList'], calendarDateList['dateOfFirstEl'], calendarDateList['dateOfLastEl'], calendarDateList['cellList'], calendarDateList['yearList'], calendarDateList['monthList']);
		} else if (data['changes']['action'] == 'remove') {
			checkBoxEl.checked = false;
			this._minorClearCellBySelectedValue(data['changes']['applyFor'])
		}
	}
	
	_minorClearCellBySelectedValue(value) {
		let elList, classEl;

		if (value == 'holidays') {
			elList = document.querySelectorAll('.holiday_cell');
			classEl = 'count_holiday';
		} else if (value == 'my_events') {
			elList = document.querySelectorAll('.event_cell');
			classEl = 'count_event';
		} else if (value == 'my_meetings') {
			elList = document.querySelectorAll('.meeting_cell');
			classEl = 'count_meeting';
		} else if (value == 'my_tasks') {
			elList = document.querySelectorAll('.task_cell');
			classEl = 'count_task';
		}
		
		if (!elList) return false;
		for (let el of elList) {
			el.innerHTML = '';
			el.classList.remove(classEl);
		}
	}
	
	//изменяем персональные данные на своей стороне
	
	_change_personal_data(data) {
		let url = location.pathname.split('/'),
			firstPartUrl = url[1],
			lastPartlUrl = url[url.length - 1];
			
		if (firstPartUrl === 'profile') {
			let urlUserID = url[2];

			if (urlUserID === this.selfID) {
				this._minorChangePersonalDataOnSelfProfilePage(data);
			}
		}

		this._minorChangePersonalDataTitle(data, firstPartUrl);
		this._minorChangePersonalDataHeaderBlock(data['pathToAva'], data['firstName'], data['lastName']);
		this._minorChangePersonalDataSelfItemBoxUser(data);
	}
	
	//второстепенные методы для изменения персональных данных на своей стороне
	
	_minorChangePersonalDataHeaderBlock(pathToAva, firstName, lastname) {
		
		let ava = document.querySelector('.avatar_block>img'),
			userFullNameBlock = document.querySelector('.personal_data_block>p');
			
		if (ava && pathToAva) {
			ava.src = pathToAva
		}

		if (userFullNameBlock && firstName && lastname) {
			userFullNameBlock.innerHTML = firstName + ' ' + lastname;
		}
		eventChangingTheContentOfTheHeaderBlock();
		
	}
	
	_minorChangePersonalDataOnSelfProfilePage(data) {
		
		let ava = document.querySelector('.block_avatar>img'),
			fullNameBlock = document.querySelector('.profile_name'),
			countryBlock = document.querySelectorAll('.profile_country_city_box')[0].children[1],
			cityBlock = document.querySelectorAll('.profile_country_city_box')[1].children[1];
		
		if (data['pathToAva']) {
			ava.setAttribute('src', data['pathToAva'])
		}
		
		if (data['firstName'] && data['lastName']) {
			fullNameBlock.innerHTML = data['firstName'] + ' ' + data['lastName'];
		}
		if (data['country']) {
			countryBlock.innerHTML = data['country'];
		}
		if (data['city']) {
			cityBlock.innerHTML = data['city'];
		}
		
	}
	
	_minorChangePersonalDataTitle(data, firstPartUrl) {
		
		if (firstPartUrl === 'calendar' || firstPartUrl === 'profile') {
			if (data['firstName'] && data['lastName']) {
				document.title = data['firstName'] + ' ' + data['lastName'];
			}
		}
		
	}
	
	_minorChangePersonalDataSelfItemBoxUser(data) {
		
		let itemBoxUserList = document.querySelectorAll('.search_main_result_item');
		
		if (itemBoxUserList.length > 0) {
			for (let i = 0; i < itemBoxUserList.length; i++) {
				if (itemBoxUserList[i].getAttribute('data-profile_id') === this.selfID) {
					let ava = itemBoxUserList[i].children[0].children[0].children[0],
						fullNameBlock = itemBoxUserList[i].children[1].children[0].children[0];
					if (data['pathToAva']) ava.setAttribute('src', data['pathToAva']);
					if (data['firstName'] && data['lastName']) {
						fullNameBlock.innerHTML = data['firstName'] + ' ' + data['lastName'];
					}
				}
			}
		}
	}
	
	//изменяем описание о себе на странице профиля
	
	_change_about_self(data) {
		this._minorAddModifiedDescriptionAboutSelfOnProfilePage(data);
		
	}
	
	//второстепенные методы для изменения описания о себе на странице профиля
	
	_minorAddModifiedDescriptionAboutSelfOnProfilePage(data) {
		
		let aboutSelfBlock = document.querySelector('.about_self');

		if (aboutSelfBlock && aboutSelfBlock.getAttribute('data-profile_id') === data['fromUserID']) {
			aboutSelfBlock.innerHTML = data['about_self'];
		}
		
	}
	
	//добавляем свое событие
	
	//второстепенные методы для добавления своего события
	
	_minorGetCellForSelfEvent(date, count, partOfClass, settingsID, id) {
		let elList = calendar.getDataForCalendarEvent().cellList;console.log(elList)

		for (let el of elList) {
			if (el.getAttribute('data-date_for_cell') == date && document.querySelector('#' + settingsID).checked) {
				let spanEl = el.querySelector('.' + partOfClass + '_cell'),
					innerCount = (spanEl.innerHTML) ? Number(spanEl.innerHTML) : 0,
					attrValue = el.getAttribute('data-' + partOfClass + '_id_list');

				if (attrValue) {
					attrValue = attrValue + ',' + id;
					el.setAttribute('data-' + partOfClass + '_id_list', attrValue);
				} else {
					el.setAttribute('data-' + partOfClass + '_id_list', id);
				}

				this._minorShowCountSelfEventIncell(el, date, count, innerCount, partOfClass, true);
			}
		}
	}
	
	_minorShowCountSelfEventIncell(el, date, count, innerCount, partOfClass, add) {

		let spanEl = el.querySelector('.' + partOfClass + '_cell');
		if (el.getAttribute('data-date_for_cell') == date) {
			spanEl.classList.remove('not_active');
			spanEl.classList.add('count_' + partOfClass);
			spanEl.innerHTML = (add) ? innerCount + Number(count) : count;
		}
	}
	
	_minorMinusOneAmountNewSubscriber(parentEl, count) {
		let amountEl = parentEl.querySelector('.new_subscriber_amount');

		if (amountEl && amountEl.tagName === 'SPAN') {
			let amount = amountEl.innerHTML,
				subtractTheNumber = (count || count === 0) ? count : 1,
				newAmount = Number(amount) - subtractTheNumber;

			if (newAmount < 1) {
				parentEl.children[0].removeChild(amountEl);
			} else {
				amountEl.innerHTML = newAmount;
			}
		}

		showNotificationAmountInBurgerMenuElem(iconBurgerMenu, burgerMenu, 'remove');
	}

	_minorMinusOneNewInvite(parentEl, count) {
		let amountEl = parentEl.querySelector('.new_invite_amount');

		if (amountEl && amountEl.tagName == 'SPAN') {
			let amount = amountEl.innerHTML,
				newAmount = Number(amount) - count;
			
			if (newAmount < 1) {
				amountEl.remove();
			} else {
				amountEl.innerHTML = newAmount;
			}
		}
	}

	_minorPlusOneNewInvite(parentEl) {
		let amountEl = parentEl.querySelector('.new_invite_amount');

		if (amountEl && amountEl.tagName == 'SPAN') {
			let amount = amountEl.innerHTML,
				newAmount = Number(amount) + 1;
			
			amountEl.innerHTML = newAmount;
		} else {
			let amountEl = document.createElement('span');
			amountEl.classList.add('new_invite_amount');
			amountEl.innerHTML = 1;
			parentEl.append(amountEl);
		}
	}
	
	_minorPlusOneAmountNewSubscriber(parentEl) {
		let amountEl = parentEl.querySelector('.new_subscriber_amount');

		if (amountEl && amountEl.tagName == 'SPAN') {
			let amount = amountEl.innerHTML,
				newAmount = Number(amount) + 1;
			
			amountEl.innerHTML = newAmount;
		} else {
			let amountEl = document.createElement('span');
			amountEl.classList.add('new_subscriber_amount');
			amountEl.innerHTML = 1;
			parentEl.children[0].appendChild(amountEl);
		}

		showNotificationAmountInBurgerMenuElem(iconBurgerMenu, burgerMenu, 'add');
	}

	_minorPlusOneAmountNewNotification(parentEl) {
		let amountEl = parentEl.querySelector('.new_notification_amount');

		if (amountEl && amountEl.tagName == 'SPAN') {
			let amount = amountEl.innerHTML,
				newAmount = Number(amount) + 1;
			
			amountEl.innerHTML = newAmount;
		} else {
			let amountEl = document.createElement('span');
			amountEl.classList.add('new_notification_amount');
			amountEl.innerHTML = 1;
			parentEl.append(amountEl);
		}
	}
	
	//ставим статус для сообщений от пользователя как прочитаные
	
	_mark_messages_as_read(data) {
		let url = location.pathname.split('/'),
			lastPartlUrl = url[url.length - 1],
			mainMenuMessagesItem = document.querySelector('.messages_menu_user_item');

		if (mainMenuMessagesItem && this.selfID === data['fromUserID']) {
			this._minorMinusOneAmountNewSubscriber(mainMenuMessagesItem, data['countNewMessage'])
		}
		
		if (lastPartlUrl === 'd' + data['toUserID'] && this.selfID === data['fromUserID']) {
			this._minorMarkMessagesAsReadOnPageDialogOrChat(data['toUserID'], data['fromUserID']);
		} else if (lastPartlUrl === 'd' + data['fromUserID'] && this.selfID === data['toUserID']) {
			this._minorMarkMessagesAsReadOnPageDialogOrChat(data['toUserID'], data['toUserID']);
		} else if (lastPartlUrl === 'messages' && this.selfID === data['toUserID']) {
			this._minorMarkMessagesAsReadOnAllСorrespondencePage(data['fromUserID'], false);
		} else if (lastPartlUrl === 'messages' && this.selfID === data['fromUserID']) {
			this._minorMarkMessagesAsReadOnAllСorrespondencePage(data['toUserID'], true);
		}
		
	}
	
	//второстепенные методы для постановки статуса для сообщений от пользователя как прочитаные
	
	_minorMarkMessagesAsReadOnPageDialogOrChat(toUserID, selfID) {
		let messageItemList = document.querySelectorAll('.message_item_box');
		
		for (let i = 0; i < messageItemList.length; i++) {
			if (messageItemList[i].getAttribute('data-profile_id') === toUserID && messageItemList[i].classList.contains('dialog_chat_item_not_read_message') && selfID === this.selfID) {
				messageItemList[i].classList.remove('dialog_chat_item_not_read_message');
			}
		}
	}
	
	_minorMarkMessagesAsReadOnAllСorrespondencePage(userID, selfMessage) {
		let dialogChatItemList = document.querySelectorAll('.dialog_chat_item');
		
		for (let i = 0; i < dialogChatItemList.length; i++) {
			let elNotReadMessage = dialogChatItemList[i].querySelector('.new_count_container');

			if (dialogChatItemList[i].getAttribute('data-profile_id') === userID) {
				this._minorRemoveElementAndClassAllСorrespondencePage(selfMessage, elNotReadMessage);
			}
		}
	}
	
	_minorRemoveElementAndClassAllСorrespondencePage(selfMessage, elNotReadMessage) {
		
		if (selfMessage) {
			if (!elNotReadMessage.previousElementSibling.children[0].classList.contains('ava_self_message_dialog_chat')) {
				elNotReadMessage.previousElementSibling.classList.remove('dialog_chat_item_not_read_message');
			}
			if (elNotReadMessage.querySelector('.new_messages_count')) {
				elNotReadMessage.querySelector('.new_messages_count').remove();
			}
		} else {
			if (elNotReadMessage.previousElementSibling.children[0].classList.contains('ava_self_message_dialog_chat')) {
				elNotReadMessage.previousElementSibling.classList.remove('dialog_chat_item_not_read_message');
			}
		}
		
	}
	
	_syncCSRF(data) {
		document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', data['csrfVal']);
	}
	
	//подготавливаем объект для передачи аргументов в функцию run
	
	collectData(el) {
		
		let data;

		if (el.dataset) {
			data = this._copyObj(el.dataset);
		} else {
			data = el;
		}

		data['fromUserID'] = this.selfID;
		let action = '_getData_' + data['ws_tab_sync_to_do'];
			
		return this[action](data, el);
		
	}
	
	_copyObj(obj) {
		
		let objCopy = {},
			key;
			
		for (key in obj) {
			objCopy[key] = obj[key];
		}
		
		return objCopy;
		
	}
	
	_getData_change_personal_data(data) {
		
		return data;

	}
	
	_getData_send_message_to_user(data) {
		
		return data;
		
	}
	
	_getData_changeSettings(data) {
		
		return data;
		
	}
	
	_getData_mark_messages_as_read(data) {

		return data;
		
	}
	
	_minorGetChangesSettingObj(str) {
		let result = {},
			arr = str.split('&');
			
		for (let i = 1; i < arr.length; i++) {
			let item = arr[i].split('=');
			result[item[0]] = item[1];
		}
		
		return result;
	}
}