/*jshint esversion: 6 */

var countryField = document.querySelector('.holiday_search_select'),
	holidayNameField = document.querySelector('.holiday_search_input'),
	searchSubmitButton = document.querySelector('.holiday_search_submit');

countryField.addEventListener('change', function() {
	if (countryField.value !== 'Выберите страну') {
		holidayNameField.classList.remove('not_active_element');
		searchSubmitButton.classList.remove('not_active_element');
	}
});

searchSubmitButton.addEventListener('click', function() {
	let countryCode = countryField.options[countryField.selectedIndex].getAttribute('data-countryCode');

	location.href = '/holiday/holiday-search/' + countryCode + '/' + holidayNameField.value;
	if (holidayNameField.value == '') location.href = '/holiday/holiday-search/' + countryCode;
	countryField.selectedIndex = 0;
	holidayNameField.value = '';
});