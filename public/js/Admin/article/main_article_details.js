/*jshint esversion: 6 */

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('delete_entity_button')) {
		let noticeModal = document.querySelector('.admin_delete_holiday'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		noticeModal.classList.add('modal_active');
		noticeModalBox.classList.add('modal_box_active');

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.classList.remove('modal_active');
			}, 300);
		});
	}

	if (e.target.classList.contains('p_delete')) {
		let dataObj = {
				'position': e.target.dataset.position,
				'type': e.target.parentElement.dataset.type,
				'article_id': e.target.closest('.admin_article_description').dataset.article_id,
				'file_id': (e.target.parentElement.dataset.type === 'image')
					? e.target.parentElement.dataset.file_id
					: null,
			},
			paragraphEl = e.target.parentElement;

		tab.checkToken(function() {
			tab.xjscsrf(function() {
				let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
					data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
						'&article_id=' + dataObj.article_id + 
						'&position=' + dataObj.position +
						'&type=' + dataObj.type;

					if (dataObj.file_id) {
						data += '&file_id=' + dataObj.file_id;
					}

					fetch('/admin/articles/delete-paragraph', {
						method: 'POST',
						body: data,
						headers: {
							'Content-Type': 'application/x-www-form-urlencoded',
							'X-Requested-With': 'XMLHttpRequest',
						}
					})
						.then(response => response.json())
						.then(response => {
							csrfVal.setAttribute('content', response.csrf);
							tab.sendCSRFToLS();
							return response;
						})
			  			.then(response => {
			  				if (response.res) {
			  					paragraphEl.remove();
			  				}

			  				if (!response.res) {
			  					console.log(response.errors);
			  				}
			  			})
			  			.catch((error) => {
							console.log(error);
						});
			});
		});
	}
});