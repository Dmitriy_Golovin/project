/*jshint esversion: 6 */

let form = document.querySelector('.upload_file_widget_form');
	
if (form) {
	let input = form.querySelector('input[type="file"]'),
		label = form.querySelector('label');

	input.addEventListener('change', function() {
		appendInputCR(form);
		form.submit();
	});
}