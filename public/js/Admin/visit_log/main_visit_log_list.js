/*jshint esversion: 6 */

let calendarContainer = document.querySelector('.calendar_container'),
	calendarBox = document.querySelector('.calendar_box'),
	calendarDateTitle = document.querySelector('.calendar_date_title'),
	calendarPrevArrow = calendarBox.querySelector('.calendar_left_arrow'),
	calendarNextArrow = calendarBox.querySelector('.calendar_right_arrow'),
	calendarTable = calendarBox.querySelector('table'),
	calendarClassData = {
		'titleDateEl': calendarDateTitle,
		'tableEl': calendarTable,
		'prevArrow': calendarPrevArrow,
		'nextArrow': calendarNextArrow,
	},
	calendarClass = new CalendarClass(calendarClassData);

calendarClass.init();

window.addEventListener('load', function() {
	document.querySelectorAll('.admin_filter_date_range_field').forEach(function(el) {
		if (el.value) {
			el.parentElement.querySelector('.admin_filter_date_range_clear_icon').classList.remove('not_active')
		}
	});
});

document.body.addEventListener('click', function(e) {
	if ((e.target.closest('.date_range_from_container') ||
			e.target.closest('.date_range_to_container')) &&
			e.target.tagName !== 'TD' && !e.target.closest('.calendar_box>table') &&
			!e.target.classList.contains('admin_filter_date_range_clear_icon')) {

		let container = e.target.closest('.date_range_main_container');
		container.append(calendarBox);
		calendarBox.classList.add('calendar_box_position_top');

		setTimeout(function() {
			calendarBox.classList.add('calendar_box_active');
		}, 10);
	}

	if (!e.target.closest('.date_range_from_container') &&
		!e.target.closest('.date_range_to_container')) {
		closeCalendarBox();
	}

	if (e.target.tagName === 'TD' && e.target.closest('.calendar_box>table')) {
		let container = e.target.closest('.date_range_main_container'),
			dateValue = e.target.getAttribute('data-filter_date'),
			dateArr = dateValue.split('-'),
			dateField = container.querySelector('.admin_filter_date_range_field'),
			clearEl = container.querySelector('.admin_filter_date_range_clear_icon');

		dateField.value = e.target.getAttribute('data-filter_date');
		clearEl.classList.remove('not_active');
		closeCalendarBox();
	}

	if (e.target.classList.contains('admin_filter_date_range_clear_icon')) {
		let container = e.target.closest('.date_range_main_container'),
			dateField = container.querySelector('.admin_filter_date_range_field'),
			clearEl = container.querySelector('.admin_filter_date_range_clear_icon');

		dateField.value = '';
		clearEl.classList.add('not_active');
	}

	if (e.target.classList.contains('admin_filter_clear_button')) {
		let container = e.target.closest('form'),
			fieldList = container.querySelectorAll('input'),
			selectList = container.querySelectorAll('select');

		for (let el of fieldList) {
			if (el.getAttribute('type') === 'button' || el.getAttribute('type') === 'submit') {
				continue;
			}
			
			el.value = '';
		}

		for (let el of selectList) {	
			el.selectedIndex = '';
		}

		container.submit();
	}
});

function closeCalendarBox() {
	if (calendarBox.classList.contains('calendar_box_position_top') || calendarBox.classList.contains('calendar_box_active')) {
		calendarBox.classList.remove('calendar_box_active');
		calendarClass.count = 0;
		calendarClass.init();

		setTimeout(function() {
			calendarBox.classList.remove('calendar_box_position_top');
			calendarContainer.append(calendarBox);
		}, 200);
	}
}