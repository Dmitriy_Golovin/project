/*jshint esversion: 6 */

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('delete_entity_button')) {
		let noticeModal = document.querySelector('.admin_delete_holiday'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		noticeModal.classList.add('modal_active');
		noticeModalBox.classList.add('modal_box_active');

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.classList.remove('modal_active');
			}, 300);
		});
	}
});