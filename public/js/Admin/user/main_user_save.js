/*jshint esversion: 6 */

let countryField = document.querySelector('select[name="country_id"]'),
	cityField = document.querySelector('select[name="city_id"]');


window.addEventListener('load', function() {
	let countryFieldValue = countryField.options[countryField.selectedIndex].value;

	if (countryFieldValue) {
		fetchGetCityList(countryFieldValue);
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('admin_base_submit_button')) {
		let form = e.target.closest('form');
		appendInputCR(form);
		form.submit();
	}
});

countryField.addEventListener('change', function() {
	let countryFieldValue = this.options[this.selectedIndex].value;

	if (countryFieldValue) {
		cityField.value = '';
		fetchGetCityList(countryFieldValue);
	}
});

function fetchGetCityList(countryId) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&country_id=' + countryId;

				fetch('/admin/city/getCityList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					let defaultValueEl = cityField.querySelector('.admin_select_default_value'),
		  						selectedIndex = cityField.dataset.selected_index;

		  					cityField.innerHTML = '';

		  					cityField.insertAdjacentHTML('afterbegin', response.html);

		  					if (selectedIndex) {
		  						let optionList = cityField.children;

		  						for (let el of optionList) {
		  							if (Number(el.value) === Number(selectedIndex)) {
		  								el.setAttribute('selected', true);
		  							}
		  						}
		  					}
		  				}

		  				if (!response.res) {
		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}