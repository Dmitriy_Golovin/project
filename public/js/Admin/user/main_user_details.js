/*jshint esversion: 6 */

document.body.addEventListener('click', function(e) {
	if ((e.target.classList.contains('image_template_delete_file_icon') ||
		e.target.classList.contains('image_template_delete_file_container')) &&
		e.target.closest('.admin_details_delete_avatar')) {

		let noticeModal = document.querySelector('.admin_delete_avatar'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		noticeModal.classList.add('modal_active');
		noticeModalBox.classList.add('modal_box_active');

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.classList.remove('modal_active');
			}, 300);
		});
	}
});