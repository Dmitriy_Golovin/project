/*jshint esversion: 6 */

let uploadFieldContainer = document.querySelector('#upload_file_image_field_container'),
	fieldContainer = document.querySelector('.field_image_container'),
	imagePreviewContainer = document.querySelector('.image_preview_container'),
	field = document.querySelector('#admin_upload_file_image'),
	fieldDeleteFile = document.querySelector('#admin_delete_file_image');

field.addEventListener('change', function() {
	let insertContainer = document.querySelector('.image_preview_container'),
		templateContainer = document.querySelector('.image_file_template_container'),
		cloneTemplate = templateContainer.firstElementChild.cloneNode(true),
		imageEl = cloneTemplate.querySelector('.image_file');

	imageEl.src = URL.createObjectURL(this.files[0]);
	insertContainer.prepend(cloneTemplate);

	fieldContainer.classList.add('not_active');
	imagePreviewContainer.classList.remove('not_active');
});

uploadFieldContainer.addEventListener('click', function(e) {
	if (e.target.classList.contains('image_template_delete_file_container') ||
		e.target.closest('.image_template_delete_file_container')) {

		let previewContainer = e.target.closest('.image_file_container_template') ||
				e.target.closest('.image_file_container_admin_preview');

		if (previewContainer.classList.contains('image_file_container_admin_preview')) {
			fieldDeleteFile.value = 1;
		}

		previewContainer.remove();
		fieldContainer.classList.remove('not_active');
		imagePreviewContainer.classList.add('not_active');
		field.value = '';
	}
});