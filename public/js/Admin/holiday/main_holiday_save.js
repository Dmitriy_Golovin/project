/*jshint esversion: 6 */

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('admin_base_submit_button')) {
		let form = e.target.closest('form');
		appendInputCR(form);
		form.submit();
	}
});