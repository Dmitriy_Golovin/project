/*jshint esversion: 6 */

let elemInsideMenuBox = document.querySelectorAll('.inside_menu_box'),
	adminElement = document.querySelector('.admin_personal_data_block'),
	adminElementArrow = document.querySelector('.pers_data_arrow'),
	menuList = document.querySelectorAll('.menu_user_item'),
	iconBurgerMenu = document.querySelector('.burger_menu'),
	adminDataBlock = document.querySelector('.admin_data_block'),
	burgerMenu = document.querySelector('.menu_burger'),
	mainMenu = document.querySelector('.menu_main'),
	burgerMenuHeader = document.querySelector('.burger_menu_header_box'),
	winWidth = document.documentElement.offsetWidth,
	tab = new BroadcastLS();

window.addEventListener('load', function() {
	setDataAttributePositionForUsermenuItem(menuList);
	showHideElemMenuList(menuList, iconBurgerMenu, adminDataBlock, mainMenu, burgerMenu, 1);
});

window.addEventListener('resize', function(e) {
	if (winWidth !== document.documentElement.offsetWidth) {
		winWidth = document.documentElement.offsetWidth;
		showHideElemMenuList(menuList, iconBurgerMenu, adminDataBlock, mainMenu, burgerMenu, 1);
	}
});

document.body.addEventListener('click', function(e) {
	if (adminElement && e.target !== adminElement && e.target !== adminElementArrow &&
		e.target !== document.querySelector('.admin_role') && e.target !== document.querySelector('.box_pers_data_arrow')) {
		removePersonalMenu(elemInsideMenuBox, e);
	}

	if (e.target.classList.contains('menu_user_item_exist_submenu')) {
		let menuUserItemSubmenuContainer = e.target.querySelector('.menu_user_item_submenu_container');

		if (e.target.dataset.active_submenu == 0) {
			hideBurgerSubMenu();
			e.target.dataset.active_submenu = 1
			menuUserItemSubmenuContainer.classList.add('menu_box_active');
			menuUserItemSubmenuContainer.classList.add('opacity_active');
		} else if (e.target.dataset.active_submenu == 1) {
			e.target.dataset.active_submenu = 0;
			menuUserItemSubmenuContainer.classList.remove('menu_box_active');
			menuUserItemSubmenuContainer.classList.remove('opacity_active');
		}
	}

	if (!e.target.classList.contains('menu_user_item_exist_submenu')) {
		hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e, burgerMenu);
		hideBurgerSubMenu();
	}
});

adminElement.addEventListener('click', function(e) {
	showHidePersonalMenu(elemInsideMenuBox, e);
});

iconBurgerMenu.addEventListener('click', function() {
	showBurgerMenu(burgerMenuHeader, iconBurgerMenu, burgerMenu);
});