function getDataForSearch(dataSearch, timestamp, rowNum) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			searchResultBlockMain = document.querySelector('.search_result_block_main'),
			data;
		
		if (getStrForDataSearch(dataSearch)) {
			data = getStrForDataSearch(dataSearch) + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');
		} else {
			data = 'csrf_test_name=' + csrfVal.getAttribute('content');
		}

		if (!timestamp) {
			timestamp = parseInt(Date.now() / 1000);
		}

		if (rowNum) {
			data += '&' + 'rowNum=' + Number(rowNum);
		}

		data += '&' + 'timestamp=' + timestamp;

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				if (result.res) {
					if (result.insert_type === 'renew') {
						searchResultBlockMain.innerHTML = '';
						searchResultBlockMain.insertAdjacentHTML('beforeend', result.html);
					}

					if (result.insert_type === 'add') {
						searchResultBlockMain.firstElementChild.insertAdjacentHTML('beforeend', result.html);
					}
				}

				scrollSearch = false;
			}
		};

		xhr.open('POST', '/search/searchRequest');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function getStrForDataSearch(obj) {
	var str = '';
	for (var key in obj) {
		str += key + '=' + obj[key] + '&'; 
	}
	if (str.length > 0) {
		return str.slice(0, -1);
	} else {
		return str;
	}
}