var holidayVariableDateList = ['sat-10-l', '1-sat-06', '3-wed-09', '3-fri-09', 'sun-10-l', 'sun-07-l', '3-sun-08', '2-sun-04', '2-sun-07', '1-sun-08', 'sat-06-l', '3-sat-05', 'sun-11-l', 'sun-09-l', '2-sat-06', '3-sun-06', '1-sun-06', '3-sun-07', '1-sun-09', 'fri-04-l', '1-sun-12', '2-sat-06', '2-sun-10', '3-sun-03', '2-sun-03', '3-sun-10', '3-sun-09', '1-sun-07', '1-sun-09', '3-sun-10', '2-sun-06', '4-sat-07', '3-sat-12', '2-sun-07', '2-sun-07', 'fri-05-l', '3-fri-09', '1-sun-12', 'fri-07-l', '3-sat-09', '4-sat-09', '2-sun-08', '2-sun-09', '1-mon-03', 'sun-09-l', '2-sat-08', 'sun-05-l', 'sun-08-l'],
	changeMonth = true;

function Calendar() {
	var self = this;
	self.count = 0;
	self.urlCount = 0;
	self.spanDay = document.getElementsByClassName('day_date');
	self.prev = document.getElementsByClassName('change-month')[1];
	self.next = document.getElementsByClassName('change-month')[0];
	var currentDate = document.querySelector('#show-date');
		numOfMonth = {
			'january': 0,
			'february': 1,
			'march': 2,
			'april': 3,
			'may': 4,
			'june': 5,
			'july': 6,
			'august': 7,
			'september': 8,
			'october': 9,
			'november': 10,
			'december': 11
		};
		
	Calendar.prototype.getDateByUrl = function() {
		var year = '';
		var month = '';
		var url = location.pathname.slice(1);
		
		for (var i = 0; i < url.length; i++) {
			if (url[i].match(/[0-9]$/)) year += url[i];
			else month += url[i];
		}
		
		return {'year': Number(year), 'month': month};
	};
		
	Calendar.prototype.getDifferenceMonth = function() {
		var url = location.pathname.slice(1);
		if (url == '/') return 0;
		
		var curDate = new Date();
			dateUrl = self.getDateByUrl();
		
		var urlDate = new Date(dateUrl['year'], numOfMonth[dateUrl['month']]),
			diffYear = (urlDate.getFullYear() - curDate.getFullYear()) * 12,
			diffMonth = (urlDate.getMonth() - curDate.getMonth());
			count = diffYear + diffMonth;
		
		self.count = count;

		return count;
	};

	Calendar.prototype.show = function(count) {
		self.d = new Date();

	    self.totalDays = new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 0).getDate();
        self.firstDayWeek = new Date(self.d.getFullYear(), self.d.getMonth() + count, 1).getDay();
        self.lastDayWeek = new Date(self.d.getFullYear(), self.d.getMonth() + count, self.totalDays).getDay();
	    self.today = self.d.getDate();
	    self.prevTotalDays = new Date(self.d.getFullYear(), self.d.getMonth() + count, 0).getDate();
	    self.prevLastDayWeek = new Date(self.d.getFullYear(), self.d.getMonth() - 1 + count, self.prevTotalDays).getDay();
	    self.nextFirstDayWeek = new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 1).getDay();
		
        if (self.firstDayWeek == 0) {
            self.firstDayWeek = 7;
        }

        if (self.lastDayWeek == 0) {
            self.lastDayWeek = 7;
        }

	    //числа предыдущего месяца
	    if (self.firstDayWeek != 1) {
	        for (var i = 0, j = self.prevTotalDays - self.prevLastDayWeek + 1; i < self.prevLastDayWeek, j <= self.prevTotalDays; i++, j++) {
	            self.spanDay[i].innerHTML = j;
				
				if (new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 0).getMonth() == 0) {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: -1, curentYear: -1})
				} else {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: -1})
				}
	        }
        } else {
	        for (var i = 0, j = self.prevTotalDays - 7 + 1; i < 7, j <= self.prevTotalDays; i++, j++) {
	            self.spanDay[i].innerHTML = j;
				
				if (new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 0).getMonth() == 0) {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: -1, curentYear: -1})
				} else {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: -1})
				}
	        }
	    }
		//числа текущего месяца
	    var n = 0;
	    if (self.firstDayWeek == 1) {
	        n = 7;
	    }
	    if (self.d.getMonth() == self.d.getMonth() + count && self.d.getFullYear() == self.d.getFullYear() + count) {
			self.spanDay[self.firstDayWeek + self.today + n - 3].parentNode.parentNode.classList.remove('td-today');
	        self.spanDay[self.firstDayWeek + self.today + n - 2].parentNode.parentNode.classList.add('td-today');
	    } else {
			[].forEach.call(self.spanDay, function(node) {
				node.parentNode.parentNode.classList.remove('td-today');
			});
		}
		for (var k = 0; k < self.spanDay.length; k++) {
		    self.spanDay[k].parentNode.parentNode.classList.remove('td-simple');
			self.spanDay[k].parentNode.parentNode.classList.remove('td_simple_sat_sun');
		}
        for (var i = self.firstDayWeek + n - 1, j = 1; i < self.spanDay.lengts - self.lastDayWeek + n - 1, j <= self.totalDays; i++, j++) {
            self.spanDay[i].innerHTML = j;
			setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d});
			
	        if (self.spanDay[i].parentNode.parentNode.classList.contains('sat_sun')) {
				self.spanDay[i].parentNode.parentNode.classList.add('td_simple_sat_sun');
			} else {
				self.spanDay[i].parentNode.parentNode.classList.add('td-simple');
			}
        }
		//числа следующего месяца
	    if (self.firstDayWeek != 1) {
	        for (var i = self.prevLastDayWeek + self.totalDays, j = 1; i < self.spanDay.length, j <= self.spanDay.length - (self.prevLastDayWeek + self.totalDays); i++, j++) {
	            self.spanDay[i].innerHTML = j;
				
				if (new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 0).getMonth() == 11) {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: 1, curentYear: 1})
				} else {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: 1})
				}
	        }
	    } else {
	        for (var i = self.prevLastDayWeek + self.totalDays + 7, j = 1; i < self.spanDay.length + 7, j <= self.spanDay.length - (self.prevLastDayWeek + self.totalDays + 7); i++, j++) {
	            self.spanDay[i].innerHTML = j;
				
				if (new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 0).getMonth() == 11) {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: 1, curentYear: 1});
				} else {
					setDateAttr({selectorElement: 'td', position: i, value: j, count: count, date: self.d, curentMonth: 1});
				}
				
	        }
	    }
		//console.log(new Date(self.d.getFullYear(), self.d.getMonth() + 1 + count, 0).getMonth());
		showDateInCaption(self.d, 'show-date', count);
	};
	
	Calendar.prototype.getDataForCalendarEvent = function() {
		var tdList = document.querySelectorAll('td');

		var dateList = [],
			dateOfFirstEl = '',
			dateOfLastEl = '',
			yearList = [],
			monthList = [];

		for (var i = 0; i < tdList.length; i++) {
			var date = tdList[i].getAttribute('data-date_for_cell'),
				el = tdList[i].getAttribute('data-holiday_date_for_cell');

			dateList.push(date);
			dateList.push(el);
			if (i == 0) monthList.push(date.slice(date.indexOf('-') + 1, date.lastIndexOf('-')));
			
			if (i > 0) {
				var curDate = tdList[i].getAttribute('data-date_for_cell'),
					previousDate = tdList[i - 1].getAttribute('data-date_for_cell'),
					curYear = curDate.slice(0, curDate.indexOf('-')),
					previousYear = previousDate.slice(0, previousDate.indexOf('-')),
					curMonth = curDate.slice(curDate.indexOf('-') + 1, curDate.lastIndexOf('-')),
					previousMonth = previousDate.slice(previousDate.indexOf('-') + 1, previousDate.lastIndexOf('-'));
					
				if (i == 1) {
					yearList.push(previousYear);
				}
				
				if (i == tdList.length - 1 && yearList[0] != curYear) {
					yearList.push(curYear);
				}
				
				if (previousMonth != curMonth) {
					monthList.push(curMonth);
				}
			}
			
			if (tdList[i].getAttribute('data-var_holiday_date_for_cell')) {
				dateList.push(tdList[i].getAttribute('data-var_holiday_date_for_cell'));
			}
			
			if (tdList[i].getAttribute('data-last_holiday_date_for_cell')) {
				dateList.push(tdList[i].getAttribute('data-last_holiday_date_for_cell'))
			}
			
			if (i == 0) dateOfFirstEl = tdList[i].getAttribute('data-date_for_cell');
			if (i == tdList.length - 1) dateOfLastEl = tdList[i].getAttribute('data-date_for_cell');
			
		}

		return {'dateList': dateList, 'dateOfFirstEl': dateOfFirstEl, 'dateOfLastEl': dateOfLastEl, 'yearList': yearList, 'monthList': monthList, 'cellList': tdList};
	};
	
	//this.show(self.count);
	//console.log(this.getDataForCalendarEvent())
	/* setInterval(function() {
		self.show(self.count);
	}, 1000); */
	
	Calendar.prototype.nextMonth = function() {
		if (changeMonth) {
			self.count++;
			self.show(self.count);
			var monthArrEn = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
				strForUrl = monthArrEn[new Date(self.d.getFullYear(), self.d.getMonth() + 1 + self.count, 0).getMonth()] + new Date(self.d.getFullYear(), self.d.getMonth() + 1 + self.count, 0).getFullYear(),
				url = location.pathname.slice(1);

			if (url !== '') {
				history.pushState(null, null, strForUrl);
			}

			changeMonth = false;
		}
	};
	
	Calendar.prototype.prevMonth = function() {
		if (changeMonth) {
			self.count--;
			self.show(self.count);
			var monthArrEn = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'],
				strForUrl = monthArrEn[new Date(self.d.getFullYear(), self.d.getMonth() + 1 + self.count, 0).getMonth()] + new Date(self.d.getFullYear(), self.d.getMonth() + 1 + self.count, 0).getFullYear(),
				url = location.pathname.slice(1);

			if (url !== '') {
				history.pushState(null, null, strForUrl);
			}

			changeMonth = false;
		}
	};
	
	self.next.addEventListener('click', self.nextMonth);

	self.prev.addEventListener('click', self.prevMonth);
}

function showDateInCaption(date, idSpanDate, count) {
	var elementDate = document.getElementById(idSpanDate),
		greetingBox = document.querySelector('.greeting'),
	    monthArr = document.querySelector('.content_wrapper>table').dataset.month.split(',');
		monthArrEn = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];
	
	dateForDisplay = {
	    year: new Date(date.getFullYear(), date.getMonth() + 1 + count, 0).getFullYear(),
	    month: monthArr[new Date(date.getFullYear(), date.getMonth() + 1 + count, 0).getMonth()]
	};
	
    elementDate.innerHTML = dateForDisplay.month.slice(0, 1).toUpperCase() + dateForDisplay.month.slice(1) + ' ' + dateForDisplay.year;
	
	var calendarDateList = calendar.getDataForCalendarEvent();

	tab.checkToken(function() {
		ajaxGetContentInCell(calendarDateList.dateList, calendarDateList.dateOfFirstEl, calendarDateList.dateOfLastEl, calendarDateList.cellList, calendarDateList.yearList, calendarDateList.monthList);
	});
}

function setDateAttr(el) {
    el.curentYear = el.curentYear || 0;
	el.curentMonth = el.curentMonth || 0;

	var cellList = document.querySelectorAll(el.selectorElement),
	    monthAbbrArr = ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'],
		numWeekInMonth = ['1-', '2-', '3-', '4-', '5-'],
		nameDayInweek = ['sun', 'mon', 'tue', 'wed', 'thu', 'fri', 'sat'];

	dateForAttr = {
	    year: new Date(el.date.getFullYear(), el.date.getMonth() + 1 + el.count + el.curentYear, 0).getFullYear(),
	    month: monthAbbrArr[new Date(el.date.getFullYear(), el.date.getMonth() + 1 + el.count + el.curentMonth, 0).getMonth()]
	};
	
	var varDateAttr = numWeekInMonth[0 | el.value / 7 - 0.1] + nameDayInweek[new Date(dateForAttr['year'], dateForAttr['month'] - 1, el.value).getDay()] + '-' + dateForAttr['month'],
		lastDateAttr = '',
		days_in_april = 32 - new Date(dateForAttr['year'], dateForAttr['month'] - 1, 32).getDate(),
		difference = days_in_april - el.value;
	
	if (difference < 7) {
		lastDateAttr = nameDayInweek[new Date(dateForAttr['year'], dateForAttr['month'] - 1, el.value).getDay()] + '-' + dateForAttr['month'] + '-l';
	}
	
	var day = String(el.value);
	if (day.length < 2) day = '0' + day;
	
	cellList[el.position].removeAttribute('data-var_holiday_date_for_cell');
	cellList[el.position].removeAttribute('data-last_holiday_date_for_cell');
	cellList[el.position].setAttribute('data-date_for_cell', (dateForAttr['year']) + '-' + dateForAttr['month'] + '-' + day);
	cellList[el.position].setAttribute('data-holiday_date_for_cell', el.value + '-' + dateForAttr['month']);
	
	for (var i = 0; i < holidayVariableDateList.length; i++) {
		if (varDateAttr == holidayVariableDateList[i]) {
			cellList[el.position].setAttribute('data-var_holiday_date_for_cell', varDateAttr);
		}
		if (lastDateAttr == holidayVariableDateList[i]) {
			cellList[el.position].setAttribute('data-last_holiday_date_for_cell', lastDateAttr);
		}
	}
}

function uniqueElList(str, elem) {
	var arr = str.split(',');
		result = [];
	for (var i = 0; i < arr.length; i++) {
		if (arr[i] !== elem) {
			result.push('\'' + arr[i] + '\'');
		}
	}
	return result;
}