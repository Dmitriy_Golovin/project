var targetElement = document.querySelector('.close_win_add_new_event'),
	targetPic = document.querySelector('.close_pic'),
	containerForElement = document.querySelector('.add_new_win_container'),
	elemInsideWin = document.querySelectorAll('.inside_win'),
	winForNewEvent = document.querySelector('.add_new_win'),
	
	formFieldsetBox = document.querySelector('.fieldset_box'),
	userLoginField = document.querySelector('#email_field'),
	userPasswordField = document.querySelector('#password_field'),
	formLogin = document.querySelector('form[name=login_form]'),
	buttonLogin = document.querySelector('.button_login'),
	
	buttonPassRecovery = document.querySelector('.button_pass_recovery'),
	buttonNewPass = document.querySelector('.button_new_pass'),
	waitingBoxNewPass = document.querySelector('.waiting_icon');
	
	buttonCancelReg = document.querySelector('.button_cancel_reg'),
	buttonReg = document.querySelector('.button_reg'),
	formRegistration = document.querySelector('form[name=registration_form]'),
	regEmail = document.querySelector('input[name=email]'),
	regPassword = document.querySelector('input[name=password]'),
	regRepeatPassword = document.querySelector('input[name=repeat_password]'),
	regFirstName = document.querySelector('input[name=first_name]'),
	regLastName = document.querySelector('input[name=last_name]'),
	noticeElemList = document.querySelectorAll('.notice_elem'),
	loginRegContainer = document.querySelector('.login_reg_box'),
	loginRegWrapper = document.querySelector('.confirmation_box'),
	waitingBox = document.querySelector('.waiting_icon'),
	
	userMenuList = document.querySelectorAll('.menu_user_item'),
	iconBurgerMenu = document.querySelector('.burger_menu')
	elemOnTheRight = document.querySelector('.autorized_or_personal_block'),
	burgerMenuHeader = document.querySelector('.burger_menu_header_box'),
	mainMenu = document.querySelector('.menu_user_main'),
	burgerMenu = document.querySelector('.menu_user_burger'),
	
	searchSubmitutton = document.querySelector('.search_submit'),
	searchField = document.querySelector('.search_field'),

	formRestorePassword = document.querySelector('form[name=restore_password]'),

	winWidth = document.documentElement.offsetWidth
	
	url = 'localhost:8080',
	userID = false,
	
	registerObj = {'res': false};
	
let tab = new BroadcastLS(url, userID/* , cookieSession */);
console.log(tab);
	
window.addEventListener('load', function() {
	setDataAttributePositionForUsermenuItem(userMenuList);
	showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
});

window.addEventListener('resize', function() {
	if (winWidth !== document.documentElement.offsetWidth) {
		winWidth = document.documentElement.offsetWidth;
		showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
		//hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e, burgerMenu);
	}
});

iconBurgerMenu.addEventListener('click', function() {
	showBurgerMenu(burgerMenuHeader, iconBurgerMenu)
});

document.body.addEventListener('click', function(e) {
	hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e);
});

if (buttonLogin) {
	buttonLogin.addEventListener('click', function() {
		
		if (!userLoginField.value || !userPasswordField.value) {
			checkFillingFormLogin(formFieldsetBox, userLoginField, userPasswordField);
			return false;
		} else if (userLoginField.value || userPasswordField.value) {
			//checkLoginPasswordAjax(userLoginField, userPasswordField);
			appendInputCR(formLogin);
			//console.log(formLogin);
			formLogin.submit();
		}

	});
	
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (userLoginField == document.activeElement || userPasswordField == document.activeElement) {
				if (!userLoginField.value || !userPasswordField.value) {
					checkFillingFormLogin(formFieldsetBox, userLoginField, userPasswordField);
					return false;
				} else if (userLoginField.value || userPasswordField.value) {
					//checkLoginPasswordAjax(userLoginField, userPasswordField);
					appendInputCR(formLogin);
					formLogin.submit();
				}
			}
		}
	})
	
	userLoginField.addEventListener('focus', function() {
		removeNotice(formFieldsetBox, userLoginField, userPasswordField)
	});
	
	userPasswordField.addEventListener('focus', function() {
		removeNotice(formFieldsetBox, userLoginField, userPasswordField)
	});
};

if (buttonNewPass) {
	let urlArr = window.location.pathname.split('/'),
		token = urlArr[urlArr.length - 1],
		formAction = formRestorePassword.getAttribute('action');

	formRestorePassword.setAttribute('action', formAction + '/' + token);

	buttonNewPass.addEventListener('click', function() {
		if (!showErrorField(regPassword, 'min 6 max 20 символов', /^[A-Za-z0-9_]{6,20}$/) ||
			!passwordMatchCheck(regPassword, regRepeatPassword)) {
			return false;
		}
	});
	
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (regPassword == document.activeElement || regRepeatPassword == document.activeElement) {
				if (!showErrorField(regPassword, 'min 6 max 20 символов', /^[A-Za-z0-9_]{6,20}$/) ||
					!passwordMatchCheck(regPassword, regRepeatPassword)) {
					return false;
				} else {
					formRestorePassword.submit();
				}
			}
		}
	})
	
	regPassword.addEventListener('blur', function(e) {
		showErrorField(regPassword, 'min 6 max 20 символов', /^[A-Za-z0-9_]{6,20}$/);
	});
	
	regPassword.addEventListener('focus', function(e) {
		removeError(this);
	});
	
	regRepeatPassword.addEventListener('blur', function(e) {
		passwordMatchCheck(regPassword, regRepeatPassword);
	});
	
	regRepeatPassword.addEventListener('focus', function(e) {
		removeError(this);
	});
	
}

if (buttonReg) {
	
	buttonReg.addEventListener('click', function() {
		var data = 'reg_email=' + regEmail.value + '&' + 'reg_password=' + regPassword.value + '&' + 'reg_firstname=' + regFirstName.value + '&' + 'reg_lastname=' + regLastName.value + '&' + 'ajax=' + true;

		ajaxCheckEmailExist(regEmail, registerObj, function() {
			if (showErrorField(regEmail, 'Введите корректный e-mail', /^[0-9a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/) &&
				showErrorField(regPassword, 'min 6 max 20 символов, допускается латинские заглавные или строчные буквы и цифры', /^[A-Za-z0-9_]{6,20}$/) &&
				showErrorField(regFirstName, 'Введите имя', /^\s*[0-9a-zA-Zа-яА-Я][0-9a-zA-Zа-яА-Я]*$/) &&
				showErrorField(regLastName, 'Введите фамилию', /^\s*[0-9a-zA-Zа-яА-Я][0-9a-zA-Zа-яА-Я]*$/) &&
				passwordMatchCheck(regPassword, regRepeatPassword)/*  && registerObj['res'] == true */) {

				activateIconWaiting(buttonReg, waitingBox);
				appendInputCR(formRegistration);
				formRegistration.submit();
			} else {
				return false;
			}
		})
	});
	
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (regEmail == document.activeElement || regPassword == document.activeElement ||
			    regRepeatPassword == document.activeElement || regFirstName == document.activeElement || regLastName == document.activeElement) {
				var data = 'reg_email=' + regEmail.value + '&' + 'reg_password=' + regPassword.value + '&' + 'reg_firstname=' + regFirstName.value + '&' + 'reg_lastname=' + regLastName.value;

				ajaxCheckEmailExist(regEmail, registerObj, function() {;
					if (showErrorField(regEmail, 'Введите корректный e-mail', /^[0-9a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/) &&
						showErrorField(regPassword, 'min 6 max 20 символов, допускается латинские заглавные или строчные буквы и цифры', /^[A-Za-z0-9_]{6,20}$/) &&
						showErrorField(regFirstName, 'Введите имя', /^\s*[0-9a-zA-Zа-яА-Я][0-9a-zA-Zа-яА-Я]*$/) &&
						showErrorField(regLastName, 'Введите фамилию', /^\s*[0-9a-zA-Zа-яА-Я][0-9a-zA-Zа-яА-Я]*$/) &&
						passwordMatchCheck(regPassword, regRepeatPassword) && registerObj['res'] == true) {

						activateIconWaiting(buttonReg, waitingBox);
						appendInputCR(formRegistration);
						formRegistration.submit();
					}
				});
			}
		}
	})
	
	buttonCancelReg.addEventListener('click', function() {
		regEmail.classList.remove('error_input');
		regEmail.classList.remove('valid_input');
		regEmail.value = '';
		regPassword.classList.remove('error_input');
		regPassword.classList.remove('valid_input');
		regPassword.value = '';
		regRepeatPassword.classList.remove('error_input');
		regRepeatPassword.classList.remove('valid_input');
		regRepeatPassword.value = '';
		regFirstName.classList.remove('error_input');
		regFirstName.classList.remove('valid_input');
		regFirstName.value = '';
		regLastName.classList.remove('error_input');
		regLastName.classList.remove('valid_input');
		regLastName.value = '';
		
		for (var i = 0; i < noticeElemList.length; i++) {
			noticeElemList[i].classList.remove('notice_elem_active');
			noticeElemList[i].classList.remove('notice_elem_large_active1');
			noticeElemList[i].classList.remove('notice_elem_large_active2');
			noticeElemList[i].innerHTML = '';
		}
	});
	
	formFieldsetBox.addEventListener('click', function(e) {
		if (!e.target.classList.contains('input_login_reg') || e.target == regEmail) return;
		e.target.classList.remove('error_input');
		document.querySelector('.' + e.target.getAttribute('name')).classList.remove('notice_elem_active');
		document.querySelector('.' + e.target.getAttribute('name')).classList.remove('notice_elem_large_active1');
		document.querySelector('.' + e.target.getAttribute('name')).classList.remove('notice_elem_large_active2');
		document.querySelector('.' + e.target.getAttribute('name')).innerHTML = '';
	});
	
	regEmail.addEventListener('blur', function(e) {
		//registerEmailCheck(regEmail, registerObj);
		showErrorField(regEmail, 'Введите корректный e-mail', /^[0-9a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/);
	});
	
	regEmail.addEventListener('focus', function(e) {
		removeError(this);
	});
	
	regPassword.addEventListener('blur', function(e) {
		showErrorField(regPassword, 'min 6 max 20 символов', /^[A-Za-z0-9_]{6,20}$/);
	});
	
	regPassword.addEventListener('focus', function(e) {
		removeError(this);
	});
	
	regRepeatPassword.addEventListener('blur', function(e) {
		passwordMatchCheck(regPassword, regRepeatPassword)
	});
	
	regRepeatPassword.addEventListener('focus', function(e) {
		removeError(this);
	});
	
	regFirstName.addEventListener('blur', function(e) {
		showErrorField(regFirstName, 'Введите имя', /^\s*[0-9a-zA-Zа-яА-Я][0-9a-zA-Zа-яА-Я]*$/);
	});
	
	regFirstName.addEventListener('focus', function(e) {
		removeError(this);
	});
	
	regLastName.addEventListener('blur', function(e) {
		showErrorField(regLastName, 'Введите фамилию', /^\s*[0-9a-zA-Zа-яА-Я][0-9a-zA-Zа-яА-Я]*$/);
	});
	
	regLastName.addEventListener('focus', function(e) {
		removeError(this);
	});
}

function getUrl() {
	return res = location['href'].split('/').pop();
}

function getRegisterdata() {
	
	if (getUrl() == 'confirmation') {
		loginRegContainer.classList.add('not_active');
		loginRegWrapper.classList.remove('not_active');
		loginRegWrapper.parentElement.classList.add('reg_wrapper_confirm');
		loginRegWrapper.parentElement.classList.remove('login_reg_wrapper');
		loginRegWrapper.innerHTML = registerObj['confirm'];
		document.title = 'Подтверждение регистрации';
		
	} else if (getUrl() == 'register') {
		loginRegContainer.classList.remove('not_active');
		loginRegWrapper.classList.add('not_active');
		loginRegWrapper.parentElement.classList.remove('reg_wrapper_confirm');
		loginRegWrapper.parentElement.classList.add('login_reg_wrapper');
		loginRegWrapper.innerHTML = '';
	}

}

if (searchSubmitutton) {
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (searchField == document.activeElement) {
				if (searchField.value !== '') {
					var url = encodeURI('/search?q=' + searchField.value);
					location.href = url;
				}
				searchField.value = '';
			}
		}
	})
	
	searchSubmitutton.addEventListener('click', function() {
		if (searchField.value !== '') {
			var url = encodeURI('/search?q=' + searchField.value);
			location.href = url;
		}
		searchField.value = '';
	});
}