/*jshint esversion: 6 */

let countryP = document.querySelector('p.show_hide_country'),
	countryListOptionEl = document.querySelectorAll('select[name=reg_country]>option'),
	countrySelectEl = document.querySelector('select[name=reg_country]'),
	avatarEditIcon = document.querySelector('.avatar_edit_icon'),
	noticeElem = document.querySelector('.notice_elem_ava'),
	buttonEditProfileSubmit = document.querySelector('.button_edit_profile_submit'),
	buttonUserSettingSubmit = document.querySelector('.button_user_setting_submit'),
	buttoneditPasswordAndSafetySubmit = document.querySelector('.button_edit_password_and_safety_submit'),
	noticeElemWrongCurrentPass = document.querySelector('div.notice_elem'),
	passwordSubmitEl = document.querySelector('.current_password_inp'),
	currentPasswordSubmit = document.querySelector('.current_password_submit'),
	changePasswordHideButton = document.querySelector('.change_password_icon_hide'),
	passSafButtonList = document.querySelectorAll('.pass_saf_icon_button'),
	passSafCancelButtonList = document.querySelectorAll('.pass_saf_cancel_button'),
	confirmNewEmailButton = document.querySelector('.button_change_email_right'),
	confirmNewPassButton = document.querySelector('.button_change_password_right'),
	passAndSafInputList = document.querySelector('.password_and_safety_edit').querySelectorAll('input'),
	waitingBoxSendData = document.querySelector('.waiting_icon'),
	workDaysAmountField = document.querySelector('input[name="work_days_amount"]'),
	workWeekStartField = document.querySelector('select[name="work_week_start"]'),
	waitingBoxSettingSendData = document.querySelector('#waitingBoxSettingSendData'),
	deleteProfileButton = document.querySelector('.delete_profile_button');

animationPushIcon({icon: '.edit_icon'});

for (var i = 0; i <countryListOptionEl.length; i++) {
	if (countryListOptionEl[i].innerHTML === countryP.innerHTML) {
		tab.checkToken(function() {
			ajaxGetCitiesForSearch(countryListOptionEl[i].getAttribute('data-countryId'));
		});
		break;
	}
}

countrySelectEl.addEventListener('change', function(e) {
	var res = countrySelectEl.value;
	for (var i = 0; i <countryListOptionEl.length; i++) {
		if (countryListOptionEl[i].innerHTML === res) {
			tab.checkToken(function() {
				ajaxGetCitiesForSearch(countryListOptionEl[i].getAttribute('data-countryId'));
			});
			break;
		}
	}

	let cityElP = document.querySelector('#city_value');

	cityElP.innerHTML = '';
	cityElP.dataset.cityid = '';
});

workDaysAmountField.addEventListener('input', function() {
	this.value = this.value.replace(/[^1-7]{1}/, '').substr(0, 1);
});

document.addEventListener('click', function(e) {
	if (e.target.hasAttribute('data-show-hide')) {
		closeNotCurrentActiveInput(e.target);
	}
	
	switch(e.target.getAttribute('data-show-hide')) {
		case 'show_hide_firstname':
			showHideInputElem('show_hide_firstname');
			//document.querySelector('.show_hide_firstname').classList.remove('error_input');
			break;
		case 'show_hide_lastname':
			showHideInputElem('show_hide_lastname');
			//document.querySelector('.show_hide_lastname').classList.remove('error_input');
			break;
		case 'show_hide_country':
			showHideInputElem('show_hide_country');
			break;
		case 'show_hide_city':
			showHideInputElem('show_hide_city');
			break;
		/* case 'show_hide_email':
			showHideInputElem('show_hide_email');
			//document.querySelector('.show_hide_email').classList.remove('error_input');
			break; */
		// case 'show_hide_numphone':
		// 	showHideInputElem('show_hide_numphone');
		// 	showHideNumPoneInputElem('show_hide_numphone');
		// 	document.querySelector('.show_hide_numphone').classList.remove('error_input');
			// break;
		case 'show_hide_work_day_start':
			showHideInputElem('show_hide_work_day_start');
			break;
		case 'show_hide_work_days_amount':
			showHideInputElem('show_hide_work_days_amount');
			break;

	}
	
	if (e.target.getAttribute('data-show-hide')) {
		switch(e.target.getAttribute('src')) {
			case '/img/edit_icon.png':
				e.target.setAttribute('src', '/img/input_arrow.png');
				break;
			case '/img/input_arrow.png':
				e.target.setAttribute('src', '/img/edit_icon.png');
				break;
		}
	}
});

avatarEditIcon.addEventListener('click', function() {
	activateChangeAvatarModalBox();
});

buttonUserSettingSubmit.addEventListener('click', function() {
	closeNotCurrentActiveInput(buttonUserSettingSubmit);
	activateIconWaiting(buttonUserSettingSubmit, waitingBoxSettingSendData);

	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let dataFetch = 'work_week_start=' + workWeekStartField.options[workWeekStartField.selectedIndex].getAttribute('data-day_value') +
				'&work_days_amount=' + workDaysAmountField.value +
				'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
			
			fetch('/user/changeSetting', {
				method: 'POST',
				body: dataFetch,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
			.then(response => response.json())
			.then(response => {
				document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
				tab.sendCSRFToLS();
				return response;
			})
  			.then(response => {
  				if (response.res) {
  					let button = buttonUserSettingSubmit,
						valueButton = button.value;
					button.value = 'Сохранено';
					disActivateIconWaiting(buttonUserSettingSubmit, waitingBoxSettingSendData);
					setTimeout(function() {
						button.value = valueButton;
					}, 1000)
  				} else {
  					console.log(response.errors);
  				}
  			})
  			.catch((error) => {
				console.log(error);
			});
  		});
	});
});

buttonEditProfileSubmit.addEventListener('click', function() {
	closeNotCurrentActiveInput(buttonEditProfileSubmit);
	let validate = validationFieldPersonalData('personal_data');

	if (validate) {
		activateIconWaiting(buttonEditProfileSubmit, waitingBoxSendData);
		tab.checkToken(function() {
			ajaxAddPersonalData(function() {
				let button = buttonEditProfileSubmit,
					valueButton = button.value;
				button.value = 'Сохранено';
				disActivateIconWaiting(buttonEditProfileSubmit, waitingBoxSendData);
				setTimeout(function() {
					button.value = valueButton;
				}, 1000)
			});
		});
	}
});


currentPasswordSubmit.addEventListener('click', function() {
	tab.checkToken(function() {
		if (!passwordSubmitEl.value) return;
		ajaxCheckPassword(passwordSubmitEl);
	});
});

passwordSubmitEl.addEventListener('keydown', function() {
	noticeElemWrongCurrentPass.classList.remove('notice_elem_active');
});

passwordSubmitEl.addEventListener('click', function() {
	this.removeAttribute('readonly');
});

for (let node of passSafButtonList) {
	node.addEventListener('click', function() {
		showBlockChangeData(node);
	});
}

for (let node of passSafCancelButtonList) {
	node.addEventListener('click', function() {
		hideBlockChangeData(node);	
	});
}

for (let node of passAndSafInputList) {
	node.addEventListener('focus', function() {
		if (node.classList.contains('error_input')) {
			node.classList.remove('error_input');
			node.previousElementSibling.classList.remove('notice_message');
			node.previousElementSibling.innerHTML = node.previousElementSibling.getAttribute('data-default-content');
		}
	});
}

confirmNewPassButton.addEventListener('click', function() {
	if (validateInputData(confirmNewPassButton)) {
		activateIconWaiting(confirmNewPassButton, confirmNewPassButton.nextElementSibling);
		tab.checkToken(function() {
			ajaxChangePassword(confirmNewPassButton, passwordSubmitEl);
		});
	}
});

confirmNewEmailButton.addEventListener('click', function() {
	if (validateInputData(confirmNewEmailButton)) {
		activateIconWaiting(confirmNewEmailButton, confirmNewEmailButton.nextElementSibling);
		tab.checkToken(function() {
			ajaxChekEmail(confirmNewEmailButton)
			.then(function(result) {
				ajaxSendNewEmail(confirmNewEmailButton, passwordSubmitEl, function() {
					disActivateIconWaiting(confirmNewEmailButton, confirmNewEmailButton.nextElementSibling);
				});
			});
		});
	}
});

deleteProfileButton.addEventListener('click', function() {
	let noticeModal = document.querySelector('.modal_notice_delete_profile'),
		noticeModalBox = noticeModal.querySelector('.modal_box'),
		rejectButton = noticeModal.querySelector('.notice_button_reject');

	noticeModal.classList.add('modal_active');
	noticeModalBox.classList.add('modal_box_active');

	rejectButton.addEventListener('click', function() {
		noticeModalBox.classList.remove('modal_box_active');
		
		setTimeout(function() {
			noticeModal.classList.remove('modal_active');
		}, 300);
	});
});