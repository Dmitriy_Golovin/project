function setNavHeaderContainerDimensions() {
	var table = document.querySelector('table'),
	    //changeMonthBox = document.querySelector('.change_month_box'),
	    calendarSettingsBox = document.querySelector('.calendar_settings_box'),
	    //headerContainer = document.querySelector('.header_container'),
		//footerContainer = document.querySelector('.footer_container'),
		greetingBox = document.querySelector('.greeting');
		
	if (greetingBox) {
		//changeMonthBox.style.width = table.offsetWidth + 'px';
		calendarSettingsBox.style.width = table.offsetWidth + 'px';
	} else if (table && !greetingBox) {
		//headerContainer.style.width = table.offsetWidth + 'px';
		//footerContainer.style.width = table.offsetWidth + 'px';
		//changeMonthBox.style.width = table.offsetWidth + 'px';
		calendarSettingsBox.style.width = table.offsetWidth + 'px';
	}
};

function setTableDimension() {
	var table = document.querySelector('table'),
	    cell = document.querySelectorAll('td'),
	    greetingBox = document.querySelector('.greeting'),
		documentWidth = document.body.clientWidth;
		
	document.body.style.overflow = 'hidden';
	var a1 = document.body.clientWidth;
	document.body.style.overflow = 'visible';
	var a2 = document.body.clientWidth;
	
	var scrollWidth = a1 - a2;
	
	if (table) {
		if (!greetingBox && document.documentElement.clientWidth > 576 - scrollWidth) {
			table.style.width = document.documentElement.clientWidth / 100 * 80 + 'px';
			
			for (var j = 0; j < cell.length; j++) {
				cell[j].style.width = table.offsetWidth / 7 + 'px';
				cell[j].style.height = (document.documentElement.clientHeight / 100 * 90) / 6 + 'px';
			};
			
		} else if (!greetingBox){
			table.style.width = document.documentElement.clientWidth / 100 * 95 + 'px';
			
			for (var j = 0; j < cell.length; j++) {
				cell[j].style.width = table.offsetWidth / 7 + 'px';
				cell[j].style.height = (document.documentElement.clientHeight / 100 * 90) / 6 + 'px';
			};
			
		};
		
		if (documentWidth >= 769 - scrollWidth && greetingBox) {
			
			table.style.width = (document.documentElement.clientWidth + scrollWidth) / 100 * 60 + 'px';
			table.style.height = document.documentElement.clientHeight / 100 * 70 + 'px';
			
			for (var i = 0; i < cell.length; i++) {
				cell[i].style.width = table.offsetWidth / 7 + 'px';
				cell[i].style.height = (document.documentElement.clientHeight / 100 * 70) / 6 + 'px';
			};
			

		} else if (greetingBox) {

			table.style.width = (document.documentElement.clientWidth + scrollWidth) / 100 * 80 + 'px';
			table.style.height = (document.documentElement.clientHeight - scrollWidth) / 100 * 70 + 'px';
			
			for (var i = 0; i < cell.length; i++) {
				cell[i].style.width = table.offsetWidth / 7 + 'px';
			};
		};
		
	};
}