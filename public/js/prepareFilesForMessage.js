function hideVideoControlContainer(controlContainer, videoBlock, videoEl) {
	clearTimeout(timeOutVideoMediaControl);
	if (!videoEl.paused) {
		timeOutVideoMediaControl = setTimeout(function() {
			controlContainer.classList.add('not_active');
			videoBlock.classList.add('cursor_none');
		}, 2000);
	}
}

function showVideoControlContainer(controlContainer, videoBlock, videoEl) {
	controlContainer.classList.remove('not_active');
	videoBlock.classList.remove('cursor_none');
}

function playOrPauseVideoOrAudio(mediaContent, playIconEl, containerClass, elemName, playIcon, pauseIcon) {
		if (!playIconEl.hasAttribute('data-action_media_icon') || playIconEl.getAttribute('data-action_media_icon') === 'play') {
			//setPausedAnotherVideoOrAudio(containerClass, elemName, playIcon);
			setPausedForActiveMediaElem();
			mediaContent.play();
			playIconEl.setAttribute('data-action_media_icon', 'pause');
			playIconEl.src = pauseIcon;
		} else if (playIconEl.getAttribute('data-action_media_icon') === 'pause') {
			mediaContent.pause();
			playIconEl.setAttribute('data-action_media_icon', 'play');
			playIconEl.src = playIcon;
			if (elemName == 'video') {
				clearTimeout(timeOutVideoMediaControl);
			}
		}
	}

function setPausedAnotherVideoOrAudio(containerClass, elemName, playIcon) {
	for (let mediaEl of document.querySelectorAll(containerClass)) {
		if (mediaEl.querySelector('.' + elemName + '_play_icon').getAttribute('data-action_media_icon') === 'pause') {
			mediaEl.querySelector(elemName).pause();
			mediaEl.querySelector('.' + elemName + '_play_icon').setAttribute('data-action_media_icon', 'play');
			mediaEl.querySelector('.' + elemName + '_play_icon').src = playIcon
		}

		if (elemName == 'video') {
			let videoControl = mediaEl.querySelector('.video_control_media_block'),
				videoBlock = mediaEl.querySelector('.select_video_message_block');

			videoControl.classList.remove('not_active');
			videoBlock.classList.remove('cursor_none');
		}
	}
}

function setPausedForActiveMediaElem() {
	let playIcon = document.querySelector('img[data-action_media_icon="pause"]');

	if (playIcon) {
		let mediaContainer = playIcon.closest('.file_container_for_message'),
			mediaEl = mediaContainer.querySelector('.media_el');

		mediaEl.pause();
		playIcon.setAttribute('data-action_media_icon', 'play');

		if (mediaEl.tagName === 'AUDIO') {
			playIcon.src = '/img/play_icon.png';
		}

		if (mediaEl.tagName === 'VIDEO') {
			playIcon.src = '/img/play_video_icon.png';
			clearTimeout(timeOutVideoMediaControl);
			showVideoControlContainer(
				mediaContainer.querySelector('.video_control_media_block'),
				mediaContainer.querySelector('.select_video_message_block'),
				mediaEl
			);
		}
	}
}

function changeCurrentTimeVideoOrAudio(e, el, elemName, content) {
	let clickCoordX = e.offsetX,
		width = el.getBoundingClientRect().width,
		widthInner = clickCoordX * 100 / width;

	el.querySelector('.' + elemName + '_time_bar_inner').style.width = widthInner + '%';
	content.currentTime = widthInner * content.duration / 100;
}

function updateCurrentTime(el, container, elemName) {
	let currentTime = el.currentTime;
		container.querySelector('.' + elemName + '_time_bar_inner').style.width = (currentTime * 100 / el.duration) + '%';
		container.querySelector('.' + elemName + '_current_time_block').innerHTML = prepareTimeForEl(currentTime);
}

function endVideoOrAudio(playIconEl, playIcon, elemName, container) {
	playIconEl.src = playIcon;
	playIconEl.removeAttribute('data-action_media_icon');
	container.querySelector('.' + elemName + '_time_bar_inner').style.width = 0 + '%';
	// container.querySelector(elemName).currentTime = 0;

	let mediaContainer = playIconEl.closest('.file_container_for_message'),
		mediaEl = mediaContainer.querySelector('.media_el');

	if (mediaEl.tagName === 'VIDEO') {
		clearTimeout(timeOutVideoMediaControl);
		showVideoControlContainer(
			mediaContainer.querySelector('.video_control_media_block'),
			mediaContainer.querySelector('.select_video_message_block'),
			mediaEl
		);
	}
}

function prepareTimeForEl(time) {
	let mainTime =  Math.floor(time),
		minutes = mainTime / 60,
		seconds = mainTime % 60;

	if (String(minutes).length < 2) {
		minutes = Number(String(0) + minutes);
	}

	if (String(seconds).length < 2) {
		seconds = String(0) + seconds;
	}

	return Math.floor(minutes) + ':' + seconds;
}

function selectFile(parentEl, fileType, messageButtonAction) {
	let selectIconList = document.querySelectorAll('.select_icon_message'),
		acceptButton = parentEl.parentElement.querySelector('.select_file_accept_button');

	toggleAcceptButton(parentEl, acceptButton);

	for (let item of selectIconList) {
		item.onclick = function() {
			if (item.classList.contains('selected_file')) {
				item.classList.remove('selected_file');
				item.src = '/img/not_selected_icon.png';
				removeFromSelectedFileList(item);
			} else {
				item.classList.add('selected_file');
				item.src = '/img/selected_icon.png';
				addElToSelectedFileList(item);
			}

			toggleAcceptButton(parentEl, acceptButton);
		}
	}

	acceptButton.onclick = function() {
		closeSubModalWin(parentEl.parentElement.parentElement, parentEl, function() {
			if (selectedFileList.length < 1 && acceptButton.classList.contains('not_active_element')) {
				return;
			}

			acceptButton.classList.add('not_active_element');

			let container = fileMessageBlock.querySelector('.' + acceptButton.getAttribute('data-insert_block'));
			container.parentElement.classList.add('available_file_message_block_active');
			container.innerHTML = '';

			for (let i = 0; i < selectedFileList.length; i++) {
				let controlBlock = selectedFileList[i].querySelector('.select_file_control_block'),
					closeEl = document.createElement('span'),
					closePic = document.createElement('img'),
					divCollection = selectedFileList[i].querySelectorAll('div');

				controlBlock.innerHTML = '';
				closeEl.classList.add('remove_file_block');
				closePic.src = '/img/close_.png';
				closePic.classList.add('remove_file_pic');

				for (let el of divCollection) {
					if (el.hasAttribute('data-css_eskiz-class')) {
						el.classList.add(el.getAttribute('data-css_eskiz-class'));
					}
				}

				if (selectedFileList[i].classList.contains('video_file_message_container')) {
					clearTimeout(timeOutVideoMediaControl);
					let videoControl = selectedFileList[i].querySelector('.video_control_media_block'),
						videoBlock = selectedFileList[i].querySelector('.select_video_message_block');

					videoControl.classList.remove('not_active');
					videoBlock.classList.remove('cursor_none');
				}

				closeEl.onclick = function() {
					this.closest('.file_container_for_message').remove();
					
					if (messageButtonAction) {
						toggleSendMessageButton();
					}

					let availableItem = container.parentElement.querySelector('.file_container_for_message');

					if (!availableItem) {
						container.parentElement.classList.remove('available_file_message_block_active');
					}

					if (container.children.length < 1) {
						container.classList.remove('box_bottom_border');
						removeBorderBottomForChildrenEl(container.parentElement);
					}

					getCountOfFiles();
					// setHeightForPreviousElOfMessageBox();
					FileElement.setHeightForMessagesBlock();
				};

				if (selectedFileList[i].classList.contains('box_bottom_border') && i == selectedFileList.length - 1) {
					selectedFileList[i].classList.remove('box_bottom_border')
				}

				closeEl.append(closePic);
				controlBlock.append(closeEl);
				container.append(selectedFileList[i]);
			}

			setBorderBottomForChildrenEl(container.parentElement)
			selectedFileList.length = 0;
			getCountOfFiles();
			// setHeightForPreviousElOfMessageBox();
			FileElement.setHeightForMessagesBlock();
		});
	}
}

function setBorderBottomForChildrenEl(parentEl) {
	let elList = parentEl.children;

	for (let i = 0; i < elList.length; i++) {
		if (elList[i].children.length > 0 && !elList[i].classList.contains('box_bottom_border') && i < elList.length - 1) {
			for (let j = i + 1; j < elList.length; j++) {
				if (elList[j].children.length > 0 && !elList[i].classList.contains('box_bottom_border')) {
					elList[i].classList.add('box_bottom_border')
				}
			}
		}
	}
}

function removeBorderBottomForChildrenEl(parentEl) {
	let elList = parentEl.children,
		hasChildList = [];

	for (let el of elList) {
		if (el.children.length > 0) {
			hasChildList.push(el);
		}
	}

	if (hasChildList.length < 2) {
		for (let el of elList) {
			el.classList.remove('box_bottom_border')
		}
	}
}

function toggleAcceptButton(parentEl, acceptButton) {
	let selectedIcon = parentEl.querySelector('.selected_file');

	if (selectedIcon) {
		acceptButton.classList.remove('not_active_element');
	} else {
		acceptButton.classList.add('not_active_element');
	}
}

function toggleSendMessageButton() {
	let sendMessageButton = document.querySelector('.send_message_to_user_button'),
		writeMessageField = writeMessageBoxModal.querySelector('.write_message_block>textarea'),
		availableFileEl = fileMessageBlock.querySelector('.file_container_for_message');

	if (availableFileEl) {
		sendMessageButton.classList.remove('not_active_element');
	} else if (!availableFileEl && !writeMessageField.value) {
		sendMessageButton.classList.add('not_active_element');
	}
}

function addElToSelectedFileList(item) {
	let container = item.closest('.file_container_for_message');

	selectedFileList.push(container);
}

function removeFromSelectedFileList(item) {
	let container = item.closest('.file_container_for_message'),
		fileId = container.getAttribute('data-file_id');

	for (let i = 0; i < selectedFileList.length; i++) {
		let elFileId = selectedFileList[i].getAttribute('data-file_id');

		if (elFileId === fileId) {
			selectedFileList.splice(i, 1);
		}
	}
}

function getCountOfFiles() {
	let count = 0;

	for (let el of fileMessageBlock.children) {
		count += el.children.length
	}

	if (count > 0) {
		countOfFilesBlock.innerHTML = 'файлы: ' + count;
		countOfFilesBlock.classList.add('count_of_files_block_active');
	} else {
		countOfFilesBlock.innerHTML = '';
		countOfFilesBlock.classList.remove('count_of_files_block_active');
	}
}

function isEmptyFileContainer() {
	let availableFileEl = fileMessageBlock.querySelector('.file_container_for_message');
	return (availableFileEl) ? true : false;
}

function getFileIdList() {
	let idList = [],
		elemList = fileMessageBlock.querySelectorAll('.file_container_for_message');

	for (let el of elemList) {
		idList.push(el.getAttribute('data-file_id'));
	}

	return idList;
}