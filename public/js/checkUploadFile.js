/*jshint esversion: 6 */

let fileTypeObj = {
	1: {
		'gif': ['image/gif'],
		'jpg': ['image/jpeg'],
		'jpeg': ['image/jpeg'],
		'png': ['image/png']
	},
	2: {
		'aac': ['audio/aac'],
		'mp3': ['audio/mpeg'],
		'ogg': ['audio/ogg'],
		'wav': ['audio/wav', 'audio/x-wav'],
		'mp4': ['audio/mp4'],
		'm4a': ['audio/mp4', 'audio/x-m4a']
	},
	3: {
		'mp4': ['video/mp4'],
	},
	4: {
		'txt': ['text/plain'],
		'doc': ['application/msword'],	
		'docx': ['application/vnd.openxmlformats-officedocument.wordprocessingml.document'],
		'pdf': ['application/pdf'],
		'ppt': ['application/vnd.ms-powerpoint'],
		'pptx': ['application/vnd.openxmlformats-officedocument.presentationml.presentation'],
		'csv': ['text/csv'],
		'csv1': ['application/csv'],
		'xls': ['application/vnd.ms-excel'],
		'xlsx': ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'],
		'zip': ['application/zip, application/octet-stream, application/x-zip-compressed, multipart/x-zip'],
		'rar': ['application/vnd.rar, application/x-rar-compressed, application/octet-stream'],
	}
};

function checkUploadFile(inputEl, fileType) {
	let fileList = inputEl.files,
		countRes = 0;
	for (let i = 0; i < fileList.length; i++) {
		let exec = fileList[i].name.substr(fileList[i].name.lastIndexOf('.') + 1),
			mimeType = fileList[i].type;
		
		for (let key in fileTypeObj[fileType]) {
			if (key === exec && matchArr(fileTypeObj[fileType][exec], mimeType)) {
				countRes++;
			}
		}
	}
	
	if (countRes !== fileList.length) {
		return {'error': getErrorMessage(fileType)};
	}

	return {'error': ''};
}

function matchArr(arr, item) {
	for (let i = 0; i < arr.length; i++) {
		if (item === arr[i]) {
			return true;
		}
	}

	return false;
}

function getErrorMessage(fileType) {
	let result = 'Возможные форматы: ',
		formatList = [];

	for (let key in fileTypeObj[fileType]) {
		formatList.push(key)
	}

	for (let i = 0; i < formatList.length; i++) {
		if (i !== formatList.length - 1) {
			result += formatList[i] + ', '
		} else {
			result += formatList[i]
		}
	}

	return result;
}