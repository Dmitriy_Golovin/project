/*jshint esversion: 6 */

class SelectTimeClass {

	constructor(data)
	{
		this.box = data.box;
		this.hoursBox = this.box.querySelector('.select_time_hours_box');
		this.minutesBox = this.box.querySelector('.select_time_minutes_box');
		this.touchStartPointY = 0;
		this.swipeDifference = 0;
	}

	init()
	{
		this.applyScroll();
	}

	applyScroll()
	{
		this.hoursBox.addEventListener('wheel', function(e) {
			let speed = 1;
			if (e.deltaY > 0 && this.hoursBox.scrollTop + this.hoursBox.offsetHeight < this.hoursBox.scrollHeight) {
				let interval = setInterval(function() {
					this.hoursBox.scrollTop += speed;
				}.bind(this), 1);

				setTimeout(function() {
					clearInterval(interval);
				}.bind(this), 150);
			}

			if (e.deltaY < 0 && this.hoursBox.scrollTop > 0) {
				let interval = setInterval(function() {
					this.hoursBox.scrollTop -= speed;
				}.bind(this), 1);

				setTimeout(function() {
					clearInterval(interval);
				}.bind(this), 150);
			}
		}.bind(this));

		this.minutesBox.addEventListener('wheel', function(e) {
			let speed = 1;
			if (e.deltaY > 0 && this.minutesBox.scrollTop + this.minutesBox.offsetHeight < this.minutesBox.scrollHeight) {
				let interval = setInterval(function() {
					this.minutesBox.scrollTop += speed;
				}.bind(this), 1);

				setTimeout(function() {
					clearInterval(interval);
				}.bind(this), 150);
			}

			if (e.deltaY < 0 && this.minutesBox.scrollTop > 0) {
				let interval = setInterval(function() {
					this.minutesBox.scrollTop -= speed;
				}.bind(this), 1);

				setTimeout(function() {
					clearInterval(interval);
				}.bind(this), 150);
			}
		}.bind(this));

		if ('ontouchmove' in this.hoursBox) {
			this.hoursBox.addEventListener('touchmove', this.getSwipeDistance.bind(this), false);
			this.minutesBox.addEventListener('touchmove', this.getSwipeDistance.bind(this), false);
		}
	}

	getSwipeDistance(e, element) {
		let newCoordPointY = e.touches[0].clientY,
			previousSwipeDiffernce = this.swipeDifference;

		this.swipeDifference = newCoordPointY - this.touchStartPointY;
		if (this.swipeDifference < previousSwipeDiffernce) {
			e.target.parentElement.scrollTop += 28;
		}

		if (this.swipeDifference > previousSwipeDiffernce) {
			e.target.parentElement.scrollTop -= 28;
		}
	}
}