let intetvalPreloader,
	count = 0;

function activateChangeAvatarModalBox() {
	let modal = document.querySelector('.modal'),
		modalBox = document.querySelector('.modal_box'),
		closePicList = modalBox.querySelectorAll('.close_win_modal'),
		loadNewAva = document.querySelector('#load_new_ava'),
		changeAvatarBoxModal = document.querySelector('.change_avatar_box_modal'),
		selectAva = document.querySelector('.change_ava_select_menu_button'),
		cancelSelectAva = document.querySelector('.select_ava_cancel_button'),
		acceptSelectAva = document.querySelector('.select_ava_accept_button'),
		containerLoadAva = document.querySelector('.container_load_avatar'),
		containerSelectAva = document.querySelector('.container_select_avatar'),
		containerAvailableImage = document.querySelector('.container_available_image');

	modal.classList.add('modal_active');
	modalBox.classList.add('modal_box_active');
	
	for (let el of closePicList) {
		el.onclick = function() {
			if (el.parentElement != containerLoadAva) {
				closeSubModalWin(containerSelectAva, containerAvailableImage);
			}

			modalBox.classList.remove('modal_box_active');
			removeNoticeError();

			setTimeout(function() {
				modal.classList.remove('modal_active');
			}, 300);
		};
	}

	loadNewAva.onchange = function() {
		let checkResult = checkUploadFile(this, 1);

		if (checkResult['error']) {
			let noticeElem = containerLoadAva.querySelector('.notice_ava_extension');
			noticeElem.innerHTML = checkResult['error'];
			noticeElem.classList.add('notice_ava_extension_active');
			this.value = '';

			modal.onclick = function() {
				if (noticeElem.classList.contains('notice_ava_extension_active')) {
					noticeElem.classList.remove('notice_ava_extension_active');
					noticeElem.innerHTML = '';
				}
			}
		} else {
			ajaxLoadAvatar(loadNewAva, function() {});
		}
	}

	loadNewAva.onclick = function() {
		removeNoticeError();
	}

	selectAva.onclick = function() {
		preloaderAction('add');
		removeNoticeError();
		containerLoadAva.classList.add('not_active_opacity');

		ajaxGetUserPublicFiles(1, function(imageList) {
			showPublicImageList(imageList, containerAvailableImage);
			containerLoadAva.ontransitionend = function() {
				if (this.classList.contains('not_active_opacity')) {
					this.classList.add('modal_block_not_active');
					containerSelectAva.classList.remove('modal_block_not_active');
					
					containerSelectAva.ontransitionend = function() {
						preloaderAction('remove')
						if (!containerSelectAva.classList.contains('modal_block_not_active')) {
							containerSelectAva.classList.remove('not_active_opacity');
						}
					}
				}
			}
		})
	}

	cancelSelectAva.onclick = function() {
		closeSubModalWin(containerSelectAva, containerAvailableImage);
	}

	acceptSelectAva.onclick = function() {
		activeImg = containerAvailableImage.querySelector('.selected_file').parentElement.parentElement.parentElement.querySelector('.change_ava_available_image');

		if (activeImg) {
			ajaxSelectAvatar(activeImg, function() {
				closeSubModalWin(containerSelectAva, containerAvailableImage);
			})
		}
	}

	function closeSubModalWin(itemBlock, containerFile) {
		itemBlock.classList.add('not_active_opacity');
		preloaderAction('add');
		containerFile.innerHTML = '';

		itemBlock.ontransitionend = function() {
			if (this.classList.contains('not_active_opacity')) {
				this.classList.add('modal_block_not_active');
				containerLoadAva.classList.remove('modal_block_not_active');
				
				containerLoadAva.ontransitionend = function() {
					preloaderAction('remove');
					if (!containerLoadAva.classList.contains('modal_block_not_active')) {
						containerLoadAva.classList.remove('not_active_opacity');
					}
				}
			}
		}
	}
}

function removeNoticeError() {
	let noticeAvaExtension = document.querySelector('.notice_ava_extension');

	noticeAvaExtension.classList.remove('notice_ava_extension_active');
	noticeAvaExtension.innerHTML = '';
}

function showPublicImageList(imageList, parentEl) {
	if (imageList.length <= 0) {
		let noIamgeEl = document.createElement('h3');

		noIamgeEl.innerHTML = 'У вас нет доступных изображений  ';
		parentEl.append(noIamgeEl);
		return;
	}

	for (let i = 0; i < imageList.length; i++) {
		let container = document.createElement('div'),
			controlBlock = document.createElement('div'),
			imgBlock = document.createElement('div'),
			img = document.createElement('img');

		container.classList.add('image_file_message_container');
		controlBlock.classList.add('select_file_control_block');
		img.classList.add('change_ava_available_image');
		img.src = imageList[i]['url'];
		imgBlock.classList.add('select_image_message_block');
		container.classList.add('file_container_for_message');
		img.setAttribute('data-file_id', imageList[i]['file_id']);
		
		imgBlock.append(img);
		controlBlock.append(FileElement.createIconSelectElement(false));
		container.append(controlBlock);
		container.append(imgBlock);

		parentEl.append(container);
	}

	setAvaActiveClassForImage(parentEl.parentElement);
}

function setAvaActiveClassForImage(parentEl) {
	let selectIconList = document.querySelectorAll('.select_icon_message'),
		acceptButton = document.querySelector('.select_ava_accept_button');

	for (let item of selectIconList) {
		item.onclick = function() {
			removeAvaActiveClassfromImageList(selectIconList)
			if (item.classList.contains('selected_file')) {
				item.classList.remove('selected_file');
				item.src = '/img/not_selected_icon.png';
			} else {
				item.classList.add('selected_file');
				item.src = '/img/selected_icon.png';
			}

			toggleAcceptButton(parentEl, acceptButton);
		}
	}
}

function toggleAcceptButton(parentEl, acceptButton) {
	let selectedIcon = parentEl.querySelector('.select_ava_accept_button');

	if (selectedIcon) {
		acceptButton.classList.remove('not_active_element');
	} else {
		acceptButton.classList.add('not_active_element');
	}
}

function removeAvaActiveClassfromImageList(imgList) {
	for (let el of imgList) {
		el.parentElement.classList.remove('selected_file');
		el.src = '/img/not_selected_icon.png';
	}
}