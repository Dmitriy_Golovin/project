/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	profileId = mainBox.dataset.profile_id,
	type = mainBox.dataset.type,
	subType = mainBox.dataset.subtype,
	profileEventContentBlock = mainBox.querySelector('.event_content_block'),
	scrollProfileEventList = true;

window.addEventListener('load', function() {
	let dataObj = {
		'profileId': profileId,
		'type': type,
		'subType': subType,
		'profileEventContentBlock': profileEventContentBlock,
	};

	fetchGetProfileMainEventList(dataObj);
});

window.addEventListener('resize', function() {
	if (document.body.offsetWidth > 650 && document.querySelector('.profile_menu_container') &&
		document.querySelector('.profile_menu_container').classList.contains('menu_container_active')) {
		document.querySelector('.profile_menu_container').classList.remove('menu_container_active');

		if (!document.querySelector('.close_menu_container').classList.contains('not_active')) {
			document.querySelector('.close_menu_container').classList.add('not_active');
		}
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('menu_icon_container') || e.target.closest('.menu_icon_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.add('menu_container_active');
		closeMenuEl.classList.remove('not_active');
	}

	if (e.target.classList.contains('close_menu_container') || e.target.closest('.close_menu_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.remove('menu_container_active');

		setTimeout(function() {
			closeMenuEl.classList.add('not_active');
		}, 200);
	}
});