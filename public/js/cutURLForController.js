function getUrlForAjax() {
	var url = location['href'].split('/')[3],
		endOfLength = url.indexOf('?');	
	if (endOfLength === -1) endOfLength = url.length;	
	var res = url.slice(0, endOfLength);

	if (res == '') {
		res = 'index';
	}
	return res;
}