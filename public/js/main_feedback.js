/*jshint esversion: 6 */

let submitButton = document.querySelector('.feedback_button_submit'),
	successfullySent = document.querySelector('.feedback_successfully_sent');

submitButton.addEventListener('click', function() {
	appendInputCR(document.querySelector('form'));
	document.querySelector('form').submit();
});

if (successfullySent) {
	setTimeout(function() {
		successfullySent.remove();
	}, 3000);
}