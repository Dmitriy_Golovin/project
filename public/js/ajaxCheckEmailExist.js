function ajaxCheckEmailExist(field, emailExists, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			emailNoticeElem = document.querySelector('.' + field.getAttribute('name')),
			data = 'email=' + field.value + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();
				
				if (result['res'] == 1) {
					emailExists['res'] = true;
					callback();
				} else if (result['res'] == 0){
					emailNoticeElem.innerHTML = result['errors'];
					emailNoticeElem.classList.add('notice_elem_active');
					field.classList.add('error_input');
					field.classList.remove('valid_input');
					emailExists['res'] = false;
				}
			}
		};

		xhr.open('POST', '/auth/emailExists');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}
