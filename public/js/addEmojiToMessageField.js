function addEmojiToMessageField(emojiContainer, messageField, sendMessageButton) {
	let emojiBox = emojiContainer.querySelector('.emoji_box_message_page'),
		emojiElList = emojiContainer.querySelectorAll('li'),
		messagesBlockWithTextField = document.querySelector('.messages_block'),
		writeMessageBoxMessagePage = document.querySelector('.write_message_box'),
		availableFileMessageBlock = document.querySelector('.available_file_message_block');

	emojiContainer.addEventListener('click', function(e) {
		if (e.target == document.querySelector('.emoji_icon') && emojiBox.classList.contains('menu_box_active')) {
			closeEmojiContainer();
		} else if (e.target == document.querySelector('.emoji_icon') && !emojiBox.classList.contains('menu_box_active')) {
			emojiBox.classList.add('menu_box_active');
			emojiBox.parentElement.classList.add('menu_box_active');

			if (!availableFileMessageBlock.classList.contains('available_file_message_block_active')) {
				new ResizeObserver(scrollDownElement.bind(null, messageField)).observe(messageField);
				new ResizeObserver(scrollDownElement.bind(null, messagesBlockWithTextField)).observe(messageField.parentElement);
				messagesBlockWithTextField.style.setProperty('--transitionEl', 0.3 + 's');
				setHeightForElementEmojiActive(messagesBlockWithTextField, 'height', messagesBlockWithTextField.parentElement.offsetHeight - searchMessagesBlock.offsetHeight - (messageField.offsetHeight + 130) - 20, '+');
				setHeightForElementEmojiActive(messageField.parentElement, 'minHeight', messageField.offsetHeight + 100, '+');
			}
		}
	})
	
	document.body.addEventListener('click', function(e) {
		if (e.target.closest('.emoji_block') !== emojiContainer && !e.target.classList.contains('select_file_accept_button')) {
			if (!writeMessageBlock.classList.contains('not_active') && emojiBox.classList.contains('menu_box_active')) {
				closeEmojiContainer();
			}
		}
	})

	for (let el of emojiElList) {
		el.addEventListener('click', function() {
			let emoji = el.innerHTML
				existMessage = messageField.innerHTML,
				range = document.createRange(),
    			sel = window.getSelection();

			messageField.focus();

			if (cursorPositionInMessageField > 0) {
				let firstPart = existMessage.slice(0, cursorPositionInMessageField),
					secondPart = existMessage.slice(cursorPositionInMessageField);

				messageField.innerHTML = firstPart + emoji + secondPart;
				range.setStart(messageField.childNodes[0], firstPart.length + emoji.length)
			    range.collapse(true)
			    
			    sel.removeAllRanges()
			    sel.addRange(range)
			} else {
				messageField.innerHTML = existMessage + emoji;
				range.setStart(messageField.childNodes[0], messageField.innerHTML.length)
			    range.collapse(true)
			    
			    sel.removeAllRanges()
			    sel.addRange(range)
			}

			cursorPositionInMessageField = getCaretPosition(messageField);
		})
	}

	function closeEmojiContainer() {
		emojiBox.classList.remove('menu_box_active');
		emojiBox.parentElement.classList.remove('menu_box_active');

		if (!availableFileMessageBlock.classList.contains('available_file_message_block_active')) {
			setHeightForElementEmojiActive(messagesBlockWithTextField, 'height', messagesBlockWithTextField.parentElement.offsetHeight - (messageField.offsetHeight + 75), '-');
			setHeightForElementEmojiActive(messageField.parentElement, 'minHeight', null, '-');
		}
	}
}

function scrollDownElement(element) {
	let scrollBlock = element.scrollHeight - element.getBoundingClientRect().height;
	element.scrollTop = scrollBlock;
}

function setHeightForElementEmojiActive(element, property, value, mathAction) {
	if (mathAction === '+') {
		element.style[property] = value + 'px';
	} else if (mathAction === '-') {
		(value !== null) ? element.style[property] = value + 'px' : element.style[property] = value;
	}
}

function getCaretPosition(editableDiv) {
	var sel, range;
	
	if (window.getSelection) {
    	sel = window.getSelection();
    	if (sel.rangeCount) {
      		range = sel.getRangeAt(0);
      		if (range.commonAncestorContainer.parentNode == editableDiv) {
        		cursorPositionInMessageField = range.endOffset;
      		}
    	}
  	} else if (document.selection && document.selection.createRange) {
    	range = document.selection.createRange();
    	if (range.parentElement() == editableDiv) {
	      	var tempEl = document.createElement("span");
	      	editableDiv.insertBefore(tempEl, editableDiv.firstChild);
	      	var tempRange = range.duplicate();
	      	tempRange.moveToElementText(tempEl);
	      	tempRange.setEndPoint("EndToEnd", range);
	      	cursorPositionInMessageField = tempRange.text.length;
    	}
  	}

	return cursorPositionInMessageField;
}