function ajaxSelectAvatar(img, callback) {
	tab.xjscsrf(function() {
		let xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			currentAva = document.querySelector('.current_ava_container>img'),
			data = 'imgId=' + img.getAttribute('data-file_id') + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onload = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				let result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();
				currentAva.src = img.getAttribute('src');
				
				if (result['res'] == 1) {
					let dataForTabSync = {
						'ws_tab_sync_to_do': 'change_personal_data',
						'firstName': false,
						'lastName': false,
						'country': false,
						'city': false,
						'pathToAva': result.data.path_to_ava
					}
					tab.send(tab.tabSync.collectData(dataForTabSync));
					tab.tabSync.run(dataForTabSync)
					callback();
				} else {
					console.log(result.errors)
				}
			}
		};

		xhr.open('POST', '/user/selectAvatar');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.send(data);
	});
}