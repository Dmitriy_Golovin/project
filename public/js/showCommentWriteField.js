/*jshint esversion: 6 */

function showCommentWriteField(commentFieldContainer) {
	let commentFieldContainerParentElRect = commentFieldContainer.parentElement.getBoundingClientRect();

	if (commentFieldContainerParentElRect.top <= window.innerHeight - commentFieldContainer.offsetHeight - 30 &&
		commentFieldContainerParentElRect.bottom > window.innerHeight) {
		commentFieldContainer.classList.add('comment_field_container_visible');
		commentFieldContainer.classList.remove('comment_field_container_visible_bottom');
	} else if (commentFieldContainerParentElRect.bottom <= window.innerHeight) {
		commentFieldContainer.classList.add('comment_field_container_visible_bottom');
		commentFieldContainer.classList.remove('comment_field_container_visible');
	} else {
		commentFieldContainer.classList.remove('comment_field_container_visible');
		commentFieldContainer.classList.remove('comment_field_container_visible_bottom');
	}
}

function setPaddingBottomForListContainer(commentFieldContainer) {
	if (commentFieldContainer) {
		new ResizeObserver(setPadding).observe(commentFieldContainer);

		commentFieldContainer.querySelector('.comment_field').oninput = function() {
			if (this.textContent.length < 1) {
				this.innerHTML = '';
			}
		};
	}

	function setPadding() {
		commentQuerySearch = commentFieldContainer.closest('.comment_query_search') ||
			commentFieldContainer.parentElement.querySelector('.comment_query_search');
		commentQuerySearch.style.paddingBottom = commentFieldContainer.offsetHeight + 'px';
	}
}