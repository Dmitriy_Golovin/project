/*jshint esversion: 6 */

const typeAny = 1;
const typeNowAndFuture = 2;

class CalendarClass {

	constructor(data)
	{
		this.titleDateEl = data.titleDateEl;
		this.tableEl = data.tableEl;
		this.tdList = this.tableEl.querySelectorAll('td');
		this.prevArrow = data.prevArrow;
		this.nextArrow = data.nextArrow;
		this.date = new Date();
		this.count = 0;
		this.type = data.type;
	}

	static typeAny()
	{
		return typeAny;
	}

	static typeNowAndFuture()
	{
		return typeNowAndFuture;
	}

	init()
	{
		this.show();

		this.prevArrow.onclick = this.prevMonth.bind(this);
		this.nextArrow.onclick = this.nextMonth.bind(this);
	}

	getDateInTitle()
	{
		let date = new Date(this.date.getFullYear(), this.date.getMonth() + 1 + this.count, 0),
			month = date.getMonth() + 1,
			year = date.getFullYear();

		if ((month + '').length < 2) {
			month = '0' + month;
		}

		this.prevArrow.classList.remove('not_active_element');
		this.titleDateEl.innerHTML = month + '.' + year;
	}

	show()
	{
		this.getDateInTitle();

		let totalDays = new Date(this.date.getFullYear(), this.date.getMonth() + 1 + this.count, 0).getDate(),
        	firstDayWeek = new Date(this.date.getFullYear(), this.date.getMonth() + this.count, 1).getDay(),
        	lastDayWeek = new Date(this.date.getFullYear(), this.date.getMonth() + this.count, totalDays).getDay(),
	    	today = this.date.getDate(),
	    	prevTotalDays = new Date(this.date.getFullYear(), this.date.getMonth() + this.count, 0).getDate(),
	    	prevLastDayWeek = new Date(this.date.getFullYear(), this.date.getMonth() - 1 + this.count, prevTotalDays).getDay(),
	    	nextFirstDayWeek = new Date(this.date.getFullYear(), this.date.getMonth() + 1 + this.count, 1).getDay();

	    if (firstDayWeek == 0) {
            firstDayWeek = 7;
        }

        if (lastDayWeek == 0) {
            lastDayWeek = 7;
        }

        //числа предыдущего месяца
	    if (firstDayWeek != 1) {
	        for (let i = 0, j = prevTotalDays - prevLastDayWeek + 1; i < prevLastDayWeek, j <= prevTotalDays; i++, j++) {
	        	let date = new Date(this.date.getFullYear(), this.date.getMonth() + 1 - 1 + this.count, 0),
			    	month = date.getMonth() + 1,
					year = date.getFullYear();
					
	        	if ((month + '').length < 2) {
					month = '0' + month;
				}

	            this.tdList[i].innerHTML = j;
	            this.tdList[i].classList.remove('calendar_current_month_cell');
	            this.tdList[i].classList.remove('not_active_element');
	            this.tdList[i].classList.add('calendar_not_current_month_cell');
	            this.tdList[i].setAttribute('data-filter_date', j + '-' + month + '-' + year);

	            if (this.type === CalendarClass.typeNowAndFuture()) {
	            	this.setNotActiveCellForPast(year, month, j, this.tdList[i]);
	            }
	        }
        } else {
	        for (let i = 0, j = prevTotalDays - 7 + 1; i < 7, j <= prevTotalDays; i++, j++) {
	            let date = new Date(this.date.getFullYear(), this.date.getMonth() + 1 - 1 + this.count, 0),
			    	month = date.getMonth() + 1,
					year = date.getFullYear();
					
	        	if ((month + '').length < 2) {
					month = '0' + month;
				}

	            this.tdList[i].innerHTML = j;
	            this.tdList[i].classList.remove('calendar_current_month_cell');
	            this.tdList[i].classList.remove('not_active_element');
	            this.tdList[i].classList.add('calendar_not_current_month_cell');
	            this.tdList[i].setAttribute('data-filter_date', j + '-' + month + '-' + year);

	            if (this.type === CalendarClass.typeNowAndFuture()) {
	            	this.setNotActiveCellForPast(year, month, j, this.tdList[i]);
	            }
	        }
	    }

        //числа текущего месяца
	    let n = 0;

	    if (firstDayWeek == 1) {
	        n = 7;
	    }

        for (let i = firstDayWeek + n - 1, j = 1; i < this.tdList.length - lastDayWeek + n - 1, j <= totalDays; i++, j++) {
        	let date = new Date(this.date.getFullYear(), this.date.getMonth() + 1 + this.count, 0),
		    	month = date.getMonth() + 1,
				year = date.getFullYear();

        	if ((month + '').length < 2) {
				month = '0' + month;
			}

			this.tdList[i].innerHTML = j;
			this.tdList[i].classList.remove('calendar_not_current_month_cell');
			this.tdList[i].classList.remove('not_active_element');
			this.tdList[i].classList.add('calendar_current_month_cell');
			this.tdList[i].setAttribute('data-filter_date', j + '-' + month + '-' + year);

			if (this.type === CalendarClass.typeNowAndFuture()) {
            	this.setNotActiveCellForPast(year, month, j, this.tdList[i]);
            }
        }

        //числа следующего месяца
	    if (firstDayWeek != 1) {
	        for (let i = prevLastDayWeek + totalDays, j = 1; i < this.tdList.length, j <= this.tdList.length - (prevLastDayWeek + totalDays); i++, j++) {
	            let date = new Date(this.date.getFullYear(), this.date.getMonth() + 1 + 1 + this.count, 0),
			    	month = date.getMonth() + 1,
					year = date.getFullYear();
					
	        	if ((month + '').length < 2) {
					month = '0' + month;
				}

	            this.tdList[i].innerHTML = j;
	            this.tdList[i].classList.remove('calendar_current_month_cell');
	            this.tdList[i].classList.remove('not_active_element');
	            this.tdList[i].classList.add('calendar_not_current_month_cell');
	            this.tdList[i].setAttribute('data-filter_date', j + '-' + month + '-' + year);

	            if (this.type === CalendarClass.typeNowAndFuture()) {
	            	this.setNotActiveCellForPast(year, month, j, this.tdList[i]);
	            }
	        }
	    } else {
	        for (let i = prevLastDayWeek + totalDays + 7, j = 1; i < this.tdList.length + 7, j <= this.tdList.length - (prevLastDayWeek + totalDays + 7); i++, j++) {
	            let date = new Date(this.date.getFullYear(), this.date.getMonth() + 1 + 1 + this.count, 0),
			    	month = date.getMonth() + 1,
					year = date.getFullYear();
					
	        	if ((month + '').length < 2) {
					month = '0' + month;
				}

	            this.tdList[i].innerHTML = j;
	            this.tdList[i].classList.remove('calendar_current_month_cell');
	            this.tdList[i].classList.remove('not_active_element');
	            this.tdList[i].classList.add('calendar_not_current_month_cell');
	            this.tdList[i].setAttribute('data-filter_date', j + '-' + month + '-' + year);

	            if (this.type === CalendarClass.typeNowAndFuture()) {
	            	this.setNotActiveCellForPast(year, month, j, this.tdList[i]);
	            }
	        }
	    }
	}

	nextMonth()
	{
		this.count++;
		this.show();
	}

	prevMonth()
	{
		this.count--;
		this.show();
	}

	setNotActiveCellForPast(year, month, day, cell)
	{
		let today = new Date(this.date.getFullYear(), this.date.getMonth(), this.date.getDate()).valueOf(),
			calendarDate = new Date(year, month - 1, day).valueOf();

		if (calendarDate < today) {
			cell.classList.add('not_active_element');

			if (!this.prevArrow.classList.contains('not_active_element')) {
				this.prevArrow.classList.add('not_active_element');
			}
		}
	}
}