/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	articleSectionId = mainBox.dataset.article_section_id,
	articleContentBlock = mainBox.querySelector('.aticle_content_block'),
	searchArticleField = document.querySelector('.search_article_field'),
	articleSearchSubmit = document.querySelector('.article_search_submit'),
	scrollArticleList = true;

window.addEventListener('load', function() {
	let articleSectionData = {
		'articleSectionId' : articleSectionId,
	};

	fetchGetArticleList(articleSectionData);
});

articleSearchSubmit.addEventListener('click', function() {
	if (!searchArticleField.value) {
		return;
	}

	let articleSectionData = {
		'articleSectionId' : articleSectionId,
		'searchString': searchArticleField.value,
	};

	fetchGetArticleList(articleSectionData);
});

searchArticleField.addEventListener('input', function() {
	if (!searchArticleField.value) {
		let articleSectionData = {
			'articleSectionId' : articleSectionId,
		};

		fetchGetArticleList(articleSectionData);
	}
});