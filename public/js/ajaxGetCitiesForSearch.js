function ajaxGetCitiesForSearch(countryId) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			citySelectSearchEl = document.querySelector('select[name=city_for_search]'),
			citySelectPersDataEl = document.querySelector('select[name=reg_city]'),
			data = 'country_id=' + countryId + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 1) {
					if (citySelectSearchEl) citySelectSearchEl.innerHTML = result['html'];				
					if (citySelectPersDataEl) citySelectPersDataEl.innerHTML = result['html'];				
				} else if (result['res'] == 0){
					console.log(0);	
				}
			}
		};

		xhr.open('POST', '/city/getCitiesForThisCountry');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	})
}