function ajaxGetUserPublicFiles(type, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
			data = 'fileType=' + type + '&' + 'timeOffsetSeconds=' + timeOffsetSeconds + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				let result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res']) {
					callback(result['fileList']);
				} else {
					console.log(result['errors'])
				}
			}
		};

		xhr.open('POST', '/file/getFileListByType');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}