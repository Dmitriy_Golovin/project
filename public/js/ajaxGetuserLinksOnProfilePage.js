function ajaxGetUserLinksOnProfilePage(friendMenuItem, parentEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'userPageId=' + getuserIDFromURL() + '&' +'friendMenuItem=' + friendMenuItem + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var response = JSON.parse(xhr.responseText),
					result = response['data'];
				csrfVal.setAttribute('content', response['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 'friends') {
					showUsersBySelectedItem(result['list'], parentEl, result['selfID'], result['res'], {'friendMenuItem': friendMenuItem, 'parentEl': parentEl});
					showFriendsAmount(result['friendsAmount'], parentEl, result['selfID']);
				} else if (result['res'] == 'friends_requests') {
					showFriendsAmount(result['friendsAmount'], parentEl, result['selfID']);
					showUsersBySelectedItem(result['list'], parentEl, result['selfID'], result['res'], {'friendMenuItem': friendMenuItem, 'parentEl': parentEl});
				} else if (result['res'] == 'friends_subscriptions') {
					showUsersBySelectedItem(result['list'], parentEl, result['selfID'], result['res'], {'friendMenuItem': friendMenuItem, 'parentEl': parentEl});
					showFriendsAmount(result['friendsAmount'], parentEl, result['selfID']);
				} else if (result['res'] == 0) {
					console.log('bad');
				}
			}
		};

		xhr.open('POST', '/user/getFriendshipUserList');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function getuserIDFromURL() {
	var url = document.location.pathname
		arrUrl = url.split('/');;
		
	if (arrUrl[2]) return arrUrl[2];
	else return '';
}

function showFriendsAmount(friendasAmountObj, el, selfID) {
	var elFriendsMenuList = el.parentElement.children[0].children;

	for (var key in friendasAmountObj) {
		for (var i = 0; i < elFriendsMenuList.length; i++) {
			var innerEl = (elFriendsMenuList[i].innerHTML.indexOf(' ') > -1) ? elFriendsMenuList[i].innerHTML.slice(0, elFriendsMenuList[i].innerHTML.indexOf(' ')) : elFriendsMenuList[i].innerHTML;
			
			var attrValue = elFriendsMenuList[i].getAttribute('data-friends_menu_item');
			if (attrValue === key) {
				elFriendsMenuList[i].innerHTML = (friendasAmountObj[key]) ? innerEl + ' ' + friendasAmountObj[key] : innerEl;
			}
		}
	}
}

function showUsersBySelectedItem(resultList, parentEl, selfID, menuItem, ajaxDataProfilePage) {
	
	parentEl.innerHTML = '';
	if (menuItem == 'friends_requests' && !document.querySelector('.new_subscriber_box')) {
		var newSubscriberBox = document.createElement('div');
		newSubscriberBox.classList.add('new_subscriber_box');
		parentEl.appendChild(newSubscriberBox);
	} else {
		var newSubscriberBox = document.querySelector('.new_subscriber_box');
	}

	for (var key in resultList) {
		var itemResult = document.createElement('div');
		itemResult.classList.add('box_bottom_border');
		itemResult.classList.add('search_main_result_item');
		itemResult.classList.add('input_search_item');
		itemResult.setAttribute('data-profile_ID', resultList[key]['user_id']);
		var avaBox = document.createElement('div'),
			userNameAndButtonBox = document.createElement('div'),
			linkAva = document.createElement('a'),
			linkUserName = document.createElement('a'),
			ava = document.createElement('img'),
			userNameBox = document.createElement('div'),
			userName = document.createElement('span'),
			buttonBox = document.createElement('div'),
			buttonContainer = document.createElement('div');

		avaBox.classList.add('ava_box_search_result');
		userNameAndButtonBox.classList.add('user_name_and_button_box_search_result');
		userNameBox.classList.add('user_name_box_search_result');
		userName.classList.add('user_name_search_result');
		buttonBox.classList.add('button_box_search_result');
		buttonContainer.classList.add('button_container_search_result');
		linkAva.setAttribute('href', '/profile/' + resultList[key]['user_id']);
		linkUserName.setAttribute('href', '/profile/' + resultList[key]['user_id']);
		ava.setAttribute('src', resultList[key]['path_to_ava']);
		ava.classList.add('ava_search_result');
		userName.innerHTML = resultList[key]['first_name'] + ' ' + resultList[key]['last_name'];
		linkAva.appendChild(ava);
		avaBox.appendChild(linkAva);
		itemResult.appendChild(avaBox);
		linkUserName.appendChild(userName);
		userNameBox.appendChild(linkUserName);
		buttonBox.append(buttonContainer);
		
		if (resultList[key]['newSubscriber'] == 1 && getuserIDFromURL() == selfID) {	
			itemResult.classList.add('new_subscriber_result_item');
			tab.tabSync._minorCreateButtonForUserItem(buttonBox, 'Принять', resultList[key]['user_id'], 'accept_button', 'accept_friendship');
			tab.tabSync._minorCreateButtonForUserItem(buttonBox, 'Отклонить', resultList[key]['user_id'], 'reject_button', 'reject_friendship');
			newSubscriberBox.appendChild(itemResult);
		} else {
			tab.tabSync._minorCreateButtonForUserItem(buttonBox, 'Сообщение', resultList[key]['user_id'], 'send_message_button');
			tab.tabSync._minorCreateButtonForUserItem(buttonBox, 'Пригласить', resultList[key]['user_id'], 'invite_button');
			
			if (selfID && getuserIDFromURL() == selfID) {
				tab.tabSync._minorCreateHiddenMenuForUserItem(buttonBox, menuItem, resultList[key]['user_id']);
			}
			parentEl.appendChild(itemResult);
		}
		
		userNameAndButtonBox.appendChild(userNameBox);
		
		if (selfID && resultList[key]['user_id'] !== selfID) {
			userNameAndButtonBox.appendChild(buttonBox);
		}
		
		itemResult.appendChild(userNameAndButtonBox);

	}
	
	tab.tabSync._minorShowHideBurgerUserFriendMenu();
	var buttonList = document.querySelectorAll('.profile_button_event'),
		messageButtonList = document.querySelectorAll('.send_message_button');
	
	//tab.tabSync._minorAddEventOnMessageButton(messageButtonList);
	tab.tabSync._minorAddEventOnUserItemInteractionButton(buttonList, ajaxDataProfilePage['friendMenuItem']);
}

function showNewSubscriberAmountOnProfilePage(parentEl, amount) {
	var span = document.createElement('span');
	span.classList.add('new_subscriber_amount');
	span.innerHTML = amount;
	parentEl.appendChild(span);
}