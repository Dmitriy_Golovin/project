/*jshint esversion: 6 */

var elemInsideMenuBox = document.querySelectorAll('.inside_menu_box'),
	userElement = document.querySelector('.personal_data_block'),
	userElementArrow = document.querySelector('.pers_data_arrow'),
	modalContentBox = document.querySelector('.modal_content_box'),
	//personDataBut = document.querySelector('.person_data_but'),
	passwordAndSafetyBut = document.querySelector('.password_and_safety'),
	//settingsBut = document.querySelector('.settings'),
	userMenuList = document.querySelectorAll('.menu_user_item'),
	iconBurgerMenu = document.querySelector('.burger_menu'),
	elemOnTheRight = document.querySelector('.autorized_or_personal_block'),
	burgerMenuHeader = document.querySelector('.burger_menu_header_box'),
	mainMenu = document.querySelector('.menu_user_main'),
	burgerMenu = document.querySelector('.menu_user_burger'),
	searchSubmitutton = document.querySelector('.search_submit'),
	searchField = document.querySelector('.search_field'),
	winWidth = document.documentElement.offsetWidth;

window.addEventListener('load', function() {
	setDataAttributePositionForUsermenuItem(userMenuList);
	showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
});

window.addEventListener('resize', function(e) {
	if (winWidth !== document.documentElement.offsetWidth) {
		winWidth = document.documentElement.offsetWidth;
		showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
		//hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e, burgerMenu);
	}
});

function eventChangingTheContentOfTheHeaderBlock(){
	showHideElemMenuList(userMenuList, iconBurgerMenu, elemOnTheRight, mainMenu, burgerMenu);
}

document.body.addEventListener('click', function(e) {
	if (userElement && e.target !== userElement && e.target !== userElementArrow && e.target !== document.querySelector('.user_full_name') && e.target !== document.querySelector('.box_pers_data_arrow')) {
		removePersonalMenu(elemInsideMenuBox, e);
	}

	if (e.target.classList.contains('menu_user_icon')) {
			let modalNotification = document.querySelector('.modal_notification'),
				modalNotificationBox = modalNotification.querySelector('.modal_notification_box'),
				closeModalPic = modalNotification.querySelector('.close_pic_modal'),
				nothingFoundEl = modalNotification.querySelector('.nothing_found'),
				notificationContentBox = modalNotification.querySelector('.modal_notification_content_box'),
				newNotificationAmount = e.target.parentElement.querySelector('.new_notification_amount'),
				scrollWidth = getScrollWidth();

			modalNotification.classList.add('modal_active');
			modalNotificationBox.classList.add('modal_box_active');
			document.body.classList.add('not_scroll');
			document.body.style.paddingRight = scrollWidth + 'px';
			document.querySelector('.autorized_or_personal_block').style.paddingRight = scrollWidth + 'px';

			closeModalPic.onclick = function() {
				modalNotificationBox.classList.remove('modal_box_active');
				document.body.classList.remove('not_scroll');
				document.body.style.paddingRight = '';
				document.querySelector('.autorized_or_personal_block').style.paddingRight = '';
				
				setTimeout(function() {
					for (let el of notificationContentBox.children) {
						if (el !== nothingFoundEl && el !== closeModalPic.parentElement) {
							el.remove();
						}
					}

					if (newNotificationAmount) {
						newNotificationAmount.remove();
					}

					nothingFoundEl.classList.remove('not_active');
					modalNotification.classList.remove('modal_active');
				}, 300);
			};
	}
	
	hideBurgerMenu(burgerMenuHeader, iconBurgerMenu, e, burgerMenu);
});

if (iconBurgerMenu) {
	iconBurgerMenu.addEventListener('click', function() {
		showBurgerMenu(burgerMenuHeader, iconBurgerMenu, burgerMenu);
	});
}

if (userElement) {
	
	userElement.addEventListener('click', function(e) {
		showHidePersonalMenu(elemInsideMenuBox, e);
	});
	
	/* personDataBut.addEventListener('click', function() {
		ajaxPersonalData();
	}); */
	
	/* passwordAndSafetyBut.addEventListener('click', function() {
		ajaxpasswordAndSafety();
	}); */
	
	/* settingsBut.addEventListener('click', function() {
		ajaxSettings();
	}); */
}

if (searchSubmitutton) {
	document.body.addEventListener('keydown', function(e) {
		if (e.keyCode == 13) {
			if (searchField == document.activeElement) {
				if (searchField.value !== '') {
					var url = encodeURI('/search?q=' + searchField.value);
					location.href = url;
				}
				searchField.value = '';
			}
		}
	});
	
	searchSubmitutton.addEventListener('click', function() {
		if (searchField.value !== '') {
			var url = encodeURI('/search?q=' + searchField.value);
			location.href = url;
		}
		searchField.value = '';
	});
}

function getScrollWidth() {
	let div = document.createElement('div');

	div.style.overflowY = 'scroll';
	div.style.width = '50px';
	div.style.height = '50px';
	div.style.position = 'fixed';
	div.style.top = - 1000 + 'px';

	// мы должны вставить элемент в документ, иначе размеры будут равны 0
	document.body.append(div);
	let scrollWidth = div.offsetWidth - div.clientWidth;
	
	div.remove();

	return scrollWidth;
}

let tab = new BroadcastLS();
