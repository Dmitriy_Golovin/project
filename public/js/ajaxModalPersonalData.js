function ajaxPersonalData() {

	var xhr = new XMLHttpRequest(),
		modaLBlock = document.querySelector('.modal_content_box'),
		data = 'personalData=' + true + '&' + 'ajax=' + true;

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var result = JSON.parse(xhr.responseText);

			if (result['res'] == 1) {
				modaLBlock.innerHTML = result['content'];
				
				animationPushIcon({icon: '.edit_icon'});
				
				var firstNameP = document.querySelector('p.show_hide_firstname'),
					lastNameP = document.querySelector('p.show_hide_lastname'),
					countryP = document.querySelector('p.show_hide_country'),
					cityP = document.querySelector('p.show_hide_city'),
					countryListOptionEl = document.querySelectorAll('select[name=reg_country]>option'),
					countrySelectEl = document.querySelector('select[name=reg_country]'),
					inputElementList = document.querySelectorAll('.input_edit'),
					fullNameBox = document.querySelector('.profile_fullname_p'),
					avatarBox = document.querySelector('.profile_avatar'),
					modalWin = document.querySelector('.modal_profile'),
					profileBox = document.querySelector('.profile_box');
				
				firstNameP.innerHTML = result['firstName'];
				lastNameP.innerHTML = result['lastName'];
				countryP.innerHTML = result['country'];
				cityP.innerHTML = result['city'];
				
				for (var i = 0; i <countryListOptionEl.length; i++) {
					if (countryListOptionEl[i].innerHTML === result['country']) {
						ajaxGetCitiesForThisCountry(countryListOptionEl[i].getAttribute('data-countryId'));
						break;
					}
				}
				
				countrySelectEl.addEventListener('change', function(e) {
					cityP.innerHTML = 'Выберите город';
					var res = countrySelectEl.value;
					for (var i = 0; i <countryListOptionEl.length; i++) {
						if (countryListOptionEl[i].innerHTML === res) {
							ajaxGetCitiesForThisCountry(countryListOptionEl[i].getAttribute('data-countryId'));
							break;
						}
					}
				});
				
				noScrollOnModalWin(modalWin, profileBox);
				
				[].forEach.call(inputElementList, function(node) {
					node.addEventListener('keydown', function(e) {
						if (e.keyCode === 13) {
							confirmInputKeystroke(node);
						}
					});
				});

				setTimeout(function() {
					profileBox.classList.add('modal_active');
					modalWin.classList.add('modal_box_active');
				}, 10);
					
			} else if (result['res'] == 0){
				modaLBlock.innerHTML = 'error';
			}
		}
	};

	xhr.open('POST', '/user_panel/getBlockPersonalData');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(data);

}

function ajaxGetCitiesForThisCountry(countryId) {
	
	var xhr = new XMLHttpRequest(),
		modaLBlock = document.querySelector('.modal_content_box'),
		citySelectEl = document.querySelector('select[name=reg_city]');
		data = 'countryId=' + countryId + '&' + 'ajax=' + true;

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var result = JSON.parse(xhr.responseText);
			if (result['res'] == 1) {
				citySelectEl.innerHTML = result['qqq'];					
			} else if (result['res'] == 0){
				console.log(0);	
			}
		}
	};

	xhr.open('POST', '/user_panel/getCitiesForThisCountry');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(data);
	
}