/*jshint esversion: 6 */

let dataContainer = document.querySelector('.event_data_container'),
	id = dataContainer.getAttribute('data-entity_id'),
	typeLabels = {
		1: 'event',
		2: 'meeting',
		3: 'task',
	},
	type = typeLabels[dataContainer.getAttribute('data-type')];
	inviteStatus = dataContainer.dataset.entity_invite_status;

window.addEventListener('load', function() {
	if (Number(inviteStatus) === 0) {
		fetchSetViewedInviteStatus(id, type, function() {
			fetchGetCommonEventDataInvitationDetails();
		});
	} else {
		fetchGetCommonEventDataInvitationDetails();
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('entity_accept_invite_button') ||
		e.target.closest('.entity_accept_invite_button')) {

		fetchAcceptInviteCommonEvent(type, id, function(data) {
			let entityButtonContainer = document.querySelector('.entity_button_container'),
				inviteStatusValue = document.querySelector('.invite_status_value');

			entityButtonContainer.innerHTML = '';
			entityButtonContainer.insertAdjacentHTML('afterBegin', data.detailsDeclineBut);
			inviteStatusValue.classList.remove('invite_status_viewed');
			inviteStatusValue.classList.remove('invite_status_decline');
			inviteStatusValue.classList.add(data.statusCssClass);
			inviteStatusValue.innerHTML = data.statusValue;

			tab.sendToLS({
				'ws_tab_sync_to_do': 'setIncominInviteAsAccept',
				'detailsDeclineBut': data.detailsDeclineBut,
				'listDeclineBut': data.listDeclineBut,
				'type': data.type,
				'id': data.id,
				'statusValue': data.statusValue,
				'statusCssClass': data.statusCssClass,
				'userId': data.userId,
				'statusNotViewedBeforeAction': data.statusNotViewedBeforeAction,
			});
		});
	}

	if (e.target.classList.contains('entity_decline_invite_button') ||
		e.target.closest('.entity_decline_invite_button')) {

		fetchDeclinetInviteCommonEvent(type, id, function(data) {
			let entityButtonContainer = document.querySelector('.entity_button_container'),
				inviteStatusValue = document.querySelector('.invite_status_value');

			entityButtonContainer.innerHTML = '';
			entityButtonContainer.insertAdjacentHTML('afterBegin', data.detailsAcceptBut);
			inviteStatusValue.classList.remove('invite_status_viewed');
			inviteStatusValue.classList.remove('invite_status_accept');
			inviteStatusValue.classList.add(data.statusCssClass);
			inviteStatusValue.innerHTML = data.statusValue;

			tab.sendToLS({
				'ws_tab_sync_to_do': 'setIncominInviteAsDecline',
				'detailsAcceptBut': data.detailsAcceptBut,
				'listAcceptBut': data.listAcceptBut,
				'type': data.type,
				'id': data.id,
				'statusValue': data.statusValue,
				'statusCssClass': data.statusCssClass,
				'userId': data.userId,
				'statusNotViewedBeforeAction': data.statusNotViewedBeforeAction,
			});
		});
	}
});