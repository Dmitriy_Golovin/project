var colorRectList = [
		{r: 220, g: 220, b: 220},
		{r: 211, g: 211, b: 211},
		{r: 192, g: 192, b: 192},
		{r: 169, g: 169, b: 169},
		{r: 128, g: 128, b: 128},
		{r: 105, g: 105, b: 105},
		{r: 119, g: 136, b: 153},
		{r: 112, g: 128, b: 144},
		{r: 47, g: 79, b: 79},
		{r: 0, g: 191, b: 255},
		{r: 0, g: 250, b: 154},
		{r: 128, g: 0, b: 0},
		/* {r: 246, g: 244, b: 255},
		{r: 255, g: 253, b: 248},
		{r: 255, g: 246, b: 237},
		{r: 255, g: 239, b: 225},
		{r: 255, g: 234, b: 216},
		{r: 255, g: 228, b: 206},
		{r: 255, g: 222, b: 195},
		{r: 255, g: 218, b: 187},
		{r: 255, g: 213, b: 179},
		{r: 255, g: 208, b: 171},
		{r: 255, g: 206, b: 166},
		{r: 255, g: 201, b: 157},
		{r: 255, g: 195, b: 146},
		{r: 255, g: 193, b: 141},
		{r: 255, g: 190, b: 135},
		{r: 255, g: 187, b: 129} */
	];
	
function drawNoiseOnScreen(cnv) {
	var ctx = cnv.getContext('2d'),
		cnvWidth = cnv.width,
		cnvHeight = cnv.height,
		coordinateRectList = getCoordinateRectList(cnvWidth, cnvHeight);

	for (var i = 0; i < coordinateRectList.length; i++) {
		var rand = Math.floor(Math.random() * colorRectList.length),
			randColor = colorRectList[rand],
			colorValue = '#' + randColor['r'].toString(16) + randColor['g'].toString(16) + randColor['b'].toString(16);
		ctx.beginPath();
		ctx.fillStyle = colorValue;
		ctx.fillRect(coordinateRectList[i]['x'], coordinateRectList[i]['y'], 2, 2);
		ctx.stroke();
	}
}

function getCoordinateRectList(cnvWidth, cnvHeight) {
	var coords = [];
	for(var i = 0; i < cnvWidth; i++) {
		if (i % 2 == 0) {
			var x = i;
			for (var j = 0; j <cnvHeight; j++) {
				if (j % 2 == 0) {
					var y = j;
					coords.push({'x': x, 'y': y});
				}
			}
		}
	}
	return coords;
}

function drawErrorText(cnv, cnv404) {
	var ctx = cnv404.getContext('2d'),
		cnvWidth = cnv404.width,
		cnvHeight = cnv404.height;
	ctx.font = 'bold 24px sans-serif';
	ctx.textAlign = 'center';
	ctx.textBaseline = 'middle';
	ctx.fillStyle = '#000';
	ctx.fillText('сигнал потерян', cnvWidth / 2, cnvHeight / 2);
	
	setTimeout(function() {
		ctx.clearRect(0, 0, cnvWidth, cnvHeight);
		ctx.font = 'bold 24px sans-serif';
		ctx.textAlign = 'center';
		ctx.textBaseline = 'middle';
		ctx.fillStyle = '#000';
		ctx.fillText('поиск...', cnvWidth / 2, cnvHeight / 2);
	}, 2000);
	
	setTimeout(function() {
		ctx.clearRect(0, 0, cnvWidth, cnvHeight);
		ctx.font = 'bold 24px sans-serif';
		ctx.textAlign = 'center';
		ctx.textBaseline = 'middle';
		ctx.fillStyle = '#000';
		ctx.fillText('страницы не существует', cnvWidth / 2, cnvHeight / 2);
	}, 4000);
	
	setTimeout(function() {
		ctx.clearRect(0, 0, cnvWidth, cnvHeight);
		ctx.font = 'bold 90px sans-serif';
		ctx.textAlign = 'center';
		ctx.textBaseline = 'middle';
		ctx.fillStyle = '#000';
		ctx.fillText('404', cnvWidth / 2, cnvHeight / 2);
		//cnv.classList.add('tv_screen_background');
	}, 6000);
}