/*jshint esversion: 6 */

function ajaxProfileAddAboutSelf(text, blockForText, addButtonAboutSelf) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'about_self=' + editText(blockForText, text) + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				if (result.res == 1) {
					var dataForTabSync = {
						'ws_tab_sync_to_do': 'change_about_self',
						'about_self': result.data.about_self,
						'fromUserID': result.data.fromUserId,
					};

					tab.send(dataForTabSync);
					blockForText.innerHTML = result.data.about_self;
					addButtonAboutSelf.innerHTML = result.data.button;
				} else if (result.res == 0){
					console.log(result.errors);
				}
			}
		};

		xhr.open('POST', '/user/changeAboutSelf');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function editText(parentEl, text) {
	var textEdit = text.split('\n'),
		result = '';

	for (var i = 0; i < textEdit.length; i++) {
		if (textEdit[i] !== '') {
			result += textEdit[i];
			if (i < textEdit.length - 1) {
				result += '_razdelitelNegnyi_';
			}
		}
	}
	
	return result;
}