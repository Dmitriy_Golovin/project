/*jshint esversion: 6 */

let mainBox = document.querySelector('main_box'),
	eventMenuContainer = document.querySelector('.event_menu_container'),
	eventContentContainer = document.querySelector('.event_content_container'),
	eventContentList = document.querySelector('.event_content_list'),
	eventMenuCloseContainer = document.querySelector('.event_menu_close_container'),
	eventMenuCloseIcon = document.querySelector('.event_menu_close_icon'),
	eventMenuIcon = document.querySelector('.event_menu_icon'),
	calendarContainer = document.querySelector('.calendar_container'),
	calendarBox = document.querySelector('.calendar_box'),
	calendarArrow = document.querySelector('.calendar_box_arrow'),
	calendarDateTitle = document.querySelector('.calendar_date_title'),
	calendarPrevArrow = calendarBox.querySelector('.calendar_left_arrow'),
	calendarNextArrow = calendarBox.querySelector('.calendar_right_arrow'),
	calendarTable = calendarBox.querySelector('table'),
	nameField = document.querySelector('#filter_name_field'),
	privateTypeField = document.querySelector('#filter_privacy_type_field'),
	inviteStatusField = document.querySelector('#filter_invite_status_field'),
	onlyCurrentAndScheduledField = document.querySelector('#only_current_and_scheduled'),
	reviewStatusField = document.querySelector('#filter_review_status_field'),
	taskStatusField = document.querySelector('#filter_task_status_field'),
	dateFromField = document.querySelector('.filter_date_item_value_from'),
	dateToField = document.querySelector('.filter_date_item_value_to'),
	type = eventContentContainer.getAttribute('data-type'),
	subType = eventContentContainer.getAttribute('data-subtype'),
	filterApplyButton = document.querySelector('.filter_apply_button'),
	scrollEventList = true,
	calendarClass = new CalendarClass({
		'titleDateEl': calendarDateTitle,
		'tableEl': calendarTable,
		'prevArrow': calendarPrevArrow,
		'nextArrow': calendarNextArrow,
		'type': CalendarClass.typeAny,
	}),
	typeLabels = {
		'event': 2,
		'meeting': 3,
		'task': 4,
	};

calendarClass.init();

window.addEventListener('load', function() {
	fetchGetMainEventData(typeLabels[type], function() {
		if (type !== 'task') {
			fetchGetEventList(getDataEventObj());
		} else {
			fetchGetTaskList(getDataTaskObj());
		}
	});
});

window.addEventListener('resize', function() {
	closeCalendarBox();
	if (document.body.offsetWidth > 820 && eventMenuContainer.classList.contains('menu_container_active')) {
		eventMenuContainer.classList.remove('menu_container_active');

		if (!eventMenuCloseContainer.classList.contains('not_active')) {
			eventMenuCloseContainer.classList.add('not_active');
		}

		if (eventMenuIcon.classList.contains('event_menu_container_not_active')) {
			eventMenuIcon.classList.remove('event_menu_container_not_active');
		}

		if (document.body.classList.contains('scroll_none')) {
			document.body.classList.remove('scroll_none');
		}
	}
});

eventMenuIcon.addEventListener('click', function() {
	eventMenuCloseContainer.classList.remove('not_active');
	eventMenuContainer.classList.add('menu_container_active');
	eventMenuIcon.classList.add('event_menu_container_not_active');
});

eventMenuCloseIcon.addEventListener('click', function() {
	eventMenuContainer.classList.remove('menu_container_active');

	setTimeout(function() {
		eventMenuCloseContainer.classList.add('not_active');
		eventMenuIcon.classList.remove('event_menu_container_not_active');
	}, 220);
});

filterApplyButton.addEventListener('click', function() {
	if (type !== 'task') {
		fetchGetEventList(getDataEventObj());
	} else {
		fetchGetTaskList(getDataTaskObj());
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('filter_date_icon')) {
		let container = e.target.parentElement;

		container.append(calendarBox);
		calendarBox.classList.add('calendar_box_position_left');

		setTimeout(function() {
			calendarBox.classList.add('calendar_box_active');
		}, 10);
	}

	if ((!e.target.classList.contains('filter_date_icon') && !e.target.closest('.calendar_box')) ||
		e.target.classList.contains('calendar_container_close_icon') ||
		e.target.classList.contains('calendar_container_close')) {
		closeCalendarBox();
	}

	if (e.target.tagName === 'TD' && e.target.closest('.calendar_box>table')) {
		let container = e.target.closest('.filter_date_item'),
			elValue = container.firstElementChild,
			clearEl = container.querySelector('.filter_date_clear_icon');

		elValue.innerHTML = e.target.getAttribute('data-filter_date');
		clearEl.classList.remove('not_active');
		closeCalendarBox();
	}

	if (e.target.classList.contains('filter_date_clear_icon')) {
		let container = e.target.closest('.filter_date_item'),
			elValue = container.firstElementChild,
			clearEl = container.querySelector('.filter_date_clear_icon');

		elValue.innerHTML = '';
		clearEl.classList.add('not_active');
	}

	if (e.target.classList.contains('show_event_menu_container') || e.target.closest('.show_event_menu_container')) {
		let menu = e.target.closest('.event_item_menu'),
			upMenuItem = menu.querySelector('.up_menu_box_item'),
			middleMenuItem = menu.querySelector('.middle_menu_box_item'),
			bottomMenuItem = menu.querySelector('.bottom_menu_box_item');

		if (!menu.classList.contains('show_event_menu_container_open')) {
			closeEventItemMenu();
			menu.classList.add('show_event_menu_container_open');
			upMenuItem.classList.add('up_menu_box_item_open');
			middleMenuItem.classList.add('middle_menu_box_item_open');
			bottomMenuItem.classList.add('bottom_menu_box_item_open');
		} else {
			closeEventItemMenu();
		}
	}

	if (!e.target.classList.contains('show_event_menu_container') && !e.target.closest('.event_item_menu')) {
		closeEventItemMenu();
	}

	if (e.target.classList.contains('delete_event_item_icon_container') ||
		e.target.closest('.delete_event_item_icon_container')) {
		let itemContainer = e.target.closest('.event_result_item'),
			noticeModal = document.querySelector('.modal_notice'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			acceptButton = noticeModal.querySelector('.notice_button_accept'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		noticeModal.classList.add('modal_active');
		noticeModalBox.classList.add('modal_box_active');

		acceptButton.addEventListener('click', function() {
			let id = itemContainer.getAttribute('data-' + type + '_id');
			fetchDeleteCommonEvent(type, id, function(data) {
				let container = document.querySelector('.event_result_item[data-' +
					data.type + '_id="' + data.id + '"]');

				if (container) {
					let parent = container.parentElement,
						spanCount = document.querySelector('.' + data.subTypeMenu + '_' + data.type + '_span_count'),
						spanCountvalue = spanCount.firstChild.textContent;

					spanCount.firstChild.textContent = Number(spanCountvalue) - 1;
					parent.dataset.itemcount = Number(parent.dataset.itemcount) - 1;
					container.remove();
				}

				noticeModalBox.classList.remove('modal_box_active');
			
				setTimeout(function() {
					noticeModal.classList.remove('modal_active');
				}, 300);
			});
		});

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.classList.remove('modal_active');
			}, 300);
		});
	}

	if (e.target.classList.contains('entity_accept_invite_button') ||
		e.target.closest('.entity_accept_invite_button')) {
		let itemContainer = e.target.closest('.event_result_item'),
			id = itemContainer.getAttribute('data-' + type + '_id');

		fetchAcceptInviteCommonEvent(type, id, function(data) {
			let itemMenuContainer = itemContainer.querySelector('.event_item_menu ul'),
				inviteStatusValue = itemContainer.querySelector('.invite_status_value');

			itemMenuContainer.innerHTML = '';
			itemMenuContainer.insertAdjacentHTML('afterBegin', data.listDeclineBut);
			inviteStatusValue.classList.remove('invite_status_viewed');
			inviteStatusValue.classList.remove('invite_status_decline');
			inviteStatusValue.classList.add(data.statusCssClass);
			inviteStatusValue.innerHTML = data.statusValue;
			itemContainer.classList.remove('not_viewed_invite');

			if (data.statusNotViewedBeforeAction) {
				tab.tabSync._update_new_invite_count_minus_one({
					'userId': data.userId,
					'type': data.type,
				});
			}

			tab.sendToLS({
				'ws_tab_sync_to_do': 'setIncominInviteAsAccept',
				'detailsDeclineBut': data.detailsDeclineBut,
				'listDeclineBut': data.listDeclineBut,
				'type': data.type,
				'id': data.id,
				'statusValue': data.statusValue,
				'statusCssClass': data.statusCssClass,
				'userId': data.userId,
				'statusNotViewedBeforeAction': data.statusNotViewedBeforeAction,
			});
		});
	}

	if (e.target.classList.contains('entity_decline_invite_button') ||
		e.target.closest('.entity_decline_invite_button')) {
		let itemContainer = e.target.closest('.event_result_item'),
			id = itemContainer.getAttribute('data-' + type + '_id');

		fetchDeclinetInviteCommonEvent(type, id, function(data) {
			let itemMenuContainer = itemContainer.querySelector('.event_item_menu ul'),
				inviteStatusValue = itemContainer.querySelector('.invite_status_value');

			itemMenuContainer.innerHTML = '';
			itemMenuContainer.insertAdjacentHTML('afterBegin', data.listAcceptBut);
			inviteStatusValue.classList.remove('invite_status_viewed');
			inviteStatusValue.classList.remove('invite_status_accept');
			inviteStatusValue.classList.add(data.statusCssClass);
			inviteStatusValue.innerHTML = data.statusValue;
			itemContainer.classList.remove('not_viewed_invite');

			if (data.statusNotViewedBeforeAction) {
				tab.tabSync._update_new_invite_count_minus_one({
					'userId': data.userId,
					'type': data.type,
				});
			}

			tab.sendToLS({
				'ws_tab_sync_to_do': 'setIncominInviteAsDecline',
				'detailsAcceptBut': data.detailsAcceptBut,
				'listAcceptBut': data.listAcceptBut,
				'type': data.type,
				'id': data.id,
				'statusValue': data.statusValue,
				'statusCssClass': data.statusCssClass,
				'userId': data.userId,
				'statusNotViewedBeforeAction': data.statusNotViewedBeforeAction,
			});
		});
	}
});

function closeCalendarBox() {
	if (calendarBox.classList.contains('calendar_box_position_left') || calendarBox.classList.contains('calendar_box_active')) {
		calendarBox.classList.remove('calendar_box_active');

		setTimeout(function() {
			calendarBox.classList.remove('calendar_box_position_left');
			calendarContainer.append(calendarBox);
			calendarClass.count = 0;
			calendarClass.init();
		}, 200);
	}
}

function closeEventItemMenu() {
	let openMenu = document.querySelector('.show_event_menu_container_open');

	if (openMenu) {
		let upMenuItem = openMenu.querySelector('.up_menu_box_item'),
			middleMenuItem = openMenu.querySelector('.middle_menu_box_item'),
			bottomMenuItem = openMenu.querySelector('.bottom_menu_box_item');

		openMenu.classList.remove('show_event_menu_container_open');
		upMenuItem.classList.remove('up_menu_box_item_open');
		middleMenuItem.classList.remove('middle_menu_box_item_open');
		bottomMenuItem.classList.remove('bottom_menu_box_item_open');
	}
}