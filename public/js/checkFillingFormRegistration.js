function showErrorField(field, report, str) {
	
	if (!str.test(field.value)) {
		field.classList.add('error_input');
		field.classList.remove('valid_input');
		document.querySelector('.' + field.getAttribute('name')).innerHTML = report;
		document.querySelector('.' + field.getAttribute('name')).classList.add('notice_elem_active');
		
		if (field.getAttribute('name') == 'reg_password') {
			if (document.documentElement.clientWidth < 430) {
				document.querySelector('.' + field.getAttribute('name')).classList.add('notice_elem_large_active2');
			} else {
				document.querySelector('.' + field.getAttribute('name')).classList.add('notice_elem_large_active1');
			}
		}
		return false

	} else if (str.test(field.value)) {
		field.classList.add('valid_input');
		field.classList.remove('error_input');
		document.querySelector('.' + field.getAttribute('name')).innerHTML = '';
		document.querySelector('.' + field.getAttribute('name')).classList.remove('notice_elem_active');
		
		if (field.getAttribute('name') == 'reg_password') {
			document.querySelector('.' + field.getAttribute('name')).classList.remove('notice_elem_large_active2');
			document.querySelector('.' + field.getAttribute('name')).classList.remove('notice_elem_large_active1');
		}
		
		return true
	}
	
};

function passwordMatchCheck(passField1, passField2) {
	if (passField1.value !== passField2.value) {
		passField2.classList.add('error_input');
		passField2.classList.remove('valid_input');
		document.querySelector('.' + passField2.getAttribute('name')).innerHTML = 'Пароли не совпадают';
		document.querySelector('.' + passField2.getAttribute('name')).classList.add('notice_elem_active');
		return false
	} else if (passField1.value === passField2.value && passField2.value) {
		passField2.classList.remove('error_input');
		passField2.classList.add('valid_input');
		document.querySelector('.' + passField2.getAttribute('name')).innerHTML = '';
		document.querySelector('.' + passField2.getAttribute('name')).classList.remove('notice_elem_active');
		return true
	}
}

function removeError(field) {
	field.classList.remove('error_input');
	document.querySelector('.' + field.getAttribute('name')).innerHTML = '';
	document.querySelector('.' + field.getAttribute('name')).classList.remove('notice_elem_active');
	
	if (field.getAttribute('name') == 'reg_password') {
		document.querySelector('.' + field.getAttribute('name')).classList.remove('notice_elem_large_active2');
		document.querySelector('.' + field.getAttribute('name')).classList.remove('notice_elem_large_active1');
	}
}
