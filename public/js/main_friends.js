/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	type = mainBox.dataset.type,
	pageId = mainBox.dataset.page_id,
	friendContentBlock = mainBox.querySelector('.friend_content_block'),
	emojiContainer = document.querySelector('.emoji_block'),
	writeMessageField = document.querySelector('.write_message_block>textarea'),
	sendMessageButton = document.querySelector('.send_message_to_user_button'),
	searchFriendsField = document.querySelector('.search_friends_field'),
	scrollFriendList = true,
	cursorPositionInMessageField = 0;

window.addEventListener('load', function() {
	let friendData = {
		'type' : type,
		'pageId': pageId,
	};

	fetchGetFriendList(friendData);
	addEmojiToMessageFieldModal(emojiContainer, writeMessageField, sendMessageButton);
});

window.addEventListener('resize', function() {
	if (document.body.offsetWidth > 650 && document.querySelector('.profile_menu_container') &&
		document.querySelector('.profile_menu_container').classList.contains('menu_container_active')) {
		document.querySelector('.profile_menu_container').classList.remove('menu_container_active');

		if (!document.querySelector('.close_menu_container').classList.contains('not_active')) {
			document.querySelector('.close_menu_container').classList.add('not_active');
		}
	}
});

searchFriendsField.addEventListener('input', function() {
	let friendData = {
		'type' : type,
		'pageId': pageId,
		'searchString': this.value,
	};

	fetchGetFriendList(friendData);
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('menu_icon_container') || e.target.closest('.menu_icon_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.add('menu_container_active');
		closeMenuEl.classList.remove('not_active');
	}

	if (e.target.classList.contains('close_menu_container') || e.target.closest('.close_menu_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.remove('menu_container_active');

		setTimeout(function() {
			closeMenuEl.classList.add('not_active');
		}, 200);
	}
	
	if (!e.target.closest('.emoji_block') && e.target !== writeMessageField) {
		cursorPositionInMessageField = 0;
	}

	if (e.target.closest('.emoji_block') !== emojiContainer) {
		emojiContainer.querySelector('.emoji_box').classList.remove('menu_box_active');
		emojiContainer.querySelector('.emoji_box').parentElement.classList.remove('menu_box_active');
		writeMessageField.classList.remove('message_field_modal_emoji_active');
	}

	if (e.target.classList.contains('send_message_button')) {
		var from = tab.userID,
			userContainer = e.target.closest('.friend_result_item'),
			to = userContainer.dataset.profile_id,
			userFullnameBlock = userContainer.querySelector('.user_name_block_friend_result_item>a'),
			toFullName = userFullnameBlock.innerHTML;
			
		activateWriteNewMessageBox(from, to, toFullName);
	}

	if (e.target.classList.contains('invite_button_friend_result_item')) {
		let profileId = e.target.closest('.friend_result_item').dataset.profile_id;
		activateInviteModal(profileId);
	}
});

document.body.addEventListener('mouseover', function(e) {
	if (e.target.classList.contains('menu_block_friend_result_item')) {
		e.target.querySelector('.burger_menu_friend_result_item').classList.add('menu_box_active');
	}
});

document.body.addEventListener('mouseout', function(e) {
	if (e.target.closest('.menu_block_friend_result_item') &&
		!e.relatedTarget.closest('.menu_block_friend_result_item')) {
		e.target.closest('.menu_block_friend_result_item').querySelector('.burger_menu_friend_result_item').classList.remove('menu_box_active');
	}
});