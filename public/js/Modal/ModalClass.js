class Modal {

	modal;
	modalBox;
	modalContainer;

	show() {
		this.modal.classList.add('modal_active');
		this.modalBox.classList.add('modal_box_active');
	}

	close() {
		this.modalBox.classList.remove('modal_box_active');
	}
}