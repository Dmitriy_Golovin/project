/*jshint esversion: 6 */

class ModalSliderImageWithCommentClass extends Modal
{
	constructor(data) {
		super();
		this.entity_id = data.entity_id;
		this.fetchFunction = data.fetchFunction;
		this.entityAction = data.entityAction;
		this.exceptionIdList = data.exceptionIdList;
		this.modal = document.querySelector('.' + data.modalCssClass);
		this.modalBox = this.modal.querySelector('.modal_image_slider_box');
		this.closeEl = this.modal.querySelector('.close_container_pic');
		this.sliderContainer = this.modal.querySelector('.slider_image_comment_container') ||
			this.modal.querySelector('.slider_image_comment_container_mobile');
		this.currentSliderImageContainer = this.modalBox.querySelector('div[data-current_image="1"]');
		this.prevButton = this.modal.querySelector('.slider_image_left_arrow_container>img');
		this.nextButton = this.modal.querySelector('.slider_image_right_arrow_container>img');
		this.scrollWidth = getScrollWidth();
		this.currentCommentImageContainer = this.currentSliderImageContainer.querySelector('.comment_field_container');
		this.currentIndex = 0;
		this.totalImageCount = Number(this.sliderContainer.dataset.image_count);
		this.touchStartPointX = 0;
		this.swipeDifference = 0;
		this.swipeTargetWidth = 0;
		this.action = true;
		this.getNewImage = true;
		this.fixedSwipeValue = false;
	}

	DIR_PREV()
	{
		return 'prev';
	}

	DIR_NEXT()
	{
		return 'next';
	}

	show()
	{
		setTimeout(function() {
			this.modal.classList.add('modal_active');
			this.modalBox.classList.add('modal_box_active');
		}.bind(this), 10);

		this.initCommentFieldContainer();
		this.getCurrentIndex();
		this.prev();
		this.next();

		if ('ontouchstart' in this.sliderContainer) {
			this.sliderContainer.addEventListener('touchstart', this.getTouchStartPointX.bind(this), false);
		}

		if ('ontouchmove' in this.sliderContainer) {
			this.sliderContainer.addEventListener('touchmove', this.getSwipeDistance.bind(this), false);
		}

		if ('ontouchend' in this.sliderContainer) {
			this.sliderContainer.addEventListener('touchend', this.fixedSwipe.bind(this), false);
		}

		document.body.classList.add('not_scroll');
		document.body.style.paddingRight = this.scrollWidth + 'px';
		document.querySelector('.autorized_or_personal_block').style.paddingRight = this.scrollWidth + 'px';
	}

	initCommentFieldContainer()
	{
		if (this.currentCommentImageContainer) {
			setTimeout(function() {
				showCommentWriteField(this.currentCommentImageContainer);
				setPaddingBottomForListContainer(this.currentCommentImageContainer);
			}.bind(this), 50);

			this.modalBox.onscroll = function() {
				showCommentWriteField(this.currentCommentImageContainer);
			}.bind(this);
		}
	}

	getCurrentIndex()
	{
		for (let i = 0; i < this.sliderContainer.children.length; i++) {
			if (Number(this.sliderContainer.children[i].dataset.current_image) === 1) {
				this.currentIndex = i;
				break;
			}
		}
	}

	addNewImageInSlider(html, dir)
	{	
		this.sliderContainer.ontransitionend = function() {
			this.currentSliderImageContainer.classList.add('fixed_image_slider');

			if (dir === this.DIR_PREV()) {
				this.sliderContainer.insertAdjacentHTML('afterBegin', html);
			}

			if (dir === this.DIR_NEXT()) {
				this.sliderContainer.insertAdjacentHTML('beforeEnd', html);
			}

			this.sliderContainer.classList.add('no_transition');
			this.getCurrentIndex();
			this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + '%');
			this.currentSliderImageContainer.classList.remove('fixed_image_slider');

			setTimeout(function() {
				this.sliderContainer.classList.remove('no_transition');
			}.bind(this), 10);

			this.sliderContainer.ontransitionend = null;
			this.getNewImage = true;
		}.bind(this);
	}

	setTranslateForSliderContainer(dir) {
		this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + '%');
		this.activateCurrentCommentBlock();
		this.getNewImageForSlider(dir);
		
		setTimeout(function() {
			this.hideOrShowPrevButton();
			this.hideOrShowNextButton();
			this.action = true;
		}.bind(this), 350);
	}

	getNewImageForSlider(dir)
	{
		if (!this.getNewImage) {
			return;
		}

		if (dir === this.DIR_PREV()) {
			if (Number(this.currentIndex) > 2) {
				return;
			}

			let lastImageContainer = this.sliderContainer.children[0].querySelector('.main_file_image_container');

			if (Number(lastImageContainer.dataset.rownum) > 1) {
				let dataObj = {
					'entity_id': this.entity_id,
					'type': TYPE_PREVIOUS,
					'last_file_id': lastImageContainer.dataset.file_id,
					'exceptionIdList': this.exceptionIdList,
					'entityAction': this.entityAction,
				};

				// fetchGetImageSliderReportViewModal(dataObj);
				window[this.fetchFunction](dataObj);
				this.getNewImage = false;
			}
		}

		if (dir === this.DIR_NEXT()) {
			if ((this.sliderContainer.children.length - 2) - this.currentIndex > 1) {
				return;
			}

			let lastImageContainer = this.sliderContainer.children[this.sliderContainer.children.length - 1].querySelector('.main_file_image_container');

			if (Number(lastImageContainer.dataset.rownum) < this.totalImageCount) {
				let dataObj = {
					'entity_id': this.entity_id,
					'type': TYPE_NEXT,
					'last_file_id': lastImageContainer.dataset.file_id,
					'exceptionIdList': this.exceptionIdList,
					'entityAction': this.entityAction,
				};

				// fetchGetImageSliderReportViewModal(dataObj);
				window[this.fetchFunction](dataObj);
				this.getNewImage = false;
			}
		}
	}

	activateCurrentCommentBlock()
	{
		this.currentSliderImageContainer.dataset.current_image = 0;
		this.currentSliderImageContainer.querySelector('.modal_slider_image_comment_container').classList.add('not_active');
		this.currentSliderImageContainer = this.sliderContainer.children[this.currentIndex];
		this.currentSliderImageContainer.dataset.current_image = 1;
		this.currentCommentImageContainer = this.currentSliderImageContainer.querySelector('.comment_field_container');
		this.currentSliderImageContainer.querySelector('.modal_slider_image_comment_container').classList.remove('not_active');
		this.initCommentFieldContainer();
	}

	hideOrShowPrevButton() {
		if (!this.prevButton) {
			return;
		}

		if (this.currentIndex == 0) {
			this.prevButton.classList.add('not_active_element_arrow_pic');
		} else {
			this.prevButton.classList.remove('not_active_element_arrow_pic');
		}
	}

	hideOrShowNextButton() {
		if (!this.nextButton) {
			return;
		}

		if (this.currentIndex == this.sliderContainer.children.length - 1) {
			this.nextButton.classList.add('not_active_element_arrow_pic');
		} else {
			this.nextButton.classList.remove('not_active_element_arrow_pic');
		}
	}

	prev() {
		if (!this.prevButton) {
			return;
		}

		this.prevButton.onclick = function() {
			if (this.action) {
				this.currentIndex -= 1;
				this.action = false;
				this.setTranslateForSliderContainer(this.DIR_PREV());
			}
		}.bind(this);
	}

	next() {
		if (!this.nextButton) {
			return;
		}

		this.nextButton.onclick = function() {
			if (this.action) {
				this.currentIndex += 1;
				this.action = false;
				this.setTranslateForSliderContainer(this.DIR_NEXT());
			}
		}.bind(this);
	}

	close()
	{
		super.close();
		document.body.classList.remove('not_scroll');
		document.body.style.paddingRight = '';
		document.querySelector('.autorized_or_personal_block').style.paddingRight = '';

		setTimeout(function() {
			this.modal.classList.remove('modal_active');
			this.modal.remove();
		}.bind(this), 300);
	}

	getTouchStartPointX(e) {
		this.touchStartPointX = e.touches[0].clientX;
		this.swipeTargetWidth = e.touches[0].target.getBoundingClientRect().width;
	}

	getSwipeDistance(e) {
		let newCoordPointX = e.touches[0].clientX,
			offset = 0;

		this.fixedSwipeValue = true;
		this.swipeDifference = newCoordPointX - this.touchStartPointX;

		if (newCoordPointX > this.touchStartPointX) {
			offset = this.swipeDifference * 100 / this.sliderContainer.getBoundingClientRect().width;
		}

		if (newCoordPointX < this.touchStartPointX) {
			offset = this.swipeDifference * 100 / this.sliderContainer.getBoundingClientRect().width;
		}

		let offsetValue = this.currentSliderImageContainer.offsetWidth / 100 * offset;

		this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + offsetValue + '%');
		this.sliderContainer.style.transition = 0 + 's';
	}

	fixedSwipe(e) {
		if (!this.fixedSwipeValue) {
			return;
		}

		this.sliderContainer.style.transition = '';

		if (this.swipeDifference > 0) {
			if (this.currentIndex < 1) {
				this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + '%');
				return;
			}
			
			if (Math.abs(this.swipeDifference) > this.swipeTargetWidth / 3) {
				this.currentIndex -= 1;
				this.setTranslateForSliderContainer(this.DIR_PREV());
			} else {
				this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + '%');
			}
		}

		if (this.swipeDifference < 0) {
			if (this.currentIndex > this.sliderContainer.children.length - 2) {
				this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + '%');
				return;
			}

			if (Math.abs(this.swipeDifference) > this.swipeTargetWidth / 3) {
				this.currentIndex += 1;
				this.setTranslateForSliderContainer(this.DIR_NEXT());
			} else {
				this.sliderContainer.style.setProperty('--sliderTranslate', (100 * this.currentIndex * (-1)) + '%');
			}
		}

		// this.setTranslateForSliderContainer(this.translateValue);
		this.swipeTargetWidth = 0;
		this.fixedSwipeValue = false;
	}
}