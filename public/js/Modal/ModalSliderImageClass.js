/*jshint esversion: 6 */

class ModalSliderImage extends Modal {

	imageOnPageDataList = [];
	countImageInSlider;
	currentIndex = 0;
	touchStartPointX;
	translateValue = 0;
	swipeDifference = 0;
	swipeTargetWidth = 0;

	constructor(data) {
		super();
		this.modal = document.querySelector('.' + data.modalCssClass);
		this.availableImageListContainer = data.availableImageListContainer;
		this.modalBox = this.modal.querySelector('.modal_box');
		this.modalContainer = this.modal.querySelector('.modal_container');
		this.container = this.modal.querySelector('.slider_image_box_modal');
		this.closeEl = this.modal.querySelector('.close_slide_modal_container>img');
		this.sliderContainer = this.modal.querySelector('.slider_image_container') || this.modal.querySelector('.slider_image_container_mobile');
		this.prevButton = this.modal.querySelector('.slider_image_left_arrow_container>img');
		this.nextButton = this.modal.querySelector('.slider_image_right_arrow_container>img');
		this.init();
	}

	init() {
		this.closeEl.addEventListener('click', function() {
			this.close();
		}.bind(this));
	}
	
	show(targetEl) {
		this.getImageDataList(targetEl);
		this.createImageTape();
		this.prev();
		this.next();
		super.show();

		if ('ontouchstart' in this.sliderContainer) {
			this.getTouchStartPointX = this.getTouchStartPointX.bind(this);
			this.sliderContainer.addEventListener('touchstart', this.getTouchStartPointX, false);
		}

		if ('ontouchmove' in this.sliderContainer) {
			this.getSwipeDistance = this.getSwipeDistance.bind(this);
			this.sliderContainer.addEventListener('touchmove', this.getSwipeDistance, false);
		}

		if ('ontouchend' in this.sliderContainer) {
			this.fixedSwipe = this.fixedSwipe.bind(this);
			this.sliderContainer.addEventListener('touchend', this.fixedSwipe, false);
		}

		this.container.classList.remove('modal_block_not_active');

		this.container.ontransitionend = function() {
			if (!this.container.classList.contains('modal_block_not_active')) {
				this.container.classList.remove('not_active_opacity');
			}
		}.bind(this);
	}

	close() {
		if ('ontouchstart' in this.sliderContainer) {
			this.sliderContainer.removeEventListener('touchstart', this.getTouchStartPointX, false);
		}

		if ('ontouchmove' in this.sliderContainer) {
			this.sliderContainer.removeEventListener('touchmove', this.getSwipeDistance, false);
		}

		if ('ontouchend' in this.sliderContainer) {
			this.sliderContainer.removeEventListener('touchend', this.fixedSwipe, false);
		}

		super.close();

		setTimeout(function() {
			this.modal.classList.remove('modal_active');
			this.container.classList.add('not_active_opacity');
			this.container.classList.add('modal_block_not_active');
		}.bind(this), 300);

		this.imageOnPageDataList.length = 0;
		this.sliderContainer.innerHTML = '';
	}

	getImageDataList(targetEl) {
		let imageContainerList = this.availableImageListContainer.querySelectorAll('.' + ImageFileElement.mainCssClassForContainer + ':not(.image_file_container_preview)'),
			index = 0;
		
		for (let item of imageContainerList) {
			let itemData = {};

			itemData.fileId = item.getAttribute('data-file_id');
			itemData.url = item.querySelector('img').getAttribute('src');

			if (item.querySelector('img') === targetEl) {
				this.currentIndex = index;
			}

			this.imageOnPageDataList.push(itemData);
			index++;
		}

		this.hideOrShowPrevButton();
		this.hideOrShowNextButton();
	}

	insertImageFile(data, insertType) {
		new ImageFileElement(
			{
				'parentEl': this.sliderContainer,
				'type': 'slider',
			},
			{
				'fileId': data['fileId'],
				'url': data['url'],
				'insertType': insertType,
			}
		);
	}

	createImageTape() {
		for (let i = 0; i < this.imageOnPageDataList.length; i++) {
			this.insertImageFile(this.imageOnPageDataList[i], 'append');
		}

		this.getTranslateValue();
		this.setTranslateForSliderContainer(this.translateValue);
	}

	getTranslateValue() {
		if (this.currentIndex >= 1) {
			this.translateValue = (100 / this.imageOnPageDataList.length * (this.currentIndex)) * (-1);
		} else {
			this.translateValue = 0;
		}
	}

	setTranslateForSliderContainer(value) {
		this.sliderContainer.style.setProperty('--sliderTranslate', value + '%');
		this.hideOrShowPrevButton();
		this.hideOrShowNextButton();
	}

	hideOrShowPrevButton() {
		if (!this.prevButton) {
			return;
		}

		if (this.currentIndex == 0) {
			this.prevButton.classList.add('not_active_element_arrow_pic');
		} else {
			this.prevButton.classList.remove('not_active_element_arrow_pic')
		}
	}

	hideOrShowNextButton() {
		if (!this.nextButton) {
			return;
		}

		if (this.currentIndex == this.imageOnPageDataList.length - 1) {
			this.nextButton.classList.add('not_active_element_arrow_pic');
		} else {
			this.nextButton.classList.remove('not_active_element_arrow_pic')
		}
	}

	prev() {
		if (!this.prevButton) {
			return;
		}

		this.prevButton.onclick = function() {
			this.currentIndex -= 1;
			this.getTranslateValue();
			this.setTranslateForSliderContainer(this.translateValue);
		}.bind(this)
	}

	next() {
		if (!this.nextButton) {
			return;
		}

		this.nextButton.onclick = function() {
			this.currentIndex += 1;
			this.getTranslateValue();
			this.setTranslateForSliderContainer(this.translateValue);
		}.bind(this)
	}

	getTouchStartPointX(e) {
		this.touchStartPointX = e.touches[0].clientX;
		this.swipeTargetWidth = e.touches[0].target.getBoundingClientRect().width;
	}

	getSwipeDistance(e) {
		let newCoordPointX = e.touches[0].clientX,
			offset = 0;

		this.swipeDifference = newCoordPointX - this.touchStartPointX;

		if (newCoordPointX > this.touchStartPointX) {
			offset = this.swipeDifference * 100 / this.sliderContainer.getBoundingClientRect().width;
		}

		if (newCoordPointX < this.touchStartPointX) {
			offset = this.swipeDifference * 100 / this.sliderContainer.getBoundingClientRect().width;
		}

		let offsetValue = this.translateValue + offset;

		this.sliderContainer.style.transition = 0 + 's';
		this.setTranslateForSliderContainer(offsetValue);
	}

	fixedSwipe(e) {
		this.sliderContainer.style.transition = '';

		if (this.swipeDifference > 0) {
			if (this.currentIndex < 1) {
				this.setTranslateForSliderContainer(this.translateValue);
				return;
			}
			
			if (Math.abs(this.swipeDifference) > this.swipeTargetWidth / 3) {
				this.currentIndex -= 1;
				this.getTranslateValue();
			}
		}

		if (this.swipeDifference < 0) {
			if (this.currentIndex > this.imageOnPageDataList.length - 2) {
				this.setTranslateForSliderContainer(this.translateValue);
				return;
			}

			if (Math.abs(this.swipeDifference) > this.swipeTargetWidth / 3) {
				this.currentIndex += 1;
				this.getTranslateValue();
			}
		}

		this.setTranslateForSliderContainer(this.translateValue);
		this.swipeTargetWidth = 0;
	}
}