function ajaxSaveEvent(dataObj, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data;

		dataObj['table'] = 'events';
		dataJSON = JSON.stringify(dataObj);
		data = 'dataJSON=' + dataJSON + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();
				callback(result['res']);
			}
		};

		xhr.open('POST', '/calendar/saveEventBase');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	})
}

function ajaxSaveMetting(dataObj, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data;

		dataObj['table'] = 'meetings';
		dataJSON = JSON.stringify(dataObj);
		data = 'dataJSON=' + dataJSON + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();
				callback(result['res']);
			}
		};

		xhr.open('POST', '/calendar/saveEventBase');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function ajaxSaveTask(dataObj, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data;

		dataObj['table'] = 'tasks';
		dataJSON = JSON.stringify(dataObj);
		data = 'dataJSON=' + dataJSON + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();
				callback(result['res']);
			}
		};

		xhr.open('POST', '/calendar/saveEventBase');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}