function contentSetMinHeight() {
	var contentElement = document.querySelector('.content_wrapper'),
		header = document.querySelector('header'),
		footer = document.querySelector('footer');

	contentElement.style.minHeight = document.documentElement.clientHeight - 1 - (header.offsetHeight + footer.offsetHeight) + 'px';

}