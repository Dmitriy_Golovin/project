/*jshint esversion: 6 */

const FILE_TYPE_IMAGE = 1;
const FILE_TYPE_VIDEO = 3;
const FILE_TYPE_DOCUMENT = 4;

let mainBox = document.querySelector('.main_box'),
	type = mainBox.getAttribute('data-type'),
	createType = mainBox.getAttribute('data-create_type'),
	dateField = document.querySelector('.create_event_date_field'),
	timeField = document.querySelector('.create_event_time_field'),
	dateInput = document.querySelector('input[name="date"]'),
	timeInput = document.querySelector('input[name="time"]'),
	periodicityContainer = document.querySelector('.periodicity_container'),
	reminderContainer = document.querySelector('.reminder_container'),
	calendarContainer = document.querySelector('.calendar_container'),
	calendarBox = document.querySelector('.calendar_box'),
	calendarDateTitle = document.querySelector('.calendar_date_title'),
	calendarPrevArrow = calendarBox.querySelector('.calendar_left_arrow'),
	calendarNextArrow = calendarBox.querySelector('.calendar_right_arrow'),
	calendarTable = calendarBox.querySelector('table'),
	selectTimeContainer = document.querySelector('.select_time_container'),
	selectTimeBox = document.querySelector('.select_time_box'),
	modalAddGuestUserBox = document.querySelector('.modal_add_guest_user_box'),
	guestUserModal = modalAddGuestUserBox.closest('.modal'),
	guestUserModalBox = modalAddGuestUserBox.closest('.modal_box'),
	addGuestUserButtton = document.querySelector('.add_guest_event_but'),
	closeGuestUserButton = guestUserModal.querySelector('.close_win_modal'),
	invitedContentBlock = guestUserModal.querySelector('.invited_content_block'),
	addedUserContainer = document.querySelector('.entity_guest_content'),
	fileVideoField = document.querySelector('#' + type + '_load_file_video'),
	fileImageField = document.querySelector('#' + type + '_load_file_image'),
	fileDocumentField = document.querySelector('#' + type + '_load_file_document'),
	fileImageContainer = document.querySelector('.create_event_image_container'),
	guestUserIdListInput = document.querySelector('input[name="guestUserIdList"]') ||
		document.querySelector('input[name="assigned_user_id"]'),
	userListObjData,
	videoFileList = new DataTransfer(),
	imageFileList = new DataTransfer(),
	documentFileList = new DataTransfer(),
	timeOutVideoMediaControl,
   	fullScreen = false,
   	taskHistoryId,
   	modalSliderImage = new ModalSliderImage({
		'modalCssClass': 'modal_slider_image',
		'availableImageListContainer': fileImageContainer,
	}),
	calendarClassData = {
		'titleDateEl': calendarDateTitle,
		'tableEl': calendarTable,
		'prevArrow': calendarPrevArrow,
		'nextArrow': calendarNextArrow,
	},
	selectTimeClass = new SelectTimeClass({
		'box': selectTimeBox,
	});

if (type === 'task') {
	calendarClassData.type = CalendarClass.typeNowAndFuture();
	if (createType === 'edit') {
		taskHistoryId = mainBox.dataset.task_history_id;
	}
} else {
	calendarClassData.type = CalendarClass.typeAny();
}

let calendarClass = new CalendarClass(calendarClassData);
calendarClass.init();
selectTimeClass.init();

if (createType === 'create') {
	if (!dateInput.value) {
		let day = calendarClass.date.getDate(),
			month = Number(calendarClass.date.getMonth()) + 1,
			year = calendarClass.date.getFullYear(),
			hours = calendarClass.date.getHours(),
			minutes = calendarClass.date.getMinutes();

		if ((month + '').length < 2) {
			month = '0' + month;
		}

		if ((hours + '').length < 2) {
			hours = '0' + hours;
		}

		if ((minutes + '').length < 2) {
			minutes = '0' + minutes;
		}

		dateField.innerHTML = day + '-' + month + '-' + year;
		timeField.innerHTML = hours + ':' + minutes;
		dateInput.value = new Date(year, month - 1, day).getTime() / 1000;
		timeInput.value = (hours * 60 * 60) + (minutes * 60);
	} else {
		getDateTimeInputValueForEdit();
	}
}

if (createType === 'edit') {
	getDateTimeInputValueForEdit();
}

document.body.addEventListener('mouseover', function(e) {
	if (e.target.classList.contains('notice_icon')) {
		let noticeLabelMessage = e.target.parentElement.querySelector('.notice_label_message');

		noticeLabelMessage.classList.remove('not_active');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active_opacity');
			noticeLabelMessage.setAttribute('data-notice_label_state', 'active');
		}, 5);
	}
});

document.body.addEventListener('mouseout', function(e) {
	if (e.target.classList.contains('notice_icon')) {
		let noticeLabelMessage = e.target.parentElement.querySelector('.notice_label_message');

		noticeLabelMessage.classList.add('not_active_opacity');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active');
		}, 5);
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('calendar_icon') || e.target.classList.contains('create_event_date_field')) {
		let container = document.querySelector('.calendar_icon').parentElement;

		container.append(calendarBox);
		calendarBox.classList.add('calendar_box_position_top');

		setTimeout(function() {
			calendarBox.classList.add('calendar_box_active');
		}, 10);
	}

	if ((!e.target.classList.contains('calendar_icon') &&
		!e.target.classList.contains('create_event_date_field') &&
		!e.target.closest('.calendar_box')) ||
		e.target.classList.contains('calendar_container_close_icon') ||
		e.target.classList.contains('calendar_container_close')) {
		closeCalendarBox();
	}

	if (e.target.tagName === 'TD' && e.target.closest('.calendar_box>table')) {
		let container = e.target.closest('.filter_date_item'),
			dateValue = e.target.getAttribute('data-filter_date'),
			dateArr = dateValue.split('-');

		dateField.innerHTML = e.target.getAttribute('data-filter_date');
		dateInput.value = new Date(dateArr[2], dateArr[1] - 1, dateArr[0]).getTime() / 1000;

		if (isPastDateTimeFromValue()) {
			periodicityContainer.classList.add('not_active_element');
			reminderContainer.classList.add('not_active_element');
		} else {
			periodicityContainer.classList.remove('not_active_element');
			reminderContainer.classList.remove('not_active_element');
		}

		closeCalendarBox();
	}

	if (e.target.classList.contains('time_icon') || e.target.classList.contains('create_event_time_field')) {
		let container = document.querySelector('.time_icon').parentElement,
			pagePosition = window.scrollY;

		container.append(selectTimeBox);
		selectTimeBox.classList.add('select_time_box_position_top');

		setTimeout(function() {
			selectTimeBox.classList.add('select_time_box_active');
		}, 10);

		let scrollWidth = getScrollWidth();
		document.body.classList.add('not_scroll');

		if (window.innerHeight - 2 < document.body.offsetHeight) {
			document.body.style.paddingRight = scrollWidth + 'px';
			document.querySelector('.autorized_or_personal_block').style.paddingRight = scrollWidth + 'px';
		}
	}

	if ((!e.target.classList.contains('time_icon') &&
		!e.target.classList.contains('create_event_time_field') &&
		!e.target.closest('.select_time_box')) ||
		e.target.classList.contains('select_time_container_close') ||
		e.target.classList.contains('select_time_container_close_icon')) {
		closeSelectTimeBox();
	}

	if (e.target.tagName === 'LI' && e.target.closest('.select_time_hours_box')) {
		let timeFieldValue = timeField.innerHTML,
			valueArr = timeFieldValue.split(':'),
			hourValue = e.target.getAttribute('data-hours');

		valueArr[0] = hourValue;
		timeField.innerHTML = valueArr.join(':');
		timeInput.value = (Number(valueArr[0] * 60 * 60)) + (Number(valueArr[1]) * 60);

		if (isPastDateTimeFromValue()) {
			periodicityContainer.classList.add('not_active_element');
			reminderContainer.classList.add('not_active_element');
		} else {
			periodicityContainer.classList.remove('not_active_element');
			reminderContainer.classList.remove('not_active_element');
		}
	}

	if (e.target.tagName === 'LI' && e.target.closest('.select_time_minutes_box')) {
		let timeFieldValue = timeField.innerHTML,
			valueArr = timeFieldValue.split(':'),
			minuteValue = e.target.getAttribute('data-minutes');

		valueArr[1] = minuteValue;
		timeField.innerHTML = valueArr.join(':');
		timeInput.value = (Number(valueArr[0] * 60 * 60)) + (Number(valueArr[1]) * 60);

		if (isPastDateTimeFromValue()) {
			periodicityContainer.classList.add('not_active_element');
			reminderContainer.classList.add('not_active_element');
		} else {
			periodicityContainer.classList.remove('not_active_element');
			reminderContainer.classList.remove('not_active_element');
		}
	}

	if (e.target.classList.contains('invited_search_button')) {
		fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
	}

	if (e.target.classList.contains('event_meeting_invite_user_button')) {
		let userQuerySearch = e.target.closest('.user_query_search'),
			userCount = userQuerySearch.getAttribute('data-usercount'),
			timestamp = userQuerySearch.getAttribute('data-timestamp'),
			userContainer = e.target.closest('.user_result_item'),
			existUserCount = userQuerySearch.children.length,
			rowNum = userQuerySearch.children[userQuerySearch.children.length - 1].getAttribute('data-rowNum'),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title');

		for (let el of addedUserContainer.children) {
			if (el.hasAttribute('data-query_timestamp') && el.getAttribute('data-query_timestamp') == userQuerySearch.getAttribute('data-timestamp')) {
				existUserCount += 1;
			}
		}

		if ((userQuerySearch.parentElement.scrollHeight - userContainer.offsetHeight) <=
			userQuerySearch.parentElement.clientHeight &&
			existUserCount < userCount) {
			fetchGetUserList(userListObjData, invitedContentBlock, timestamp, rowNum);
		}

		userContainer.setAttribute('data-query_timestamp', timestamp);
		noEventsTitle.classList.add('not_active');
		addedUserContainer.append(userContainer);
		editValueGuestListInput(userContainer.getAttribute('data-profile_id'), 'add');
		userContainer.querySelector('.event_meeting_invite_user_button').remove();
		userContainer.querySelector('.event_meeting_cancel_user_button').classList.remove('not_active');
	}

	if (e.target.classList.contains('invited_assign_self')) {		
		let assigneSelfContainer = document.querySelector('.self_assign_executor_container'),
			userContainer = assigneSelfContainer.firstElementChild.cloneNode(true),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title'),
			existEl = addedUserContainer.querySelector('.user_result_item');

		if (existEl) {
			return;
		}

		noEventsTitle.classList.add('not_active');
		addGuestUserButtton.classList.add('not_active');
		addedUserContainer.append(userContainer);
		editValueGuestListInput(userContainer.getAttribute('data-profile_id'), 'add');

		guestUserModalBox.classList.remove('modal_box_active');

		setTimeout(function() {
			guestUserModal.classList.remove('modal_active');
		}, 300);
	}

	if (e.target.classList.contains('task_assign_user_button')) {
		let userQuerySearch = e.target.closest('.user_query_search'),
			userCount = userQuerySearch.getAttribute('data-usercount'),
			timestamp = userQuerySearch.getAttribute('data-timestamp'),
			userContainer = e.target.closest('.user_result_item'),
			existUserCount = userQuerySearch.children.length,
			rowNum = userQuerySearch.children[userQuerySearch.children.length - 1].getAttribute('data-rowNum'),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title'),
			existEl = addedUserContainer.querySelector('.user_result_item');

		if (existEl) {
			return;
		}

		for (let el of addedUserContainer.children) {
			if (el.hasAttribute('data-query_timestamp') && el.getAttribute('data-query_timestamp') == userQuerySearch.getAttribute('data-timestamp')) {
				existUserCount += 1;
			}
		}

		if ((userQuerySearch.parentElement.scrollHeight - userContainer.offsetHeight) <=
			userQuerySearch.parentElement.clientHeight &&
			existUserCount < userCount) {
			fetchGetUserList(userListObjData, invitedContentBlock, timestamp, rowNum);
		}

		userContainer.setAttribute('data-query_timestamp', timestamp);
		noEventsTitle.classList.add('not_active');
		addedUserContainer.append(userContainer);
		editValueGuestListInput(userContainer.getAttribute('data-profile_id'), 'add');
		userContainer.querySelector('.task_assign_user_button').remove();
		userContainer.querySelector('.task_cancel_user_button').classList.remove('not_active');
		addGuestUserButtton.classList.add('not_active');

		guestUserModalBox.classList.remove('modal_box_active');

		setTimeout(function() {
			guestUserModal.classList.remove('modal_active');
		}, 300);
	}

	if (e.target.classList.contains('event_meeting_cancel_user_button')) {
		let userContainer = e.target.closest('.user_result_item'),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title');

		editValueGuestListInput(userContainer.getAttribute('data-profile_id'), 'remove');
		userContainer.remove();

		let userElList = addedUserContainer.querySelectorAll('.user_result_item');

		if (userElList.length <= 0) {
			noEventsTitle.classList.remove('not_active');
		}
	}

	if (e.target.classList.contains('task_cancel_user_button')) {
		let userContainer = e.target.closest('.user_result_item'),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title');

		editValueGuestListInput(userContainer.getAttribute('data-profile_id'), 'remove');
		userContainer.remove();

		let userElList = addedUserContainer.querySelectorAll('.user_result_item');

		if (userElList.length <= 0) {
			noEventsTitle.classList.remove('not_active');
		}

		addGuestUserButtton.classList.remove('not_active');
		fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
		guestUserModal.classList.add('modal_active');
		guestUserModalBox.classList.add('modal_box_active');
	}

	if (e.target.classList.contains('video_template_delete_file_container') ||
		e.target.closest('.video_template_delete_file_container')) {
		let fileContainerPreload = e.target.closest('.file_container_preload'),
			fileContainerPreview = e.target.closest('.file_container_preview'),
			labelContainer = e.target.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value'),
			parent;

		if (fileContainerPreload) {
			let fieldPayloadState = fileContainerPreload.getAttribute('data-field_payload_state'),
				field = e.target.closest('.create_event_file_field_row').querySelector('input[type=file]');
			
			parent = fileContainerPreload.parentElement;

			for (let i = 0; i < videoFileList.files.length; i++) {
				if (videoFileList.files[i].index === Number(fieldPayloadState)) {
					videoFileList.items.remove(i);
				}
			}

			field.files = videoFileList.files;
			fileContainerPreload.remove();
		}

		if (fileContainerPreview) {
			let fieldDeleteFileIdList = document.querySelector('input[name="delete' + type[0].toUpperCase() + type.slice(1) + 'FileIdList"]'),
				fieldDeleteValue = fieldDeleteFileIdList.value;

			parent = fileContainerPreview.parentElement;

			if (!fieldDeleteValue) {
				fieldDeleteValue = [fileContainerPreview.dataset.file_id,];
			} else {
				fieldDeleteValue = fieldDeleteValue.split(',');
				fieldDeleteValue.push(fileContainerPreview.dataset.file_id);
			}

			fieldDeleteFileIdList.value = fieldDeleteValue;
			fileContainerPreview.remove();
		}

		if (parent.children.length < 1) {
			labelContainer.classList.remove('not_active');
		}
	}

	if (e.target.classList.contains('image_template_delete_file_container') ||
		e.target.closest('.image_template_delete_file_container')) {
		let fileContainerTemplate = e.target.closest('.image_file_container_template'),
			fileContainerPreview = e.target.closest('.image_file_container_preview'),
			labelContainer = e.target.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value'),
			parent,
			field = e.target.closest('.create_event_file_field_row').querySelector('input[type=file]');

		if (fileContainerTemplate) {
			let fieldPayloadState = fileContainerTemplate.getAttribute('data-field_payload_state');

			parent = fileContainerTemplate.parentElement;

			for (let i = 0; i < imageFileList.files.length; i++) {
				if (imageFileList.files[i].index === Number(fieldPayloadState)) {
					imageFileList.items.remove(i);
				}
			}

			field.files = imageFileList.files;
			fileContainerTemplate.remove();
		}

		if (fileContainerPreview) {
			let fieldDeleteFileIdList = document.querySelector('input[name="delete' + type[0].toUpperCase() + type.slice(1) + 'FileIdList"]'),
				fieldDeleteValue = fieldDeleteFileIdList.value;

			parent = fileContainerPreview.parentElement;

			if (!fieldDeleteValue) {
				fieldDeleteValue = [fileContainerPreview.dataset.file_id,];
			} else {
				fieldDeleteValue = fieldDeleteValue.split(',');
				fieldDeleteValue.push(fileContainerPreview.dataset.file_id);
			}

			fieldDeleteFileIdList.value = fieldDeleteValue;
			fileContainerPreview.remove();
		}

		if (parent.children.length < Number(field.dataset.max_value)) {
			labelContainer.classList.remove('not_active');
		}

		if (parent.children.length <= 0) {
			parent.classList.remove('mb_10');
		} else {
			parent.classList.add('mb_10');
		}
	}

	if (e.target.classList.contains('document_template_delete_file_container') ||
		e.target.closest('.document_template_delete_file_container')) {
		let fileContainerTemplate = e.target.closest('.document_file_container_template'),
			fileContainerPreview = e.target.closest('.document_file_container_preview'),
			labelContainer = e.target.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value'),
			parent,
			field = e.target.closest('.create_event_file_field_row').querySelector('input[type=file]');

		if (fileContainerTemplate) {
			let fieldPayloadState = fileContainerTemplate.getAttribute('data-field_payload_state');

			parent = fileContainerTemplate.parentElement;

			for (let i = 0; i < documentFileList.files.length; i++) {
				if (documentFileList.files[i].index === Number(fieldPayloadState)) {
					documentFileList.items.remove(i);
				}
			}

			field.files = documentFileList.files;
			fileContainerTemplate.remove();
		}

		if (fileContainerPreview) {
			let fieldDeleteFileIdList = document.querySelector('input[name="delete' + type[0].toUpperCase() + type.slice(1) + 'FileIdList"]'),
				fieldDeleteValue = fieldDeleteFileIdList.value;

			parent = fileContainerPreview.parentElement;

			if (!fieldDeleteValue) {
				fieldDeleteValue = [fileContainerPreview.dataset.file_id,];
			} else {
				fieldDeleteValue = fieldDeleteValue.split(',');
				fieldDeleteValue.push(fileContainerPreview.dataset.file_id);
			}

			fieldDeleteFileIdList.value = fieldDeleteValue;
			fileContainerPreview.remove();console.log(fieldDeleteFileIdList.value)
		}

		if (parent.children.length < Number(field.dataset.max_value)) {
			labelContainer.classList.remove('not_active');
		}

		if (parent.children.length <= 0) {
			parent.classList.remove('mb_10');
		} else {
			parent.classList.add('mb_10');
		}
	}

	if (e.target.closest('.video_file_preview_container') &&
		!e.target.classList.contains('video_template_delete_file_container') &&
		!e.target.closest('.video_template_delete_file_container')) {
		let videoContainer = e.target.closest('.video_file_preview_container');
		fetchGetVideoViewModal({'page': 0}, videoContainer);
	}

	if (e.target.classList.contains('image_file') &&
		e.target.parentElement.dataset.load_type === 'preload') {
		modalSliderImage.show(e.target);
	}

	if (e.target.closest('.image_file_container_preview') &&
		!e.target.classList.contains('image_template_delete_file_container') &&
		!e.target.closest('.image_template_delete_file_container')) {
		let imageContainer = e.target.closest('.image_file_container_preview'),
			fieldDeleteFileIdList = document.querySelector('input[name="delete' + type[0].toUpperCase() + type.slice(1) + 'FileIdList"]'),
			exceptionIdList = (fieldDeleteFileIdList.value) ?
				fieldDeleteFileIdList.value.split(',') :
				'',
			dataObj = {
				'type': TYPE_SHOW,
				'entity_id': taskHistoryId,
				'current_file_id': imageContainer.dataset.file_id,
				'exceptionIdList': exceptionIdList,
				'entityAction': 'taskHistorySlider',
			};

		fetchGetImageSliderViewModal(dataObj);
	}

	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}

	if (e.target.classList.contains('dot_menu_container_pic')) {
		let menuList = e.target.parentElement.querySelector('.file_menu_list');
		menuList.classList.add('menu_box_active');
	}

	if (!e.target.classList.contains('dot_menu_container_pic')) {
		let sliderImage = document.querySelector('.modal_image_slider'),
			modalVideo = document.querySelector('.modal_video');

		if (sliderImage) {
			let activeMenuList = sliderImage.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}

		if (modalVideo) {
			let activeMenuList = modalVideo.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}
	}

	if (e.target.classList.contains('create_event_button_submit')) {
		let timeOffsetSeconds = new Date().getTimezoneOffset() * (-60);
		document.querySelector('input[name="timeOffsetSeconds"]').value = timeOffsetSeconds;
		appendInputCR(document.querySelector('form'));
		document.querySelector('form').submit();
	}
});

addGuestUserButtton.addEventListener('click', function() {
	fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
	guestUserModal.classList.add('modal_active');
	guestUserModalBox.classList.add('modal_box_active');
});

closeGuestUserButton.addEventListener('click', function() {
	guestUserModalBox.classList.remove('modal_box_active');

	setTimeout(function() {
		guestUserModal.classList.remove('modal_active');
	}, 300);
});

if (fileVideoField) {
	fileVideoField.addEventListener('change', function() {
		checkAndUploadFile(this, FILE_TYPE_VIDEO, videoFileList);
	});
}

if (fileImageField) {
	fileImageField.addEventListener('change', function() {
		checkAndUploadFile(this, FILE_TYPE_IMAGE, imageFileList);
	});
}

if (fileDocumentField) {
	fileDocumentField.addEventListener('change', function() {
		checkAndUploadFile(this, FILE_TYPE_DOCUMENT, documentFileList);
	});
}

if (fileVideoField) {
	fileVideoField.addEventListener('click', function() {
		clearErrorFileInput(this);
	});
}

if (fileImageField) {
	fileImageField.addEventListener('click', function() {
		clearErrorFileInput(this);
	});
}

if (fileDocumentField) {
	fileDocumentField.addEventListener('click', function() {
		clearErrorFileInput(this);
	});
}

function checkMaxItemField(field, existList, maxValue) {
	let result = {'error': ''};

	if (Number(field.files.length) + Number(existList.files.length) > Number(maxValue)) {
		result.error = field.dataset.error_max_value;
	}

	return result;
}

function checkAndUploadFile(field, fileType, fileList) {
	let checkResult = checkUploadFile(field, fileType),
		checkMax = checkMaxItemField(field, fileList, Number(field.dataset.max_value)),
		container = field.closest('.container_create_new_event');

	if (checkResult.error) {
		let noticeElem = field.closest('.create_event_file_field_row').querySelector('.notice_elem');
		noticeElem.innerHTML = checkResult.error;
		noticeElem.classList.add('notice_elem_full_width_active');
	} else if (checkMax.error) {
		let noticeElem = field.closest('.create_event_file_field_row').querySelector('.notice_elem');
		noticeElem.innerHTML = checkMax.error;
		noticeElem.classList.add('notice_elem_full_width_active');
	} else {
		insertLoadFile(field, fileType);
	}
}

function clearErrorFileInput(field) {
	let noticeElem = field.closest('.create_event_file_field_row').querySelector('.notice_elem');

	if (noticeElem.classList.contains('notice_elem_full_width_active')) {
		noticeElem.classList.remove('notice_elem_full_width_active');
		noticeElem.innerHTML = '';
	}
}

function insertLoadFile(field, fileType) {
	let insertContainer,
		templateContainer;

	if (fileType === FILE_TYPE_VIDEO) {
		insertContainer = document.querySelector('.create_event_video_container');
		templateContainer = document.querySelector('.video_file_template_container');
	}

	if (fileType === FILE_TYPE_IMAGE) {
		insertContainer = document.querySelector('.create_event_image_container');
		templateContainer = document.querySelector('.image_file_template_container');
	}

	if (fileType === FILE_TYPE_DOCUMENT) {
		insertContainer = document.querySelector('.create_event_document_container');
		templateContainer = document.querySelector('.document_file_template_container');
	}

	for (let i = 0; i < field.files.length; i++) {
		let cloneTemplate = templateContainer.firstElementChild.cloneNode(true);

		if (fileType === FILE_TYPE_VIDEO) {
			let videoEl = cloneTemplate.querySelector('.video_el');

			videoEl.src = URL.createObjectURL(field.files[i]);
			videoFileList.items.add(field.files[i]);
			new VideoFileExistElement(cloneTemplate);

			for (let j = 0; j < videoFileList.files.length; j++) {
				if (videoFileList.files[j] === field.files[i]) {
					videoFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}

			insertContainer.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value').
				classList.add('not_active');
		}

		if (fileType === FILE_TYPE_IMAGE) {
			let imageEl = cloneTemplate.querySelector('.image_file');

			imageEl.src = URL.createObjectURL(field.files[i]);
			imageFileList.items.add(field.files[i]);

			for (let j = 0; j < imageFileList.files.length; j++) {
				if (imageFileList.files[j] === field.files[i]) {
					imageFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}
		}

		if (fileType === FILE_TYPE_DOCUMENT) {
			let nameEl = cloneTemplate.querySelector('.document_name_block_template');

			nameEl.innerHTML = field.files[i].name;
			documentFileList.items.add(field.files[i]);

			for (let j = 0; j < documentFileList.files.length; j++) {
				if (documentFileList.files[j] === field.files[i]) {
					documentFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}
		}

		cloneTemplate.setAttribute('data-load_type', 'preload');
		insertContainer.prepend(cloneTemplate);
	}

	if (fileType === FILE_TYPE_VIDEO) {
		field.files = videoFileList.files;
	}

	if (fileType === FILE_TYPE_IMAGE) {
		if (insertContainer.children.length > 0 && insertContainer.children.length < Number(field.dataset.max_value)) {
			insertContainer.classList.add('mb_10');
		}

		if (insertContainer.children.length === Number(field.dataset.max_value) && insertContainer.classList.contains('mb_10')) {
			insertContainer.classList.remove('mb_10');
		}

		field.files = imageFileList.files;

		if (insertContainer.children.length >= Number(field.dataset.max_value)) {
			insertContainer.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value').
				classList.add('not_active');
		}
	}

	if (fileType === FILE_TYPE_DOCUMENT) {
		if (insertContainer.children.length > 0 && insertContainer.children.length < Number(field.dataset.max_value)) {
			insertContainer.classList.add('mb_10');
		}

		if (insertContainer.children.length === Number(field.dataset.max_value) && insertContainer.classList.contains('mb_10')) {
			insertContainer.classList.remove('mb_10');
		}

		field.files = documentFileList.files;

		if (insertContainer.children.length >= Number(field.dataset.max_value)) {
			insertContainer.closest('.create_event_file_field_row').querySelector('.create_event_file_field_value').
				classList.add('not_active');
		}
	}
}

function closeCalendarBox() {
	if (calendarBox.classList.contains('calendar_box_position_top') || calendarBox.classList.contains('calendar_box_active')) {
		calendarBox.classList.remove('calendar_box_active');

		setTimeout(function() {
			calendarBox.classList.remove('calendar_box_position_top');
			calendarContainer.append(calendarBox);
			calendarClass.count = 0;
			calendarClass.init();
		}, 200);
	}
}

function closeSelectTimeBox() {
	if (selectTimeBox.classList.contains('select_time_box_position_top') || selectTimeBox.classList.contains('select_time_box_active')) {
		selectTimeBox.classList.remove('select_time_box_active');

		setTimeout(function() {
			selectTimeBox.classList.remove('select_time_box_position_top');
			selectTimeContainer.append(selectTimeBox);
		}, 200);

		document.body.classList.remove('not_scroll');
		document.body.style.paddingRight = '';
		document.querySelector('.autorized_or_personal_block').style.paddingRight = '';
	}
}

function isPastDateTimeFromValue() {
	let now = new Date().valueOf(),
		dateArr = dateField.innerHTML.split('-'),
		timeArr = timeField.innerHTML.split(':'),
		dateValue = new Date(dateArr[2], dateArr[1] - 1, dateArr[0], timeArr[0], timeArr[1]).valueOf();

	if (now > dateValue) {
		return true;
	}

	return false;
}

function getDataForInviteUsers() {
	let invitedFilterBlock = modalAddGuestUserBox.querySelector('.invited_filter_block'),
		searchField = modalAddGuestUserBox.querySelector('.invited_search_field'),
		onlyFriends = invitedFilterBlock.querySelector('.invited_only_friends').checked,
		invitedSelectGroup = invitedFilterBlock.querySelector('select[name="invited_select_group"]'),
		result = {};

	if (searchField.value) {
		result.searchString = searchField.value;
	}

	result.only_friends = Number(onlyFriends);
	// result.group_id = invitedSelectGroup.options[invitedSelectGroup.selectedIndex].getAttribute('data-group_id');
	// result.within_group = 0;

	// if (inviteContainer.querySelector('.within_group') && !inviteContainer.querySelector('.within_group').classList.contains('not_active')) {
	// 	result.within_group = inviteContainer.querySelector('.within_group').value;
	// }

	if (type === 'event' || type === 'meeting') {
		result.type = 'invite';
	}

	if (type === 'task') {
		result.type = 'assign';
	}

	if (addedUserContainer && addedUserContainer.children.length > 0) {
		let exceptionUserIdList = [];

		for (let el of addedUserContainer.children) {
			if (!el.classList.contains('no_events_title')) {
				exceptionUserIdList.push(el.getAttribute('data-profile_id'));
			}
		}

		if (exceptionUserIdList.length > 0) {
			result.exceptionUserIdList = exceptionUserIdList;
		}
	}

	return result;
}

function editValueGuestListInput(id, action) {
	let value = guestUserIdListInput.value;

	if (action === 'add') {
		if (value) {
			value = value.split(',');
			value.push(id);
		} else {
			value = [id];
		}
	}

	if (action === 'remove') {
		if (value) {
			value = value.split(',');

			for (let i = 0; i < value.length; i++) {
				if (Number(value[i]) === Number(id)) {
					value.splice(i, 1);
				}
			}
		}
	}

	guestUserIdListInput.value = value;
}

function getDateTimeInputValueForEdit() {
	let timeOffsetSeconds = new Date().getTimezoneOffset() * (-60) * (1000),
		dateDate = new Date(Number(dateInput.value)),
		dateTime = new Date(Number(timeInput.value) - Number(timeOffsetSeconds)),
		day = dateDate.getDate(),
		month = dateDate.getMonth() + 1,
		year = dateDate.getFullYear(),
		hours = dateTime.getHours(),
		minutes = dateTime.getMinutes();

	if ((month + '').length < 2) {
		month = '0' + month;
	}

	if ((hours + '').length < 2) {
		hours = '0' + hours;
	}

	if ((minutes + '').length < 2) {
		minutes = '0' + minutes;
	}

	dateField.innerHTML = day + '-' + month + '-' + year;
	timeField.innerHTML = hours + ':' + minutes;
	dateInput.value = Number(dateInput.value) / 1000;
	timeInput.value = Number(timeInput.value) / 1000;
}