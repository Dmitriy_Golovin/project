function ajaxSendDataSettings(chekBoxList, partOfIdChangeEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = getCheckBoxData(chekBoxList).slice(1) + '&' + 'change_field=' + partOfIdChangeEl + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');
console.log(partOfIdChangeEl)
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 1) {
					let dataForTabSync = {
							'ws_tab_sync_to_do': 'changeSettings',
							'changes': result.data
						};

					tab.send(tab.tabSync.collectData(dataForTabSync));
					
					if (result.data.action == 'add') {
						let calendarDateList = calendar.getDataForCalendarEvent();
						ajaxGetContentInCell(calendarDateList['dateList'], calendarDateList['dateOfFirstEl'], calendarDateList['dateOfLastEl'], calendarDateList['cellList'], calendarDateList['yearList'], calendarDateList['monthList']);
					} else if (result.data.action == 'remove') {
						tab.tabSync._minorClearCellBySelectedValue(result.data.applyFor);
					}
				} else if (result['res'] == 0) {
					console.log(result.errors);
				}
			}
		};
		
		xhr.open('POST', '/calendar/changeSettings');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	})
}

function getCheckBoxData(chekBoxList) {
	let result = '&';
	for (let el of chekBoxList) {
		if (el.checked) {
			result += el.id.slice(0, el.id.lastIndexOf('_')) + '=1&';
		} else {
			result += el.id.slice(0, el.id.lastIndexOf('_')) + '=0&';
		}
	}
	return result.slice(0, result.length - 1);
}

// function getItemChangeAndAction(strData, desiredItem) {
// 	let action, 
// 		data = '',
// 		arrData = strData.split('&');
// 	for (let i = 0; i < arrData.length - 1; i++) {
// 		if (arrData[i].slice(0, arrData[i].indexOf('=')) == desiredItem) {
// 			action = (arrData[i].slice(arrData[i].indexOf('=') + 1) == 1) ? 'add' : 'remove';
// 			arrData[i] = arrData[i].slice(0, arrData[i].indexOf('=') + 1) + arrData[i].slice(arrData[i].indexOf('=') + 1);
// 		} else {
// 			arrData[i] = arrData[i].slice(0, arrData[i].indexOf('=') + 1) + 0;
// 		}
// 		data += arrData[i] + '&';
// 	}
	
// 	return {
// 		'action': action,
// 		'applyFor': desiredItem,
// 		'data': '&' + data.slice(0, -1)
// 	}
// }