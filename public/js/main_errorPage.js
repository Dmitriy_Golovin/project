/*jshint esversion: 6 */

var cnv = document.querySelector('.tv_screen'),
	cnv404 = document.querySelector('.tv_screen_404');
	

var intervalCNV = setInterval(function() {
	drawNoiseOnScreen(cnv);
}, 100);

drawErrorText(cnv, cnv404);

setTimeout(function() {
	clearInterval(intervalCNV);
}, 8000);