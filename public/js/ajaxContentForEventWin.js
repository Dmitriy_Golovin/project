/*jshint esversion: 6 */

function ajaxGetContentForEventWin(el, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			holidaysBox = document.querySelector('.holidays_box'),
			newWinEventContentContainer = document.querySelector('.new_win_event_content_container'),
			year = document.querySelector('#show-date').innerText.slice(-4),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'date=' + JSON.stringify(getDataForAjax(el)) +
				'&eventIdList=' + el.getAttribute('data-event_id_list') +
				'&meetingIdList=' + el.getAttribute('data-meeting_id_list') +
				'&taskIdList=' + el.getAttribute('data-task_id_list') +
				'&year=' + year +
				'&csrf_test_name=' + csrfVal.getAttribute('content') +
				'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60);

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				 if (result.res == 1) {
					newWinEventContentContainer.insertAdjacentHTML('afterbegin', result.html);
					callback();
				} else if (result.res == 0) {
					console.log(result.errors);
					callback();
				}
			}
		};

		xhr.open('POST', '/calendar/getContentForEventWin');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function getDataForAjax(el) {
	let result = [];
	result.push(el.getAttribute('data-date_for_cell').substr(el.getAttribute('data-date_for_cell').lastIndexOf('.') + 1));
	
	if (el.hasAttribute('data-holiday_date_for_cell')) {
		result.push(el.getAttribute('data-holiday_date_for_cell'));
	}
	if (el.hasAttribute('data-var_holiday_date_for_cell')) {
		result.push(el.getAttribute('data-var_holiday_date_for_cell'));
	}
	if (el.hasAttribute('data-last_holiday_date_for_cell')) {
		result.push(el.getAttribute('data-last_holiday_date_for_cell'));
	}

	return result;
}