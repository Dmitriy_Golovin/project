function showHideInputElem(attribute) {

	if (!attribute) return;
	
	var elemList = document.getElementsByClassName(attribute),
		tempP = '';

	for (var i = 0; i < elemList.length; i++) {

		if (!elemList[i].classList.contains('not_active') && elemList[i].tagName === 'P') {
			tempP = elemList[i].innerHTML;
		} else if (!elemList[i].classList.contains('not_active') && elemList[i].firstElementChild) {
			elemList[i - 1].innerHTML = tempP;
		}
		
		elemList[i].classList.toggle('not_active');
		
		if (!elemList[i].classList.contains('not_active') && elemList[i].firstElementChild) {
			elemList[i].firstElementChild.focus();
			elemList[i].firstElementChild.value = tempP;
		} else if (!elemList[i].classList.contains('not_active') && elemList[i].tagName === 'P') {
			tempP = elemList[i + 1].firstElementChild.value;
			if (elemList[i + 1].firstElementChild.options && elemList[i + 1].firstElementChild.selectedIndex >= 0) {
				dataAttrObj = elemList[i + 1].firstElementChild.options[elemList[i + 1].firstElementChild.selectedIndex].dataset;

				for (key in dataAttrObj) {
					elemList[i].setAttribute('data-' + key, dataAttrObj[key])
				}
			}
		}
	
	}
	
}

function closeNotCurrentActiveInput(activeEl) {
	let boxEl = activeEl.parentElement.parentElement,
		pEl = [],
		divEl = [],
		imgEl = [];

	for (let node of boxEl.children) {
		if (node.children.length < 1) continue;
		for (let innerNode of node.children) {  
			if (innerNode.tagName == 'P' && innerNode.classList.contains('qqq')) {
				pEl.push(innerNode)
			}
			
			if (innerNode.tagName == 'DIV' && innerNode.classList.contains('edit_profile_inp')) {
				divEl.push(innerNode);
			}
			
			if (innerNode.tagName == 'IMG' && innerNode.classList.contains('edit_icon')) {
				imgEl.push(innerNode);
			}
		}
	}
	
	for (let i = 0; i < imgEl.length; i++) {
		if (imgEl[i] != activeEl && imgEl[i].getAttribute('src') != '/img/edit_icon.png') {
			if (divEl[i]) pEl[i].innerHTML = divEl[i].children[0].value;

			if (divEl[i].children[0].options && divEl[i].children[0].selectedIndex >= 0) {
				dataAttrObj = divEl[i].children[0].options[divEl[i].children[0].selectedIndex].dataset;

				for (key in dataAttrObj) {
					pEl[i].setAttribute('data-' + key, dataAttrObj[key])
				}
			}

			imgEl[i].setAttribute('src', '/img/edit_icon.png');
			pEl[i].classList.remove('not_active');
			if (divEl[i]) divEl[i].classList.add('not_active');
		}
	}
	
}

function validationFieldPersonalData(box) {
	let reportTextArr = [];
	
	if (box == 'personal_data') {
		let boxEl = document.querySelector('.' + box),
			notEmptyElArr = boxEl.querySelectorAll('.not_empty'),
			avaEl = boxEl.querySelector('.input_edit_ava'),
			reportEl = boxEl.querySelector('.notice_elem_ava');
			
		for (let node of notEmptyElArr) {
			if (node.innerHTML == '') {
				node.parentElement.classList.add('error_el');
				reportTextArr.push('заполните поля');
			}
		}
		
		/* if (avaEl.files[0]) {
			let avaName = avaEl.files[0].name.replace(/ /g, '_'),
				exec = avaName.slice(avaName.lastIndexOf('.') + 1).toLowerCase();
			
			if (!exec.match(/(png|jpg|jpeg|gif)/)) {
				avaEl.parentElement.classList.add('error_el');
				reportTextArr.push('Возможный формат JPG, GIF, PNG');
			}
		} */
		
		return showErrorReport(reportEl);
	}
	
	function showErrorReport(reportEl) {
		if (reportTextArr.length > 0) {
			let reportText = '';
			for (let i = 0; i < reportTextArr.length; i++) {
				reportText += reportTextArr[i];
				if (i < reportTextArr.length - 1) reportText += '<br>';
			}
			reportEl.classList.add('notice_elem_ava_active');
			reportEl.innerHTML = reportText;
			return false;
		} else {
			return true;
		}
	}
}