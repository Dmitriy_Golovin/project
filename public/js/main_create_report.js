/*jshint esversion: 6 */

const FILE_TYPE_IMAGE = 1;
const FILE_TYPE_VIDEO = 3;

let mainBox = document.querySelector('.main_box'),
	id = mainBox.getAttribute('data-entity_id'),
	action = mainBox.getAttribute('data-action'),
	type = mainBox.getAttribute('data-type'),
	maxImage = mainBox.dataset.max_image,
	maxVideo = mainBox.dataset.max_video,
	videoFileList = new DataTransfer(),
	imageFileList = new DataTransfer(),
	timeOutVideoMediaControl,
   	activeTimeout = false,
   	fullScreen = false,
   	modalSliderImage,
   	reportId;

if (action === 'edit') {
	reportId = mainBox.dataset.report_id;
}

window.addEventListener('load', function() {
	fetchGetCommonEventDataSaveReport(function() {
		let reportNoteField = document.querySelector('.report_note_field'),
			reportVideoField = document.querySelector('#load_report_video'),
			reportPhotoField = document.querySelector('#load_report_photo'),
			reportPhotoContainer = document.querySelector('.report_photo_container');
		
		modalSliderImage = new ModalSliderImage({
			'modalCssClass': 'modal_slider_image',
			'availableImageListContainer': reportPhotoContainer,
		});

		reportNoteField.addEventListener('input', function() {
			if (this.textContent.length < 1) {
				this.innerHTML = '';
			}
		});

		reportVideoField.addEventListener('change', function() {
			let checkResult = checkUploadFile(this, FILE_TYPE_VIDEO),
				checkMax = checkMaxItemField(this, videoFileList, maxVideo);

			if (checkResult.error) {
				let noticeElem = this.closest('.create_report_field_row').querySelector('.notice_elem');
				noticeElem.innerHTML = checkResult.error;
				noticeElem.classList.add('notice_elem_full_width_active');
			} else if (checkMax.error) {
				let noticeElem = this.closest('.create_report_field_row').querySelector('.notice_elem');
				noticeElem.innerHTML = checkMax.error;
				noticeElem.classList.add('notice_elem_full_width_active');
			} else {
				insertLoadFile(this, FILE_TYPE_VIDEO);
			}
		});

		reportVideoField.addEventListener('click', function() {
			let noticeElem = this.closest('.create_report_field_row').querySelector('.notice_elem');

			if (noticeElem.classList.contains('notice_elem_full_width_active')) {
				noticeElem.classList.remove('notice_elem_full_width_active');
				noticeElem.innerHTML = '';
			}
		});

		reportPhotoField.addEventListener('change', function() {
			let checkResult = checkUploadFile(this, FILE_TYPE_IMAGE),
				checkMax = checkMaxItemField(this, imageFileList, maxImage);

			if (checkResult.error) {
				let noticeElem = this.closest('.create_report_field_row').querySelector('.notice_elem');
				noticeElem.innerHTML = checkResult.error;
				noticeElem.classList.add('notice_elem_full_width_active');
			} else if (checkMax.error) {
				let noticeElem = this.closest('.create_report_field_row').querySelector('.notice_elem');
				noticeElem.innerHTML = checkMax.error;
				noticeElem.classList.add('notice_elem_full_width_active');
			} else {
				insertLoadFile(this, FILE_TYPE_IMAGE);
			}
		});

		reportPhotoField.addEventListener('click', function() {
			let noticeElem = this.closest('.create_report_field_row').querySelector('.notice_elem');

			if (noticeElem.classList.contains('notice_elem_full_width_active')) {
				noticeElem.classList.remove('notice_elem_full_width_active');
				noticeElem.innerHTML = '';
			}
		});
	});
});

document.body.addEventListener('mouseover', function(e) {
	if (e.target.classList.contains('notice_icon')) {
		let noticeLabelMessage = e.target.parentElement.querySelector('.notice_label_message');

		noticeLabelMessage.classList.remove('not_active');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active_opacity');
			noticeLabelMessage.setAttribute('data-notice_label_state', 'active');
		}, 5);
	}
});

document.body.addEventListener('mouseout', function(e) {
	if (e.target.classList.contains('notice_icon')) {
		let noticeLabelMessage = e.target.parentElement.querySelector('.notice_label_message');

		noticeLabelMessage.classList.add('not_active_opacity');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active');
		}, 5);
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('note_label')) {
		closeNoticeLabelMessege();
		let noticeLabelMessage = e.target.querySelector('.notice_label_message');

		noticeLabelMessage.classList.remove('not_active');

		setTimeout(function() {
			noticeLabelMessage.classList.remove('not_active_opacity');
		}, 5);
	}

	if (!e.target.classList.contains('note_label')) {
		closeNoticeLabelMessege();
	}

	if (e.target.classList.contains('video_template_delete_file_container') ||
		e.target.closest('.video_template_delete_file_container')) {
		let fileContainerPreload = e.target.closest('.file_container_preload'),
			fileContainerPreview = e.target.closest('.file_container_preview'),
			labelContainer = e.target.closest('.create_report_field_row').querySelector('.create_report_field_value'),
			parent;

		if (fileContainerPreload) {
			let fieldPayloadState = fileContainerPreload.getAttribute('data-field_payload_state'),
				field = e.target.closest('.create_report_field_row').querySelector('input[type=file]');
			
			parent = fileContainerPreload.parentElement;

			for (let i = 0; i < videoFileList.files.length; i++) {
				if (videoFileList.files[i].index === Number(fieldPayloadState)) {
					videoFileList.items.remove(i);
				}
			}

			field.files = videoFileList.files;
			fileContainerPreload.remove();
		}

		if (fileContainerPreview) {
			let fieldDeleteFileIdList = document.querySelector('input[name="deleteFileIdList"]'),
				fieldDeleteValue = fieldDeleteFileIdList.value;

			parent = fileContainerPreview.parentElement;

			if (!fieldDeleteValue) {
				fieldDeleteValue = [fileContainerPreview.dataset.file_id,];
			} else {
				fieldDeleteValue = fieldDeleteValue.split(',');
				fieldDeleteValue.push(fileContainerPreview.dataset.file_id);
			}

			fieldDeleteFileIdList.value = fieldDeleteValue;
			fileContainerPreview.remove();
		}

		if (parent.children.length < 1) {
			labelContainer.classList.remove('not_active');
		}
	}

	if (e.target.classList.contains('image_template_delete_file_container') ||
		e.target.closest('.image_template_delete_file_container')) {
		let fileContainerTemplate = e.target.closest('.image_file_container_template'),
			fileContainerPreview = e.target.closest('.image_file_container_preview'),
			labelContainer = e.target.closest('.create_report_field_row').querySelector('.create_report_field_value'),
			parent;

		if (fileContainerTemplate) {
			let fieldPayloadState = fileContainerTemplate.getAttribute('data-field_payload_state'),
				field = e.target.closest('.create_report_field_row').querySelector('input[type=file]');

			parent = fileContainerTemplate.parentElement;

			for (let i = 0; i < imageFileList.files.length; i++) {
				if (imageFileList.files[i].index === Number(fieldPayloadState)) {
					imageFileList.items.remove(i);
				}
			}

			field.files = imageFileList.files;
			fileContainerTemplate.remove();
		}

		if (fileContainerPreview) {
			let fieldDeleteFileIdList = document.querySelector('input[name="deleteFileIdList"]'),
				fieldDeleteValue = fieldDeleteFileIdList.value;

			parent = fileContainerPreview.parentElement;

			if (!fieldDeleteValue) {
				fieldDeleteValue = [fileContainerPreview.dataset.file_id,];
			} else {
				fieldDeleteValue = fieldDeleteValue.split(',');
				fieldDeleteValue.push(fileContainerPreview.dataset.file_id);
			}

			fieldDeleteFileIdList.value = fieldDeleteValue;
			fileContainerPreview.remove();
		}

		if (parent.children.length < 10) {
			labelContainer.classList.remove('not_active');
		}

		if (parent.children.length <= 0) {
			parent.classList.remove('mb_10');
		} else {
			parent.classList.add('mb_10');
		}
	}

	if (e.target.closest('.video_file_preview_container') &&
		!e.target.classList.contains('video_template_delete_file_container') &&
		!e.target.closest('.video_template_delete_file_container')) {
		let videoContainer = e.target.closest('.video_file_preview_container');
		fetchGetVideoViewModal({'page': 0}, videoContainer);
	}

	if (e.target.classList.contains('image_file') &&
		e.target.parentElement.dataset.load_type === 'preload') {
		modalSliderImage.show(e.target);
	}

	if (e.target.closest('.image_file_container_preview') &&
		!e.target.classList.contains('image_template_delete_file_container') &&
		!e.target.closest('.image_template_delete_file_container')) {
		let imageContainer = e.target.closest('.image_file_container_preview'),
			fieldDeleteFileIdList = document.querySelector('input[name="deleteFileIdList"]'),
			exceptionIdList = (fieldDeleteFileIdList.value) ?
				fieldDeleteFileIdList.value.split(',') :
				'',
			dataObj = {
				'type': TYPE_SHOW,
				'entity_id': reportId,
				'current_file_id': imageContainer.dataset.file_id,
				'exceptionIdList': exceptionIdList,
				'entityAction': 'reportSlider',
			};

		fetchGetImageSliderViewModal(dataObj);
	}

	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}

	if (e.target.classList.contains('dot_menu_container_pic')) {
		let menuList = e.target.parentElement.querySelector('.file_menu_list');
		menuList.classList.add('menu_box_active');
	}

	if (!e.target.classList.contains('dot_menu_container_pic')) {
		let sliderImage = document.querySelector('.modal_image_slider'),
			modalVideo = document.querySelector('.modal_video');

		if (sliderImage) {
			let activeMenuList = sliderImage.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}

		if (modalVideo) {
			let activeMenuList = modalVideo.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}
	}

	if (e.target.classList.contains('create_report_button_save')) {
		let noteHiddenField = document.createElement('input');

		noteHiddenField.setAttribute('type', 'hidden');
		noteHiddenField.setAttribute('name', 'note');
		noteHiddenField.value = document.querySelector('.report_note_field').innerHTML;
		document.querySelector('#save_report').append(noteHiddenField);
		appendInputCR(document.querySelector('#save_report'));
		document.querySelector('#save_report').submit();
	}
});

function closeNoticeLabelMessege() {
	let noticeLabelMessage = document.querySelector('.notice_label_message[data-notice_label_state="active"]');

	if (noticeLabelMessage) {
		noticeLabelMessage.classList.add('not_active_opacity');

		setTimeout(function() {
			noticeLabelMessage.classList.add('not_active');
			noticeLabelMessage.setAttribute('data-notice_label_state', 'not-active');
		}, 200);
	}
}

function checkMaxItemField(field, existList, maxValue) {
	let result = {'error': ''};

	if (Number(field.files.length) + Number(existList.files.length) > Number(maxValue)) {
		result.error = field.dataset.error_max_value;
	}

	return result;
}

function insertLoadFile(field, type) {
	let insertContainer,
		templateContainer,
		form = document.querySelector('form');

	if (type === FILE_TYPE_VIDEO) {
		insertContainer = document.querySelector('.report_video_container');
		templateContainer = document.querySelector('.video_file_template_container');
	}

	if (type === FILE_TYPE_IMAGE) {
		insertContainer = document.querySelector('.report_photo_container');
		templateContainer = document.querySelector('.image_file_template_container');
	}

	for (let i = 0; i < field.files.length; i++) {
		let cloneTemplate = templateContainer.firstElementChild.cloneNode(true);

		if (type === FILE_TYPE_VIDEO) {
			let videoEl = cloneTemplate.querySelector('.video_el');

			videoEl.src = URL.createObjectURL(field.files[i]);
			videoFileList.items.add(field.files[i]);
			new VideoFileExistElement(cloneTemplate);

			for (let j = 0; j < videoFileList.files.length; j++) {
				if (videoFileList.files[j] === field.files[i]) {
					videoFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}

			document.querySelector('.create_report_load_video').parentElement.classList.add('not_active');
		}

		if (type === FILE_TYPE_IMAGE) {
			let imageEl = cloneTemplate.querySelector('.image_file');

			imageEl.src = URL.createObjectURL(field.files[i]);
			imageFileList.items.add(field.files[i]);

			for (let j = 0; j < imageFileList.files.length; j++) {
				if (imageFileList.files[j] === field.files[i]) {
					imageFileList.files[j].index = j;
					cloneTemplate.setAttribute('data-field_payload_state', j);
				}
			}
		}

		cloneTemplate.setAttribute('data-load_type', 'preload');
		insertContainer.prepend(cloneTemplate);
	}

	if (type === FILE_TYPE_VIDEO) {
		field.files = videoFileList.files;
	}

	if (type === FILE_TYPE_IMAGE) {
		if (insertContainer.children.length > 0 && insertContainer.children.length < 10/* && action === 'create'*/) {
			insertContainer.classList.add('mb_10');
		}

		if (insertContainer.children.length === 10 && insertContainer.classList.contains('mb_10')) {
			insertContainer.classList.remove('mb_10');
		}

		field.files = imageFileList.files;

		if (insertContainer.children.length >= 10) {
			document.querySelector('.create_report_load_photo').parentElement.classList.add('not_active');
		}
	}
}