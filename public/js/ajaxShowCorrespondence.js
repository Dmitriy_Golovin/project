/*jshint esversion: 6 */

function ajaxShowСorrespondence(parentEl, dataObj, rowNum, timestamp) {
	tab.xjscsrf(function() {
		let xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
			data = 'act=' + dataObj.act + /*'&' + 'step=' + dataObj.step +*/ '&' + 'with_user_id=' + dataObj.id + '&' + 'timeOffsetSeconds=' + timeOffsetSeconds + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		if (!timestamp) {
			timestamp = parseInt(Date.now() / 1000);
		}

		if (rowNum) {
			data += '&rowNum=' + rowNum;
		}

		data += '&timestamp=' + timestamp;

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				let result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				if (result.res) {
					if (result.action === 'list') {
						let searchMessagesField = document.querySelector('.search_messages_field'),
							createChatButton = document.querySelector('.create_chat_button'),
							backToAllCorrespondenceButton = document.querySelector('.back_to_all_correspondence_button'),
							correspondenceFullnameOrChatRoom = document.querySelector('.correspondence_fullname_or_chat_room');

						searchMessagesField.classList.remove('not_active');
						createChatButton.classList.remove('not_active');
						backToAllCorrespondenceButton.classList.add('not_active');
						correspondenceFullnameOrChatRoom.classList.add('not_active');

						document.querySelector('.write_message_box').classList.add('not_active');
						parentEl.classList.remove('messages_block_with_text_field');
						parentEl.innerHTML = '';
						parentEl.insertAdjacentHTML('afterBegin', result.html);
					}

					if (result.action === 'details') {
						if (result.data.insert_type === 'renew') {
							parentEl.innerHTML = '';
						}

						showSelectedDialogueOrChat(result.data.messageResult, result.data.messageCount, result.data.timestamp, parentEl, dataObj.addType);
					}

					if (result.empty) {
						let emptyTitle = document.createElement('h2');
						emptyTitle.innerHTML = result.empty;
						emptyTitle.classList.add('empty_title');
						parentEl.append(emptyTitle);
					}
				}

				if (!result.res) {
					console.log(result.errors);
				}
			}
		};

		xhr.open('POST', '/message/getCorrespondence');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function showAllDialogs(dataObj, parentEl) {
	let messagesTitleEl = parentEl.previousElementSibling;
		writeMessageBlock = parentEl.nextElementSibling;
		
	messagesTitleEl.children[0].classList.remove('not_active');
	messagesTitleEl.children[1].classList.remove('not_active');
	writeMessageBlock.classList.add('not_active');
	parentEl.classList.remove('messages_block_with_text_field');
	if (!messagesTitleEl.children[2].classList.contains('not_active')) {
		messagesTitleEl.children[2].classList.add('not_active');
		messagesTitleEl.children[3].classList.add('not_active');
	}

	for (let i = 0; i < dataObj.length; i++) {
		let item = document.createElement('div'),
			avaBox = document.createElement('div'),
			ava = document.createElement('img'),
			userNameAndDateBox = document.createElement('div'),
			userNameBox = document.createElement('div'),
			firstNameEl = document.createElement('span'),
			lastNameEl = document.createElement('span'),
			dateBox = document.createElement('div'),
			dateEl = document.createElement('span'),
			timeEl = document.createElement('span'),
			textBox = document.createElement('div'),
			textEl = document.createElement('span'),
			deleteEl = document.createElement('span'),
			deletePic = document.createElement('img');

			
		item.classList.add('dialog_chat_border');
		item.classList.add('dialog_chat_item');
		item.classList.add('input_search_item');
		deleteEl.classList.add('delete_correspondence');
		deleteEl.classList.add('not_active_opacity');
		deletePic.classList.add('delete_correspondence_pic');
		deletePic.src = '/img/close_.png';

		dataObj[i].text = dataObj[i].text.replace(/\n/g, '<br>');
		
		if (dataObj[i].user_to === tab.userID) {
			if (dataObj[i].new_messages_from > 0) textBox.classList.add('dialog_chat_item_not_read_message');
			item.setAttribute('data-profile_id', dataObj[i].user_from);
			ava.setAttribute('src', dataObj[i].user_from_ava);
			firstNameEl.innerHTML = dataObj[i].user_from_fullname.slice(0, dataObj[i].user_from_fullname.indexOf(' '));
			lastNameEl.innerHTML = dataObj[i].user_from_fullname.slice(dataObj[i].user_from_fullname.indexOf(' '));
			textEl.classList.add('text_dialog_chat');
			textEl.innerHTML = dataObj[i].text;
		} else {
			if (dataObj[i].self_message_not_read) textBox.classList.add('dialog_chat_item_not_read_message');
			let avaSelf = document.createElement('img');
			avaSelf.classList.add('ava_self_message_dialog_chat');
			avaSelf.setAttribute('src', dataObj[i].user_from_ava);
			item.setAttribute('data-profile_id', dataObj[i].user_to);
			ava.setAttribute('src', dataObj[i].user_to_ava);
			firstNameEl.innerHTML = dataObj[i].user_to_fullname.slice(0, dataObj[i].user_to_fullname.indexOf(' '));
			lastNameEl.innerHTML = dataObj[i].user_to_fullname.slice(dataObj[i].user_to_fullname.indexOf(' '));
			textEl.classList.add('text_self_dialog_chat');
			textEl.innerHTML = dataObj[i].text;
			textBox.appendChild(avaSelf);
		}
		
		if (dataObj[i].new_messages_from > 0) {
			let newMessagesCountEl = document.createElement('span');
			newMessagesCountEl.classList.add('new_messages_count');
			newMessagesCountEl.innerHTML = '+' + dataObj[i].new_messages_from;
			textBox.appendChild(newMessagesCountEl);
		}
		
		if (dataObj[i].time[0] == 0) {
			timeEl.innerHTML = dataObj[i].time;
		} else {
			timeEl.innerHTML = dataObj[i].time;
		}
		
		deleteEl.append(deletePic);
		dateEl.innerHTML = dataObj[i].date;
		avaBox.classList.add('ava_box_dialog_chat');
		ava.classList.add('ava_dialog_chat');
		userNameAndDateBox.classList.add('user_name_and_date_box_dialog_chat');
		userNameBox.classList.add('user_name_box_dialog_chat');
		dateBox.classList.add('date_box_dialog_chat');
		avaBox.appendChild(ava);
		textBox.classList.add('text_box_dialog_chat');
		userNameBox.appendChild(firstNameEl);
		userNameBox.appendChild(lastNameEl);
		dateBox.appendChild(dateEl);
		dateBox.appendChild(timeEl);
		userNameAndDateBox.appendChild(userNameBox);
		userNameAndDateBox.appendChild(dateBox);
		textBox.appendChild(textEl);
		item.appendChild(deleteEl);
		item.appendChild(avaBox);
		item.appendChild(userNameAndDateBox);
		item.appendChild(textBox);
		parentEl.appendChild(item);

		deleteEl.addEventListener('click', function() {
			let dialogItem = this.closest('.dialog_chat_item'),
				newItemCountEl = document.querySelector('.new_messages_count');
			fetchDeleteDialogAllMessages(dialogItem.getAttribute('data-profile_id'));

			if (newItemCountEl) {
				let count = newItemCountEl.innerHTML.slice(1),
					mainNewMessagesCountEl = document.querySelector('.messages_menu_user_item .new_subscriber_amount'),
					mainNewMessagesCountElValue = mainNewMessagesCountEl.innerHTML.slice(1),
					newMainNewMessagesCountElValue = Number(mainNewMessagesCountElValue) - count;

				if (mainNewMessagesCountElValue <= 0) {
					mainNewMessagesCountEl.remove();
				} else {
					mainNewMessagesCountEl.innerHTML = newMainNewMessagesCountElValue;
				}
			}
		});
	}

	addEventOnDialogOrChatList(parentEl, parentEl.children);
}

function addEventOnDialogOrChatList(parentEl, collection) {
	for (let i = 0; i < collection.length; i++) {
		collection[i].addEventListener('click', function(e) {
			if (this.hasAttribute('data-profile_id') && e.target !== document.querySelector('.delete_correspondence') &&
				e.target !== document.querySelector('.delete_correspondence_pic')) {
				let elOfUrl = location.href + '/d' + this.getAttribute('data-profile_id');
				history.pushState(null, null, elOfUrl);
				ajaxShowСorrespondence(parentEl, getLastPartOfURL());
			}
		});
	}
}

function showSelectedDialogueOrChat(dataObj, messageCount, timestamp, parentEl, addType) {
	let from, to, data,
		countNewMessage = 0,
		mainMenuMessagesItem = document.querySelector('.messages_menu_user_item'),
		parentElScrollHeight = parentEl.scrollHeight;

	if (dataObj[0].user_to === tab.userID ) {
		from = tab.userID;
		to = dataObj[0].user_from;
	} else {
		from = tab.userID;
		to = dataObj[0].user_to;
	}

	// if (dataObj.length < 30) {
	// 	parentEl.removeEventListener('scroll', loadMessages);
	// } else if (dataObj.length >= 20 && addType === 'first') {
		// parentEl.addEventListener('scroll', loadMessages);
	// }

	let messagesTitleEl = parentEl.previousElementSibling,
		writeMessageBlock = parentEl.nextElementSibling,
		backButton = messagesTitleEl.children[2];
	writeMessageBlock.classList.remove('not_active');
	parentEl.classList.add('messages_block_with_text_field');
	messagesTitleEl.children[0].classList.add('not_active');
	messagesTitleEl.children[1].classList.add('not_active');
	backButton.classList.remove('not_active');
	messagesTitleEl.children[3].classList.remove('not_active');
	
	if (dataObj[0].user_from === tab.userID) {
		messagesTitleEl.children[3].children[0].innerHTML = dataObj[0].user_to_fullname;
		messagesTitleEl.children[3].setAttribute('data-profile_id', dataObj[0].user_to);
		messagesTitleEl.children[3].children[0].setAttribute('href', '/profile/' + dataObj[0].user_to);
	} else {
		messagesTitleEl.children[3].children[0].innerHTML = dataObj[0].user_from_fullname;
		messagesTitleEl.children[3].setAttribute('data-profile_id', dataObj[0].user_from);
		messagesTitleEl.children[3].children[0].setAttribute('href', '/profile/' + dataObj[0].user_from);
	}
	
	if (addType === 'first') {
		parentEl.innerHTML = '';
	}

	for (let i = 0; i < dataObj.length; i++) {
		if (dataObj[i].status == 1) {
			countNewMessage += 1;
		}
		
		tab.tabSync._minorAddMessageOnDialogOrChatPage(dataObj[i].user_from, dataObj[i].user_from_ava, dataObj[i].user_from_fullname, dataObj[i].date.date, dataObj[i].date.time, dataObj[i].text, dataObj[i].file_list, dataObj[i].important, dataObj[i].message_id, dataObj[i].self_message_not_read, dataObj[i].rowNum, 'prepend');
	}

	if (parentEl.firstElementChild) {
		parentEl.firstElementChild.setAttribute('data-message_count', messageCount);
		parentEl.firstElementChild.setAttribute('data-timestamp', timestamp);
	}
	
	if (addType === 'first') {
		tab.tabSync._scrollDownTheBlockWithMessages();
	} else {
		parentEl.scrollTop = parentEl.scrollHeight - parentElScrollHeight;
	}

	data = {
		'ws_tab_sync_to_do': 'mark_messages_as_read',
		'actionDB': 'markIncomingMessagesFromUserAsRead',
		'fromUserID': from,
		'toUserID': to,
		'countNewMessage': countNewMessage
	};
	tab.send(tab.tabSync.collectData(data));
	loadMessages();
}

function loadMessages() {
	let dialogOrChatContent = document.querySelector('.messages_block'),
		elScrollHeight = dialogOrChatContent.scrollHeight;

	if (dialogOrChatContent.classList.contains('messages_block_with_text_field')) {
		let firstMessageEl = dialogOrChatContent.firstElementChild,
			messageCount = firstMessageEl.getAttribute('data-message_count'),
			rowNum = firstMessageEl.getAttribute('data-rowNum'),
			timestamp = firstMessageEl.getAttribute('data-timestamp');

		dialogOrChatContent.onscroll = function() {
			if (dialogOrChatContent.scrollTop == 0 && rowNum < messageCount) {
				tab.checkToken(function() {
					ajaxShowСorrespondence(dialogOrChatContent, getLastPartOfURL(), rowNum, timestamp);
				});
			}
		};
	}
}