/*jshint esversion: 6 */

function ajaxCheckPassword(passwordInputEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			currentPasswordBlock = document.querySelector('.current_password'),
			emailChangeEl = document.querySelector('.edit_email_elem'),
			passwordChangeEl = document.querySelector('.edit_password_elem'),
			noticeElemWrongCurrentPass = document.querySelector('div.notice_elem'),
			deleteProfileBlock = document.querySelector('.delete_profile_box'),
			noticeModalDeleteProfile = document.querySelector('.modal_notice_delete_profile'),
			deleteProfileAnchor = noticeModalDeleteProfile.querySelector('.notice_button_box').querySelector('a'),
			data = 'password=' + passwordInputEl.value + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				if (result.res == 1) {
					noticeElemWrongCurrentPass.classList.remove('notice_elem_active');
					emailChangeEl.classList.remove('not_active_element');
					passwordChangeEl.classList.remove('not_active_element');
					deleteProfileBlock.classList.remove('not_active_element');
					currentPasswordBlock.classList.add('not_active_element');
					passwordInputEl.value = '';
					passwordInputEl.setAttribute('data-tcsd', result.tcsd);
					deleteProfileAnchor.setAttribute('href', deleteProfileAnchor.getAttribute('href') + '/' + result.tcsd);
				} else if (result.res == 0) {
					noticeElemWrongCurrentPass.classList.add('notice_elem_active');
					noticeElemWrongCurrentPass.innerHTML = result.errors;
					console.log('bad');
				}
			}
		};
		
		xhr.open('POST', '/user/checkPassword');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function ajaxChangePassword(confirmNewPassButton, passwordInputEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			currentPasswordBlock = document.querySelector('.current_password'),
			emailChangeEl = document.querySelector('.edit_email_elem'),
			passwordChangeEl = document.querySelector('.edit_password_elem'),
			newPasswordInput = document.querySelector('input[name=new_password]'),
			repeatNewPasswordInput = document.querySelector('input[name=repeat_new_password]'),
			changePasswordTitleAndNotice = document.querySelector('.show_hide_edit_password'),
			data = 'password=' + newPasswordInput.value +
				'&repeat_password=' + repeatNewPasswordInput.value +
				'&tcsd=' + passwordInputEl.getAttribute('data-tcsd') +
				'&csrf_test_name=' + csrfVal.getAttribute('content');
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				if (result.res == 1) {
					changePasswordTitleAndNotice.innerHTML = result.message.result;
					changePasswordTitleAndNotice.classList.add('well_done_new_pass');
					hideBlockChangeData(confirmNewPassButton);
					disActivateIconWaiting(confirmNewPassButton, confirmNewPassButton.nextElementSibling);
					passwordInputEl.removeAttribute('data-tcsd');
					
					setTimeout(function() {
						changePasswordTitleAndNotice.classList.remove('well_done_new_pass');
						changePasswordTitleAndNotice.innerHTML = result.message.default;
						emailChangeEl.classList.add('not_active_element');
						passwordChangeEl.classList.add('not_active_element');
						currentPasswordBlock.classList.remove('not_active_element');
					}, 1000);
				} else if (result.res == 0){
					console.log(result.errors);
				}
			}
		};
		
		xhr.open('POST', '/user/changePassword');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function ajaxChekEmail(confirmNewEmailButton) {
	return new Promise(function(resolve, reject) {
		tab.xjscsrf(function() {
			var xhr = new XMLHttpRequest(),
				csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				emailInp = document.querySelector('input[name=new_email]'),
				emailP = document.querySelector('.new_email_elem>p'),
				data = 'email=' + emailInp.value + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');
			
			xhr.onreadystatechange = function() {
				if (xhr.readyState == 4 && xhr.status == 200) {
					var result = JSON.parse(xhr.responseText);
					csrfVal.setAttribute('content', result.csrf);
					tab.sendCSRFToLS();

					if (result.res == 0) {
						emailInp.classList.add('error_input');
						emailP.classList.add('notice_message');
						emailP.innerHTML = result.errors;
						disActivateIconWaiting(confirmNewEmailButton, confirmNewEmailButton.nextElementSibling);
					} else if (result.res == 1) {
						resolve(result);
					}
				}
			};
			
			xhr.open('POST', '/auth/emailExists');
			xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
			xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
			xhr.send(data);
		});
	});
}

function ajaxSendNewEmail(confirmNewEmailButton, passwordInputEl, callback) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			emailInp = document.querySelector('input[name=new_email]'),
			emailP = document.querySelector('.new_email_elem>p'),
			emailEl = document.querySelector('.edit_email_elem>p.show_hide_email'),
			emailElVal = emailEl.innerHTML;
			data = 'email_tmp=' + emailInp.value +
				'&tcsd=' + passwordInputEl.getAttribute('data-tcsd') + 
				'&csrf_test_name=' + csrfVal.getAttribute('content');
		
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result.csrf);
				tab.sendCSRFToLS();

				if (result.res == 1) {
					callback();
					emailEl.innerHTML = 'Проверьте почту';
					emailEl.classList.add('well_done_new_pass');
					hideBlockChangeData(confirmNewEmailButton);
					passwordInputEl.removeAttribute('data-tcsd');
					setTimeout(function() {
						emailEl.innerHTML = emailElVal;
						emailEl.classList.remove('well_done_new_pass');
					}, 1000);
				} else if (result.res == 0) {
					emailInp.classList.add('error_input');
					emailP.innerHTML = result.errors;
					emailP.classList.add('notice_message');
					disActivateIconWaiting(confirmNewEmailButton, confirmNewEmailButton.nextElementSibling);
				}
			}
		};
		
		xhr.open('POST', '/user/createTempEmail');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}