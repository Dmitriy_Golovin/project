/*jshint esversion: 6 */

let addButtonAboutSelf = document.querySelector('.add_button_about_self'),
	aboutSelfBlock = document.querySelector('.about_self'),
	fieldAddAboutSelf = document.querySelector('.field_add_about_self'),
	blockAddAboutSelf = document.querySelector('.block_add_about_self'),
	cancelButtonAboutSelf = document.querySelector('.cancel_button_about_self'),
	saveButtonAboutSelf = document.querySelector('.save_button_about_self');

addButtonAboutSelf.addEventListener('click', function() {
	addButtonAboutSelf.classList.add('not_active');
	aboutSelfBlock.classList.add('not_active');
	blockAddAboutSelf.classList.remove('not_active');
	fieldAddAboutSelf.value = editAboutSelf(aboutSelfBlock, fieldAddAboutSelf);
	fieldAddAboutSelf.focus();
	fieldAddAboutSelf.selectionStart = fieldAddAboutSelf.value.length;
});

cancelButtonAboutSelf.addEventListener('click', function() {
	addButtonAboutSelf.classList.remove('not_active');
	aboutSelfBlock.classList.remove('not_active');
	blockAddAboutSelf.classList.add('not_active');
});

saveButtonAboutSelf.addEventListener('click', function() {
	aboutSelfBlock.innerHTML = '';
	tab.checkToken(function() {
		ajaxProfileAddAboutSelf(fieldAddAboutSelf.value, aboutSelfBlock, addButtonAboutSelf);
	});
	fieldAddAboutSelf.value = '';
	addButtonAboutSelf.classList.remove('not_active');
	aboutSelfBlock.classList.remove('not_active');
	blockAddAboutSelf.classList.add('not_active');
});

function editAboutSelf(aboutSelfBlock, editBlock) {
	let result = '';
	if (aboutSelfBlock.children.length > 0) {
		for(let i = 0; i < aboutSelfBlock.children.length; i++) {
			result += aboutSelfBlock.children[i].innerHTML;
			if (i < aboutSelfBlock.children.length - 1) {
				result += '\n';
			}
		}
	}
	return result;
}