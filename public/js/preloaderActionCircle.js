function preloaderAction(action) {
	let preloader = document.querySelector('.preloader_circle'),
		preloaderContainer = document.querySelector('.preloader_circle_container');
	
	if (action == 'add') {
		preloader.classList.remove('not_active');
		showPreloaderAnimation();
	} else {
		preloader.classList.add('not_active');
		clearInterval(intetvalPreloader);
		count = 0;

		for (let item of preloaderContainer.children) {
			item.classList.remove('preloader_item_active');
		}
	}
}

function showPreloaderAnimation() {
	let itemList = document.querySelector('.preloader_circle_container').children;
		
	intetvalPreloader = setInterval( function() {
		
		for (var i = 0; i < itemList.length; i++) {
			itemList[i].classList.remove('preloader_item_active');
		};
		
		itemList[count].classList.add('preloader_item_active');
		count++;
		
		if (count == itemList.length) {
			count = 0;
		}
		
	}, 200);
}