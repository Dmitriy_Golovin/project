let interval;

function activateIconWaiting(original, copy) {
	original.classList.add('not_active');
	copy.classList.remove('not_active');
	changeBackgroundOfSpan(original);
}

function disActivateIconWaiting(original, copy) {
	original.classList.remove('not_active');
	copy.classList.add('not_active');
	clearInterval(interval);
}

function changeBackgroundOfSpan(original) {
	var spanList = original.parentElement.querySelectorAll('.waiting_icon_span'),
		count = 0;
	
	spanList[0].classList.add('waiting_icon_span_active');
	
	interval = setInterval( function() {
		
		for (var i = 0; i < spanList.length; i++) {
			spanList[i].classList.remove('waiting_icon_span_active');
		}
		
		spanList[count].classList.add('waiting_icon_span_active');
		count++;
		
		if (count == spanList.length) {
			count = 0;
		}
		
	}, 400);
}