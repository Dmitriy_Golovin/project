let errorsBlock = document.querySelector('.errors[role="validation_errors"]'),
	closeEl = document.querySelector('.close_errors_block');

if (closeEl) {
	closeEl.addEventListener('click', function() {
		if (errorsBlock) {
			errorsBlock.remove();
		}
	})
}