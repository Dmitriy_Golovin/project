function animationPushIcon(el) {
	var editIconList = document.querySelectorAll(el.icon),
		listP = document.querySelectorAll('.qqq');

	[].forEach.call(editIconList, function(node) {
		node.addEventListener('mousedown', function(e) {
			if (e.target !== node) return;
			node.classList.add('edit_icon_push');
		});
		
		node.addEventListener('mouseup', function(e) {
			let noticeElem = e.target.parentElement.parentElement.querySelector('.notice_elem_ava');

			if (e.target !== node) return;
			node.classList.remove('edit_icon_push');
			
			if (noticeElem) {
				for (var i = 0; i < listP.length; i++) {
					if (!listP[i].classList.contains('error_el')) {
						listP[i].parentElement.classList.remove('error_el');
						noticeElem.classList.remove('notice_elem_ava_active')
					}
				}
			}
		});
		
		node.addEventListener('mouseout', function(e) {
			if (e.target !== node) return;
			node.classList.remove('edit_icon_push');
		});
	});
}

function showBlockChangeData(eventEl) {
	let box = eventEl.parentElement.parentElement,
		attribute = eventEl.getAttribute('data-pass-saf-show'),
		actionElemList = box.querySelectorAll('.' + attribute),
		exceptionEl = box.querySelector('.current_password'),
		resCollection = getResultCollection(box, exceptionEl, actionElemList);

	for (let node of resCollection) {
		node.classList.add('not_active_element');
	}
	
	actionElemList[0].classList.remove('pass_saf_active_edit_box');
	actionElemList[1].classList.add('pass_saf_active_change_box');
	
	for (let node of actionElemList[0].children) {
		node.classList.add('hide_elem');
	}
	
	for (let node of actionElemList[1].children) {
		node.classList.remove('hide_elem');
	}
}

function hideBlockChangeData(eventEl) {
	let box = eventEl.parentElement.parentElement,
		attribute = eventEl.getAttribute('data-pass-saf-show'),
		actionElemList = box.querySelectorAll('.' + attribute),
		exceptionEl = box.querySelector('.current_password'),
		resCollection = getResultCollection(box, exceptionEl, actionElemList);

	for (let node of resCollection) {
		node.classList.remove('not_active_element');
	}
	
	for (let node of actionElemList[1].children) {
		node.classList.add('hide_elem');
		if (node.tagName == 'INPUT') node.value = '';
	}
	
	for (let node of actionElemList[0].children) {
		node.classList.remove('hide_elem');
	}

	actionElemList[0].classList.add('pass_saf_active_edit_box');
	actionElemList[1].classList.remove('pass_saf_active_change_box');
	
	removeNoticeAndClasserror(eventEl);
}

function getResultCollection(box, exceptionEl, actionElemList) {
	let result = [];
	
	for (let node of box.children) {
		if (node != exceptionEl && node != actionElemList[0] && node != actionElemList[1] && !node.classList.contains('pass_saf_change')) {
			result.push(node);
		}
	}
	
	return result;
}

function validateInputData(eventEl) {
	let box = eventEl.parentElement,
		inputList = box.querySelectorAll('input.input_edit'),
		titleList = box.querySelectorAll('p.title_change_pass_saf');
		
	Array.from(inputList);
	Array.from(titleList);

	for (let i = 0; i < inputList.length; i++) {
		if (inputList[i].name == 'new_password') {
			let str = /^[A-Za-z0-9_]{6,20}$/;
			
			if (!str.test(inputList[i].value)) {
				titleList[i].classList.add('notice_message');
				inputList[i].classList.add('error_input');
				titleList[i].innerHTML = 'некорректные символы';
				return false;
			}
		} else if (inputList[i].name == 'repeat_new_password' && i == 1) {
			if (inputList[0].value !== inputList[1].value) {
				titleList[i].classList.add('notice_message');
				inputList[1].classList.add('error_input');
				titleList[i].innerHTML = 'пароли не совпадают';
				return false;
			}
		} else if (inputList[i].name == 'new_email') {
			let str = /^[0-9a-zA-Z-.]+@[a-z]+\.[a-z]{2,3}$/;
			
			if (!str.test(inputList[i].value)) {
				titleList[i].classList.add('notice_message');
				inputList[i].classList.add('error_input');
				titleList[i].innerHTML = 'некорректные символы';
				return false;
			}
		}
	}
	
	return true;
}

function removeNoticeAndClasserror(eventEl) {
	let box = eventEl.parentElement,
		inputList = box.querySelectorAll('input.input_edit'),
		titleList = box.querySelectorAll('p.title_change_pass_saf');
		
	for (let node of inputList) {
		if (node.classList.contains('error_input')) node.classList.remove('error_input');
	}
	
	for (let node of titleList) {
		if (node.classList.contains('notice_message')) {
			node.classList.remove('notice_message');
			node.innerHTML = node.getAttribute('data-default-content');
		}
	}
}

function showErrorFieldPassAndSafety(field, str, addCond) {
	addCond = addCond || '';
	if (!str.test(field.innerHTML) && field.innerHTML && field.innerHTML !== addCond) {
		field.classList.add('error_input');
		return false;
	}

	return true;
}

function confirmInputKeystroke(el) {
	if (el.parentElement.tagName === 'DIV' && el.parentElement.classList.contains('edit_profile_inp')) {
		var value = el.value;
		el.parentElement.previousElementSibling.innerHTML = value;
		el.parentElement.nextElementSibling.setAttribute('src', '/img/edit_icon.png');
		el.parentElement.previousElementSibling.classList.remove('not_active');
		el.parentElement.classList.add('not_active');
	}
}