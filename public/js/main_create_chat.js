/*jshint esversion: 6 */

let uploadImageButton = document.querySelector('.load_file'),
	loadFileLabel = document.querySelector('.create_chat_load_file'),
	addMemberChatButtton = document.querySelector('.add_chat_member_but'),
	modalAddGuestUserBox = document.querySelector('.modal_add_guest_user_box'),
	userModal = modalAddGuestUserBox.closest('.modal'),
	userModalBox = modalAddGuestUserBox.closest('.modal_box'),
	invitedContentBlock = userModal.querySelector('.invited_content_block'),
	addedUserContainer = document.querySelector('.chat_member_content'),
	closeUserModalButton = userModal.querySelector('.close_win_modal'),
	chatSaveButton = document.querySelector('.chat_create_button_save'),
	form = document.querySelector('#create_chat_form'),
	userListObjData;

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('remove_available_avatar_chat')) {
		let avatarContainer = e.target.closest('.create_chat_image_container'),
			labelButton = avatarContainer.parentElement.querySelector('.create_chat_load_file'),
			removeAvatarInput = document.querySelector('input[name="remove_avatar"]');

		removeAvatarInput.value = 1;
		labelButton.classList.remove('not_active');
		avatarContainer.remove();
	}

	if (e.target.classList.contains('delete_chat_button')) {
		let noticeModal = document.querySelector('.modal_notice');

		if (!noticeModal) {
			return false;
		}

		let noticeModalBox = noticeModal.querySelector('.modal_box'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		setTimeout(function() {
			noticeModal.classList.add('modal_active');
			noticeModalBox.classList.add('modal_box_active');
		}, 10);

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.classList.remove('modal_active');
			}, 300);
		});
	}

	if (e.target.classList.contains('invited_search_button')) {
		fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
	}

	if (e.target.classList.contains('chat_invite_user_button')) {
		let container = e.target.closest('.invited_chat_content_box'),
			userContainer = e.target.closest('.user_result_item'),
			cancelInviteButton = userContainer.querySelector('.chat_cancel_user_button'),
			selectUserRoleContainer = userContainer.querySelector('.user_item_control_select_role_block'),
			userQuerySearch = e.target.closest('.user_query_search'),
			userCount = userQuerySearch.getAttribute('data-usercount'),
			timestamp = userQuerySearch.getAttribute('data-timestamp'),
			existUserCount = userQuerySearch.children.length,
			rowNum = userQuerySearch.children[userQuerySearch.children.length - 1].getAttribute('data-rowNum'),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title');

		for (let el of addedUserContainer.children) {
			if (el.hasAttribute('data-query_timestamp') && el.getAttribute('data-query_timestamp') == userQuerySearch.getAttribute('data-timestamp')) {
				existUserCount += 1;
			}
		}

		if ((userQuerySearch.parentElement.scrollHeight - userContainer.offsetHeight) <=
			userQuerySearch.parentElement.clientHeight &&
			existUserCount < userCount) {
			fetchGetUserList(userListObjData, invitedContentBlock, timestamp, rowNum);
		}

		userContainer.setAttribute('data-query_timestamp', userQuerySearch.getAttribute('data-timestamp'));
		addedUserContainer.append(userContainer);
		e.target.classList.add('not_active');
		cancelInviteButton.classList.remove('not_active');
		selectUserRoleContainer.classList.remove('not_active');
		noEventsTitle.classList.add('not_active');
	}

	if (e.target.classList.contains('chat_cancel_user_button')) {
		let userContainer = e.target.closest('.user_result_item'),
			noEventsTitle = addedUserContainer.querySelector('.no_events_title');

		userContainer.remove();

		let userElList = addedUserContainer.querySelectorAll('.user_result_item');

		if (userElList.length <= 0) {
			noEventsTitle.classList.remove('not_active');
		}
	}
});

addMemberChatButtton.addEventListener('click', function() {
	fetchGetUserList(getDataForInviteUsers(), invitedContentBlock);
	userModal.classList.add('modal_active');
	userModalBox.classList.add('modal_box_active');
});

closeUserModalButton.addEventListener('click', function() {
	userModalBox.classList.remove('modal_box_active');

	setTimeout(function() {
		userModal.classList.remove('modal_active');
	}, 300);
});

loadFileLabel.addEventListener('click', function() {
	let imageNoticeElem = this.parentElement.querySelector('.notice_elem');

	if (imageNoticeElem.classList.contains('notice_elem_active')) {
		imageNoticeElem.classList.remove('notice_elem_active');
		imageNoticeElem.innerHTML = '';
	}
});

uploadImageButton.addEventListener('change', function(e) {
	let checkResult = checkUploadFile(this, 1);

	if (checkResult.error) {
		let noticeElem = this.parentElement.querySelector('.notice_elem');
		noticeElem.innerHTML = checkResult['error'];
		noticeElem.classList.add('notice_elem_active');
		this.value = '';
	} else {
		showImage(this.parentElement, this);
	}
});

chatSaveButton.addEventListener('click', function(e) {
	prepareMemberData();
	appendInputCR(form);
	form.submit();
});

function showImage(parentEl, fileInput) {
	let file = fileInput.files[0],
		imageContainer = document.createElement('div'),
		fileControlBlock = document.createElement('div'),
		removeFileBlock = document.createElement('span'),
		removeFilePic = document.createElement('img'),
		imageBox = document.createElement('div'),
		image = document.createElement('img'),
		labelButton = parentEl.querySelector('.create_chat_load_file');

	imageContainer.classList.add('create_chat_image_container');
	fileControlBlock.classList.add('create_chat_file_control_block');
	removeFileBlock.classList.add('create_chat_remove_file_block');
	removeFilePic.classList.add('create_chat_remove_file_pic');
	imageBox.classList.add('create_chat_image_box');
	image.classList.add('create_chat_upload_image');
	removeFilePic.src = '/img/close_.png';
	image.src = URL.createObjectURL(file);

	removeFileBlock.append(removeFilePic);
	fileControlBlock.append(removeFileBlock);
	imageContainer.append(fileControlBlock);
	imageBox.append(image);
	imageContainer.append(imageBox);
	parentEl.append(imageContainer);
	labelButton.classList.add('not_active');

	removeFileBlock.onclick = function() {
		labelButton.classList.remove('not_active');
		imageContainer.remove();
	};
}

function getDataForInviteUsers() {
	let invitedFilterBlock = modalAddGuestUserBox.querySelector('.invited_filter_block'),
		searchField = modalAddGuestUserBox.querySelector('.invited_search_field'),
		onlyFriends = invitedFilterBlock.querySelector('.invited_only_friends').checked,
		invitedSelectGroup = invitedFilterBlock.querySelector('select[name="invited_select_group"]'),
		result = {};

	if (searchField.value) {
		result.searchString = searchField.value;
	}

	result.only_friends = Number(onlyFriends);
	// result.group_id = invitedSelectGroup.options[invitedSelectGroup.selectedIndex].getAttribute('data-group_id');
	result.type = 'invite-chat';
	// result.within_group = 0;

	// if (inviteContainer.querySelector('.within_group') && !inviteContainer.querySelector('.within_group').classList.contains('not_active')) {
	// 	result.within_group = inviteContainer.querySelector('.within_group').value;
	// }

	if (addedUserContainer && addedUserContainer.children.length > 0) {
		let exceptionUserIdList = [];

		for (let el of addedUserContainer.children) {
			if (!el.classList.contains('no_events_title')) {
				exceptionUserIdList.push(el.getAttribute('data-profile_id'));
			}
		}

		if (exceptionUserIdList.length > 0) {
			result.exceptionUserIdList = exceptionUserIdList;
		}
	}

	return result;
}

function prepareMemberData() {
	let userItemList = addedUserContainer.querySelectorAll('.user_result_item'),
		inputEl = document.querySelector('input[name="chat_member"]'),
		memberDataList = [];

	if (userItemList.length <= 0) {
		return;
	}

	for (let el of userItemList) {
		let memberData = {};
		memberData.memberId = el.getAttribute('data-profile_id');
		memberData.memberRole = el.querySelector('select[name="user_item_role_select"]').value;
		memberDataList.push(memberData);
	}

	inputEl.value = JSON.stringify(memberDataList);
}