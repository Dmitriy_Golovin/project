function sendNewPassword(data, formBox, reportBox) {
	
	var xhr = new XMLHttpRequest(),
		noticeElem = document.querySelector('.notice_elem');

	xhr.onreadystatechange = function() {
		if (xhr.readyState == 4 && xhr.status == 200) {
			var result = JSON.parse(xhr.responseText);
			if (result['res'] == 1) {
				noticeElem.innerHTML = 'произошла ошибка';
				noticeElem.classList.add('notice_elem_active');
				//email.classList.add('error_input');
				//email.classList.remove('valid_input');
			} else if (result['res'] == 0){
				formBox.classList.add('not_active');
				reportBox.classList.remove('not_active');
			}
		}
	};

	xhr.open('POST', '/ajax/setNewPassword');
	xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
	xhr.send(data);
}