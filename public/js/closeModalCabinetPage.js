function closeModal(parent) {
	if (parent.firstElementChild) {
		parent.firstElementChild.classList.remove('modal_box_active');
		parent.firstElementChild.firstElementChild.firstElementChild.classList.remove('modal_active');
	}
		
	setTimeout(function() {
		parent.innerHTML = '';
	}, 500);
}