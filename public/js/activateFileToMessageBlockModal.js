let intetvalPreloader,
	count = 0,
	fileMessageBlock = document.querySelector('.available_file_message_block'),
	countOfFilesBlock = document.querySelector('.count_of_files_block'),
	selectedFileList = [];
	selectedFileIdList = [];

var timeOutVideoMediaControl,
	activeTimeout = false;

function activateFileToMessageBlockModal() {
	let modal = document.querySelector('.modal'),
		modalBox = document.querySelector('.modal_box'),
		iconButtonList = document.querySelectorAll('.add_file_icon');

	for (let item of iconButtonList) {
		item.onclick = function() {
			setPausedForActiveMediaElem();
			modal.classList.add('modal_active');
			modalBox.classList.add('modal_box_active');
			let itemBlock = document.querySelector('.' + this.getAttribute('data-block_modal')),
				availableFileMessageBlock = document.querySelector('.' + this.getAttribute('data-message_selected_file')),
				fileType = Number(item.getAttribute('data-file_type'));

			for (let el of availableFileMessageBlock.children) {
				selectedFileIdList.push(el.getAttribute('data-file_id'));
			}

			preloaderAction('add');
			showModal(modal, modalBox, itemBlock, fileType);
		};
	}
}

function showModal(modal, modalBox, itemBlock, fileType) {
	let closePic = itemBlock.querySelector('.close_win_modal'),
		containerFile = itemBlock.querySelector('.file_container'),
		loadFileInput = itemBlock.querySelector('.load_file');

	closePic.onclick = function() {
		modalBox.classList.remove('modal_box_active');
		containerFile.innerHTML = '';

		setTimeout(function() {
			modal.classList.remove('modal_active');
			itemBlock.classList.add('not_active_opacity');
			itemBlock.classList.add('modal_block_not_active');
		}, 300);
	};

	ajaxGetUserPublicFiles(fileType, function(imageList) {
		for (let i = 0; i < imageList.length; i++) {
			for (let j = 0; j < selectedFileIdList.length; j++) {
				if (imageList[i]['file_id'] == selectedFileIdList[j]) {
					imageList[i]['selected'] = true;
				}
			}
		}

		selectedFileIdList.length = 0;
		selectedFileList.length = 0;

		containerFile.innerHTML = '';
		itemBlock.classList.remove('modal_block_not_active');
		showFileList(imageList, containerFile, fileType);

		itemBlock.ontransitionend = function() {
			preloaderAction('remove')
			if (!itemBlock.classList.contains('modal_block_not_active')) {
				itemBlock.classList.remove('not_active_opacity');
			}
		}

		loadFileInput.onchange = function() {
			let checkResult = checkUploadFile(this, fileType);

			if (checkResult['error']) {
				let noticeElem = itemBlock.querySelector('.notice_elem_modal_message');
				noticeElem.innerHTML = checkResult['error'];
				noticeElem.classList.add('notice_elem_modal_message_active');
				this.value = '';

				itemBlock.onclick = function() {
					if (noticeElem.classList.contains('notice_elem_modal_message_active')) {
						noticeElem.classList.remove('notice_elem_modal_message_active');
						noticeElem.innerHTML = '';
					}
				};
			} else {
				ajaxLoadFile(loadFileInput, fileType, function(data) {
					showFileList(data, containerFile, fileType);
				});
			}
		}
	})
}

function closeSubModalWin(itemBlock, containerFile, callback) {
	let modal = document.querySelector('.modal'),
		modalBox = document.querySelector('.modal_box');

	modalBox.classList.remove('modal_box_active');

	setTimeout(function() {
		modal.classList.remove('modal_active');
		itemBlock.classList.add('not_active_opacity');
		itemBlock.classList.add('modal_block_not_active');
		callback();
	}, 300);
}

function showFileList(imageList, parentEl, fileType) {
	if (imageList.length <= 0 && parentEl.children.length <= 0) {
		let noFileEl = document.createElement('h3');
		noFileEl.classList.add('no_file_el_h3');

		if (fileType === 1) {
			noFileEl.innerHTML = 'У вас нет доступных изображений';
		} else if (fileType === 2) {
			noFileEl.innerHTML = 'У вас нет доступных аудио файлов';
		} else if (fileType === 3) {
			noFileEl.innerHTML = 'У вас нет доступных видео файлов';
		} else if (fileType === 4) {
			noFileEl.innerHTML = 'У вас нет доступных файлов';
		}

		parentEl.append(noFileEl);
		return;
	} else {
		existsnoFileEl = document.querySelector('.no_file_el_h3');

		if (existsnoFileEl) {
			existsnoFileEl.remove();
		}
	}

	for (let i = 0; i < imageList.length; i++) {
		let notLast = true;


		if (Number(fileType) === 1) {
			new ImageFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
				}
			);
		} else if (Number(fileType) === 2) {
			new AudioFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
					'name': imageList[i]['name'],
				}
			);
		} else if (Number(fileType) === 3) {
			new VideoFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
					'name': imageList[i]['name'],
				}
			);
		} else if (Number(fileType) === 4) {
			new DocumentFileElement(
				{
					'parentEl': parentEl,
					'type': 'prepareForMessage',
				},
				{
					'fileId': imageList[i]['file_id'],
					'url': imageList[i]['url'],
					'privacyStatus': imageList[i]['privacy_status'],
					'selected': imageList[i]['selected'],
					'name': imageList[i]['name'],
					'date': imageList[i]['date'],
				}
			);
		}
	}

	selectFile(parentEl, fileType, false);
}