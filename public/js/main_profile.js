/*jshint esversion: 6 */

let messageButton = document.querySelector('.message_button'),
	emojiContainer = document.querySelector('.emoji_block'),
	writeMessageField = document.querySelector('.write_message_block>textarea'),
	sendMessageButton = document.querySelector('.send_message_to_user_button'),
	inviteButton = document.querySelector('.invite_button'),
	cursorPositionInMessageField = 0,
	scrollInviteEntityList = true;

window.addEventListener('load', function() {
	addEmojiToMessageFieldModal(emojiContainer, writeMessageField, sendMessageButton);
});

window.addEventListener('resize', function() {
	if (document.body.offsetWidth > 650 && document.querySelector('.profile_menu_container') &&
		document.querySelector('.profile_menu_container').classList.contains('menu_container_active')) {
		document.querySelector('.profile_menu_container').classList.remove('menu_container_active');

		if (!document.querySelector('.close_menu_container').classList.contains('not_active')) {
			document.querySelector('.close_menu_container').classList.add('not_active');
		}
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('menu_icon_container') || e.target.closest('.menu_icon_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.add('menu_container_active');
		closeMenuEl.classList.remove('not_active');
	}

	if (e.target.classList.contains('close_menu_container') || e.target.closest('.close_menu_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.remove('menu_container_active');

		setTimeout(function() {
			closeMenuEl.classList.add('not_active');
		}, 200);
	}

	if (!e.target.closest('.emoji_block') && e.target !== writeMessageField) {
		cursorPositionInMessageField = 0;
	}

	if (e.target.closest('.emoji_block') !== emojiContainer) {
		emojiContainer.querySelector('.emoji_box').classList.remove('menu_box_active');
		emojiContainer.querySelector('.emoji_box').parentElement.classList.remove('menu_box_active');
		writeMessageField.classList.remove('message_field_modal_emoji_active');
	}

	if (e.target.classList.contains('friend_button')) {
		let noticeModal = document.body.querySelector('.modal_notice'),
			dataObj = {
				'profileId': e.target.dataset.profile_id,
				'type': e.target.dataset.ws_tab_sync_to_do,
			};

		if (noticeModal) {
			noticeModal.querySelector('.modal_box').classList.remove('modal_box_active');
							
			setTimeout(function() {
				noticeModal.remove();
			}, 300);
		}


		fetchSendFriendRequest(dataObj, function(response) {
			tab.tabSync['_' + response.action]({
				'htmlData': response.htmlData,
				'userFromId': response.userFromId,
				'userToId': response.userToId,
				'newUserAction': response.newUserAction,
				'newSubscriberAction': response.newSubscriberAction,
			});

			tab.sendToLS({
				'ws_tab_sync_to_do': response.action,
				'htmlData': response.htmlData,
				'userFromId': response.userFromId,
				'userToId': response.userToId,
				'newUserAction': response.newUserAction,
				'newSubscriberAction': response.newSubscriberAction,
			});
		});
	}
});

messageButton.addEventListener('click', function() {
	var from = tab.userID,
		to = this.getAttribute('data-profile_id'),
		toFullName = messageButton.parentElement.previousElementSibling.children[0].innerHTML;
			
	activateWriteNewMessageBox(from, to, toFullName);
});

writeMessageField.addEventListener('keyup', function() {
	cursorPositionInMessageField = this.selectionStart;
});

writeMessageField.addEventListener('click', function() {
	cursorPositionInMessageField = this.selectionStart;
});

inviteButton.addEventListener('click', function() {
	let profileId = this.dataset.profile_id;
	activateInviteModal(profileId);
});