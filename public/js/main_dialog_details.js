/*jshint esversion: 6 */

let messageField = document.querySelector('.text_message'),
	emojiBox = document.querySelector('.emoji_relative_box'),
	emojiContainer = emojiBox.querySelector('ul'),
	emojiButton = document.querySelector('.emoji_icon'),
	sendMessageButton = document.querySelector('.write_message_send_message_button'),
	messagesBlock = document.querySelector('.messages_block'),
	availableFileMessageBlock = document.querySelector('.available_file_message_block'),
	withUserId = document.querySelector('.correspondence_fullname_or_chat_room').getAttribute('data-profile_id'),
	cursorPositionInMessageField = {
		'element': '',
		'caret': 0
	},
	modalSliderImage = new ModalSliderImage({
		'modalCssClass': 'modal_slider_image',
		'availableImageListContainer': messagesBlock,
	}),
	scrollmessageList = true,
	fullScreen = false,
	readyMarkMessage = true,
	markMessageIdList = [];

new ResizeObserver(scrollDownMessageBlock).observe(emojiBox);
new ResizeObserver(scrollDownMessageBlock).observe(availableFileMessageBlock);

window.addEventListener('load', function() {
	activateFileToMessageBlockModal();
	fetchGetDialogDetails(messagesBlock);
});

messagesBlock.addEventListener('scroll', function(e) {
	if (this.offsetHeight >= this.scrollHeight) {
		return false;
	}

	let newMessageContainerList = this.querySelectorAll('.dialog_chat_item_not_read_message'),
		parentRect = this.getBoundingClientRect();

	if (newMessageContainerList) {

		for (let el of newMessageContainerList) {
			let elRect = el.getBoundingClientRect();
			if (((elRect.top + 5 >= parentRect.top && elRect.bottom - 5 <= this.offsetHeight + parentRect.top) ||
				(el.offsetHeight > this.offsetHeight && elRect.bottom - 30 <= this.offsetHeight + parentRect.top)) &&
				!existItemInArr(markMessageIdList, el.getAttribute('data-message_id'))) {
				markMessageIdList.push(el.getAttribute('data-message_id'));
			}
		}

		if (markMessageIdList.length > 0 && readyMarkMessage) {
			readyMarkMessage = false;
			fetchMarkDialogMessageAsRead(withUserId);
		}
	}

	if (this.scrollTop == 0) {
		let messageQuery = this.querySelector('.message_query_search');

		if (!messageQuery.firstElementChild.hasAttribute('data-rowNum')) {
			return false;
		}

		let timestamp = messageQuery.getAttribute('data-timestamp'),
			messageCount = messageQuery.getAttribute('data-messageCount'),
			rowNum = messageQuery.firstElementChild.getAttribute('data-rowNum');

		if (scrollmessageList && Number(messageCount) > Number(rowNum)) {
			scrollmessageList = false;
			fetchGetDialogDetails(this, rowNum, timestamp);
		}
	}
});

messageField.addEventListener('paste', function(e) {
    let paste = (event.clipboardData || window.clipboardData).getData('text'),
    	node = document.createTextNode(paste);

    const sel = window.getSelection();
    if (!sel.rangeCount) return false;
    sel.deleteFromDocument();
    let range = sel.getRangeAt(0);
    range.insertNode(node);
    range.setStart(node, node.length);
    range.collapse(true);

    e.preventDefault();
});

messageField.addEventListener('keydown', function(e) {
	if (e.ctrlKey && e.keyCode === 13) {
		if ((cursorPositionInMessageField.element && cursorPositionInMessageField.caret > 0) || cursorPositionInMessageField.element.textContent) {
			let node = document.createElement('div'),
				br = document.createElement('br'),
				textNode = document.createTextNode(''),
				range = document.createRange(),
				sel = window.getSelection(),
				afterElement = (cursorPositionInMessageField.element.parentNode == messageField) ? cursorPositionInMessageField.element : cursorPositionInMessageField.element.parentNode;

			if (cursorPositionInMessageField.caret < cursorPositionInMessageField.element.length || (cursorPositionInMessageField.element.nextSibling && cursorPositionInMessageField.element.nextSibling.textContent)) {
				let nodeList = Array.from(cursorPositionInMessageField.element.parentNode.childNodes);

				for (let i = nodeList.length - 1; i >= 0; i--) {
					if (nodeList[i] != cursorPositionInMessageField.element) {
						range.selectNode(nodeList[i]);
						node.prepend(range.extractContents());
					}

					if (nodeList[i] == cursorPositionInMessageField.element) {
						range.setStart(nodeList[i], cursorPositionInMessageField.caret);
						range.setEnd(nodeList[i], nodeList[i].length);
						node.prepend(range.extractContents());

						// if (cursorPositionInMessageField.caret <= 0) {
						// 	node.prepend(br);
						// }

						break;
					}
				}
			} else {
				node.append(br);
			}

			node.append(textNode);
			range.setStartAfter(afterElement, 0);
			range.insertNode(node);
			range.setStart(node, 0);
			cursorPositionInMessageField.element = textNode;
			cursorPositionInMessageField.caret = 0;
			range.collapse(true);
			sel.removeAllRanges();
			sel.addRange(range);
		} else {
			let node = document.createElement('div'),
				br = document.createElement('br'),
				textNode = document.createTextNode(''),
				range = document.createRange(),
				sel = window.getSelection();

			node.append(br);
			node.append(textNode);
			this.append(node);
			range.setStart(textNode, 0);
			range.collapse(true);
			sel.removeAllRanges();
			sel.addRange(range);
		}
	}
});

sendMessageButton.addEventListener('click', function(e) {
	let messageData = getMessageTextAndFiles(),
		urlArr = document.location.pathname.split('/'),
		data;

	if (!messageData) {
		return;
	}
	
	if (!Number.isInteger(Number(urlArr[urlArr.length - 1]))) {
		return;
	}

	tab.checkToken(function() {
		tab.xjscsrf(function() {
			data = 'user_to=' + withUserId + '&text=' + messageData.text;
			data += '&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
			data += '&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60);

			if (messageData.fileList) {
				for (let i = 0; i < messageData.fileList.length; i++) {
					data += '&fileList[]=' + messageData.fileList[i];
				}
			}

			fetch('/message/sendDialogMessage', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
			.then(response => response.json())
			.then(response => {
				document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
				tab.sendCSRFToLS();
				return response;
			})
				.then(response => {
					if (response.res) {
						messagesBlock.querySelector('.message_query_search').insertAdjacentHTML('beforeend', response.html);

						let newMediaElList = messagesBlock.querySelector('.message_query_search').querySelectorAll('div[media_init="0"]');

		  				for (let mediaEl of newMediaElList) {
		  					if (Number(mediaEl.getAttribute('data-file_type')) === 2) {
		  						new AudioFileExistElement(mediaEl);
		  					}

		  					if (Number(mediaEl.getAttribute('data-file_type')) === 3) {
		  						new VideoFileExistElement(mediaEl);
		  					}
		  				}

						scrollDownMessageBlock();
						messageField.innerHTML = '';
						FileElement.clearContainerForFilesOfMessage();
						let dataForLS = {
							'ws_tab_sync_to_do': 'update_dialog_message_list_item',
							'dialogId': response.dialogId,
							'user_to': response.user_to,
							'user_from': response.user_from,
						};
						tab.sendToLS(dataForLS);
					} else {
						console.log(response.errors);
					}
				})
				.catch((error) => {
					console.log(error);
			});
		});
	});
});

emojiButton.addEventListener('click', function(e) {
	emojiBox.classList.toggle('emoji_relative_box_active');
});

messageField.addEventListener('click', function() {
	cursorPositionInMessageField = getCaretPosition(this);
});

messageField.addEventListener('keyup', function() {
	cursorPositionInMessageField = getCaretPosition(this);
});

document.body.addEventListener('click', function(e) {
	if (!e.target.closest('.emoji_relative_box') && e.target !== emojiButton && emojiBox.classList.contains('emoji_relative_box_active')) {
		emojiBox.classList.remove('emoji_relative_box_active');
	}

	if (!e.target.closest('.emoji_relative_box') && e.target !== messageField && e.target !== emojiButton && !e.target.closest('.text_message')) {
		cursorPositionInMessageField.element = '';
		cursorPositionInMessageField.caret = 0;
	}

	if (e.target.classList.contains('image_file_in_message')) {
		modalSliderImage.show(e.target);
	}

	if (e.target.classList.contains('menu_message_container')) {
		modalSliderImage.show(e.target);
	}

	if (e.target.classList.contains('menu_message_important_item')) {
		let messageContainer = e.target.closest('.message_item_box'),
			menuContainer = e.target.closest('.menu_message_container'),
			important = messageContainer.getAttribute('data-important_message'),
			messageId = messageContainer.getAttribute('data-message_id');

		fetchToggleImportantMessage(important, messageId, function(response) {
			if (response.res) {
				if (Number(response.important) === 1) {
					let imoprtantEl = TabSync._createImportantElement();
					menuContainer.insertAdjacentElement('beforebegin', imoprtantEl);
					e.target.innerHTML = response.buttonText;
					messageContainer.setAttribute('data-important_message', response.important);
				} else if (Number(response.important) === 0) {
					let existImportantEl = messageContainer.querySelector('.important_message_container');

					if (existImportantEl) {
						existImportantEl.remove();
					}

					e.target.innerHTML = response.buttonText;
					messageContainer.setAttribute('data-important_message', response.important);
				}
			} else {
				console.log(response.errors);
			}
		});
	}

	if (e.target.classList.contains('menu_message_delete_item')) {
		let messageContainer = e.target.closest('.message_item_box'),
			messageId = messageContainer.getAttribute('data-message_id');

		fetchDeleteDialogMessage(messageId);
	}
});

document.body.addEventListener('mouseover', function(e) {
	if (e.target.closest('.menu_message_container')) {
		let menuContainer = e.target.closest('.menu_message_container');
		menuContainer.querySelector('.menu_message_list_container').classList.add('menu_box_active');
	}
});

document.body.addEventListener('mouseout', function(e) {
	if (e.target.closest('.menu_message_container')) {
		let menuContainer = e.target.closest('.menu_message_container');
		menuContainer.querySelector('.menu_message_list_container').classList.remove('menu_box_active');
	}
});

for (let el of emojiContainer.children) {
	el.addEventListener('click', addEmojiToMessageField.bind(null, el));
}

function scrollDownMessageBlock() {
	if (!fullScreen) {
		messagesBlock.scrollTo(0, messagesBlock.scrollHeight);
	}
}

function addEmojiToMessageField(el) {
	let emoji = el.innerHTML,
		range = document.createRange(),
		sel = window.getSelection(),
		node = document.createTextNode(emoji);

	if (cursorPositionInMessageField.element) {
		range.setStart(cursorPositionInMessageField.element, cursorPositionInMessageField.caret);
		range.insertNode(node);
		
		range.setStart(node, node.length);
		range.collapse(true);
		sel.removeAllRanges();
		sel.addRange(range);
		messageField.focus();
		cursorPositionInMessageField.element = node;
		cursorPositionInMessageField.caret = node.length;
	} else {
		messageField.append(node);
		range.setStart(node, node.length);
		range.collapse(true);
		sel.removeAllRanges();
		sel.addRange(range);
		messageField.focus();
		cursorPositionInMessageField.element = node;
		cursorPositionInMessageField.caret = node.length;
	}
}

function getCaretPosition() {
	let sel, range;
	
	if (window.getSelection) {
    	sel = window.getSelection();
  		range = sel.getRangeAt(0);
  		let element = range.commonAncestorContainer,
  			caret = range.endOffset;

  		cursorPositionInMessageField.element = element;
  		cursorPositionInMessageField.caret = caret;
  	}

	return cursorPositionInMessageField;
}

function fetchGetDialogDetails(parentEl, rowNum, timestamp) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let xhr = new XMLHttpRequest(),
				csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'with_user_id=' + withUserId + '&timeOffsetSeconds=' + timeOffsetSeconds + '&csrf_test_name=' + csrfVal.getAttribute('content');

			if (!timestamp) {
				timestamp = parseInt(Date.now() / 1000);
			}

			if (rowNum) {
				data += '&rowNum=' + rowNum;
			}

			data += '&timestamp=' + timestamp;

			fetch('/message/getDialogDetails', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
				.then(response => response.json())
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
	  			.then(response => {
	  				if (response.insert_type === 'renew') {
	  					parentEl.innerHTML = '';
	  					parentEl.insertAdjacentHTML('afterbegin', response.html);

	  					let firstNotReadMessage = parentEl.querySelector('.first_not_read_message');

	  					if (firstNotReadMessage) {
	  						firstNotReadMessage.scrollIntoView();
	  						firstNotReadMessage.classList.remove('first_not_read_message');
	  					} else {
	  						scrollDownMessageBlock();
	  					}
	  				}

	  				if (response.insert_type === 'add') {
	  					let messageBlockScrollHeight = parentEl.scrollHeight;
	  					parentEl.querySelector('.message_query_search').insertAdjacentHTML('afterbegin', response.html);
	  					parentEl.scrollTo(0, parentEl.scrollHeight - messageBlockScrollHeight);
	  				}

	  				let newMediaElList = parentEl.querySelector('.message_query_search').querySelectorAll('div[media_init="0"]');

	  				if (parentEl.scrollHeight <= parentEl.offsetHeight) {
	  					let newMessageContainerList = parentEl.querySelectorAll('.dialog_chat_item_not_read_message');

						if (newMessageContainerList) {
							for (let el of newMessageContainerList) {
								markMessageIdList.push(el.getAttribute('data-message_id'));
							}

							if (markMessageIdList.length > 0 && readyMarkMessage) {
								readyMarkMessage = false;
								fetchMarkDialogMessageAsRead(withUserId);
							}
						}
	  				}

	  				for (let mediaEl of newMediaElList) {
	  					if (Number(mediaEl.getAttribute('data-file_type')) === 2) {
	  						new AudioFileExistElement(mediaEl);
	  					}

	  					if (Number(mediaEl.getAttribute('data-file_type')) === 3) {
	  						new VideoFileExistElement(mediaEl);
	  					}
	  				}

	  				scrollmessageList = true;
	  			})
	  			.catch((error) => {
					console.log(error);
				});
		});
	});
}

function getMessageTextAndFiles() {
	let message = messageField.innerHTML,
		fileList = getFileIdList();

	if (!message && fileList.length <= 0) {
		return false;
	}

	return {
		'text': message,
		'fileList': fileList,
	};
}

function existItemInArr(arr, item) {
	for (let i = 0; i < arr.length; i++) {
		if (arr[i] === item) {
			return true;
		}
	}

	return false;
}