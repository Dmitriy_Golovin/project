/*jshint esversion: 6 */

let dataContainer = document.querySelector('.event_data_container'),
	id = dataContainer.getAttribute('data-entity_id'),
	typeLabels = {
		1: 'event',
		2: 'meeting',
		3: 'task',
	},
	type = typeLabels[dataContainer.getAttribute('data-type')],
	detailsType = (dataContainer.hasAttribute('data-details_type')) ?
		dataContainer.getAttribute('data-details_type') :
		'',
	timeOutVideoMediaControl,
   	activeTimeout = false,
   	fullScreen = false,
   	modalSliderImage;

window.addEventListener('load', function() {
	fetchGetCommonEventDataDetails();
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('entity_delete_button')) {
		let noticeModal = document.querySelector('.modal_notice'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			acceptButton = noticeModal.querySelector('.notice_button_accept'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		noticeModal.classList.add('modal_active');
		noticeModalBox.classList.add('modal_box_active');

		acceptButton.addEventListener('click', function() {
			fetchDeleteCommonEvent(type, id, function(data) {
				window.location.href = data.backLink;
			});
		});

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.classList.remove('modal_active');
			}, 300);
		});
	}

	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}

	if (e.target.closest('.video_file_preview_container')) {
		let videoContainer = e.target.closest('.video_file_preview_container');
		fetchGetVideoViewModal({'page': 0}, videoContainer);
	}

	if (e.target.closest('.image_file_container_preview')) {
		let imageContainer = e.target.closest('.image_file_container_preview'),
			taskHistoryContainer = imageContainer.closest('.task_history_item_container') ||
				imageContainer.closest('.task_history_item_photo_container'),
			taskHistoryId = taskHistoryContainer.dataset.task_history_id,
			fieldDeleteFileIdList = document.querySelector('input[name="deleteFileIdList"]'),
			dataObj = {
				'type': TYPE_SHOW,
				'entity_id': taskHistoryId,
				'current_file_id': imageContainer.dataset.file_id,
				'entityAction': 'taskHistorySlider',
			};

		fetchGetImageSliderViewModal(dataObj);
	}

	if (e.target.classList.contains('dot_menu_container_pic')) {
		let menuList = e.target.parentElement.querySelector('.file_menu_list');
		menuList.classList.add('menu_box_active');
	}

	if (!e.target.classList.contains('dot_menu_container_pic')) {
		let sliderImage = document.querySelector('.modal_image_slider'),
			modalVideo = document.querySelector('.modal_video');

		if (sliderImage) {
			let activeMenuList = sliderImage.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}

		if (modalVideo) {
			let activeMenuList = modalVideo.querySelector('.menu_box_active');
		
			if (activeMenuList) {
				activeMenuList.classList.remove('menu_box_active');
			}
		}
	}
});