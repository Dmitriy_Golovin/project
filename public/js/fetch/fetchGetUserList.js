/*jshint esversion: 6 */

function fetchGetUserList(dataObj, invitedContentBlock, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = '&csrf_test_name=' + csrfVal.getAttribute('content');

			if (dataObj.searchString) {
				data += '&searchString=' + dataObj.searchString;
			}

			if (dataObj.only_friends) {
				data += '&only_friends=' + dataObj.only_friends;
			}

			if (dataObj.within_group) {
				data += '&group_id=' + dataObj.group_id;
			}

			if (dataObj.group_id) {
				data += '&within_group=' + dataObj.within_group;
			}

			if (!timestamp) {
				timestamp = parseInt(Date.now() / 1000);
			}

			if (rowNum) {
				data += '&' + 'rowNum=' + Number(rowNum);
			}

			if (dataObj.type) {
				data += '&' + 'type=' + dataObj.type;
			}

			if (dataObj.exceptionUserIdList) {
				data += '&' + 'exceptionUserIdList=' + JSON.stringify(dataObj.exceptionUserIdList);
			}

			if (dataObj.exceptionEntityId) {
				data += '&exceptionEntityId=' + dataObj.exceptionEntityId;
			}

			if (dataObj.exceptionEntity) {
				data += '&exceptionEntity=' + dataObj.exceptionEntity;
			}

			data += '&' + 'timestamp=' + timestamp;

			fetch('/user/list', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
				.then(response => response.json())
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
	  			.then(response => insertUserList(response, invitedContentBlock, dataObj))
	  			.catch((error) => {
					console.log(error);
				});
		});
	});
}

function insertUserList(data, invitedContentBlock, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			invitedContentBlock.innerHTML = '';
			invitedContentBlock.insertAdjacentHTML('beforeend', data.html);
			invitedContentBlock.scrollTop = 0;
			userListObjData = dataObj;
			loadUsersByScroll(invitedContentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			invitedContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollUserList = true;
	} else {
		console.log(data.errors);
	}
}

function loadUsersByScroll(invitedContentBlock, dataObj) {
	let container = invitedContentBlock.querySelector('.user_query_search');

	container.parentElement.onscroll = function(e) {
		setTimeout(function() {
			let containerScroll = container.parentElement.scrollTop,
				addedUserContainer = invitedContentBlock.parentElement.parentElement.querySelector('.invited_added_users_container'),
				userCount = container.getAttribute('data-usercount'),
				timestamp = container.getAttribute('data-timestamp'),
				existUserCount = container.children.length;
				rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

			if (addedUserContainer) {
				for (let el of addedUserContainer.children) {
					if (el.hasAttribute('data-query_timestamp') && container.getAttribute('data-timestamp') == el.getAttribute('data-query_timestamp')) {
						existUserCount += 1;
					}
				}
			}

			if (scrollUserList && containerScroll + container.parentElement.offsetHeight >= container.parentElement.scrollHeight - 10 && userCount > existUserCount) {
				scrollUserList = false;
				fetchGetUserList(dataObj, invitedContentBlock, timestamp, rowNum);
			}
		}, 50);
	};
}