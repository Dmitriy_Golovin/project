/*jshint esversion: 6 */

function fetchGetCommonEventDataDetails() {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&entity_id=' + dataContainer.getAttribute('data-entity_id') +
					'&type=' + dataContainer.getAttribute('data-type') +
					'&timeOffsetSeconds=' + timeOffsetSeconds,
				action = (Number(dataContainer.getAttribute('data-type')) === 3) ?
					'/task/getTaskDetailsData' :
					'/commonEventList/getCommonEventDetailsData';

				if (detailsType) {
					data += '&detailsType=' + detailsType;
				}

				fetch(action, {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					dataContainer.insertAdjacentHTML('afterBegin', response.html);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}