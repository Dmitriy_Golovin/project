/*jshint esversion: 6 */

const TYPE_FILE_PAGE = 1;
const TYPE_MESSAGE_PAGE = 2;

function fetchGetSelfFileList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&timeOffsetSeconds=' + timeOffsetSeconds;


				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/file/getFileList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertFileList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertFileList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			dataObj.fileContentBlock.innerHTML = '';
			dataObj.fileContentBlock.insertAdjacentHTML('beforeend', data.html);
			dataObj.fileContentBlock.scrollTop = 0;

			if (dataObj.pageType === TYPE_FILE_PAGE) {
				loadFilesByScrollSelfPage(dataObj.fileContentBlock, dataObj);
			}
		}

		if (data.insert_type === 'add') {
			dataObj.fileContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		setTimeout(function() {
			scrollFileList = true;
		}, 200);
	} else {
		console.log(data.errors);
	}
}

function loadFilesByScrollSelfPage(fileContentBlock, dataObj) {
	let container = fileContentBlock.querySelector('.file_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			fileCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existFileCount = container.children.length,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		if (scrollFileList &&
			containerRect.bottom <= window.screen.height - 72 &&
			fileCount > existFileCount) {
			scrollFileList = false;
			fetchGetSelfFileList(dataObj, timestamp, rowNum);
		}
	};
}