/*jshint esversion: 6 */

function fetchMarkDialogMessageAsRead(withUserId) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			sendIdList();
		});
	});
}

function sendIdList() {
	let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
		data = 'user_from=' + withUserId + '&idList=' + JSON.stringify(markMessageIdList) + '&csrf_test_name=' + csrfVal.getAttribute('content');

	fetch('/message/markDialogMessageAsRead', {
		method: 'POST',
		body: data,
		headers: {
			'Content-Type': 'application/x-www-form-urlencoded',
			'X-Requested-With': 'XMLHttpRequest',
		}
	})
		.then(response => response.json())
		.then(response => {
			csrfVal.setAttribute('content', response.csrf);
			tab.sendCSRFToLS();
			return response;
		})
		.then(response => {
			if (!response.res) {
				console.log(response.errors);
				return;
			}

			let countNewMessage = 0;

			for (let i = 0; i < response.messageDataList.length; i++) {
				let messageContainer = document.querySelector('.message_item_box[data-message_id="' + response.messageDataList[i].id + '"]'),
					mainMenuMessagesItem = document.querySelector('.messages_menu_user_item');

				messageContainer.classList.remove('dialog_chat_item_not_read_message');
				tab.tabSync._minorMinusOneAmountNewSubscriber(mainMenuMessagesItem);

				let dataUpdateCountForLS = {
					'ws_tab_sync_to_do': 'update_new_message_count_minus_count',
					'userId': response.userId, 
					'count': 1,
				};
				tab.sendToLS(dataUpdateCountForLS);
				countNewMessage++;

				for (let y = 0; y < markMessageIdList.length; y++) {
					if (Number(response.messageDataList[i].id) === Number(markMessageIdList[y])) {
						markMessageIdList.splice(y, 1);
					}
				}
			}

			if (countNewMessage > 0) {
				let dataUpdateChatItemForLS = {
					'ws_tab_sync_to_do': 'update_correspondence_item_new_message_count',
					'type': 'dialog',
					'dataElId': response.dialogId,
					'countNewMessage': countNewMessage,
				};
				tab.sendToLS(dataUpdateChatItemForLS);
			}

			if (markMessageIdList.length > 0) {
				sendIdList();
			} else {
				readyMarkMessage = true;
			}
		})
		.catch((error) => {
			console.log(error);
		});
}