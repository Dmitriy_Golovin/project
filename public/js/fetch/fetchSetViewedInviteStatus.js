/*jshint esversion: 6 */

function fetchSetViewedInviteStatus(id, type, callback) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&id=' + id;

				fetch('/' + type + '/setViewedInviteStatus', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					tab.tabSync._setIncominInviteAsView({
		  						'id': response.data.id,
		  						'type': response.data.type,
		  						'userId': response.data.userId
		  					});

		  					tab.sendToLS({
		  						'ws_tab_sync_to_do': 'setIncominInviteAsView',
		  						'id': response.data.id,
		  						'type': response.data.type,
		  						'userId': response.data.userId,
		  						'className': response.data.className,
		  						'guestStatusValue': response.data.guestStatusValue
		  					});

		  					callback();
		  				}

		  				if (!response.res) {
		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}