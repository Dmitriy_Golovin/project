/*jshint esversion: 6 */

function fetchLeaveChat(chatId, accept) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let acceptVal = (accept) ? accept : 0,
				csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = 'chat_id=' + chatId + '&accept=' + acceptVal + '&csrf_test_name=' + csrfVal.getAttribute('content');

			fetch('/message/leaveChat', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
				.then(response => response.json())
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
				.then(response => responseProcessing(response, chatId, 'fetchLeaveChat'))
				.catch((error) => {
					console.log(error);
				});
		});
	});
}

function responseProcessing(data, chatId, acceptFunc) {
	if (data.res === true) {
		let chatItem = document.querySelector('.dialog_chat_item[data-chat_id="' + chatId + '"]'),
			noticeModal = document.querySelector('.modal_notice');

		chatItem.remove();

		if (noticeModal) {
			let noticeModalBox = noticeModal.querySelector('.modal_box');
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.remove();
			}, 300);
		}
	}

	if (data.res === 'notice') {
		document.body.insertAdjacentHTML('beforeend', data.notice_html);
		let noticeModal = document.querySelector('.modal_notice'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			acceptButton = noticeModal.querySelector('.notice_button_accept'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		setTimeout(function() {
			noticeModal.classList.add('modal_active');
			noticeModalBox.classList.add('modal_box_active');
		}, 10);

		if (acceptButton.parentElement.tagName !== 'A') {
			acceptButton.addEventListener('click', function() {
				window[acceptFunc](chatId, 1);
			});
		}

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.remove();
			}, 300);
		});
	}

	if (data.errors) {
		console.log(data.errors);
	}
}