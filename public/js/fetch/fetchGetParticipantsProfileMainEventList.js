/*jshint esversion: 6 */

function fetchGetParticipantsProfileMainEventList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&entity_id=' + dataObj.entityId +
					'&profile_id=' + dataObj.profileId;


				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/profile/getMainEventParticipantsList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertProfileEventParticipantsList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertProfileEventParticipantsList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			dataObj.contentBlock.innerHTML = '';
			dataObj.contentBlock.insertAdjacentHTML('beforeend', data.html);
			dataObj.contentBlock.scrollTop = 0;

			loadProfileEventParticipantsByScroll(dataObj.contentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			dataObj.contentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		setTimeout(function() {
			scrollProfileEventParticipantsList = true;
		}, 200);
	} else {
		console.log(data.errors);
	}
}

function loadProfileEventParticipantsByScroll(contentBlock, dataObj) {
	let container = contentBlock.querySelector('.user_query_search');

	dataObj.scrollEl.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			profileEventParticipantsCount = container.getAttribute('data-usercount'),
			timestamp = container.getAttribute('data-timestamp'),
			existProfileEventParticipantsCount = container.children.length,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		if (scrollProfileEventParticipantsList &&
			containerRect.bottom <= window.screen.height - 20 &&
			profileEventParticipantsCount > existProfileEventParticipantsCount) {
			scrollProfileEventParticipantsList = false;
			fetchGetParticipantsProfileMainEventList(dataObj, timestamp, rowNum);
		}
	};
}