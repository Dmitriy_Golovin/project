/*jshint esversion: 6 */

function fetchDeclinetInviteCommonEvent(type, id, callback) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&id=' + id;

				fetch('/' + type + '/declineInvite', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					callback(response.data);
		  				}

		  				if (!response.res) {
		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}