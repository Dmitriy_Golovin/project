/*jshint esversion: 6 */

const TYPE_SHOW = 1;
const TYPE_PREVIOUS = 2;
const TYPE_NEXT = 3;

function fetchGetImageSliderViewModal(dataObj) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&entity_id=' + dataObj.entity_id +
					'&type=' + dataObj.type +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				if (dataObj.modalSliderClass) {
					modalSliderClass = dataObj.modalSliderClass;
				}

				if (dataObj.current_file_id) {
					data += '&current_file_id=' + dataObj.current_file_id;
				}

				if (dataObj.last_file_id) {
					data += '&last_file_id=' + dataObj.last_file_id;
				}

				if (dataObj.exceptionIdList) {
					for (let i = 0; i < dataObj.exceptionIdList.length; i++) {
						data += '&exceptionIdList[]=' + dataObj.exceptionIdList[i];
					}
				}

				fetch('/imageFile/' + dataObj.entityAction, {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					if (dataObj.type === TYPE_SHOW) {
			  					document.body.insertAdjacentHTML('beforeEnd', response.html);
		  						modalSliderClass = new ModalSliderImageWithCommentClass({
		  							'modalCssClass': 'modal_image_slider',
		  							'entity_id': dataObj.entity_id,
		  							'entityAction': dataObj.entityAction,
		  							'fetchFunction': 'fetchGetImageSliderViewModal',
		  							'exceptionIdList': (dataObj.exceptionIdList) ?
		  								dataObj.exceptionIdList :
		  								'',
		  						});
		  						modalSliderClass.show();

				  				modalSliderClass.closeEl.addEventListener('click', function() {
				  					modalSliderClass.close();
				  					modalSliderClass = null;
				  				});
			  				}

			  				if (dataObj.type === TYPE_PREVIOUS) {
			  					modalSliderClass.addNewImageInSlider(response.html, modalSliderClass.DIR_PREV());
			  				}

			  				if (dataObj.type === TYPE_NEXT) {
			  					modalSliderClass.addNewImageInSlider(response.html, modalSliderClass.DIR_NEXT());
			  				}
		  				} else {
		  					document.body.insertAdjacentHTML('beforeend', response.notice_html);

		  					let noticeModal = document.querySelector('.modal_notice_close'),
								noticeModalBox = noticeModal.querySelector('.modal_box'),
								closeButton = noticeModal.querySelector('.notice_button_close');

							setTimeout(function() {
								noticeModal.classList.add('modal_active');
								noticeModalBox.classList.add('modal_box_active');
							}, 10);

							closeButton.addEventListener('click', function() {
								noticeModalBox.classList.remove('modal_box_active');
								
								setTimeout(function() {
									noticeModal.remove();
								}, 300);
							});

		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}