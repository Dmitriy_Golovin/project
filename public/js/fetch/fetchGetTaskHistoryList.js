/*jshint esversion: 6 */

function fetchGetTaskHistoryList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&task_id=' + taskId +
					'&dateFrom=' + dataObj.dateFrom +
					'&dateTo=' + dataObj.dateTo +
					'&source_id=' + dataObj.sourceId +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				if (dataObj.type) {
					data += '&type=' + dataObj.type;
				}

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/task/getHistoryList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertEventList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function getDataTaskHistoryObj() {
	let result = {
		'sourceId': sourceId,
		'dateFrom': '',
		'dateTo': '',
	};

	if (dateFromField) {
		result.dateFrom = dateFromField.innerHTML;
	}

	if (dateToField) {
		result.dateTo = dateToField.innerHTML;
	}

	if (typeField) {
		result.type = typeField.options[typeField.selectedIndex].value;
	}

	return result;
}

function insertEventList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			taskContentList.innerHTML = '';
			taskContentList.insertAdjacentHTML('beforeend', data.html);
			taskContentList.scrollTop = 0;

			loadEventsByScroll(taskContentList, dataObj);
		}

		if (data.insert_type === 'add') {
			taskContentList.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollEventList = true;
	} else {
		console.log(data.errors);
	}
}

function loadEventsByScroll(eventContentList, dataObj) {
	let container = taskContentList.querySelector('.task_history_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			eventCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existTaskHistoryCount = 0,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		for (let el of container.children) {
			if (el.hasAttribute('data-rowNum')) {
				existTaskHistoryCount++;
			}
		}
		
		if (scrollEventList &&
			containerRect.bottom <= window.screen.height - 72 &&
			eventCount > existTaskHistoryCount) {
			scrollEventList = false;
			fetchGetTaskHistoryList(dataObj, timestamp, rowNum);
		}
	};
}