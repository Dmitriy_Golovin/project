/*jshint esversion: 6 */

function fetchGetChatMemberList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&chat_id=' + dataObj.chatId;

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/message/getChatMemberList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertChatMemberList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertChatMemberList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			chatMemberContentBlock.innerHTML = '';
			chatMemberContentBlock.insertAdjacentHTML('beforeend', data.html);
			chatMemberContentBlock.scrollTop = 0;

			loadChatMembersByScroll(chatMemberContentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			chatMemberContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollChatMember = true;
	} else {
		console.log(data.errors);
	}
}

function loadChatMembersByScroll(chatMemberContentBlock, dataObj) {
	let container = chatMemberContentBlock.querySelector('.user_query_search');

	container.parentElement.onscroll = function(e) {
		setTimeout(function() {
			let containerScroll = container.parentElement.scrollTop,
				userCount = container.getAttribute('data-usercount'),
				timestamp = container.getAttribute('data-timestamp'),
				existUserCount = container.children.length;
				rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');


			if (scrollChatMember &&
				containerScroll + container.parentElement.offsetHeight >= container.parentElement.scrollHeight - 10 &&
				userCount > existUserCount) {
				scrollChatMember = false;
				fetchGetChatMemberList(dataObj, timestamp, rowNum);
			}
		}, 50);
	};
}