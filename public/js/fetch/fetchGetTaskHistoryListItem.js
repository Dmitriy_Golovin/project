/*jshint esversion: 6 */

function fetchGetTaskHistoryListItem(data, callback) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let dataFetch = 'task_id=' + data.task_id +
				'&task_history_id=' + data.task_history_id +
				'&scenario=' + data.scenario +
				'&type=' + data.type +
				'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60) +
				'&csrf_test_name=' + document.querySelector('meta[name=X-CSRF-TOKEN]').getAttribute('content');
			
			fetch('/task/getTaskHistoryListItem', {
				method: 'POST',
				body: dataFetch,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
			.then(response => response.json())
			.then(response => {
				document.querySelector('meta[name=X-CSRF-TOKEN]').setAttribute('content', response.csrf);
				tab.sendCSRFToLS();
				return response;
			})
			.then(response => {
	  			if (response.res) {
	  				callback(response);
	  			} else {
	  				console.log(response.errors);
	  			}
			})
			.catch((error) => {
				console.log(error);
			});
		});
	});
}