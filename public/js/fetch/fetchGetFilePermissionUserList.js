/*jshint esversion: 6 */

function fetchGetFilePermissionUserList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&file_id=' + dataObj.fileId;

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/file/getFilePermissionUserList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertFilePermissionUserList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertFilePermissionUserList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			dataObj.contentBlock.innerHTML = '';
			dataObj.contentBlock.insertAdjacentHTML('beforeend', data.html);
			dataObj.contentBlock.scrollTop = 0;

			loadFileAccessUsersByScroll(dataObj);
		}

		if (data.insert_type === 'add') {
			dataObj.contentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollPermissionUserList = true;
	} else {
		console.log(data.errors);
	}
}

function loadFileAccessUsersByScroll(dataObj) {
	let container = dataObj.contentBlock.querySelector('.user_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			userFileAccessCount = container.getAttribute('data-usercount'),
			timestamp = container.getAttribute('data-timestamp'),
			existUserFileAccessCount = 0,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		for (let el of container.children) {
			if (el.hasAttribute('data-rowNum')) {
				existUserFileAccessCount++;
			}
		}

		if (scrollPermissionUserList &&
			containerRect.bottom <= window.screen.height - 72 &&
			userFileAccessCount > existUserFileAccessCount) {
			scrollPermissionUserList = false;
			fetchGetFilePermissionUserList(dataObj, timestamp, rowNum);
		}
	};
}