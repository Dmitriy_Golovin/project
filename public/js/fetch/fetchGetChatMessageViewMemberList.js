/*jshint esversion: 6 */

function fetchGetChatMessageViewMemberList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&chat_id=' + dataObj.chatId +
					'&chat_message_id=' + dataObj.chatMessageId;

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/message/getChatMessageViewMemberList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertViewedChatMemberList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertViewedChatMemberList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			chatMemberViewMessageContentBlock.innerHTML = '';
			chatMemberViewMessageContentBlock.insertAdjacentHTML('beforeend', data.html);
			chatMemberViewMessageContentBlock.scrollTop = 0;

			loadViewedChatMembersByScroll(chatMemberViewMessageContentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			chatMemberViewMessageContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollChatMessageViewMember = true;
	} else {
		console.log(data.errors);
	}
}

function loadViewedChatMembersByScroll(chatMemberViewMessageContentBlock, dataObj) {
	let container = chatMemberViewMessageContentBlock.querySelector('.user_query_search');

	container.parentElement.onscroll = function(e) {
		setTimeout(function() {
			let containerScroll = container.parentElement.scrollTop,
				userCount = container.getAttribute('data-usercount'),
				timestamp = container.getAttribute('data-timestamp'),
				existUserCount = container.children.length;
				rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');


			if (scrollChatMessageViewMember &&
				containerScroll + container.parentElement.offsetHeight >= container.parentElement.scrollHeight - 10 &&
				userCount > existUserCount) {
				scrollChatMessageViewMember = false;
				fetchGetChatMessageViewMemberList(dataObj, timestamp, rowNum);
			}
		}, 50);
	};
}