/*jshint esversion: 6 */

function fetchGetTaskList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&subType=' + dataObj.subType +
					'&name=' + dataObj.name +
					'&dateFrom=' + dataObj.dateFrom +
					'&dateTo=' + dataObj.dateTo +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				if (dataObj.privacy) {
					data += '&privacy=' + dataObj.privacy;
				}

				if (dataObj.reviewStatus) {
					data += '&reviewStatus=' + dataObj.reviewStatus;
				}

				if (dataObj.status) {
					data += '&status=' + dataObj.status;
				}

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/task/getList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertTaskList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function getDataTaskObj() {
	let result = {
		'subType': subType,
		'name': nameField.value,
		'privacy': privateTypeField.options[privateTypeField.selectedIndex].value,
		'dateFrom': '',
		'dateTo': '',
	};

	if (dateFromField) {
		result.dateFrom = dateFromField.innerHTML;
	}

	if (dateToField) {
		result.dateTo = dateToField.innerHTML;
	}

	if (reviewStatusField) {
		result.reviewStatus = reviewStatusField.options[reviewStatusField.selectedIndex].value;
	}

	if (taskStatusField) {
		result.status = taskStatusField.options[taskStatusField.selectedIndex].value;
	}

	return result;
}

function insertTaskList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			eventContentList.innerHTML = '';
			eventContentList.insertAdjacentHTML('beforeend', data.html);
			eventContentList.scrollTop = 0;

			loadTasksByScroll(eventContentList, dataObj);
		}

		if (data.insert_type === 'add') {
			eventContentList.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollEventList = true;
	} else {
		console.log(data.errors);
	}
}

function loadTasksByScroll(eventContentList, dataObj) {
	let container = eventContentList.querySelector('.event_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			taskCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existTaskCount = 0,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		for (let el of container.children) {
			if (el.hasAttribute('data-rowNum')) {
				existTaskCount++;
			}
		}

		if (scrollEventList &&
			containerRect.bottom <= window.screen.height - 72 &&
			taskCount > existTaskCount) {
			scrollEventList = false;
			fetchGetTaskList(dataObj, timestamp, rowNum);
		}
	};
}