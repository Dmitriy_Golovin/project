/*jshint esversion: 6 */

function fetchGetCommonEventDataSaveReport(callback) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&entity_id=' + id +
					'&type=' + type +
					'&action=' + action +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				if (reportId) {
					data += '&report_id=' + reportId;
				}

				fetch('/commonEventList/getCommonEventSaveReportData', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					mainBox.insertAdjacentHTML('beforeEnd', response.html);
		  					callback();
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}