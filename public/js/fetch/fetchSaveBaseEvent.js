/*jshint esversion: 6 */

function fetchSaveBaseEvent(dataObj) {
	return new Promise((resolve, reject) => {
		tab.checkToken(function() {
			tab.xjscsrf(function() {
				let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
					data = new FormData();

				data.append('csrf_test_name', csrfVal.getAttribute('content'));
				data.append('timeOffsetSeconds', new Date().getTimezoneOffset() * (-60));

				for (let key in dataObj) {
					if (typeof dataObj[key] === 'object') {
						prepareFileList(dataObj[key], key, data);
					} else {
						data.append(key, dataObj[key]);
					}
				}

				fetch('/calendar/saveBaseEvent', {
					method: 'POST',
					body: data,
					headers: {
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
				.then(response => response.json())
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
	  			.then(response => { resolve(response); })
	  			.catch((error) => {
					console.log(error);
				});
			});
		});
	});
}

function prepareFileList(fileList, name, data) {
	for (let i = 0; i < fileList.length; i++) {
		data.append(name + '[]', fileList[i]);
	}
}