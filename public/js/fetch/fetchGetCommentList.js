/*jshint esversion: 6 */

function fetchGetCommentList(dataObj) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&entity_id=' + dataObj.entityId +
					'&last_id=' + dataObj.lastId +
					'&timestamp=' + dataObj.timestamp +
					'&existCount=' + dataObj.existCount +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				fetch('/comment/getList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					if (!response.showPrevious) {
		  						dataObj.showPreviousEl.remove();
		  					}

		  					let offsetBefore = dataObj.scrollEl.scrollTop,
		  						heightBefore = (dataObj.scrollEl.classList.contains('modal_box_active')) ?
		  							dataObj.scrollEl.scrollHeight :
		  							dataObj.scrollEl.offsetHeight;

		  					dataObj.resultContainer.insertAdjacentHTML('afterBegin', response.html);

		  					let heightAfter = (dataObj.scrollEl.classList.contains('modal_box_active')) ?
		  							dataObj.scrollEl.scrollHeight :
		  							dataObj.scrollEl.offsetHeight;

		  					dataObj.scrollEl.scrollTo(0, offsetBefore + (heightAfter - heightBefore));
		  				} else {
		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}