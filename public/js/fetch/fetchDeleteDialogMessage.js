/*jshint esversion: 6 */

function fetchDeleteDialogMessage(messageId, accept) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let acceptVal = (accept) ? accept : 0,
				csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = 'messages_id=' + messageId + '&accept=' + acceptVal + '&csrf_test_name=' + csrfVal.getAttribute('content');

			fetch('/message/deleteDialogMessage', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
				.then(response => response.json())
	  			.then(response => responseProcessingDialog(response, messageId, 'fetchDeleteDialogMessage'))
	  			.catch((error) => {
					console.log(error);
				});
		});
	});
}

function fetchDeleteDialogAllMessages(withUserId, accept) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let acceptVal = (accept) ? accept : 0,
				csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = 'with_user_id=' + withUserId + '&accept=' + acceptVal + '&csrf_test_name=' + csrfVal.getAttribute('content');

			fetch('/message/deleteDialogAllMesages', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
				.then(response => response.json())
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
	  			.then(response => responseProcessingDialog(response, withUserId, 'fetchDeleteDialogAllMessages'))
	  			.catch((error) => {
					console.log(error);
				});
		});
	});
}

function responseProcessingDialog(data, messageOrDialogId, acceptFunc) {
	if (data.res === true) {
		let itemMessageOrDialog = document.querySelector('.message_item_box[data-message_id="' + messageOrDialogId + '"]') ||
				document.querySelector('.dialog_chat_item[data-profile_id="' + messageOrDialogId + '"]'),
			noticeModal = document.querySelector('.modal_notice');

		itemMessageOrDialog.remove();

		if (noticeModal) {
			let noticeModalBox = noticeModal.querySelector('.modal_box');
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.remove();
			}, 300);
		}
	}

	if (data.res === 'notice') {
		document.body.insertAdjacentHTML('beforeend', data.notice_html);
		let noticeModal = document.querySelector('.modal_notice'),
			noticeModalBox = noticeModal.querySelector('.modal_box'),
			acceptButton = noticeModal.querySelector('.notice_button_accept'),
			rejectButton = noticeModal.querySelector('.notice_button_reject');

		setTimeout(function() {
			noticeModal.classList.add('modal_active');
			noticeModalBox.classList.add('modal_box_active');
		}, 10);

		acceptButton.addEventListener('click', function() {
			window[acceptFunc](messageOrDialogId, 1);
		});

		rejectButton.addEventListener('click', function() {
			noticeModalBox.classList.remove('modal_box_active');
			
			setTimeout(function() {
				noticeModal.remove();
			}, 300);
		});
	}

	if (data.errors) {
		console.log(data.errors);
	}
}