/*jshint esversion: 6 */

function fetchGetFriendList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&page_id=' + dataObj.pageId;

				if (dataObj.searchString) {
					data += '&search_string=' + dataObj.searchString;
				}

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/friends/getFriendList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertFriendList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertFriendList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			friendContentBlock.innerHTML = '';
			friendContentBlock.insertAdjacentHTML('beforeend', data.html);
			friendContentBlock.scrollTop = 0;

			loadFriendsByScroll(friendContentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			friendContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollFriendList = true;
	} else {
		console.log(data.errors);
	}
}

function loadFriendsByScroll(friendContentBlock, dataObj) {
	let container = friendContentBlock.querySelector('.friend_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			friendCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existFriendCount = 0,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		for (let el of container.children) {
			if (el.hasAttribute('data-rowNum')) {
				existFriendCount++;
			}
		}

		if (scrollFriendList &&
			containerRect.bottom <= window.screen.height - 20 &&
			friendCount > existFriendCount) {
			scrollFriendList = false;
			fetchGetFriendList(dataObj, timestamp, rowNum);
		}
	};
}