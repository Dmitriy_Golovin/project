/*jshint esversion: 6 */

function fetchGetProfileMainEventList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&subType=' + dataObj.subType +
					'&timeOffsetSeconds=' + timeOffsetSeconds +
					'&profile_id=' + dataObj.profileId;


				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/profile/getMainEventList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertProfileEventList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertProfileEventList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			dataObj.profileEventContentBlock.innerHTML = '';
			dataObj.profileEventContentBlock.insertAdjacentHTML('beforeend', data.html);
			dataObj.profileEventContentBlock.scrollTop = 0;

			loadProfileEventsByScroll(dataObj.profileEventContentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			dataObj.profileEventContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		setTimeout(function() {
			scrollProfileEventList = true;
		}, 200);
	} else {
		console.log(data.errors);
	}
}

function loadProfileEventsByScroll(profileEventContentBlock, dataObj) {
	let container = profileEventContentBlock.querySelector('.profile_event_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			profileEventCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existProfileEventCount = container.children.length,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		if (scrollProfileEventList &&
			containerRect.bottom <= window.screen.height - 20 &&
			profileEventCount > existProfileEventCount) {
			scrollProfileEventList = false;
			fetchGetProfileMainEventList(dataObj, timestamp, rowNum);
		}
	};
}