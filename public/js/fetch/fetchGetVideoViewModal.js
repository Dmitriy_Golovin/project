/*jshint esversion: 6 */

function fetchGetVideoViewModal(dataObj, videoContainer) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&timeOffsetSeconds=' + timeOffsetSeconds +
					'&file_id=' + videoContainer.dataset.file_id +
					'&page=' + dataObj.page;

				fetch('/videoFile/details', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					document.body.insertAdjacentHTML('beforeEnd', response.html);
		  					let modal = document.querySelector('.modal_video'),
		  						modalBox = modal.querySelector('.modal_video_box'),
		  						mediaEl = modal.querySelector('div[media_init="0"]'),
		  						closePic = modal.querySelector('.close_container_pic'),
		  						commentFieldContainer = modal.querySelector('.comment_field_container'),
		  						modalVideoContentBox = modal.querySelector('.modal_video_content_box'),
		  						scrollWidth = getScrollWidth();

		  					new VideoFileExistElement(mediaEl);

		  					setTimeout(function() {
			  					modal.classList.add('modal_active');
			  					modalBox.classList.add('modal_box_active');

			  					if (commentFieldContainer) {
			  						showCommentWriteField(commentFieldContainer);
			  					}

			  					setPaddingBottomForListContainer(commentFieldContainer);
			  					document.body.classList.add('not_scroll');
								document.body.style.paddingRight = scrollWidth + 'px';
								document.querySelector('.autorized_or_personal_block').style.paddingRight = scrollWidth + 'px';
			  				}, 10);

			  				modalBox.addEventListener('scroll', function() {
			  					if (commentFieldContainer) {
			  						showCommentWriteField(commentFieldContainer);
			  					}
			  				});

			  				closePic.addEventListener('click', function() {
			  					modalBox.classList.remove('modal_box_active');
			  					document.body.classList.remove('not_scroll');
								document.body.style.paddingRight = '';
								document.querySelector('.autorized_or_personal_block').style.paddingRight = '';
									
			  					setTimeout(function() {
			  						modal.classList.remove('modal_active');
			  						modal.remove();
			  					}, 300);
			  				});
		  				} else {
		  					document.body.insertAdjacentHTML('beforeend', response.notice_html);

		  					let noticeModal = document.querySelector('.modal_notice_close'),
								noticeModalBox = noticeModal.querySelector('.modal_box'),
								closeButton = noticeModal.querySelector('.notice_button_close');

							setTimeout(function() {
								noticeModal.classList.add('modal_active');
								noticeModalBox.classList.add('modal_box_active');
							}, 10);

							closeButton.addEventListener('click', function() {
								noticeModalBox.classList.remove('modal_box_active');
								
								setTimeout(function() {
									noticeModal.remove();
								}, 300);
							});
							
		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}