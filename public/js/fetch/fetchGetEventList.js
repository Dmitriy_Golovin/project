/*jshint esversion: 6 */

function fetchGetEventList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&subType=' + dataObj.subType +
					'&name=' + dataObj.name +
					'&dateFrom=' + dataObj.dateFrom +
					'&dateTo=' + dataObj.dateTo +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				if (dataObj.privacy) {
					data += '&privacy=' + dataObj.privacy;
				}

				if (dataObj.inviteStatus) {
					data += '&inviteStatus=' + dataObj.inviteStatus;
				}

				if (dataObj.onlyCurrentAndScheduled) {
					data += '&onlyCurrentAndScheduled=' + dataObj.onlyCurrentAndScheduled;
				}

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/commonEventList/getList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertEventList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function getDataEventObj() {
	let result = {
		'type': type,
		'subType': subType,
		'name': nameField.value,
		'privacy': privateTypeField.options[privateTypeField.selectedIndex].value,
		'dateFrom': '',
		'dateTo': '',
	};

	if (dateFromField) {
		result.dateFrom = dateFromField.innerHTML;
	}

	if (dateToField) {
		result.dateTo = dateToField.innerHTML;
	}

	if (inviteStatusField) {
		result.inviteStatus = inviteStatusField.options[inviteStatusField.selectedIndex].value;
	}

	if (onlyCurrentAndScheduledField) {
		result.onlyCurrentAndScheduled = Number(onlyCurrentAndScheduledField.checked);
	}

	return result;
}

function insertEventList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			eventContentList.innerHTML = '';
			eventContentList.insertAdjacentHTML('beforeend', data.html);
			eventContentList.scrollTop = 0;

			loadEventsByScroll(eventContentList, dataObj);
		}

		if (data.insert_type === 'add') {
			eventContentList.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollEventList = true;
	} else {
		console.log(data.errors);
	}
}

function loadEventsByScroll(eventContentList, dataObj) {
	let container = eventContentList.querySelector('.event_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			eventCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existEventCount = 0,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		for (let el of container.children) {
			if (el.hasAttribute('data-rowNum')) {
				existEventCount++;
			}
		}

		if (scrollEventList &&
			containerRect.bottom <= window.screen.height - 72 &&
			eventCount > existEventCount) {
			scrollEventList = false;
			fetchGetEventList(dataObj, timestamp, rowNum);
		}
	};
}