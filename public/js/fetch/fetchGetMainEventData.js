/*jshint esversion: 6 */

function fetchGetMainEventData(type, callback) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + type +
					'&timeOffsetSeconds=' + timeOffsetSeconds;

				fetch('/commonEventList/getMainEventData', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertData(response.data))
		  			.then(response => callback())
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertData(data) {
	if (Number(data.type) === 1) {
		document.querySelector('.events_current_item_count').innerHTML = data.eventDataList.current.length;
		document.querySelector('.events_scheduled_item_count').innerHTML = data.eventDataList.scheduled.length;
		document.querySelector('.events_completed_item_count').innerHTML = data.eventDataList.completed.length;
		document.querySelector('.invites_event_item_count').insertAdjacentHTML('afterBegin', ' ' + data.eventDataList.invitations.length);

		document.querySelector('.meeting_current_item_count').innerHTML = data.meetingDataList.current.length;
		document.querySelector('.meeting_scheduled_item_count').innerHTML = data.meetingDataList.scheduled.length;
		document.querySelector('.meeting_completed_item_count').innerHTML = data.meetingDataList.completed.length;
		document.querySelector('.invites_meeting_item_count').insertAdjacentHTML('afterBegin', ' ' + data.meetingDataList.invitations.length);

		document.querySelector('.tasks_current_item_count').innerHTML = data.taskDataList.current;
		document.querySelector('.tasks_scheduled_item_count').innerHTML = data.taskDataList.scheduled;
		document.querySelector('.tasks_completed_item_count').innerHTML = data.taskDataList.completed;
		document.querySelector('.tasks_overdue_item_count').innerHTML = data.taskDataList.overdue;
		document.querySelector('.tasks_created_item_count').innerHTML = data.taskDataList.created;

		if (Number(data.taskNotViewedCountdata.currentCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.currentCount;
			document.querySelector('.tasks_current_item_count').append(spanEl);
		}

		if (Number(data.taskNotViewedCountdata.scheduledCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.scheduledCount;
			document.querySelector('.tasks_scheduled_item_count').append(spanEl);
		}

		if (Number(data.taskNotViewedCountdata.overdueCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.overdueCount;
			document.querySelector('.tasks_overdue_item_count').append(spanEl);
		}

		if (Number(data.taskNotViewedCountdata.createdCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.createdCount;
			document.querySelector('.tasks_created_item_count').append(spanEl);
		}
	}

	if (Number(data.type) === 2) {
		let currentEl = document.querySelector('.event_menu_item[data-type_event="current"]'),
			scheduledEl = document.querySelector('.event_menu_item[data-type_event="scheduled"]'),
			completedEl = document.querySelector('.event_menu_item[data-type_event="completed"]'),
			invitationsEl = document.querySelector('.event_menu_item[data-type_event="invitations"]');

		currentEl.querySelector('.span_count').innerHTML = ' ' + data.eventDataList.current.length;
		scheduledEl.querySelector('.span_count').innerHTML = ' ' + data.eventDataList.scheduled.length;
		completedEl.querySelector('.span_count').innerHTML = ' ' + data.eventDataList.completed.length;
		invitationsEl.querySelector('.span_count').insertAdjacentHTML('afterBegin', ' ' + data.eventDataList.invitations.length);
	}

	if (Number(data.type) === 3) {
		let currentEl = document.querySelector('.event_menu_item[data-type_meeting="current"]'),
			scheduledEl = document.querySelector('.event_menu_item[data-type_meeting="scheduled"]'),
			completedEl = document.querySelector('.event_menu_item[data-type_meeting="completed"]'),
			invitationsEl = document.querySelector('.event_menu_item[data-type_meeting="invitations"]');

		currentEl.querySelector('.span_count').innerHTML = ' ' + data.meetingDataList.current.length;
		scheduledEl.querySelector('.span_count').innerHTML = ' ' + data.meetingDataList.scheduled.length;
		completedEl.querySelector('.span_count').innerHTML = ' ' + data.meetingDataList.completed.length;
		invitationsEl.querySelector('.span_count').insertAdjacentHTML('afterBegin', ' ' + data.meetingDataList.invitations.length);
	}

	if (Number(data.type) === 4) {
		let currentEl = document.querySelector('.event_menu_item[data-type_task="current"]'),
			scheduledEl = document.querySelector('.event_menu_item[data-type_task="scheduled"]'),
			completedEl = document.querySelector('.event_menu_item[data-type_task="completed"]'),
			overdueEl = document.querySelector('.event_menu_item[data-type_task="overdue"]');
			createdEl = document.querySelector('.event_menu_item[data-type_task="created"]');

		currentEl.querySelector('.span_count').innerHTML = ' ' + data.taskDataList.current;
		scheduledEl.querySelector('.span_count').innerHTML = ' ' + data.taskDataList.scheduled;
		completedEl.querySelector('.span_count').innerHTML = ' ' + data.taskDataList.completed;
		overdueEl.querySelector('.span_count').innerHTML = ' ' + data.taskDataList.overdue;
		createdEl.querySelector('.span_count').innerHTML = ' ' + data.taskDataList.created;

		if (Number(data.taskNotViewedCountdata.currentCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.currentCount;
			currentEl.querySelector('.span_count').append(spanEl);
		}

		if (Number(data.taskNotViewedCountdata.scheduledCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.scheduledCount;
			scheduledEl.querySelector('.span_count').append(spanEl);
		}

		if (Number(data.taskNotViewedCountdata.overdueCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.overdueCount;
			overdueEl.querySelector('.span_count').append(spanEl);
		}

		if (Number(data.taskNotViewedCountdata.createdCount) > 0) {
			let spanEl = document.createElement('span');
			spanEl.classList.add('new_invite_amount');
			spanEl.innerHTML = data.taskNotViewedCountdata.createdCount;
			createdEl.querySelector('.span_count').append(spanEl);
		}
	}
}