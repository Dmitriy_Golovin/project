/*jshint esversion: 6 */

function fetchDeleteFile(dataObj) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&file_id=' + dataObj.file_id +
					'&type=' + dataObj.type;

				fetch('/file/delete', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				if (response.res) {
		  					if (Number(response.data.type) === 1) {
		  						modalSliderClass.close();
				  				modalSliderClass = null;
		  					}

		  					if (Number(response.data.type) === 3) {
		  						let modal = document.querySelector('.modal_video'),
		  							modalBox = modal.querySelector('.modal_video_box');

		  						modalBox.classList.remove('modal_box_active');
			  					document.body.classList.remove('not_scroll');
								document.body.style.paddingRight = '';
								document.querySelector('.autorized_or_personal_block').style.paddingRight = '';
									
			  					setTimeout(function() {
			  						modal.classList.remove('modal_active');
			  						modal.remove();
			  					}, 300);
		  					}

		  					deleteFileElementFromList(response.data.file_id, response.nothingFound);
		  				} else {
		  					console.log(response.errors);
		  				}
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function deleteFileElementFromList(fileId, nothingFound) {
	let parentContainer = document.querySelector('.file_query_search'),
		fileContainer = parentContainer.querySelector('div[data-file_id="' + fileId + '"]');
console.log(fileContainer)
	if (fileContainer) {
		fileContainer.remove();

		parentContainer.dataset.itemcount = Number(parentContainer.dataset.itemcount) - 1;
		let rowNum = 1;

		for (let el of parentContainer.children) {
			el.setAttribute('data-rowNum', rowNum);
			rowNum++;
		}

		if (parentContainer.children.length <= 0) {
			parentContainer.insertAdjacentHTML('afterbegin', nothingFound);
		}
	}
}