/*jshint esversion: 6 */

function fetchSendFriendRequest(dataObj, callback) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&profile_id=' + dataObj.profileId;

			fetch('/friends/' + dataObj.type, {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
			.then(response => response.json())
			.then(response => {
				csrfVal.setAttribute('content', response.csrf);
				tab.sendCSRFToLS();
				return response;
			})
  			.then(response => {
  				if (response.res) {
  					if (response.htmlNotice) {
  						document.body.insertAdjacentHTML('beforeend', response.htmlNotice);
  						let noticeModal = document.body.querySelector('.modal_notice'),
  							noticeModalBox = noticeModal.querySelector('.modal_box'),
  							closePic = noticeModal.querySelector('.close_win_modal');

  						setTimeout(function() {
							noticeModal.classList.add('modal_active');
							noticeModalBox.classList.add('modal_box_active');
						}, 10);

						closePic.onclick = function() {
							noticeModalBox.classList.remove('modal_box_active');
							
							setTimeout(function() {
								noticeModal.remove();
							}, 300);
						};
  					}

  					if (response.htmlData) {
  						callback(response);
  					}
  				}

  				if (!response.res) {
  					console.log(response.errors);
  				}
  			})
  			.catch((error) => {
				console.log(error);
			});
		});
	});
}