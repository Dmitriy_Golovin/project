/*jshint esversion: 6 */

function fetchGetArticleList(dataObj, timestamp, rowNum) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&article_section_id=' + dataObj.articleSectionId;

				if (dataObj.searchString) {
					data += '&search_string=' + dataObj.searchString;
				}

				if (!timestamp) {
					timestamp = parseInt(Date.now() / 1000);
				}

				data += '&timestamp=' + timestamp;

				if (rowNum) {
					data += '&rowNum=' + Number(rowNum);
				}

				fetch('/universum/getArticleList', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => insertArticleList(response, dataObj))
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}

function insertArticleList(data, dataObj) {
	if (data.res) {
		if (data.insert_type == 'renew') {
			articleContentBlock.innerHTML = '';
			articleContentBlock.insertAdjacentHTML('beforeend', data.html);
			articleContentBlock.scrollTop = 0;

			loadFriendsByScroll(articleContentBlock, dataObj);
		}

		if (data.insert_type === 'add') {
			articleContentBlock.firstElementChild.insertAdjacentHTML('beforeend', data.html);
		}

		scrollArticleList = true;
	} else {
		console.log(data.errors);
	}
}

function loadFriendsByScroll(articleContentBlock, dataObj) {
	let container = articleContentBlock.querySelector('.article_query_search');

	document.body.onscroll = function(e) {
		let containerRect = container.getBoundingClientRect(),
			articleCount = container.getAttribute('data-itemcount'),
			timestamp = container.getAttribute('data-timestamp'),
			existArticleCount = 0,
			rowNum = container.children[container.children.length - 1].getAttribute('data-rowNum');

		for (let el of container.children) {
			if (el.hasAttribute('data-rowNum')) {
				existArticleCount++;
			}
		}

		if (scrollArticleList &&
			containerRect.bottom <= window.screen.height - 20 &&
			articleCount > existArticleCount) {
			scrollArticleList = false;
			fetchGetArticleList(dataObj, timestamp, rowNum);
		}
	};
}