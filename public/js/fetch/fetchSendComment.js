/*jshint esversion: 6 */

function fetchSendComment(dataObj) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&type=' + dataObj.type +
					'&entity_id=' + dataObj.entityId +
					'&text=' + dataObj.text +
					'&timeOffsetSeconds=' + new Date().getTimezoneOffset() * (-60);

			fetch('/comment/sendComment', {
				method: 'POST',
				body: data,
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
			.then(response => response.json())
			.then(response => {
				csrfVal.setAttribute('content', response.csrf);
				tab.sendCSRFToLS();
				return response;
			})
  			.then(response => {
  				if (response.res) {
  					dataObj.commentQuerySearch.insertAdjacentHTML('beforeend', response.html);
  					dataObj.commentField.innerHTML = '';
  					dataObj.scrollEl.scrollTop = dataObj.scrollEl.offsetHeight;
  					dataObj.commentAmountEl.innerHTML = Number(dataObj.commentAmountEl.innerHTML) + 1;
  				}

  				if (!response.res) {
  					console.log(response.errors);
  				}
  			})
  			.catch((error) => {
				console.log(error);
			});
		});
	});
}