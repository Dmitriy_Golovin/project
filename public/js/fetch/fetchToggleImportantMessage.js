/*jshint esversion: 6 */

function fetchToggleImportantMessage(important, messageId, callback) {
	important = (important == 0 ) ? 1 : 0;
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]');

			fetch('/message/toggleImportantMessage', {
				method: 'POST',
				body: 'important=' + important + '&messages_id=' + messageId + '&csrf_test_name=' + csrfVal.getAttribute('content'),
				headers: {
					'Content-Type': 'application/x-www-form-urlencoded',
					'X-Requested-With': 'XMLHttpRequest',
				}
			})
				.then(response => response.json())
				.then(response => {
					csrfVal.setAttribute('content', response.csrf);
					tab.sendCSRFToLS();
					return response;
				})
	  			.then(response => callback(response))
	  			.catch((error) => {
					console.log(error);
				});
		});
	});
}