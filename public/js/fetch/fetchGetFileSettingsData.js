/*jshint esversion: 6 */

const TYPE_FILE_PAGE = 1;
const TYPE_MESSAGE_PAGE = 2;

function fetchGetFileSettingsData(dataObj) {
	tab.checkToken(function() {
		tab.xjscsrf(function() {
			let csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
				timeOffsetSeconds = new Date().getTimezoneOffset() * (-60),
				data = 'csrf_test_name=' + csrfVal.getAttribute('content') +
					'&file_id=' + dataObj.fileId +
					'&timeOffsetSeconds=' + timeOffsetSeconds;


				fetch('/file/getFileSettingsData', {
					method: 'POST',
					body: data,
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded',
						'X-Requested-With': 'XMLHttpRequest',
					}
				})
					.then(response => response.json())
					.then(response => {
						csrfVal.setAttribute('content', response.csrf);
						tab.sendCSRFToLS();
						return response;
					})
		  			.then(response => {
		  				let container = document.querySelector('.file_settings_data_container');
		  				container.insertAdjacentHTML('afterbegin', response.html);
		  			})
		  			.catch((error) => {
						console.log(error);
					});
		});
	});
}