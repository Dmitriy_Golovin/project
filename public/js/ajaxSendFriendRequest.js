function sendFriendRequest(recipientID, buttonEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'id_request_recipient=' + recipientID + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');
			
		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 1) {
					if (buttonEl['friendMenuItem']) {
						refreshFriendsList(buttonEl['friendMenuItem'])
					}

					tab.send(result['data']);
					changeElButtonInFriend(buttonEl['parentEl']);

					if (result['data']['ws_tab_sync_to_do'] === 'accept_friendship') {
						var parent = document.querySelector('.menu_user_main').children[3];
						tab.tabSync._minorMinusOneAmountNewSubscriber(parent)
					}
				} else if (result['res'] == 0) {
					console.log(result['errors'])
				}
			}
		};

		xhr.open('POST', '/user/sendFriendRequest');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
	
}

function abortFriendRequestSelf(recipientID, buttonEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'id_request_recipient=' + recipientID + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 1) {
					if (buttonEl['friendMenuItem']) {
						refreshFriendsList(buttonEl['friendMenuItem'])
					}
					
					tab.send(result['data']);
					changeElButtonUnsubscribe(buttonEl['parentEl']);
				} else if (result['res'] == 0) {
					console.log(result['errors'])
				}
			}
		};

		xhr.open('POST', '/user/unsubscribe');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function deleteFriend(recipientID, buttonEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'id_request_recipient=' + recipientID + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 1) {
					if (buttonEl['friendMenuItem']) {
						refreshFriendsList(buttonEl['friendMenuItem'])
					}
					
					tab.send(result['data']);
					changeElButtonDelete(buttonEl['parentEl']);
				} else if (result['res'] == 0) {
					console.log(result['errors'])
				}
			}
		};

		xhr.open('POST', '/user/deleteFriendFromList');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function rejectFriendRequest(recipientID, buttonEl) {
	tab.xjscsrf(function() {
		var xhr = new XMLHttpRequest(),
			csrfVal = document.querySelector('meta[name=X-CSRF-TOKEN]'),
			data = 'id_request_recipient=' + recipientID + '&' + 'csrf_test_name=' + csrfVal.getAttribute('content');

		xhr.onreadystatechange = function() {
			if (xhr.readyState == 4 && xhr.status == 200) {
				var result = JSON.parse(xhr.responseText);
				csrfVal.setAttribute('content', result['csrf']);
				tab.sendCSRFToLS();

				if (result['res'] == 1) {
					if (buttonEl['friendMenuItem']) {
						refreshFriendsList(buttonEl['friendMenuItem'])
					}
					
					tab.send(result['data']);
					if (result['data']['ws_tab_sync_to_do'] === 'reject_friendship') {
						var parent = document.querySelector('.menu_user_main').children[3];
						tab.tabSync._minorMinusOneAmountNewSubscriber(parent)
					}
				}
			}
		};

		xhr.open('POST', '/user/rejectFriendRequest');
		xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
		xhr.setRequestHeader("X-Requested-With", "XMLHttpRequest");
		xhr.send(data);
	});
}

function refreshFriendsList(friendMenuItem) {
	var parentEl = document.querySelector('.friend_content'),
		parentElChildren = parentEl.children;
	ajaxGetUserLinksOnProfilePage(friendMenuItem, parentEl);
}

function changeElButtonInFriend(buttonEl) {
	buttonEl.value = '';
	if (buttonEl.getAttribute('data-user_links') == 'subscriber') {
		buttonEl.classList.add('delete_friend_button');
		buttonEl.value = 'Удалить из друзей';
		buttonEl.classList.remove('profile_button');
		buttonEl.classList.remove('in_friend_button');
		buttonEl.setAttribute('data-user_links', 'friend');
		buttonEl.setAttribute('data-ws_tab_sync_to_do', 'remove_friend');
	} else if (buttonEl.getAttribute('data-user_links') == 'new_subscriber') {
		buttonEl.classList.add('delete_friend_button');
		buttonEl.value = 'Удалить из друзей';
		buttonEl.classList.remove('profile_button');
		buttonEl.classList.remove('in_friend_button');
		buttonEl.setAttribute('data-user_links', 'friend');
		buttonEl.setAttribute('data-ws_tab_sync_to_do', 'remove_friend');
	} else if (buttonEl.getAttribute('data-user_links') == 'no_communication') {
		buttonEl.classList.add('unsubscribe_button');
		buttonEl.value = 'Отписаться';
		buttonEl.classList.remove('profile_button');
		buttonEl.classList.remove('in_friend_button');
		buttonEl.setAttribute('data-user_links', 'you_are_subscribed');
		buttonEl.setAttribute('data-ws_tab_sync_to_do', 'unsubscribe');
	}
}

function changeElButtonUnsubscribe(buttonEl) {
	buttonEl.value = '';
	buttonEl.classList.add('profile_button');
	buttonEl.classList.add('in_friend_button');
	buttonEl.classList.remove('unsubscribe_button');
	buttonEl.setAttribute('data-user_links', 'no_communication');
	buttonEl.setAttribute('data-ws_tab_sync_to_do', 'subscribe');
	buttonEl.value = 'В друзья';
}

function changeElButtonDelete(buttonEl) {
	buttonEl.value = '';
	buttonEl.classList.add('profile_button');
	buttonEl.classList.add('in_friend_button');
	buttonEl.classList.remove('delete_friend_button');
	buttonEl.setAttribute('data-user_links', 'subscriber');
	buttonEl.setAttribute('data-ws_tab_sync_to_do', 'add_in_friend');
	buttonEl.value = 'В друзья';
}