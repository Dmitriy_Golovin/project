function createWindowForNewEvent(el) {
	
	var self = this;

	self.table = document.querySelector(el.parent),
	self.header = document.querySelector(el.header),
    self.spanDay = document.querySelectorAll(el.child),
	self.choice = document.querySelector(el.child);

	function setPositionElement(elem) {
		var winForNewEventActive = document.querySelector('.div-new-event'),
			winForNewEventActiveStartPage = document.querySelector('.div_new_event_start_page'),
		    arrowActiveL = document.querySelector('.arrow-left'),
			arrowActiveR = document.querySelector('.arrow-right'),
			contentElement = document.querySelector('.content_wrapper'),
			greetingBox = document.querySelector('.greeting');
		
		if (winForNewEventActive) {
			winForNewEventActive.classList.remove('div-new-event');
			winForNewEventActive.classList.remove('div-new-event-right');
			winForNewEventActive.classList.remove('div-new-event-left');
			winForNewEventActive.classList.remove('div-new-event-bottom');
			winForNewEventActive.classList.remove('div-new-event-top');
			winForNewEventActive.classList.remove('active');
		}
		
		if (winForNewEventActiveStartPage) {
			winForNewEventActiveStartPage.classList.remove('div_new_event_start_page');
			winForNewEventActiveStartPage.classList.remove('div-new-event-right');
			winForNewEventActiveStartPage.classList.remove('div-new-event-left');
			winForNewEventActiveStartPage.classList.remove('div-new-event-bottom');
			winForNewEventActiveStartPage.classList.remove('div-new-event-top');
			winForNewEventActiveStartPage.classList.remove('active');
		}
		
		if (arrowActiveL) {
			arrowActiveL.classList.remove('arrow-left');
			arrowActiveL.classList.remove('active');
		}
		
		if (arrowActiveR) {
			arrowActiveR.classList.remove('arrow-right');
			arrowActiveR.classList.remove('active');
		}
		
		self.choice = elem;
		
		var speed = 0,
		    arrow = document.querySelector('.arrow'),
		    winForNewEvent = document.querySelector('.add_new_win'),
	        boxElemTD = self.choice.getBoundingClientRect(),
		    top = boxElemTD.top,
		    bottom = boxElemTD.bottom,
		    left = boxElemTD.left;

		// if (el.startPage) {
		// 	winForNewEvent.classList.add('div_new_event_start_page');
		// } else {
			winForNewEvent.classList.add('div-new-event');
		// }
		
		self.choice.appendChild(winForNewEvent);
		
		setTimeout(function() {
			winForNewEvent.classList.add('active');	 
			arrow.classList.add('active');
		}, 10);

	    if (left - self.table.offsetLeft < self.table.clientWidth / 2) {
			winForNewEvent.classList.add('div-new-event-right');
			arrow.classList.add('arrow-left');
	    } else {
			winForNewEvent.classList.add('div-new-event-left');
			arrow.classList.add('arrow-right');
	    }
		
		if (top > document.documentElement.clientHeight / 2 ||
			winForNewEvent.offsetHeight + top > document.documentElement.clientHeight) {

			if (bottom - winForNewEvent.offsetHeight < 0) {
				winForNewEvent.style.bottom = (bottom - winForNewEvent.offsetHeight) + 'px';
			} else {
				winForNewEvent.classList.add('div-new-event-bottom');
			}
		} else {
			winForNewEvent.classList.add('div-new-event-top');
		}

		self.choice.appendChild(arrow);
		
		if (self.header.offsetHeight > 50) {
			if (top < self.header.offsetHeight) {
				var res = (self.header.offsetHeight > 50 && top < 0) ? self.header.offsetHeight + Math.abs(top) :
						  (self.header.offsetHeight > 50 && top > 0) ? self.header.offsetHeight - top : top;

				var intervalTop = setInterval(function() {

					window.scrollBy(0, -2);
					
					speed += 2;
					if (speed >= Math.abs(res)) {
						clearInterval(intervalTop);
						speed = 0;
					}
				}, 15);
					
			}
		} else {
			if (top < 0 || winForNewEvent.classList.contains('div-new-event-bottom') && winForNewEvent.offsetHeight > bottom) {
				var res = (winForNewEvent.classList.contains('div-new-event-bottom') && winForNewEvent.offsetHeight > bottom) ?
						  winForNewEvent.offsetHeight - bottom : (top < 0) ? top : 0;

				var intervalTop = setInterval(function() {

					window.scrollBy(0, -2);
					
					speed += 2;
					if (speed >= Math.abs(res)) {
						clearInterval(intervalTop);
						speed = 0;
					}
				}, 15);
					
			}
		}
		
		if (bottom > document.documentElement.clientHeight) {
	
			var res = bottom - document.documentElement.clientHeight;
			var intervalBottom = setInterval(function() {
				window.scrollBy(0, 2);

				speed += 2;
				if (speed >= Math.abs(res)) {
			        clearInterval(intervalBottom);
					speed = 0;
				}
			}, 15);
		}
		
		if (greetingBox) {
			setTableDimension();
		}
	}
	
	[].forEach.call(self.spanDay, function(node) {
		node.addEventListener('click', function(e) {
			var greetingBox = document.querySelector('.greeting'),
				newWin = document.querySelector('.add_new_win');

			if (!node.contains(e.target) || e.target == newWin || newWin.contains(e.target)) return;
			
			tab.checkToken(function() {
				ajaxGetContentForEventWin(node.parentElement, function() {
					showDateInWinNewEvent(node.parentElement);
					setPositionElement(node);
				});
			});
			
		});
	});
}

function closeWinForNewEvent(containerForElement) {

	var winForNewEventActive = document.querySelector('.add_new_win'),
		arrow = document.querySelector('.arrow'),
		arrowActiveL = document.querySelector('.arrow-left'),
		arrowActiveR = document.querySelector('.arrow-right'),
		newWinEventContentContainer = document.querySelector('.new_win_event_content_container');

	newWinEventContentContainer.innerHTML = '';
	// holidaysBox.classList.remove('holidays_box_style');
	
	if (winForNewEventActive) {
		winForNewEventActive.classList.remove('div-new-event');
		winForNewEventActive.classList.remove('div_new_event_start_page');
		winForNewEventActive.classList.remove('div-new-event-right');
		winForNewEventActive.classList.remove('div-new-event-left');
		winForNewEventActive.classList.remove('div-new-event-bottom');
		winForNewEventActive.classList.remove('div-new-event-top');
		winForNewEventActive.classList.remove('active');
		winForNewEventActive.style.bottom = '';
	}
	
	if (arrowActiveL) {
		arrowActiveL.classList.remove('arrow-left');
		arrowActiveL.classList.remove('active');
	}
	
	if (arrowActiveR) {
		arrowActiveR.classList.remove('arrow-right');
		arrowActiveR.classList.remove('active');
	}
	
	containerForElement.appendChild(winForNewEventActive);
	containerForElement.appendChild(arrow);
}

function showDateInWinNewEvent(el) {
	var elForContent = document.querySelector('.date_title_new_event'),
		dateArr = el.getAttribute('data-date_for_cell').split('-');
	elForContent.innerHTML = dateArr[2] + '.' + dateArr[1] + '.' + dateArr[0];
}
