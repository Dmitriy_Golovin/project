/*jshint esversion: 6 */

function addEmojiToMessageFieldModal(emojiContainer, messageField, sendMessageButton) {
	let emojiBox = emojiContainer.querySelector('.emoji_box'),
		emojiElList = emojiContainer.querySelectorAll('li'),
		messagesBlockWithTextField = document.querySelector('.messages_block'),
		writeMessageBoxMessagePage = document.querySelector('.write_message_box'),
		availableFileMessageBlock = document.querySelector('.available_file_message_block');

	emojiContainer.onclick = function(e) {
		if (e.target == document.querySelector('.emoji_icon') && emojiBox.classList.contains('menu_box_active')) {
			emojiBox.classList.remove('menu_box_active');
			emojiBox.parentElement.classList.remove('menu_box_active');
			messageField.classList.remove('message_field_modal_emoji_active');

		} else {
			if (!availableFileMessageBlock.classList.contains('available_file_message_block_active')) {
				messageField.classList.add('message_field_modal_emoji_active');
				new ResizeObserver(scrollDownMessageField.bind(null, messageField)).observe(messageField);
			}
			emojiBox.classList.add('menu_box_active');
			emojiBox.parentElement.classList.add('menu_box_active');
		}
	};

	for (let el of emojiElList) {
		el.addEventListener('click', function() {
			let text = messageField.value,
				emoji = el.innerHTML;

			messageField.setRangeText(emoji, messageField.selectionStart, messageField.selectionEnd, 'end');
    		messageField.focus();

			sendMessageButton.classList.remove('not_active_element');
			cursorPositionInMessageField = messageField.selectionStart;
		});
	}
}

function scrollDownMessageField(messageField) {
	let scrollBlock = messageField.scrollHeight - messageField.getBoundingClientRect().height;
	messageField.scrollTop = scrollBlock;
}