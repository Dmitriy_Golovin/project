/*jshint esversion: 6 */

let mainBox = document.querySelector('.main_box'),
	profileId = mainBox.dataset.profile_id,
	type = mainBox.dataset.file_type,
	fileContentBlock = mainBox.querySelector('.content_block'),
	scrollFileList = true,
	modalSliderClass,
	timeOutVideoMediaControl,
   	fullScreen = false;


window.addEventListener('load', function() {
	let dataObj = {
		'profileId': profileId,
		'type': type,
		'pageType': TYPE_FILE_PAGE,
		'fileContentBlock': fileContentBlock,
	};

	fetchGetFileList(dataObj);
});

window.addEventListener('resize', function() {
	if (document.body.offsetWidth > 650 && document.querySelector('.profile_menu_container') &&
		document.querySelector('.profile_menu_container').classList.contains('menu_container_active')) {
		document.querySelector('.profile_menu_container').classList.remove('menu_container_active');

		if (!document.querySelector('.close_menu_container').classList.contains('not_active')) {
			document.querySelector('.close_menu_container').classList.add('not_active');
		}
	}
});

document.body.addEventListener('click', function(e) {
	if (e.target.classList.contains('menu_icon_container') || e.target.closest('.menu_icon_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.add('menu_container_active');
		closeMenuEl.classList.remove('not_active');
	}

	if (e.target.classList.contains('close_menu_container') || e.target.closest('.close_menu_container')) {
		let menuContainer = document.querySelector('.profile_menu_container'),
			closeMenuEl = document.querySelector('.close_menu_container');

		menuContainer.classList.remove('menu_container_active');

		setTimeout(function() {
			closeMenuEl.classList.add('not_active');
		}, 200);
	}

	if (e.target.closest('.image_file_container_preview')) {
		let imageContainer = e.target.closest('.image_file_container_preview'),
			dataObj = {
				'type': TYPE_SHOW,
				'entity_id': profileId,
				'current_file_id': imageContainer.dataset.file_id,
				'entityAction': 'userSlider',
			};

		fetchGetImageSliderViewModal(dataObj);
	}

	if (e.target.classList.contains('comment_show_previous')) {
		let commentQuerySearch = e.target.parentElement.querySelector('.comment_query_search'),
			showPreviousEl = commentQuerySearch.parentElement.querySelector('.comment_show_previous'),
			commentCount = commentQuerySearch.dataset.comment_count,
			existCommentList = 0,
			timestamp = commentQuerySearch.dataset.timestamp,
			lastId = commentQuerySearch.children[0].dataset.comment_id,
			commentEntityId = commentQuerySearch.dataset.entity_id,
			commentType = commentQuerySearch.dataset.type;

			for (let el of commentQuerySearch.children) {
				if (el.hasAttribute('data-rowNum')) {
					existCommentList++;
				}
			}

			let dataObj = {
				'type': commentType,
				'entityId': commentEntityId,
				'timestamp': timestamp,
				'lastId': lastId,
				'existCount': existCommentList,
				'resultContainer': commentQuerySearch,
				// 'commentFieldContainer': commentFieldContainer,
				'showPreviousEl': showPreviousEl,
				'scrollEl': (e.target.closest('.modal_box_active')) ?
					e.target.closest('.modal_box_active') :
					document.documentElement,
			};

		fetchGetCommentList(dataObj);
	}

	if (e.target.classList.contains('send_comment_icon')) {
		let commentContainer = e.target.closest('.comment_container'),
			commentQuerySearch = commentContainer.querySelector('.comment_query_search'),
			commentField = commentContainer.querySelector('.comment_field'),
			commentValue = commentField.innerHTML,
			commentAmountEl = commentContainer.querySelector('.main_comment_amount');

		if (!commentValue) {
			return;
		}

		let commentData = {
			'type': commentQuerySearch.dataset.type,
			'entityId': commentQuerySearch.dataset.entity_id,
			'text': commentValue,
			'commentQuerySearch': commentQuerySearch,
			'commentField': commentField,
			'commentAmountEl': commentAmountEl,
			'scrollEl': e.target.closest('.slider_image_comment_container') ||
				e.target.closest('.slider_image_comment_container_mobile') ||
				e.target.closest('.modal_video_content_box') ||
				document.documentElement,
		};

		fetchSendComment(commentData);
	}

	if (e.target.closest('.video_file_preview_container') &&
		!e.target.classList.contains('video_template_delete_file_container') &&
		!e.target.closest('.video_template_delete_file_container')) {
		let videoContainer = e.target.closest('.video_file_preview_container');
		fetchGetVideoViewModal({'page': 0}, videoContainer);
	}
});