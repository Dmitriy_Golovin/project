function setGreetingBoxDimention() {
	
	var table = document.querySelector('table'),
		greetingBox = document.querySelector('.greeting'),
		documentWidth = document.body.clientWidth;
		
	document.body.style.overflow = 'hidden';
	var a1 = document.body.clientWidth;
	document.body.style.overflow = 'visible';
	var a2 = document.body.clientWidth;
	
	var scrollWidth = a1 - a2;
	
	if (greetingBox) {
		if (documentWidth >= 769 - scrollWidth && greetingBox) {
			greetingBox.style.maxWidth = document.documentElement.clientWidth - (table.offsetWidth + 65) + 'px';
		} else {
			greetingBox.style.maxWidth = document.documentElement.clientWidth - 40 + 'px';
		}
	}
}