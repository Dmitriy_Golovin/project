function hideScroll() {
	
	document.body.style.overflow = 'hidden';
	var a1 = document.body.clientWidth;
	document.body.style.overflow = 'visible';
	var a2 = document.body.clientWidth;
	
	var scrollWidth = a1 - a2;
	
	document.body.style.overflow = 'hidden';
	document.body.style.paddingRight = scrollWidth + 'px';
	
	if (document.documentElement.clientHeight > 481) {
		document.querySelector('.header_container').style.paddingRight = 2 * scrollWidth + 'px';
	} else {
		document.querySelector('.header_container').style.width = document.querySelector('.header_container').offsetWidth + scrollWidth;
	}
}

function showScroll() {
	
	document.body.style.overflow = 'hidden';
	var a1 = document.body.clientWidth;
	document.body.style.overflow = 'visible';
	var a2 = document.body.clientWidth;
	
	var scrollWidth = a1 - a2;
	
	document.body.style.overflow = 'auto';
	document.body.style.paddingRight = '0px';
	document.querySelector('.header_container').style.paddingRight = '0px';
	
	if (document.documentElement.clientHeight > 481) {
		document.querySelector('.header_container').style.paddingRight = 2 * scrollWidth + 'px';
	} else {
		document.querySelector('.header_container').style.width = document.querySelector('.header_container').offsetWidth - scrollWidth;
	}
	
}