<?php

namespace Admin\Controllers;

use App\Controllers\BaseController;
use Admin\Models\holiday\HolidaySearchModel;
use Admin\Models\holiday\SaveHolidayForm;
use Common\Models\HolidayModel;
use Common\Models\HolidayDateModel;
use Common\Models\FileModel;
use Common\Models\CountryTranslationModel;
use Common\Models\CountryModel;

class HolidayController extends BaseController
{
    public function index()
    {
        $filterData = $this->request->data;
        $model = new HolidaySearchModel();
        $model->fill($this->request->getGet());
        $searchResult = $model->search();

        return view('Admin\Views\holiday\index', array_merge($searchResult, [
            'title' => 'Праздники',
            'userData' => $filterData,
        ]));
    }

    public function details($id)
    {
        $filterData = $this->request->data;
        $model = new HolidayModel();
        $holiday = $model
            ->select(HolidayModel::table() . '.*')
            ->select('holidayDateTable.holiday_date')
            ->select('fileTable.url, fileTable.url as file, fileTable.title as file_title, fileTable.source as file_link')
            ->select('countryTable.title as country')
            ->join(HolidayDateModel::table() . ' as holidayDateTable', 'holidayDateTable.holiday_id = ' . HolidayModel::table() . '.holiday_id')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . HolidayModel::table() . '.file_id', 'left')
            ->join(CountryTranslationModel::table() . ' as countryTable', 'countryTable.country_id = ' . HolidayModel::table() . '.country_id and countryTable.lang_code = "ru"', 'left')
            ->where(HolidayModel::table() . '.holiday_id', $id)
            ->withDeleted()
            ->first();

        if (empty($holiday)) {
            return view('Admin\Views\templates\not_found_entity', [
                'title' => 'Праздник',
                'userData' => $filterData,
                'notFoundText' => 'Праздник с id ' . $id . ' не найден',
            ]);
        }

        return view('Admin\Views\holiday\details', [
            'title' => 'Праздник',
            'userData' => $filterData,
            'model' => $model,
            'result' => $holiday,
        ]);
    }

    public function create()
    {
        helper('validation');
        helper('text');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $model = new SaveHolidayForm();
        
        if ($this->request->getMethod() == 'post') {
            $model->fill($this->request->getPost());
            $model->file = $this->request->getFile('file');
            $model->scenario = $model::SCENARIO_CREATE;

            if ($model->validate() && $model->save()) {
                $session->destroy();

                return redirect()->to('/admin/holidays/details/' . $model->holiday_id);
            } else {
                $session->setFlashdata('redirectError', $model->getFirstError());
                $model->file = null;

                return view('Admin\Views\holiday/create' , [
                    'title' => 'Добавить праздник',
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            }
        }

        return view('Admin\Views\holiday\create', [
            'title' => 'Добавить праздник',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    public function edit($holidayId)
    {
        helper('validation');
        helper('text');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $holidayModel = new HolidayModel();
        $model = new SaveHolidayForm();
        $holiday = $holidayModel
            ->select(HolidayModel::table() . '.*')
            ->select('holidayDateTable.holiday_date')
            ->select('fileTable.url as fileUrl, fileTable.title as file_title, fileTable.source as file_link')
            ->join(HolidayDateModel::table() . ' as holidayDateTable', 'holidayDateTable.holiday_id = ' . HolidayModel::table() . '.holiday_id')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_HOLIDAY . '" and fileTable.item_id = ' . HolidayModel::table() . '.holiday_id', 'left')
            ->where(HolidayModel::table() . '.holiday_id', $holidayId)
            ->first();
        
        $model->fill((array)$holiday);

        if ($this->request->getMethod() == 'post') {
            $model->fill($this->request->getPost());
            $model->file = $this->request->getFile('file');
            $model->scenario = $model::SCENARIO_EDIT;

            if ($model->validate() && $model->save()) {
                $session->destroy();

                return redirect()->to('/admin/holidays/details/' . $model->holiday_id);
            } else {
                $session->setFlashdata('redirectError', $model->getFirstError());
                $model->file = null;

                return view('Admin\Views\holiday/create' , [
                    'title' => 'Редактировать праздник',
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            }
        }

        return view('Admin\Views\holiday\edit', [
            'title' => 'Редактировать праздник',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    public function delete($holidayId)
    {
        $holidayModel = new HolidayModel();
        $holiday = $holidayModel
            ->where('holiday_id', $holidayId)
            ->withDeleted()
            ->first();

        if (empty($holiday)) {
            return redirect()->back()->with('redirectError', lang('Holiday.Holiday with id {0} not found', [$holidayId]));
        }

        if (!$holidayModel->delete($holiday->holiday_id)) {
            return redirect()->back()->with('redirectError', lang('Holiday.Failed to delete holiday'));
        } else {
            return redirect()->to('/admin/holidays');
        }
    }
}
