<?php

namespace Admin\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Controllers\BaseController;
use Admin\Models\article\ArticleSearchModel;
use Common\Models\ArticleModel;
use Common\Models\FileModel;

class ArticleController extends BaseController
{
    use ResponseTrait;

    public function index()
    {
        $filterData = $this->request->data;
        $model = new ArticleSearchModel();
        $model->fill($this->request->getGet());
        $searchResult = $model->search();

        return view('Admin\Views\article\index', array_merge($searchResult, [
            'title' => 'Статьи',
            'userData' => $filterData,
        ]));
    }

    public function details($id)
    {
        $filterData = $this->request->data;
        $model = new ArticleModel();
        $article = $model
            ->select(ArticleModel::table() . '.*')
            ->where(ArticleModel::table() . '.article_id', $id)
            ->withDeleted()
            ->first();

        if (empty($article)) {
            return view('Admin\Views\templates\not_found_entity', [
                'title' => 'Статья',
                'userData' => $filterData,
                'notFoundText' => 'Статья с id ' . $id . ' не найдена',
            ]);
        }

        $article->description = $model::prepareDescriptionForDetailsAdmin($article->description);

        return view('Admin\Views\article\details', [
            'title' => 'Статья',
            'userData' => $filterData,
            'model' => $model,
            'result' => $article,
        ]);
    }

    public function create()
    {
        helper('text');
        helper('validation');
        helper('cirillicToLatin');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $model = new ArticleModel();
        $model->labels = $model::getLabels();

        if ($this->request->getMethod() == 'post') {
            $validationData = [
                'article_section_id' => 'required|integer',
                'title' => 'required|string',
                'meta_description' => 'required|string',
            ];

            if (!$this->validate($validationData)) {
                $session->setFlashdata('redirectError', getValidationFirstError($this->validator->getErrors()));

                return view('Admin\Views\article\create', [
                    'title' => 'Добавить статью',
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            } else {
                $articleData = $this->request->getPost();
                $articleData['title_transliteration'] = toLatin($articleData['title']);
                if (!$model->save($articleData)) {
                    $session->setFlashdata('redirectError', lang('Article.Failed to save article'));

                    return view('Admin\Views\article\create', [
                        'title' => 'Добавить статью',
                        'userData' => $filterData,
                        'model' => $model,
                    ]);
                }

                return redirect()->to('/admin/articles/details/' . $model->getInsertID());
            }
        }

        return view('Admin\Views\article\create', [
            'title' => 'Добавить статью',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    public function edit($articleId)
    {
        helper('validation');
        helper('text');
        helper('cirillicToLatin');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $articleModel = new ArticleModel();
        $article = $articleModel->find($articleId);
        $article->labels = $articleModel::getLabels();

        if ($this->request->getMethod() == 'post') {
            $validationData = [
                'article_section_id' => 'required|integer',
                'title' => 'required|string|max_length[255]',
                'meta_description' => 'required|string|max_length[255]',
            ];

            if (!$this->validate($validationData)) {
                $session->setFlashdata('redirectError', getValidationFirstError($this->validator->getErrors()));

                return view('Admin\Views\article\create', [
                    'title' => 'Редактировать статью',
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            } else {
                $article->title = $this->request->getVar('title');
                $article->title_transliteration = toLatin($article->title);
                $article->article_section_id = $this->request->getVar('article_section_id');
                $article->meta_description = $this->request->getVar('meta_description');

                if (!$articleModel->save($article)) {
                    $session->setFlashdata('redirectError', lang('Article.Failed to save article'));

                    return view('Admin\Views\article\create', [
                        'title' => 'Редактировать статью',
                        'userData' => $filterData,
                        'model' => $model,
                    ]);
                }

                return redirect()->to('/admin/articles/details/' . $articleId);
            }
        }

        return view('Admin\Views\article\edit', [
            'title' => 'Редактировать статью',
            'userData' => $filterData,
            'model' => $article,
        ]);
    }

    public function addParagraph($articleId)
    {
        helper('text');
        helper('validation');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $model = new ArticleModel();
        $model->labels = $model::getLabels();
        $article = $model->find($articleId);

        if (empty($article)) {
            return view('Admin\Views\templates\not_found_entity', [
                'title' => 'Статья',
                'userData' => $filterData,
                'notFoundText' => 'Статья с id ' . $articleId . ' не найдена',
            ]);
        }

        $article->paragraphCount = !empty($article->description)
            ? count(explode(ArticleModel::PARAGRAPH_DELIMITER, $article->description))
            : 0;

        if ($this->request->getMethod() == 'post') {
            $validationData = [
                'description' => 'required|string',
                'position' => 'required|integer',
            ];

            if (!$this->validate($validationData)) {
                $session->setFlashdata('redirectError', getValidationFirstError($this->validator->getErrors()));

                return view('Admin\Views\article\paragraph-form', [
                    'title' => 'Добавить абзац для статьи',
                    'userData' => $filterData,
                    'model' => $model,
                    'article' => $article,
                ]);
            } else {
                $text = $this->request->getVar('description');
                $position = $this->request->getVar('position');
                $article->description = $model::prepareDescription($article->description, $text, $position, 'text');

                if (!$model->save($article)) {
                    $session->setFlashdata('redirectError', lang('Article.Failed to save article'));

                    return view('Admin\Views\article\paragraph-form', [
                        'title' => 'Добавить абзац для статьи',
                        'userData' => $filterData,
                        'model' => $model,
                        'article' => $article,
                    ]);
                }

                return redirect()->to('/admin/articles/details/' . $articleId);
            }
        }

        return view('Admin\Views\article\paragraph-form', [
            'title' => 'Добавить абзац для статьи',
            'userData' => $filterData,
            'model' => $model,
            'article' => $article,
        ]);
    }

    public function addImage($articleId)
    {
        helper('text');
        helper('validation');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $model = new ArticleModel();
        $model->labels = $model::getLabels();
        $article = $model->find($articleId);

        if (empty($article)) {
            return view('Admin\Views\templates\not_found_entity', [
                'title' => 'Статья',
                'userData' => $filterData,
                'notFoundText' => 'Статья с id ' . $articleId . ' не найдена',
            ]);
        }

        $article->paragraphCount = !empty($article->description)
            ? count(explode(ArticleModel::PARAGRAPH_DELIMITER, $article->description))
            : 0;

        if ($this->request->getMethod() == 'post') {
            $validationData = [
                'file_link' => 'permit_empty|string|max_length[255]',
                'file_title' => 'required|string|max_length[255]',
                'position' => 'required|integer',
                'file' => 'permit_empty|uploaded[file]|max_size[file,19000]|is_image[file]|ext_in[file,png,jpeg,jpg,gif]|mime_in[file,image/png,image/jpg,image/jpeg,image/gif]',
            ];

            if (!$this->validate($validationData)) {
                $session->setFlashdata('redirectError', getValidationFirstError($this->validator->getErrors()));

                return view('Admin\Views\article\image-form', [
                    'title' => 'Добавить изображение для статьи',
                    'userData' => $filterData,
                    'model' => $model,
                    'article' => $article,
                ]);
            } else {
                $file = $this->request->getFile('file');

                if (!empty($file->getError())) {
                    $session->setFlashdata('redirectError', 'Загрузите изображение');

                    return view('Admin\Views\article\image-form', [
                        'title' => 'Добавить изображение для статьи',
                        'userData' => $filterData,
                        'model' => $model,
                        'article' => $article,
                    ]);
                }

                $fileModel = new FileModel();
                $file_title = $this->request->getVar('file_title');
                $file_link = $this->request->getVar('file_link');
                $position = $this->request->getVar('position');
                $fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_IMAGE);
                $fileData = [
                    'type' => FileModel::TYPE_IMAGE,
                    'related_item' => FileModel::RELATED_ITEM_ARTICLE,
                    'item_id' => $articleId,
                    'url' => $fileDataDisk['fileName'],
                    'privacy_status' => FileModel::PUBLIC_STATUS,
                    'mime_type' => $fileDataDisk['mimeType'],
                    'original_name' => $fileDataDisk['originalName'],
                    'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
                    'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
                    'title' => $file_title,
                    'source' => !empty($file_link) ? $file_link : null,
                ];

                if (!$fileModel->save($fileData)) {
                    $session->setFlashdata('redirectError', lang('File.Failed to save file'));

                    return view('Admin\Views\article\image-form', [
                        'title' => 'Добавить изображение для статьи',
                        'userData' => $filterData,
                        'model' => $model,
                        'article' => $article,
                    ]);
                }

                $fileData['file_id'] = $fileModel->getInsertID();
                $article->description = $model::prepareDescription($article->description, $fileData, $position, 'image');

                if (!$model->save($article)) {
                    $session->setFlashdata('redirectError', lang('Article.Failed to save article'));

                    return view('Admin\Views\article\image-form', [
                        'title' => 'Добавить абзац для статьи',
                        'userData' => $filterData,
                        'model' => $model,
                        'article' => $article,
                    ]);
                }

                return redirect()->to('/admin/articles/details/' . $articleId);
            }
        }

        return view('Admin\Views\article\image-form', [
            'title' => 'Добавить изображение для статьи',
            'userData' => $filterData,
            'model' => $model,
            'article' => $article,
        ]);
    }

    public function deleteParagraph()
    {
        helper('validation');

        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        if (!$this->validate([
            'article_id' => 'required|integer',
            'position' => 'required|integer',
            'type' => 'required|string|in_list[image,text]',
            'file_id' => 'permit_empty|integer'
        ])) {

            return $this->respond([
                'res' => false,
                'errors' => getValidationFirstError($this->validator->getErrors()),
                'csrf' => csrf_hash()
            ]);
        } else {
            $dataPost = $this->request->getPost();
            $articleModel = new ArticleModel();
            $article = $articleModel->find($dataPost['article_id']);

            if (empty($article)) {
                return $this->respond([
                    'res' => false,
                    'errors' => 'Статья с id ' . $dataPost['article_id'] . ' не найдена',
                    'csrf' => csrf_hash()
                ]);
            }

            if ($dataPost['type'] === 'image') {
                $fileModel = new FileModel();
                $file = $fileModel
                    ->where('related_item', FileModel::RELATED_ITEM_ARTICLE)
                    ->where('item_id', $article->article_id)
                    ->where('file_id', $dataPost['file_id'])
                    ->first();

                if (empty($file)) {
                    return $this->respond([
                        'res' => false,
                        'errors' => 'Файл с id ' . $dataPost['file_id'] . ' не найден',
                        'csrf' => csrf_hash()
                    ]);
                }

                $fileModel->delete($file->file_id);
            }

            $article->description = $articleModel::deleteParagraph($article->description, $dataPost['position']);

            if (!$articleModel->save($article)) {
                return $this->respond([
                    'res' => false,
                    'errors' => lang('Article.Failed to save article'),
                    'csrf' => csrf_hash()
                ]);
            } else {
                return $this->respond([
                    'res' => true,
                    'csrf' => csrf_hash()
                ]);
            }
        }
    }

    public function publish($articleId)
    {
        $articleModel = new ArticleModel();
        $article = $articleModel->find($articleId);

        if (empty($article)) {
            return redirect()->back()->with('redirectError', lang('Article.Article with id {0} not found', [$articleId]));
        }

        $article->status = ArticleModel::STATUS_PUBLISHED;

        if (!$articleModel->save($article)) {
            return redirect()->back()->with('redirectError', lang('Article.Failed to save article'));
        }

        return redirect()->to('/admin/articles/details/' . $articleId);
    }

    public function unpublish($articleId)
    {
        $articleModel = new ArticleModel();
        $article = $articleModel->find($articleId);

        if (empty($article)) {
            return redirect()->back()->with('redirectError', lang('Article.Article with id {0} not found', [$articleId]));
        }

        $article->status = ArticleModel::STATUS_NOT_PUBLISHED;

        if (!$articleModel->save($article)) {
            return redirect()->back()->with('redirectError', lang('Article.Failed to save article'));
        }

        return redirect()->to('/admin/articles/details/' . $articleId);
    }

    public function delete($articleId)
    {
        $articleModel = new ArticleModel();
        $article = $articleModel->find($articleId);

        if (empty($article)) {
            return redirect()->back()->with('redirectError', lang('Article.Article with id {0} not found', [$articleId]));
        }

        if (!$articleModel->deleteFiles($article->article_id) || !$articleModel->delete($article->article_id)) {
            return redirect()->back()->with('redirectError', lang('Article.Failed to delete article'));
        } else {
            return redirect()->to('/admin/articles');
        }
    }
}
