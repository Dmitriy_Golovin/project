<?php

namespace Admin\Controllers;

use Admin\Controllers\BaseController;
use Admin\Models\VisitLogSearchModel;

class VisitLogController extends BaseController
{
    public function index()
    {
        $filterData = $this->request->data;
        $model = new VisitLogSearchModel();
        $model->fill($this->request->getGet());
        $searchResult = $model->search();

        return view('Admin\Views\visit-log\index', array_merge($searchResult, [
            'title' => 'Логи посещений',
            'userData' => $filterData,
        ]));
    }
}
