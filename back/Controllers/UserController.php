<?php

namespace Admin\Controllers;

use Admin\Controllers\BaseController;
use Admin\Models\UserSearchModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\CountryModel;
use Common\Models\CityModel;

class UserController extends BaseController
{
    public function index()
    {
        $filterData = $this->request->data;
        $model = new UserSearchModel();
        $model->fill($this->request->getGet());
        $searchResult = $model->search();

        return view('Admin\Views\user\index', array_merge($searchResult, [
            'title' => 'Пользователи',
            'userData' => $filterData,
        ]));
    }

    public function details($id)
    {
        $filterData = $this->request->data;
        $model = new UserModel();
        $user = $model->getUserData($id, true, true, true);

        if (empty($user)) {
            return view('Admin\Views\templates\not_found_entity', [
                'title' => 'Пользователь',
                'userData' => $filterData,
                'notFoundText' => 'Пользователь с id ' . $id . ' не найден',
            ]);
        }

        return view('Admin\Views\user\details', [
            'title' => 'Пользователь',
            'userData' => $filterData,
            'model' => $model,
            'result' => $user,
        ]);
    }

    public function create()
    {
        helper('validation');
        helper('text');
        $filterData = $this->request->data;
        $model = new \Common\Entities\User();
        
        if ($this->request->getMethod() == 'post') {
            return $this->save($model, $filterData, 'create', 'Добавить пользователя');
        }

        return view('Admin\Views\user\create', [
            'title' => 'Добавить пользователя',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    public function edit($userId)
    {
        helper('validation');
        helper('text');
        $filterData = $this->request->data;
        $userModel = new UserModel();
        $model = $userModel->getUserData($userId, true, true, true);
        
        if ($this->request->getMethod() == 'post') {
            return $this->save($model, $filterData, 'edit', 'Редактировать пользователя',  $userId);
        }

        return view('Admin\Views\user\edit', [
            'title' => 'Редактировать пользователя',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    protected function save($model, $filterData, $view, $title, $userId = null)
    {
        $session = \Config\Services::session();
        $model->fill($this->request->getPost());

        $validationData = [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'email' => 'required|valid_email',
            'password' => 'required|string|min_length[6]|max_length[24]',
            'repeat_password' => 'required|string|matches[password]',
            'country_id' => 'permit_empty|integer',
            'city_id' => 'permit_empty|integer',
            'role' => 'required|integer|in_list[1,2]',
            'is_blocked' => 'required|integer|in_list[1,2]',
        ];

        if (empty($userId)) {
            $validationData['password'] = 'required|string|min_length[6]|max_length[24]';
            $validationData['repeat_password'] = 'required|string|matches[password]';
        } else {
            $validationData['password'] = 'permit_empty|string|min_length[6]|max_length[24]';
            $validationData['repeat_password'] = 'permit_empty|string|required_with[password]|matches[password]';
        }

        if (!$this->validate($validationData)) {
            $session->setFlashdata('redirectError', getValidationFirstError($this->validator->getErrors()));
            $model->password = null;
            $model->repeat_password = null;

            return view('Admin\Views\user\/' . $view, [
                'title' => $title,
                'userData' => $filterData,
                'model' => $model,
            ]);
        } else {
            if (!empty($model->country_id)) {
                $countryModel = new CountryModel();
                $country = $countryModel->find($model->country_id);

                if (empty($country)) {
                    $session->setFlashdata('redirectError', lang('Country.Country not found'));
                    $model->password = null;
                    $model->repeat_password = null;

                    return view('Admin\Views\user\/' . $view, [
                        'title' => $title,
                        'userData' => $filterData,
                        'model' => $model,
                    ]);
                }
            } else {
                $model->country_id = null;
            }

            if (!empty($model->city_id)) {
                $cityModel = new CityModel();
                $city = $cityModel->find($model->city_id);

                if (empty($city)) {
                    $session->setFlashdata('redirectError', lang('City.City not found'));
                    $model->password = null;
                    $model->repeat_password = null;

                    return view('Admin\Views\user\/' . $view, [
                        'title' => $title,
                        'userData' => $filterData,
                        'model' => $model,
                    ]);
                }
            } else {
                $model->city_id = null;
            }

            $userModel = new UserModel();
            $model->user_id = $userId;

            if (empty($userId)) {
                $model->password = password_hash($model->password, PASSWORD_DEFAULT);
            } else {
                if (!empty($model->password)) {
                    $model->password = password_hash($model->password, PASSWORD_DEFAULT);
                } else {
                    $userModel = new UserModel();
                    $user = $userModel->find($userId);
                    $model->password = $user->password;
                }
            }

            $model->accept_terms = true;

            if (!$userModel->save($model)) {
                $session->setFlashdata('redirectError', lang('Main.Failed to save user'));
                $model->password = null;
                $model->repeat_password = null;

                return view('Admin\Views\user\/' . $view, [
                    'title' => $title,
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            } else {
                $insertId = (is_null($userId))
                    ? $userModel->getInsertID()
                    : $userId;
                
                if (is_null($userId)) {
                    $userModel->setSettingsUserDefault($userModel->getInsertID());
                }

                $session->destroy();
                return redirect()->to('/admin/users/details/' . $insertId);
            }
        }
    }

    public function uploadAvatar($userId)
    {
        helper('validation');
        $userModel = new UserModel();
        $user = $userModel
            ->where('user_id', $userId)
            ->withDeleted()
            ->first();

        if (empty($user)) {
            return redirect()->back()->with('redirectError', lang('Main.User with id {0} not found', [$userId]));
        }

        if (!$this->validate([
            'avatar' => 'uploaded[avatar]|max_size[avatar,19000]|is_image[avatar]|ext_in[avatar,png,jpeg,jpg,gif]|mime_in[avatar,image/png,image/jpg,image/jpeg,image/gif]',
        ])) {

            return redirect()->back()->with('redirectError', getValidationFirstError($this->validator->getErrors()));
        }

        $fileModel = new FileModel();
        $file = $this->request->getFile('avatar');

        if (empty($file)) {
            return redirect()->back()->with('redirectError', lang('File.File not found'));
        }

        $fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_IMAGE);
        $fileData = [
            'type' => FileModel::TYPE_IMAGE,
            'related_item' => 'user',
            'item_id' => $userId,
            'url' => $fileDataDisk['fileName'],
            'privacy_status' => FileModel::PUBLIC_STATUS,
            'mime_type' => $fileDataDisk['mimeType'],
            'original_name' => $fileDataDisk['originalName'],
            'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
            'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
        ];

        if (!$fileModel->save($fileData)) {
            return redirect()->back()->with('redirectError', lang('File.Failed to save file'));
        }

        $user->file_id = $fileModel->getInsertID();

        if (!$userModel->save($user)) {
            return redirect()->back()->with('redirectError', lang('Main.Failed to save avatar'));
        } else {
            return redirect()->to('/admin/users/details/' . $userId);
        }
    }

    public function deleteAvatar($userId, $fileId)
    {
        $userModel = new UserModel();
        $user = $userModel
            ->where('user_id', $userId)
            ->where('file_id', $fileId)
            ->withDeleted()
            ->first();

        if (empty($user)) {
            return redirect()->back()->with('redirectError', lang('Main.User with id {0} not found', [$userId]));
        }

        $fileModel = new FileModel();
        $fileModel->delete($fileId);

        return redirect()->to('/admin/users/details/' . $userId);
    }

    public function block($userId)
    {
        $userModel = new UserModel();
        $user = $userModel
            ->where('user_id', $userId)
            ->withDeleted()
            ->first();

        if (empty($user)) {
            return redirect()->back()->with('redirectError', lang('Main.User with id {0} not found', [$userId]));
        }

        $user->is_blocked = UserModel::STATUS_BLOCKED;

        if (!$userModel->save($user)) {
            return redirect()->back()->with('redirectError', lang('Main.Failed to save user'));
        } else {
            return redirect()->to('/admin/users/details/' . $userId);
        }
    }

    public function unblock($userId)
    {
        $userModel = new UserModel();
        $user = $userModel
            ->where('user_id', $userId)
            ->withDeleted()
            ->first();

        if (empty($user)) {
            return redirect()->back()->with('redirectError', lang('Main.User with id {0} not found', [$userId]));
        }

        $user->is_blocked = UserModel::STATUS_NOT_BLOCKED;

        if (!$userModel->save($user)) {
            return redirect()->back()->with('redirectError', lang('Main.Failed to save user'));
        } else {
            return redirect()->to('/admin/users/details/' . $userId);
        }
    }

    public function delete($userId)
    {
        $userModel = new UserModel();
        $user = $userModel
            ->where('user_id', $userId)
            ->where('deleted_at', null)
            ->withDeleted()
            ->first();

        if (empty($user)) {
            return redirect()->back()->with('redirectError', lang('Main.User with id {0} not found', [$userId]));
        }

        if (!$userModel->delete($user->user_id)) {
            return redirect()->back()->with('redirectError', lang('Main.Failed to delete account'));
        } else {
            return redirect()->to('/admin/users/details/' . $userId);
        }
    }

    public function restore($userId)
    {
        $userModel = new UserModel();
        $user = $userModel
            ->where('user_id', $userId)
            ->where('deleted_at is not', null)
            ->withDeleted()
            ->first();

        if (empty($user)) {
            return redirect()->back()->with('redirectError', lang('Main.User with id {0} not found', [$userId]));
        }

        $userRestoreQuery = $userModel->builder()
            ->where('user_id', $userId)
            ->set(['deleted_at' => null]);

        if (!$userRestoreQuery->update()) {
            return redirect()->back()->with('redirectError', lang('Main.Failed to save account'));
        } else {
            return redirect()->to('/admin/users/details/' . $userId);
        }
    }
}
