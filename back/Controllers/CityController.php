<?php

namespace Admin\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Common\Models\CityTranslationModel;
use Common\Models\CityModel;
use Common\Models\CountryModel;

class CityController extends BaseController
{
    use ResponseTrait;

    public function index()
    {
        
    }

    public function getCityList()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] !== 'user') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        if (!$this->validate([
            'country_id' => 'required|integer',
        ])) {

            return $this->respond([
                'res' => false,
                'errors' => $this->validator->getError('country_id'),
                'csrf' => csrf_hash()
            ]);
        } else {            $countryModel = new CountryModel();
            $country = $countryModel->find($this->request->getVar('country_id'));

            if (empty($country)) {
                return $this->respond([
                    'res' => false,
                    'errors' => lang('Country.Country not found'),
                    'csrf' => csrf_hash()
                ]);
            }

            helper('arraySelf');
            $cityTranslationModel  = new CityTranslationModel();
            $cityList = $cityTranslationModel
                ->select(CityTranslationModel::table() . '.*')
                ->join(CityModel::table() . ' as cityTable', 'cityTable.city_id = ' . CityTranslationModel::table() . '.city_id')
                ->where('cityTable.country_id', $this->request->getVar('country_id'))
                ->where('lang_code', 'ru')
                ->get()
                ->getResultArray();

            $cityData = map($cityList, 'city_id', 'title');

            return $this->respond([
                'res' => true,
                'html' => view('Admin\city\city_list_options', ['cityData' => $cityData]),
                'csrf' => csrf_hash()
            ]);
        }
    }
}
