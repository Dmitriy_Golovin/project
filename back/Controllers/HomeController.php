<?php

namespace Admin\Controllers;

use Admin\Controllers\BaseController;
use Common\Models\UserModel;
use Common\Models\HolidayModel;
use Common\Models\CountryModel;
use Common\Models\CityModel;
use Common\Models\FileModel;
use Common\Models\ChatModel;
use Common\Models\MessageModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;

class HomeController extends BaseController
{
    public function index()
    {
        $filterData = $this->request->data;
        $userModel = new UserModel;
        $holidayModel = new HolidayModel();
        $countryModel = new CountryModel();
        $cityModel = new CityModel();
        $fileModel = new FileModel();
        $chatModel = new ChatModel();
        $messageModel = new MessageModel();

        return view('Admin\Views\home\index', [
            'title' => 'Административная панель',
            'userData' => $filterData,
            'userCount' => $userModel->countAllResults(),
            'holidayCount' => $holidayModel->countAllResults(),
            'countryCount' => $countryModel->countAllResults(),
            'cityCount' => $cityModel->countAllResults(),
            'fileCount' => $fileModel->countAllResults(),
            'chatCount' => $chatModel->countAllResults(),
            'messageCount' => $messageModel->countAllResults(),
        ]);
    }
}
