<?php

namespace Admin\Controllers;

use App\Controllers\BaseController;
use Admin\Models\article_section\ArticleSectionSearchModel;
use Admin\Models\article_section\SaveArticleSectionForm;
use Common\Models\ArticleSectionModel;
use Common\Models\FileModel;

class ArticleSectionController extends BaseController
{
    public function index()
    {
        $filterData = $this->request->data;
        $model = new ArticleSectionSearchModel();
        $model->fill($this->request->getGet());
        $searchResult = $model->search();

        return view('Admin\Views\article-section\index', array_merge($searchResult, [
            'title' => 'Разделы статей',
            'userData' => $filterData,
        ]));
    }

    public function details($id)
    {
        $filterData = $this->request->data;
        $model = new ArticleSectionModel();
        $articleSection = $model
            ->select(ArticleSectionModel::table() . '.*')
            ->select('fileTable.file_id, fileTable.url, fileTable.url as file, fileTable.title as file_title, fileTable.source as file_link')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_ARTICLE_SECTION . '" and fileTable.item_id = ' . ArticleSectionModel::table() . '.article_section_id', 'left')
            ->where(ArticleSectionModel::table() . '.article_section_id', $id)
            ->withDeleted()
            ->first();

        if (empty($articleSection)) {
            return view('Admin\Views\templates\not_found_entity', [
                'title' => 'Раздел статей',
                'userData' => $filterData,
                'notFoundText' => 'Раздел с id ' . $id . ' не найден',
            ]);
        }

        return view('Admin\Views\article-section\details', [
            'title' => 'Раздел статей',
            'userData' => $filterData,
            'model' => $model,
            'result' => $articleSection,
        ]);
    }

    public function create()
    {
        helper('text');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $model = new SaveArticleSectionForm();
        
        if ($this->request->getMethod() == 'post') {
            $model->fill($this->request->getPost());
            $model->file = $this->request->getFile('file');
            $model->scenario = $model::SCENARIO_CREATE;

            if ($model->validate() && $model->save()) {
                $session->destroy();

                return redirect()->to('/admin/article-sections/details/' . $model->article_section_id);
            } else {
                $session->setFlashdata('redirectError', $model->getFirstError());
                $model->file = null;

                return view('Admin\Views\article-section/create' , [
                    'title' => 'Добавить праздник',
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            }
        }

        return view('Admin\Views\article-section\create', [
            'title' => 'Добавить раздел для статей',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    public function edit($articleSectionId)
    {
        helper('validation');
        helper('text');
        $session = \Config\Services::session();
        $filterData = $this->request->data;
        $articleSectionModel = new ArticleSectionModel();
        $model = new SaveArticleSectionForm();
        $section = $articleSectionModel
            ->select(ArticleSectionModel::table() . '.*')
            ->select('fileTable.file_id, fileTable.url as fileUrl, fileTable.title as file_title, fileTable.source as file_link')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_ARTICLE_SECTION . '" and fileTable.item_id = ' . ArticleSectionModel::table() . '.article_section_id', 'left')
            ->where(ArticleSectionModel::table() . '.article_section_id', $articleSectionId)
            ->first();

        $model->fill((array)$section);

        if ($this->request->getMethod() == 'post') {
            $model->fill($this->request->getPost());
            $model->file = $this->request->getFile('file');
            $model->scenario = $model::SCENARIO_EDIT;

            if ($model->validate() && $model->save()) {
                $session->destroy();

                return redirect()->to('/admin/article-sections/details/' . $model->article_section_id);
            } else {
                $session->setFlashdata('redirectError', $model->getFirstError());
                $model->file = null;

                return view('Admin\Views\article-section/create' , [
                    'title' => 'Редактировать раздел для статей',
                    'userData' => $filterData,
                    'model' => $model,
                ]);
            }
        }

        return view('Admin\Views\article-section\edit', [
            'title' => 'Редактировать раздел для статей',
            'userData' => $filterData,
            'model' => $model,
        ]);
    }

    public function delete($articleSectionId)
    {
        $articleSectionModel = new ArticleSectionModel();
        $articleSection = $articleSectionModel
            ->where('article_section_id', $articleSectionId)
            ->withDeleted()
            ->first();

        if (empty($articleSection)) {
            return redirect()->back()->with('redirectError', lang('ArticleSection.Article section with id {0} not found', [$articleSectionId]));
        }

        if (!$articleSectionModel->deleteFiles($articleSection->article_section_id)
            || !$articleSectionModel->delete($articleSection->article_section_id)) {
            return redirect()->back()->with('redirectError', lang('ArticleSection.Failed to delete article section'));
        } else {
            return redirect()->to('/admin/article-sections');
        }
    }
}
