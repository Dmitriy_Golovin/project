<?php

use App\Libraries\DateAndTime\DateAndTime;

helper('html');

?>

<!DOCTYPE HTML>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<?= csrf_meta() ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title><?= esc($title); ?></title>
	<link rel="shortcut icon" href="/img/favicon.ico" type="image/ico">
	<?= link_tag('css/admin_page.css', 'stylesheet', 'text/css'); ?>
</head>
<body>
 	<header>
		<div class="header_container">
			<div class="logo_container">
				<a href="/admin">
					<img alt="Home page" class="logo_img" src="/img/logo.png">
				</a>
			</div>
			<div class="menu_block header_col">
				<ul class="menu_main">
					<li class="menu_user_item <?= ($userData['activeItem'] === 'users') ? ' menu_user_item_active' : '' ?>">

						<?php if ($userData['activeItem'] !== 'users') : ?>

							<a href="/admin/users">

						<?php endif ?>

							Пользователи

						<?php if ($userData['activeItem'] !== 'users') : ?>
							
							</a>

						<?php endif ?>

					</li>
					<li class="menu_user_item <?= ($userData['activeItem'] === 'holidays') ? ' menu_user_item_active' : '' ?>">

						<?php if ($userData['activeItem'] !== 'holidays') : ?>

							<a href="/admin/holidays">

						<?php endif ?>

							Праздники

						<?php if ($userData['activeItem'] !== 'holidays') : ?>

							</a>

						<?php endif ?>

					</li>
					<li class="menu_user_item menu_user_item_exist_submenu <?= (in_array($userData['activeSubItem'], ['articles', 'article-sections']) ) ? ' menu_user_item_active' : '' ?>" data-active_submenu="0">
						Статьи
						<ul class="menu_user_item_submenu_container">
							<li class="active_burger_submenu"><a href="/admin/article-sections">Разделы статей</a></li>
							<li class="active_burger_submenu"><a href="/admin/articles">Статьи</a></li>
						</ul>
					</li>
					<li class="menu_user_item <?= ($userData['activeItem'] === 'countries') ? ' menu_user_item_active' : '' ?>">
						<?php if ($userData['activeItem'] !== 'countries') : ?>

							<a href="/admin/countries">

						<?php endif ?>

							Страны

						<?php if ($userData['activeItem'] !== 'countries') : ?>

							</a>

						<?php endif ?>

					</li>
					<li class="menu_user_item <?= ($userData['activeItem'] === 'cities') ? ' menu_user_item_active' : '' ?>">

						<?php if ($userData['activeItem'] !== 'cities') : ?>

							<a href="/admin/cities">
						
						<?php endif ?>

							Города

						<?php if ($userData['activeItem'] !== 'cities') : ?>

							</a>

						<?php endif ?>

					</li>
					<li class="menu_user_item menu_user_item_exist_submenu <?= (in_array($userData['activeSubItem'], ['images', 'videos', 'documents']) ) ? ' menu_user_item_active' : '' ?>" data-active_submenu="0">
						Файлы
						<ul class="menu_user_item_submenu_container">
							<li class="active_burger_submenu"><a href="/admin/images">Изображения</a></li>
							<li class="active_burger_submenu"><a href="/admin/videos">Видео</a></li>
							<li class="active_burger_submenu"><a href="/admin/documents">Документы</a></li>
						</ul>
					</li>
					<li class="menu_user_item <?= ($userData['activeItem'] === 'chats') ? ' menu_user_item_active' : '' ?>">

						<?php if ($userData['activeItem'] !== 'chats') : ?>

							<a href="/admin/chats">

						<?php endif ?>

							Чаты

						<?php if ($userData['activeItem'] !== 'chats') : ?>

							</a>

						<?php endif ?>

					</li>
					<li class="menu_user_item <?= ($userData['activeItem'] === 'dialog-messages') ? ' menu_user_item_active' : '' ?>">
						<?php if ($userData['activeItem'] !== 'dialog-messages') : ?>

							<a href="/admin/dialog-messages">

						<?php endif ?>

							Личные сообщения

						<?php if ($userData['activeItem'] !== 'dialog-messages') : ?>

							</a>

						<?php endif ?>
						
					</li>
					<li class="menu_user_item <?= ($userData['activeItem'] === 'visit-logs') ? ' menu_user_item_active' : '' ?>">
						<?php if ($userData['activeItem'] !== 'visit-logs') : ?>

							<a href="/admin/visit-logs">

						<?php endif ?>

							Логи посещений

						<?php if ($userData['activeItem'] !== 'visit-logs') : ?>

							</a>

						<?php endif ?>
						
					</li>
					<li class="burger_menu not_active">
						<img class="burger_menu_icon" src="/img/icon_burger_menu_light.png">
					</li>
				</ul>
				<div class="burger_menu_header_box">
					<ul class="menu_burger">
					</ul>
				</div>
			</div>
			<div class="header_col admin_data_block">
				<div class="header_col admin_personal_data_block">
					<p class="admin_role" data-userID="<?= $userData['userID'] ?>">Администратор</p>
					<p class="box_pers_data_arrow">
						<img alt="menu_presonal_data" class="pers_data_arrow" src="/img/pers_data_arrow_light.png">
					</p>
					<div class="menu_box inside_menu_box">
						<div class="menu_box_wrapper inside_menu_box">
							<ul class="menu_box_list inside_menu_box">
								<a href="/profile/<?= $userData['userID'] ?>">
									<li class="profile_but menu_box_item inside_menu_box">Мой профиль</li>
								</a>
								<a href="/profile/change_personal_data">
									<li class="person_data_but menu_box_item inside_menu_box">Личные данные</li>
								</a>
			  					<a href="/logout"><li class="menu_box_item inside_menu_box">Выход</li></a>
	        				</ul>
		  				</div>
					</div>
				</div>
				<div class="header_col avatar_block"><img alt="Avatar" class="avatar" src="<?= $userData['pathToAva'] ?>"></div>
			</div>
		</div>
 	</header>
	<div class="content_wrapper">
		<?= $this->renderSection('content') ?>
	</div>
	<footer>
		<div class="footer_container">
			<div class="logo_container"><img alt="Home page" class="logo_img" src="/img/logo.png"></div>
			<div class="footer_menu_container">
				<div class="copyright footer_col"><p>&copy; <?= DateAndTime::now()->getYear() ?>, calendaric.fun</p></div>
			</div>
		</div>
	</footer>
	<script src="/js/BroadcastLS/BroadcastLSClass.js"></script>
  	<script src="/js/TabSync/TabSyncClass.js"></script>
	<script src="/js/adaptive_user_menu_.js"></script>
	<script src="/js/personalMenu.js"></script>
	<script src="/js/Admin/main_admin.js"></script>
	<?= $this->renderSection('script') ?>
</body>
</html>