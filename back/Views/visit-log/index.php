<?php

use Common\Widgets\view_list\ViewListWidget;
use App\Libraries\DateAndTime\DateAndTime;

helper('html');

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

<?php new ViewListWidget([
		'model' => $model,
		'result' => $searchResult,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Visit logs'),
					'active' => true,
				],
			]
		]),
        'title' => lang('Main.Visit logs'),

        'pager' => $pager,
        'columns' => [
        	'visit_log_id',

        	[
        		'label' => 'ip',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['ip'],
        			'fieldName' => 'ip',
        		])
        	],

        	[
        		'label' => 'user_agent',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['user_agent'],
        			'fieldName' => 'user_agent',
        		])
        	],

        	[
        		'label' => 'uri_path',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['uri_path'],
        			'fieldName' => 'uri_path',
        		]),
        	],

        	[
        		'label' => 'created_at',
        		'style' => 'max-width: 70px',
        		'value' => function ($item) {
        			return DateAndTime::getFullDateTime($item->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		},
        		'filter' => view_cell('\Common\Widgets\date_range_input\DateRangeInputWidget::renderBase', [
        			'labelValue' => $model::getLabels()['created_at'],
        			'fromFieldName' => 'date_from',
        			'toFieldName' => 'date_to',
        			'from' => $form->date_from,
        			'to' => $form->date_to,
        		])
        	],
        	[
        		'label' => 'buttonActions',
        		'style' => 'max-width: 30px',
        		'value' => function ($item) {
        			$viewButton = anchor('admin/visit-logs/details/' . $item->visit_log_id, img([
        					'src' => '/img/eye_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				return '<div class="admin_button_cell">' . $viewButton . '</div>';
        		}
        	],
        ],
    ]) ?>

    <div class="calendar_container">
		<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
	</div>

<?= $this->endSection() ?>

<?= $this->section('script'); ?>
	<script src="/js/Calendar/CalendarClass.js"></script>
	<script src="/js/Admin/visit_log/main_visit_log_list.js"></script>
<?= $this->endSection(); ?>