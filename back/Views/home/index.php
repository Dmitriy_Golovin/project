<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<div class="main_home_box">
		<div class="admin_list_item">
			<div class="admin_list_item_title">Пользователи</div>
			<div class="data_container">
				<div>всего: <?= $userCount ?></div>
			</div>
		</div>
		<div class="admin_list_item">
			<div class="admin_list_item_title">Праздники</div>
			<div class="data_container">
				<div>всего: <?= $holidayCount ?></div>
			</div>
		</div>
		<div class="admin_list_item">
			<div class="admin_list_item_title">Страны</div>
			<div class="data_container">
				<div>всего: <?= $countryCount ?></div>
			</div>
		</div>
		<div class="admin_list_item">
			<div class="admin_list_item_title">Города</div>
			<div class="data_container">
				<div>всего: <?= $cityCount ?></div>
			</div>
		</div>
		<div class="admin_list_item">
			<div class="admin_list_item_title">Файлы</div>
			<div class="data_container">
				<div>всего: <?= $fileCount ?></div>
			</div>
		</div>
		<div class="admin_list_item">
			<div class="admin_list_item_title">Чаты</div>
			<div class="data_container">
				<div>всего: <?= $chatCount ?></div>
			</div>
		</div>
		<div class="admin_list_item">
			<div class="admin_list_item_title">Личные сообщения</div>
			<div class="data_container">
				<div>всего: <?= $messageCount ?></div>
			</div>
		</div>
	</div>

<?= $this->endSection() ?>