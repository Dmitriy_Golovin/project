<?php

use Common\Models\CountryTranslationModel;
use Common\Models\UserModel;

$countryTranslationModel = new CountryTranslationModel();
helper('arraySelf');

$countryList = $countryTranslationModel
	->where('lang_code', 'ru')
	->get()
	->getResultArray();

$countryList = map($countryList, 'country_id', 'title');

?>

<form action="<?= current_url() ?>" method="post">

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->first_name,
		'fieldName' => 'first_name',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->last_name,
		'fieldName' => 'last_name',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->email,
		'fieldName' => 'email',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->password,
		'fieldName' => 'password',
		'type' => 'password',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->repeat_pessword,
		'fieldName' => 'repeat_password',
		'type' => 'password',
	]) ?>

	<?= view_cell('\Common\Widgets\select_input\SelectInputWidget::renderBase', [
		'labelValue' => $model->country_id,
		'fieldName' => 'country_id',
		'data' => $countryList,
	]) ?>

	<?= view_cell('\Common\Widgets\select_input\SelectInputWidget::renderBase', [
		'labelValue' => $model->city_id,
		'fieldName' => 'city_id',
		'data' => [],
		'selectedIndex' => $model->city_id,
	]) ?>

	<?= view_cell('\Common\Widgets\select_input\SelectInputWidget::renderBase', [
		'labelValue' => $model->role,
		'fieldName' => 'role',
		'data' => UserModel::getRoleLabels(),
	]) ?>

	<?= view_cell('\Common\Widgets\select_input\SelectInputWidget::renderBase', [
		'labelValue' => $model->is_blocked,
		'fieldName' => 'is_blocked',
		'data' => UserModel::getBlockedStatusLabels(),
	]) ?>

	<span class="admin_base_submit_button"><?= lang('Main.save') ?></span>

</form>