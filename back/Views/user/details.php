<?php

use Common\Widgets\details_view\ViewDetailsWidget;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

$actions = [];

$actions[] = [
	'label' => 'Редактировать',
	'url' => '/admin/users/edit/' . $result->user_id,
	'cssClass' => 'edit_entity_button',
];

if ((int)$result->is_blocked === (int)$model::STATUS_NOT_BLOCKED) {
	$actions[] = [
		'label' => 'Блокировать',
		'url' => '/admin/users/block/' . $result->user_id,
		'cssClass' => 'block_entity_button',
	];
} else {
	$actions[] = [
		'label' => 'Разблокировать',
		'url' => '/admin/users/unblock/' . $result->user_id,
		'cssClass' => 'create_entity_button',
	];
}

if (is_null($result->userDeleted)) {
	$actions[] = [
		'label' => 'Удалить',
		'url' => '/admin/users/delete/' . $result->user_id,
		'cssClass' => 'delete_entity_button',
	];
} else {
	$actions[] = [
		'label' => 'Восстановить',
		'url' => '/admin/users/restore/' . $result->user_id,
		'cssClass' => 'create_entity_button',
	];
}

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<?php new ViewDetailsWidget([
		'model' => $model,
		'result' => $result,
		'title' => lang('Main.User') . ': ' . $result->first_name . ' ' . $result->last_name,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Users'),
					'url' => '/admin/users',
				],
				[
					'label' => $result->user_id,
					'active' => true,
				],
			]
		]),
		'actions' => $actions,
		'items' => [
			'user_id',
			'first_name',
			'last_name',
			'country',
			'city',
			'email',
			'about_self',

			[
        		'label' => 'path_to_ava',
        		'value' => function () use ($result) {
        			if (empty($result->url)) {
        				return view_cell('\Common\Widgets\upload_file_input\UploadFileInputWidget::renderBase', [
        					'action' => '/admin/users/upload-avatar/' . $result->user_id,
        					'fieldName' => 'avatar',
        					'id' => 'admin_user_upload_avatar',
        				]);
        			}

        			return view_cell('\Common\Widgets\file\ImageFileWidget::renderAdminPreview', [
						'file' => (object)[
							'file_id' => $result->file_id,
							'url' => FileModel::preparePathAdminToUserFile($result->url, FileModel::TYPE_IMAGE),
							'type' => FileModel::TYPE_IMAGE,
						],
						'deleteBlock' => true,
						'cssClass' => ' admin_details_delete_avatar'
					]);
        		}
        	],

        	[
        		'label' => 'role',
        		'value' => function ($result) use ($model) {
        			return $model::getRoleLabels()[$result->role];
        		},
        	],

        	[
        		'label' => 'is_blocked',
        		'value' => function ($result) use ($model) {
        			return $model->getBlockedStatusLabels()[$result->is_blocked];
        		}
        	],

        	[
        		'label' => 'userDeleted',
        		'value' => function ($result) use ($model) {
        			$value = (is_null($result->userDeleted))
        				? $model::ACCOUNT_IS_NOT_DELETED
        				: $model::ACCOUNT_IS_DELETED;

        			return $model->getAccountDeletedLabel()[$value];
        		}
        	],

        	[
        		'label' => 'created_at',
        		'value' => function ($result) {
        			return DateAndTime::getFullDate($result->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		}
        	]
		],
	]) ?>

	<?php
	if (!empty($result->file_id)) {
		$this->setVar('mainModalNoticeCssClass', ' admin_delete_avatar');
		$this->setVar('titleNotice', lang('Main.Are you sure you want to delete your avatar?'));
		$this->setVar('acceptNoticeAnchor', '/admin/users/delete-avatar/' . $result->user_id . '/' . $result->file_id);
		echo $this->include('modal/notice');
	}
	?>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/Admin/upload_file_input.js"></script>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/user/main_user_details.js"></script>
<?= $this->endSection() ?>