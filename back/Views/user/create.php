<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<div class="admin_create_edit_block">

		<?= view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Users'),
					'url' => '/admin/users',
				],
				[
					'label' => lang('Main.Add user'),
					'active' => true,
				],
			]
		]) ?>

		<div class="admin_create_edit_container">

			<h2 class="admin_list_title"><?= lang('Main.Add user') ?></h2>

			<?= $this->include('Admin\Views\user\form') ?>

		</div>

	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/user/main_user_save.js"></script>
<?= $this->endSection() ?>