<?php

use Common\Models\FileModel;
use Common\Widgets\view_list\ViewListWidget;
use App\Libraries\DateAndTime\DateAndTime;

helper('html');

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?php new ViewListWidget([
		'model' => $model,
		'result' => $searchResult,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Users'),
					'active' => true,
				],
			]
		]),
        'title' => lang('Main.Users'),
        'actions' => [
        	[
        		'label' => 'Добавить пользователя',
        		'url' => '/admin/users/create',
        		'cssClass' => 'create_entity_button',
        	],
        ],
        'pager' => $pager,
        'columns' => [
        	'user_id',

        	[
        		'label' => 'first_name',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['first_name'],
        			'fieldName' => 'first_name',
        		])
        	],

        	[
        		'label' => 'last_name',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['last_name'],
        			'fieldName' => 'last_name',
        		])
        	],

        	[
        		'label' => 'country',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['country'],
        			'fieldName' => 'country',
        		]),
        	],

        	[
        		'label' => 'email',
        		'style' => 'max-width: 200px',
        	],

        	[
        		'label' => 'path_to_ava',
        		'value' => function ($item) {
        			if (empty($item->path_to_ava)) {
        				return null;
        			}

        			return view_cell('\Common\Widgets\file\ImageFileWidget::renderAdminPreview', [
						'file' => (object)[
							'file_id' => $item->file_id,
							'url' => FileModel::preparePathAdminToUserFile($item->path_to_ava, FileModel::TYPE_IMAGE),
							'type' => FileModel::TYPE_IMAGE,
						],
						'deleteBlock' => false,
					]);
        		}
        	],

        	[
        		'label' => 'role',
        		'style' => 'max-width: 70px',
        		'value' => function ($item) use ($model) {
        			return $model::getRoleLabels()[$item->role];
        		},
        		'filter' => view_cell('\Common\Widgets\select_input\SelectInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['role'],
        			'fieldName' => 'role',
        			'data' => $model::getRoleLabels(),
        		]),
        	],

        	[
        		'label' => 'is_blocked',
        		'style' => 'max-width: 80px',
        		'value' => function ($item) use ($model) {
        			return $model->getBlockedStatusLabels()[$item->is_blocked];
        		},
        		'filter' => view_cell('\Common\Widgets\select_input\SelectInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['is_blocked'],
        			'fieldName' => 'is_blocked',
        			'data' => $model::getBlockedStatusLabels(),
        		]),
        	],

        	[
        		'label' => 'deleted_at',
        		'style' => 'max-width: 60px',
        		'value' => function($item) use ($model) {
        			$value = (is_null($item->deleted_at))
        				? $model::ACCOUNT_IS_NOT_DELETED
        				: $model::ACCOUNT_IS_DELETED;

        			return $model->getAccountDeletedLabel()[$value];
        		},
        		'filter' => view_cell('\Common\Widgets\select_input\SelectInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['deleted_at'],
        			'fieldName' => 'deleted_at',
        			'data' => $model::getAccountDeletedLabel(),
        		]),
        	],

        	[
        		'label' => 'created_at',
        		'style' => 'max-width: 70px',
        		'value' => function ($item) {
        			return DateAndTime::getFullDate($item->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		},
        		'filter' => view_cell('\Common\Widgets\date_range_input\DateRangeInputWidget::renderBase', [
        			'labelValue' => $model::getLabels()['created_at'],
        			'fromFieldName' => 'date_from',
        			'toFieldName' => 'date_to',
        			'from' => $form->date_from,
        			'to' => $form->date_to,
        		])
        	],
        	[
        		'label' => 'buttonActions',
        		'style' => 'max-width: 30px',
        		'value' => function ($item) {
        			$viewButton = anchor('admin/users/details/' . $item->user_id, img([
        					'src' => '/img/eye_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				$editButton = anchor('admin/users/edit/' . $item->user_id, img([
        					'src' => '/img/pencil_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				return '<div class="admin_button_cell">' . $viewButton . $editButton . '</div>';
        		}
        	],
        ],
    ]) ?>

    <div class="calendar_container">
		<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/Calendar/CalendarClass.js"></script>
	<script src="/js/Admin/user/main_user_list.js"></script>
<?= $this->endSection() ?>