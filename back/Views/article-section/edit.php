<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<div class="admin_create_edit_block">

		<?= view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Article sections'),
					'url' => '/admin/article-sections',
				],
				[
					'label' => $model->article_section_id,
					'url' => '/admin/article-sections/details/' . $model->article_section_id,
				],
				[
					'label' => lang('Main.Edit a section for articles'),
					'active' => true,
				],
			]
		]) ?>

		<div class="admin_create_edit_container">

			<h2 class="admin_list_title"><?= lang('Main.Edit a section for articles') ?></h2>

			<?= $this->include('Admin\Views\article-section\form') ?>

		</div>

	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/article_section/main_article_section_save.js"></script>
<?= $this->endSection() ?>