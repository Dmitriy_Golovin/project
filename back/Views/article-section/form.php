<?php

use Common\Models\ArticleSectionModel;

helper('arraySelf');

?>

<form action="<?= current_url() ?>" method="post" enctype="multipart/form-data">

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->title,
		'fieldName' => 'title',
	]) ?>

	<?= view_cell('\Common\Widgets\textarea_input\TextareaInputWidget::renderBase', [
		'labelValue' => $model->description,
		'fieldName' => 'description',
		'rows' => 8,
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->meta_description,
		'fieldName' => 'meta_description',
	]) ?>

	<?= view_cell('\Common\Widgets\upload_file_input\UploadFileInputWidget::renderImage', [
		'labelValue' => $model->file,
		'fieldName' => 'file',
		'existUrl' => $model->fileUrl,
		'existFileId' => $model->file_id,
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->file_title,
		'fieldName' => 'file_title',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->file_link,
		'fieldName' => 'file_link',
	]) ?>

	<span class="admin_base_submit_button"><?= lang('Main.save') ?></span>

</form>