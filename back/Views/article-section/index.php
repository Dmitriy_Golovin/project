<?php

use Common\Models\FileModel;
use Common\Widgets\view_list\ViewListWidget;
use App\Libraries\DateAndTime\DateAndTime;

helper('html');

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?php new ViewListWidget([
		'model' => $model,
		'result' => $searchResult,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Article sections'),
					'active' => true,
				],
			]
		]),
        'title' => lang('Main.Article sections'),
        'actions' => [
        	[
        		'label' => 'Добавить раздел для статей',
        		'url' => '/admin/article-sections/create',
        		'cssClass' => 'create_entity_button',
        	],
        ],
        'pager' => $pager,
        'columns' => [
        	'article_section_id',

        	[
        		'label' => 'title',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['title'],
        			'fieldName' => 'title',
        		])
        	],

        	[
        		'label' => 'description',
        		'value' => function ($item) {
        			if (mb_strlen($item->description) > 150) {
        				return mb_substr($item->description, 0, 150) . '...';
        			} else {
        				return $item->description;
        			}
        		},
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['description'],
        			'fieldName' => 'description',
        		])
        	],

        	[
        		'label' => 'meta_description',
        		'style' => 'max-width: 300px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['meta_description'],
        			'fieldName' => 'meta_description',
        		])
        	],

        	[
        		'label' => 'file',
        		'value' => function ($item) {
        			if (empty($item->file)) {
        				return null;
        			}

        			return view_cell('\Common\Widgets\file\ImageFileWidget::renderAdminPreview', [
						'file' => (object)[
							'file_id' => $item->file_id,
							'url' => FileModel::preparePathAdminToUserFile($item->file, FileModel::TYPE_IMAGE),
							'type' => FileModel::TYPE_IMAGE,
						],
						'deleteBlock' => false,
					]);
        		}
        	],

        	[
        		'label' => 'created_at',
        		'style' => 'max-width: 70px',
        		'value' => function ($item) {
        			return DateAndTime::getFullDate($item->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		},
        		'filter' => view_cell('\Common\Widgets\date_range_input\DateRangeInputWidget::renderBase', [
        			'labelValue' => $model::getLabels()['created_at'],
        			'fromFieldName' => 'date_from',
        			'toFieldName' => 'date_to',
        			'from' => $form->date_from,
        			'to' => $form->date_to,
        		])
        	],
        	[
        		'label' => 'buttonActions',
        		'style' => 'max-width: 30px',
        		'value' => function ($item) {
        			$viewButton = anchor('admin/article-sections/details/' . $item->article_section_id, img([
        					'src' => '/img/eye_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				$editButton = anchor('admin/article-sections/edit/' . $item->article_section_id, img([
        					'src' => '/img/pencil_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				return '<div class="admin_button_cell">' . $viewButton . $editButton . '</div>';
        		}
        	],
        ],
    ]) ?>

	<div class="calendar_container">
		<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/Calendar/CalendarClass.js"></script>
	<script src="/js/Admin/article_section/main_article_section_list.js"></script>
<?= $this->endSection() ?>