<?php

use Common\Models\CountryTranslationModel;
use Common\Models\HolidayModel;

$countryTranslationModel = new CountryTranslationModel();
helper('arraySelf');

$countryList = $countryTranslationModel
	->where('lang_code', 'ru')
	->get()
	->getResultArray();

$countryList = map($countryList, 'country_id', 'title');

?>

<form action="<?= current_url() ?>" method="post" enctype="multipart/form-data">

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->holiday_day,
		'fieldName' => 'holiday_day',
	]) ?>

	<?= view_cell('\Common\Widgets\textarea_input\TextareaInputWidget::renderBase', [
		'labelValue' => $model->description,
		'fieldName' => 'description',
		'rows' => 12,
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->holiday_date,
		'fieldName' => 'holiday_date',
	]) ?>

	<?= view_cell('\Common\Widgets\select_input\SelectInputWidget::renderBase', [
		'labelValue' => $model->country_id,
		'fieldName' => 'country_id',
		'data' => $countryList,
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->link,
		'fieldName' => 'link',
	]) ?>

	<?= view_cell('\Common\Widgets\upload_file_input\UploadFileInputWidget::renderImage', [
		'labelValue' => $model->file,
		'fieldName' => 'file',
		'existUrl' => $model->fileUrl,
		'existFileId' => $model->file_id,
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->file_title,
		'fieldName' => 'file_title',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->file_link,
		'fieldName' => 'file_link',
	]) ?>

	<span class="admin_base_submit_button"><?= lang('Main.save') ?></span>

</form>