<?php

use Common\Widgets\view_list\ViewListWidget;
use App\Libraries\DateAndTime\DateAndTime;

helper('html');

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

<?php new ViewListWidget([
		'model' => $model,
		'result' => $searchResult,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Holidays'),
					'active' => true,
				],
			]
		]),
        'title' => lang('Main.Holidays'),
        'actions' => [
        	[
        		'label' => 'Добавить праздник',
        		'url' => '/admin/holidays/create',
        		'cssClass' => 'create_entity_button',
        	],
        ],
        'pager' => $pager,
        'columns' => [
        	'holiday_id',

        	[
        		'label' => 'holiday_day',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['holiday_day'],
        			'fieldName' => 'holiday_day',
        		])
        	],

        	[
        		'label' => 'description',
        		'value' => function ($item) {
        			if (mb_strlen($item->description) > 150) {
        				return mb_substr($item->description, 0, 150) . '...';
        			} else {
        				return $item->description;
        			}
        		},
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['description'],
        			'fieldName' => 'description',
        		])
        	],

        	[
        		'label' => 'country',
        		'style' => 'max-width: 200px',
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['country'],
        			'fieldName' => 'country',
        		]),
        	],

        	'holiday_date',
        	
        	[
        		'label' => 'link',
        		'style' => 'max-width: 120px',
        	],

        	[
        		'label' => 'created_at',
        		'style' => 'max-width: 70px',
        		'value' => function ($item) {
        			return DateAndTime::getFullDate($item->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		},
        		'filter' => view_cell('\Common\Widgets\date_range_input\DateRangeInputWidget::renderBase', [
        			'labelValue' => $model::getLabels()['created_at'],
        			'fromFieldName' => 'date_from',
        			'toFieldName' => 'date_to',
        			'from' => $form->date_from,
        			'to' => $form->date_to,
        		])
        	],
        	[
        		'label' => 'buttonActions',
        		'style' => 'max-width: 30px',
        		'value' => function ($item) {
        			$viewButton = anchor('admin/holidays/details/' . $item->holiday_id, img([
        					'src' => '/img/eye_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				$editButton = anchor('admin/holidays/edit/' . $item->holiday_id, img([
        					'src' => '/img/pencil_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				return '<div class="admin_button_cell">' . $viewButton . $editButton . '</div>';
        		}
        	],
        ],
    ]) ?>

    <div class="calendar_container">
		<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
	</div>

<?= $this->endSection() ?>

<?= $this->section('script'); ?>
	<script src="/js/Calendar/CalendarClass.js"></script>
	<script src="/js/Admin/holiday/main_holiday_list.js"></script>
<?= $this->endSection(); ?>