<?php

use Common\Widgets\details_view\ViewDetailsWidget;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

$actions = [];

$actions[] = [
	'label' => 'Редактировать',
	'url' => '/admin/holidays/edit/' . $result->holiday_id,
	'cssClass' => 'edit_entity_button',
];

$actions[] = [
	'label' => 'Удалить',
	// 'url' => '/admin/holidays/delete/' . $result->holiday_id,
	'cssClass' => 'delete_entity_button',
];

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<?php new ViewDetailsWidget([
		'model' => $model,
		'result' => $result,
		'title' => lang('Main.Holiday') . ': ' . $result->holiday_day,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Holidays'),
					'url' => '/admin/holidays',
				],
				[
					'label' => $result->holiday_id,
					'active' => true,
				],
			]
		]),
		'actions' => $actions,
		'items' => [
			'holiday_id',
			'holiday_day',
			'description',
			'holiday_date',
			'country',
			'link',

			[
        		'label' => 'file',
        		'value' => function () use ($result) {
        			if (empty($result->url)) {
        				return null;
        			}

        			return view_cell('\Common\Widgets\file\ImageFileWidget::renderAdminPreview', [
						'file' => (object)[
							'file_id' => $result->file_id,
							'url' => FileModel::preparePathAdminToUserFile($result->url, FileModel::TYPE_IMAGE),
							'type' => FileModel::TYPE_IMAGE,
						],
						'deleteBlock' => false,
						// 'cssClass' => ' admin_details_delete_file_holiday'
					]);
        		}
        	],

        	'file_title',
        	'file_link',

        	[
        		'label' => 'created_at',
        		'value' => function ($result) {
        			return DateAndTime::getFullDate($result->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		}
        	]
		],
	]) ?>


	<?php $this->setVar('mainModalNoticeCssClass', ' admin_delete_holiday'); ?>
	<?php $this->setVar('titleNotice', lang('Main.Are you sure you want to delete holiday?')); ?>
	<?php $this->setVar('acceptNoticeAnchor', '/admin/holidays/delete/' . $result->holiday_id); ?>
	<?= $this->include('modal/notice'); ?>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/Admin/upload_file_input.js"></script>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/holiday/main_holiday_details.js"></script>
<?= $this->endSection() ?>