<?php

use Common\Models\FileModel;
use Common\Widgets\view_list\ViewListWidget;
use App\Libraries\DateAndTime\DateAndTime;

helper('html');
helper('arraySelf');

$articleSectionList = $model::getArticleSectionIdTitle();

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?php new ViewListWidget([
		'model' => $model,
		'result' => $searchResult,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Articles'),
					'active' => true,
				],
			]
		]),
        'title' => lang('Main.Articles'),
        'actions' => [
        	[
        		'label' => 'Добавить статью',
        		'url' => '/admin/articles/create',
        		'cssClass' => 'create_entity_button',
        	],
        ],
        'pager' => $pager,
        'columns' => [
        	'article_id',

        	[
        		'label' => 'article_section_id',
        		'style' => 'max-width: 200px',
        		'value' => function ($item) use ($model, $articleSectionList) {
        			return map($articleSectionList, 'article_section_id', 'title')[$item->article_section_id];
        		},
        		'filter' => view_cell('\Common\Widgets\select_input\SelectInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['article_section_id'],
        			'fieldName' => 'article_section_id',
        			'data' => map($articleSectionList, 'article_section_id', 'title'),
        		]),
        	],

        	'title',

        	[
        		'label' => 'description',
        		'value' => function ($item) {
        			if (mb_strlen(esc($item->description)) > 150) {
        				return mb_substr(esc($item->description), 0, 150) . '...';
        			} else {
        				return esc($item->description);
        			}
        		},
        		'filter' => view_cell('\Common\Widgets\text_input\TextInputWidget::renderFilter', [
        			'labelValue' => $model::getLabels()['description'],
        			'fieldName' => 'description',
        		])
        	],

        	'meta_description',

        	[
        		'label' => 'status',
        		'value' => function ($item) use ($model) {
        			return $model::getStatusLabels()[$item->status];
        		}
        	],

        	[
        		'label' => 'created_at',
        		'style' => 'max-width: 70px',
        		'value' => function ($item) {
        			return DateAndTime::getFullDate($item->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		},
        		'filter' => view_cell('\Common\Widgets\date_range_input\DateRangeInputWidget::renderBase', [
        			'labelValue' => $model::getLabels()['created_at'],
        			'fromFieldName' => 'date_from',
        			'toFieldName' => 'date_to',
        			'from' => $form->date_from,
        			'to' => $form->date_to,
        		])
        	],
        	[
        		'label' => 'buttonActions',
        		'style' => 'max-width: 30px',
        		'value' => function ($item) {
        			$viewButton = anchor('admin/articles/details/' . $item->article_id, img([
        					'src' => '/img/eye_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				$editButton = anchor('admin/articles/edit/' . $item->article_id, img([
        					'src' => '/img/pencil_icon.png',
        					'class' => 'admin_button_cell_icon'
    					]), [
    					'class' => 'admin_button_anchor',
    				]);

    				return '<div class="admin_button_cell">' . $viewButton . $editButton . '</div>';
        		}
        	],
        ],
    ]) ?>

    <div class="calendar_container">
		<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/Calendar/CalendarClass.js"></script>
	<script src="/js/Admin/article/main_article_list.js"></script>
<?= $this->endSection() ?>