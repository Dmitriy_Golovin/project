<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<div class="admin_create_edit_block">

		<?= view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Articles'),
					'url' => '/admin/articles',
				],
				[
					'label' => lang('Main.Add an article'),
					'active' => true,
				],
			]
		]) ?>

		<div class="admin_create_edit_container">

			<h2 class="admin_list_title"><?= lang('Main.Add an article') ?></h2>

			<?= $this->include('Admin\Views\article\form') ?>

		</div>

	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/article/main_article_save.js"></script>
<?= $this->endSection() ?>