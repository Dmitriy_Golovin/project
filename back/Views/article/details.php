<?php

use Common\Widgets\details_view\ViewDetailsWidget;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

helper('arraySelf');

$articleSectionList = $model::getArticleSectionIdTitle();

$actions = [];

$actions[] = [
	'label' => 'Редактировать статью',
	'url' => '/admin/articles/edit/' . $result->article_id,
	'cssClass' => 'edit_entity_button',
];

if ((int)$result->status === $model::STATUS_NOT_PUBLISHED || (int)$result->status === $model::STATUS_DRAFT) {
	$actions[] = [
		'label' => 'Опубликовать',
		'url' => '/admin/articles/publish/' . $result->article_id,
		'cssClass' => 'edit_entity_button',
	];
}

if ((int)$result->status === $model::STATUS_PUBLISHED) {
	$actions[] = [
		'label' => 'Снять с публикации',
		'url' => '/admin/articles/unpublish/' . $result->article_id,
		'cssClass' => 'block_entity_button',
	];
}

$actions[] = [
	'label' => 'Добавить абзац',
	'url' => '/admin/articles/add-paragraph/' . $result->article_id,
	'cssClass' => 'create_entity_button',
];

$actions[] = [
	'label' => 'Добавить изображение',
	'url' => '/admin/articles/add-image/' . $result->article_id,
	'cssClass' => 'create_entity_button',
];

$actions[] = [
	'label' => 'Удалить',
	'cssClass' => 'delete_entity_button',
];

?>

<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<?php new ViewDetailsWidget([
		'model' => $model,
		'result' => $result,
		'title' => lang('Main.Article') . ': ' . $result->article_id,
		'breadCrumbs' => view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Articles'),
					'url' => '/admin/articles',
				],
				[
					'label' => $result->article_id,
					'active' => true,
				],
			]
		]),
		'actions' => $actions,
		'items' => [
			[
				'label' => 'article_section_id',
				'value' => function ($result) use ($model, $articleSectionList) {
        			return map($articleSectionList, 'article_section_id', 'title')[$result->article_section_id];
        		},
			],

			[
				'label' => 'status',
				'value' => function ($result) use ($model) {
					return $model::getStatusLabels()[$result->status];
				}
			],

			'title',
			'title_transliteration',
			
			[
				'label' => 'description',
				'value' => function ($result) {
					return '<div class="admin_article_description" data-article_id="' . $result->article_id . '">'
						. $result->description
						. '</div>';
				}
			],

			'meta_description',

        	[
        		'label' => 'created_at',
        		'value' => function ($result) {
        			return DateAndTime::getFullDate($result->created_at, config('AdminConfig')->adminTimeOffsetSeconds);
        		}
        	]
		],
	]) ?>


	<?php $this->setVar('mainModalNoticeCssClass', ' admin_delete_holiday'); ?>
	<?php $this->setVar('titleNotice', lang('Main.Are you sure you want to delete article?')); ?>
	<?php $this->setVar('acceptNoticeAnchor', '/admin/articles/delete/' . $result->article_id); ?>
	<?= $this->include('modal/notice'); ?>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/Admin/upload_file_input.js"></script>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/article/main_article_details.js"></script>
<?= $this->endSection() ?>