<?php

use Common\Models\ArticleSectionModel;
use Common\Models\ArticleModel;

helper('arraySelf');

?>

<form action="<?= current_url() ?>" method="post" enctype="multipart/form-data">

	<?= view_cell('\Common\Widgets\select_input\SelectInputWidget::renderBase', [
		'labelValue' => $model->article_section_id,
		'fieldName' => 'article_section_id',
		'data' => map(ArticleModel::getArticleSectionIdTitle(), 'article_section_id', 'title'),
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->title,
		'fieldName' => 'title',
	]) ?>

	<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
		'labelValue' => $model->meta_description,
		'fieldName' => 'meta_description',
	]) ?>

	<span class="admin_base_submit_button"><?= lang('Main.save') ?></span>

</form>