<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<div class="admin_create_edit_block">

		<?= view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Articles'),
					'url' => '/admin/article',
				],
				[
					'label' => $article->article_id,
					'url' => '/admin/articles/details/' . $article->article_id,
				],
				[
					'label' => lang('Main.Add paragraph'),
					'active' => true,
				],
			]
		]) ?>

		<div class="admin_create_edit_container">

			<h2 class="admin_list_title"><?= lang('Main.Add paragraph') ?></h2>

			<form action="<?= current_url() ?>" method="post" enctype="multipart/form-data">

				<select name="position" class="admin_base_field">

					<?php

					$i = 0;

					while ($i <= $article->paragraphCount) {
						if ($i === 0) {
							echo '<option value="' . $i . '">в начало</option>';
						} else if ($i === $article->paragraphCount && $i > 0) {
							echo '<option value="' . $i . '" selected>в конец</option>';
						} else {
							echo '<option value="' . $i . '">после ' . $i . ' абзаца</option>';
						}

						$i++;
					}

					?>

				</select>

				<?= view_cell('\Common\Widgets\textarea_input\TextareaInputWidget::renderBase', [
					'labelValue' => $model->description,
					'fieldName' => 'description',
					'rows' => 8,
				]) ?>

				<span class="admin_base_submit_button"><?= lang('Main.save') ?></span>

			</form>

		</div>

	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/article/main_article_save.js"></script>
<?= $this->endSection() ?>