<?= $this->extend('Admin\Views\layouts\main') ?>

<?= $this->section('content') ?>

	<?= $this->include('templates/error/flash_error') ?>

	<div class="admin_create_edit_block">

		<?= view_cell('\Common\Widgets\bread_crumbs\BreadCrumbsWidget::renderBase', [
			'items' => [
				[
					'label' => lang('Main.Articles'),
					'url' => '/admin/article',
				],
				[
					'label' => $article->article_id,
					'url' => '/admin/articles/details/' . $article->article_id,
				],
				[
					'label' => lang('Main.Add image'),
					'active' => true,
				],
			]
		]) ?>

		<div class="admin_create_edit_container">

			<h2 class="admin_list_title"><?= lang('Main.Add image') ?></h2>

			<form action="<?= current_url() ?>" method="post" enctype="multipart/form-data">

				<select name="position" class="admin_base_field">

					<?php

					$i = 0;

					while ($i <= $article->paragraphCount) {
						if ($i === 0) {
							echo '<option value="' . $i . '">в начало</option>';
						} else if ($i === $article->paragraphCount && $i > 0) {
							echo '<option value="' . $i . '" selected>в конец</option>';
						} else {
							echo '<option value="' . $i . '">после ' . $i . ' абзаца</option>';
						}

						$i++;
					}

					?>

				</select>

				<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
					'labelValue' => $model->file_title,
					'fieldName' => 'file_title',
				]) ?>

				<?= view_cell('\Common\Widgets\text_input\TextInputWidget::renderBase', [
					'labelValue' => $model->file_link,
					'fieldName' => 'file_link',
				]) ?>

				<?= view_cell('\Common\Widgets\upload_file_input\UploadFileInputWidget::renderImage', [
					'labelValue' => $model->file,
					'fieldName' => 'file',
					'existUrl' => $model->fileUrl,
					'existFileId' => $model->file_id,
				]) ?>

				<span class="admin_base_submit_button"><?= lang('Main.save') ?></span>

			</form>

		</div>

	</div>

<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/Admin/article/main_article_save.js"></script>
<?= $this->endSection() ?>