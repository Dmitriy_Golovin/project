<?php

namespace Admin\Models\holiday;

use Common\Forms\BaseForm;
use Common\Models\HolidayModel;
use Common\Models\HolidayDateModel;
use Common\Models\CountryTranslationModel;
use App\Libraries\DateAndTime\DateAndTime;

class HolidaySearchModel extends BaseForm
{
	public $holiday_day;
	public $description;
	public $date_from;
	public $date_to;
	public $country;

	protected $validationRules = [
		'holiday_day' => 'permit_empty|string',
		'description' => 'permit_empty|string',
		'country' => 'permit_empty|string',
		'date_from' => 'permit_empty|valid_date[d-m-Y]',
		'date_to' => 'permit_empty|valid_date[d-m-Y]',
	];

	public function search()
	{;
		$model = new HolidayModel();

		$holidays = $model
			->select(HolidayModel::table() . '.*')
			->select('countryTable.title as country')
			->select('holidayDateTable.holiday_date')
			->join(HolidayDateModel::table() . ' as holidayDateTable', 'holidayDateTable.holiday_id = ' . HolidayModel::table() . '.holiday_id')
			->join(CountryTranslationModel::table() . ' as countryTable', 'countryTable.country_id = ' . HolidayModel::table() . '.country_id and countryTable.lang_code = "ru"', 'left');

		if (!empty($this->holiday_day)) {
			$holidays->like('holiday_day', $this->holiday_day);
		}

		if (!empty($this->description)) {
			$holidays->like('description', $this->description);
		}

		if (!empty($this->country)) {
			$holidays->like('countryTable.title', $this->country);
		}

		if (!empty($this->date_from)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_from . '00:00');
			$holidays->where(HolidayModel::table() . '.created_at >=', $date->getTimestamp());
		}

		if (!empty($this->date_to)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_to . '00:00');
			$holidays->where(HolidayModel::table() . '.created_at <=', $date->getTimestamp() + DAY - 1);
		}
		
		$holidays = $holidays->orderBy(HolidayModel::table() . '.holiday_id', 'ASC')
			->withDeleted()
			->paginate(20, 'default');

		return [
			'searchResult' => $holidays,
			'pager' => $model->pager,
			'model' => $model,
			'form' => $this,
		];
	}
}