<?php

namespace Admin\Models\holiday;

use Common\Forms\BaseForm;
use Common\Models\HolidayModel;
use Common\Models\HolidayDateModel;
use Common\Models\FileModel;
use Common\Models\CountryModel;

class SaveHolidayForm extends BaseForm
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	public $holiday_id;
	public $holiday_day;
	public $description;
	public $holiday_date;
	public $fileUrl;
	public $file;
	public $country_id;
	public $file_id;
	public $file_link;
    public $file_title;
	public $link;
	public $delete_file;

	protected $validationRules = [
		'holiday_id' => 'permit_empty|integer',
		'holiday_day' => 'required|string|max_length[255]',
        'description' => 'required|string|max_length[65000]',
        'holiday_date' => 'required|valid_date[d-m]',
        'country_id' => 'required|integer',
        'delete_file' => 'required|integer|in_list[0,1]',
        'link' => 'permit_empty|valid_url',
        'file' => 'permit_empty|uploaded[file]|max_size[file,19000]|is_image[file]|ext_in[file,png,jpeg,jpg,gif]|mime_in[file,image/png,image/jpg,image/jpeg,image/gif]',
        'file_link' => 'permit_empty|valid_url',
        'file_title' => 'permit_empty|string|max_length[255]',
	];

	protected $cleanValidationRules = false;

	public function __construct()
	{
		parent::__construct();
		$this->labels = HolidayModel::getLabels();
	}

	public function save()
	{
		if (!empty($model->country_id)) {
            $countryModel = new CountryModel();
            $country = $countryModel->find($model->country_id);

            if (empty($country)) {
                $this->setError('', lang('Country.Country not found'));
                return false;
            }
        }

        $holidayModel = new HolidayModel();
        $holiday = [
        	'holiday_id' => $this->holiday_id,
        	'holiday_day' => $this->holiday_day,
        	'description' => $this->description,
        	'country_id' => $this->country_id,
        	'link' => $this->link,
        ];

        if (!$holidayModel->save($holiday)) {
        	$this->setError('', lang('Holiday.Failed to save holiday'));
        	return false;
        }

        $this->holiday_id = (is_null($this->holiday_id))
            ? $holidayModel->getInsertID()
            : $this->holiday_id;
        $holidayDateModel = new HolidayDateModel();
 		$holidayDateData = [
            'holiday_date' => $this->holiday_date,
            'holiday_id' => $this->holiday_id,
        ];

        if ($this->scenario === self::SCENARIO_EDIT) {
        	$holidayDate = $holidayDateModel
                ->where('holiday_id', $this->holiday_id)
                ->first();

            if (!empty($holidayDate)) {
                $holidayDateData['holiday_date_id'] = $holidayDate->holiday_date_id;
            }
        }

        if (!$holidayDateModel->save($holidayDateData)) {
            $this->setError('', lang('Holiday.Failed to save holiday date'));
            return false;
        }

        $fileModel = new FileModel();

        if ((int)$this->delete_file === 1 && $this->scenario === self::SCENARIO_EDIT) {
            $holiday = $holidayModel
                ->where('holiday_id', $this->holiday_id)
                ->withDeleted()
                ->first();

            if (!empty($holiday)) {
                $fileModel->delete($holiday->file_id);
            }
        }

        if (empty($this->file->getError())) {
            $fileDataDisk = FileModel::saveFileOnDisk($this->file, FileModel::TYPE_IMAGE);
            $fileData = [
                'type' => FileModel::TYPE_IMAGE,
                'related_item' => FileModel::RELATED_ITEM_HOLIDAY,
                'item_id' => $this->holiday_id,
                'url' => $fileDataDisk['fileName'],
                'privacy_status' => FileModel::PUBLIC_STATUS,
                'mime_type' => $fileDataDisk['mimeType'],
                'original_name' => $fileDataDisk['originalName'],
                'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
                'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
                'title' => !empty($this->file_title) ? $this->file_title : null,
                'source' => !empty($this->file_link) ? $this->file_link : null,
            ];

            if (!$fileModel->save($fileData)) {
                $this->setError('', lang('File.Failed to save file'));
                return false;
            }

            $holiday = $holidayModel->find($this->holiday_id);
            $holiday->file_id = $fileModel->getInsertID();

            if (!$holidayModel->save($holiday)) {
	        	$this->setError('', lang('Holiday.Failed to save holiday'));
	        	return false;
	        }
        }

        return true;
	}
}