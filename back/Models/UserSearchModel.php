<?php

namespace Admin\Models;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\CountryTranslationModel;
use Common\Models\CityTranslationModel;
use App\Libraries\DateAndTime\DateAndTime;

class UserSearchModel extends BaseForm
{
	public $first_name;
	public $last_name;
	public $country;
	public $role;
	public $is_blocked;
	public $date_from;
	public $date_to;
	public $deleted_at;

	protected $validationRules = [
		'first_name' => 'permit_empty|string',
		'last_name' => 'permit_empty|string',
		'country' => 'permit_empty|string',
		'role' => 'permit_empty|integer|in_list[1,2]',
		'is_blocked' => 'permit_empty|integer|in_list[1,2]',
		'deleted_at' => 'permit_empty|integer|in_list[1,2]',
		'date_from' => 'permit_empty|valid_date[d-m-Y]',
		'date_to' => 'permit_empty|valid_date[d-m-Y]',
	];

	public function search()
	{;
		$model = new UserModel();

		$users = $model;
		$users = $users
			->select(UserModel::table() . '.*')
			->select('fileTable.url as path_to_ava')
			->select('countryTable.title as country')
			->select('cityTable.title as city')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . UserModel::table() . '.file_id', 'left')
			->join(CountryTranslationModel::table() . ' as countryTable', 'countryTable.country_id = ' . UserModel::table() . '.country_id and countryTable.lang_code = "ru"', 'left')
			->join(CityTranslationModel::table() . ' as cityTable', 'cityTable.city_id = ' . UserModel::table() . '.city_id and cityTable.lang_code = "ru"', 'left');

		if (!empty($this->first_name)) {
			$users->like('first_name', $this->first_name);
		}

		if (!empty($this->last_name)) {
			$users->like('last_name', $this->last_name);
		}

		if (!empty($this->country)) {
			$users->like('countryTable.title', $this->country);
		}

		if (!empty($this->role)) {
			$users->where('role', $this->role);
		}

		if (!empty($this->is_blocked)) {
			$users->where('is_blocked', $this->is_blocked);
		}

		if (!empty($this->date_from)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_from . '00:00');
			$users->where(UserModel::table() . '.created_at >=', $date->getTimestamp());
		}

		if (!empty($this->date_to)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_to . '00:00');
			$users->where(UserModel::table() . '.created_at <=', $date->getTimestamp() + DAY - 1);
		}

		if (!empty($this->deleted_at)) {
			if ((int)$this->deleted_at === UserModel::ACCOUNT_IS_NOT_DELETED) {
				$users->where(UserModel::table() . '.deleted_at', null);
			} else if ((int)$this->deleted_at === UserModel::ACCOUNT_IS_DELETED) {
				$users->where(UserModel::table() . '.deleted_at is not null');
			}
		}
		
		$users = $users->orderBy('user_id', 'ASC')
			->withDeleted()
			->paginate(20, 'default');

		return [
			'searchResult' => $users,
			'pager' => $model->pager,
			'model' => $model,
			'form' => $this,
		];
	}
}