<?php

namespace Admin\Models\article_section;

use Common\Forms\BaseForm;
use Common\Models\ArticleSectionModel;
use Common\Models\FileModel;

class SaveArticleSectionForm extends BaseForm
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	public $article_section_id;
	public $title;
	public $description;
	public $meta_description;
	public $file;
    public $fileUrl;
	public $file_id;
	public $file_link;
    public $file_title;
	public $delete_file;

	protected $validationRules = [
		'article_section_id' => 'permit_empty|integer',
		'title' => 'required|string|max_length[255]',
        'description' => 'required|string|max_length[65000]',
        'meta_description' => 'required|string|max_length[255]',
        'delete_file' => 'required|integer|in_list[0,1]',
        'file' => 'permit_empty|uploaded[file]|max_size[file,19000]|is_image[file]|ext_in[file,png,jpeg,jpg,gif]|mime_in[file,image/png,image/jpg,image/jpeg,image/gif]',
        'file_link' => 'permit_empty|valid_url',
        'file_title' => 'permit_empty|string|max_length[255]',
	];

	protected $cleanValidationRules = false;

	public function __construct()
	{
		parent::__construct();
		$this->labels = ArticleSectionModel::getLabels();
	}

	public function save()
	{
        helper('cirillicToLatin');
        $articleSectionModel = new ArticleSectionModel();
        $articleSection = [
        	'article_section_id' => $this->article_section_id,
        	'title' => $this->title,
            'title_transliteration' => toLatin($this->title),
        	'description' => $this->description,
        	'meta_description' => $this->meta_description,
        ];

        if (!$articleSectionModel->save($articleSection)) {
        	$this->setError('', lang('ArticleSection.Failed to save article section'));
        	return false;
        }

        $this->article_section_id = (is_null($this->article_section_id))
            ? $articleSectionModel->getInsertID()
            : $this->article_section_id;

        $fileModel = new FileModel();

        if ((int)$this->delete_file === 1 && $this->scenario === self::SCENARIO_EDIT) {
            $file = $fileModel
                ->where('related_item', FileModel::RELATED_ITEM_ARTICLE_SECTION)
                ->where('item_id', $this->article_section_id)
                ->first();

            if (!empty($file)) {
                $fileModel->delete($file->file_id);
            }
        }

        if (empty($this->file->getError())) {
            $fileDataDisk = FileModel::saveFileOnDisk($this->file, FileModel::TYPE_IMAGE);
            $fileData = [
                'type' => FileModel::TYPE_IMAGE,
                'related_item' => FileModel::RELATED_ITEM_ARTICLE_SECTION,
                'item_id' => $this->article_section_id,
                'url' => $fileDataDisk['fileName'],
                'privacy_status' => FileModel::PUBLIC_STATUS,
                'mime_type' => $fileDataDisk['mimeType'],
                'original_name' => $fileDataDisk['originalName'],
                'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
                'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
                'title' => !empty($this->file_title) ? $this->file_title : null,
                'source' => !empty($this->file_link) ? $this->file_link : null,
            ];

            if (!$fileModel->save($fileData)) {
                $this->setError('', lang('File.Failed to save file'));
                return false;
            }
        }

        return true;
	}
}