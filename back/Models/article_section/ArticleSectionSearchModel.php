<?php

namespace Admin\Models\article_section;

use Common\Forms\BaseForm;
use Common\Models\ArticleSectionModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class ArticleSectionSearchModel extends BaseForm
{
	public $title;
	public $description;
	public $meta_description;
	public $date_from;
	public $date_to;

	protected $validationRules = [
		'title' => 'permit_empty|string',
		'description' => 'permit_empty|string',
		'meta_description' => 'permit_empty|string',
		'date_from' => 'permit_empty|valid_date[d-m-Y]',
		'date_to' => 'permit_empty|valid_date[d-m-Y]',
	];

	public function search()
	{;
		$model = new ArticleSectionModel();

		$sections = $model
			->select(ArticleSectionModel::table() . '.*')
			->select('fileTable.file_id, fileTable.url as file')
			->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_ARTICLE_SECTION . '" and fileTable.item_id = ' . ArticleSectionModel::table() . '.article_section_id', 'left');

		if (!empty($this->title)) {
			$sections->like(ArticleSectionModel::table() . '.title', $this->title);
		}

		if (!empty($this->description)) {
			$sections->like(ArticleSectionModel::table() . '.description', $this->description);
		}

		if (!empty($this->description)) {
			$sections->like(ArticleSectionModel::table() . '.meta_description', $this->meta_description);
		}

		if (!empty($this->date_from)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_from . '00:00');
			$sections->where(ArticleSectionModel::table() . '.created_at >=', $date->getTimestamp());
		}

		if (!empty($this->date_to)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_to . '00:00');
			$sections->where(ArticleSectionModel::table() . '.created_at <=', $date->getTimestamp() + DAY - 1);
		}
		
		$sections = $sections->orderBy(ArticleSectionModel::table() . '.article_section_id', 'ASC')
			->withDeleted()
			->paginate(20, 'default');

		return [
			'searchResult' => $sections,
			'pager' => $model->pager,
			'model' => $model,
			'form' => $this,
		];
	}
}