<?php

namespace Admin\Models\article;

use Common\Forms\BaseForm;
use Common\Models\ArticleModel;
use Common\Models\ArticleSectionModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class ArticleSearchModel extends BaseForm
{
	public $title;
	public $description;
	public $meta_description;
	public $article_section_id;
	public $date_from;
	public $date_to;

	protected $validationRules = [
		'title' => 'permit_empty|string',
		'description' => 'permit_empty|string',
		'meta_description' => 'permit_empty|string',
		'article_section_id' => 'permit_empty|integer',
		'date_from' => 'permit_empty|valid_date[d-m-Y]',
		'date_to' => 'permit_empty|valid_date[d-m-Y]',
	];

	public function search()
	{;
		$model = new ArticleModel();

		$articles = $model
			->select(ArticleModel::table() . '.*')
			->select('articleSectionTable.title as sectionTitle')
			->join(ArticleSectionModel::table() . ' as articleSectionTable', 'articleSectionTable.article_section_id = ' . ArticleModel::table() . '.article_section_id');

		if (!empty($this->title)) {
			$articles->like(ArticleModel::table() . '.title', $this->title);
		}

		if (!empty($this->description)) {
			$articles->like(ArticleModel::table() . '.description', $this->description);
		}

		if (!empty($this->description)) {
			$articles->like(ArticleModel::table() . '.meta_description', $this->meta_description);
		}

		if (!empty($this->article_section_id)) {
			$articles->where(ArticleModel::table() . '.article_section_id', $this->article_section_id);
		}

		if (!empty($this->date_from)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_from . '00:00');
			$articles->where(ArticleModel::table() . '.created_at >=', $date->getTimestamp());
		}

		if (!empty($this->date_to)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_to . '00:00');
			$articles->where(ArticleModel::table() . '.created_at <=', $date->getTimestamp() + DAY - 1);
		}
		
		$articles = $articles->orderBy(ArticleModel::table() . '.article_id', 'ASC')
			->withDeleted()
			->paginate(20, 'default');

		return [
			'searchResult' => $articles,
			'pager' => $model->pager,
			'model' => $model,
			'form' => $this,
		];
	}
}