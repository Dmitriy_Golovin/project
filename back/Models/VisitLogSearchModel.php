<?php

namespace Admin\Models;

use Common\Forms\BaseForm;
use Common\Models\VisitLogModel;
use App\Libraries\DateAndTime\DateAndTime;

class VisitLogSearchModel extends BaseForm
{
	public $ip;
	public $user_agent;
	public $uri_path;
	public $date_from;
	public $date_to;

	protected $validationRules = [
		'ip' => 'permit_empty|string',
		'user_agent' => 'permit_empty|string',
		'uri_path' => 'permit_empty|string',
		'date_from' => 'permit_empty|valid_date[d-m-Y]',
		'date_to' => 'permit_empty|valid_date[d-m-Y]',
	];

	public function search()
	{;
		$model = new VisitLogModel();

		$visitLogs = $model
			->select(VisitLogModel::table() . '.*');

		if (!empty($this->ip)) {
			$visitLogs->like('ip', $this->ip);
		}

		if (!empty($this->user_agent)) {
			$visitLogs->like('user_agent', $this->user_agent);
		}

		if (!empty($this->uri_path)) {
			$visitLogs->like('uri_path', $this->uri_path);
		}

		if (!empty($this->date_from)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_from . '00:00');
			$visitLogs->where(VisitLogModel::table() . '.created_at >=', $date->getTimestamp());
		}

		if (!empty($this->date_to)) {
			$date = DateAndTime::createFromFormat('j-m-Y H:i', $this->date_to . '00:00');
			$visitLogs->where(VisitLogModel::table() . '.created_at <=', $date->getTimestamp() + DAY - 1);
		}
		
		$visitLogs = $visitLogs->orderBy('visit_log_id', 'ASC')
			->withDeleted()
			->paginate(20, 'default');

		return [
			'searchResult' => $visitLogs,
			'pager' => $model->pager,
			'model' => $model,
			'form' => $this,
		];
	}
}