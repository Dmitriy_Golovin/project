<?php

namespace Admin\Filters;

use CodeIgniter\Filters\FilterInterface;
use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Libraries\AuthJWT\AuthController;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Models\MessageModel;
use Common\Models\ChatMessageModel;
use Common\Models\EventGuestUserModel;
use Common\Models\TaskModel;

class AdminAuthorizationFilter implements FilterInterface
{
    /**
     * Do whatever processing this filter needs to do.
     * By default it should not return anything during
     * normal execution. However, when an abnormal state
     * is found, it should return an instance of
     * CodeIgniter\HTTP\Response. If it does, script
     * execution will end and that Response will be
     * sent back to the client, allowing for error pages,
     * redirects, etc.
     *
     * @param RequestInterface $request
     * @param array|null       $arguments
     *
     * @return mixed
     */
    public function before(RequestInterface $request, $arguments = null)
    {
        $uri = service('uri');
        $uriSegments = $uri->getSegments();
        $auth = new AuthController();
        $request->data['permission'] = $request->getCookie('sta');
        $statusUser = $auth->authorization($request, $uri->getPath());

        if (is_string($statusUser)) {
            if ($statusUser === 'user') {
                $userModel = new UserModel();
                $userId = $auth->getUserIDByAccessToken($request);
                $user = $userModel->find($userId);
                $request->data['statusUser'] = $statusUser;
                $request->data['userID'] = $userId;
                $request->data['logoUrl'] = '/' . strtolower(date('Fo'));
                $request->data['activeItem'] = (count($uriSegments) > 0)
                    ? $uri->getSegment(2)
                    : '';
                $request->data['activeSubItem'] = (count($uriSegments) > 1)
                    ? $uri->getSegment(3)
                    : '';
                $this->getUserDataInHeader($request, $userId);

                if (empty($user->role) || (int)$user->role !== UserModel::ROLE_ADMIN) {
                    throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
                }
            } else {
                $request->data = null;
                throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
            }
        } else {
            return $statusUser;
        }
    }

    /**
     * Allows After filters to inspect and modify the response
     * object as needed. This method does not allow any way
     * to stop execution of other after filters, short of
     * throwing an Exception or Error.
     *
     * @param RequestInterface  $request
     * @param ResponseInterface $response
     * @param array|null        $arguments
     *
     * @return mixed
     */
    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        //
    }

    private function getUserDataInHeader($request, $userID) {
        $db = db_connect();
        $userModel = new UserModel($db);
        $friendModel = new FriendModel();
        $messageModel = new MessageModel();
        $chatMessageModel = new ChatMessageModel();
        $eventGuestUserModel = new EventGuestUserModel();
        $taskModel = new TaskModel();
        $userData = $userModel->getUserData($userID);
        $friendAmont = $friendModel->getAmountOfUsersInSpecificRelationships($userID);
        $mainNewMessageAmount = 0;
        $mainNewMessageAmount += $messageModel->getNewMessagesAmountDB($userID)->status;
        $mainNewMessageAmount += $chatMessageModel->getNewMessagesAmountDB($userID);
        $eventMeetingNewActiveInvite = $eventGuestUserModel->getAllNewActiveInviteList($userID);
        $notViewedTaskCount = $taskModel->getNotViewedTaskCount($userID);
        $eventMeetingNewActiveInvite['mainCount'] += (int)$notViewedTaskCount;

        if (!empty($friendAmont) && $friendAmont->new_subscriber > 0) {
            $newSubscriberAmount = '<span class="new_subscriber_amount">' . $friendAmont->new_subscriber . '</span>';
        } else {
            $newSubscriberAmount = '';
        }
        
        if ($mainNewMessageAmount > 0) {
            $newMessagesAmount = '<span class="new_subscriber_amount">' . $mainNewMessageAmount . '</span>';
        } else {
            $newMessagesAmount = '';
        }
        
        $request->data['user_id'] = $userData->user_id;
        $request->data['firstName'] = $userData->first_name;
        $request->data['lastName'] = $userData->last_name;
        $request->data['pathToAva'] = $userData->path_to_ava;
        $request->data['country'] = $userData->country;
        $request->data['city'] = $userData->city;
        $request->data['country_id'] = $userData->country_id;
        $request->data['city_id'] = $userData->city_id;
        $request->data['numPhone'] = $userData->number_phone;
        $request->data['email'] = $userData->email;
        $request->data['about_self'] = $userData->about_self;
        $request->data['newSubscriberAmount'] = $newSubscriberAmount;
        $request->data['newMessagesAmount'] = $newMessagesAmount;
        $request->data['cabinetData'] = $eventMeetingNewActiveInvite;
    }
}
