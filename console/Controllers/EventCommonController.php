<?php

namespace Console\Controllers;

use CodeIgniter\Controller;
use Common\Models\UserModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\EventGuestUserModel;
use Common\Models\UserSettingModel;
use App\Libraries\DateAndTime\DateAndTime;

class EventCommonController extends Controller
{
    public function run()
    {
        $this->CompleteAndCloneEvent();
        $this->CompleteAndCloneMeeting();
        $this->CompleteAndCloneTask();
    }

    protected function CompleteAndCloneEvent()
    {
        $eventModel = new EventModel();
        $newId = '';
        $eventQuery = $eventModel
            ->where('(date + time + duration) <=', time())
            ->where('is_completed', EventModel::STATUS_NOT_COMPLETED)
            ->where('is_cloned', EventModel::STATUS_NOT_CLONED)
            ->where('deleted_at', null)
            ->get();

        foreach ($eventQuery->getResultObject() as $event) {
            if ((int)$event->periodicity === EventModel::PERIODICITY_WORKING_DAY) {
                $newEventData = self::_cloneEntity($event);

                $newEventData['date'] = self::_getRepeatdDateWorkingday($event->date, $event->user_id, $event->time_offset_seconds);
                $eventModel->save($newEventData);
                $newId = $eventModel->insertID();
                $event->is_cloned = EventModel::STATUS_CLONED;
            }

            if ((int)$event->periodicity === EventModel::PERIODICITY_DAILY) {
                $newEventData = self::_cloneEntity($event);

                $newEventData['date'] = DateAndTime::createFromTimestamp($event->date)->addDays(1)->getTimestamp();
                $eventModel->save($newEventData);
                $newId = $eventModel->insertID();
                $event->is_cloned = EventModel::STATUS_CLONED;
            }

            if ((int)$event->periodicity === EventModel::PERIODICITY_WEEKLY) {
                $newEventData = self::_cloneEntity($event);

                $newEventData['date'] = DateAndTime::createFromTimestamp($event->date)->addDays(7)->getTimestamp();
                $eventModel->save($newEventData);
                $newId = $eventModel->insertID();
                $event->is_cloned = EventModel::STATUS_CLONED;
            }

            if ((int)$event->periodicity === EventModel::PERIODICITY_MONTHLY) {
                $newEventData = self::_cloneEntity($event);

                $newEventData['date'] = DateAndTime::createFromTimestamp($event->date)->addMonths(1)->getTimestamp();
                $eventModel->save($newEventData);
                $newId = $eventModel->insertID();
                $event->is_cloned = EventModel::STATUS_CLONED;
            }

            if ((int)$event->periodicity === EventModel::PERIODICITY_YEARLY) {
                $newEventData = self::_cloneEntity($event);

                $newEventData['date'] = DateAndTime::createFromTimestamp($event->date)->addYears(1)->getTimestamp();
                $eventModel->save($newEventData);
                $newId = $eventModel->insertID();
                $event->is_cloned = EventModel::STATUS_CLONED;
            }

            $event->is_completed = EventModel::STATUS_COMPLETED;
            $eventModel->save($event);

            if ((int)$event->periodicity !== EventModel::PERIODICITY_NONE) {
                self::_moveEventGuestList($newId, $event->event_id, EventModel::table());
            }
        }
    }

    protected function CompleteAndCloneMeeting()
    {
        $meetingModel = new MeetingModel();
        $newId = '';
        $meetingQuery = $meetingModel
            ->where('(date + time + duration) <=', time())
            ->where('is_completed', MeetingModel::STATUS_NOT_COMPLETED)
            ->where('is_cloned', MeetingModel::STATUS_NOT_CLONED)
            ->where('deleted_at', null)
            ->get();

        foreach ($meetingQuery->getResultObject() as $meeting) {
            if ((int)$meeting->periodicity === MeetingModel::PERIODICITY_WORKING_DAY) {
                $newMeetingData = self::_cloneEntity($meeting);

                $newMeetingData['date'] = self::_getRepeatdDateWorkingday($meeting->date, $meeting->user_id, $meeting->time_offset_seconds);
                $meetingModel->save($newMeetingData);
                $newId = $meetingModel->insertID();
                $meeting->is_cloned = MeetingModel::STATUS_CLONED;
            }

            if ((int)$meeting->periodicity === MeetingModel::PERIODICITY_DAILY) {
                $newMeetingData = self::_cloneEntity($meeting);

                $newMeetingData['date'] = DateAndTime::createFromTimestamp($meeting->date)->addDays(1)->getTimestamp();
                $meetingModel->save($newMeetingData);
                $newId = $meetingModel->insertID();
                $meeting->is_cloned = MeetingModel::STATUS_CLONED;
            }

            if ((int)$meeting->periodicity === MeetingModel::PERIODICITY_WEEKLY) {
                $newMeetingData = self::_cloneEntity($meeting);

                $newMeetingData['date'] = DateAndTime::createFromTimestamp($meeting->date)->addDays(7)->getTimestamp();
                $meetingModel->save($newMeetingData);
                $newId = $meetingModel->insertID();
                $meeting->is_cloned = MeetingModel::STATUS_CLONED;
            }

            if ((int)$meeting->periodicity === MeetingModel::PERIODICITY_MONTHLY) {
                $newMeetingData = self::_cloneEntity($meeting);

                $newMeetingData['date'] = DateAndTime::createFromTimestamp($meeting->date)->addMonths(1)->getTimestamp();
                $meetingModel->save($newMeetingData);
                $newId = $meetingModel->insertID();
                $meeting->is_cloned = MeetingModel::STATUS_CLONED;
            }

            if ((int)$meeting->periodicity === MeetingModel::PERIODICITY_YEARLY) {
                $newMeetingData = self::_cloneEntity($meeting);

                $newMeetingData['date'] = DateAndTime::createFromTimestamp($meeting->date)->addYears(1)->getTimestamp();
                $meetingModel->save($newMeetingData);
                $newId = $meetingModel->insertID();
                $meeting->is_cloned = MeetingModel::STATUS_CLONED;
            }

            $meeting->is_completed = MeetingModel::STATUS_COMPLETED;
            $meetingModel->save($meeting);

            if ((int)$meeting->periodicity !== MeetingModel::PERIODICITY_NONE) {
                self::_moveEventGuestList($newId, $meeting->meeting_id, MeetingModel::table());
            }
        }
    }

    protected function CompleteAndCloneTask()
    {
        $taskModel = new TaskModel();
        $newId = '';
        $taskQuery = $taskModel
            ->where('(date + time + duration) <=', time())
            ->where('deleted_at', null)
            ->where('status !=', TaskModel::STATUS_NO_EXECUTOR)
            ->groupStart()
                ->groupStart()
                    ->where('is_cloned', TaskModel::STATUS_NOT_CLONED)
                    ->where('periodicity !=', TaskModel::PERIODICITY_NONE)
                ->groupEnd()
                ->orGroupStart()
                    ->where('periodicity', TaskModel::PERIODICITY_NONE)
                    ->where('status', TaskModel::STATUS_NOT_COMPLETED)
                ->groupEnd()
            ->groupEnd()
            ->get();

        foreach ($taskQuery->getResultObject() as $task) {
            if ((int)$task->periodicity === TaskModel::PERIODICITY_WORKING_DAY) {
                $newTaskData = self::_cloneEntity($task);

                $newTaskData['date'] = self::_getRepeatdDateWorkingday($task->date, $task->owner_user_id, $task->time_offset_seconds);
                $newTaskData['source_id'] = (!empty($task->source_id))
                    ? $task->source_id
                    : $task->task_id;
                $taskModel->save($newTaskData);
                $newId = $taskModel->insertID();
                self::createCloneTaskHistory($newId);
                $task->is_cloned = TaskModel::STATUS_CLONED;
            }

            if ((int)$task->periodicity === TaskModel::PERIODICITY_DAILY) {
                $newTaskData = self::_cloneEntity($task);

                $newTaskData['date'] = DateAndTime::createFromTimestamp($task->date)->addDays(1)->getTimestamp();
                $newTaskData['source_id'] = (!empty($task->source_id))
                    ? $task->source_id
                    : $task->task_id;
                $taskModel->save($newTaskData);
                $newId = $taskModel->insertID();
                self::createCloneTaskHistory($newId);
                $task->is_cloned = TaskModel::STATUS_CLONED;
            }

            if ((int)$task->periodicity === TaskModel::PERIODICITY_WEEKLY) {
                $newTaskData = self::_cloneEntity($task);

                $newTaskData['date'] = DateAndTime::createFromTimestamp($task->date)->addDays(7)->getTimestamp();
                $newTaskData['source_id'] = (!empty($task->source_id))
                    ? $task->source_id
                    : $task->task_id;
                $taskModel->save($newTaskData);
                $newId = $taskModel->insertID();
                self::createCloneTaskHistory($newId);
                $task->is_cloned = TaskModel::STATUS_CLONED;
            }

            if ((int)$task->periodicity === TaskModel::PERIODICITY_MONTHLY) {
                $newTaskData = self::_cloneEntity($task);

                $newTaskData['date'] = DateAndTime::createFromTimestamp($task->date)->addMonths(1)->getTimestamp();
                $newTaskData['source_id'] = (!empty($task->source_id))
                    ? $task->source_id
                    : $task->task_id;
                $taskModel->save($newTaskData);
                $newId = $meetingModel->insertID();
                self::createCloneTaskHistory($newId);
                $task->is_cloned = TaskModel::STATUS_CLONED;
            }

            if ((int)$task->periodicity === TaskModel::PERIODICITY_YEARLY) {
                $newTaskData = self::_cloneEntity($task);

                $newTaskData['date'] = DateAndTime::createFromTimestamp($task->date)->addYears(1)->getTimestamp();
                $newTaskData['source_id'] = (!empty($task->source_id))
                    ? $task->source_id
                    : $task->task_id;
                $taskModel->save($newTaskData);
                $newId = $taskModel->insertID();
                self::createCloneTaskHistory($newId);
                $task->is_cloned = TaskModel::STATUS_CLONED;
            }

            if ((int)$task->status === TaskModel::STATUS_NOT_COMPLETED) {
                $task = self::checkAndSetOverdueStatusTask($task);
            }

            $taskModel->save($task);
        }
    }

    private static function _cloneEntity($entity)
    {
        $result = [];

        foreach ($entity as $index => $value) {
            if (in_array($index, ['event_id', 'meeting_id', 'task_id', 'review_status', 'is_viewed_assigned', 'status', 'created_at', 'updated_at', 'deleted_at'])) {
                continue;
            }

            $result[$index] = $value;
        }

        return $result;
    }

    private static function _moveEventGuestList($newId, $oldId, $table)
    {
        $eventGuestUserModel = new EventGuestUserModel();

        $query = $eventGuestUserModel
            ->where('related_item', $table)
            ->where('item_id', $oldId)
            ->get();

        foreach ($query->getResultObject() as $item) {
            $newItemData = [
                'related_item' => $table,
                'item_id' => $newId,
                'user_id' => $item->user_id
            ];

            $eventGuestUserModel->save($newItemData);
        }
    }

    private static function _getRepeatdDateWorkingDay($date, $userId, $timeOffsetSeconds)
    {
        $userSettingModel = new UserSettingModel();
        $userSetting = $userSettingModel
            ->where('user_id', $userId)
            ->first();

        $userLastWorkingDayOfWeek = (int)$userSetting->work_week_start + (int)$userSetting->work_days_amount - 1;

        if ($userLastWorkingDayOfWeek > 7) {
            $userLastWorkingDayOfWeek = $userLastWorkingDayOfWeek % 7;
        }

        $dateDayOfWeek = DateAndTime::createFromTimestamp((int)$date + (int)$timeOffsetSeconds)->getDayOfWeek();

        if ((int)$dateDayOfWeek === (int)$userLastWorkingDayOfWeek) {
            $notWorkingDayAmount = 7 - (int)$userSetting->work_days_amount + 1;

            return DateAndTime::createFromTimestamp($date)->addDays($notWorkingDayAmount)->getTimestamp();
        }

        return DateAndTime::createFromTimestamp($date)->addDays(1)->getTimestamp();
    }

    private static function createCloneTaskHistory($taskId)
    {
        $taskHistoryModel = new TaskHistoryModel();
        $taskHistoryData = [
            'task_id' => $taskId,
            'type' =>TaskHistoryModel::TYPE_CLONE,
        ];
        $taskHistoryModel->save($taskHistoryData);
    }

    private static function checkAndSetOverdueStatusTask($task)
    {
        $task->status = TaskModel::STATUS_OVERDUE;
        $task->is_viewed_assigned = TaskModel::STATUS_NOT_VIEW_ASSIGNED;
        $taskHistoryModel = new TaskHistoryModel();
        $taskHistoryData = [
            'task_id' => $task->task_id,
            'type' =>TaskHistoryModel::TYPE_OVERDUE,
        ];
        $taskHistoryModel->save($taskHistoryData);

        return $task;
    }
}