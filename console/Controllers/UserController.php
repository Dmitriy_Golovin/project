<?php

namespace Console\Controllers;

use CodeIgniter\Controller;
use Common\Models\UserModel;
use Common\Models\ChangeEmailModel;

class UserController extends Controller
{
    public function run()
    {
        $this->deleteNotConfirmedUser();
        $this->deleteNotConfirmedChangeEmail();
        $this->deleteTokenChangeSafetyData();
    }

    protected function deleteNotConfirmedUser()
    {
        $userModel = new UserModel();
        $timeHour = time() - HOUR;
        $arrWhere = ['veryfication_token !=' => null, 'created_at <=' => $timeHour];
        $userModel->db->table($userModel->db->prefixTable('user'))
            ->where($arrWhere)
            ->delete();
    }

    protected function deleteNotConfirmedChangeEmail()
    {
        $changeEmailModel = new ChangeEmailModel();
        $timeHour = time() - HOUR;
        $arrWhere = ['token !=' => null, 'changed' => 0, 'created_at <=' => $timeHour];
        $changeEmailModel->where($arrWhere)
                 ->delete();
    }

    protected function deleteTokenChangeSafetyData()
    {
        $userModel = new UserModel();
        $query = $userModel
            ->where('token_change_safety_data !=', null)
            ->where('updated_at <=', time() - (5 * MINUTE))
            ->get();

        foreach ($query->getResultObject() as $user) {
            $user->token_change_safety_data = null;
            $userModel->save($user);
        }
    }
}