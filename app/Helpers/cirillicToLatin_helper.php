<?php

function toLatin($str)
{
	$cirillicToLatinConfig = config('CirillicToLatinConfig');

	$str = preg_replace('/\?/', '', $str);
	$str = preg_replace('/,/', '', $str);
	$str = preg_replace('/\./', '', $str);

	foreach ($cirillicToLatinConfig->pattern as $index => $item) {
		$str = preg_replace('/' . $index . '/', $item, $str);
	}

	return $str;
}