<?php

function getPreviousUrl(string $url, array $availableSegment, string $defaultUrl)
{
	$urlArr = explode('/', $url);

	if (isset($urlArr[$availableSegment['segment']])
		&& in_array($urlArr[$availableSegment['segment']], $availableSegment['variant'])) {
		return $url;
	}

	return $defaultUrl;
}