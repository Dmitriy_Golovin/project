<?php

function map(array $array, string $key, string $value)
{
	$result = [];

	foreach ($array as $item) {
		$result[$item[$key]] = $item[$value];
	}

	return $result;
}