<?php

namespace App\Models\profile;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\ReportModel;
use Common\Models\ReportFileModel;
use Common\Models\FileModel;
use Common\Models\UserModel;
use Common\Models\PrivateFilePermissionModel;
use Common\Models\EventGuestUserModel;
use Common\Models\CommentModel;
use App\Libraries\DateAndTime\DateAndTime;

class ProfileMainEventListForm extends BaseForm
{
	public $user_id;
	public $profile_id;
	public $type;
	public $subType;
	public $timeOffsetSeconds;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
		'type' => 'required|string|in_list[1,2]',
		'subType' => 'required|string|in_list[1,2]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	protected $entityModel;
	protected $entityFieldId;
	protected $entityType;

	private $_result;
	private $_profileEventCount;

	public function getList()
	{
		$mainEventParamsConfig = config('MainEventParamsConfig');

		if ((int)$this->type === (int)$mainEventParamsConfig->typeProfilePageEvent) {
			$this->entityModel = new EventModel();
			$this->entityFieldId = 'event_id';
			$this->entityType = 'event';
		} else if ((int)$this->type === (int)$mainEventParamsConfig->typeProfilePageMeeting) {
			$this->entityModel = new MeetingModel();
			$this->entityFieldId = 'meeting_id';
			$this->entityType = 'meeting';
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$subQueryEventGuest = $eventGuestUserModel
			->select('count(event_guest_user_id)')
			->where('related_item', $this->entityModel::table())
			->where(EventGuestUserModel::table() . '.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId)
			->getCompiledSelect();

		$this->_result = $this->entityModel
			->select($this->entityModel::table() . '.*')
			->select('(' . $subQueryEventGuest . ') as userGuestCount')
			->select('reportTable.report_id, reportTable.note as reportNote')
			->select('count(commentTable.comment_id) as commentCount')
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . $this->entityModel::table() . '" and eventGuestTable.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId, 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.related_item = "' . $this->entityModel::table() . '" and reportTable.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId, 'left')
			->join(CommentModel::table() . ' as commentTable', 'commentTable.related_item = "' . ReportModel::table() . '" and commentTable.item_id = reportTable.report_id', 'left')
			->where($this->entityModel::table() . '.user_id', $this->profile_id);

		if ((int)$this->subType === (int)$mainEventParamsConfig->subTypeProfilePageCurrentCompleted) {
			$this->_result
				->groupStart()
					->groupStart()
						->where('(' . $this->entityModel::table() . '.date + ' . $this->entityModel::table() . '.time + ' . $this->entityModel::table() . '.duration) <', time())
					->groupEnd()
					->orGroupStart()
						->where('(' . $this->entityModel::table() . '.date + ' . $this->entityModel::table() . '.time) <', time())
						->where('(' . $this->entityModel::table() . '.date + ' . $this->entityModel::table() . '.time + ' . $this->entityModel::table() . '.duration) >', time())
					->groupEnd()
				->groupEnd();
		}

		if ((int)$this->subType === (int)$mainEventParamsConfig->subTypeProfilePageScheduled) {
			$this->_result
				->where('(' . $this->entityModel::table() . '.date + ' . $this->entityModel::table() . '.time) >', time());
		}

		$this->_result
			->groupStart()
				->groupStart()
					->where($this->entityModel::table() . '.privacy', $this->entityModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where($this->entityModel::table() . '.privacy', $this->entityModel::PRIVATE_STATUS)
					->where('eventGuestTable.user_id', $this->user_id)
				->groupEnd()
			->groupEnd()
			->groupBy($this->entityModel::table() . '.' . $this->entityFieldId)
			->orderBy($this->entityModel::table() . '.created_at', 'DESC');

		$this->_profileEventCount = $this->_result->countAllResults(false);

		$this->_result = $this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $this->entityModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $this->entityModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['profileEventResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['profileEventCount'] = $this->_profileEventCount;
		$result['profile_id'] = $this->profile_id;
		$result['entityType'] = $this->entityType;
		$result['entityFieldId'] = $this->entityFieldId;

		foreach ($this->_result->getResultObject() as $item) {
			$startDate = (int)$item->date + (int)$item->time;
			$endDate = (int)$item->date + (int)$item->time + (int)$item->duration;
			$item->date = DateAndTime::getDateTime($startDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
			$item->endDate = DateAndTime::getDateTime($endDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');

			if (empty($item->name)) {
				$item->name = lang('Main.No title');
			}

			if (!empty($item->report_id)) {
				$item->reportFileList = ReportModel::getPublicReportFileAndWithPermission($item->report_id, $this->user_id);
			}

			$item->isComplete = ($endDate <= time())
				? true
				: false;

			$result['profileEventResult'][] = $item;
		}
// var_dump($result);die;
		return $result;
	}
}