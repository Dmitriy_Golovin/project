<?php

namespace App\Models\profile;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;

class ProfileDataForm extends BaseForm
{
	const USER_BLOCKED = 1;
	const USER_DELETED = 2;
	const USER_NOT_FOUND = 3;

	public $user;
	public $page_id;

	protected $validationRules = [
		'user.user_id' => 'required|integer',
		'page_id' => 'required|integer',
	];

	private $_result;

	public function getData()
	{
		$this->_result['selfPage'] = false;

		if ($this->page_id === $this->user['user_id']) {
			$this->_result['title'] = $this->user['firstName'] . ' ' . $this->user['lastName'];
			$this->_result['profileFullName'] = $this->user['firstName'] . ' ' . $this->user['lastName'];
			$this->_result['profileCountry'] = $this->user['country'];
			$this->_result['profileCity'] = $this->user['city'];
			$this->_result['pageID'] = $this->user['user_id'];
			$this->_result['profileChangeAboutSelf'] = !empty($this->user['about_self'])
				? lang('Main.write something about yourself...')
				: lang('Main.change');
			$this->_result['profileAboutSelf'] = $this->user['about_self'];
			$this->_result['pathToAvaPage'] = $this->user['pathToAva'];
			$this->_result['selfPage'] = true;
		}

		$userModel = new UserModel();
		$userData = $userModel->getUserData($this->page_id, true, true);

		if (empty($userData)) {
			$this->_result = self::USER_NOT_FOUND;
			return true;
		}

		if ((int)$userData->is_blocked === UserModel::STATUS_BLOCKED) {
			$this->_result = self::USER_BLOCKED;
			return true;
		}

		if (!is_null($userData->userDeleted)) {
			$this->_result = self::USER_DELETED;
			return true;
		}

		$friendModel = new FriendModel();
		$this->_result['title'] = $userData->first_name . ' ' . $userData->last_name;
		$this->_result['profileFullName'] = $userData->first_name . ' ' . $userData->last_name;
		$this->_result['profileCountry'] = !empty($userData->country)
			? $userData->country
			: lang('Main.not specified');
		$this->_result['profileCity'] = !empty($userData->city)
			? $userData->city
			: lang('Main.not specified');
		$this->_result['pageID'] = $userData->user_id;
		$this->_result['profileChangeAboutSelf'] = !empty($userData->about_self)
			? lang('Main.change')
			: lang('Main.write something about yourself...');
		$this->_result['profileAboutSelf'] = $userData->about_self;
		$this->_result['pathToAvaPage'] = $userData->path_to_ava;
		$this->_result['friendLinks'] = $friendModel->getUserRelationships([$this->page_id], $this->user['user_id']);

		if (empty($this->_result['friendLinks'])) {
			$this->_result['friendLinks'][$this->page_id] = FriendModel::getNoLinksDataLabel();
		}

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}