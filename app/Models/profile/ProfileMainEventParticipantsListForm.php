<?php

namespace App\Models\profile;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;

class ProfileMainEventParticipantsListForm extends BaseForm
{
	public $user_id;
	public $profile_id;
	public $entity_id;
	public $type;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'type' => 'required|string|in_list[1,2]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	protected $entityModel;
	protected $entityFieldId;
	protected $entityType;

	private $_userCount;
	private $_result;

	public function getList()
	{
		$mainEventParamsConfig = config('MainEventParamsConfig');

		if ((int)$this->type === (int)$mainEventParamsConfig->typeProfilePageEvent) {
			$this->entityModel = new EventModel();
			$this->entityFieldId = 'event_id';
			$this->entityType = 'event';
		} else if ((int)$this->type === (int)$mainEventParamsConfig->typeProfilePageMeeting) {
			$this->entityModel = new MeetingModel();
			$this->entityFieldId = 'meeting_id';
			$this->entityType = 'meeting';
		}

		$entity = $this->entityModel
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . $this->entityModel::table() . '" and eventGuestTable.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId, 'left')
			->where($this->entityModel::table() . '.user_id', $this->profile_id)
			->where($this->entityModel::table() . '.' . $this->entityFieldId, $this->entity_id)
			->groupStart()
				->groupStart()
					->where($this->entityModel::table() . '.privacy', $this->entityModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where($this->entityModel::table() . '.privacy', $this->entityModel::PRIVATE_STATUS)
					->where('eventGuestTable.user_id', $this->user_id)
				->groupEnd()
			->groupEnd()
			->first();

		if (empty($entity)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$query = $eventGuestUserModel
			->select('userTable.user_id, concat(userTable.first_name, " ", userTable.last_name) as fullName')
			->select('fileTable.url as path_to_ava')
			->join($this->entityModel::table() . ' as entityTable', EventGuestUserModel::table() . '.related_item = "' . $this->entityModel::table() . '" and entityTable.' . $this->entityFieldId . ' = ' . EventGuestUserModel::table() . '.item_id')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . EventGuestUserModel::table() . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where('entityTable.' . $this->entityFieldId, $this->entity_id)
			->where('entityTable.user_id', $this->profile_id);

		$this->_userCount = $query->countAllResults(false);

		$query = $query->orderBy('fullName', 'ASC')
			->getCompiledSelect();

		$query = 'select (@rowNum := @rowNum + 1) as rowNum, userQuery.* from (' . $query . ') as userQuery, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$query = 'select userQuery.* from (' . $query . ') as userQuery where rowNum > ' . $eventGuestUserModel->db->escape($this->rowNum);
		}

		$query = 'select userQuery.* from (' . $query . ') as userQuery limit ' . $this->limit;
		$this->_result = $eventGuestUserModel->db->query('select query.* from (' . $query . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['userResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['userCount'] = $this->_userCount;
		$result['type'] = null;

		foreach ($this->_result->getResultObject() as $item) {
			if (empty($item->path_to_ava)) {
				$item->path_to_ava = FileModel::DEFAULT_AVA_URL;
			} else {
				$item->path_to_ava = FileModel::preparePathToAva($item->path_to_ava);
			}

			$result['userResult'][] = $item;
		}

		return $result;
	}
}