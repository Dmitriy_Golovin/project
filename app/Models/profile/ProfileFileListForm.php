<?php

namespace App\Models\profile;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\PrivateFilePermissionModel;

class ProfileFileListForm extends BaseForm
{
	public $user_id;
	public $profile_id;
	public $type;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 30;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
		'type' => 'required|string|in_list[image,video,document]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;
	private $_fileCount;

	public function getList()
	{
		$fileModel = new FileModel();
		$this->_result = $fileModel
			->select(FileModel::table() . '.*')
			->join(PrivateFilePermissionModel::table() . ' as filePermissionTable', 'filePermissionTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where(FileModel::table() . '.item_id', $this->profile_id)
			->where(FileModel::table() . '.type', FileModel::getTypeByFileConfig()[$this->type])
			->where(FileModel::table() . '.created_at <=', $this->timestamp)
			->groupStart()
				->groupStart()
					->where(FileModel::table() . '.privacy_status', FileModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where(FileModel::table() . '.privacy_status', FileModel::PRIVATE_STATUS)
					->where('filePermissionTable.permitted_id', $this->user_id)
				->groupEnd()
			->groupEnd();

		$this->_fileCount = $this->_result->countAllResults(false);

		$this->_result = $this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $fileModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $fileModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['fileResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['fileCount'] = $this->_fileCount;
		$result['type'] = $this->type;

		foreach ($this->_result->getResultObject() as $file) {
			$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
            $file->name = FileModel::getFileNameWithoutExec($file->original_name);

            if ($this->type === config('FileConfig')->filePageTypeDocument) {
            	$file->selfFile = false;
				$file->canDelete = false;
            	$file->documentDate = DateAndTime::getDateTime($file->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
            }

			$result['fileResult'][] = $file;
		}

		return $result;
	}
}