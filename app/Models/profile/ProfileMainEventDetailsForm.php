<?php

namespace App\Models\profile;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use App\Libraries\DateAndTime\DateAndTime;

class ProfileMainEventDetailsForm extends BaseForm
{
	public $user_id;
	public $profile_id;
	public $entity_id;
	public $type;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'type' => 'required|string|in_list[1,2]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	protected $entityModel;
	protected $entityFieldId;
	protected $entityType;

	private $_result;

	public function getDetails()
	{
		$mainEventParamsConfig = config('MainEventParamsConfig');

		if ((int)$this->type === (int)$mainEventParamsConfig->typeProfilePageEvent) {
			$this->entityModel = new EventModel();
			$this->entityFieldId = 'event_id';
			$this->entityType = 'event';
		} else if ((int)$this->type === (int)$mainEventParamsConfig->typeProfilePageMeeting) {
			$this->entityModel = new MeetingModel();
			$this->entityFieldId = 'meeting_id';
			$this->entityType = 'meeting';
		}

		$this->_result = $this->entityModel
			->select($this->entityModel::table() . '.*')
			->select('userTable.first_name, userTable.last_name')
			->select('fileTable.url as userAva')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . $this->entityModel::table() . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . $this->entityModel::table() . '" and eventGuestTable.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId, 'left')
			->where($this->entityModel::table() . '.user_id', $this->profile_id)
			->where($this->entityModel::table() . '.' . $this->entityFieldId, $this->entity_id)
			->groupStart()
				->groupStart()
					->where($this->entityModel::table() . '.privacy', $this->entityModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where($this->entityModel::table() . '.privacy', $this->entityModel::PRIVATE_STATUS)
					->where('eventGuestTable.user_id', $this->user_id)
				->groupEnd()
			->groupEnd()
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$startDate = (int)$this->_result->date + (int)$this->_result->time;
		$endDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$startDay = DateAndTime::getTimestampStartDay(time()) - $this->timeOffsetSeconds;
		$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;
		$this->_result->date = DateAndTime::getDateTime($startDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->endDate = DateAndTime::getDateTime($endDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->type = $this->entityType;
		$this->_result->privacyValue = ((int)$this->_result->privacy === 1)
			? lang('Event.private')
			: lang('Event.public');
		$this->_result->userAva = (!empty($this->_result->userAva))
				? FileModel::preparePathToAva($this->_result->userAva)
				: FileModel::DEFAULT_AVA_URL;

		if (empty($this->_result->name)) {
			$this->_result->name = lang('Main.No title');
		}

		if ($endDate <= time()) {
			$this->_result->statusEntity = COMPLETED;
		} else if (($startDate <= time() && $endDate >= time())
			|| ($startDate >= time() && $startDate <= $endDay)) {
			$this->_result->statusEntity = CURRENT;
		} else if ($startDate > $endDay) {
			$this->_result->statusEntity = SCHEDULED;
		}

// var_dump($this->_result);die;
		return ['entity' => $this->_result];
	}
}