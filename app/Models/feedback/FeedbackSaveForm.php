<?php

namespace App\Models\feedback;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FeedbackModel;

class FeedbackSaveForm extends BaseForm
{
	public $user_id;
	public $email;
	public $text;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'email' => 'required|valid_email|max_length[255]',
		'text' => 'required|string|max_length[65000]',
	];

	protected $cleanValidationRules = false;

	public function save()
	{
		$userModel = new UserModel();
		$user = $userModel
			->where('user_id', $this->user_id)
			->where('is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->where('veryfication_token', null)
			->first();

		if (empty($user)) {
			$this->setError(''. lang('Main.User with id {0} not found', [$this->user_id]));
			return false;
		}

		$feedbackModel = new FeedbackModel();
		$feedbackData = [
			'user_id' => $this->user_id,
			'email' => $this->email,
			'text' => $this->text,
		];

		if (!$feedbackModel->save($feedbackData)) {
			$this->setError('', lang('Main.Failed to send message'));
			return false;
		}

		$this->user_id = null;
		$this->email = null;
		$this->text = null;

		return true;
	}
}