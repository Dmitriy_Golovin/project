<?php

namespace App\Models\imageFile;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\UserModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\PrivateFilePermissionModel;
use Common\Models\CommentModel;
use App\Libraries\DateAndTime\DateAndTime;

class ImageSliderTaskHistoryDataForm extends BaseForm
{
	const TYPE_SHOW = 1;
	const TYPE_PREVIOUS = 2;
	const TYPE_NEXT = 3;

	public $user_id;
	public $type;
	public $current_file_id;
	public $last_file_id;
	public $entity_id;
	public $timeOffsetSeconds;
	public $exceptionIdList;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2,3]',
		'current_file_id' => 'permit_empty|integer|required_without[last_file_id]',
		'last_file_id' => 'permit_empty|integer|required_without[current_file_id]',
		'entity_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'exceptionIdList.*' => 'permit_empty|integer',
	];

	protected $cleanValidationRules = false;

	protected $limit = 3;

	private $_imageCount;
	private $_result;

	public function getData()
	{
		$taskHistoryFileModel = new TaskHistoryFileModel();
		$db = $taskHistoryFileModel->db;

		if ((int)$this->type === self::TYPE_SHOW) {
			$currentFile = $taskHistoryFileModel
				->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . TaskHistoryFileModel::table() . '.file_id')
				->join(PrivateFilePermissionModel::table() . ' as filePermissionTable', 'filePermissionTable.file_id = fileTable.file_id', 'left')
				->where(TaskHistoryFileModel::table() . '.task_history_id', $this->entity_id)
				->where('fileTable.type', FileModel::TYPE_IMAGE)
				->where('fileTable.file_id', $this->current_file_id)
				->groupStart()
					->groupStart()
						->where('fileTable.privacy_status', FileModel::PUBLIC_STATUS)
					->groupEnd()
					->orGroupStart()
						->where('fileTable.privacy_status', FileModel::PRIVATE_STATUS)
						->where('fileTable.related_item', 'user')
						->where('fileTable.item_id', $this->user_id)
					->groupEnd()
					->orGroupStart()
						->where('fileTable.privacy_status', FileModel::PRIVATE_STATUS)
						->where('fileTable.related_item', 'user')
						->where('fileTable.item_id !=', $this->user_id)
						->where('filePermissionTable.permitted_id', $this->user_id)
					->groupEnd()
				->groupEnd()
				->first();

			if (empty($currentFile)) {
				$this->setError('', lang('File.File does not exist, it may have been deleted'));
				return false;
			}
		}
		
		$query = $taskHistoryFileModel
			->select('fileTable.*')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . TaskHistoryFileModel::table() . '.file_id')
			->join(PrivateFilePermissionModel::table() . ' as filePermissionTable', 'filePermissionTable.file_id = fileTable.file_id', 'left')
			->where(TaskHistoryFileModel::table() . '.task_history_id', $this->entity_id)
			->where('fileTable.type', FileModel::TYPE_IMAGE)
			->groupStart()
				->groupStart()
					->where('fileTable.privacy_status', FileModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where('fileTable.privacy_status', FileModel::PRIVATE_STATUS)
					->where('fileTable.related_item', 'user')
					->where('fileTable.item_id', $this->user_id)
				->groupEnd()
				->orGroupStart()
					->where('fileTable.privacy_status', FileModel::PRIVATE_STATUS)
					->where('fileTable.related_item', 'user')
					->where('fileTable.item_id !=', $this->user_id)
					->where('filePermissionTable.permitted_id', $this->user_id)
				->groupEnd()
			->groupEnd();

		if (!empty($this->exceptionIdList)) {
			$query->whereNotIn('fileTable.file_id', $this->exceptionIdList);
		}

		$query = $query->getCompiledSelect();

		$query = 'select (@rowN := @rowN + 1) as rowN, query.* from (' . $query . ') as query, (select (@rowN := 0)) as rowN';

		if ((int)$this->type === self::TYPE_SHOW) {
			$this->_imageCount = $db->query('select countQuery.* from (' . $query . ') as countQuery')->getNumRows();
			$firstPartQuery = 'select (query.rowN) as rowNum, query.* from(' . $query . ') as query where query.file_id < ' . $db->escape($this->current_file_id) . ' order by query.file_id DESC limit ' . $this->limit;

			$secondPartQuery = 'select (query.rowN - ' . $this->_imageCount . ') as rowNum, query.* from(' . $query . ') as query where query.file_id >= ' . $db->escape($this->current_file_id) . ' order by query.file_id ASC limit ' . ($this->limit + 1);

			$this->_result = $db->query('select mainQuery.* from ((' . $firstPartQuery . ') union all (' . $secondPartQuery . ')) as mainQuery order by mainQuery.file_id ASC');
		}

		if ((int)$this->type === self::TYPE_PREVIOUS) {
			$query = 'select query.rowN as rowNum, query.* from (' . $query . ') as query where query.file_id < ' . $db->escape($this->last_file_id) . 'order by query.file_id DESC limit ' . $this->limit;
			$this->_result = $db->query('select mainQuery.* from (' . $query . ') as mainQuery order by mainQuery.file_id ASC');
		}

		if ((int)$this->type === self::TYPE_NEXT) {
			$query = 'select query.rowN as rowNum, query.* from (' . $query . ') as query where query.file_id > ' . $db->escape($this->last_file_id) . 'order by query.file_id ASC limit ' . $this->limit;
			$this->_result = $db->query('select mainQuery.* from (' . $query . ') as mainQuery');
		}

		return true;
	}

	public function getResult()
	{
		$result = [];
		$currentOrdinalRow = 0;
		$queryCount = 0;
		$commentModel = new CommentModel();
		$result['type'] = (int)$this->type;
		$result['leftArrowActive'] = true;
		$result['rightArrowActive'] = true;

		if (!empty($this->_imageCount)) {
			$result['imageCount'] = $this->_imageCount;
		}

		foreach ($this->_result->getResultObject() as $index => $file) {
			$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
			$file->date = DateAndTime::getDateTime($file->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
			$file->commentData = $commentModel->getCommentData(CommentModel::TYPE_TABLE_FILE, $file->file_id, $this->timeOffsetSeconds);
			$file->current = false;

			if ((int)$file->rowNum === 1 || (int)$file->rowNum === (int)$this->_imageCount) {
				$file->position = 'last';
			} else {
				$file->position = 'center';
			}

			if ((int)$this->type === self::TYPE_SHOW) {
				if ((int)$this->current_file_id === (int)$file->file_id && (int)$file->rowNum === 1) {
					$result['leftArrowActive'] = false;
				}

				if ((int)$this->current_file_id === (int)$file->file_id && (int)$file->rowNum === (int)$this->_imageCount) {
					$result['rightArrowActive'] = false;
				}

				if ((int)$this->current_file_id === (int)$file->file_id) {
					$currentOrdinalRow = $index + 1;
					$file->current = true;
				}
			}

			$file->selfFile = ((int)$file->item_id === (int)$this->user_id && $file->related_item === FileModel::RELATED_ITEM_USER)
				? true
				: false;
			$file->canDelete = false;

			$result['imageData'][] = $file;
		}

		if ((int)$this->type === self::TYPE_SHOW) {
			if ($currentOrdinalRow > 1) {
				// $result['sliderTranslate'] = (100 / ($queryCount + 1) * ($currentOrdinalRow - 1)) * (-1);
				$result['sliderTranslate'] = 100 * ($currentOrdinalRow - 1)  * (-1);
			} else {
				$result['sliderTranslate'] = 0;
			}

		}

		return $result;
	}
}