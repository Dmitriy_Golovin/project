<?php

namespace App\Models\imageFile;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\CommentModel;
use Common\Models\ReportFileModel;
use Common\Models\ReportModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskModel;
use App\Libraries\DateAndTime\DateAndTime;

class ImageSliderSelfUserDataForm extends BaseForm
{
	const TYPE_SHOW = 1;
	const TYPE_PREVIOUS = 2;
	const TYPE_NEXT = 3;

	public $user_id;
	public $type;
	public $current_file_id;
	public $last_file_id;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2,3]',
		'current_file_id' => 'permit_empty|integer|required_without[last_file_id]',
		'last_file_id' => 'permit_empty|integer|required_without[current_file_id]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	protected $limit = 3;

	private $_imageCount;
	private $_result;

	public function getData()
	{
		$fileModel = new FileModel();
		$db = $fileModel->db;

		if ((int)$this->type === self::TYPE_SHOW) {
			$currentFile = $fileModel
				->where('file_id', $this->current_file_id)
				->where(FileModel::table() . '.related_item', FileModel::RELATED_ITEM_USER)
				->where(FileModel::table() . '.item_id', $this->user_id)
				->first();

			if (empty($currentFile)) {
				$this->setError('', lang('File.File does not exist, it may have been deleted'));
				return false;
			}
		}

		$query = $fileModel
			->select(FileModel::table() . '.*')
			->select('eventTable.event_id, eventTable.name as eventName')
			->select('meetingTable.meeting_id, meetingTable.name as meetingName')
			->select('taskTable.task_id, taskTable.assigned_user_id, taskTable.owner_user_id, taskTable.name as taskName, taskTable.deleted_at as taskDeleted')
			->join(ReportFileModel::table() . ' as reportFileTable', 'reportFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.report_id = reportFileTable.report_id', 'left')
			->join(EventModel::table() . ' as eventTable', 'eventTable.event_id = reportTable.item_id and reportTable.related_item = "' . EventModel::table() . '"', 'left')
			->join(MeetingModel::table() . ' as meetingTable', 'meetingTable.meeting_id = reportTable.item_id and reportTable.related_item = "' . MeetingModel::table() . '"', 'left')
			->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->join(TaskHistoryModel::table() . ' as taskHistoryTable', 'taskHistoryTable.task_history_id = taskHistoryFileTable.task_history_id', 'left')
			->join(TaskModel::table() . ' as taskTable', 'taskTable.task_id = taskHistoryTable.task_id', 'left')
			->where(FileModel::table() . '.related_item', FileModel::RELATED_ITEM_USER)
			->where(FileModel::table() . '.item_id', $this->user_id)
			->where(FileModel::table() . '.type', FileModel::TYPE_IMAGE)
			->getCompiledSelect();

		$query = 'select (@rowN := @rowN + 1) as rowN, query.* from (' . $query . ') as query, (select (@rowN := 0)) as rowN';

		if ((int)$this->type === self::TYPE_SHOW) {
			$this->_imageCount = $db->query('select countQuery.* from (' . $query . ') as countQuery')->getNumRows();
			$firstPartQuery = 'select (query.rowN) as rowNum, query.* from(' . $query . ') as query where query.file_id < ' . $db->escape($this->current_file_id) . ' order by query.file_id DESC limit ' . $this->limit;

			$secondPartQuery = 'select (query.rowN - ' . $this->_imageCount . ') as rowNum, query.* from(' . $query . ') as query where query.file_id >= ' . $db->escape($this->current_file_id) . ' order by query.file_id ASC limit ' . ($this->limit + 1);

			$this->_result = $db->query('select mainQuery.* from ((' . $firstPartQuery . ') union all (' . $secondPartQuery . ')) as mainQuery order by mainQuery.file_id ASC');
		}

		if ((int)$this->type === self::TYPE_PREVIOUS) {
			$query = 'select query.rowN as rowNum, query.* from (' . $query . ') as query where query.file_id < ' . $db->escape($this->last_file_id) . 'order by query.file_id DESC limit ' . $this->limit;
			$this->_result = $db->query('select mainQuery.* from (' . $query . ') as mainQuery order by mainQuery.file_id ASC');
		}

		if ((int)$this->type === self::TYPE_NEXT) {
			$query = 'select query.rowN as rowNum, query.* from (' . $query . ') as query where query.file_id > ' . $db->escape($this->last_file_id) . 'order by query.file_id ASC limit ' . $this->limit;
			$this->_result = $db->query('select mainQuery.* from (' . $query . ') as mainQuery');
		}

		return true;
	}

	public function getResult()
	{
		$result = [];
		$currentOrdinalRow = 0;
		$queryCount = 0;
		$commentModel = new CommentModel();
		$result['type'] = (int)$this->type;
		$result['leftArrowActive'] = true;
		$result['rightArrowActive'] = true;

		if (!empty($this->_imageCount)) {
			$result['imageCount'] = $this->_imageCount;
		}

		foreach ($this->_result->getResultObject() as $index => $file) {
			$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
			$file->date = DateAndTime::getDateTime($file->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
			$file->commentData = $commentModel->getCommentData(CommentModel::TYPE_TABLE_FILE, $file->file_id, $this->timeOffsetSeconds);
			$file->current = false;

			if ((int)$file->rowNum === 1 || (int)$file->rowNum === (int)$this->_imageCount) {
				$file->position = 'last';
			} else {
				$file->position = 'center';
			}

			if ((int)$this->type === self::TYPE_SHOW) {
				if ((int)$this->current_file_id === (int)$file->file_id && (int)$file->rowNum === 1) {
					$result['leftArrowActive'] = false;
				}

				if ((int)$this->current_file_id === (int)$file->file_id && (int)$file->rowNum === (int)$this->_imageCount) {
					$result['rightArrowActive'] = false;
				}

				if ((int)$this->current_file_id === (int)$file->file_id) {
					$currentOrdinalRow = $index + 1;
					$file->current = true;
				}
			}

			$file->selfFile = true;
			$file->canDelete = true;
			$file->entityData = $this->prepareEntityData($file);

			$result['imageData'][] = $file;
		}

		if ((int)$this->type === self::TYPE_SHOW) {
			if ($currentOrdinalRow > 1) {
				// $result['sliderTranslate'] = (100 / ($queryCount + 1) * ($currentOrdinalRow - 1)) * (-1);
				$result['sliderTranslate'] = 100 * ($currentOrdinalRow - 1)  * (-1);
			} else {
				$result['sliderTranslate'] = 0;
			}

		}

		return $result;
	}

	protected function prepareEntityData($item)
	{
		$result = [];

		if (!empty($item->event_id)) {
			$result['entity'] = lang('Event.Event');
			$result['name'] = (!empty($item->eventName))
				? $item->eventName
				: lang('Main.No title');
			$result['href'] = '/event/details/' . $item->event_id;
		}

		if (!empty($item->meeting_id)) {
			$result['entity'] = lang('Meeting.Meeting');
			$result['name'] = (!empty($item->meetingName))
				? $item->meetingName
				: lang('Main.No title');
			$result['href'] = '/meeting/details/' . $item->meeting_id;
		}

		if (!empty($item->task_id) && is_null($item->taskDeleted)
			&& ((int)$item->assigned_user_id === (int)$this->user_id
				|| (int)$item->owner_user_id === (int)$this->user_id)) {
			$result['entity'] = lang('Task.Task');
			$result['name'] = (!empty($item->taskName))
				? $item->taskName
				: lang('Main.No title');

			if ((int)$item->assigned_user_id === (int)$this->user_id) {
				$result['href'] = '/task/assigned-details/' . $item->task_id;
			} else if ((int)$item->owner_user_id === (int)$this->user_id) {
				$result['href'] = '/task/owner-details/' . $item->task_id;
			}
		}

		return $result;
	}
}