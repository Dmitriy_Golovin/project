<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;

class LeaveChatForm extends BaseForm
{
	const NO_ADMIN = 'noAdmin';
	const EXIST_IMPORTANT = 'existImportant';

	public $user_id;
	public $chat_id;
	public $accept;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'accept' => 'required|integer|in_list[0,1]',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function leave()
	{
		$chatModel = new ChatModel();
		$chatMemberModel = new ChatMemberModel();
		$chatMessageModel = new ChatMessageModel();
		$chatTableName = $chatModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();

		$adminCountSubquery = $chatMemberModel
			->select('count(chat_member_id) as adminCount')
			->where('chat_id', $this->chat_id)
			->where('deleted_at', null)
			->whereIn('role', [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN])
			->getCompiledSelect();

		$chat = $chatModel
			->select($chatTableName . '.*')
			->select('chatMember.chat_member_id, chatMember.user_id, chatMember.role')
			->select('(select adminQuery.* from (' . $adminCountSubquery . ') as adminQuery) as adminCount')
			->select('count(messageImportant.chat_message_id) as messageImportantCount')
			->join($chatMemberTableName . ' as chatMember', 'chatMember.chat_id = ' . $chatTableName . '.chat_id and chatMember.user_id = ' . $this->user_id . '  and chatMember.deleted_at is null')
			->join($chatMessageTableName . ' as messageImportant', 'messageImportant.chat_id = ' . $chatTableName . '.chat_id and json_contains(messageImportant.important_chat_member_ids, cast(chatMember.chat_member_id as json), "$") = 1 and json_contains(messageImportant.deleted_chat_member_ids, cast(chatMember.chat_member_id as json), "$") = 0', 'left')
			->where($chatTableName . '.chat_id', $this->chat_id)
			->first();

		if (empty($chat->chat_id)) {
			$this->setError('', lang('Chat.Chat not found'));
			return false;
		}

		$this->_result['chat'] = $chat;
		$this->_result['notice'] = '';

		if (in_array($chat->role, [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN]) && $chat->adminCount <= 1) {
			$this->_result['notice'] = self::NO_ADMIN;
			return true;
		}

		if ($chat->messageImportantCount > 0 && (int)$this->accept === 0) {
			$this->_result['notice'] = self::EXIST_IMPORTANT;
			return true;
		}

		$chatMemberModel->delete($chat->chat_member_id);
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}