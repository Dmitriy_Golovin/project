<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FileMessageModel;
use App\Libraries\DateAndTime\DateAndTime;

class DialogMessageDetailsItemForm extends BaseForm
{
	public $user_id;
	public $user_to;
	public $user_from;
	public $message_id;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'user_to' => 'required|integer',
		'user_from' => 'required|integer',
		'message_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getItem()
	{
		if ((int)$this->user_to !== (int)$this->user_id && (int)$this->user_from !== (int)$this->user_id) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$messageModel = new MessageModel();
		$messageQuery = $messageModel
			->select(MessageModel::table() . '.*')
			->select('concat(userTable.first_name, " ", userTable.last_name) as user_from_fullname')
			->select('fileTable.url as user_from_ava')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . MessageModel::table() . '.user_from')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where('message_id', $this->message_id)
			->where('user_to', $this->user_to)
			->where('user_from', $this->user_from);

		if ((int)$this->user_from === (int)$this->user_id) {
			$messageQuery->where('delete_from', MessageModel::MESSAGE_NOT_DELETED);
		}

		if ((int)$this->user_to === (int)$this->user_id) {
			$messageQuery->where('delete_to', MessageModel::MESSAGE_NOT_DELETED);
		}

		$this->_result = $messageQuery->first();

		if (empty($this->_result)) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$fileMessageModel = new FileMessageModel();
		$fileModel = new FileModel();
		$fileListQuery = $fileMessageModel
			->select('fileTable.file_id, fileTable.type, fileTable.url, fileTable.original_name AS name, fileTable.preview_height, fileTable.preview_width, fileTable.related_item, fileTable.item_id, fileTable.privacy_status')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . FileMessageModel::table() . '.file_id')
			->where('message_id', $this->_result->message_id)
			->get();

		foreach ($fileListQuery->getResultObject() as $file) {
			if (!$fileModel->getFilePermission($file, $this->user_id)) {
				$file = 'notAllowed';
				$this->_result->file_list[] = $file;
				continue;
			}

			$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
			$file->name = FileModel::getFileNameWithoutExec($file->name);
			$this->_result->file_list[] = $file;
		}

		return true;
	}

	public function getResult()
	{
		$this->_result->date = DateAndTime::getDateTime($this->_result->created_at, $this->timeOffsetSeconds, 'genetive');
		$this->_result->user_from_ava = !empty($this->_result->user_from_ava)
			? FileModel::preparePathToAva($this->_result->user_from_ava)
			: FileModel::DEFAULT_AVA_URL;
		$this->_result->selfMessage = false;
		$this->_result->self_message_not_read = false;

		if ($this->_result->user_from === $this->user_id) {
			$this->_result->selfMessage = true;
			$this->_result->important = (int)$this->_result->important_from;

			if ((int)$this->_result->status === 1) {
				$this->_result->self_message_not_read = true;
			} else {
				$this->_result->self_message_not_read = false;
			}
		}

		if ($this->_result->user_to === $this->user_id) {
			$this->_result->important = (int)$this->_result->important_to;
		}

		return $this->_result;
	}
}