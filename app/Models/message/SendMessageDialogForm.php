<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FileMessageModel;
use Common\Models\PrivateFilePermissionModel;
use App\Libraries\DateAndTime\DateAndTime;

class SendMessageDialogForm extends BaseForm
{
	public $user_id;
	public $user_to;
	public $text;
	public $fileList;
	public $timeOffsetSeconds;
	public $selfAvatar;
	public $firstName;
	public $lastName;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'user_to' => 'required|integer',
		'text' => 'permit_empty|string|required_without[fileList]',
		'fileList.*' => 'permit_empty|integer|required_without[text]',
		'timeOffsetSeconds' => 'required|integer',
		'selfAvatar' => 'required|string',
		'firstName' => 'required|string',
		'lastName' => 'required|string',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function send()
	{
		$userModel = new UserModel();
		$userTo = $userModel
			->where('user_id', $this->user_to)
			->first();

		if (empty($userTo)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->user_to]));
			return false;
		}

		$messageModel = new MessageModel();
		$messageData = (object)[
			'user_from' => $this->user_id,
			'user_to' => $this->user_to,
			'text' => $this->text,
		];
		$messageModel->db->transBegin();

		if (!$messageModel->save($messageData)) {
			$this->setError('', lang('Message.Failed to save message'));
			$messageModel->db->transRollback();
			return false;
		}

		$message = $messageModel->find($messageModel->getInsertID());
		$fileList = [];

		if (!empty($this->fileList)) {
			$fileModel = new FileModel();
			$fileMessageModel = new FileMessageModel();
			$privateFilePermissionModel = new PrivateFilePermissionModel();

			foreach ($this->fileList as $fileId) {
				$file = $fileModel->find($fileId);

				if (empty($file)) {
					$this->setError('', lang('File.File not found'));
					$messageModel->db->transRollback();
					return false;
				}
				
				$fileMessageData = [
					'message_id' => $message->message_id,
					'file_id' => $file->file_id,
				];

				if (!$fileMessageModel->save($fileMessageData)) {
					$this->setError('', lang('Message.Failed to save message file'));
					$messageModel->db->transRollback();
					return false;
				}

				if ($file->privacy_status == FileModel::PRIVATE_STATUS) {
					$privateFilePermission = $privateFilePermissionModel
						->where('file_id', $file->file_id)
						->where('owner_id', $this->user_id)
						->where('permitted_id', $this->user_to)
						->first();

					if (empty($privateFilePermission)) {
						$privateFilePermissionData = [
							'owner_id' => $this->user_id,
							'permitted_id' => $this->user_to,
							'file_id' => $file->file_id,
						];

						if (!$privateFilePermissionModel->save($privateFilePermissionData)) {
							$this->setError('', lang('PrivateFilePermission.Failed to save permissions to view the file'));
							$messageModel->db->transRollback();
							return false;
						}
					}
				}

				$file->name = $file->original_name;
				$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
				unset($file->original_name);
				$fileList[] = $file;
			}
		}

		$messageModel->db->transCommit();
		$message->file_list = $fileList;
		$message->user_from_fullname = $this->firstName . ' ' . $this->lastName;
		$message->user_from_ava = $this->selfAvatar;
		$message->self_message_not_read = true;
		$message->selfMessage = true;
		$message->date = DateAndTime::getDateTime($message->created_at, $this->timeOffsetSeconds, 'genetive');
		$message->important = (int)$message->important_from;
		$this->_result['message'] = $message;
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}