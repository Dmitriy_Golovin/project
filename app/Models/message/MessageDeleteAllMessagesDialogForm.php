<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;

class MessageDeleteAllMessagesDialogForm extends BaseForm
{
	public $accept;
	public $with_user_id;
	public $user_id;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'accept' => 'required|integer|in_list[0,1]',
		'with_user_id' => 'required|integer',
		'user_id' => 'required|integer',
	];

	public function delete() {
		if (!$this->validate()) {
			return false;
		}

		$messageModel = new MessageModel();
		$dialogMessages = $messageModel->getDialogMessages($this->with_user_id, $this->user_id);

		if (empty($dialogMessages)) {
			return false;
		}

		foreach ($dialogMessages as $message) {
			if ($this->user_id == $message->user_from) {
				if ((int)$message->important_from === MessageModel::MESSAGE_IMPORTANT && (int)$message->delete_from === MessageModel::MESSAGE_NOT_DELETED && !$this->accept) {
					return 'asImportant';
				}
			}

			if ($this->user_id == $message->user_to) {
				if ((int)$message->important_to === MessageModel::MESSAGE_IMPORTANT && (int)$message->delete_to === MessageModel::MESSAGE_NOT_DELETED  && !$this->accept) {
					return 'asImportant';
				}
			}
		}

		foreach ($dialogMessages as $message) {
			if ($this->user_id == $message->user_from) {
				$message->delete_from = MessageModel::MESSAGE_DELETED;
			}

			if ($this->user_id == $message->user_to) {
				$message->delete_to = MessageModel::MESSAGE_DELETED;
			}

			if (!$messageModel->save($message)) {
				$this->setError('', $messageModel->errors());
				return false;
			}
		}

		return true;
	}
}