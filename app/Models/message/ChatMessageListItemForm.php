<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;
use Common\Models\FileChatMessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class ChatMessageListItemForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getItem()
	{
		$chatMemberModel = new ChatMemberModel();
		$chatMember = $chatMemberModel
			->where('chat_id', $this->chat_id)
			->where('user_id', $this->user_id)
			->first();

		if (empty($chatMember)) {
			$this->setError('', lang('ChatMember.You are not a member of this chat'));
			return false;
		}

		$chatModel = new ChatModel();
		$chat = $chatModel->find($this->chat_id);

		if (empty($chat)) {
			$this->setError('', lang('Chat.Chat not found'));
			return false;
		}

		$chatMessageModel = new ChatMessageModel();
		$chatMemberModel = new ChatMemberModel();
		$userModel = new UserModel();
		$fileModel = new FileModel();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$fileChatMessageModel = new FileChatMessageModel();
		$chatTableName = $chatModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();
		$fileChatMessageTableName = $fileChatMessageModel->getTableName();

		$subQueryMemberChat = $chatMemberModel
			->select('concat(' . $chatMemberTableName . '.chat_member_id, "") as chat_member_id')
			->where($chatTableName . '.chat_id = ' . $chatMemberTableName . '.chat_id')
			->where('user_id', $this->user_id)
			->where('deleted_at', null)
			->getCompiledSelect();

		$subQueryNewMessageCount = $chatMessageModel
			->select('count(' . $chatMessageTableName . '.chat_message_id)')
			->join($chatTableName, $chatTableName . '.chat_id = ' . $chatMessageTableName . '.chat_id')
			->join($chatMemberTableName . ' as chatMember', 'chatMember.chat_id = ' . $chatTableName . '.chat_id')
			->join($chatMemberTableName . ' as messageSender', 'messageSender.chat_member_id = ' . $chatMessageTableName . '.chat_member_id')
			->where('chatMember.user_id', $this->user_id)
			->where('messageSender.user_id !=', $this->user_id)
			->where($chatMessageTableName . '.chat_id', $this->chat_id)
			->where($chatMessageTableName . '.deleted_at', null)
			->where('not json_contains(who_viewed_chat_member_ids, (select chatMemberId.chat_member_id from (' . $subQueryMemberChat . ') as chatMemberId), \'$\')')
			->where('not json_contains(deleted_chat_member_ids, (select chatMemberId.chat_member_id from (' . $subQueryMemberChat . ') as chatMemberId), \'$\')')
			->getCompiledSelect();

		$this->_result = $chatMessageModel
			->select($chatMessageTableName . '.*, (' . $subQueryNewMessageCount . ') as newMessageCount, ' . $chatTableName  . '.chat_id, ' . $chatTableName . '.title, chatFile.url as chatAva, chatFile.type as chatAvaType, ' . $userTableName . '.user_id, concat(' . $userTableName . '.first_name, " ", ' . $userTableName . '.last_name) as fullName, userFile.url as userAva, userFile.type as userAvaType, (select count(' . $fileChatMessageTableName . '.file_chat_message_id) from ' . $fileChatMessageTableName . ' where ' . $fileChatMessageTableName . '.chat_message_id = ' . $chatMessageTableName . '.chat_message_id) as fileCount')
			->join($chatTableName, $chatTableName . '.chat_id = ' . $chatMessageTableName . '.chat_id')
			->join($fileTableName . ' as chatFile', 'chatFile.file_id = ' . $chatTableName . '.file_id', 'left')
			->join($chatMemberTableName, $chatMemberTableName . '.chat_member_id = ' . $chatMessageTableName . '.chat_member_id')
			->join($userTableName, $userTableName . '.user_id = ' . $chatMemberTableName . '.user_id')
			->join($fileTableName . ' as userFile', 'userFile.file_id = ' . $userTableName . '.file_id', 'left')
			->where($chatMessageTableName . '.chat_id', $this->chat_id)
			->where('not json_contains(' . $chatMessageTableName . '.deleted_chat_member_ids, (select chatMemberId.chat_member_id from (' . $subQueryMemberChat . ') as chatMemberId), \'$\')')
			->orderBy($chatMessageTableName . '.created_at', 'desc')
			->limit(1)
			->first();

		$this->_result->chatMemberIdNotSender = $chatMember->chat_member_id;
		return true;
	}

	public function getResult()
	{
		$this->_result->chatAva = empty($this->_result->chatAva)
			? FileModel::DEFAULT_CHAT_AVA_URL
			: FileModel::preparePathToUserFile($this->_result->chatAva, $this->_result->chatAvaType);
		$this->_result->userAva = empty($this->_result->userAva)
			? FileModel::DEFAULT_AVA_URL
			: FileModel::preparePathToAva($this->_result->userAva);
		$this->_result->date = DateAndTime::getDateTime($this->_result->created_at, $this->timeOffsetSeconds, 'genetive');

		if (($this->_result->user_id != $this->user_id && !empty($this->_result->who_viewed_chat_member_ids) && !in_array($this->_result->chatMemberIdNotSender, json_decode($this->_result->who_viewed_chat_member_ids)))) {
			$this->_result->notReadMessage = true;
		} else {
			$this->_result->notReadMessage = false;
		}

		return $this->_result;
	}
}