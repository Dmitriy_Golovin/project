<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;

class MessageImportantForm extends BaseForm
{
	public $important;
	public $messages_id;
	public $user_id;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'important' => 'required|integer|in_list[0,1]',
		'messages_id' => 'required|integer',
		'user_id' => 'required|integer',
	];

	public function toggleImportant()
	{
		$messageModel = new MessageModel();
		$message = $messageModel->getMessageByIdAndUserId($this->messages_id, $this->user_id);

		if (empty($message)) {
			return false;
		}

		if ($this->user_id == $message->user_from) {
			$message->important_from = $this->important;
		} else {
			$message->important_to = $this->important;
		}

		if (!$messageModel->save($message)) {
			$this->setError('', $messageModel->errors());
			return false;
		}

		return true;
	}
}