<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMessageModel;
use Common\Models\ChatMemberModel;

class MessageDeleteOneMessageChatForm extends BaseForm
{
	public $user_id;
	public $chat_message_id;
	public $chat_id;
	public $accept;

	protected $validationRules = [
		'accept' => 'required|integer|in_list[0,1]',
		'chat_message_id' => 'required|integer',
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function delete()
	{
		$chatModel = new ChatModel();
		$chatMessageModel = new ChatMessageModel();
		$chatMemberModel = new ChatMemberModel();
		$chatTableName = $chatModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();

		$chatMessage = $chatMessageModel
			->select($chatMessageTableName . '.*')
			->select('chatMember.chat_member_id as self_chat_member_id')
			->join($chatTableName . ' as chatTable', 'chatTable.chat_id = ' . $chatMessageTableName . '.chat_id and chatTable.chat_id = ' . $this->chat_id)
			->join($chatMemberTableName . ' as chatMember', 'chatMember.chat_id = chatTable.chat_id and chatMember.user_id = ' . $this->user_id)
			->where('chat_message_id', $this->chat_message_id)
			->where('chatTable.deleted_at', null)
			->where('chatMember.deleted_at', null)
			->first();

		if (empty($chatMessage)) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$importantIdList = json_decode($chatMessage->important_chat_member_ids);
		$deletedIdList = json_decode($chatMessage->deleted_chat_member_ids);

		if (in_array($chatMessage->self_chat_member_id, $deletedIdList)) {
			$this->setError('', lang('Message.message already deleted'));
			return false;
		}

		if (in_array($chatMessage->self_chat_member_id, $importantIdList) && (int)$this->accept === 0) {
			$this->_result['asImportant'] = true;
			$this->_result['messageId'] = $chatMessage->chat_message_id;
			return true;
		}

		array_push($deletedIdList, (int)$chatMessage->self_chat_member_id);
		$chatMessage->deleted_chat_member_ids = json_encode($deletedIdList);

		if (!$chatMessageModel->save($chatMessage)) {
			$this->setError('', lang('ChatMessage.Failed to save chat message'));
			return false;
		}

		$this->_result['messageId'] = $chatMessage->chat_message_id;
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}