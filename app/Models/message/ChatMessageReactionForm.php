<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMessageModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageReactionModel;

class ChatMessageReactionForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $chat_message_id;
	public $reaction;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'chat_message_id' => 'required|integer',
		'reaction' => 'required|string',
	];

	protected $cleanValidationRules = false;

	public function toggleReaction()
	{
		$chatModel = new ChatModel();
		$chatMessageModel = new ChatMessageModel();
		$chatMemberModel = new ChatMemberModel();
		$chatTableName = $chatModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();

		$chatMember = $chatMemberModel
			->where('chat_id', $this->chat_id)
			->where('user_id', $this->user_id)
			->first();

		if (empty($chatMember)) {
			$this->setError('', lang('ChatMember.You are not a member of this chat'));
			return false;
		}

		$chatMessage = $chatMessageModel
			->join($chatTableName . ' as chatTable', 'chatTable.chat_id = ' . $chatMessageTableName . '.chat_id')
			->join($chatMemberTableName . ' as chatMember', 'chatMember.chat_id = chatTable.chat_id')
			->where($chatMessageTableName . '.chat_id', $this->chat_id)
			->where($chatMessageTableName . '.chat_message_id', $this->chat_message_id)
			->where('not json_contains(deleted_chat_member_ids, "' . $chatMember->chat_member_id . '", "$")')
			->where('chatTable.deleted_at', null)
			->where('chatMember.deleted_at', null)
			->first();

		if (empty($chatMessage)) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$chatMessageReactionModel = new ChatMessageReactionModel();	
		$findReaction = false;
		$reactionData = [];
		$reactionList = $chatMessageReactionModel
			->select('chat_message_reaction_id, chat_message_id, chat_member_id, reaction')
			->where('chat_message_id', $this->chat_message_id)
			->where('chat_member_id', $chatMember->chat_member_id)
			->get()
			->getResultObject();

		if (empty($reactionList)) {
			$reactionData['chat_message_id'] = $this->chat_message_id;
			$reactionData['chat_member_id'] = $chatMember->chat_member_id;
			$reactionData['reaction'] = $this->reaction;

			if (!$chatMessageReactionModel->save($reactionData)) {
				$this->setError('', lang('ChatMessage.Failed to save reaction to message'));
				return false;
			}

			return true;
		}

		foreach ($reactionList as $reaction) {
			if ($findReaction) {
				$chatMessageReactionModel->delete($reaction->chat_message_reaction_id);
				continue;
			}

			if ($reaction->reaction === $this->reaction) {
				$chatMessageReactionModel->delete($reaction->chat_message_reaction_id);
				$findReaction = true;
			} else {
				$reaction->reaction = $this->reaction;

				if (!$chatMessageReactionModel->save($reaction)) {
					$this->setError('', lang('ChatMessage.Failed to save reaction to message'));
					return false;
				}

				$findReaction = true;
			}
		}

		return true;
	}

	public function getResult()
	{
		$chatMessageReactionModel = new ChatMessageReactionModel();
		$chatMemberModel = new ChatMemberModel();
		$reactionList = $chatMessageReactionModel->getMessageReactionList($this->chat_message_id);
		$userIdList = $chatMemberModel->getMemberUserIdListWithoutSelf($this->chat_id, $this->user_id);

		return [
			'reactionList' => $reactionList,
			'userIdList' => $userIdList,
			'chatId' => $this->chat_id,
			'chatMessageId' => $this->chat_message_id
		];
	}
}