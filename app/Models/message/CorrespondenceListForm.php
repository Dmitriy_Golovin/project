<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FileMessageModel;
use Common\Models\ChatModel;
use Common\Models\ChatMessageModel;
use Common\Models\ChatMemberModel;
use Common\Models\FileChatMessageModel;
use App\Libraries\DateAndTime\DateAndTime;
use CodeIgniter\Database\BaseBuilder;

class CorrespondenceListForm extends BaseForm
{
	public $user_id;
	public $timeOffsetSeconds;
	public $selfAvatar;
	public $firstName;
	public $lastName;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'selfAvatar' => 'required|string',
		'firstName' => 'required|string',
		'lastName' => 'required|string',
	];

	protected $cleanValidationRules = false;

	private $_resultDialog;
	private $_resultChat;

	public function getList()
	{
		$messageModel = new MessageModel();
		$userModel = new UserModel();
		$fileModel = new FileModel();
		$chatModel = new ChatModel();
		$chatMessageModel = new ChatMessageModel();
		$chatMemberModel = new ChatMemberModel();
		$fileChatMessageModel = new FileChatMessageModel();
		$messageTableName = $messageModel->getTableName();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();
		$chatTableName = $chatModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$fileChatMessageTableName = $fileChatMessageModel->getTableName();

		$queryAsRecipient = $messageModel
			->select('message_id, url, user_from, user_to, text, ' . $messageTableName . '.created_at, status, status as status_message, ' . $userTableName . '.first_name, ' . $userTableName . '.last_name, user_to as `to`, user_from as `from`, ' . $userTableName . '.deleted_at as userDeleted')
			->join($userTableName, $messageTableName . '.user_from = ' . $userTableName . '.user_id')
			->join($fileTableName, $userTableName . '.file_id = ' . $fileTableName . '.file_id', 'left')
			->where('user_to', $this->user_id)
			->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
			->getCompiledSelect();

		$queryAsSender = $messageModel
			->select('message_id, url, user_to, user_from, text, ' . $messageTableName . '.created_at, 0, status as status_message, ' . $userTableName . '.first_name, ' . $userTableName . '.last_name, user_to as `to`, user_from as `from`, ' . $userTableName . '.deleted_at as userDeleted')
			->join($userTableName, $messageTableName . '.user_to = ' . $userTableName . '.user_id')
			->join($fileTableName, $userTableName . '.file_id = ' . $fileTableName . '.file_id', 'left')
			->where('user_from', $this->user_id)
			->where('delete_from', MessageModel::MESSAGE_NOT_DELETED)
			->getCompiledSelect();

		$this->_resultDialog = $messageModel->db->query('select message_id, MAX(created_at) as sortDate, MAX(created_at) as date, sum(status) as new_messages_from, text, user_from, `from`, `to`, first_name, last_name, status_message, url as fromAva, userDeleted from (' . $queryAsRecipient . ' union all ' . $queryAsSender . ' order by created_at desc) as talk_list group by user_from order by date desc');

		$subQueryMemberChat = $chatMemberModel
			->select('cast(' . $chatMemberTableName . '.chat_member_id as json) as chat_member_id')
			->select($chatMemberTableName . '.created_at')
			->where(ChatModel::table()  . '.chat_id = ' . $chatMemberTableName . '.chat_id')
			->where('user_id', $this->user_id)
			->getCompiledSelect();

		$subQueryNewMessage = $chatMessageModel
			->select('count(' . $chatMessageTableName . '.chat_message_id)')
			->join($chatMemberTableName . ' as messageSender', 'messageSender.chat_id = ' . $chatMessageTableName . '.chat_id and messageSender.user_id = ' . $this->user_id)
			->where('messageSender.chat_member_id != ' . $chatMessageTableName . '.chat_member_id')
			->where($chatMessageTableName . '.chat_id = ' . ChatModel::table()  . '.chat_id')
			->where($chatMessageTableName . '.created_at >= (select chatMemberId.created_at from (' . $subQueryMemberChat . ') as chatMemberId)')
			->where('not json_contains(who_viewed_chat_member_ids, (select chatMemberId.chat_member_id from (' . $subQueryMemberChat . ') as chatMemberId), \'$\')')
			->where('not json_contains(deleted_chat_member_ids, (select chatMemberId.chat_member_id from (' . $subQueryMemberChat . ') as chatMemberId), \'$\')')
			->getCompiledSelect();

		$this->_resultChat = $chatModel
			->select(ChatModel::table() . '.chat_id, ' . $chatTableName . '.title, chatFile.url as chatAva, chatFile.type as chatAvaType,' . $chatTableName . '.created_at')
			->select('(' . $subQueryNewMessage . ') as newMessageCount')
			->select('chatMember.chat_member_id')
			->join($chatMemberTableName . ' as chatMember', 'chatMember.chat_id = ' . ChatModel::table() . '.chat_id and chatMember.user_id = ' . $this->user_id)
			->join($fileTableName . ' as chatFile', 'chatFile.file_id = ' . $chatTableName . '.file_id', 'left')
			->where('chatMember.deleted_at', null)
			->where($chatTableName . '.deleted_at', null)
			->groupBy('chat_id')
			->get();
			
		return true;
	}

	public function getResult()
	{
		$result = [];
		$fileMessageModel = new FileMessageModel();
		$messageModel = new MessageModel();
		$userModel = new UserModel();
		$fileModel = new FileModel();
		$chatModel = new ChatModel();
		$chatMemberModel = new ChatMemberModel();
		$chatMessageModel = new ChatMessageModel();
		$fileMessageTableName = $fileMessageModel->getTableName();
		$messageTableName = $messageModel->getTableName();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$chatTableName = $chatModel->getTableName();

		foreach ($this->_resultDialog->getResultObject() as &$item) {
			if ($item->new_messages_from > 0 || ($item->from == $this->user_id && $item->status_message == 1)) {
				$item->notReadMessage = true;
			} else {
				$item->notReadMessage = false;
			}

			if ($item->from === $this->user_id) {
				$item->selfMessage = true;
			} else {
				$item->selfMessage = false;
			}

			$item->newFileCount = $fileMessageModel
				->select('file_message_id')
				->where('message_id', $item->message_id)
				->countAllResults();

			if (is_null($item->userDeleted)) {
				$item->fromAva = !empty($item->fromAva) ? FileModel::preparePathToAva($item->fromAva) : FileModel::DEFAULT_AVA_URL;
			} else {
				$item->fromAva = FileModel::DEFAULT_AVA_URL;
			}

			$item->selfAva = $this->selfAvatar;
			$item->selfFullName = $this->firstName . ' ' . $this->lastName;
			$item->date = DateAndTime::getDateTime($item->date, $this->timeOffsetSeconds, 'genetive');
			$item->messageType = 'dialog';

			$result[] = $item;
		}

		foreach ($this->_resultChat->getResultObject() as &$item) {
			$message = $chatMessageModel
				->select('(select count(fileChatMessage.file_chat_message_id) as count from ' . FileChatMessageModel::table() . ' as fileChatMessage where fileChatMessage.chat_message_id = ' . ChatMessageModel::table() . '.chat_message_id) as fileCount')
				->select(ChatMessageModel::table() . '.chat_message_id, ' . ChatMessageModel::table() . '.text, ' . ChatMessageModel::table() .  '.who_viewed_chat_member_ids, ' . ChatMessageModel::table() . '.deleted_chat_member_ids, ' . ChatMessageModel::table() . '.recipient_chat_member_ids, ' . ChatMessageModel::table() . '.important_chat_member_ids, ' . ChatMessageModel::table() . '.created_at')
				->where(ChatMessageModel::table() . '.chat_id', $item->chat_id)
				->where('not json_contains(' . ChatMessageModel::table() . '.deleted_chat_member_ids, \'' . $item->chat_member_id . '\', \'$\')')
				->orderBy(ChatMessageModel::table() . '.created_at', 'DESC')
				->limit(1)
				->get()
				->getResultObject();

			$item->fileCount = (!empty($message[0])) ? $message[0]->fileCount : null;
			$item->chat_message_id = (!empty($message[0])) ? $message[0]->chat_message_id : null;
			$item->text = (!empty($message[0])) ? $message[0]->text : null;
			$item->who_viewed_chat_member_ids = (!empty($message[0])) ? $message[0]->who_viewed_chat_member_ids : null;
			$item->deleted_chat_member_ids = (!empty($message[0])) ? $message[0]->deleted_chat_member_ids : null;
			$item->recipient_chat_member_ids = (!empty($message[0])) ? $message[0]->recipient_chat_member_ids : null;
			$item->important_chat_member_ids = (!empty($message[0])) ? $message[0]->important_chat_member_ids : null;
			$item->date = (!empty($message[0]->created_at)) ? $message[0]->created_at : $item->created_at;
			$item->sortDate = (!empty($message[0]->created_at)) ? $message[0]->created_at : $item->created_at;

			if (empty($item->chat_message_id)) {
				$user = $userModel
					->select($userTableName . '.user_id, concat(' . $userTableName . '.first_name, " ", ' . $userTableName . '.last_name) as fullName,' . $userTableName . '.deleted_at')
					->select('userFile.url as userAva, userFile.type as userAvaType')
					->join($fileTableName . ' as userFile', 'userFile.file_id = ' . $userTableName . '.file_id', 'left')
					->join($chatMemberTableName . ' as chatMemberCreater', 'chatMemberCreater.user_id = ' . $userTableName . '.user_id and chatMemberCreater.role = ' . ChatMemberModel::ROLE_CREATER)
					->where('chatMemberCreater.chat_id = ' . $item->chat_id)
					->withDeleted()
					->first();

				if (empty($user)) {
					var_dump($item);die;
				}

				$item->text = $user->fullName . ' ' . lang('Chat.created a chat');
			} else {
				$user = $userModel
					->select($userTableName . '.user_id, concat(' . $userTableName . '.first_name, " ", ' . $userTableName . '.last_name) as fullName, ' . $userTableName . '.deleted_at')
					->select('userFile.url as userAva, userFile.type as userAvaType')
					->join($fileTableName . ' as userFile', 'userFile.file_id = ' . $userTableName . '.file_id', 'left')
					->join($chatMemberTableName . ' as chatMemberSender', 'chatMemberSender.user_id = ' . $userTableName . '.user_id')
					->join($chatMessageTableName, $chatMessageTableName . '.chat_member_id = chatMemberSender.chat_member_id')
					->where($chatMessageTableName . '.chat_message_id', $item->chat_message_id)
					->withDeleted()
					->first();
			}

			$item->chatAva = empty($item->chatAva)
				? FileModel::DEFAULT_CHAT_AVA_URL
				: FileModel::preparePathToUserFile($item->chatAva, $item->chatAvaType);
			$item->fullName = $user->fullName;

			if (empty($user->deleted_at)) {
				$item->userAva = empty($user->userAva)
					? FileModel::DEFAULT_AVA_URL
					: FileModel::preparePathToAva($user->userAva);
			} else {
				$item->userAva = FileModel::DEFAULT_AVA_URL;
			}

			$item->date = DateAndTime::getDateTime($item->date, $this->timeOffsetSeconds, 'genetive');
			$item->user_id = $user->user_id;

			if ($item->user_id != $this->user_id && !empty($item->who_viewed_chat_member_ids) && !in_array((int)$item->chat_member_id, json_decode($item->who_viewed_chat_member_ids))) {
				$item->notReadMessage = true;
			} else {
				$item->notReadMessage = false;
			}
			$item->messageType = 'chat';

			$result[] = $item;
		}

		helper('array');
		array_sort_by_multiple_keys($result, [
		    'sortDate' => SORT_DESC,
		]);

		return ['modelResult' => $result];
	}
}