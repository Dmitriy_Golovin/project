<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;

class MarkChatMessageAsReadForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $idList;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'idList' => 'required|valid_json',
	];

	protected $cleanValidationRules = false;

	private $_chat;
	private $_messageDataList;

	public function markMessage()
	{
		$chatModel = new ChatModel();
		$chatMemberModel = new ChatMemberModel();
		$chatMessageModel = new ChatMessageModel();
		$chatTableName = $chatModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$this->_chat = $chatModel
			->select($chatTableName .'.*')
			->select($chatMemberTableName . '.chat_member_id')
			->join($chatMemberTableName, $chatMemberTableName . '.chat_id = ' . $chatTableName . '.chat_id')
			->where($chatTableName . '.chat_id', $this->chat_id)
			->where($chatMemberTableName . '.user_id', $this->user_id)
			->where($chatTableName . '.deleted_at', null)
			->where($chatMemberTableName . '.deleted_at', null)
			->first();

		if (empty($this->_chat)) {
			$this->setError('', lang('Chat.Chat not found'));
			return false;
		}

		$this->idList = json_decode($this->idList);
		$chatModel->db->transBegin();

		foreach ($this->idList as $id) {
			$chatMessage = $chatMessageModel
				->where('chat_message_id', $id)
				->where('chat_id', $this->_chat->chat_id)
				->where('not json_contains(deleted_chat_member_ids, "' . $this->_chat->chat_member_id . '", "$")')
				->where('not json_contains(who_viewed_chat_member_ids, "' . $this->_chat->chat_member_id . '", "$")')
				->first();

			if (empty($chatMessage)) {
				$this->setError('', lang('ChatMessage.Message not found'));
				$chatModel->db->transRollback();
				return false;
			}

			$whoViewedIdList = json_decode($chatMessage->who_viewed_chat_member_ids);
			array_push($whoViewedIdList, (int)$this->_chat->chat_member_id);
			$chatMessage->who_viewed_chat_member_ids = json_encode($whoViewedIdList);

			if (!$chatMessageModel->save($chatMessage)) {
				$this->setError('', lang('ChatMessage.Failed to save chat message'));
				$chatModel->db->transRollback();
				return false;
			}

			$messageData['id'] = $id;
			$messageData['viewCount'] = count($whoViewedIdList);
			$this->_messageDataList[] = $messageData;
		}

		$chatModel->db->transCommit();
		return true;
	}

	public function getResult()
	{
		$chatMemberModel = new ChatMemberModel();
		$memberIdList = [];
		$chatMemberList = $chatMemberModel->getMemberList($this->_chat->chat_id);

		foreach ($chatMemberList as $chatMember) {
			if ($chatMember->user_id !== $this->user_id) {
				$memberIdList[] = $chatMember->user_id;
			}
		}

		return [
			'messageDataList' => $this->_messageDataList,
			'chatMemberList' => $memberIdList,
			'userId' => $this->user_id,
			'chatId' => $this->chat_id,
		];
	}
}