<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMessageModel;
use Common\Models\ChatMemberModel;

class ChatMessageImportantForm extends BaseForm
{
	const NOT_IMPORTANT = 0;
	const IMPORTANT = 1;

	public $user_id;
	public $chat_message_id;
	public $chat_id;
	public $important;

	protected $validationRules = [
		'important' => 'required|integer|in_list[0,1]',
		'chat_message_id' => 'required|integer',
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	public function toggleImportant()
	{
		$chatModel = new ChatModel();
		$chatMessageModel = new ChatMessageModel();
		$chatMemberModel = new ChatMemberModel();
		$chatTableName = $chatModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();

		$chatMessage = $chatMessageModel
			->select($chatMessageTableName . '.*')
			->select('chatMember.chat_member_id as self_chat_member_id')
			->join($chatTableName . ' as chatTable', 'chatTable.chat_id = ' . $chatMessageTableName . '.chat_id and chatTable.chat_id = ' . $this->chat_id)
			->join($chatMemberTableName . ' as chatMember', 'chatMember.chat_id = chatTable.chat_id and chatMember.user_id = ' . $this->user_id)
			->where('chat_message_id', $this->chat_message_id)
			->where('chatTable.deleted_at', null)
			->where('chatMember.deleted_at', null)
			->first();

		if (empty($chatMessage)) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$importantIdList = json_decode($chatMessage->important_chat_member_ids);

		if ((int)$this->important === self::IMPORTANT) {
			if (in_array($chatMessage->self_chat_member_id, $importantIdList)) {
				$this->setError('', lang('Main.an error has occurred'));
				return false;
			}

			array_push($importantIdList, (int)$chatMessage->self_chat_member_id);
			$chatMessage->important_chat_member_ids = json_encode($importantIdList);
		}

		if ((int)$this->important === self::NOT_IMPORTANT) {
			if (!in_array($chatMessage->self_chat_member_id, $importantIdList)) {
				$this->setError('', lang('Main.an error has occurred'));
				return false;
			}

			$newImpotantIdList = [];

			foreach ($importantIdList as $item) {
				if ((int)$item !== (int)$chatMessage->self_chat_member_id) {
					array_push($newImpotantIdList, (int)$item);
				}
			}
			
			$chatMessage->important_chat_member_ids = json_encode($newImpotantIdList);
		}

		if (!$chatMessageModel->save($chatMessage)) {
			$this->setError('', lang('ChatMessage.Failed to save chat message'));
			return false;
		}

		return true;
	}
}