<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;
use Common\Models\FileChatMessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\PrivateFilePermissionModel;
use App\Libraries\DateAndTime\DateAndTime;

class SendMessageChatForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $text;
	public $fileList;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'text' => 'permit_empty|string|required_without[fileList]',
		'fileList.*' => 'permit_empty|integer|required_without[text]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function send()
	{
		$chatModel = new ChatModel();
		$fileModel = new FileModel();
		$fileTableName = $fileModel->getTableName();
		$chatTableName = $chatModel->getTableName();
		$chat = $chatModel
			->select($chatTableName . '.*, ' . $fileTableName . '.url as fileUrl, ' . $fileTableName . '.type')
			->join($fileTableName, $chatTableName . '.file_id = ' . $fileTableName . '.file_id', 'left')
			->where($chatTableName . '.chat_id', $this->chat_id)
			->first();

		if (empty($chat)) {
			$this->setError('', lang('Chat.Chat not found'));
			return false;
		}

		$this->_result['chat'] = $chat;
		$chatMemberModel = new ChatMemberModel();
		$userModel = new UserModel();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$chatMember = $chatMemberModel
			->select($chatMemberTableName . '.*, concat(' . $userTableName . '.first_name, " ", ' . $userTableName . '.last_name) as fullName, ' . $fileTableName . '.url as path_to_ava')
			->join($userTableName, $chatMemberTableName . '.user_id = ' . $userTableName . '.user_id')
			->join($fileTableName, $userTableName . '.file_id = ' . $fileTableName . '.file_id', 'left')
			->where('chat_id', $this->chat_id)
			->where($chatMemberTableName . '.user_id', $this->user_id)
			->first();

		if (empty($chatMember)) {
			$this->setError('', lang('ChatMember.You are not a member of this chat'));
			return false;
		}

		$chatMember->path_to_ava = !empty($chatMember->path_to_ava)
			? FileModel::preparePathToAva($chatMember->path_to_ava)
			: FileModel::DEFAULT_AVA_URL;
		$chatMessageModel = new ChatMessageModel();
		$chatMessageModel->db->transBegin();
		$chatMessageData = [
			'chat_id' => $chat->chat_id,
			'chat_member_id' => $chatMember->chat_member_id,
			'text' => $this->text,
			'recipient_chat_member_ids' => json_encode([]),
			'who_viewed_chat_member_ids' => json_encode([]),
			'important_chat_member_ids' => json_encode([]),
			'deleted_chat_member_ids' => json_encode([]),
		];

		if (!$chatMessageModel->save($chatMessageData)) {
			$this->setError('', lang('ChatMessage.Failed to save chat message'));
			$chatMessageModel->db->transRollback();
			return false;
		}

		$chatMessageId = $chatMessageModel->getInsertID();
		$chatMemberList = $chatMemberModel->getMemberList($chat->chat_id);
		$fileList = [];

		if (!empty($this->fileList)) {
			$fileChatMessageModel = new FileChatMessageModel();
			$privateFilePermissionModel = new PrivateFilePermissionModel();

			foreach ($this->fileList as $fileId) {
				$file = $fileModel->find($fileId);

				if (empty($file)) {
					$this->setError('', lang('File.File not found'));
					$chatMessageModel->db->transRollback();
					return false;
				}

				$this->_result['fileList'][] = $file;
				$fileChatMessageData = [
					'chat_message_id' => $chatMessageId,
					'file_id' => $file->file_id,
				];

				if (!$fileChatMessageModel->save($fileChatMessageData)) {
					$this->setError('', lang('Message.Failed to save message file'));
					$chatMessageModel->db->transRollback();
					return false;
				}

				if ($file->privacy_status == FileModel::PRIVATE_STATUS) {
					foreach ($chatMemberList as $chatMemberItem) {
						if ($chatMemberItem->user_id === $this->user_id) {
							continue;
						}

						$privateFilePermission = $privateFilePermissionModel
							->where('file_id', $file->file_id)
							->where('owner_id', $this->user_id)
							->where('permitted_id', $chatMemberItem->user_id)
							->first();

						if (empty($privateFilePermission)) {
							$privateFilePermissionData = [
								'owner_id' => $this->user_id,
								'permitted_id' => $chatMemberItem->user_id,
								'file_id' => $file->file_id,
							];

							if (!$privateFilePermissionModel->save($privateFilePermissionData)) {
								$this->setError('', lang('PrivateFilePermission.Failed to save permissions to view the file'));
								$chatMessageModel->db->transRollback();
								return false;
							}
						}
					}
				}

				$file->name = $file->original_name;
				$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
				unset($file->original_name);
				$fileList[] = $file;
			}
		}

		$this->_result['memberIdList'] = [];
		$this->_result['chatMessage'] = $chatMessageModel->find($chatMessageId);
		$this->_result['chat'] = $chat;
		$this->_result['sender'] = $chatMember;
		$this->_result['file_list'] = $fileList;

		foreach ($chatMemberList as &$chatMember) {
			if ($chatMember->user_id !== $this->user_id) {
				$this->_result['memberIdList'][] = $chatMember->user_id;
			}
		}

		$this->_result['memberList'] = $chatMemberList;
		$chatMessageModel->db->transCommit();
		return true;
	}

	public function getResult()
	{
		$this->_result['chat']->fileUrl = empty($this->_result['chat']->fileUrl)
			? FileModel::DEFAULT_CHAT_AVA_URL
			: FileModel::preparePathToUserFile($this->_result['chat']->fileUrl, $this->_result['chat']->type);
		$this->_result['chatMessage']->date = DateAndTime::getDateTime($this->_result['chatMessage']->created_at, $this->timeOffsetSeconds, 'genetive');
		$this->_result['chatMessage']->notReadMessage = false;

		return $this->_result;
	}
}