<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;

class ChatMessageViewMemberListForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $chat_message_id;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'chat_message_id' => 'required|integer',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;
	private $_userCount;

	public function getList()
	{
		$chatModel = new ChatModel();
		$chat = $chatModel
			->join(ChatMemberModel::table() . ' as chatMemberTable', 'chatMemberTable.chat_id = ' . ChatModel::table() . '.chat_id')
			->where(ChatModel::table() . '.chat_id', $this->chat_id)
			->where('chatMemberTable.user_id', $this->user_id)
			->first();

		if (empty($chat)) {
			$this->setError('', lang('ChatMember.You are not a member of this chat'));
			return false;
		}

		$chatMemberModel = new ChatMemberModel();

		$this->_result = $chatMemberModel
			->select(ChatMemberModel::table() . '.*')
			->select('concat(userTable.first_name, " ", userTable.last_name) as fullName')
			->select('fileTable.url as path_to_ava')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . ChatMemberModel::table() . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->join(ChatMessageModel::table() . ' as chatMessageTable', 'chatMessageTable.chat_id = ' . ChatMemberModel::table() . '.chat_id')
			->where('chatMessageTable.chat_id', $this->chat_id)
			->where('chatMessageTable.chat_message_id', $this->chat_message_id)
			->where('json_contains(chatMessageTable.who_viewed_chat_member_ids, cast(' . ChatMemberModel::table() . '.chat_member_id as json), \'$\')')
			->orderBy('fullName', 'ASC');

		$this->_userCount = $this->_result->countAllResults(false);

		$this->_result =$this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $chatMemberModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $chatMemberModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['userResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['userCount'] = $this->_userCount;
		$result['type'] = 'chat-member-list';

		foreach ($this->_result->getResultObject() as $item) {
			$item->path_to_ava = (!empty($item->path_to_ava))
				? FileModel::preparePathToAva($item->path_to_ava)
				: FileModel::DEFAULT_AVA_URL;

			$result['userResult'][] = $item;
		}

		return $result;
	}
}