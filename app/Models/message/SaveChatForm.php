<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\FileModel;
use Common\Models\UserModel;

class SaveChatForm extends BaseForm
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';

	public $user_id;
	public $chat_name;
	public $chat_id;
	public $chat_member;
	public $chat_avatar;
	public $remove_avatar;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_name' => 'required|string|max_length[255]',
		'chat_id' => 'permit_empty|integer',
		'chat_member' => 'permit_empty|valid_json',
		'remove_avatar' => 'permit_empty|integer|in_list[0,1]',
		'chat_avatar' => 'permit_empty|max_size[chat_avatar,2048]uploaded[chat_avatar]|is_image[chat_avatar]|ext_in[chat_avatar,png,jpeg,jpg,gif]|mime_in[chat_avatar,image/png,image/jpg,image/jpeg,image/gif]',
	];

	protected $validationMessages = [
        'chat_name' => [
            'required' => 'Validation.The "name" field is required',
            'max_length' => 'Validation.Maximum number of characters 255',
        ],
        'chat_avatar' => [
        	'ext_in' => 'Validation.Invalid file extension',
        	'mime_in' => 'Validation.Invalid file mime type',
        ],
    ];

	protected $cleanValidationRules = false;
	protected $chat;

	public function save()
	{
		$chatModel = new ChatModel();
		$chatMemberModel = new ChatMemberModel();
		$fileModel = new FileModel();
		$chatModel->db->transBegin();

		if ($this->scenario == self::SCENARIO_CREATE) {
			$this->chat = (object)[
				'title' => $this->chat_name,
			];

			if (!$chatModel->save($this->chat)) {
				$this->setError('', lang('Chat.Failed to save chat'));
				$chatModel->db->transRollback();
				return false;
			}

			$this->chat->chat_id = $chatModel->getInsertID();
			$createrData = (object)[
				'chat_id' => $this->chat->chat_id,
				'user_id' => $this->user_id,
				'role' => ChatMemberModel::ROLE_CREATER,
			];

			if (!$chatMemberModel->save($createrData)) {
				$this->setError('', lang('ChatMember.Failed to save chat member'));
				$chatModel->db->transRollback();
				return false;
			}
		}

		if ($this->scenario == self::SCENARIO_UPDATE) {
			if (empty($this->chat_id)) {
				$this->setError('', lang('Chat.The {0} parameter is required', ['chat_id']));
				$chatModel->db->transRollback();
				return false;
			}

			$userMember = $chatMemberModel
				->where('chat_id', $this->chat_id)
				->where('user_id', $this->user_id)
				->first();

			if (!in_array($userMember->role, [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN])) {
				$this->setError('', lang('Validation.You do not have permission to do this'));
				$chatModel->db->transRollback();
				return false;
			}

			$this->chat = $chatModel->find($this->chat_id);

			if (empty($this->chat)) {
				$this->setError('', lang('Chat.Chat not found'));
				$chatModel->db->transRollback();
				return false;
			}

			if (!empty($this->remove_avatar)) {
				$fileModel->delete($this->chat->file_id);
				$this->chat->file_id = null;
			}

			$this->chat->title = $this->chat_name;

			if (!$chatModel->save($this->chat)) {
				$this->setError('', lang('Chat.Failed to save chat'));
				$chatModel->db->transRollback();
				return false;
			}
		}

		if (!is_null($this->chat_avatar)) {
			$avatarDataDisk = FileModel::saveFileOnDisk($this->chat_avatar, 1);

			if ($this->scenario == self::SCENARIO_UPDATE && !empty($this->chat->file_id)) {
				$fileModel->delete($this->chat->file_id);
				$this->chat->file_id = null;
			}

			$fileData = [
				'type' => FileModel::TYPE_IMAGE,
				'related_item' => 'chat',
				'item_id' => $this->chat->chat_id,
				'url' => $avatarDataDisk['fileName'],
				'privacy_status' => FileModel::PUBLIC_STATUS,
				'mime_type' => $avatarDataDisk['mimeType'],
				'original_name' => $avatarDataDisk['originalName'],
				'preview_height' => $avatarDataDisk['imageData']['previewHeight'],
				'preview_width' => $avatarDataDisk['imageData']['previewWidth'],
			];

			if (!$fileModel->save($fileData)) {
				$this->setError('', $fileModel->errors());
				$chatModel->db->transRollback();
				return false;
			}

			$this->chat->file_id = $fileModel->getInsertID();

			if (!$chatModel->save($this->chat)) {
				$this->setError('', lang('Chat.Failed to save chat'));
				$chatModel->db->transRollback();
				return false;
			}
		}

		if (!empty($this->chat_member)) {
			$this->chat_member = json_decode($this->chat_member);
			$userModel = new UserModel();
			$roleList = ($this->scenario === self::SCENARIO_UPDATE)
				? [ChatMemberModel::ROLE_MEMBER, ChatMemberModel::ROLE_ADMIN, ChatMemberModel::ROLE_CREATER]
				: [ChatMemberModel::ROLE_MEMBER, ChatMemberModel::ROLE_ADMIN];
			$notDeletedUserIdlist = [];

			foreach ($this->chat_member as $item) {
				if (!in_array($item->memberRole, $roleList)) {
					$this->setError('', lang('ChatMember.Member role not valid'));
					$chatModel->db->transRollback();
					return false;
				}

				$availableUser = $userModel->find($item->memberId);

				if (empty($availableUser)) {
					$this->setError('', lang('Main.User with id {0} not found', [$item->memberId]));
					$chatModel->db->transRollback();
					return false;
				}

				$existChatMember = $chatMemberModel
					->where('chat_id', $this->chat->chat_id)
					->where('user_id', $item->memberId)
					->withDeleted()
					->first();	

				if (!empty($existChatMember)) {
					$chatMemberData = [
						'chat_id' => $this->chat->chat_id,
						'user_id' => $existChatMember->user_id,
						'role' => $item->memberRole,
						'updated_at' => time(),
						'deleted_at' => null,
					];

					$chatMemberModel->db->table($chatMemberModel->getTableName())
						->where('chat_member_id', $existChatMember->chat_member_id)
						->update($chatMemberData);

					if (!$chatMemberModel->db->affectedRows()) {
						$this->setError('', lang('ChatMember.Failed to save chat member'));
						$chatModel->db->transRollback();
						return false;
					}
				} else {
					$chatMemberData = [
						'chat_id' => $this->chat->chat_id,
						'user_id' => $item->memberId,
						'role' => $item->memberRole,
					];

					if (!$chatMemberModel->save($chatMemberData)) {
						$this->setError('', lang('ChatMember.Failed to save chat member'));
						$chatModel->db->transRollback();
						return false;
					}
				}

				$notDeletedUserIdlist[] = $item->memberId;
			}

			if ($this->scenario === self::SCENARIO_UPDATE) {
				$chatMemberModel->where('chat_id', $this->chat->chat_id)
					->where('deleted_at', null)
					->whereNotIn('user_id', $notDeletedUserIdlist)
					->delete();
			}
		}

		$chatModel->db->transCommit();
		return true;
	}

	public function getResult()
	{
		return $this->chat;
	}
}