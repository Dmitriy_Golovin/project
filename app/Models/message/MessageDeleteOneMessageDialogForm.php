<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;

class MessageDeleteOneMessageDialogForm extends BaseForm
{
	public $accept;
	public $messages_id;
	public $user_id;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'accept' => 'required|integer|in_list[0,1]',
		'messages_id' => 'required|integer',
		'user_id' => 'required|integer',
	];

	public function delete() {
		if (!$this->validate()) {
			return false;
		}

		$messageModel = new MessageModel();
		$message = $messageModel->getMessageByIdAndUserId($this->messages_id, $this->user_id);

		if (empty($message)) {
			return false;
		}

		if ($this->user_id == $message->user_from) {
			if ((int)$message->important_from === MessageModel::MESSAGE_IMPORTANT && !$this->accept) {
				return 'asImportant';
			}

			$message->delete_from = MessageModel::MESSAGE_DELETED;
		}

		if ($this->user_id == $message->user_to) {
			if ((int)$message->important_to === MessageModel::MESSAGE_IMPORTANT && !$this->accept) {
				return 'asImportant';
			}

			$message->delete_to = MessageModel::MESSAGE_DELETED;
		}

		if (!$messageModel->save($message)) {
			$this->setError('', $messageModel->errors());
			return false;
		}

		return true;
	}
}