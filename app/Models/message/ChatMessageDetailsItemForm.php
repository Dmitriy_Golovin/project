<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;
use Common\Models\FileChatMessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;
use Common\Models\ChatMessageReactionModel;

class ChatMessageDetailsItemForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $chat_message_id;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'chat_message_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_chatMember;
	private $_result;

	public function getItem()
	{
		$chatModel = new ChatModel();
		$chatMemberModel = new ChatMemberModel();
		$chatMessageModel = new ChatMessageModel();
		$userModel = new UserModel();
		$fileModel = new FileModel();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$this->_chatMember = $chatMemberModel
			->where('chat_id', $this->chat_id)
			->where('user_id', $this->user_id)
			->first();

		if (empty($this->_chatMember)) {
			$this->setError('', lang('ChatMember.You are not a member of this chat'));
			return false;
		}

		$message = $chatMessageModel
			->select($chatMessageTableName . '.*')
			->select($userTableName . '.user_id, concat(first_name, " ", last_name) as fullName, url as userAva')
			->join($chatMemberTableName, $chatMemberTableName . '.chat_member_id = ' . $chatMessageTableName . '.chat_member_id')
			->join($userTableName, $userTableName . '.user_id = ' . $chatMemberTableName . '.user_id')
			->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id', 'left')
			->where($chatMessageTableName . '.chat_id', $this->chat_id)
			->where($chatMessageTableName . '.chat_message_id', $this->chat_message_id)
			->where('not json_contains(deleted_chat_member_ids, \'' . $this->_chatMember->chat_member_id . '\', \'$\')')
			->first();

		if (empty($message)) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$message->file_list = [];
		$fileChatMessageModel = new FileChatMessageModel();
		$fileChatMessageTableName = $fileChatMessageModel->getTableName();
		$messageFileQuery = $fileChatMessageModel
			->select($fileTableName . '.file_id, ' . $fileTableName . '.type, ' . $fileTableName . '.url, ' . $fileTableName . '.original_name AS name, ' . $fileTableName . '.preview_height, ' . $fileTableName . '.preview_width, ' . $fileTableName . '.related_item, ' . $fileTableName . '.item_id, ' . $fileTableName . '.privacy_status')
			->join($fileTableName, $fileTableName . '.file_id = ' . $fileChatMessageTableName . '.file_id')
			->where('chat_message_id', $this->chat_message_id)
			->get();

		foreach ($messageFileQuery->getResultObject() as &$file) {
			if (!$fileModel->getFilePermission($file, $this->user_id)) {
				$file = 'notAllowed';
				$message->file_list[] = $file;
				continue;
			}

			$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
			$file->name = FileModel::getFileNameWithoutExec($file->name);
			$message->file_list[] = $file;
		}

		$this->_result = $message;
		return true;
	}

	public function getResult()
	{
		if ($this->_result->user_id != $this->user_id && !in_array($this->_result->chat_member_id, json_decode($this->_result->who_viewed_chat_member_ids))) {
			$this->_result->notReadMessage = true;
		} else {
			$this->_result->notReadMessage = false;
		}

		$this->_result->date = DateAndTime::getDateTime($this->_result->created_at, $this->timeOffsetSeconds, 'genetive');
		$this->_result->userAva = !empty($this->_result->userAva) ? FileModel::preparePathToAva($this->_result->userAva) : FileModel::DEFAULT_AVA_URL;

		$importantMessageIdList = !empty($this->_result->important_chat_member_ids) ? json_decode($this->_result->important_chat_member_ids) : [];
		$this->_result->importantMessage = in_array($this->_chatMember->chat_member_id, $importantMessageIdList) ? 1 : 0;
		$viewMessageIdList = !empty($this->_result->who_viewed_chat_member_ids) ? json_decode($this->_result->who_viewed_chat_member_ids) : [];
		$this->_result->viewCount = !empty($viewMessageIdList) ? count($viewMessageIdList) : 0;

		unset($this->_result->created_at);
		return $this->_result;
	}
}