<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;
use Common\Models\UserModel;

class MarkDialogMessageAsReadForm extends BaseForm
{
	public $user_id;
	public $user_from;
	public $idList;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'user_from' => 'required|integer',
		'idList' => 'required|valid_json',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function markMessage()
	{
		$messageModel = new MessageModel();
		$this->idList = json_decode($this->idList);
		$messageModel->db->transBegin();

		foreach ($this->idList as $id) {
			$message = $messageModel
				->where('user_from', $this->user_from)
				->where('user_to', $this->user_id)
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
				->where('status', 1)
				->where('message_id', $id)
				->first();

			if (empty($message)) {
				$this->setError('', lang('ChatMessage.Message not found'));
				$messageModel->db->transRollback();
				return false;
			}

			$message->status = 0;

			if (!$messageModel->save($message)) {
				$this->setError('', lang('Message.Failed to save message'));
				$messageModel->db->transRollback();
				return false;
			}

			$messageData['id'] = $message->message_id;
			$this->_result['messageDataList'][] = $messageData;
		}

		$messageModel->db->transCommit();
		$this->_result['newMessageForSenderCount'] = $messageModel
			->groupStart()
				->where('user_from', $this->user_from)
				->where('user_to', $this->user_id)
				->where('delete_from', MessageModel::MESSAGE_NOT_DELETED)
				->where('status', 1)
			->groupEnd()
			->orGroupStart()
				->where('user_to', $this->user_from)
				->where('user_from', $this->user_id)
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
				->where('status', 1)
			->groupEnd()
			->countAllResults();

		return true;
	}

	public function getResult()
	{
		$this->_result['userId'] = $this->user_id;
		$this->_result['dialogId'] = $this->user_from;
		return $this->_result;
	}
}