<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FileMessageModel;
use App\Libraries\DateAndTime\DateAndTime;
use CodeIgniter\Database\BaseBuilder;

class DialogMessagesDetailsForm extends BaseForm
{
	public $user_id;
	public $with_user_id;
	public $timeOffsetSeconds;
	public $timestamp;
	public $rowNum; // last row in list
	public $selfAvatar;
	public $firstName;
	public $lastName;

	protected $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'with_user_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'timestamp' => 'required|integer',
		'rowNum' => 'permit_empty|integer',
		'selfAvatar' => 'required|string',
		'firstName' => 'required|string',
		'lastName' => 'required|string',
	];

	protected $cleanValidationRules = false;

	private $_messageCount;
	private $_result;

	public function getDetails()
	{
		$messageModel = new MessageModel();
		$userModel = new userModel();
		$fileModel = new FileModel();
		$messageTableName = $messageModel->getTableName();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$messageQuery = $messageModel
			->select('message_id, user_from, user_to, text, ' . $messageTableName . '.created_at as date, status, important_from, important_to, (SELECT CONCAT(first_name, " ", last_name) FROM ' . $userTableName . ' WHERE user_id = ' . $this->with_user_id . ') as user_with_fullname, (SELECT url FROM ' . $fileTableName . ' WHERE file_id = (SELECT file_id FROM ' . $userTableName . ' WHERE user_id = ' . $this->with_user_id . ')) as url, (SELECT deleted_at FROM ' . $userTableName . ' WHERE user_id = ' . $this->with_user_id . ') as userDeleted')
			->groupStart()
				->where('user_to', $this->user_id)
				->where('user_from', $this->with_user_id)
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
				->where($messageTableName . '.created_at <=', $this->timestamp)
			->groupEnd()
			->orGroupStart()
				->where('user_from', $this->user_id)
				->where('user_to', $this->with_user_id)
				->where('delete_from', MessageModel::MESSAGE_NOT_DELETED)
				->where($messageTableName . '.created_at <=', $this->timestamp)
			->groupEnd()
			->orderBy($messageTableName . '.created_at', 'DESC')
			->getCompiledSelect();

		$this->_messageCount = $messageModel->db
			->query('select messageCount.* from (' . $messageQuery . ') as messageCount')
			->getNumRows();

		$messageQuery = 'select (@rowNum := @rowNum + 1) as rowNum, messageQuery.* from (' . $messageQuery . ') as messageQuery, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$messageQuery = 'select messageQuery.* from (' . $messageQuery . ') as messageQuery where rowNum > ' . $messageModel->db->escape($this->rowNum);
		} else {
			$oldNewMessage = $messageModel
				->where('user_to', $this->user_id)
				->where('user_from', $this->with_user_id)
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
				->where('status', 1)
				->limit(1)
				->orderBy($messageTableName . '.created_at', 'ASC')
				->first();

			if (!empty($oldNewMessage)) {
				$countWithNewMessages = $messageModel
					->where($messageTableName . '.created_at >=', $oldNewMessage->created_at)
					->where($messageTableName . '.created_at <=', $this->timestamp)
					->where('user_to', $this->user_id)
					->where('user_from', $this->with_user_id)
					->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
					->countAllResults();

				$this->limit += $countWithNewMessages;
			}
		}

		$this->_result = $messageModel->db->query('select messageList.* from (select messageQuery.* from (' . $messageQuery . ') as messageQuery limit ' . $this->limit . ') as messageList order by messageList.date asc');

		return true;
	}

	private function _getFileListOfMessage($messageId)
	{
		$fileMessageModel = new FileMessageModel();
		$fileModel = new FileModel();
		$fileTableName = $fileModel->getTableName();
		$fileMessageTableName = $fileMessageModel->getTableName();
		$result = [];

		$query = $fileMessageModel
			->select($fileTableName . '.file_id, ' . $fileTableName . '.type, ' . $fileTableName . '.url, ' . $fileTableName . '.original_name AS name, ' . $fileTableName . '.preview_height, ' . $fileTableName . '.preview_width, ' . $fileTableName . '.related_item, ' . $fileTableName . '.item_id, ' . $fileTableName . '.privacy_status')
			->join($fileTableName, $fileTableName . '.file_id = ' . $fileMessageTableName . '.file_id', 'left')
			->where($fileMessageTableName . '.message_id', $messageId)
			->get();

		foreach ($query->getResultObject() as &$item) {
			if (empty($item->type)) {
				$item = 'deleted';
				$result[] = $item;
				continue;
			}

			if (!$fileModel->getFilePermission($item, $this->user_id)) {
				$item = 'notAllowed';
				$result[] = $item;
				continue;
			}

			$item->url = FileModel::preparePathToUserFile($item->url, $item->type);
			$item->name = FileModel::getFileNameWithoutExec($item->name);
			$result[] = $item;
		}

		return $result;
	}

	public function getResult()
	{
		$result = [];
		$result['messageResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['messageCount'] = $this->_messageCount;
		$result['timestamp'] = $this->timestamp;

		foreach ($this->_result->getResultObject() as &$item) {
			$item->important = MessageModel::MESSAGE_NOT_IMPORTANT;

			if ((int)$item->user_from === (int)$this->user_id && $item->status == 1) {
				$item->self_message_not_read = true;
			} else {
				$item->self_message_not_read = false;
			}

			if ((int)$item->user_from === (int)$this->user_id) {
				$item->selfMessage = true;
			} else {
				$item->selfMessage = false;
			}

			if ($this->user_id === $item->user_from) {
				$item->user_from_ava = $this->selfAvatar;
				$item->user_from_fullname = $this->firstName . ' ' . $this->lastName;
				$item->user_to_fullname = $item->user_with_fullname;

				if (is_null($item->userDeleted)) {
					$item->user_to_ava = !empty($item->url) ? FileModel::preparePathToAva($item->url) : FileModel::DEFAULT_AVA_URL;
				} else {
					$item->user_to_ava = FileModel::DEFAULT_AVA_URL;
				}
			} else {
				$item->user_to_ava = $this->selfAvatar;
				$item->user_to_fullname = $this->firstName . ' ' . $this->lastName;
				$item->user_from_fullname = $item->user_with_fullname;

				if (is_null($item->userDeleted)) {
					$item->user_from_ava = !empty($item->url) ? FileModel::preparePathToAva($item->url) : FileModel::DEFAULT_AVA_URL;
				} else {
					$item->user_from_ava = FileModel::DEFAULT_AVA_URL;
				}
			}

			if ($this->user_id == $item->user_from && $item->important_from == MessageModel::MESSAGE_IMPORTANT) {
				$item->important = MessageModel::MESSAGE_IMPORTANT;
			}

			if ($this->user_id == $item->user_to && $item->important_to == MessageModel::MESSAGE_IMPORTANT) {
				$item->important = MessageModel::MESSAGE_IMPORTANT;
			}

			$item->date = DateAndTime::getDateTime($item->date, $this->timeOffsetSeconds, 'genetive');
			$item->file_list = $this->_getFileListOfMessage($item->message_id);
			
			unset($item->user_with_fullname);
			unset($item->url);
			unset($item->important_from);
			unset($item->important_to);

			$result['messageResult'][] = $item;
		}

		return $result;
	}
}