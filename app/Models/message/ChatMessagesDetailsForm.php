<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;
use Common\Models\FileChatMessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;
use Common\Models\ChatMessageReactionModel;

class ChatMessagesDetailsForm extends BaseForm
{
	public $user_id;
	public $chat_id;
	public $timeOffsetSeconds;
	public $timestamp;
	public $rowNum; // last row in list

	protected $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'chat_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'timestamp' => 'required|integer',
		'rowNum' => 'permit_empty|integer',
	];

	protected $cleanValidationRules = false;

	private $_messageCount;
	private $_chat;
	private $_chatMember;
	private $_result;

	public function getDetails()
	{
		$chatModel = new ChatModel();
		$chat = $chatModel->find($this->chat_id);

		if (empty($chat)) {
			$this->setError('', lang('Chat.Chat not found'));
			return false;
		}

		$this->_chat = $chat;
		$chatMemberModel = new ChatMemberModel();
		$this->_chatMember = $chatMemberModel
			->where('chat_id', $this->chat_id)
			->where('user_id', $this->user_id)
			->first();

		if (empty($this->_chatMember)) {
			$this->setError('', lang('ChatMember.You are not a member of this chat'));
			return false;
		}

		$chatMessageModel = new ChatMessageModel();
		$userModel = new UserModel();
		$fileModel = new FileModel;
		$chatTableName = $chatModel->getTableName();
		$chatMessageTableName = $chatMessageModel->getTableName();
		$chatMemberTableName = $chatMemberModel->getTableName();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$messageQuery = $chatMessageModel
			->select($chatMessageTableName . '.*')
			->select($userTableName . '.user_id, concat(first_name, " ", last_name) as fullName, ' . $fileTableName . '.url as userAva, ' . $fileTableName . '.type as userAvaType, ' . $userTableName . '.deleted_at as userDeleted')
			->join($chatMemberTableName, $chatMemberTableName . '.chat_member_id = ' . $chatMessageTableName . '.chat_member_id')
			->join($userTableName, $userTableName . '.user_id = ' . $chatMemberTableName . '.user_id')
			->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id', 'left')
			->where($chatMessageTableName . '.chat_id', $this->chat_id)
			->where($chatMessageTableName . '.created_at <=', $this->timestamp)
			->where('not json_contains(deleted_chat_member_ids, \'' . $this->_chatMember->chat_member_id . '\', \'$\')')
			->orderBy($chatMessageTableName . '.created_at', 'DESC')
			->getCompiledSelect();

		$this->_messageCount = $chatMessageModel->db
			->query('select messageCount.* from (' . $messageQuery . ') as messageCount')
			->getNumRows();

		$messageQuery = 'select (@rowNum := @rowNum + 1) as rowNum, messageQuery.* from (' . $messageQuery . ') as messageQuery, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$messageQuery = 'select messageQuery.* from (' . $messageQuery . ') as messageQuery where rowNum > ' . $chatMessageModel->db->escape($this->rowNum);
		} else {
			$oldNewMessage = $chatMessageModel
				->where($chatMessageTableName . '.chat_id', $this->chat_id)
				->where('not json_contains(deleted_chat_member_ids, \'' . $this->_chatMember->chat_member_id . '\', \'$\')')
				->where('not json_contains(who_viewed_chat_member_ids, \'' . $this->_chatMember->chat_member_id . '\', \'$\')')
				->where($chatMessageTableName . '.chat_member_id !=', $this->_chatMember->chat_member_id)
				->where($chatMessageTableName . '.created_at >=', $this->_chatMember->created_at)
				->limit(1)
				->orderBy($chatMessageTableName . '.created_at', 'ASC')
				->first();

			if (!empty($oldNewMessage)) {
				$countWithNewMessages = $chatMessageModel
					->where($chatMessageTableName . '.chat_id', $this->chat_id)
					->where($chatMessageTableName . '.created_at >=', $oldNewMessage->created_at)
					->where($chatMessageTableName . '.created_at <=', $this->timestamp)
					->where('not json_contains(deleted_chat_member_ids, \'' . $this->_chatMember->chat_member_id . '\', \'$\')')
					->countAllResults();

				$this->limit += $countWithNewMessages;
			}
		}

		$this->_result = $chatMessageModel->db->query('select messageList.* from (select messageQuery.* from (' . $messageQuery . ') as messageQuery limit ' . $this->limit . ') as messageList order by messageList.created_at asc');

		return true;
	}

	private function _getFileListOfMessage($chatMessageId)
	{
		$fileChatMessageModel = new FileChatMessageModel();
		$fileModel = new FileModel();
		$fileTableName = $fileModel->getTableName();
		$fileChatMessageTableName = $fileChatMessageModel->getTableName();
		$result = [];

		$query = $fileChatMessageModel
			->select($fileTableName . '.file_id, ' . $fileTableName . '.type, ' . $fileTableName . '.url, ' . $fileTableName . '.original_name AS name, ' . $fileTableName . '.preview_height, ' . $fileTableName . '.preview_width, ' . $fileTableName . '.related_item, ' . $fileTableName . '.item_id, ' . $fileTableName . '.privacy_status')
			->join($fileTableName, $fileTableName . '.file_id = ' . $fileChatMessageTableName . '.file_id', 'left')
			->where($fileChatMessageTableName . '.chat_message_id', $chatMessageId)
			->get();

		foreach ($query->getResultObject() as &$item) {
			if (empty($item->type)) {
				$item = 'deleted';
				$result[] = $item;
				continue;
			}
			
			if (!$fileModel->getFilePermission($item, $this->user_id)) {
				$item = 'notAllowed';
				$result[] = $item;
				continue;
			}

			$item->url = FileModel::preparePathToUserFile($item->url, $item->type);
			$item->name = FileModel::getFileNameWithoutExec($item->name);
			$result[] = $item;
		}

		return $result;
	}

	public function getResult()
	{
		$result = [];
		$result['messageResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['messageCount'] = $this->_messageCount;
		$result['timestamp'] = $this->timestamp;
		$this->_chat->created_at = DateAndTime::getDateTime($this->_chat->created_at, $this->timeOffsetSeconds, 'genetive');
		$result['chat'] = $this->_chat;
		$chatMessageReactionModel = new ChatMessageReactionModel();
		$chatMemberModel = new ChatMemberModel();

		foreach ($this->_result->getResultObject() as &$item) {
			if (empty($item->userDeleted)) {
				$item->userAva = !empty($item->userAva) ? FileModel::preparePathToAva($item->userAva) : FileModel::DEFAULT_AVA_URL;
			} else {
				$item->userAva = FileModel::DEFAULT_AVA_URL;
				// $item->fullName = lang('Main.Profile deleted');
			}

			$item->date = DateAndTime::getDateTime($item->created_at, $this->timeOffsetSeconds, 'genetive');
			$item->file_list = $this->_getFileListOfMessage($item->chat_message_id);
			$importantMessageIdList = !empty($item->important_chat_member_ids) ? json_decode($item->important_chat_member_ids) : [];
			$item->importantMessage = in_array($this->_chatMember->chat_member_id, $importantMessageIdList) ? 1 : 0;
			$viewMessageIdList = !empty($item->who_viewed_chat_member_ids) ? json_decode($item->who_viewed_chat_member_ids) : [];
			$item->viewCount = !empty($viewMessageIdList)
				// ? count($viewMessageIdList)
				? $chatMemberModel->select(ChatmemberModel::table() . '.chat_member_id')
						->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . ChatMemberModel::table() . '.user_id')
						->whereIn(ChatmemberModel::table() . '.chat_member_id', $viewMessageIdList)
						->where('userTable.deleted_at', null)
						->where(ChatMemberModel::table() . '.deleted_at', null)
						->countAllResults()
				: 0;
			$item->reactionList = $chatMessageReactionModel->getMessageReactionList($item->chat_message_id);

			if ($item->user_id != $this->user_id && !in_array($this->_chatMember->chat_member_id, json_decode($item->who_viewed_chat_member_ids))) {
				$item->notReadMessage = true;
			} else {
				$item->notReadMessage = false;
			}

			unset($item->created_at);
			$result['messageResult'][] = $item;
		}

		if (empty($result['messageResult'])) {
			$chatMemberModel = new ChatMemberModel();
			$userModel = new UserModel();
			$chatMemberTableName = $chatMemberModel->getTableName();
			$userTableName = $userModel->getTableName();
			$chatCreater = $chatMemberModel
				->select('concat(first_name, " ", last_name) as fullName, ' . $userTableName . '.deleted_at as userDeleted')
				->join($userTableName, $userTableName . '.user_id = ' . $chatMemberTableName . '.user_id')
				->where('chat_id', $result['chat']->chat_id)
				->where($chatMemberTableName . '.role', ChatMemberModel::ROLE_CREATER)
				->withDeleted()
				->first();

			// if (empty($chatCreater->userDeleted)) {
				$chatCreater->fullName = $chatCreater->fullName;
			// } else {
			// 	$chatCreater->fullName = lang('Main.Profile deleted');
			// }

			$result['chatCreater'] = $chatCreater;
		}

		return $result;
	}
}