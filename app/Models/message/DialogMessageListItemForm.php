<?php

namespace App\Models\message;

use Common\Forms\BaseForm;
use Common\Models\MessageModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FileMessageModel;
use App\Libraries\DateAndTime\DateAndTime;

class DialogMessageListItemForm extends BaseForm
{
	public $user_id;
	public $user_to;
	public $user_from;
	public $timeOffsetSeconds;
	public $selfAvatar;
	public $firstName;
	public $lastName;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'user_to' => 'required|integer',
		'user_from' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'selfAvatar' => 'required|string',
		'firstName' => 'required|string',
		'lastName' => 'required|string',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getItem()
	{
		$messageModel = new MessageModel();
		$withuserId = ((int)$this->user_from === (int)$this->user_id)
			? $this->user_to
			: $this->user_from;

		$countNewMessageSubquery = $messageModel
			->select('count(message_id)');

		if ((int)$this->user_from === (int)$this->user_id) {
			$countNewMessageSubquery->where('user_to', $this->user_id)
				->where('user_from', $this->user_to)
				->where('delete_from', MessageModel::MESSAGE_NOT_DELETED);
		}

		if ((int)$this->user_to === (int)$this->user_id) {
			$countNewMessageSubquery->where('user_to', $this->user_id)
				->where('user_from', $this->user_from)
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED);
		}

		$countNewMessageSubquery = $countNewMessageSubquery->where('status', 1)
			->getCompiledSelect();

		$messageQuery = $messageModel
			->select(MessageModel::table() . '.*')
			->select('(' . $countNewMessageSubquery . ') as new_messages_from')
			->select('first_name, last_name')
			->select('fileTable.url as fromAva')
			->select('count(fileMessageTable.file_message_id) as newFileCount');

		if ((int)$this->user_id === (int)$this->user_from) {
			$messageQuery->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . MessageModel::table() . '.user_to')
				->where('delete_from', MessageModel::MESSAGE_NOT_DELETED);
		}

		if ((int)$this->user_id === (int)$this->user_to) {
			$messageQuery->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . MessageModel::table() . '.user_from')
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED);
		}

		$message = $messageQuery->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->join(FileMessageModel::table() . ' as fileMessageTable', 'fileMessageTable.message_id = ' . MessageModel::table() . '.message_id', 'left')
			->where('user_to', $this->user_to)
			->where('user_from', $this->user_from)
			->orderBy(MessageModel::table() . '.created_at', 'DESC')
			->groupBy(MessageModel::table() . '.message_id')
			->limit(1)
			->first();

		if (empty($message)) {
			$this->setError('', lang('ChatMessage.Message not found'));
			return false;
		}

		$message->selfMessage = ((int)$this->user_id === (int)$message->user_from) ? true : false;

		if ($message->user_from === $this->user_id) {
			$message->user_from = $message->user_to;
		}
		
		$message->selfAva = $this->selfAvatar;
		$message->fromAva = !empty($message->fromAva) ? FileModel::preparePathToAva($message->fromAva) : FileModel::DEFAULT_AVA_URL;
		$message->date = DateAndTime::getDateTime($message->created_at, $this->timeOffsetSeconds, 'genetive');
		$message->notReadMessage = ((int)$message->status === 1) ? true : false;
		$this->_result = $message;

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}