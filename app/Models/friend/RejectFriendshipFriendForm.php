<?php

namespace App\Models\friend;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Components\friendRequest\FriendRequestComponent;
use Common\Components\friendRequestNotice\FriendRequestNoticeComponent;

class RejectFriendshipFriendForm extends BaseForm
{
	public $user_id;
	public $profile_id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;
	private $_relationNoticeData;
	private $_userProfile;

	public function __construct()
	{
		parent::__construct();
		$this->_friendModel = new FriendModel();
		$this->_userModel = new UserModel();
		$this->_newSubscriberAction = false;
	}

	public function reject()
	{
		$this->_userProfile = $this->_userModel->getUserData($this->profile_id);

		if (empty($this->_userProfile)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->profile_id]));
			return false;
		}

		$existRelation = $this->_friendModel->getUserRelationships([$this->profile_id], $this->user_id);

		if (!empty($existRelation)) {
			 if ($existRelation[$this->profile_id]['user_relations'] === FriendModel::RELATION_NEW_SUBSCRIBER) {
			 	$this->_newSubscriberAction = 'remove';
			 }		
		}

		if (empty($existRelation)
			|| !in_array($existRelation[$this->profile_id]['user_relations'], [FriendModel::RELATION_SUBSCRIBER, FriendModel::RELATION_NEW_SUBSCRIBER])) {

			$existRelation = (empty($existRelation))
				? ["$this->profile_id" => FriendModel::getNoLinksDataLabel()]
				: $existRelation;
			$this->_relationNoticeData = $existRelation;

			return true;
		}

		$friendData = [
			'friend_id' => $existRelation[$this->profile_id]['friend_id'],
			'status' => FriendModel::SUBSCRIBER_STATUS,
		];

		if (!$this->_friendModel->save($friendData)) {
			$this->setError('', lang('Friend.Failed to change friend request status'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$this->_result['relationNoticeData'] = $this->_relationNoticeData;
		$this->_userProfile->userAva = $this->_userProfile->path_to_ava;
		$this->_userProfile->userFullname = $this->_userProfile->first_name . ' ' . $this->_userProfile->last_name;
		$this->_userProfile->selfItem = false;

		if (!empty($this->_result['relationNoticeData'])) {
			$this->_result['user'] = $this->_userProfile;
			$this->_result['action'] = FriendRequestNoticeComponent::ACTION_REJECT_FRIENDSHIP;
			$this->_result['noticeData'] = FriendRequestNoticeComponent::getNotice($this->_result);

			return $this->_result;
		}

		$relation = $this->_friendModel->getUserRelationships([$this->profile_id], $this->user_id);
		$this->_result['htmlData']['friendUserItem'] = FriendRequestComponent::getFriendUserItem([
			'selfProfile' => true,
			'friendLinks' => $relation,
			'item' => $this->_userProfile,
		]);
		$this->_result['userFromId'] = $this->user_id;
		$this->_result['userToId'] = $this->profile_id;
		$this->_result['newSubscriberAction'] = $this->_newSubscriberAction;
		$this->_result['action'] = FriendModel::WS_ACTION_REJECT_FRIENDSHIP;

		return $this->_result;
	}
}