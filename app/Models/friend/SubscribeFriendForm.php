<?php

namespace App\Models\friend;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Components\WsClientComponent;
use Common\Components\friendRequest\FriendRequestComponent;
use Common\Components\friendRequestNotice\FriendRequestNoticeComponent;

class SubscribeFriendForm extends BaseForm
{
	public $user_id;
	public $profile_id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;
	private $_relationNoticeData;
	private $_userProfile;

	public function __construct()
	{
		parent::__construct();
		$this->_friendModel = new FriendModel();
		$this->_userModel = new UserModel();
	}

	public function subscribe()
	{
		$this->_userProfile = $this->_userModel->getUserData($this->profile_id);

		if (empty($this->_userProfile)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->profile_id]));
			return false;
		}

		$existRelation = $this->_friendModel->getUserRelationships([$this->profile_id], $this->user_id);

		if (!empty($existRelation)) {
			$this->_relationNoticeData = $existRelation;
			return true;
		}

		$friendData = [
			'id_request_sender' => $this->user_id,
			'id_request_recipient' => $this->profile_id,
			'status' => FriendModel::NEW_SUBSCRIBER_STATUS,
		];

		if (!$this->_friendModel->save($friendData)) {
			$this->setError('', lang('Friend.Failed to send friend request'));
			return false;
		}

		$this->sendWsAddNewSubscriber();

		return true;
	}

	public function getResult()
	{
		$this->_result['relationNoticeData'] = $this->_relationNoticeData;
		$this->_userProfile->userAva = $this->_userProfile->path_to_ava;
		$this->_userProfile->userFullname = $this->_userProfile->first_name . ' ' . $this->_userProfile->last_name;
		$this->_userProfile->selfItem = false;

		if (!empty($this->_result['relationNoticeData'])) {
			$this->_result['user'] = $this->_userProfile;
			$this->_result['action'] = FriendRequestNoticeComponent::ACTION_SUBSCRIBE;
			$this->_result['noticeData'] = FriendRequestNoticeComponent::getNotice($this->_result);

			return $this->_result;
		}

		$relation = $this->_friendModel->getUserRelationships([$this->profile_id], $this->user_id);
		$this->_result['htmlData']['friendButtonUserProfile'] = FriendRequestComponent::getFriendButtonUserProfile([
			'user_id' => $this->profile_id,
			'relation' => $relation,
		]);
		$this->_result['htmlData']['friendUserItem'] = FriendRequestComponent::getFriendUserItem([
			'selfProfile' => true,
			'friendLinks' => $relation,
			'item' => $this->_userProfile,
		]);
		$this->_result['userFromId'] = $this->user_id;
		$this->_result['userToId'] = $this->profile_id;
		$this->_result['newSubscriberAction'] = false;
		$this->_result['action'] = FriendModel::WS_ACTION_SUBSCRIBE;

		return $this->_result;
	}

	protected function sendWsAddNewSubscriber()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();
		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getLocale',
				'userId' => $this->profile_id,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->locale)) {
			$relation = $this->_friendModel->getUserRelationships([$this->user_id], $this->profile_id, $response->locale);
			$user = $this->_userModel->getUserData($this->user_id);
			$user->userAva = $user->path_to_ava;
			$user->userFullname = $user->first_name . ' ' . $user->last_name;
			$user->selfItem = false;
			$user->newSubscriber = true;
			$user->locale = $response->locale;
			$htmlData = [];
			$htmlData['friendButtonUserProfile'] = FriendRequestComponent::getFriendButtonUserProfile([
				'user_id' => $this->user_id,
				'relation' => $relation,
			]);
			$htmlData['friendUserItem'] = FriendRequestComponent::getFriendUserItem([
				'selfProfile' => true,
				'friendLinks' => $relation,
				'item' => $user,
			]);
			$wsData = [
				'server' => [
					'controller' => 'Friend',
					'action' => 'addNewSubscriber',
					'userId' => $this->profile_id,
					'data' => [
						'userFromId' => $this->user_id,
						'userToId' => $this->profile_id,
						'htmlData' => $htmlData,
						'newSubscriberAction' => 'add',
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
			}
		}

		$wsClientComponent->closeSocket();
	}
}