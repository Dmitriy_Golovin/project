<?php

namespace App\Models\friend;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Components\WsClientComponent;
use Common\Components\friendRequest\FriendRequestComponent;
use Common\Components\friendRequestNotice\FriendRequestNoticeComponent;
use Common\Components\nothingFoundList\NothingFoundListComponent;

class UnubscribeFriendForm extends BaseForm
{
	public $user_id;
	public $profile_id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;
	private $_relationNoticeData;
	private $_userProfile;

	public function __construct()
	{
		parent::__construct();
		$this->_friendModel = new FriendModel();
		$this->_userModel = new UserModel();
		$this->_newSubscriberAction = false;
	}

	public function unsubscribe()
	{
		$this->_userProfile = $this->_userModel->getUserData($this->profile_id);

		if (empty($this->_userProfile)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->profile_id]));
			return false;
		}

		$existRelation = $this->_friendModel->getUserRelationships([$this->profile_id], $this->user_id);

		if (!empty($existRelation)) {
			$relationForProfileId = $this->_friendModel->getUserRelationships([$this->user_id], $this->profile_id);
			 if ($relationForProfileId[$this->user_id]['user_relations'] === FriendModel::RELATION_NEW_SUBSCRIBER) {
			 	$this->_newSubscriberAction = 'remove';
			 }		
		}

		if (empty($existRelation)
			|| !in_array($existRelation[$this->profile_id]['user_relations'], [FriendModel::RELATION_YOU_ARE_SUBSCRIBED])) {

			$existRelation = (empty($existRelation))
				? ["$this->profile_id" => FriendModel::getNoLinksDataLabel()]
				: $existRelation;
			$this->_relationNoticeData = $existRelation;

			return true;
		}

		$deleteRequest = $this->_friendModel
			->where('friend_id', $existRelation[$this->profile_id]['friend_id'])
			->delete();

		if (!$deleteRequest) {
			$this->setError('', lang('Friend.Failed to unsubscribe from user'));
			return false;
		}

		$this->sendWsDeleteUserFromSubscribers();

		return true;
	}

	public function getResult()
	{
		$this->_result['relationNoticeData'] = $this->_relationNoticeData;
		$this->_userProfile->userAva = $this->_userProfile->path_to_ava;
		$this->_userProfile->userFullname = $this->_userProfile->first_name . ' ' . $this->_userProfile->last_name;
		$this->_userProfile->selfItem = false;

		if (!empty($this->_result['relationNoticeData'])) {
			$this->_result['user'] = $this->_userProfile;
			$this->_result['action'] = FriendRequestNoticeComponent::ACTION_UNSUBSCRIBE;
			$this->_result['noticeData'] = FriendRequestNoticeComponent::getNotice($this->_result);

			return $this->_result;
		}

		$relation = ["$this->profile_id" => FriendModel::getNoLinksDataLabel()];
		$this->_result['htmlData']['friendButtonUserProfile'] = FriendRequestComponent::getFriendButtonUserProfile([
			'user_id' => $this->profile_id,
			'relation' => $relation,
		]);
		$this->_result['htmlData']['nothingFound'] = NothingFoundListComponent::render();

		$this->_result['userFromId'] = $this->user_id;
		$this->_result['userToId'] = $this->profile_id;
		$this->_result['newSubscriberAction'] = false;
		$this->_result['action'] = FriendModel::WS_ACTION_UNSUBSCRIBE;

		return $this->_result;
	}

	protected function sendWsDeleteUserFromSubscribers()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();
		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getLocale',
				'userId' => $this->profile_id,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->locale)) {
			$relation = ["$this->user_id" => FriendModel::getNoLinksDataLabel($response->locale)];
			$htmlData = [];
			$htmlData['friendButtonUserProfile'] = FriendRequestComponent::getFriendButtonUserProfile([
				'user_id' => $this->user_id,
				'relation' => $relation,
			]);
			$htmlData['nothingFound'] = NothingFoundListComponent::render($response->locale);
			$wsData = [
				'server' => [
					'controller' => 'Friend',
					'action' => 'deleteSubscriber',
					'userId' => $this->profile_id,
					'data' => [
						'userFromId' => $this->user_id,
						'userToId' => $this->profile_id,
						'htmlData' => $htmlData,
						'newSubscriberAction' => $this->_newSubscriberAction,
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
			}
		}

		$wsClientComponent->closeSocket();
	}
}