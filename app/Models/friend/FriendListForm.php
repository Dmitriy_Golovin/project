<?php

namespace App\Models\friend;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FriendModel;

class FriendListForm extends BaseForm
{
	public $user_id;
	public $page_id;
	public $type;
	public $search_string;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'page_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2,3]',
		'search_string' => 'permit_empty|string',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_itemCount;
	private $_result;

	public function getList()
	{
		$userModel = new UserModel();

		$this->_result = $userModel
			->select(UserModel::table() . '.user_id, ' . UserModel::table() . '.first_name, ' . UserModel::table() . '.last_name, concat(' . UserModel::table() . '.first_name, " ", ' . UserModel::table() . '.last_name) as userFullname, fileTable.url as userAva')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . UserModel::table() . '.file_id' , 'left')
			->where(UserModel::table() . '.deleted_at', null)
			->where(UserModel::table() . '.is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->where(UserModel::table() . '.veryfication_token', null);

		if ((int)$this->type === FriendModel::TYPE_FRIENDS) {
			$this->_result = $this->_result
				->join(FriendModel::table() . ' as friendReciept', 'friendReciept.id_request_recipient = ' . UserModel::table() . '.user_id', 'left')
				->join(FriendModel::table() . ' as friendSender', 'friendSender.id_request_sender = ' . UserModel::table() . '.user_id', 'left')
				->groupStart()
					->groupStart()
						->where('friendReciept.id_request_sender', $this->page_id)
						->where('friendReciept.status', FriendModel::FRIEND_STATUS)
						->where('friendReciept.updated_at < ', $this->timestamp)
					->groupEnd()
					->orGroupStart()
						->where('friendSender.id_request_recipient', $this->page_id)
						->where('friendSender.status', FriendModel::FRIEND_STATUS)
						->where('friendSender.updated_at < ', $this->timestamp)
					->groupEnd()
				->groupEnd();
		}

		if ((int)$this->type === FriendModel::TYPE_SUBSCRIBERS) {
			$this->_result = $this->_result
				->join(FriendModel::table() . ' as friendTable', 'friendTable.id_request_sender = ' . UserModel::table() . '.user_id')
				->where('friendTable.id_request_recipient', $this->page_id)
				->where('friendTable.updated_at < ', $this->timestamp)
				->whereIn('friendTable.status', [FriendModel::NEW_SUBSCRIBER_STATUS, FriendModel::SUBSCRIBER_STATUS]);
		}

		if (!empty($this->search_string)) {
			$this->_result->like('(concat(' . UserModel::table() . '.first_name, " ", ' . UserModel::table() . '.last_name))', $this->search_string);
		}

		$this->_result->groupBy('user_id')
			->orderBy('userFullname', 'ASC');

		$this->_itemCount = $this->_result->countAllResults(false);

		$this->_result =$this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $userModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $userModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$friendModel = new FriendModel();
		$result = [];
		$result['friendResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['itemCount'] = $this->_itemCount;
		$result['selfProfile'] = false;
		$userIdList = [];

		foreach ($this->_result->getResultObject() as $item) {
			$item->selfItem = ((int)$item->user_id === (int)$this->user_id)
				? true
				: false;
			$item->userAva = (!empty($item->userAva))
				? FileModel::preparePathToAva($item->userAva)
				: FileModel::DEFAULT_AVA_URL;

			if ((int)$item->user_id !== $this->user_id) {
				array_push($userIdList, $item->user_id);
			}

			$result['friendResult'][] = $item;
		}

		$result['friendLinks'] = $friendModel->getUserRelationships($userIdList, $this->user_id);

		return $result;
	}
}