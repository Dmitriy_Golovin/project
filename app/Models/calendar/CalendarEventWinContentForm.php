<?php

namespace App\Models\calendar;

use Common\Forms\BaseForm;
use CodeIgniter\Database\BaseBuilder;
use Common\Models\CountryModel;
use Common\Models\UserSettingModel;
use Common\Models\HolidayDateModel;
use Common\Models\HolidayModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;
use App\Libraries\DateAndTime\DateAndTime;

class CalendarEventWinContentForm extends BaseForm
{
	public $user_id;
	public $user_country_id;
	public $country_abbr;
	public $date;
	public $eventIdList;
	public $meetingIdList;
	public $taskIdList;
	public $year;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'permit_empty|integer',
		'user_country_id' => 'permit_empty|integer',
		'country_abbr' => 'permit_empty|string',
		'date' => 'required|string',
		'eventIdList' => 'permit_empty|string',
		'meetingIdList' => 'permit_empty|string',
		'taskIdList' => 'permit_empty|string',
		'year' => 'required|string',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function get()
	{
		$holidayList = [];
		$eventList = [];
		$meetingList = [];
		$taskList = [];

		if (empty($this->user_id)) {
			$countryModel = new CountryModel();
			$country = $countryModel
				->where('code', $this->country_abbr)
				->first();
			$this->user_country_id = !empty($country) ? $country->country_id : '';
			$holidayList = $this->_getHolidayList();
		} else {
			$userSettingModel = new UserSettingModel();
			$userSettings = $userSettingModel->getUserSettings($this->user_id);

			if ((int)$userSettings->holidays === UserSettingModel::HOLIDAYS_ACTIVE) {
				$holidayList = $this->_getHolidayList();
			}

			if ((int)$userSettings->my_events === UserSettingModel::MY_EVENTS_ACTIVE && !empty($this->eventIdList)) {
				$eventList = $this->_getEventList();
			}

			if ((int)$userSettings->my_meetings === UserSettingModel::MY_MEETINGS_ACTIVE && !empty($this->meetingIdList)) {
				$meetingList = $this->_getMeetingList();
			}

			if ((int)$userSettings->my_tasks === UserSettingModel::MY_TASKS_ACTIVE && !empty($this->taskIdList)) {
				$taskList = $this->_getTaskList();
			}
		}

		$this->_result['holidayList'] = $holidayList;
		$this->_result['eventList'] = $eventList;
		$this->_result['meetingList'] = $meetingList;
		$this->_result['taskList'] = $taskList;
		return true;
	}

	private function _getHolidayList()
	{
		$result = [];
		$holidayModel = new HolidayModel();
		$holidayDateModel = new HolidayDateModel();
		$countryModel = new CountryModel();
		$holidayTableName = $holidayModel->getTableName();
		$holidayDateTableName = $holidayDateModel->getTableName();
		$countryTableName = $countryModel->getTableName();

		$query = $holidayModel
			->join($holidayDateTableName, $holidayTableName . '.holiday_id = ' . $holidayDateTableName . '.holiday_id')
			->join($countryTableName, $holidayTableName . '.country_id = ' . $countryTableName . '.country_id')
			->select($holidayTableName . '.holiday_id, ' . $holidayTableName . '.holiday_day, ' . $countryTableName . '.code')
			->where($holidayTableName . '.country_id', $this->user_country_id)
			->whereIn($holidayDateTableName . '.holiday_date', json_decode($this->date))
			->get();

		foreach ($query->getResultObject() as $item) {
			$item->link = '/holiday/' . $item->code . '/' . $item->holiday_id;
			$result[] = $item;
		}

		return $result;
	}

	private function _getEventList()
	{
		$result = [];
		$eventModel = new EventModel();
		$date = json_decode($this->date)[0];

		$query = $eventModel
			->select('event_id, name, date, time, duration')
			->whereIn('event_id', explode(',', $this->eventIdList))
			->where('user_id', $this->user_id)
			->where('deleted_at', null)
			->get();

		foreach ($query->getResultObject() as $item) {
			$timestamp = $item->date + $item->time + $this->timeOffsetSeconds;
			$item->time = DateAndTime::getHours($timestamp) . ':' . DateAndTime::getMinutes($timestamp);
			$item->link = '/event/details/' . $item->event_id;
			$result[] = $item;
		}

		return $result;
	}

	private function _getMeetingList()
	{
		$result = [];
		$meetingModel = new MeetingModel();
		$date = json_decode($this->date)[0];

		$query = $meetingModel
		->select('meeting_id, name, date, time, duration')
		->whereIn('meeting_id', explode(',', $this->meetingIdList))
			->where('user_id', $this->user_id)
			->where('deleted_at', null)
			->get();

		foreach ($query->getResultObject() as $item) {
			$timestamp = $item->date + $item->time + $this->timeOffsetSeconds;
			$item->time = DateAndTime::getHours($timestamp) . ':' . DateAndTime::getMinutes($timestamp);
			$item->link = '/meeting/details/' . $item->meeting_id;
			$result[] = $item;
		}

		return $result;
	}

	private function _getTaskList()
	{
		$result = [];
		$taskModel = new TaskModel();
		$date = json_decode($this->date)[0];

		$query = $taskModel
			->select('task_id, name, date, time, duration')
			->whereIn('task_id', explode(',', $this->taskIdList))
			->where('assigned_user_id', $this->user_id)
			->where('deleted_at', null)
			->get();

		foreach ($query->getResultObject() as $item) {
			$timestamp = $item->date + $item->time + $this->timeOffsetSeconds;
			$item->time = DateAndTime::getHours($timestamp) . ':' . DateAndTime::getMinutes($timestamp);
			$item->link = '/task/assigned-details/' . $item->task_id;
			$result[] = $item;
		}

		return $result;
	}

	public function getResult()
	{
		return $this->_result;
	}
}