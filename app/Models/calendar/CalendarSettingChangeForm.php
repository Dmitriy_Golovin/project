<?php

namespace App\Models\calendar;

use Common\Forms\BaseForm;
use Common\Models\UserSettingModel;

class CalendarSettingChangeForm extends BaseForm
{
	public $user_id;
	public $holidays;
	public $my_events;
	public $my_meetings;
	public $my_tasks;
	public $friends_events;
	public $change_field;

	protected $validationRules =[
		'user_id' => 'required|integer',
		'holidays' => 'required|integer|in_list[0,1]',
		'my_events' => 'required|integer|in_list[0,1]',
		'my_meetings' => 'required|integer|in_list[0,1]',
		'my_tasks' => 'required|integer|in_list[0,1]',
		'friends_events' => 'required|integer|in_list[0,1]',
		'change_field' => 'required|string',
	];

	private $_result;

	public function change()
	{
		$userSettingModel = new UserSettingModel();
		$dataUpdate = [
			'holidays' => $this->holidays,
			'my_events' => $this->my_events,
			'my_meetings' => $this->my_meetings,
			'my_tasks' => $this->my_tasks,
			'friends_events' => $this->friends_events,
		];
		$queryUserSetting = $userSettingModel
			->where('user_id', $this->user_id)
			->set($dataUpdate);

		if (!$queryUserSetting->update()) {
			$this->setError('', $queryUserSetting->errors());
			return false;
		}

		$this->_result['applyFor'] = $this->change_field;
		$this->_result['action'] = ($dataUpdate[$this->change_field]) ? 'add' : 'remove';
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}