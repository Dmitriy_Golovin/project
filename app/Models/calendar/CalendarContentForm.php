<?php

namespace App\Models\calendar;

use Common\Forms\BaseForm;
use CodeIgniter\Database\BaseBuilder;
use App\Libraries\DateAndTime\DateAndTime;
use Common\Models\CountryModel;
use Common\Models\UserSettingModel;
use Common\Models\HolidayDateModel;
use Common\Models\HolidayModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;

class CalendarContentForm extends BaseForm
{
	public $user_id;
	public $user_country_id;
	public $country_abbr;
	public $dateList;
	public $dateOfFirstEl;
	public $dateOfLastEl;
	public $yearList;
	public $monthList;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'integer',
		'user_country_id' => 'permit_empty|integer',
		'country_abbr' => 'string',
		'dateList' => 'required|string',
		'dateOfFirstEl' => 'required|string',
		'dateOfLastEl' => 'required|string',
		'yearList' => 'required|string',
		'monthList' => 'required|string',
		'timeOffsetSeconds' => 'integer',
	];

	private $_result;
	private $_userSetting;
	private $_selfEventList = [];
	private $_meetingList = [];
	private $_taskList = [];

	public function get()
	{
		if (empty($this->user_id)) {
			$countryModel = new CountryModel();
			$country = $countryModel
				->where('code', $this->country_abbr)
				->first();
			$this->user_country_id = !empty($country) ? $country->country_id : '';
			$holidayDateModel = new HolidayDateModel();
			$holidayModel = new HolidayModel();
			$holidayTableName = $holidayModel->getTableName();
			$holidayList = [];

			$holidayQuery = $holidayDateModel
				->select('holiday_date')
				->whereIn('holiday_date', explode(',', $this->dateList))
				->whereIn('holiday_id', function (BaseBuilder $builder) use ($holidayTableName) {
					return $builder->select('holiday_id')
						->from($holidayTableName)
						->where('country_id', $this->user_country_id);
				})
				->get();

			foreach ($holidayQuery->getResultObject() as $item) {
				$holidayList[] = $item->holiday_date;
			}

			$this->_result['holidayList'] = array_count_values($holidayList);
		} else {
			$userSettingModel = new UserSettingModel();
			$this->_userSetting = $userSettingModel->getUserSettings($this->user_id);
			$holidayList = [];

			if ((int)$this->_userSetting->holidays === UserSettingModel::HOLIDAYS_ACTIVE) {
				$holidayDateModel = new HolidayDateModel();
				$holidayModel = new HolidayModel();
				$countryModel = new CountryModel();
				$holidayTableName = $holidayModel->getTableName();

				if (empty($this->user_country_id)) {
					$this->user_country_id = $countryModel->getDefaultCountryId();
				}

				$holidayQuery = $holidayDateModel
					->select('holiday_date')
					->whereIn('holiday_date', explode(',', $this->dateList))
					->whereIn('holiday_id', function (BaseBuilder $builder) use ($holidayTableName) {
						return $builder->select('holiday_id')
							->from($holidayTableName)
							->where('country_id', $this->user_country_id);
					})
					->get();

				foreach ($holidayQuery->getResultObject() as $item) {
					$holidayList[] = $item->holiday_date;
				}
			}

			if ((int)$this->_userSetting->my_events === UserSettingModel::MY_EVENTS_ACTIVE) {
				$eventModel = new EventModel();
				$eventQuery = $eventModel
					->select('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . ')) as date, periodicity, event_id')
					->groupStart()
						->whereIn('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . '))', explode(',', $this->dateList))
						->where('user_id', $this->user_id)
						->where('periodicity', EventModel::PERIODICITY_NONE)
						->where('is_completed', EventModel::STATUS_NOT_COMPLETED)
						->where('deleted_at', null)
					->groupEnd()
					->orGroupStart()
						->where('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . ')) <=', strtotime($this->dateOfLastEl) + DateAndTime::SECONDS_IN_DAY - 1)
						->where('user_id', $this->user_id)
						->where('periodicity !=', EventModel::PERIODICITY_NONE)
						->where('is_completed', EventModel::STATUS_NOT_COMPLETED)
						->where('deleted_at', null)
					->groupEnd()
					->get();

				foreach ($eventQuery->getResultObject() as $item) {
					$this->_selfEventList = array_merge($this->_selfEventList, $this->_prepareSelfEventList($item, $item->event_id, $this->_selfEventList));
				}
			}

			if ((int)$this->_userSetting->my_meetings === UserSettingModel::MY_MEETINGS_ACTIVE) {
				$meetingModel = new MeetingModel();
				$meetingQuery = $meetingModel
					->select('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . ')) as date, periodicity, meeting_id')
					->groupStart()
						->whereIn('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . '))', explode(',', $this->dateList))
						->where('user_id', $this->user_id)
						->where('periodicity', MeetingModel::PERIODICITY_NONE)
						->where('is_completed', MeetingModel::STATUS_NOT_COMPLETED)
						->where('deleted_at', null)
					->groupEnd()
					->orGroupStart()
						->where('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . ')) <=', strtotime($this->dateOfLastEl) + DateAndTime::SECONDS_IN_DAY - 1)
						->where('user_id', $this->user_id)
						->where('periodicity !=', MeetingModel::PERIODICITY_NONE)
						->where('is_completed', MeetingModel::STATUS_NOT_COMPLETED)
						->where('deleted_at', null)
					->groupEnd()
					->get();

				foreach ($meetingQuery->getResultObject() as $item) {
					$this->_meetingList = array_merge($this->_meetingList, $this->_prepareSelfEventList($item, $item->meeting_id, $this->_meetingList));
				}
			}

			if ((int)$this->_userSetting->my_tasks === UserSettingModel::MY_TASKS_ACTIVE) {
				$taskModel = new TaskModel();
				$taskQuery = $taskModel
					->select('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . ')) as date, periodicity, task_id')
					->groupStart()
						->whereIn('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . '))', explode(',', $this->dateList))
						->where('assigned_user_id', $this->user_id)
						->where('periodicity', TaskModel::PERIODICITY_NONE)
						->where('status', TaskModel::STATUS_NOT_COMPLETED)
						->where('deleted_at', null)
					->groupEnd()
					->orGroupStart()
						->where('date(from_unixtime(date + time + ' . $this->timeOffsetSeconds . ')) <=', strtotime($this->dateOfLastEl) + DateAndTime::SECONDS_IN_DAY - 1)
						->where('assigned_user_id', $this->user_id)
						->where('periodicity !=', TaskModel::PERIODICITY_NONE)
						->where('status', TaskModel::STATUS_NOT_COMPLETED)
						->where('deleted_at', null)
					->groupEnd()
					->get();

				foreach ($taskQuery->getResultObject() as $item) {
					$this->_taskList = array_merge($this->_taskList, $this->_prepareSelfEventList($item, $item->task_id, $this->_taskList));
				}
			}

			$this->_result['holidayList'] = array_count_values($holidayList);
		}

		return true;
	}

	public function getResult()
	{
		$this->_result['selfEventList'] = $this->_selfEventList;
		$this->_result['meetingList'] = $this->_meetingList;
		$this->_result['taskList'] = $this->_taskList;
		return $this->_result;
	}

	private function _prepareSelfEventList($item, $id, $resultList)
	{
		$result = [];
		$yearList = explode(',', $this->yearList);
		$monthList = explode(',', $this->monthList);

		if ($item->periodicity == EventModel::PERIODICITY_NONE) {
			if (isset($resultList[$item->date])) {
				array_push($resultList[$item->date], $id);
			} else {
				$resultList[$item->date] = [];
				array_push($resultList[$item->date], $id);
			}
		}

		if ($item->periodicity == EventModel::PERIODICITY_WORKING_DAY) {
			$resultList = $this->_getSelfEventPeriodicityWorkingDay($item->date, $yearList, $monthList, $this->dateOfFirstEl, $this->dateOfLastEl, $id, $resultList);
		}

		if ($item->periodicity == EventModel::PERIODICITY_YEARLY) {
			$resultList = $this->_getSelfEventPeriodicityYear($item->date, $yearList, $monthList, $this->dateOfFirstEl, $this->dateOfLastEl, $id, $resultList);
		}

		if ($item->periodicity == EventModel::PERIODICITY_MONTHLY) {
			$resultList = $this->_getSelfEventPeriodicityMonth($item->date, $yearList, $monthList, $this->dateOfFirstEl, $this->dateOfLastEl, $id, $resultList);
		}

		if ($item->periodicity == EventModel::PERIODICITY_WEEKLY) {
			$resultList = $this->_getSelfEventPeriodicityWeek($item->date, $yearList, $monthList, $this->dateOfFirstEl, $this->dateOfLastEl, $id, $resultList);
		}

		if ($item->periodicity == EventModel::PERIODICITY_DAILY) {
			$resultList = $this->_getSelfEventPeriodicityDay($item->date, $yearList, $monthList, $this->dateOfFirstEl, $this->dateOfLastEl, $id, $resultList);
		}
		
		return $resultList;
	}

	private function _getSelfEventPeriodicityWorkingDay($val, $yearList, $monthList, $dateOfFirstEl, $dateOfLastEl, $id, $result)
	{
		$dateStart = DateAndTime::createFromFormat('Y-m-j H:i', $val . '00:00');
		$dateFinish = DateAndTime::createFromFormat('Y-m-j H:i', $dateOfLastEl . '00:00');
		
		while ($dateStart->getTimestamp() < $dateFinish->getTimestamp()) {
			$year = $dateStart->getYear();
			$month = $dateStart->getMonth();
			$day = $dateStart->getDay();

			if (strlen($month . '') < 2) {
				$month = '0' . $month;
			}

			if (strlen($day . '') < 2) {
				$day = '0' . $day;
			}

			$value = $year . '-' . $month . '-' . $day;
			$dayOfWeek = DateAndTime::createFromFormat('Y-m-j H:i', $value . '00:00')->getDayOfWeek();

			if (($dayOfWeek >= $this->_userSetting->work_week_start
				&& $dayOfWeek < (int)$this->_userSetting->work_week_start + (int)$this->_userSetting->work_days_amount)
				|| $value === $val) {
				if (isset($result[$value])) {
					array_push($result[$value], $id);
				} else {
					$result[$value] = [];
					array_push($result[$value], $id);
				}
			}

			$dateStart = $dateStart->addDays(1);
		}

		return $result;
	}
	
	private function _getSelfEventPeriodicityYear($val, $yearList, $monthList, $dateOfFirstEl, $dateOfLastEl, $id, $result)
	{
		$dateYear = mb_substr($val, 0, mb_strpos($val, '-'));
		$dateday = mb_substr($val, mb_strrpos($val, '-') + 1);
		$dateMonth = mb_substr($val, mb_strpos($val, '-') + 1, 2);
		
		for ($i = 0; $i < count($yearList); $i++) {
			if (($yearList[$i] >= $dateYear)) {
				for ($j = 0; $j < count($monthList); $j++) {
					if ($monthList[$j] == $dateMonth) {
						$value = $yearList[$i] . '-' . $monthList[$j] . '-' . $dateday;
						if (strtotime($value) >= strtotime($dateOfFirstEl) && strtotime($value) <= strtotime($dateOfLastEl)) {
							if (isset($result[$value])) {
								array_push($result[$value], $id);
							} else {
								$result[$value] = [];
								array_push($result[$value], $id);
							}
						}
					}
				}
			}
		}
		return $result;
	}
	
	private function _getSelfEventPeriodicityMonth($val, $yearList, $monthList, $dateOfFirstEl, $dateOfLastEl, $id, $result)
	{
		$dateYear = mb_substr($val, 0, mb_strpos($val, '-'));
		$dateday = (int)mb_substr($val, mb_strrpos($val, '-') + 1);
		$dateMonth = mb_substr($val, mb_strpos($val, '-') + 1, 2);
		
		for ($i = 0; $i < count($yearList); $i++) {
			for ($j = 0; $j < count($monthList); $j++) {
				if (($yearList[$i] == $dateYear && $monthList[$j] >= $dateMonth) || ($yearList[$i] > $dateYear)) {
					$dateDayNew = $this->_getLastDayOfMonth((int)$yearList[$i], (int)$monthList[$j], $dateday);
					$value = $yearList[$i] . '-' . $monthList[$j] . '-' . $dateDayNew;
					if (strtotime($value) >= strtotime($dateOfFirstEl) && strtotime($value) <= strtotime($dateOfLastEl)) {
						if (isset($result[$value])) {
							array_push($result[$value], $id);
						} else {
							$result[$value] = [];
							array_push($result[$value], $id);
						}
					}
				}
			}
		}

		return $result;
	}
	
	private function _getSelfEventPeriodicityWeek($val, $yearList, $monthList, $dateOfFirstEl, $dateOfLastEl, $id, $result)
	{
		$dateStart = $dateOfFirstEl;
		$dayOfWeek = date('N', strtotime($val));
		$i = 0;
		
		while (strtotime($dateStart) < strtotime($dateOfLastEl)) {
			$value = date('Y-m-d', strtotime($dateOfFirstEl . $i . ' days'));
			if (date('N', strtotime($value)) == $dayOfWeek && strtotime($value) >= strtotime($val)) {
				if (isset($result[$value])) {
					array_push($result[$value], $id);
				} else {
					$result[$value] = [];
					array_push($result[$value], $id);
				}
			}
			$dateStart = $value;
			$i++;
		}

		return $result;
	}
	
	private function _getSelfEventPeriodicityDay($val, $yearList, $monthList, $dateOfFirstEl, $dateOfLastEl, $id, $result)
	{
		$dateStart = $dateOfFirstEl;
		$i = 0;
		
		while (strtotime($dateStart) < strtotime($dateOfLastEl)) {
			$value = date('Y-m-d', strtotime($dateOfFirstEl . $i . ' days'));
			if (strtotime($value) >= strtotime($val)) {
				if (isset($result[$value])) {
					array_push($result[$value], $id);
				} else {
					$result[$value] = [];
					array_push($result[$value], $id);
				}
			}
			$dateStart = $value;
			$i++;
		}

		return $result;
	}

	private function _getLastDayOfMonth($year, $month, $day)
	{
		if (cal_days_in_month(CAL_GREGORIAN, $month, $year) >= $day) {
			return $day;
		}

		while (cal_days_in_month(CAL_GREGORIAN, $month, $year) < $day) {
			$day--;

		}

		return $day;
	}
}