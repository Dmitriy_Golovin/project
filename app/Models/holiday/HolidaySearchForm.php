<?php

namespace App\Models\holiday;

use Common\Forms\BaseForm;
use Common\Models\HolidayModel;
use Common\Models\HolidayDateModel;
use Common\Models\CountryModel;

class HolidaySearchForm extends BaseForm
{
	public $country_code;
	public $query;

	private $_result;

	protected $validationRules = [
		'country_code' => 'string',
		'query' => 'string',
	];

	public function search() {
		if (empty($this->country_code)) {
			$this->setError('country_code', 'Обязательный параметр');
			return false;
		}

		if (!$this->validate()) {
			return false;
		}

		$holidayModel = new HolidayModel;
		$holidayDateModel = new HolidayDateModel();
		$countryModel = new CountryModel();
		$holidayTableName = $holidayModel->getTableName();
		$holidayDateTableName = $holidayDateModel->getTableName();
		$countryTableName = $countryModel->getTableName();

		$holidayQuery = $holidayModel
			->join($holidayDateTableName, $holidayTableName . '.holiday_id = ' . $holidayDateTableName . '.holiday_id')
			->join($countryTableName, $holidayTableName . '.country_id = ' . $countryTableName . '.country_id')
			->where($countryTableName . '.code =', $this->country_code)
			->select($holidayTableName . '.holiday_id, ' . $holidayTableName . '.holiday_day, ' . $countryTableName . '.code, ' . $holidayDateTableName . '.holiday_date');

		if (!empty($this->query)) {
			$holidayQuery->groupStart()
					->like('holiday_day', $this->query)
					->orLike('description', $this->query)
				->groupEnd();
		}

		$holidayQuery = $holidayQuery->orderBy($holidayTableName . '.holiday_day', 'ASC')
			->groupBy($holidayTableName . '.holiday_id')->get();

		foreach ($holidayQuery->getResultObject() as &$item) {
			$item->link = $item->code . '/' . $item->holiday_id;
			$item->holiday_date = $holidayModel::getHolidayDay($item->holiday_date);
			$this->_result[] = $item;
		}

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}