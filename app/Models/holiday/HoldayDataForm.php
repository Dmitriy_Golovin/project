<?php

namespace App\Models\holiday;

use Common\Forms\BaseForm;
use Common\Models\HolidayModel;
use Common\Models\HolidayDateModel;
use Common\Models\CountryModel;
use Common\Models\FileModel;

class HoldayDataForm extends BaseForm
{
	public $holiday_id;
	public $countryCode;

	private $_result;

	protected $validationRules = [
		'holiday_id' => 'integer',
		'countryCode' => 'string',
	];

	public function getHolidayData() {
		if (!$this->validate()) {
			return false;
		}

		$holidayModel = new HolidayModel();
		$holidayDateModel = new HolidayDateModel();
		$countryModel = new CountryModel();

		$holiday = $holidayModel
			->select($holidayModel->getTableName() . '.*')
			->select($holidayDateModel->getTableName() . '.holiday_date')
			->select('fileTable.url as imageUrl, fileTable.type as fileType, fileTable.title as fileTitle')
			->join($countryModel->getTableName(), $holidayModel->getTableName() . '.country_id = ' . $countryModel->getTableName() . '.country_id')
			->join($holidayDateModel->getTableName(), $holidayDateModel->getTableName() . '.holiday_id =' . $holidayModel->getTableName() . '.holiday_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_HOLIDAY . '" and fileTable.item_id = ' . $holidayModel->getTableName() . '.holiday_id', 'left')
			->where($holidayModel->getTableName() . '.holiday_id', $this->holiday_id)
			->where($countryModel->getTableName() . '.code', $this->countryCode)
			->first();

		if (empty($holiday)) {
			return false;
		}

		if (!empty($holiday->imageUrl)) {
			$holiday->imageUrl = FileModel::preparePathToOpenFile($holiday->imageUrl, $holiday->fileType);
		}

		if (!empty($holiday->link)) {
			$holiday->linkLabel = mb_substr(explode('/', $holiday->link)[2], 4);
		}

		$holiday->holiday_date = $holidayModel->getHolidayDay($holiday->holiday_date);
		$this->_result = $holiday;

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}