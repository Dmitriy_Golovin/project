<?php

namespace App\Models\videoFile;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\CommentModel;
use Common\Models\ReportFileModel;
use Common\Models\ReportModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskModel;
use App\Libraries\DateAndTime\DateAndTime;

class VideoFileDataForm extends BaseForm
{
	const NOT_SELF_FILE_PAGE = 0;
	const SELF_FILE_PAGE = 1;
	const NOT_SELF_PAGE_NOT_ENTITY_DATA = 2;

	public $user_id;
	public $file_id;
	public $page;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'file_id' => 'required|integer',
		'page' => 'required|integer|in_list[0,1,2]',
		'timeOffsetSeconds' => 'required|integer',
	];

	public $cleanValidationRules = false;

	private $_result;

	public function getData()
	{
		$fileModel = new FileModel();
		$file = $fileModel
			->select(FileModel::table() . '.*')
			->select('eventTable.event_id, eventTable.name as eventName')
			->select('meetingTable.meeting_id, meetingTable.name as meetingName')
			->select('taskTable.task_id, taskTable.assigned_user_id, taskTable.owner_user_id, taskTable.name as taskName, taskTable.deleted_at as taskDeleted')
			->join(ReportFileModel::table() . ' as reportFileTable', 'reportFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.report_id = reportFileTable.report_id', 'left')
			->join(EventModel::table() . ' as eventTable', 'eventTable.event_id = reportTable.item_id and reportTable.related_item = "' . EventModel::table() . '"', 'left')
			->join(MeetingModel::table() . ' as meetingTable', 'meetingTable.meeting_id = reportTable.item_id and reportTable.related_item = "' . MeetingModel::table() . '"', 'left')
			->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->join(TaskHistoryModel::table() . ' as taskHistoryTable', 'taskHistoryTable.task_history_id = taskHistoryFileTable.task_history_id', 'left')
			->join(TaskModel::table() . ' as taskTable', 'taskTable.task_id = taskHistoryTable.task_id', 'left')
			->where(FileModel::table() . '.file_id', $this->file_id)
			->first();

		if (empty($file)) {
			$this->setError('', lang('File.File does not exist, it may have been deleted'));
			return false;
		}

		if ((int)$file->privacy_status === FileModel::PRIVATE_STATUS
			&& $file->related_item === 'user'
			&& (int)$file->item_id !== (int)$this->user_id) {
			if (!$fileModel->getFilePermission($file, $this->user_id)) {
				$this->setError('', lang('File.File access denied'));
				return false;
			}
		}

		$commentModel = new CommentModel();
		$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
		$file->date = DateAndTime::getDateTime($file->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result = $file;
		$this->_result->commentData = $commentModel->getCommentData(CommentModel::TYPE_TABLE_FILE, $this->_result->file_id, $this->timeOffsetSeconds);

		return true;
	}

	public function getResult()
	{
		$this->_result->selfFile = ((int)$this->_result->item_id === (int)$this->user_id
			&& $this->_result->related_item === FileModel::RELATED_ITEM_USER)
				? true
				: false;
		$this->_result->canDelete = ((int)$this->page === self::SELF_FILE_PAGE)
			? true
			: false;

		if ((int)$this->page === self::SELF_FILE_PAGE) {
			$this->_result->entityData = $this->prepareEntityData($this->_result);
		}

		if ((int)$this->page === self::NOT_SELF_FILE_PAGE) {
			$this->_result->entityData = $this->prepareEntityDataUser($this->_result);
		}

		return ['file' => $this->_result];
	}

	protected function prepareEntityData($item)
	{
		$result = [];

		if (!empty($item->event_id)) {
			$result['entity'] = lang('Event.Event');
			$result['name'] = (!empty($item->eventName))
				? $item->eventName
				: lang('Main.No title');
			$result['href'] = '/event/details/' . $item->event_id;
		}

		if (!empty($item->meeting_id)) {
			$result['entity'] = lang('Meeting.Meeting');
			$result['name'] = (!empty($item->meetingName))
				? $item->meetingName
				: lang('Main.No title');
			$result['href'] = '/meeting/details/' . $item->meeting_id;
		}

		if (!empty($item->task_id) && is_null($item->taskDeleted)) {
			$result['entity'] = lang('Task.Task');
			$result['name'] = (!empty($item->taskName))
				? $item->taskName
				: lang('Main.No title');

			if ((int)$item->assigned_user_id === (int)$this->user_id) {
				$result['href'] = '/task/assigned-details/' . $item->task_id;
			} else if ((int)$item->owner_user_id === (int)$this->user_id) {
				$result['href'] = '/task/owner-details/' . $item->task_id;
			}
		}

		return $result;
	}

	protected function prepareEntityDataUser($item)
	{
		$result = [];

		if (!empty($item->event_id)) {
			$result['entity'] = lang('Event.Event');
			$result['name'] = (!empty($item->eventName))
				? $item->eventName
				: lang('Main.No title');
			$result['href'] = '/profile/' . $item->item_id . '/event/' . $item->event_id;
		}

		if (!empty($item->meeting_id)) {
			$result['entity'] = lang('Meeting.Meeting');
			$result['name'] = (!empty($item->meetingName))
				? $item->meetingName
				: lang('Main.No title');
			$result['href'] = '/profile/' . $item->item_id . '/meeting/' . $item->meeting_id;
		}

		return $result;
	}
}