<?php

namespace App\Models\home;

use Common\Forms\BaseForm;
use Common\Models\UserModel;

class LoginForm extends BaseForm
{
	public $email;
	public $password;
	public $user_id;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'email' => 'valid_email|required',
		'password' => 'string|required|min_length[6]|max_length[24]',
	];

	public function login() {
		if (!$this->validate()) {
			return false;
		}

		$userModel = new userModel();
		$user = $userModel
			->where(['email' => $this->email])
			->where(['veryfication_token' => null])
			->where('deleted_at', null)
			->where('is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->first();

		if (empty($user)) {
			$this->setError('', 'Неверный email или пароль');
			return false;
		}

		if (!password_verify($this->password, $user->password)) {
			$this->setError('', 'Неверный email или пароль');
			return false;
		}

		$this->user_id = $user->user_id;
		return true;
	}
}