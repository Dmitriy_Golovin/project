<?php

namespace App\Models\home;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Components\MailComponent;

class SendRestoreTokenForm extends BaseForm
{
	public $email;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'email' => 'valid_email|required|max_length[255]',
	];

	public function send() {
		helper('text');

		$userModel = new UserModel();
		$user = $userModel
			->where(['email' => $this->email])
			->where(['veryfication_token' => null])
			->first();

		if (empty($user)) {
			$this->setError('', 'Пользователь с таким email не найден');
			return false;
		}

		$user->token_change_safety_data = random_string('alnum', 32);

		if (!$userModel->save($user)) {
			$this->setError('', $userModel->errors());
			return false;
		}

		if (!MailComponent::sendRestoreToken($user->email, $user->token_change_safety_data)) {
			$this->setError('', 'Не удалось отправить email');
			return false;
		}

		return true;
	}
}