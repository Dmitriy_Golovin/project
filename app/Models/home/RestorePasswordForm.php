<?php

namespace App\Models\home;

use Common\Forms\BaseForm;
use Common\Models\UserModel;

class RestorePasswordForm extends BaseForm
{
	public $password;
	public $repeat_password;
	public $token_change_safety_data;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'password' => 'string|required|min_length[6]|max_length[24]',
		'repeat_password' => 'string|required|matches[password]',
		'token_change_safety_data' => 'required|string',
	];

	public function createNewPassword() {
		$userModel = new userModel();

		$user = $userModel
			->where(['token_change_safety_data' => $this->token_change_safety_data])
			->first();

		if (empty($user)) {
			$this->setError('', 'Пользователь не найден');
			return false;
		}

		$user->password = password_hash($this->password, PASSWORD_DEFAULT);
		$user->token_change_safety_data = null;

		if (!$userModel->save($user)) {
			var_dump($userModel->errors());die;
			$this->setError('', $userModel->errors());
			return false;
		}

		return true;
	}
}