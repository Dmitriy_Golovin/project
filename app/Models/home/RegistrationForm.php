<?php

namespace App\Models\home;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Components\MailComponent;

class RegistrationForm extends BaseForm
{
	public $email;
	public $password;
	public $repeat_password;
	public $first_name;
	public $last_name;
	public $accept_terms;

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'email' => 'valid_email|required',
		'password' => 'string|required|min_length[6]|max_length[24]',
		'repeat_password' => 'string|required|matches[password]',
		'first_name' => 'string|required|max_length[255]',
		'last_name' => 'string|required|max_length[255]',
		'accept_terms' => 'required',
	];

	protected $validationMessages = [
        'accept_terms' => [
            'required' => 'Validation.Terms must be accepted',
        ],
    ];

	public function registration() {
		helper('text');

		if (!$this->validate()) {
			return false;
		}

		$userModel = new UserModel();
		$dataDb['first_name'] = $this->first_name;
		$dataDb['last_name'] = $this->last_name;
		$dataDb['password'] = password_hash($this->password, PASSWORD_DEFAULT);
		$dataDb['email'] = $this->email;
		$dataDb['veryfication_token'] = random_string('crypto', 32);
		$dataDb['accept_terms'] = true;
		$dataDb['is_blocked'] = UserModel::STATUS_NOT_BLOCKED;

		if (!$userModel->save($dataDb)) {
			$this->setError('', $userModel->errors());
			return false;
		}

		if (!MailComponent::sendActivationToken($dataDb['email'], $dataDb['veryfication_token'])) {
			$this->setError('', lang('Main.Failed to send email'));
			return false;
		}

		return true;
	}
}