<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\PrivateFilePermissionModel;

class FilePermissionUserListForm extends BaseForm
{
	public $user_id;
	public $file_id;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'file_id' => 'required|integer',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_userCount;
	private $_result;

	public function getList()
	{
		$fileModel = new FileModel();
		$file = $fileModel
			->where('file_id', $this->file_id)
			->where('privacy_status', FileModel::PRIVATE_STATUS)
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where('item_id', $this->user_id)
			->first();

		if (empty($file)) {
			$this->setError('', lang('File.File not found'));
			return false;
		}

		$userModel = new UserModel();
		$userQuery = $userModel
			->select('user_id, concat(' . UserModel::table() .'.first_name, " ",  ' . UserModel::table() . '.last_name) as fullName, fileTable.url as path_to_ava')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . UserModel::table() . '.file_id' , 'left')
			->join(PrivateFilePermissionModel::table() . ' as filePermissionTable', 'filePermissionTable.permitted_id = ' . UserModel::table() . '.user_id')
			->where('filePermissionTable.owner_id', $this->user_id)
			->where('filePermissionTable.file_id', $this->file_id)
			->where('filePermissionTable.created_at <=', $this->timestamp);

		$this->_userCount = $userQuery->countAllResults(false);

		$userQuery = $userQuery->orderBy('fullName', 'ASC')
				->getCompiledSelect();

		$userQuery = 'select (@rowNum := @rowNum + 1) as rowNum, userQuery.* from (' . $userQuery . ') as userQuery, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$userQuery = 'select userQuery.* from (' . $userQuery . ') as userQuery where rowNum > ' . $userModel->db->escape($this->rowNum);
		}

		$userQuery = 'select userQuery.* from (' . $userQuery . ') as userQuery limit ' . $this->limit;
		$this->_result = $userModel->db->query('select userQuery.* from (' . $userQuery . ') as userQuery');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['userResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['userCount'] = $this->_userCount;
		$result['type'] = 'file-permission-cancel';

		foreach ($this->_result->getResultObject() as $item) {
			if (empty($item->path_to_ava)) {
				$item->path_to_ava = FileModel::DEFAULT_AVA_URL;
			} else {
				$item->path_to_ava = FileModel::preparePathToAva($item->path_to_ava);
			}

			$result['userResult'][] = $item;
		}

		return $result;
	}
}