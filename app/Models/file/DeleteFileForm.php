<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\FileModel;

class DeleteFileForm extends BaseForm
{
	public $user_id;
	public $file_id;
	public $type;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'file_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,3,4]',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function delete()
	{
		$fileModel = new FileModel();
		$file = $fileModel
			->where('file_id', $this->file_id)
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where('item_id', $this->user_id)
			->where('type', $this->type)
			->first();

		if (empty($file)) {
			$this->setError('', lang('File.File not found'));
			return false;
		}

		$this->_result = $file;

		if (!$fileModel->delete($file->file_id)) {
			$this->setError('', lang('File.Failed to delete file'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		return ['type' => $this->_result->type, 'file_id' => $this->_result->file_id];
	}
}