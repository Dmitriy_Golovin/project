<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\PrivateFilePermissionModel;

class RemoveUserFilePermissionForm extends BaseForm
{
	public $user_id;
	public $file_id;
	public $profile_id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'file_id' => 'required|integer',
		'profile_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function remove()
	{
		$fileModel = new FileModel();
		$file = $fileModel
			->where('file_id', $this->file_id)
			->where('privacy_status', FileModel::PRIVATE_STATUS)
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where('item_id', $this->user_id)
			->first();

		if (empty($file)) {
			$this->setError('', lang('File.File not found'));
			return false;
		}

		$privateFilePermissionModel = new PrivateFilePermissionModel();
		$filePermission = $privateFilePermissionModel
			->where('file_id', $this->file_id)
			->where('owner_id', $this->user_id)
			->where('permitted_id', $this->profile_id)
			->first();

		if (empty($filePermission)) {
			$this->setError('', lang('PrivateFilePermission.The user does not have access to the file'));
			return false;
		}

		if (!$privateFilePermissionModel->delete($filePermission->private_file_permission_id)) {
			$this->setError('', lang('PrivateFilePermission.Failed to remove file permissions'));
			return false;
		}

		return true;
	}
}