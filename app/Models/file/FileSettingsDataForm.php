<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\ReportFileModel;
use Common\Models\ReportModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskModel;
use App\Libraries\DateAndTime\DateAndTime;

class FileSettingsDataForm extends BaseForm
{
	public $user_id;
	public $file_id;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'file_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getData()
	{
		$fileModel = new FileModel();
		$this->_result = $fileModel
			->select(FileModel::table() . '.*')
			->select('eventTable.event_id, eventTable.name as eventName')
			->select('meetingTable.meeting_id, meetingTable.name as meetingName')
			->select('taskTable.task_id, taskTable.assigned_user_id, taskTable.owner_user_id, taskTable.name as taskName, taskTable.deleted_at as taskDeleted')
			->join(ReportFileModel::table() . ' as reportFileTable', 'reportFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.report_id = reportFileTable.report_id', 'left')
			->join(EventModel::table() . ' as eventTable', 'eventTable.event_id = reportTable.item_id and reportTable.related_item = "' . EventModel::table() . '"', 'left')
			->join(MeetingModel::table() . ' as meetingTable', 'meetingTable.meeting_id = reportTable.item_id and reportTable.related_item = "' . MeetingModel::table() . '"', 'left')
			->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
			->join(TaskHistoryModel::table() . ' as taskHistoryTable', 'taskHistoryTable.task_history_id = taskHistoryFileTable.task_history_id', 'left')
			->join(TaskModel::table() . ' as taskTable', 'taskTable.task_id = taskHistoryTable.task_id', 'left')
			->where(FileModel::table() . '.file_id', $this->file_id)
			->where(FileModel::table() . '.related_item', FileModel::RELATED_ITEM_USER)
			->where(FileModel::table() . '.item_id', $this->user_id)
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('File.File not found'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$this->_result->entityData = $this->prepareEntityData($this->_result);
		$this->_result->date = DateAndTime::getDateTime($this->_result->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->privacyLabel = FileModel::getPrivacyStatusLabel()[$this->_result->privacy_status];
		$this->_result->privacyValueButton = FileModel::getPrivacyStatusValueButton()[$this->_result->privacy_status];

		return ['fileData' => $this->_result];
	}

	protected function prepareEntityData($item)
	{
		$result = [];

		if (!empty($item->event_id)) {
			$result['entity'] = lang('Event.event');
			$result['name'] = (!empty($item->eventName))
				? $item->eventName
				: lang('Main.No title');
			$result['href'] = '/event/details/' . $item->event_id;
		}

		if (!empty($item->meeting_id)) {
			$result['entity'] = lang('Meeting.meeting');
			$result['name'] = (!empty($item->meetingName))
				? $item->meetingName
				: lang('Main.No title');
			$result['href'] = '/meeting/details/' . $item->meeting_id;
		}

		if (!empty($item->task_id) && is_null($item->taskDeleted)) {
			$result['entity'] = lang('Task.task');
			$result['name'] = (!empty($item->taskName))
				? $item->taskName
				: lang('Main.No title');

			if ((int)$item->assigned_user_id === (int)$this->user_id) {
				$result['href'] = '/task/assigned-details/' . $item->task_id;
			} else if ((int)$item->owner_user_id === (int)$this->user_id) {
				$result['href'] = '/task/owner-details/' . $item->task_id;
			}
		}

		return $result;
	}
}