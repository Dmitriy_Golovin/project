<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\UserModel;
use CodeIgniter\Database\BaseBuilder;
use App\Libraries\DateAndTime\DateAndTime;

class FileListByTypeForm extends BaseForm
{
	public $fileType;
	public $timeOffsetSeconds;
	public $user_id;

	public $resultList = [];

	protected $cleanValidationRules = false;

	protected $validationRules = [
		'fileType' => 'required|integer|in_list[1,3,4]',
		'timeOffsetSeconds' => 'required|integer',
		'user_id' => 'required|integer',
	];

	public function getList() {
		$fileModel = new FileModel();
		$userModel = new UserModel();
		$user_id = $this->user_id;

		$query = $fileModel
			->where('related_item', 'user')
			->where('item_id', $this->user_id)
			->where('type', $this->fileType)
			->whereNotIn('file_id', function(BaseBuilder $builder) use ($user_id, $userModel) {
				return $builder->select('file_id')
					->from($userModel->getTableName())
					->where('user_id', $user_id)
					->where('file_id IS NOT NULL');
			})
			->get();
		
		foreach ($query->getResult() as &$file) {
			$file->url = FileModel::preparePathToUserFile($file->url, $this->fileType);
			$file->privacy_status = (int)$file->privacy_status;
			$file->type = (int)$file->type;
			$file->name = !empty($file->original_name) ? $file->original_name : $fileModel->getOriginalFileName($file->url);
			$dateAndTimeArr = DateAndTime::getDateTime($file->created_at, $this->timeOffsetSeconds, 'genetive');
			$file->date = lang('Main.Added') . ' ' . $dateAndTimeArr['date'] . ' ' . $dateAndTimeArr['time'];
			$this->resultList[] = $file;
		}

		return true;
	}
}