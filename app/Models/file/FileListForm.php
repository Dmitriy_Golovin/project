<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskModel;
use App\Libraries\DateAndTime\DateAndTime;

class FileListForm extends BaseForm
{
	public $user_id;
	public $type;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 30;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|string|in_list[image,video,document]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;
	private $_fileCount;

	public function getList()
	{
		$fileModel = new FileModel();
		$this->_result = $fileModel
			->select(FileModel::table() . '.*');

		if ($this->type === config('FileConfig')->filePageTypeDocument) {
			$this->_result
				->select('taskTable.task_id, taskTable.assigned_user_id, taskTable.owner_user_id, taskTable.name as taskName, taskTable.deleted_at as taskDeleted')
				->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id', 'left')
				->join(TaskHistoryModel::table() . ' as taskHistoryTable', 'taskHistoryTable.task_history_id = taskHistoryFileTable.task_history_id', 'left')
				->join(TaskModel::table() . ' as taskTable', 'taskTable.task_id = taskHistoryTable.task_id', 'left');
		}

		$this->_result->where('related_item', FileModel::RELATED_ITEM_USER)
			->where(FileModel::table() . '.item_id', $this->user_id)
			->where(FileModel::table() . '.type', FileModel::getTypeByFileConfig()[$this->type])
			->where(FileModel::table() . '.created_at <=', $this->timestamp)/*
			->orderBy('created_at', 'ASC')*/;

		$this->_fileCount = $this->_result->countAllResults(false);

		$this->_result = $this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $fileModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $fileModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['fileResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['fileCount'] = $this->_fileCount;
		$result['type'] = $this->type;

		foreach ($this->_result->getResultObject() as $file) {
			$file->url = FileModel::preparePathToUserFile($file->url, $file->type);
            $file->name = FileModel::getFileNameWithoutExec($file->original_name);

            if ($this->type === config('FileConfig')->filePageTypeDocument) {
            	$file->selfFile = true;
				$file->canDelete = true;
            	$file->entityData = $this->prepareEntityData($file);
            	$file->documentDate = DateAndTime::getDateTime($file->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
            }

			$result['fileResult'][] = $file;
		}
// var_dump($result);die;
		return $result;
	}

	protected function prepareEntityData($item)
	{
		$result = [];

		if (!empty($item->task_id) && is_null($item->taskDeleted)) {
			$result['entity'] = lang('Task.Task');
			$result['name'] = (!empty($item->taskName))
				? $item->taskName
				: lang('Main.No title');

			if ((int)$item->assigned_user_id === (int)$this->user_id) {
				$result['href'] = '/task/assigned-details/' . $item->task_id;
			} else if ((int)$item->owner_user_id === (int)$this->user_id) {
				$result['href'] = '/task/owner-details/' . $item->task_id;
			}
		}

		return $result;
	}
}