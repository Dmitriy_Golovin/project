<?php

namespace App\Models\file;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\PrivateFilePermissionModel;

class AddUserFilePermissionForm extends BaseForm
{
	public $user_id;
	public $file_id;
	public $profile_id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'file_id' => 'required|integer',
		'profile_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function add()
	{
		$fileModel = new FileModel();
		$file = $fileModel
			->where('file_id', $this->file_id)
			->where('privacy_status', FileModel::PRIVATE_STATUS)
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where('item_id', $this->user_id)
			->first();

		if (empty($file)) {
			$this->setError('', lang('File.File not found'));
			return false;
		}

		$userModel = new UserModel();
		$this->_result = $userModel->getUserData($this->profile_id);

		if (!$this->_result) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->profile_id]));
			return false;
		}

		$privateFilePermissionModel = new PrivateFilePermissionModel();
		$filePermission = $privateFilePermissionModel
			->where('file_id', $this->file_id)
			->where('owner_id', $this->user_id)
			->where('permitted_id', $this->profile_id)
			->first();

		if (!empty($filePermission)) {
			$this->setError('', lang('PrivateFilePermission.The user already has access to the file'));
			return false;
		}

		$filePermissionData = [
			'file_id' => $this->file_id,
			'owner_id' => $this->user_id,
			'permitted_id' => $this->profile_id,
		];

		if (!$privateFilePermissionModel->save($filePermissionData)) {
			$this->setError('', lang('PrivateFilePermission.Failed to save permissions to view the file'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$this->_result->fullName = $this->_result->first_name . ' ' . $this->_result->last_name;
		
		return [
			'user' => $this->_result,
			'type' => 'file-permission-cancel',
		];
	}
}