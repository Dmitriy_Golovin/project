<?php

namespace App\Models\auth;

use Common\Forms\BaseForm;
use Common\Models\UserModel;

class ActivationForm extends BaseForm
{
	public $token;

	protected $validationRules = [
		'token' => 'required|string'
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function activate()
	{
		$userModel = new UserModel();
		$user = $userModel
			->where('veryfication_token', $this->token)
			->first();

		if (empty($user)) {
			return false;
		}

		if (!$userModel->update($user->user_id, ['veryfication_token' => null])) {
			return false;
		}

		if (!$userModel->setSettingsUserDefault($user->user_id)) {
			return false;
		}

		$this->_result = $user->user_id;
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}