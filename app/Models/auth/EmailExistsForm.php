<?php

namespace App\Models\auth;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\ChangeEmailModel;

class EmailExistsForm extends BaseForm
{
	public $email;

	protected $validationRules = [
		'email' => 'required|valid_email',
	];

	public function checkEmail()
	{
		$userModel = new UserModel();
		$changeEmailModel = new ChangeEmailModel();

		$changeEmail = $changeEmailModel
			->where('email_tmp', $this->email)
			->where('changed', changeEmailModel::EMAIL_NOT_CHANGE)
			->first();

		$user = $userModel
			->where('email', $this->email)
			->first();

		if (!empty($user) || !empty($changeEmail)) {
			$this->setError('email', lang('Main.Email already registered'));
			return false;
		}

		return true;
	}
}