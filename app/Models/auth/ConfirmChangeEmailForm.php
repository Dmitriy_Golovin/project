<?php

namespace App\Models\auth;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\ChangeEmailModel;

class ConfirmChangeEmailForm extends BaseForm
{
	public $token;

	protected $validationRules = [
		'token' => 'required|string'
	];

	protected $cleanValidationRules = false;

	public function confirmChange()
	{
		$changeEmailModel = new ChangeEmailModel();
		$userModel = new UserModel();
		$changeEmailItem = $changeEmailModel
			->select('change_email_id, user_id, email_tmp')
			->where('token', $this->token)
			->first();

		if (empty($changeEmailItem)) {
			return false;
		}

		if (!$changeEmailModel->update($changeEmailItem->change_email_id, ['token' => null, 'changed' => ChangeEmailModel::EMAIL_CHANGE])) {
			return false;
		}

		if (!$userModel->update($changeEmailItem->user_id, ['email' => $changeEmailItem->email_tmp])) {
			return false;
		}

		return true;
	}
}