<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;

class ChangePasswordForm extends BaseForm
{
	public $password;
	public $repeat_password;
	public $user_id;
	public $tcsd;

	protected $validationRules = [
		'password' => 'string|required|min_length[6]|max_length[24]',
		'repeat_password' => 'string|required|matches[password]',
		'user_id' => 'integer|required',
		'tcsd' => 'required|string',
	];

	public function change()
	{
		$userModel = new UserModel();
		$this->password = password_hash($this->password, PASSWORD_DEFAULT);
		$user = $userModel
			->where('user_id', $this->user_id)
			->where('token_change_safety_data', $this->tcsd)
			->first();

		if (empty($user)) {
			$this->setError('', 'Main.Invalid token');
			return false;
		}

		$data['user_id'] = $this->user_id;
		$data['password'] = $this->password;
		$data['token_change_safety_data'] = null;

		if (!$userModel->save($data)) {
			$this->setError('', $userModel->errors());
			return false;
		}

		return true;
	}
}