<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class LoadFileForm extends BaseForm
{
	public $user_id;
	public $fileList;
	public $fileType;
	public $timeOffsetSeconds;

	private $_result;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'fileType' => 'required|integer|in_list[1,2,3,4]',
		'fileList[]' => [
            'uploaded[fileList]',
            'max_size[fileList,20000]',
        ],
	];

	protected $cleanValidationRules = false;

	public function load()
	{
		if (count($this->fileList) > config('FileConfig')->maxUploadFileCount) {
			$this->setError('', lang('File.Limit on the number of files to upload at one time - {0}', [config('FileConfig')->maxUploadFileCount]));
			return false;
		}

		foreach ($this->fileList as $file) {
			$fileModel = new FileModel();
			$fileDataDisk = FileModel::saveFileOnDisk($file, $this->fileType);
			$fileData = [
				'type' => $this->fileType,
				'related_item' => 'user',
				'item_id' => $this->user_id,
				'url' => $fileDataDisk['fileName'],
				'privacy_status' => FileModel::PRIVATE_STATUS,
				'mime_type' => $fileDataDisk['mimeType'],
				'original_name' => $fileDataDisk['originalName'],
				'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
				'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
			];

			if (!$fileModel->save($fileData)) {
				$this->setError('', $fileModel->errors());
				return false;
			}

			$savedFile = $fileModel->find($fileModel->getInsertID());
			$dateAndTimeArr = DateAndTime::getDateTime($savedFile->created_at, $this->timeOffsetSeconds, 'genetive');
			$this->_result[] = [
				'file_id' => $savedFile->file_id,
				'name' => $savedFile->original_name,
				'url' => FileModel::preparePathToUserFile($fileDataDisk['fileName'], $this->fileType),
				'privacy_status' => $savedFile->privacy_status,
				'date' => lang('Main.Added') . ' ' . $dateAndTimeArr['date'] . ' ' . $dateAndTimeArr['time'],
			];
		}

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}