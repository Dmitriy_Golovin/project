<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Models\FileModel;

class FriendshipUserListForm extends BaseForm
{
	const SUBSCRIBER_PAGE_LABEL = 'friends_requests';
    const SUBSCRIPTION_PAGE_LABEL = 'friends_subscriptions';
    const FRIEND_PAGE_LABEL = 'friends';

	public $user_id;
	public $userPageId;
	public $friendMenuItem;

	private $_result;
	private $_userFriendData;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'userPageId' => 'required|integer',
		'friendMenuItem' => 'required|string',	
	];

	protected $cleanValidationRules = false;

	public function getList()
	{
		$userModel = new UserModel();
		$friendModel = new FriendModel();
		$fileModel = new FileModel();
		$userTableName = $userModel->getTableName();
		$friendTableName = $friendModel->getTableName();
		$fileTableName = $fileModel->getTableName();
		$userPage = $userModel->find($this->userPageId);

		if (empty($userPage)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->userPageId]));
			return false;
		}

		$this->_result['friendsAmount'] = $friendModel->getAmountOfUsersInSpecificRelationships($this->userPageId);
		$this->_result['res'] = $this->friendMenuItem;
		$this->_result['selfID'] = $this->user_id;

		$this->_userFriendData = $userModel
			->select($userTableName . '.user_id, ' . $userTableName . '.first_name, ' . $userTableName . '.last_name, ' . $fileTableName . '.url as path_to_ava')
			->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id' , 'left');
		
		if ($this->friendMenuItem === self::FRIEND_PAGE_LABEL) {
			$this->_userFriendData = $this->_userFriendData
				->join($friendTableName . ' as friendReciept', 'friendReciept.id_request_recipient = ' . $userTableName . '.user_id', 'left')
				->join($friendTableName . ' as friendSender', 'friendSender.id_request_sender = ' . $userTableName . '.user_id', 'left')
				->groupStart()
					->where('friendReciept.id_request_sender', $this->userPageId)
					->where('friendReciept.status', FriendModel::FRIEND_STATUS)
				->groupEnd()
				->orGroupStart()
					->where('friendSender.id_request_recipient', $this->userPageId)
					->where('friendSender.status', FriendModel::FRIEND_STATUS)
				->groupEnd()
				->get();
		}

		if ($this->friendMenuItem === self::SUBSCRIBER_PAGE_LABEL) {
			$this->_userFriendData = $this->_userFriendData
				->select('if(status = 0, 1, 0) as newSubscriber')
				->join($friendTableName, $friendTableName . '.id_request_sender = ' . $userTableName . '.user_id')
				->where($friendTableName . '.id_request_recipient', $this->userPageId)
				->whereIn($friendTableName . '.status', [FriendModel::NEW_SUBSCRIBER_STATUS, FriendModel::SUBSCRIBER_STATUS])
				->get();
		}

		if ($this->friendMenuItem === self::SUBSCRIPTION_PAGE_LABEL) {
			$this->_userFriendData = $this->_userFriendData
				->join($friendTableName, $friendTableName . '.id_request_recipient = ' . $userTableName . '.user_id')
				->where($friendTableName . '.id_request_sender', $this->userPageId)
				->whereIn($friendTableName . '.status', [FriendModel::NEW_SUBSCRIBER_STATUS, FriendModel::SUBSCRIBER_STATUS])
				->get();
		}

		return true;
	}

	public function getResult()
	{
		$this->_result['list'] = [];

		foreach ($this->_userFriendData->getResultObject() as $item) {
			if (empty($item->path_to_ava)) {
				$item->path_to_ava = FileModel::DEFAULT_AVA_URL;
			} else {
				$item->path_to_ava = FileModel::preparePathToAva($item->path_to_ava);
			}

			$this->_result['list'][] = $item;
		}

		return $this->_result;
	}
}