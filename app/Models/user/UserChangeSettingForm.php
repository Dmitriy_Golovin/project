<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\UserSettingModel;

class UserChangeSettingForm extends BaseForm
{
	public $user_id;
	public $work_week_start;
	public $work_days_amount;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'work_week_start' => 'required|integer|in_list[1,2,3,4,5,6,7]',
		'work_days_amount' => 'required|integer|in_list[1,2,3,4,5,6,7]',
	];

	protected $cleanValidationRules = false;

	// private $_result;

	public function change()
	{
		$userModel = new UserModel();
		$user = $userModel->find($this->user_id);

		if (empty($user)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->user_id]));
			return false;
		}

		$userSettingModel = new UserSettingModel();

		$userSettingModel->db->table(UserSettingModel::table())
			->where('user_id', $this->user_id)
			->update([
				'work_week_start' => $this->work_week_start,
				'work_days_amount' => $this->work_days_amount,
			]);

		/*$userSettingModel
			->where('user_id', $this->user_id)
			->set('work_week_start', $this->work_week_start)
			->set('work_days_amount', $this->work_days_amount)
			->update();*/

		if (!$userSettingModel->db->affectedRows()) {
			$this->setError('', lang('Main.Failed to save user settings'));
			return false;
		}

		return true;
	}

	// public function getResult()
	// {
	// 	return $
	// }
}