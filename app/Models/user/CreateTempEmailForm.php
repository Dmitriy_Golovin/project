<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\ChangeEmailModel;
use Common\Models\UserModel;
use Common\Components\MailComponent;

class CreateTempEmailForm extends BaseForm
{
	public $user_id;
	public $email_tmp;
	public $tcsd;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'email_tmp' => 'required|valid_email|max_length[255]',
		'tcsd' => 'required|string',
	];

	public function create()
	{
		helper('text');
		$userModel = new userModel();
		$user = $userModel
			->where('user_id', $this->user_id)
			->where('token_change_safety_data', $this->tcsd)
			->first();

		if (empty($user)) {
			$this->setError('', lang('Main.Invalid token'));
			return false;
		}

		$changeEmailModel = new ChangeEmailModel();

		$existChangeEmail = $changeEmailModel
			->where('user_id', $this->user_id)
			->where('changed', ChangeEmailModel::EMAIL_NOT_CHANGE)
			->first();

		if (!empty($existChangeEmail)) {
			$this->setError('', lang('Main.Confirm previous email change'));
			return false;
		}

		$data['user_id'] = $this->user_id;
		$data['email_tmp'] = $this->email_tmp;
		$data['token'] = random_string('crypto', 32);
		$data['changed'] = ChangeEmailModel::EMAIL_NOT_CHANGE;

		if (!$changeEmailModel->save($data)) {
			$this->setError('', $changeEmailModel->errors());
			return false;
		}

		// if (!MailComponent::sendTokenChangeEmail($this->email_tmp, $data['token'])) {
		// 	$this->setError('', lang('Main.Failed to send email'));
		// 	return false;
		// }

		$userData = [
			'user_id' => $user->user_id,
			'token_change_safety_data' => null,
		];

		if (!$userModel->save($userData)) {
			$this->setError('', $userModel->errors());
			return false;
		}

		return true;
	}
}