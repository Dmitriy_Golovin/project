<?php

namespace app\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\FriendModel;
use Common\Models\PrivateFilePermissionModel;
use CodeIgniter\Database\BaseBuilder;

class UserListForm extends BaseForm
{
	const EXCEPTION_ENTITY_FILE_PERMISSION = 1;

	public $user_id;
	public $searchString;
	public $only_friends;
	public $group_id;
	// public $within_group;
	public $type;
	public $exceptionUserIdList;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $exceptionEntityId;
	public $exceptionEntity;
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'searchString' => 'string',
		'only_friends' => 'integer|in_list[0,1]',
		// 'within_group' => 'integer|in_list[0,1]',
		'type' => 'string',
		'group_id' => 'integer',
		'rowNum' => 'integer',
		'timestamp' => 'required|integer',
		'exceptionUserIdList' => 'string',
		'exceptionEntityId' => 'integer',
		'exceptionEntity' => 'integer|in_list[1]'
	];

	private $_userCount;
	private $_result;

	public function getList()
	{
		$userModel = new UserModel();
		$fileModel = new FileModel();
		$userTableName = $userModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$userQuery = $userModel
			->select('user_id, concat(' . $userTableName .'.first_name, " ",  ' . $userTableName . '.last_name) as fullName, ' . $fileTableName . '.url as path_to_ava')
			->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id' , 'left')
			->where($userTableName . '.user_id !=', $this->user_id)
			->where('veryfication_token', null)
			->where($userTableName . '.deleted_at', null)
			->where($userTableName . '.is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->where($userTableName . '.created_at <=', $this->timestamp);

		if (!empty($this->searchString)) {
			$this->searchString = trim($this->searchString);
			$userQuery->groupStart()
					->like('first_name', $this->searchString)
					->orLike('last_name', $this->searchString)
					->orLike('concat(' . $userTableName .'.first_name, " ", ' . $userTableName . '.last_name)', $this->searchString)
				->groupEnd();
		}

		if (!empty($this->only_friends)) {
			$friendModel = new FriendModel();
			$friendTableName = $friendModel->getTableName();

			$userQuery->groupStart()
				->whereIn($userTableName . '.user_id', function (BaseBuilder $builder) use ($friendTableName) {
					return $builder->select('id_request_sender')
						->from($friendTableName)
						->where('status', FriendModel::FRIEND_STATUS)
						->where('id_request_recipient', $this->user_id);
				})
				->orWhereIn($userTableName . '.user_id', function (BaseBuilder $builder) use ($friendTableName) {
					return $builder->select('id_request_recipient')
						->from($friendTableName)
						->where('status', FriendModel::FRIEND_STATUS)
						->where('id_request_sender', $this->user_id);
				})
				->groupEnd();
		}

		if (!empty($this->exceptionEntityId) && !empty($this->exceptionEntity)) {
			if ((int)$this->exceptionEntity === self::EXCEPTION_ENTITY_FILE_PERMISSION) {
				$filePermissionTable = PrivateFilePermissionModel::table();
				$userQuery->whereNotIn($userTableName . '.user_id', function (BaseBuilder $builder) use ($filePermissionTable) {
					return $builder->select('permitted_id')
						->from($filePermissionTable)
						->where('file_id', $this->exceptionEntityId)
						->where('owner_id', $this->user_id);
				});
			}
		}

		if (!empty($this->exceptionUserIdList)) {
			$userQuery->whereNotIn($userTableName.'.user_id', json_decode($this->exceptionUserIdList));
		}

		$this->_userCount = $userQuery->countAllResults(false);

		$userQuery = $userQuery->orderBy('fullName', 'ASC')
				->getCompiledSelect();

		// $this->_userCount = $userModel->db->query('select userCount.* from (' . $userQuery . ') as userCount')->getNumRows();

		$userQuery = 'select (@rowNum := @rowNum + 1) as rowNum, userQuery.* from (' . $userQuery . ') as userQuery, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$userQuery = 'select userQuery.* from (' . $userQuery . ') as userQuery where rowNum > ' . $userModel->db->escape($this->rowNum);
		}

		$userQuery = 'select userQuery.* from (' . $userQuery . ') as userQuery limit ' . $this->limit;
		$this->_result = $userModel->db->query('select userQuery.* from (' . $userQuery . ') as userQuery');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['userResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['userCount'] = $this->_userCount;
		$result['type'] = $this->type;

		foreach ($this->_result->getResultObject() as $item) {
			if (empty($item->path_to_ava)) {
				$item->path_to_ava = FileModel::DEFAULT_AVA_URL;
			} else {
				$item->path_to_ava = FileModel::preparePathToAva($item->path_to_ava);
			}

			$result['userResult'][] = $item;
		}

		return $result;
	}
}