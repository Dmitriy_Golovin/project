<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;
use Common\Models\EventGuestUserModel;
use App\Libraries\AuthJWT\AuthController;
use App\Libraries\AuthJWT\AuthModel;

class DeleteAccountForm extends BaseForm
{
	public $user_id;
	public $tcsd;
	public $access;

	protected $validationRules =[
		'user_id' => 'required|integer',
		'tcsd' => 'required|string',
		'access' => 'required|string',
	];

	protected $cleanValidationRules = false;

	public function delete()
	{
		$userModel = new UserModel();
		$user = $userModel
			->where('veryfication_token', null)
			->where('deleted_at', null)
			->where('is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->where('token_change_safety_data', $this->tcsd)
			->where('user_id', $this->user_id)
			->first();

		if (empty($user)) {
			$this->setError('', lang('Main.User with id {0} not found', [$this->user_id]));
			return false;
		}

		$userModel->db->transBegin();

		if (!$userModel->delete($this->user_id)) {
			$this->setError('', lang('Main.Failed to delete account'));
			$userModel->db->transRollback();
			return false;
		}

		$chatModel = new ChatModel();
		$chatMemberModel = new ChatMemberModel();
		$chatMemberCountQuery = $chatMemberModel
			->select('count(chat_member_id)')
			->where(ChatMemberModel::table() . '.chat_id = ' . ChatModel::table() . '.chat_id')
			->where('deleted_at', null)
			->getCompiledSelect();

		$isCreaterOrAdminChatQuery = $chatMemberModel
			->select('chat_member_id')
			->where(ChatMemberModel::table() . '.chat_id = ' . ChatModel::table() . '.chat_id')
			->where('deleted_at', null)
			->whereIn('role', [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN])
			->where('user_id', $user->user_id)
			->getCompiledSelect();

		$hasAnotherCreaterOrAdminChatQuery = $chatMemberModel
			->select('chat_member_id')
			->where(ChatMemberModel::table() . '.chat_id = ' . ChatModel::table() . '.chat_id')
			->where('deleted_at', null)
			->whereIn('role', [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN])
			->where('user_id !=', $user->user_id)
			->limit(1)
			->getCompiledSelect();

		$adminCandidatQuery = $chatMemberModel
			->select('chat_member_id')
			->where(ChatMemberModel::table() . '.chat_id = ' . ChatModel::table() . '.chat_id')
			->where('deleted_at', null)
			->where('role', ChatMemberModel::ROLE_MEMBER)
			->where('user_id !=', $user->user_id)
			->limit(1)
			->getCompiledSelect();

		$chatQuery = $chatModel
			->select(ChatModel::table() . '.chat_id')
			->select('chatMemberTable.chat_member_id')
			->select('(' . $chatMemberCountQuery . ') as chatMemberCount')
			->select('if((' . $isCreaterOrAdminChatQuery . ') is not null, 1, 0) as isCreaterOrAdmin')
			->select('if((' . $hasAnotherCreaterOrAdminChatQuery . ') is not null, 1, 0) as hasAnotherCreaterOrAdmin')
			->select('(' . $adminCandidatQuery . ') as adminCandidateId')
			->join(ChatMemberModel::table() . ' as chatMemberTable', 'chatMemberTable.chat_id = ' . ChatModel::table() . '.chat_id')
			->where('chatMemberTable.user_id', $user->user_id)
			->where('chatMemberTable.deleted_at', null)
			->get();

		$adminChatMemberIdList = [];

		foreach ($chatQuery->getResultObject() as $item) {
			if ((int)$item->chatMemberCount <= 1) {
				if (!$chatModel->delete($item->chat_id)) {
					$this->setError('', lang('Main.Failed to delete account'));
					$userModel->db->transRollback();
					return false;
				}
			} else {
				if ((boolean)$item->isCreaterOrAdmin) {
					if (!(boolean)$item->hasAnotherCreaterOrAdmin) {
						$adminChatMemberIdList[] = $item->adminCandidateId;
					}
				}
			}

			if (!$chatMemberModel->delete($item->chat_member_id)) {
				$this->setError('', lang('Main.Failed to delete account'));
				$userModel->db->transRollback();
				return false;
			}
		}

		if (!empty($adminChatMemberIdList)) {
			$chatMemberModel->update($adminChatMemberIdList, ['role' => ChatMemberModel::ROLE_ADMIN]);
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$eventGuestUserModel->where('user_id', $user->user_id)
			->delete();

		$eventModel = new EventModel();
		$eventModel->where('user_id', $user->user_id)
			->where('deleted_at', null)
			->delete();

		$meetingModel = new MeetingModel();
		$meetingModel->where('user_id', $user->user_id)
			->where('deleted_at', null)
			->delete();

		$taskModel = new TaskModel();
		$taskModel->where('owner_user_id', $user->user_id)
			->where('deleted_at', null)
			->delete();

		$taskModel->where('assigned_user_id', $user->user_id)
			->where('deleted_at', null)
			->groupStart()
				->groupStart()
					->where('periodicity', TaskModel::PERIODICITY_NONE)
                    ->where('status', TaskModel::STATUS_NOT_COMPLETED)
				->groupEnd()
				->orGroupStart()
					->where('is_cloned', TaskModel::STATUS_NOT_CLONED)
                    ->where('periodicity !=', TaskModel::PERIODICITY_NONE)
				->groupEnd()
			->groupEnd()
			->set([
				'assigned_user_id' => null,
				'status' => TaskModel::STATUS_NO_EXECUTOR,
				'is_viewed_assigned' => TaskModel::STATUS_NOT_VIEW_ASSIGNED,
				'review_status' => TaskModel::STATUS_NOT_REVIEWED,
			])
			->update();

		$auth = new AuthController();
		$accessDecode = $auth->decodeAccess($this->access);
		
		$db = db_connect();
		$authModel = new AuthModel($db);
		$authModel->deleteTokensFromDB($accessDecode['payload']['jti']);

		$userModel->db->transCommit();
		return true;
	}
}