<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FileModel;

class SetAvatarForm extends BaseForm
{
	public $user_id;
	public $imgId;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'imgId' => 'required|integer',
	];

	private $_result;

	public function set()
	{
		$fileModel = new FileModel();
		$fileImage = $fileModel
			->where('file_id', $this->imgId)
			->where('type', FileModel::TYPE_IMAGE)
			->first();

		if (empty($fileImage)) {
			$this->setError('', lang('File.Image file not found'));
			return false;
		}

		$userModel = new UserModel();
		$userModel->where('user_id', $this->user_id)
			->set(['file_id' => $fileImage->file_id]);

		if (!$userModel->update()) {
			$this->setError('', lang('Main.Failed to set avatar'));
			return false;
		}

		$this->_result['path_to_ava'] = FileModel::preparePathToAva($fileImage->url);
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}