<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Models\FileModel;
use CodeIgniter\Database\BaseBuilder;

class SearchUsersWithoutFriendsForm extends BaseForm
{
	public $user_id;
	public $keyword;

	private $limit = 50;
	private $_result;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'keyword' => 'string',
	];

	public function getList()
	{
		$userModel = new UserModel();
		$friendModel = new FriendModel();
		$fileModel = new FileModel();
		$userTableName = $userModel->getTableName();
		$friendTableName = $friendModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$this->_result = $userModel
			->select('user_id, first_name, last_name, ' . $fileTableName . '.url as path_to_ava')
			->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id' , 'left');

		if (!empty($this->keyword)) {
			$this->_result = $this->_result
				->groupStart()
					->like('first_name', $this->keyword)
					->orLike('last_name', $this->keyword)
					->orLike('concat(first_name, " ", last_name)', $this->keyword)
				->groupEnd();
		}

		$this->_result = $this->_result
			->whereNotIn($userTableName . '.user_id', function (BaseBuilder $builder) use ($friendTableName) {
				return $builder->select('id_request_sender')
					->from($friendTableName)
					->where('status', FriendModel::FRIEND_STATUS)
					->where('id_request_recipient', $this->user_id);
			})
			->whereNotIn($userTableName . '.user_id', function (BaseBuilder $builder) use ($friendTableName) {
				return $builder->select('id_request_recipient')
					->from($friendTableName)
					->where('status', FriendModel::FRIEND_STATUS)
					->where('id_request_sender', $this->user_id);
			})
			->where($userTableName . '.user_id !=', $this->user_id)
			->get();

		return true;
	}

	public function getResult()
	{
		$result = [];
		
		foreach ($this->_result->getResultObject() as $item) {
			if (empty($item->path_to_ava)) {
				$item->path_to_ava = FileModel::DEFAULT_AVA_URL;
			}

			$result[] = $item;
		}

		return $result;
	}
}