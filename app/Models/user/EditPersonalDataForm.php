<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\CountryModel;
use Common\Models\CityModel;

class EditPersonalDataForm extends BaseForm
{
	public $user_id;
	public $first_name;
	public $last_name;
	public $country_id;
	public $city_id;

	private $_result;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'first_name' => 'string|max_length[255]',
		'last_name' => 'string|max_length[255]',
		'country_id' => 'permit_empty|integer',
		'city_id' => 'permit_empty|integer',
	];

	public function edit()
	{
		$userModel = new UserModel();
		$countryModel = new CountryModel();
		$cityModel = new CityModel();
		$userData = [];
		$userData['user_id'] = $this->user_id;

		if (!empty($this->country_id) && !$countryModel->isExistCoutryById($this->country_id)) {
			$this->setError('country_id', 'Неверное значение');
			return false;
		}

		if (!empty($this->city_id) && !$cityModel->isExistCityById($this->city_id)) {
			$this->setError('city_id', 'Неверное значение');
			return false;
		}

		if (!empty($this->first_name)) {
			$userData['first_name'] = $this->first_name;
		}

		if (!empty($this->last_name)) {
			$userData['last_name'] = $this->last_name;
		}

		if (!empty($this->country_id)) {
			if (empty($this->city_id)) {
				$user = $userModel->find($this->user_id);

				if (!empty($user->city_id)) {
					$city = $cityModel->find($user->city_id);

					if ((int)$city->country_id !== (int)$this->country_id) {
						$this->setError('', lang('Validation.The "city_id" field is required'));
				return false;
					}
				}
			}

			$userData['country_id'] = $this->country_id;
		}

		if (!empty($this->city_id)) {
			if (empty($this->country_id)) {
				$this->setError('', lang('Validation.The "country_id" field is required'));
				return false;
			}

			$city = $cityModel->find($this->city_id);

			if ((int)$city->country_id !== (int)$this->country_id) {
				$this->setError('city_id', 'Неверное значение');
				return false;
			}

			$userData['city_id'] = $this->city_id;
		}

		if (!$userModel->save($userData)) {
			$this->setError('', $userModel->errors());
			return false;
		}

		$updatedUser = $userModel->getUserData($this->user_id);

		if (empty($updatedUser)) {
			$this->setError('', 'Пользователь не найден');
			return false;
		}

		$this->_result = $updatedUser;

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}