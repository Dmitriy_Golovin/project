<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;

class CheckPasswordForm extends BaseForm
{
	public $user_id;
	public $password;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'password' => 'string|required|min_length[6]|max_length[24]',
	];

	private $_result;

	public function check()
	{
		helper('text');
		$userModel = new UserModel();
		$user = $userModel
			->where('user_id', $this->user_id)
			->first();

		if (empty($user) || !password_verify($this->password, $user->password)) {
			$this->setError('', lang('Main.Invalid password'));
			return false;
		}

		$userData = [
			'user_id' => $user->user_id,
			'token_change_safety_data' => random_string('crypto', 32),
		];

		if (!$userModel->save($userData)) {
			$this->setError('', lang('Main.Failed to save token'));
			return false;
		}

		$this->_result = $userData['token_change_safety_data'];

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}