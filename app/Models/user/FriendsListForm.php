<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Models\FileModel;

class FriendsListForm extends BaseForm
{
	public $user_id;

	private $_result;

	protected $validationRules = [
		'user_id' => 'required|integer',
	];

	public function getList()
	{
		$userModel = new UserModel();
		$friendModel = new FriendModel();
		$fileModel = new FileModel();
		$userTableName = $userModel->getTableName();
		$friendTableName = $friendModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$this->_result = $userModel
			->select($userTableName . '.user_id, ' . $userTableName . '.first_name, ' . $userTableName . '.last_name, ' . $fileTableName . '.url as path_to_ava')
			->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id' , 'left')
			->join($friendTableName . ' as friendReciept', 'friendReciept.id_request_recipient = ' . $userTableName . '.user_id', 'left')
			->join($friendTableName . ' as friendSender', 'friendSender.id_request_sender = ' . $userTableName . '.user_id', 'left')
			->groupStart()
				->where('friendReciept.id_request_sender', $this->user_id)
				->where('friendReciept.status', FriendModel::FRIEND_STATUS)
			->groupEnd()
			->orGroupStart()
				->where('friendSender.id_request_recipient', $this->user_id)
				->where('friendSender.status', FriendModel::FRIEND_STATUS)
			->groupEnd()
			->get();

		return true;
	}

	public function getResult()
	{
		$result = [];
		
		foreach ($this->_result->getResultObject() as $item) {
			if (empty($item->path_to_ava)) {
				$item->path_to_ava = FileModel::DEFAULT_AVA_URL;
			}

			$result[] = $item;
		}

		return $result;
	}
}