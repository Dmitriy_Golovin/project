<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\FileModel;
use Common\Models\UserModel;

class LoadAvatarForm extends BaseForm
{
	public $user_id;
	public $avatar;

	private $_result;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'avatar' => 'max_size[avatar,2048]|uploaded[avatar]|is_image[avatar]|ext_in[avatar,png,jpeg,jpg,gif]|mime_in[avatar,image/png,image/jpg,image/jpeg,image/gif]',
	];

	protected $cleanValidationRules = false;

	public function load()
	{
		$avatarDataDisk = FileModel::saveFileOnDisk($this->avatar, 1);
		$fileModel = new FileModel();
		$userModel = new UserModel();
		$fileData = [
			'type' => FileModel::TYPE_IMAGE,
			'related_item' => 'user',
			'item_id' => $this->user_id,
			'url' => $avatarDataDisk['fileName'],
			'privacy_status' => FileModel::PUBLIC_STATUS,
			'mime_type' => $avatarDataDisk['mimeType'],
			'original_name' => $avatarDataDisk['originalName'],
			'preview_height' => $avatarDataDisk['imageData']['previewHeight'],
			'preview_width' => $avatarDataDisk['imageData']['previewWidth'],
		];

		if (!$fileModel->save($fileData)) {
			$this->setError('', $fileModel->errors());
			return false;
		}

		$userModel->where('user_id', $this->user_id)
			->set(['file_id' => $fileModel->getInsertID()]);

		if (!$userModel->update()) {
			$this->setError('', $userModel->errors());
			return false;
		}

		$this->_result['path_to_ava'] = FileModel::preparePathToAva($avatarDataDisk['fileName']);
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}