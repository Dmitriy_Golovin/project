<?php

namespace App\Models\user;

use Common\Forms\BaseForm;
use Common\Models\UserModel;

class ChangeAboutSelfForm extends BaseForm
{
	public $user_id;
	public $about_self;

	private $_result;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'about_self' => 'permit_empty|string|max_length[65535]',
	];

	protected $cleanValidationRules = false;

	public function change()
	{
		$userModel = new UserModel();
		$this->_result['button'] = (!empty($this->about_self))
			? lang('Main.change')
			: lang('Main.write something about yourself...');

		if (!empty($this->about_self)) {
			$this->_result['button'] = lang('Main.change');
			$arrAboutSelf = explode('_razdelitelNegnyi_', $this->about_self);
			$this->about_self = '';

			foreach ($arrAboutSelf as $item) {
				$this->about_self .= '<p class="paragraph_about_self">' . esc($item) . '</p>';
			}
		}

		$userUpdate = $userModel
			->where('user_id', $this->user_id)
			->set(['about_self' => $this->about_self]);

		if (!$userUpdate->update()) {
			$this->setError('', lang('Main.Failed to change the description about yourself'));
			return false;
		}

		$this->_result['fromUserId'] = $this->user_id;
		$this->_result['about_self'] = $this->about_self;
		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}