<?php

namespace App\models\universum;

use Common\Forms\BaseForm;
use Common\Models\ArticleModel;
use Common\Models\ArticleSectionModel;
use Common\Models\FileModel;

class ArticleListForm extends BaseForm
{
	public $article_section_id;
	public $search_string;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 10;

	protected $validationRules = [
		'article_section_id' => 'required|integer',
		'search_string' => 'permit_empty|string',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_itemCount;
	private $_result;
	private $_articleSection;

	public function getList()
	{
		$articleSectionModel = new ArticleSectionModel();
		$this->_articleSection = $articleSectionModel->find($this->article_section_id);

		if (empty($this->_articleSection)) {
			$this->setError('', lang('ArticleSection.Article section with id {0} not found', [$this->article_section_id]));
			return false;
		}

		$articleModel = new ArticleModel();

		$this->_result = $articleModel
			->select(ArticleModel::table() . '.*')
			->select('fileTable.file_id, fileTable.url as fileUrl, fileTable.title as fileTitle, fileTable.source, fileTable.type as fileType')
			->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_ARTICLE . '" and fileTable.item_id = ' . ArticleModel::table() . '.article_id and fileTable.type = ' . FileModel::TYPE_IMAGE, 'left')
			->where(ArticleModel::table() . '.article_section_id', $this->article_section_id)
			->where(ArticleModel::table() . '.status', ArticleModel::STATUS_PUBLISHED);

		if (!empty($this->search_string)) {
			$this->_result->like('(' . ArticleModel::table() . '.title)', $this->search_string);
		}

		$this->_result->groupBy(ArticleModel::table() . '.article_id')
			->orderBy(ArticleModel::table() . '.created_at', 'DESC');

		$this->_itemCount = $this->_result->countAllResults(false);

		$this->_result =$this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $articleModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $articleModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['articleResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['itemCount'] = $this->_itemCount;
		$result['articleSection'] = $this->_articleSection;

		foreach ($this->_result->getResultObject() as $item) {
			$item->fileUrl = (!empty($item->fileUrl))
				? FileModel::preparePathToOpenFile($item->fileUrl, $item->fileType)
				: ArticleModel::DEFAULT_ARTICLE_IMAGE;

			$item->fileTitle = (!empty($item->fileTitle))
				? $item->fileTitle
				: lang('Main.' . ArticleModel::DEFAULT_ARTICLE_IMAGE_TITLE);

			$item->descriptionPreview = ArticleModel::prepareDescriptionForPreview($item->description);

			$result['articleResult'][] = $item;
		}

		return $result;
	}
}