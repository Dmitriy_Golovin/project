<?php

namespace App\Models\search;

use Common\Forms\BaseForm;
use Common\Models\CityTranslationModel;
use Common\Models\CityModel;
use Common\Models\CountryModel;

class GetCityForm extends BaseForm
{
	public $country_id;

	protected $cleanValidationRules = false;

	private $_resultList;

	protected $validationRules = [
		'country_id' => 'required|integer',
	];

	public function getList() {
		$countryModel = new CountryModel();

		if (!$countryModel->isExistCoutryById($this->country_id)) {
			$this->setError('country_id', lang('Country.Country not found'));
			return false;
		}

		$negotiate = \Config\Services::negotiator();
		$language = $negotiate->language(config('app')->supportedLocales);
		$cityModel = new CityModel();
		$cityTranslationModel = new CityTranslationModel();

		$this->_resultList = $cityTranslationModel
			->join($cityModel->getTableName(), $cityModel->getTableName() . '.city_id = ' . $cityTranslationModel->getTableName() . '.city_id')
			->where('country_id', $this->country_id)
			->where('lang_code', $language);

		return true;
	}

	public function getResult() {
		$result = [];

		foreach ($this->_resultList->get()->getResult() as $city) {
			$result[] = $city;
		}

		return $result;
	}
}