<?php

namespace App\Models\search;

use Common\Forms\BaseForm;
use Common\Models\UserModel;
use Common\Models\HolidayModel;
use Common\Models\HolidayDateModel;
use Common\Models\CountryModel;
use Common\Models\CityModel;
use Common\Models\FileModel;
use Common\Models\FriendModel;

class SearchMainForm extends BaseForm
{
	const QUERY_USER = 'f_users';
	const QUERY_HOLIDAY = 'f_holidays';

	public $q;     // keyword
	public $m_v;   // mainСhoice
	public $u_co;  // userCountry
	public $u_ci;  // userCity
	public $h_co;  // holidayCountry
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $user_id;
	public $limit = 20;

	private $_result;

	protected $validationRules = [
		'q' => 'string',
		'm_v' => 'string',
		'u_co' => 'integer',
		'u_ci' => 'integer',
		'h_co' => 'integer',
		'rowNum' => 'integer',
		'timestamp' => 'required|integer',
		'user_id' => 'integer',
	];

	public function search() {
		$userModel = new UserModel();
		$holidayModel = new HolidayModel();

		$this->q = trim($this->q);

		if (empty($this->m_v)) {
			$userCount = null;
			$holidayCount = null;
			if (!empty($this->user_id)) {
				$userCountQuery = $userModel;
				if (!empty($this->q)) {
					$userCountQuery->like('first_name', $this->q)
						->orLike('last_name', $this->q);
				}

				$userCount = $userCountQuery
					->where('veryfication_token', null)
					->where('deleted_at', null)
					->where('is_blocked', UserModel::STATUS_NOT_BLOCKED)
					->countAllResults();
			}

			$holidayCountQuery = $holidayModel;

			if (!empty($this->q)) {
				$holidayCountQuery->like('holiday_day', $this->q)
					->orLike('description', $this->q);
			}

			$holidayCount = $holidayCountQuery->countAllResults();

			$this->_result['template'] = 'main_query';
			$this->_result['insert_type'] = 'renew';

			if (!is_null($userCount)) {
				$this->_result['userCount'] = $userCount;
			}

			if (!is_null($holidayCount)) {
				$this->_result['holidayCount'] = $holidayCount;
			}
		}

		if ($this->m_v === self::QUERY_HOLIDAY) {
			$holidayDateModel = new HolidayDateModel();
			$countryModel = new CountryModel();
			$holidayTableName = $holidayModel->getTableName();
			$holidayDateTableName = $holidayDateModel->getTableName();
			$countryTableName = $countryModel->getTableName();
			$db = db_connect();
			$holidayResult = [];

			$query = $holidayModel
				->join($holidayDateTableName, $holidayTableName . '.holiday_id = ' . $holidayDateTableName . '.holiday_id')
				->join($countryTableName, $holidayTableName . '.country_id = ' . $countryTableName . '.country_id')
				->where($holidayTableName . '.created_at <=', $this->timestamp)
				->select($holidayTableName . '.holiday_id, ' . $holidayTableName . '.holiday_day, ' . $countryTableName . '.code, ' . $holidayDateTableName . '.holiday_date');


			if (!empty($this->h_co)) {
				$query->where($holidayTableName . '.country_id', $this->h_co);
			}

			if (!empty($this->q)) {
				$query->groupStart()
						->like('holiday_day', $this->q)
						->orLike('description', $this->q)
					->groupEnd();
			}

			$holidayQuery = $query->orderBy($holidayTableName . '.holiday_day', 'ASC')
				->getCompiledSelect();

			$holidayCount = $db->query('select holidayCount.* from (' . $holidayQuery . ') as holidayCount')->getNumRows();
			
			$holidayQuery = 'select (@rowNum := @rowNum + 1) as rowNum, holidayQuery.* from (' . $holidayQuery . ') as holidayQuery, (select (@rowNum := 0)) as rowNum';

			if (!empty($this->rowNum)) {
				$holidayQuery = 'select holidayQuery.* from (' . $holidayQuery . ') as holidayQuery where rowNum > ' . $db->escape($this->rowNum);
			}

			$holidayQuery = 'select holidayQuery.* from (' . $holidayQuery . ') as holidayQuery limit ' . $this->limit;

			foreach ($db->query('select holidayQuery.* from (' . $holidayQuery . ') as holidayQuery')->getResultObject() as &$item) {
				$item->holiday_date = $holidayModel::getHolidayDay($item->holiday_date);
				$title = $item->holiday_day;
				$item->link = $item->code . '/' . url_title($title, '+');
				$holidayResult[] = $item;
			}

			$this->_result['template'] = 'holiday_query';
			$this->_result['holidayResult'] = $holidayResult;
			$this->_result['holidayCount'] = $holidayCount;
			$this->_result['timestamp'] = $this->timestamp;
			$this->_result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		}

		if ($this->m_v === self::QUERY_USER) {
			$userModel = new UserModel();
			$fileModel = new FileModel();
			$friendModel = new FriendModel();
			$userTableName = $userModel->getTableName();
			$fileTableName = $fileModel->getTableName();
			$db = db_connect();
			$userResult = [];
			$userIdList = [];

			$query = $userModel
				->select($userTableName . '.user_id, first_name, last_name, url as userAva, concat(' . $userTableName .'.first_name, " ",  ' . $userTableName . '.last_name) as userFullname')
				->join($fileTableName, $userTableName . '.file_id = ' . $fileTableName . '.file_id', 'left')
				->where($userTableName . '.created_at <=', $this->timestamp)
				->where($userTableName . '.deleted_at', null)
				->where($userTableName . '.is_blocked', UserModel::STATUS_NOT_BLOCKED)
				->where($userTableName . '.veryfication_token', null);

			if (!empty($this->u_co)) {
				$query->where($userTableName . '.country_id', $this->u_co);
			}

			if (!empty($this->u_ci)) {
				$query->where($userTableName . '.city_id', $this->u_ci);
			}

			if (!empty($this->q)) {
				$query->groupStart()
						->like('first_name', $this->q)
						->orLike('last_name', $this->q)
						->orLike('concat(' . $userTableName .'.first_name, " ", ' . $userTableName . '.last_name)', $this->q)
					->groupEnd();
			}

			$userQuery = $query->orderBy('userFullname', 'ASC')
				->getCompiledSelect();

			$userCount = $db->query('select userCount.* from (' . $userQuery . ') as userCount')->getNumRows();

			$userQuery = 'select (@rowNum := @rowNum + 1) as rowNum, userQuery.* from (' . $userQuery . ') as userQuery, (select (@rowNum := 0)) as rowNum';

			if (!empty($this->rowNum)) {
				$userQuery = 'select userQuery.* from (' . $userQuery . ') as userQuery where rowNum > ' . $db->escape($this->rowNum);
			}

			$userQuery = 'select userQuery.* from (' . $userQuery . ') as userQuery limit ' . $this->limit;

			foreach ($db->query('select userQuery.* from (' . $userQuery . ') as userQuery')->getResultObject() as &$item) {
				$item->selfProfile = false;
				$item->userAva = empty($item->userAva)
					? FileModel::DEFAULT_AVA_URL
					: FileModel::preparePathToAva($item->userAva);

				if (!empty($this->user_id) && (int)$item->user_id === (int)$this->user_id) {
					$item->selfProfile = true;
				}

				$userResult[] = $item;
				$userIdList[] = $item->user_id;
			}

			$this->_result['friendLinks'] = $friendModel->getUserRelationships($userIdList, $this->user_id);
			$this->_result['selfUserId'] = $this->user_id;
			$this->_result['template'] = 'user_query';
			$this->_result['userResult'] = $userResult;
			$this->_result['userCount'] = $userCount;
			$this->_result['timestamp'] = $this->timestamp;
			$this->_result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		}

		return true;
	}

	public function getResult()
	{
		return $this->_result;
	}
}