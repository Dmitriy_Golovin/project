<?php

namespace App\Models\comment;

use Common\Forms\BaseForm;
use Common\Models\CommentModel;

class CommentListForm extends BaseForm
{
	public $type;
	public $entity_id;
	public $timestamp;
	public $timeOffsetSeconds;
	public $last_id;
	public $existCount;
	public $statusUser;

	protected $validationRules = [
		'type' => 'required|integer|in_list[1,2,3]',
		'entity_id' => 'required|integer|',
		'timestamp' => 'required|integer|',
		'timeOffsetSeconds' => 'required|integer|',
		'last_id' => 'required|integer|',
		'existCount' => 'required|integer',
		'statusUser' => 'required|string|in_list[guest,user]',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getList()
	{
		$db = db_connect();
		$entity = $db->table(CommentModel::getTableByType()[$this->type])
			->where(CommentModel::getPrimaryKeyByType()[$this->type], $this->entity_id)
			->get()
			->getResultObject();

		if (empty($entity)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$guest = ($this->statusUser === 'guest') ? true : false;

		$commentModel = new CommentModel();
		$this->_result = $commentModel->getCommentData($this->type, $this->entity_id, $this->timeOffsetSeconds, $this->last_id, $this->timestamp, $this->existCount, $guest);

		return true;
	}

	public function getResult()
	{
		return ['commentData' => $this->_result];
	}
}