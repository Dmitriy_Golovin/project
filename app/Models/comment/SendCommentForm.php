<?php

namespace App\Models\comment;

use Common\Forms\BaseForm;
use Common\Models\CommentModel;
use Common\Models\ReportModel;
use Common\Models\FileModel;
use Common\Models\UserModel;
use App\Libraries\DateAndTime\DateAndTime;

class SendCommentForm extends BaseForm
{
	public $user_id;
	public $type;
	public $entity_id;
	public $text;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2,3]',
		'entity_id' => 'required|integer',
		'text' => 'required|string',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function send()
	{
		$entityModel = CommentModel::getModelByType()[$this->type];
		$entityPrimaryKey = CommentModel::getPrimaryKeyByType()[$this->type];
		$entity = $entityModel
			->where($entityPrimaryKey, $this->entity_id)
			->first();

		if (empty($entity)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$commentData = [
			'related_item' => CommentModel::getTableByType()[$this->type],
			'item_id' => $this->entity_id,
			'user_id' => $this->user_id,
			'text' => $this->text,
		];

		$commentModel = new CommentModel();

		if (!$commentModel->save($commentData)) {
			$this->setError('', lang('Comment.Failed to save comment'));
			return false;
		}

		$commentId = $commentModel->getInsertID();

		$this->_result = $commentModel
			->select(CommentModel::table() . '.*')
			->select('concat(userTable.first_name, " ", userTable.last_name) as userFullname')
			->select('fileTable.url as userAva')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . CommentModel::table() . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where('comment_id', $commentId)
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Comment.Comment not found'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$this->_result->date = DateAndTime::getDateTime($this->_result->created_at, $this->timeOffsetSeconds, 'genetive');
        $this->_result->userAva = !empty($this->_result->userAva) ? FileModel::preparePathToAva($this->_result->userAva) : FileModel::DEFAULT_AVA_URL;

		return ['item' => $this->_result];
	}
}