<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class TaskDetailsForm extends BaseForm
{
	const DETAILS_TYPE_ASSIGNED = 1;
	const DETAILS_TYPE_OWNER = 2;

	public $user_id;
	public $entity_id;
	public $detailsType;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'detailsType' => 'required|integer|in_list[1,2]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	protected $startTime;

	private $_result;
	private $_canEditReportData = ['state' => false, 'url' => ''];
	private $_canEditAcceptTask = ['state' => false, 'url' => ''];
	private $_canEditRejectTask = ['state' => false, 'url' => ''];

	public function getDetails()
	{
		$this->startTime = microtime(true);
		$taskModel = new TaskModel();
		$taskHistoryModel = new TaskHistoryModel();
		$whereUserIdField = ((int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED)
			? 'assigned_user_id'
			: 'owner_user_id';
		$joinUserIdField = ((int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED)
			? 'owner_user_id'
			: 'assigned_user_id';
		$this->_result = $taskModel
			->select(TaskModel::table() . '.*')
			->select('userTable.first_name, userTable.last_name, userTable.deleted_at as userDeleted')
			->select('fileTable.url as userAva')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.' . $joinUserIdField, 'left')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where($whereUserIdField, $this->user_id)
			->where(TaskModel::table() . '.task_id', $this->entity_id)
			->groupBy(TaskModel::table() . '.task_id')
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$endEntityDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$taskHistoryModel = new TaskHistoryModel();
		$taskHistoryFileModel = new TaskHistoryFileModel();
		$historyQuery = $taskHistoryModel
			->select(TaskHistoryModel::table() . '.*')
			->select('userTable.first_name, userTable.last_name, userTable.deleted_at as userDeleted')
			->select('fileTable.url as userAva')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskHistoryModel::table() . '.user_id', 'left')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where(TaskHistoryModel::table() . '.task_id', $this->_result->task_id)
			->orderBy(TaskHistoryModel::table() . '.created_at', 'DESC')
			->get();

		foreach ($historyQuery->getResultObject() as $item) {
			$item->typeLabel = TaskHistoryModel::getStatusLabels()[(int)$item->type];
			$item->date = DateAndTime::getDateTime($item->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
			$item->scenario = 'create';

			if ((int)$item->is_main === TaskHistoryModel::IS_NOT_MAIN) {
				$item->fileList = $taskHistoryFileModel->getFileList($item->task_history_id, $this->user_id);
			} else {
				$this->_result->fileList = $taskHistoryFileModel->getFileList($item->task_history_id, $this->user_id);
				$this->_result->main_task_history_id = $item->task_history_id;
			}

			if (!empty($item->user_id)) {
				$item->userAva = (!empty($item->userAva))
					? FileModel::preparePathToAva($item->userAva)
					: FileModel::DEFAULT_AVA_URL;

				if (!is_null($item->userDeleted)) {
					$item->first_name = lang('Main.Profile');
					$item->last_name = lang('Main.deleted');
					$item->userAva = FileModel::DEFAULT_AVA_URL;
				}
			}

			if ((int)$item->type === TaskHistoryModel::TYPE_COMPLETE
				&& (int)$this->_result->review_status === TaskModel::STATUS_NOT_REVIEWED
				&& (int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED
				&& (int)$this->_result->assigned_user_id === (int)$this->user_id
				&& !$this->_canEditReportData['state']) {
				$this->_canEditReportData = [
					'state' => true,
					'url' => '/task/complete-edit/' . $this->_result->task_id . '/' . $item->task_history_id
				];
			}

			if ((int)$item->type === TaskHistoryModel::TYPE_ACCEPT
				&& $this->_result->owner_user_id === $this->user_id
				&& (int)$this->detailsType === self::DETAILS_TYPE_OWNER
				&& (int)$this->_result->status === TaskModel::STATUS_COMPLETED
				&& (int)$this->_result->review_status === TaskModel::STATUS_ACCEPTED
				&& ($endEntityDate + DAY) > time()
				&& !$this->_canEditAcceptTask['state']) {
				$this->_canEditAcceptTask = [
					'state' => true,
					'url' => '/task/accept-edit/' . $this->_result->task_id . '/' . $item->task_history_id
				];
			}

			if ((int)$item->type === TaskHistoryModel::TYPE_REJECT
				&& $this->_result->owner_user_id === $this->user_id
				&& (int)$this->detailsType === self::DETAILS_TYPE_OWNER
				&& (int)$this->_result->review_status === TaskModel::STATUS_DECLINED
				&& !$this->_canEditRejectTask['state']) {
				$this->_canEditRejectTask = [
					'state' => true,
					'url' => '/task/reject-edit/' . $this->_result->task_id . '/' . $item->task_history_id
				];
			}

			$this->_result->history[] = $item;
		}

		if (empty($this->_result->main_task_history_id) && !empty($this->_result->source_id)) {
			$mainTaskHistory = $taskHistoryModel
				->where(TaskHistoryModel::table() . '.task_id', $this->_result->source_id)
				->first();

			$this->_result->fileList = $taskHistoryFileModel->getFileList($mainTaskHistory->task_history_id, $this->user_id);
			$this->_result->main_task_history_id = $mainTaskHistory->task_history_id;
		}

		return true;
	}

	public function getResult()
	{
		$periodicityList = config('MainEventParamsConfig')->periodicity;
		$reminderList = config('MainEventParamsConfig')->duration;
		$startEntityDate = (int)$this->_result->date + (int)$this->_result->time;
		$endEntityDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$startDay = DateAndTime::getTimestampStartDay(time()) - $this->timeOffsetSeconds;
		$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;
		$this->_result->detailsType = $this->detailsType;
		$this->_result->canEditReport = $this->_canEditReportData;
		$this->_result->canEditTask = false;
		$this->_result->canCompleteTask = false;
		$this->_result->canDeleteTask = false;
		$this->_result->canAcceptTask = false;
		$this->_result->canRejectTask = false;
		$this->_result->canEditAcceptTask = $this->_canEditAcceptTask;
		$this->_result->canEditRejectTask = $this->_canEditRejectTask;

		$this->_result->userAva = (!empty($this->_result->userAva))
			? FileModel::preparePathToAva($this->_result->userAva)
			: FileModel::DEFAULT_AVA_URL;

		if (!is_null($this->_result->userDeleted) || empty($this->_result->assigned_user_id)) {
			$this->_result->assignedUserDeleted = true;
		} else {
			$this->_result->assignedUserDeleted = false;
		}

		if ((int)$this->_result->status === TaskModel::STATUS_COMPLETED
			&& (int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED) {
			$this->_result->entityStatus = COMPLETED;
			$this->_result->backHrefList = 'completed';
		} else if ((($startEntityDate <= time() && $endEntityDate >= time())
			|| ($startEntityDate >= time() && $startEntityDate <= $endDay))
			&& (int)$this->_result->status === TaskModel::STATUS_NOT_COMPLETED
			&& (int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED) {
			$this->_result->entityStatus = CURRENT;
			$this->_result->backHrefList = '';
		} else if ($startEntityDate > $endDay && (int)$this->_result->status === TaskModel::STATUS_NOT_COMPLETED
			&& (int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED) {
			$this->_result->entityStatus = SCHEDULED;
			$this->_result->backHrefList = 'scheduled';
		} else if ((($endEntityDate <= time() && (int)$this->_result->status !== TaskModel::STATUS_COMPLETED)
			|| (int)$this->_result->status === TaskModel::STATUS_OVERDUE)
			&& (int)$this->detailsType === self::DETAILS_TYPE_ASSIGNED) {
			$this->_result->entityStatus = OVERDUE;
			$this->_result->backHrefList = 'overdue';
		} else if ((int)$this->detailsType === self::DETAILS_TYPE_OWNER) {
			$this->_result->entityStatus = CREATED;
			$this->_result->backHrefList = 'created';
		}

		if ($this->_result->owner_user_id === $this->user_id
			&& (int)$this->detailsType === self::DETAILS_TYPE_OWNER
			&& (int)$this->_result->status !== TaskModel::STATUS_COMPLETED) {
			$this->_result->canEditTask = true;
		}

		if ($this->_result->owner_user_id === $this->user_id
			&& (int)$this->detailsType === self::DETAILS_TYPE_OWNER
			&& (int)$this->_result->status === TaskModel::STATUS_COMPLETED
			&& (int)$this->_result->review_status !== TaskModel::STATUS_ACCEPTED) {
			$this->_result->canAcceptTask = true;
		}

		if ($this->_result->owner_user_id === $this->user_id
			&& (int)$this->detailsType === self::DETAILS_TYPE_OWNER
			&& (int)$this->_result->status === TaskModel::STATUS_COMPLETED
			&& ((int)$this->_result->review_status === TaskModel::STATUS_NOT_REVIEWED
				|| ((int)$this->_result->review_status === TaskModel::STATUS_ACCEPTED
				&& ($endEntityDate + DAY) > time()))) {
			$this->_result->canRejectTask = true;
		}

		if (in_array($this->_result->entityStatus, [CURRENT, SCHEDULED, OVERDUE])
			&& $this->_result->assigned_user_id === $this->user_id) {
			$this->_result->canCompleteTask = true;
		}

		if ($this->_result->owner_user_id === $this->user_id
			&& (int)$this->detailsType === self::DETAILS_TYPE_OWNER
			&& (int)$this->_result->review_status !== TaskModel::STATUS_ACCEPTED) {
			$this->_result->canDeleteTask = true;
		}

		if ((int)$this->_result->status === TaskModel::STATUS_NOT_COMPLETED) {
			$this->_result->statusCssClass = ' entity_status_scheduled';
			$this->_result->statusValue = lang('Task.not completed');
		} else if ((int)$this->_result->status === TaskModel::STATUS_COMPLETED) {
			$this->_result->statusCssClass = ' entity_status_completed';
			$this->_result->statusValue = lang('Task.completed');
		} else if ((int)$this->_result->status === TaskModel::STATUS_OVERDUE) {
			$this->_result->statusCssClass = ' entity_status_overdue';
			$this->_result->statusValue = lang('Task.overdue');
		} else if ((int)$this->_result->status === TaskModel::STATUS_NO_EXECUTOR) {
			$this->_result->statusCssClass = ' entity_status_overdue';
			$this->_result->statusValue = lang('Task.no task executor');
		}

		if (!empty($this->_result->reminder)) {
			$this->_result->reminderValue = lang('TimeAndDate.remind {0} before', [lang('TimeAndDate.' . $reminderList[$this->_result->reminder])]);
		} else {
			$this->_result->reminderValue = lang('Event.without reminder');
		}

		$this->_result->date = DateAndTime::getDateTime($startEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->endDate = DateAndTime::getDateTime($endEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->userDataLabel = ((int)$this->detailsType === self::DETAILS_TYPE_OWNER)
			? lang('Task.executor')
			: lang('Task.task created by');
		$this->_result->privacyValue = ((int)$this->_result->privacy === 1) ? lang('Event.private') : lang('Event.public');
		$this->_result->periodicityValue = lang('TimeAndDate.' . $periodicityList[$this->_result->periodicity]);

		return [
			'entity' => $this->_result,
			'titleNotice' => lang('Task.Are you sure you want to delete the task?'),
		];
	}
}