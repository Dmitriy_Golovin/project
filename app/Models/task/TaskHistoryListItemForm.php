<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\FileModel;
use Common\Models\UserModel;
use App\Libraries\DateAndTime\DateAndTime;

class TaskHistoryListItemForm extends BaseForm
{
	const TYPE_OWNER = 'owner';
	const TYPE_ASSIGNED = 'assigned';

	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	public $user_id;
	public $task_id;
	public $task_history_id;
	public $scenario;
	public $type;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'task_id' => 'required|integer',
		'task_history_id' => 'required|integer',
		'scenario' => 'required|string',
		'type' => 'required|string',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getItem()
	{
		$taskUserField = ($this->type === self::TYPE_OWNER)
			? 'owner_user_id'
			: 'assigned_user_id';
		$taskHistoryModel = new TaskHistoryModel();
		$this->_result['item'] = $taskHistoryModel
			->select(TaskHistoryModel::table() . '.*')
			->select('userTable.first_name, userTable.last_name')
			->select('fileTable.url as userAva')
			->select('taskTable.status, taskTable.review_status, taskTable.assigned_user_id, taskTable.owner_user_id, taskTable.is_cloned, (taskTable.date + taskTable.time + taskTable.duration) as endEntityDate')
			->join(TaskModel::table() . ' as taskTable', 'taskTable.task_id = ' . TaskHistoryModel::table() . '.task_id')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskHistoryModel::table() . '.user_id', 'left')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where('taskTable.' . $taskUserField, $this->user_id)
			->where(TaskHistoryModel::table() . '.task_history_id', $this->task_history_id)
			->first();

		if (empty($this->_result['item'])) {
			$this->setError('', lang('TaskHistory.Task history not found'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$this->_result['item']->typeLabel = TaskHistoryModel::getStatusLabels()[(int)$this->_result['item']->type];
		$this->_result['item']->date = DateAndTime::getDateTime($this->_result['item']->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result['item']->scenario = $this->scenario;

		if ((int)$this->_result['item']->is_main === TaskHistoryModel::IS_NOT_MAIN) {
			$taskHistoryFileModel = new TaskHistoryFileModel();
			$this->_result['item']->fileList = $taskHistoryFileModel->getFileList($this->_result['item']->task_history_id, $this->user_id);
		}

		if (!empty($this->_result['item']->user_id)) {
			$this->_result['item']->userAva = (!empty($this->_result['item']->userAva))
				? FileModel::preparePathToAva($this->_result['item']->userAva)
				: FileModel::DEFAULT_AVA_URL;
		}

		$this->_result['entity'] = (object)[];
		$this->_result['entity']->task_id = $this->_result['item']->task_id;
		$this->_result['entity']->canEditReport = ['state' => false, 'url' => ''];
		$this->_result['entity']->canEditTask = false;
		$this->_result['entity']->canCompleteTask = false;
		$this->_result['entity']->canDeleteTask = false;
		$this->_result['entity']->canAcceptTask = false;
		$this->_result['entity']->canRejectTask = false;
		$this->_result['entity']->canEditAcceptTask = ['state' => false, 'url' => ''];
		$this->_result['entity']->canEditRejectTask = ['state' => false, 'url' => ''];

		if ((int)$this->_result['item']->type === TaskHistoryModel::TYPE_COMPLETE
			&& (int)$this->_result['item']->review_status === TaskModel::STATUS_NOT_REVIEWED
			&& $this->type === self::TYPE_ASSIGNED
			&& (int)$this->_result['item']->assigned_user_id === (int)$this->user_id) {

			$this->_result['entity']->canEditReport = [
				'state' => true,
				'url' => '/task/complete-edit/' . $this->_result['item']->task_id . '/' . $this->_result['item']->task_history_id
			];
		}

		if ((int)$this->_result['item']->owner_user_id === (int)$this->user_id
			&& $this->type === self::TYPE_OWNER
			&& (int)$this->_result['item']->status !== TaskModel::STATUS_COMPLETED) {
			$this->_result['entity']->canEditTask = true;
		}

		if ((int)$this->_result['item']->status !== TaskModel::STATUS_COMPLETED
			&& $this->type === self::TYPE_ASSIGNED
			&& (int)$this->_result['item']->assigned_user_id === (int)$this->user_id) {
			$this->_result['entity']->canCompleteTask = true;
		}

		if ((int)$this->_result['item']->owner_user_id === (int)$this->user_id
			&& $this->type === self::TYPE_OWNER
			&& (int)$this->_result['item']->is_cloned === TaskModel::STATUS_NOT_CLONED
			&& (int)$this->_result['item']->review_status !== TaskModel::STATUS_ACCEPTED) {
			$this->_result['entity']->canDeleteTask = true;
		}

		if ((int)$this->_result['item']->owner_user_id === (int)$this->user_id
			&& $this->type === self::TYPE_OWNER
			&& (int)$this->_result['item']->status === TaskModel::STATUS_COMPLETED
			&& (int)$this->_result['item']->review_status !== TaskModel::STATUS_ACCEPTED) {
			$this->_result['entity']->canAcceptTask = true;
		}

		if ((int)$this->_result['item']->type === TaskHistoryModel::TYPE_ACCEPT
			&& (int)$this->_result['item']->owner_user_id === (int)$this->user_id
			&& $this->type === self::TYPE_OWNER
			&& (int)$this->_result['item']->status === TaskModel::STATUS_COMPLETED
			&& (int)$this->_result['item']->review_status === TaskModel::STATUS_ACCEPTED
			&& ($this->_result['item']->endEntityDate + DAY) > time()) {
			
			$this->_result['entity']->canEditAcceptTask = [
				'state' => true,
				'url' => '/task/accept-edit/' . $this->_result['item']->task_id . '/' . $this->_result['item']->task_history_id
			];
		}

		if ((int)$this->_result['item']->owner_user_id === (int)$this->user_id
			&& $this->type === self::TYPE_OWNER
			&& (int)$this->_result['item']->status === TaskModel::STATUS_COMPLETED
			&& ((int)$this->_result['item']->review_status === TaskModel::STATUS_NOT_REVIEWED
				|| ((int)$this->_result['item']->review_status === TaskModel::STATUS_ACCEPTED
				&& ($this->_result['item']->endEntityDate + DAY) > time()))) {
			$this->_result['entity']->canRejectTask = true;
		}

		if ((int)$this->_result['item']->owner_user_id === (int)$this->user_id
			&& $this->type === self::TYPE_OWNER
			&& (int)$this->_result['item']->review_status === TaskModel::STATUS_DECLINED) {

			$this->_result['entity']->canEditAcceptTask = [
				'state' => true,
				'url' => '/task/reject-edit/' . $this->_result['item']->task_id . '/' . $this->_result['item']->task_history_id
			];
		}

		$this->_result['statusDetailsData']['statusCssClass'] = TaskModel::getStatusDetailsCssClass()[$this->_result['item']->status];
		$this->_result['statusDetailsData']['statusValue'] = TaskModel::getStatusLabel()[$this->_result['item']->status];
		$this->_result['statusDetailsData']['reviewStatusCssClass'] = TaskModel::getReviewStatusCssClass()[$this->_result['item']->review_status];
		$this->_result['statusDetailsData']['reviewStatusValue'] = TaskModel::getReviewStatusLabel()[$this->_result['item']->review_status];

		return $this->_result;
	}
}