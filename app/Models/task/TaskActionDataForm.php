<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class TaskActionDataForm extends BaseForm
{
	const TYPE_CREATE = 'create';
	const TYPE_EDIT = 'edit';

	public $user_id;
	public $task_id;
	public $task_history_id;
	public $type;
	public $action;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'task_id' => 'required|integer',
		'task_history_id' => 'permit_empty|integer',
		'type' => 'required|string|in_list[create,edit]',
		'action' => 'required|string|in_list[complete,accept,reject]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getData()
	{
		$taskModel = new TaskModel();

		if ($this->action === TaskHistoryModel::ACTION_TYPE_COMPLETE) {
			$this->_result = $taskModel
				->where('assigned_user_id', $this->user_id)
				->where('task_id', $this->task_id);

			if ($this->type === self::TYPE_CREATE) {
				$this->_result->where('status !=', TaskModel::STATUS_COMPLETED)
					->where('review_status !=', TaskModel::STATUS_ACCEPTED);
			}

			if ($this->type === self::TYPE_EDIT) {
				$this->_result->where('status', TaskModel::STATUS_COMPLETED)
					->where('review_status', TaskModel::STATUS_NOT_REVIEWED);
			}

			$this->_result = $this->_result->first();
		}

		if ($this->action === TaskHistoryModel::ACTION_TYPE_ACCEPT) {
			$this->_result = $taskModel
				->where('task_id', $this->task_id)
				->where('owner_user_id', $this->user_id);

			if ($this->type === self::TYPE_CREATE) {
				$this->_result->where('status', TaskModel::STATUS_COMPLETED)
					->where('review_status !=', TaskModel::STATUS_ACCEPTED);
			}

			if ($this->type === self::TYPE_EDIT) {
				$this->_result->where('status', TaskModel::STATUS_COMPLETED)
					->where('review_status', TaskModel::STATUS_ACCEPTED);
			}
			

			$this->_result = $this->_result->first();
		}

		if ($this->action === TaskHistoryModel::ACTION_TYPE_REJECT) {
			$this->_result = $taskModel
				->where('task_id', $this->task_id)
				->where('owner_user_id', $this->user_id);

			if ($this->type === self::TYPE_CREATE) {
				$this->_result->where('status', TaskModel::STATUS_COMPLETED)
					->where('review_status !=', TaskModel::STATUS_DECLINED);
			}

			if ($this->type === self::TYPE_EDIT) {
				$this->_result->where('review_status', TaskModel::STATUS_DECLINED);
			}

			$this->_result = $this->_result->first();
		}

		if (empty($this->_result)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		if ($this->type === self::TYPE_EDIT) {
			$taskHistoryModel = new TaskHistoryModel();
			$taskHistory = $taskHistoryModel
				->where('task_history_id', $this->task_history_id)
				->where('task_id', $this->task_id)
				->first();

			if (empty($taskHistory)) {
				$this->setError('', lang('TaskHistory.Task history not found'));
				return false;
			}

			$taskHistoryFileModel = new TaskHistoryFileModel();
			$taskHistory->fileList = $taskHistoryFileModel->getFileList($taskHistory->task_history_id, $this->user_id);
			$this->_result->taskHistory = $taskHistory;
		}

		return true;
	}

	public function getResult()
	{
		if (empty($this->_result->name)) {
			$this->_result->name = lang('Main.No title');
		}

		$startEntityDate = (int)$this->_result->date + (int)$this->_result->time;
		$endEntityDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$this->_result->date = DateAndTime::getDateTime($startEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->endDate = DateAndTime::getDateTime($endEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');

		return [
			'entity' => $this->_result,
			'type' => $this->type,
			'action' => $this->action,
			'formAction' => ($this->type === self::TYPE_EDIT)
				? '/task/' . $this->action . '-edit/' . $this->task_id . '/' . $this->task_history_id
				: '/task/' . $this->action . '/' . $this->task_id,
			'contentTitle' => TaskHistoryModel::getActionContentTitle()[$this->action][$this->type],
		];
	}
}