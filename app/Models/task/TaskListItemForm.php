<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class TaskListItemForm extends BaseForm
{
	const SUBTYPE_CURRENT = 'current';
	const SUBTYPE_SCHEDULED = 'scheduled';
	const SUBTYPE_COMPLETED = 'completed';
	const SUBTYPE_OVERDUE = 'overdue';
	const SUBTYPE_CREATED = 'created';

	public $user_id;
	public $task_id;
	public $subType;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'task_id' => 'required|integer',
		'subType' => 'required|string|in_list[current,scheduled,completed,overdue,created]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getItem()
	{
		$taskModel = new TaskModel();
		$whereUserIdField = ($this->subType !== self::SUBTYPE_CREATED)
			? 'assigned_user_id'
			: 'owner_user_id';
		$joinUserIdField = ($this->subType !== self::SUBTYPE_CREATED)
			? 'owner_user_id'
			: 'assigned_user_id';

		$this->_result = $taskModel
			->select(TaskModel::table() . '.*')
			->select('userTable.first_name, userTable.last_name')
			->select('fileTable.url as userAva');

		if ($this->subType !== self::SUBTYPE_CREATED) {
			$this->_result->select('(select if(is_viewed_assigned = ' . TaskModel::STATUS_NOT_VIEW_ASSIGNED . ', 1, 0)) as notViewed');
		} else {
			$this->_result->select('(select if(review_status = ' . TaskModel::STATUS_NOT_REVIEWED . ' and status = ' . TaskModel::STATUS_COMPLETED . ', 1, 0)) as notViewed');
		}

		$this->_result = $this->_result->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.' . $joinUserIdField)
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where('task_id', $this->task_id)
			->where($whereUserIdField, $this->user_id)
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$periodicityList = config('MainEventParamsConfig')->periodicity;
		$reminderList = config('MainEventParamsConfig')->reminder;

		if ($this->subType === self::SUBTYPE_CREATED) {
			$this->_result->ownerLabel = lang('Task.executor');
		} else {
			$this->_result->ownerLabel = lang('Task.task created by');
		}

		$this->_result->detailsType = ($this->subType === self::SUBTYPE_CREATED)
			? 'owner'
			: 'assigned';
		$this->_result->userAva = (!empty($this->_result->userAva))
			? FileModel::preparePathToAva($this->_result->userAva)
			: FileModel::DEFAULT_AVA_URL;

		$endEventTimestamp = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$this->_result->dateTimestamp = (int)$this->_result->date + (int)$this->_result->time;
		$this->_result->endEventTimestamp = $endEventTimestamp;
		$this->_result->date = DateAndTime::getDateTime(((int)$this->_result->date + (int)$this->_result->time), $this->timeOffsetSeconds, 'genetive');
		$this->_result->endDate = DateAndTime::getDateTime($endEventTimestamp, $this->timeOffsetSeconds, 'genetive');
		$this->_result->privacyValue = ((int)$this->_result->privacy === 1) ? lang('Event.private') : lang('Event.public');
		$this->_result->periodicityValue = lang('TimeAndDate.' . $periodicityList[$this->_result->periodicity]);

		if (!empty($this->_result->reminder)) {
			$this->_result->reminderValue = lang('TimeAndDate.remind {0} before', [lang('TimeAndDate.' . $reminderList[$this->_result->reminder])]);
		} else {
			$this->_result->reminderValue = lang('Event.without reminder');
		}

		if (empty($this->_result->name)) {
			$this->_result->name = lang('Main.No title');
		}

		return [
			'item' => $this->_result,
			'subType' => $this->subType,
		];
	}
}