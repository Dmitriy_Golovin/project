<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use CodeIgniter\Database\BaseBuilder;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class TaskHistoryForm extends BaseForm
{
	public $user_id;
	public $source_id;
	public $task_id;
	public $type;
	public $dateFrom;
	public $dateTo;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $timeOffsetSeconds;
	public $limit = 30;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'source_id' => 'permit_empty|integer',
		'task_id' => 'required|integer',
		'type' => 'permit_empty|integer|in_list[1,2,3,4,5,6,7]',
		'dateFrom' => 'permit_empty|valid_date[d-m-Y]',
		'dateTo' => 'permit_empty|valid_date[d-m-Y]',
		'timeOffsetSeconds' => 'required|integer',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_itemCount;
	private $_result;

	public function getList()
	{
		$taskHistoryModel = new TaskHistoryModel();
		$queryId = (empty($this->source_id))
			? $this->task_id
			: $this->source_id;
		$this->_result = $taskHistoryModel
			->select(TaskHistoryModel::table() . '.*')
			->select('userTable.first_name, userTable.last_name, userTable.deleted_at as userDeleted')
			->select('fileTable.url as userAva')
			->join(TaskModel::table() . ' as taskTable', 'taskTable.task_id = ' . TaskHistoryModel::table() . '.task_id')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskHistoryModel::table() . '.user_id', 'left')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->groupStart()
				->groupStart()
					->where('taskTable.task_id', $queryId)
					->groupStart()
						->where('assigned_user_id', $this->user_id)
						->orWhere('owner_user_id', $this->user_id)
					->groupEnd()
				->groupEnd()
				->orGroupStart()
					->where('taskTable.source_id', $queryId)
					->groupStart()
						->where('assigned_user_id', $this->user_id)
						->orWhere('owner_user_id', $this->user_id)
					->groupEnd()
				->groupEnd()
			->groupEnd();

		if (!is_null($this->type)) {
			$this->_result->where(TaskHistoryModel::table() . '.type', $this->type);
		}

		if (!empty($this->dateFrom)) {
			$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
			$this->_result->where(TaskHistoryModel::table() . '.created_at >=', $this->dateFrom);
		}

		if (!empty($this->dateTo)) {
			$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
			$this->_result->where(TaskHistoryModel::table() . '.created_at <=', $this->dateTo);
		}

		$this->_result->groupBy('task_history_id')
			->orderBy(TaskHistoryModel::table() . '.created_at', 'DESC');
			
		$this->_itemCount = $this->_result->countAllResults(false);

		$this->_result =$this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $taskHistoryModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $taskHistoryModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['taskHistoryResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['itemCount'] = $this->_itemCount;
		$taskHistoryFileModel = new TaskHistoryFileModel();

		foreach ($this->_result->getResultObject() as $item) {
			$item->typeLabel = TaskHistoryModel::getStatusLabels()[(int)$item->type];
			$item->date = DateAndTime::getDateTime($item->created_at, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
			$item->scenario = 'create';

			if ((int)$item->is_main === TaskHistoryModel::IS_NOT_MAIN) {
				$item->fileList = $taskHistoryFileModel->getFileList($item->task_history_id, $this->user_id);
			}

			if (!empty($item->user_id)) {
				$item->userAva = (!empty($item->userAva))
					? FileModel::preparePathToAva($item->userAva)
					: FileModel::DEFAULT_AVA_URL;

				if (!is_null($item->userDeleted)) {
					$item->first_name = lang('Main.Profile');
					$item->last_name = lang('Main.deleted');
					$item->userAva = FileModel::DEFAULT_AVA_URL;
				}
			}

			$result['taskHistoryResult'][] = $item;
		}

		return $result;
	}
}