<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\FileModel;
use Common\Models\UserModel;
use Common\Components\WsClientComponent;
use Common\Components\notification\NotificationComponent;
use App\Libraries\DateAndTime\DateAndTime;

class TaskActionSaveForm extends BaseForm
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	public $user_id;
	public $action;
	public $task_history_video;
	public $task_history_photo;
	public $task_history_document;
	public $note;
	public $task_history_id;
	public $task;
	public $deleteFileIdList;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'action' => 'required|string[complete,accept,reject]',
		'task_history_video' => 'permit_empty|max_size[task_history_video,20000]|ext_in[task_history_video,mp4]|mime_in[task_history_video,video/mp4]',
		'task_history_photo.*' => 'permit_empty|uploaded[task_history_photo]|is_image[task_history_photo]|ext_in[task_history_photo,png,jpeg,jpg,gif]|mime_in[task_history_photo,image/png,image/jpg,image/jpeg,image/gif]',
		'task_history_document.*' => 'permit_empty|max_size[task_history_document,5000]|uploaded[task_history_document]|ext_in[task_history_document,txt,doc,docx,pdf,ppt,pptx,csv,csv1,xls,xlsx,zip,rar]',
		'note' => 'permit_empty|string|max_length[65000]',
		'task_history_id' => 'permit_empty|integer',
		'task' => 'required',
		'deleteFileIdList' => 'permit_empty',
	];

	protected $validationMessages = [
        'task' => [
            'required' => 'Task.The task cannot be with empty data',
        ],
    ];

	protected $cleanValidationRules = false;

	private $_taskHistoryData;
	private $_taskHistoryFileModel;
	private $_existFileList = [
		'videoList' => [],
		'imageList' => [],
		'documentList' => [],
	];

	public function save()
	{
		$taskHistoryModel = new TaskHistoryModel();
		$taskModel = new TaskModel();
		$fileModel = new FileModel();
		$this->_taskHistoryFileModel = new TaskHistoryFileModel();

		if ($this->scenario === self::SCENARIO_CREATE) {
			// if ($this->action === TaskHistoryModel::ACTION_TYPE_COMPLETE) {
			// 	$existTaskHistory = $taskHistoryModel
			// 		->where('task_id', $this->task->task_id)
			// 		->where('user_id', $this->user_id)
			// 		->where('type', TaskHistoryModel::getUserActionType()[$this->action])
			// 		->first();
			// }

			// if (!empty($existTaskHistory)) {
			// 	$this->setError('', lang('TaskHistory.Task history already exists'));
			// 	return false;
			// }

			$this->_taskHistoryData = (object)[
				'task_id' => $this->task->task_id,
				'note' => $this->note,
				'type' => TaskHistoryModel::getUserActionType()[$this->action],
				'user_id' => $this->user_id,
			];

			$taskHistoryModel->db->transBegin();

			if (!$taskHistoryModel->save($this->_taskHistoryData)) {
				$this->setError('', lang('TaskHistory.Failed to save task history'));
				$taskHistoryModel->db->transRollback();
				return false;
			}

			if ($this->action === 'complete') {
				$this->task->status = TaskModel::STATUS_COMPLETED;
				$this->task->review_status = TaskModel::STATUS_NOT_REVIEWED;
			} else if ($this->action === 'accept') {
				$this->task->review_status = TaskModel::STATUS_ACCEPTED;
			} else if ($this->action === 'reject') {
				if ((int)$this->task->date + (int)$this->task->time + (int)$this->task->duration > time()) {
					$this->task->status = TaskModel::STATUS_NOT_COMPLETED;
				} else {
					$this->task->status = TaskModel::STATUS_OVERDUE;
					$taskHistoryData = [
						'task_id' => $this->task->task_id,
						'type' => TaskHistoryModel::TYPE_OVERDUE,
					];

					if (!$taskHistoryModel->save($taskHistoryData)) {
						$this->setError('', lang('TaskHistory.Failed to save task history'));
						$taskHistoryModel->db->transRollback();
						return false;
					}
				}

				$this->task->review_status = TaskModel::STATUS_DECLINED;
				$this->task->is_viewed_assigned = TaskModel::STATUS_NOT_VIEW_ASSIGNED;
			}

			if (!$taskModel->save($this->task)) {
				$this->setError('', lang('Task.Failed to save task'));
				$taskHistoryModel->db->transRollback();
				return false;
			}

			$this->_taskHistoryData->task_history_id = $taskHistoryModel->getInsertID();
		}

		if ($this->scenario === self::SCENARIO_EDIT) {
			if (empty($this->task_history_id)) {
				$this->setError('', lang('Validation.The "task_history_id" field is required'));
				return false;
			}

			$this->_taskHistoryData = $taskHistoryModel
				->where('task_id', $this->task->task_id)
				->where('task_history_id', $this->task_history_id)
				->where('type', TaskHistoryModel::getUserActionType()[$this->action])
				->first();

			if (empty($this->_taskHistoryData)) {
				$this->setError('', lang('TaskHistory.Task history not found'));
				return false;
			}

			$this->_taskHistoryData->note = $this->note;

			$taskHistoryModel->db->transBegin();

			if (!$taskHistoryModel->save($this->_taskHistoryData)) {
				$this->setError('', lang('TaskHistory.Failed to save task history'));
				$taskHistoryModel->db->transRollback();
				return false;
			}

			$this->deleteFileIdList = !empty($this->deleteFileIdList)
				? explode(',', $this->deleteFileIdList)
				: [];

			if (!empty($this->deleteFileIdList)) {
				$deleteQuery = $fileModel
					->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id and taskHistoryFileTable.task_history_id = ' . $this->_taskHistoryData->task_history_id)
					->whereIn(FileModel::table() . '.file_id', $this->deleteFileIdList)
					->get();

				foreach ($deleteQuery->getResultObject() as $item) {
					if (!$fileModel->delete($item->file_id)) {
						$this->setError('', lang('File.Failed to delete file'));
						$reportModel->db->transRollback();
						return false;
					}
				}
			}

			$this->_existFileList = $this->_taskHistoryFileModel->getFileList($this->task_history_id, $this->user_id);
		}

		if (!empty($this->task_history_video)) {
			if ((1 + count($this->_existFileList['videoList'])) > config('MainEventParamsConfig')->taskHistoryMaxVideo) {
				$this->setError('', lang('Main.Max video files {0}', [config('MainEventParamsConfig')->taskHistoryMaxVideo]));
				$taskHistoryModel->db->transRollback();
				return false;
			}

			$fileDataDisk = FileModel::saveFileOnDisk($this->task_history_video, FileModel::TYPE_VIDEO);
			$fileData = [
				'type' => FileModel::TYPE_VIDEO,
				'related_item' => 'user',
				'item_id' => $this->user_id,
				'url' => $fileDataDisk['fileName'],
				'privacy_status' => FileModel::getPrivacyByEvent()[$this->task->privacy],
				'mime_type' => $fileDataDisk['mimeType'],
				'original_name' => $fileDataDisk['originalName'],
				'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
				'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
			];

			if (!$fileModel->save($fileData)) {
				$this->setError('', $fileModel->errors());
				$taskHistoryModel->db->transRollback();
				return false;
			}

			$fileId = $fileModel->getInsertID();

			if ((int)$this->task->privacy === TaskModel::PRIVATE_STATUS) {
				$permittedId = ((int)$this->user_id === (int)$this->task->owner_user_id)
					? $this->task->assigned_user_id
					: $this->task->owner_user_id;
				$fileModel->setFilePermission($fileId, $this->user_id, [$permittedId]);
			}

			if (!$this->saveTaskHistoryFile($this->_taskHistoryData->task_history_id, $fileId)) {
				$this->setError('', lang('TaskHistory.Failed to save task history file'));
				$taskHistoryModel->db->transRollback();
				return false;
			}
		}

		if (!empty($this->task_history_photo)) {
			if ((count($this->task_history_photo) + count($this->_existFileList['imageList']))
				> config('MainEventParamsConfig')->taskHistoryMaxImage) {
				$this->setError('', lang('Main.Max images {0}', [config('MainEventParamsConfig')->taskHistoryMaxImage]));
				$taskHistoryModel->db->transRollback();
				return false;
			}

			foreach ($this->task_history_photo as $file) {
				$fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_IMAGE);
				$fileData = [
					'type' => FileModel::TYPE_IMAGE,
					'related_item' => 'user',
					'item_id' => $this->user_id,
					'url' => $fileDataDisk['fileName'],
					'privacy_status' => FileModel::getPrivacyByEvent()[$this->task->privacy],
					'mime_type' => $fileDataDisk['mimeType'],
					'original_name' => $fileDataDisk['originalName'],
					'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
					'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
				];

				if (!$fileModel->save($fileData)) {
					$this->setError('', $fileModel->errors());
					$taskHistoryModel->db->transRollback();
					return false;
				}

				$fileId = $fileModel->getInsertID();

				if ((int)$this->task->privacy === TaskModel::PRIVATE_STATUS) {
					$permittedId = ((int)$this->user_id === (int)$this->task->owner_user_id)
						? $this->task->assigned_user_id
						: $this->task->owner_user_id;
					$fileModel->setFilePermission($fileId, $this->user_id, [$permittedId]);
				}

				if (!$this->saveTaskHistoryFile($this->_taskHistoryData->task_history_id, $fileId)) {
					$this->setError('', lang('TaskHistory.Failed to save task history file'));
					$taskHistoryModel->db->transRollback();
					return false;
				}
			}
		}

		if (!empty($this->task_history_document)) {
			if ((count($this->task_history_document) + count($this->_existFileList['documentList']))
				> config('MainEventParamsConfig')->taskHistoryMaxDocument) {
				$this->setError('', lang('Main.Max documents {0}', [config('MainEventParamsConfig')->taskHistoryMaxDocument]));
				$taskModel->db->transRollback();
				return false;
			}

			foreach ($this->task_history_document as $file) {
				$fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_DOCUMENT);
				$fileData = [
					'type' => FileModel::TYPE_DOCUMENT,
					'related_item' => 'user',
					'item_id' => $this->user_id,
					'url' => $fileDataDisk['fileName'],
					'privacy_status' => FileModel::getPrivacyByEvent()[$this->task->privacy],
					'mime_type' => $fileDataDisk['mimeType'],
					'original_name' => $fileDataDisk['originalName'],
					'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
					'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
				];

				if (!$fileModel->save($fileData)) {
					$this->setError('', $fileModel->errors());
					$taskHistoryModel->db->transRollback();
					return false;
				}

				$fileId = $fileModel->getInsertID();

				if ((int)$this->task->privacy === TaskModel::PRIVATE_STATUS) {
					$permittedId = ((int)$this->user_id === (int)$this->task->owner_user_id)
						? $this->task->assigned_user_id
						: $this->task->owner_user_id;
					$fileModel->setFilePermission($fileId, $this->user_id, [$permittedId]);
				}

				if (!$this->saveTaskHistoryFile($this->_taskHistoryData->task_history_id, $fileId)) {
					$this->setError('', lang('TaskHistory.Failed to save task history file'));
					$taskHistoryModel->db->transRollback();
					return false;
				}
			}
		}

		if (!$this->sendWsCompleteTask()) {
			$taskHistoryModel->db->transRollback();
			return false;
		}

		$taskHistoryModel->db->transCommit();
		return true;
	}

	public function getResult()
	{
		return ['taskId' => $this->task->task_id];
	}

	protected function saveTaskHistoryFile($taskHistoryId, $fileId)
	{
		$data = [
			'task_history_id' => $taskHistoryId,
			'file_id' => $fileId,
		];

		if (!$this->_taskHistoryFileModel->save($data)) {
			return false;
		}

		return true;
	}

	protected function sendWsCompleteTask()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();

		$sendUserId = (in_array($this->action, ['accept', 'reject']))
			? $this->task->assigned_user_id
			: $this->task->owner_user_id;

		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getLocale',
				'userId' => $sendUserId,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->locale)) {
			$notificationComponent = new NotificationComponent();
			$userModel = new UserModel();
			$fromUserData = $userModel->getUserData($this->user_id);

			$notificationHtml = $notificationComponent->getTaskActionNotification([
				// 'action' => $this->action,
				// 'actionType' => $this->scenario,
				'fromUserData' => $fromUserData,
				'task' => $this->task,
				'textInfo' => TaskHistoryModel::getTaskActionText($response->locale)[$this->action][$this->scenario],
				'locale' => $response->locale,
			]);

			$wsData = [
				'server' => [
					'controller' => 'Notification',
					'action' => 'sendNotification',
					'userId' => $sendUserId,
					'data' => [
						'html' => $notificationHtml,
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
				return false;
			}

		// 	$startEntityDate = (int)$this->task->date + (int)$this->task->time;
		// 	$endEntityDate = (int)$this->task->date + (int)$this->task->time + (int)$this->task->duration;
		// 	$startDay = DateAndTime::getTimestampStartDay(time()) - $response->timeOffsetSeconds;
		// 	$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;

		// 	if ($this->action === 'complete') {
		// 		$subType = 'created';
		// 	} else if ($this->action === 'accept') {
		// 		$subType = 'completed';
		// 	} else if ($this->action === 'reject') {
		// 		if (($startEntityDate <= time() && $endEntityDate >= time())
		// 			|| ($startEntityDate >= time() && $startEntityDate <= $endDay)) {
		// 			$subType = 'current';
		// 		} else if ($startEntityDate > $endDay) {
		// 			$subType = 'scheduled';
		// 		} else if ($endEntityDate <= time()) {
		// 			$subType = 'overdue';
		// 		}
		// 	}

		// 	$wsData = [
		// 		'server' => [
		// 			'controller' => 'Task',
		// 			'action' => $this->action . 'Task',
		// 			'userId' => $sendUserId,
		// 			'scenario' => $this->scenario,
		// 			'data' => [
		// 				'taskId' => (int)$this->task->task_id,
		// 				'task_history_id' => (int)$this->_taskHistoryData->task_history_id,
		// 				'subType' => $subType,
		// 				'dateTimestamp' => (int)$this->task->date + (int)$this->task->time,
		// 			],
		// 		],
		// 	];

		// 	if (!$wsClientComponent->sendData($wsData)) {
		// 		$this->setError('', $wsClientComponent->error);
		// 		$wsClientComponent->closeSocket();
		// 		return false;
		// 	}
		}

		$wsClientComponent->closeSocket();

		return true;
	}
}