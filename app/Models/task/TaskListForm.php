<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class TaskListForm extends BaseForm
{
	const SUBTYPE_CURRENT = 'current';
	const SUBTYPE_SCHEDULED = 'scheduled';
	const SUBTYPE_COMPLETED = 'completed';
	const SUBTYPE_OVERDUE = 'overdue';
	const SUBTYPE_CREATED = 'created';

	public $user_id;
	public $subType;
	public $name;
	public $privacy;
	public $reviewStatus;
	public $status;
	public $dateFrom;
	public $dateTo;
	public $timeOffsetSeconds;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 10;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'subType' => 'required|string|in_list[current,scheduled,completed,overdue,created]',
		'name' => 'permit_empty|string',
		'privacy' => 'permit_empty|integer|in_list[0,1]',
		'reviewStatus' => 'permit_empty|integer|in_list[0,1,2]',
		'status' => 'permit_empty|integer|in_list[1,2,3]',
		'timeOffsetSeconds' => 'required|integer',
		'dateFrom' => 'permit_empty|valid_date[d-m-Y]',
		'dateTo' => 'permit_empty|valid_date[d-m-Y]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer'
	];

	private $_result;
	private $_query;
	private $_itemCount;

	public function getList()
	{
		$taskModel = new TaskModel();
		$db = $taskModel->db;
		$startDay = DateAndTime::getTimestampStartDay(time()) - $this->timeOffsetSeconds;
		$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;

		if ($this->subType === self::SUBTYPE_CURRENT) {
			$this->_query = $taskModel
				->select(TaskModel::table() . '.*')
				->select('userTable.first_name, userTable.last_name')
				->select('fileTable.url as userAva')
				->select('(select if(is_viewed_assigned = ' . TaskModel::STATUS_NOT_VIEW_ASSIGNED . ', 1, 0)) as notViewed')
				->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.owner_user_id')
				->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
				->where(TaskModel::table() . '.assigned_user_id', $this->user_id)
				->where('status', TaskModel::STATUS_NOT_COMPLETED)
				->where(TaskModel::table() . '.created_at <=', $this->timestamp)
				->groupStart()
					->groupStart()
						->where('(date + time) <=', time())
						->where('(date + time + duration) >=', time())
					->groupEnd()
					->orGroupStart()
						->where('(date + time) >=', time())
						->where('(date + time) <=', $endDay)
					->groupEnd()
				->groupEnd();

			if (!empty($this->name)) {
				$this->_query->like('name', $this->name);
			}

			if (!is_null($this->privacy)) {
				$this->_query->where('privacy', $this->privacy);
			}
		}

		if ($this->subType === self::SUBTYPE_SCHEDULED) {
			$this->_query = $taskModel
				->select(TaskModel::table() . '.*')
				->select('userTable.first_name, userTable.last_name')
				->select('fileTable.url as userAva')
				->select('(select if(is_viewed_assigned = ' . TaskModel::STATUS_NOT_VIEW_ASSIGNED . ', 1, 0)) as notViewed')
				->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.owner_user_id')
				->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
				->where(TaskModel::table() . '.assigned_user_id', $this->user_id)
				->where('status', TaskModel::STATUS_NOT_COMPLETED)
				->where(TaskModel::table() . '.created_at <=', $this->timestamp)
				->where('(date + time) >', $endDay);

			if (!empty($this->name)) {
				$this->_query->like('name', $this->name);
			}

			if (!is_null($this->privacy)) {
				$this->_query->where('privacy', $this->privacy);
			}

			if (!empty($this->dateFrom)) {
				$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
				$this->_query->where('(date + time) >=', $this->dateFrom);
			}

			if (!empty($this->dateTo)) {
				$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
				$this->_query->where('(date + time) <=', $this->dateTo);
			}
		}

		if ($this->subType === self::SUBTYPE_COMPLETED) {
			$this->_query = $taskModel
				->select(TaskModel::table() . '.*')
				->select('userTable.first_name, userTable.last_name')
				->select('fileTable.url as userAva')
				->select('(select if(status = ' . TaskModel::STATUS_COMPLETED . ', 0, 1)) as notViewed')
				->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.owner_user_id')
				->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
				->where(TaskModel::table() . '.assigned_user_id', $this->user_id)
				->where('status', TaskModel::STATUS_COMPLETED)
				->where(TaskModel::table() . '.created_at <=', $this->timestamp);

			if (!empty($this->name)) {
				$this->_query->like('name', $this->name);
			}

			if (!is_null($this->privacy)) {
				$this->_query->where('privacy', $this->privacy);
			}

			if (!is_null($this->reviewStatus)) {
				$this->_query->where('review_status', $this->reviewStatus);
			}

			if (!empty($this->dateFrom)) {
				$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
				$this->_query->where('(date + time) >=', $this->dateFrom);
			}

			if (!empty($this->dateTo)) {
				$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
				$this->_query->where('(date + time) <=', $this->dateTo);
			}
		}

		if ($this->subType === self::SUBTYPE_OVERDUE) {
			$this->_query = $taskModel
				->select(TaskModel::table() . '.*')
				->select('userTable.first_name, userTable.last_name')
				->select('fileTable.url as userAva')
				->select('(select if(is_viewed_assigned = ' . TaskModel::STATUS_NOT_VIEW_ASSIGNED . ', 1, 0)) as notViewed')
				->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.owner_user_id')
				->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
				->where(TaskModel::table() . '.assigned_user_id', $this->user_id)
				->where(TaskModel::table() . '.created_at <=', $this->timestamp)
				->groupStart()
					->groupStart()
						->where('status', TaskModel::STATUS_OVERDUE)
					->groupEnd()
					->orGroupStart()
						->where('(date + time + duration) <=', time())
						->where('status', TaskModel::STATUS_NOT_COMPLETED)
					->groupEnd()
				->groupEnd();

			if (!empty($this->name)) {
				$this->_query->like('name', $this->name);
			}

			if (!is_null($this->privacy)) {
				$this->_query->where('privacy', $this->privacy);
			}

			if (!empty($this->dateFrom)) {
				$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
				$this->_query->where('(date + time) >=', $this->dateFrom);
			}

			if (!empty($this->dateTo)) {
				$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
				$this->_query->where('(date + time) <=', $this->dateTo);
			}
		}

		if ($this->subType === self::SUBTYPE_CREATED) {
			$this->_query = $taskModel
				->select(TaskModel::table() . '.*')
				->select('userTable.first_name, userTable.last_name, userTable.deleted_at as userDeleted')
				->select('fileTable.url as userAva')
				->select('(select if(review_status = ' . TaskModel::STATUS_NOT_REVIEWED . ' and status = ' . TaskModel::STATUS_COMPLETED . ', 1, 0)) as notViewed')
				->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . TaskModel::table() . '.assigned_user_id', 'left')
				->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
				->where(TaskModel::table() . '.owner_user_id', $this->user_id)
				->where(TaskModel::table() . '.created_at <=', $this->timestamp)
				->groupStart()
					->groupStart()
						->where('is_cloned', TaskModel::STATUS_CLONED)
                    	->where('periodicity !=', TaskModel::PERIODICITY_NONE)
                    	->where('review_status !=', TaskModel::STATUS_ACCEPTED)
					->groupEnd()
					->orGroupStart()
						->where(TaskModel::table() . '.is_cloned', TaskModel::STATUS_NOT_CLONED)
					->groupEnd()
				->groupEnd();

			if (!empty($this->name)) {
				$this->_query->like('name', $this->name);
			}

			if (!is_null($this->privacy)) {
				$this->_query->where('privacy', $this->privacy);
			}

			if (!is_null($this->status)) {
				$this->_query->where('status', $this->status);
			}

			if (!is_null($this->reviewStatus)) {
				$this->_query->where('review_status', $this->reviewStatus);
			}

			if (!empty($this->dateFrom)) {
				$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
				$this->_query->where('(date + time) >=', $this->dateFrom);
			}

			if (!empty($this->dateTo)) {
				$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
				$this->_query->where('(date + time) <=', $this->dateTo);
			}
		}

		$this->_query->groupBy(TaskModel::table() . '.task_id')
			->orderBy('notViewed', 'DESC')
			->orderBy('(' . TaskModel::table() . '.date + ' . TaskModel::table() . '.time)', 'DESC');

		$this->_itemCount = $this->_query->countAllResults(false);

		$this->_query =$this->_query->getCompiledSelect();

		$this->_query = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_query . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_query = 'select query.* from (' . $this->_query . ') as query where rowNum > ' . $db->escape($this->rowNum);
		}

		$this->_query = 'select query.* from (' . $this->_query . ') as query limit ' . $this->limit;
		$this->_result = $db->query('select query.* from (' . $this->_query . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$result['taskResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['itemCount'] = $this->_itemCount;
		$result['subType'] = $this->subType;
		$result['titleNotice'] = lang('Task.Are you sure you want to delete the task?');
		$periodicityList = config('MainEventParamsConfig')->periodicity;
		$reminderList = config('MainEventParamsConfig')->reminder;

		foreach ($this->_result->getResultObject() as &$item) {
			if ($this->subType === self::SUBTYPE_CREATED) {
				$item->ownerLabel = lang('Task.executor');
			} else {
				$item->ownerLabel = lang('Task.task created by');
			}

			$item->detailsType = ($this->subType === self::SUBTYPE_CREATED)
				? 'owner'
				: 'assigned';
			$item->userAva = (!empty($item->userAva))
				? FileModel::preparePathToAva($item->userAva)
				: FileModel::DEFAULT_AVA_URL;

			if ($this->subType === self::SUBTYPE_CREATED
				&& (!is_null($item->userDeleted))
					|| empty($item->assigned_user_id)) {
				$item->assignedUserDeleted = true;
			} else {
				$item->assignedUserDeleted = false;
			}

			$item->entity_id = $item->task_id;
			$endEventTimestamp = (int)$item->date + (int)$item->time + (int)$item->duration;
			$item->dateTimestamp = (int)$item->date + (int)$item->time;
			$item->endEventTimestamp = $endEventTimestamp;
			$item->date = DateAndTime::getDateTime(((int)$item->date + (int)$item->time), $this->timeOffsetSeconds, 'genetive');
			$item->endDate = DateAndTime::getDateTime($endEventTimestamp, $this->timeOffsetSeconds, 'genetive');
			$item->privacyValue = ((int)$item->privacy === 1) ? lang('Event.private') : lang('Event.public');
			$item->periodicityValue = lang('TimeAndDate.' . $periodicityList[$item->periodicity]);

			if (!empty($item->reminder)) {
				$item->reminderValue = lang('TimeAndDate.remind {0} before', [lang('TimeAndDate.' . $reminderList[$item->reminder])]);
			} else {
				$item->reminderValue = lang('Event.without reminder');
			}

			if (empty($item->name)) {
				$item->name = lang('Main.No title');
			}

			$result['taskResult'][] = $item;
		}

		return $result;
	}
}