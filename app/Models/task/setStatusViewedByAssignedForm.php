<?php

namespace App\Models\task;

use Common\Forms\BaseForm;
use Common\Models\TaskModel;
use Common\Components\WsClientComponent;

class setStatusViewedByAssignedForm extends BaseForm
{
	public $user_id;
	public $task_id;
	public $owner_user_id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'task_id' => 'required|integer',
		'owner_user_id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	public function setStatusViewed()
	{
		$taskModel = new TaskModel();
		$data = [
			'task_id' => $this->task_id,
			'is_viewed_assigned' => TaskModel::STATUS_VIEW_ASSIGNED,
		];

		if (!$taskModel->save($data)) {
			$this->setError('', lang('Task.Failed to set status as "viewed by executor"'));
			return false;
		}

		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();
		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getLocale',
				'userId' => $this->owner_user_id,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->locale)) {
			$wsData = [
				'server' => [
					'controller' => 'Task',
					'action' => 'changeOutgoingViewedByAssignedStatus',
					'userId' => $this->owner_user_id,
					'data' => [
						'id' => $this->task_id,
						'className' => TaskModel::getViewedByExecutorCssClass()[TaskModel::STATUS_VIEW_ASSIGNED],
						'statusValue' => TaskModel::getViewedByExecutorStatusLabel($response->locale)[TaskModel::STATUS_VIEW_ASSIGNED],
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
				return false;
			}
		}

		$wsClientComponent->closeSocket();

		return true;
	}
}