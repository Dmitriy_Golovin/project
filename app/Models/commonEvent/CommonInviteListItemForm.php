<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use App\Libraries\DateAndTime\DateAndTime;

class CommonInviteListItemForm extends BaseForm
{
	const TYPE_EVENT = 'event';
	const TYPE_MEETING = 'meeting';

	public $user_id;
	public $invite_from_id;
	public $entity_id;
	public $timeOffsetSeconds;
	public $type;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'invite_from_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'type' => 'required|string|in_list[event,meeting]',
	];

	protected $cleanValidationRules = false;

	private $_entityModel;
	private $_result;

	public function getItem()
	{
		if ($this->type === self::TYPE_EVENT) {
			$this->_entityModel = new EventModel();
		}

		if ($this->type === self::TYPE_MEETING) {
			$this->_entityModel = new MeetingModel();
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$subQueryGuestCount = $eventGuestUserModel
			->select('count(event_guest_user_id)')
			->where('related_item', $this->_entityModel::table())
			->where('item_id = ' . $this->_entityModel::table() . '.' . $this->type . '_id')
			->getCompiledSelect();

		$this->_result = $this->_entityModel
			->select($this->_entityModel::table() . '.*')
			->select('(' . $this->_entityModel::table() . '.date + ' . $this->_entityModel::table() . '.time) as fullDate')
			->select('(' . $subQueryGuestCount . ') as userGuestCount')
			->select('userTable.first_name, userTable.last_name')
			->select('fileTable.url as userAva')
			->select('selfGuest.status as selfGuestStatus')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . $this->_entityModel::table() . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->join(EventGuestUserModel::table() . ' as selfGuest', 'selfGuest.item_id = ' . $this->_entityModel::table() . '.' . $this->type . '_id and selfGuest.related_item = "' . $this->_entityModel::table() . '"')
			->where($this->_entityModel::table() . '.' . $this->type . '_id', $this->entity_id)
			->where($this->_entityModel::table() . '.user_id', $this->invite_from_id)
			->where('selfGuest.user_id', $this->user_id)
			->first();

		return true;
	}

	public function getResult()
	{
		$this->_result->entity_id = $this->_result->{$this->type . '_id'};
		$this->_result->date = DateAndTime::getDateTime($this->_result->fullDate, $this->timeOffsetSeconds, 'genetive');
		$endEventTimestamp = $this->_result->fullDate + $this->_result->duration;
		$this->_result->endDate = DateAndTime::getDateTime($endEventTimestamp, $this->timeOffsetSeconds, 'genetive');
		$this->_result->privacyValue = ((int)$this->_result->privacy === 1)
			? lang('Event.private')
			: lang('Event.public');
		$this->_result->userGuestLabel = lang('Event.participants');
		$this->_result->userAva = (!empty($this->_result->userAva))
			? FileModel::preparePathToAva($this->_result->userAva)
			: FileModel::DEFAULT_AVA_URL;
		$this->_result->scheduledInvite = true;

		if (empty($this->_result->name)) {
			$this->_result->name = lang('Main.No title');
		}

		return [
			'item' => $this->_result,
			'type' => $this->type,
			'subType' => 'invitations',
		];
	}
}