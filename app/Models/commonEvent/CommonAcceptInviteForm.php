<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Components\WsClientComponent;

class CommonAcceptInviteForm extends BaseForm
{
	const SCENARIO_EVENT = 'event';
	const SCENARIO_MEETING = 'meeting';

	public $user_id;
	public $id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_entityModel;
	private $_result;

	public function accept()
	{
		if ($this->scenario === self::SCENARIO_EVENT) {
			$this->_entityModel = new EventModel();
		}

		if ($this->scenario === self::SCENARIO_MEETING) {
			$this->_entityModel = new meetingModel();
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$this->_result = $eventGuestUserModel
			->select(EventGuestUserModel::table() . '.*')
			->select('entityTable.user_id as sender_id')
			->join($this->_entityModel::table() . ' as entityTable', 'entityTable.' . $this->scenario . '_id = ' . EventGuestUserModel::table() . '.item_id and "' . $this->_entityModel::table() . '" = ' . EventGuestUserModel::table() . '.related_item')
			->where(EventGuestUserModel::table() . '.user_id', $this->user_id)
			->where('entityTable.' . $this->scenario . '_id', $this->id)
			->where('(entityTable.date + entityTable.time + entityTable.duration) > ', time())
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$guestData = [
			'event_guest_user_id' => $this->_result->event_guest_user_id,
			'status' => EventGuestUserModel::STATUS_ACCEPTED,
		];

		if (!$eventGuestUserModel->save($guestData)) {
			$this->setError('', lang('Main.An error occurred, please try again later'));
			return false;
		}

		$this->sendWsInviteStatusForSender();

		return true;
	}

	public function getResult()
	{
		$statusNotViewedBeforeAction = ((int)$this->_result->status === EventGuestUserModel::STATUS_NOT_VIEWED)
			? true
			: false;
		$detailsDeclineBut = '<span class="entity_decline_invite_button">' . lang('Main.decline') . '</span>';
		$listDeclineBut = '<li class="entity_decline_invite_button"><img src="/img/icon_close_red_pad.png"></li>';
		return [
			'detailsDeclineBut' => $detailsDeclineBut,
			'listDeclineBut' => $listDeclineBut,
			'statusCssClass' => 'invite_status_accept',
			'statusValue' => EventGuestUserModel::getStatusLabels()[EventGuestUserModel::STATUS_ACCEPTED],
			'type' => $this->scenario,
			'id' => $this->id,
			'userId' => $this->user_id,
			'statusNotViewedBeforeAction' => $statusNotViewedBeforeAction,
		];
	}

	protected function sendWsInviteStatusForSender()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();
		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getLocale',
				'userId' => $this->_result->sender_id,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->locale)) {
			$wsData = [
				'server' => [
					'controller' => ucfirst($this->scenario),
					'action' => 'changeOutgoingInviteStatus',
					'userId' => $this->_result->sender_id,
					'data' => [
						'type' => $this->scenario,
						'id' => $this->id,
						'inviteRecipientId' => $this->user_id,
						'className' => 'guest_status invite_status_accept',
						'guestStatusValue' => EventGuestUserModel::getStatusLabels($response->locale)[EventGuestUserModel::STATUS_ACCEPTED],
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
				return false;
			}
		}

		$wsClientComponent->closeSocket();

		return true;
	}
}