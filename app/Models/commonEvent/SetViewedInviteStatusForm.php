<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Components\WsClientComponent;

class SetViewedInviteStatusForm extends BaseForm
{
	const SCENARIO_EVENT = 'event';
	const SCENARIO_MEETING = 'meeting';

	public $user_id;
	public $id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_model;
	private $_guest;
	private $_result;

	public function setStatus()
	{
		if ($this->scenario === self::SCENARIO_EVENT) {
			$this->_model = new EventModel();
		}

		if ($this->scenario === self::SCENARIO_MEETING) {
			$this->_model = new MeetingModel();
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$this->_guest = $eventGuestUserModel
			->select(EventGuestUserModel::table() . '.*')
			->select('commonEventTable.user_id as sender_id')
			->join($this->_model::table() . ' as commonEventTable', 'commonEventTable.' . $this->scenario . '_id = ' . EventGuestUserModel::table() . '.item_id and ' . EventGuestUserModel::table() . '.related_item = "' . $this->_model::table() . '"')
			->where(EventGuestUserModel::table() . '.user_id', $this->user_id)
			->where('commonEventTable.' . $this->scenario . '_id', $this->id)
			->where('commonEventTable.deleted_at', null)
			->first();

		if (empty($this->_guest)) {
			$this->setError('', lang('Event.Invitation not found'));
			return false;
		}

		$this->_guest->status = EventGuestUserModel::STATUS_VIEWED;

		if (!$eventGuestUserModel->save($this->_guest)) {
			$this->setError('', lang('Main.Failed to save entity'));
			return false;
		}

		$this->sendWsInviteStatusForSender();

		return true;
	}

	public function getResult()
	{
		$this->_result['id'] = $this->id;
		$this->_result['type'] = $this->scenario;
		$this->_result['userId'] = $this->user_id;
		$this->_result['className'] = 'event_item_value invite_status_value invite_status_viewed';
		$this->_result['guestStatusValue'] = EventGuestUserModel::getStatusLabels()[$this->_guest->status];
		return $this->_result;
	}

	protected function sendWsInviteStatusForSender()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();
		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getLocale',
				'userId' => $this->_guest->sender_id,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->locale)) {
			$wsData = [
				'server' => [
					'controller' => ucfirst($this->scenario),
					'action' => 'changeOutgoingInviteStatus',
					'userId' => $this->_guest->sender_id,
					'data' => [
						'type' => $this->scenario,
						'id' => $this->id,
						'inviteRecipientId' => $this->user_id,
						'className' => 'guest_status invite_status_viewed',
						'guestStatusValue' => EventGuestUserModel::getStatusLabels($response->locale)[$this->_guest->status],
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
				return false;
			}
		}

		$wsClientComponent->closeSocket();

		return true;
	}
}