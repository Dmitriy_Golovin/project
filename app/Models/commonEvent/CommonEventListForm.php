<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Models\UserModel;
use Common\Models\FileModel;
use Common\Models\ReportModel;
use App\Libraries\DateAndTime\DateAndTime;

class CommonEventListForm extends BaseForm
{
	const TYPE_EVENT = 'event';
	const TYPE_MEETING = 'meeting';

	const SUBTYPE_CURRENT = 'current';
	const SUBTYPE_SCHEDULED = 'scheduled';
	const SUBTYPE_COMPLETED = 'completed';
	const SUBTYPE_INVITATIONS = 'invitations';

	public $user_id;
	public $type;
	public $subType;
	public $name;
	public $privacy;
	public $inviteStatus;
	public $onlyCurrentAndScheduled;
	public $dateFrom;
	public $dateTo;
	public $timeOffsetSeconds;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 10;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|string',
		'subType' => 'required|string',
		'name' => 'permit_empty|string',
		'privacy' => 'permit_empty|integer|in_list[0,1]',
		'inviteStatus' => 'permit_empty|integer|in_list[0,1,2,3]',
		'onlyCurrentAndScheduled' => 'permit_empty|integer|in_list[0,1]',
		'timeOffsetSeconds' => 'required|integer',
		'dateFrom' => 'permit_empty|valid_date[d-m-Y]',
		'dateTo' => 'permit_empty|valid_date[d-m-Y]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_query;
	private $_itemCount;
	private $_result;

	public function getList()
	{
		if ($this->type === self::TYPE_EVENT) {
			$eventModel = new EventModel();
			$db = $eventModel->db;
			$this->_query = $eventModel;

			if ($this->subType === self::SUBTYPE_CURRENT) {
				$this->_getCurrent(EventModel::table());
			}

			if ($this->subType === self::SUBTYPE_SCHEDULED) {
				$this->_getScheduled(EventModel::table());
			}

			if ($this->subType === self::SUBTYPE_COMPLETED) {
				$this->_getCompleted(EventModel::table());
			}

			if ($this->subType === self::SUBTYPE_INVITATIONS) {
				$this->_getInvitations(EventModel::table());
			}
		}

		if ($this->type === self::TYPE_MEETING) {
			$meetingModel = new MeetingModel();
			$db = $meetingModel->db;
			$this->_query = $meetingModel;

			if ($this->subType === self::SUBTYPE_CURRENT) {
				$this->_getCurrent(MeetingModel::table());
			}

			if ($this->subType === self::SUBTYPE_SCHEDULED) {
				$this->_getScheduled(MeetingModel::table());
			}

			if ($this->subType === self::SUBTYPE_COMPLETED) {
				$this->_getCompleted(MeetingModel::table());
			}

			if ($this->subType === self::SUBTYPE_INVITATIONS) {
				$this->_getInvitations(MeetingModel::table());
			}
		}

		$this->_query = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_query . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_query = 'select query.* from (' . $this->_query . ') as query where rowNum > ' . $db->escape($this->rowNum);
		}

		$this->_query = 'select query.* from (' . $this->_query . ') as query limit ' . $this->limit;
		$this->_result = $db->query('select query.* from (' . $this->_query . ') as query');

		return true;
	}

	private function _getCurrent($table)
	{
		$startDay = DateAndTime::getTimestampStartDay(time()) - $this->timeOffsetSeconds;
		$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;
		$this->_query->select($table . '.*')
			->select('(' . $table . '.date + ' . $table . '.time) as fullDate')
			->select('count(userGuest.event_guest_user_id) as userGuestCount')
			->join(EventGuestUserModel::table() . ' as userGuest', 'userGuest.item_id = ' . $table . '.' . $this->type . '_id and userGuest.related_item = "' . $table . '"', 'left')
			->where($table . '.user_id', $this->user_id)
			->where('is_completed', EventModel::STATUS_NOT_COMPLETED)
			->where($table . '.created_at <=', $this->timestamp)
			->groupStart()
				->groupStart()
					->where('(date + time) <=', time())
					->where('(date + time + duration) >=', time())
				->groupEnd()
				->orGroupStart()
					->where('(date + time) >=', time())
					->where('(date + time) <=', $endDay)
				->groupEnd()
			->groupEnd();

		if (!empty($this->name)) {
			$this->_query->like('name', $this->name);
		}

		if (!is_null($this->privacy)) {
			$this->_query->where('privacy', $this->privacy);
		}

		$this->_query->groupBy($table . '.' . $this->type . '_id')
			->orderBy('fullDate', 'ASC');

		$this->_itemCount = $this->_query->countAllResults(false);

		$this->_query =$this->_query->getCompiledSelect();
	}

	private function _getScheduled($table)
	{
		$startDay = DateAndTime::getTimestampStartDay(time()) - $this->timeOffsetSeconds;
		$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;
		$this->_query->select($table . '.*')
			->select('(' . $table . '.date + ' . $table . '.time) as fullDate')
			->select('count(userGuest.event_guest_user_id) as userGuestCount')
			->join(EventGuestUserModel::table() . ' as userGuest', 'userGuest.item_id = ' . $table . '.' . $this->type . '_id and userGuest.related_item = "' . $table . '"', 'left')
			->where($table . '.user_id', $this->user_id)
			->where('is_completed', EventModel::STATUS_NOT_COMPLETED)
			->where('(date + time) >', $endDay)
			->where($table . '.created_at <=', $this->timestamp);

		if (!empty($this->name)) {
			$this->_query->like('name', $this->name);
		}

		if (!is_null($this->privacy)) {
			$this->_query->where('privacy', $this->privacy);
		}

		if (!empty($this->dateFrom)) {
			$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
			$this->_query->where('(date + time) >=', $this->dateFrom);
		} else {
			$this->_query->where('(date + time) >=', time());
		}

		if (!empty($this->dateTo)) {
			$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
			$this->_query->where('(date + time) <=', $this->dateTo);
		}

		$this->_query->groupBy($table . '.' . $this->type . '_id')
			->orderBy('fullDate', 'ASC');
			
		$this->_itemCount = $this->_query->countAllResults(false);

		$this->_query =$this->_query->getCompiledSelect();
	}

	private function _getCompleted($table)
	{
		$this->_query->select($table . '.*')
			->select('(' . $table . '.date + ' . $table . '.time) as fullDate')
			->select('count(userGuest.event_guest_user_id) as userGuestCount')
			->select('reportTable.report_id')
			->join(EventGuestUserModel::table() . ' as userGuest', 'userGuest.item_id = ' . $table . '.' . $this->type . '_id and userGuest.related_item = "' . $table . '"', 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.item_id = ' . $table . '.' . $this->type . '_id and reportTable.related_item = "' . $table . '"', 'left')
			->where($table . '.user_id', $this->user_id)
			->where($table . '.created_at <=', $this->timestamp)
			->where('(date + time + duration) <=', time());

		if (!empty($this->name)) {
			$this->_query->like('name', $this->name);
		}

		if (!is_null($this->privacy)) {
			$this->_query->where('privacy', $this->privacy);
		}

		if (!empty($this->dateFrom)) {
			$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
			$this->_query->where('(date + time) >=', $this->dateFrom);
		}

		if (!empty($this->dateTo)) {
			$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
			$this->_query->where('(date + time) <=', $this->dateTo);
		}

		$this->_query->groupBy($table . '.' . $this->type . '_id')
			->orderBy('fullDate', 'DESC');
			
		$this->_itemCount = $this->_query->countAllResults(false);

		$this->_query =$this->_query->getCompiledSelect();
	}

	private function _getInvitations($table)
	{
		$eventGuestUserModel = new EventGuestUserModel();
		$subQueryGuestCount = $eventGuestUserModel
			->select('count(event_guest_user_id)')
			->where('related_item', $table)
			->where('item_id = ' . $table . '.' . $this->type . '_id')
			->getCompiledSelect();

		$this->_query->select($table . '.*')
			->select('(' . $table . '.date + ' . $table . '.time) as fullDate')
			->select('(' . $subQueryGuestCount . ') as userGuestCount')
			->select('userTable.first_name, userTable.last_name')
			->select('fileTable.url as userAva')
			->select('selfGuest.status as selfGuestStatus')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . $table . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->join(EventGuestUserModel::table() . ' as selfGuest', 'selfGuest.item_id = ' . $table . '.' . $this->type . '_id and selfGuest.related_item = "' . $table . '"')
			->where('selfGuest.user_id', $this->user_id)
			->where($table . '.created_at <=', $this->timestamp);

		if (!empty($this->name)) {
			$this->_query->like('name', $this->name);
		}

		if (!is_null($this->privacy)) {
			$this->_query->where('privacy', $this->privacy);
		}

		if (!is_null($this->inviteStatus)) {
			$this->_query->where('selfGuest.status', $this->inviteStatus);
		}

		if (!empty($this->dateFrom)) {
			$this->dateFrom = DateAndTime::parse($this->dateFrom . ' 00:00:00')->getTimestamp() - $this->timeOffsetSeconds;
			$this->_query->where('(date + time) >=', $this->dateFrom);
		}

		if (!empty($this->dateTo)) {
			$this->dateTo = DateAndTime::parse($this->dateTo . ' 00:00:00')->getTimestamp() + DAY - $this->timeOffsetSeconds - 1;
			$this->_query->where('(date + time) <=', $this->dateTo);
		}

		if (!empty($this->onlyCurrentAndScheduled)) {
			$this->_query->groupStart()
					->groupStart()
						->where('(date + time) <=', time())
						->where('(date + time + duration) >=', time())
					->groupEnd()
					->orGroupStart()
						->where('(date + time) >=', time())
					->groupEnd()
				->groupEnd();
		}

		$this->_query->groupBy($table . '.' . $this->type . '_id')
			->orderBy('fullDate', 'ASC');
			
		$this->_itemCount = $this->_query->countAllResults(false);

		$this->_query =$this->_query->getCompiledSelect();
	}

	public function getResult()
	{
		$result = [];
		$result['eventResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['itemCount'] = $this->_itemCount;
		$result['type'] = $this->type;
		$result['subType'] = $this->subType;
		
		if ($this->type === self::TYPE_EVENT) {
			$result['titleNotice'] = lang('Event.Are you sure you want to delete the event?');
		}

		if ($this->type === self::TYPE_MEETING) {
			$result['titleNotice'] = lang('Meeting.Are you sure you want to delete the meeting?');
		}

		$periodicityList = config('MainEventParamsConfig')->periodicity;
		$reminderList = config('MainEventParamsConfig')->duration;

		foreach ($this->_result->getResultObject() as &$item) {
			if ($this->subType === self::SUBTYPE_INVITATIONS) {
				$item->userAva = (!empty($item->userAva))
					? FileModel::preparePathToAva($item->userAva)
					: FileModel::DEFAULT_AVA_URL;
				$item->scheduledInvite = (((int)$item->date + (int)$item->time + (int)$item->duration) > time()) ? true : false;
			}

			$item->entity_id = $item->{$this->type . '_id'};
			$item->date = DateAndTime::getDateTime($item->fullDate, $this->timeOffsetSeconds, 'genetive');
			$endEventTimestamp = $item->fullDate + $item->duration;
			$item->endDate = DateAndTime::getDateTime($endEventTimestamp, $this->timeOffsetSeconds, 'genetive');
			$item->privacyValue = ((int)$item->privacy === 1) ? lang('Event.private') : lang('Event.public');
			$item->periodicityValue = lang('TimeAndDate.' . $periodicityList[$item->periodicity]);

			if (!empty($item->reminder)) {
				$item->reminderValue = lang('TimeAndDate.remind {0} before', [lang('TimeAndDate.' . $reminderList[$item->reminder])]);
			} else {
				$item->reminderValue = lang('Event.without reminder');
			}

			if (in_array($this->type, [self::TYPE_EVENT, self::TYPE_MEETING])) {
				$item->userGuestLabel = lang('Event.participants');
			}

			if (empty($item->name)) {
				$item->name = lang('Main.No title');
			}

			$result['eventResult'][] = $item;
		}

		return $result;
	}
}