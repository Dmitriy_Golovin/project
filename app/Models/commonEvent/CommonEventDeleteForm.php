<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;

class CommonEventDeleteForm extends BaseForm
{
	const SCENARIO_EVENT = 'event';
	const SCENARIO_MEETING = 'meeting';
	const SCENARIO_TASK = 'task';

	public $user_id;
	public $id;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'id' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_entityModel;
	private $_result;

	public function deleteItem()
	{
		if ($this->scenario === self::SCENARIO_EVENT) {
			$this->_entityModel = new EventModel();
			$event = $this->_entityModel
				->where($this->scenario . '_id', $this->id)
				->where('user_id', $this->user_id)
				->first();

			if (empty($event)) {
				$this->setError('', lang('Main.Entity not found'));
				return false;
			}

			$this->_result = $event;
		}

		if ($this->scenario === self::SCENARIO_MEETING) {
			$this->_entityModel = new MeetingModel();
			$meeting = $this->_entityModel
				->where($this->scenario . '_id', $this->id)
				->where('user_id', $this->user_id)
				->first();

			if (empty($meeting)) {
				$this->setError('', lang('Main.Entity not found'));
				return false;
			}

			$this->_result = $meeting;
		}

		if ($this->scenario === self::SCENARIO_TASK) {
			$this->_entityModel = new TaskModel();
			$task = $this->_entityModel
				->where($this->scenario . '_id', $this->id)
				->where('owner_user_id', $this->user_id)
				->where('review_status !=', TaskModel::STATUS_ACCEPTED)
				->first();

			if (empty($task)) {
				$this->setError('', lang('Main.Entity not found'));
				return false;
			}

			$this->_result = $task;
		}

		if (!$this->_entityModel->delete($this->_result->{$this->scenario . '_id'})) {
			$this->setError('', lang('Main.Failed to delete entity'));
			return false;
		}

		return true;
	}

	public function getResult()
	{
		$subType = '';
		$subTypeMenu = 'current';
		$startEventDate = (int)$this->_result->date + (int)$this->_result->time;
		$endEventDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;

		if ($this->scenario !== self::SCENARIO_TASK) {
			if ($endEventDate <= time()) {
				$subType = 'completed';
				$subTypeMenu = 'completed';
			} else if ($startEventDate > time()) {
				$subType = 'scheduled';
				$subTypeMenu = 'scheduled';
			}
		} else {
			$subType = 'created';
			$subTypeMenu = 'created';
		}

		return [
			'backLink' => getenv('baseURL') . $this->scenario . '/' . $subType,
			'type' => $this->scenario,
			'subTypeMenu' => $subTypeMenu,
			'id' => $this->_result->{$this->scenario . '_id'},
		];
	}
}