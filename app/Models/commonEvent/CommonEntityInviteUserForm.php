<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\UserModel;
use Common\Models\EventGuestUserModel;
use App\Libraries\DateAndTime\DateAndTime;
use Common\Components\WsClientComponent;

class CommonEntityInviteUserForm extends BaseForm
{
	public $user_id;
	public $profile_id;
	public $entity_id;
	public $type;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2]',
	];

	protected $cleanValidationRules = false;

	protected $entityModel;
	protected $entityFieldId;
	protected $entityType;

	private $_result;

	public function invite()
	{
		$mainEventParamsConfig = config('MainEventParamsConfig');

		if ((int)$this->type === (int)$mainEventParamsConfig->mainTypeEvent) {
			$this->entityModel = new EventModel();
			$this->entityFieldId = 'event_id';
			$this->entityType = 'event';
		} else if ((int)$this->type === (int)$mainEventParamsConfig->mainTypeMeeting) {
			$this->entityModel = new MeetingModel();
			$this->entityFieldId = 'meeting_id';
			$this->entityType = 'meeting';
		}

		$entity = $this->entityModel
			->select($this->entityModel::table() . '.*')
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . $this->entityModel::table() . '" and eventGuestTable.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId . ' and eventGuestTable.user_id = ' . $this->profile_id, 'left')
			->where($this->entityModel::table() . '.user_id', $this->user_id)
			->where($this->entityModel::table() . '.' . $this->entityFieldId, $this->entity_id)
			->where('eventGuestTable.event_guest_user_id is null')
			->first();

		if (empty($entity)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$eventGuestUserModel = new EventGuestUserModel();
		$eventGuestUserData = [
			'related_item' => $this->entityModel::table(),
			'item_id' => $this->entity_id,
			'user_id' => $this->profile_id,
			'status' => EventGuestUserModel::STATUS_NOT_VIEWED,
		];

		if (!$eventGuestUserModel->save($eventGuestUserData)) {
			$this->setError('', lang('Main.Failed to send invitation'));
			return false;
		}

		$this->sendWsInvite();

		return true;
	}

	public function getResult()
	{
		return ['entityId' => $this->entity_id];
	}

	protected function sendWsInvite()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();

		$wsData = [
			'server' => [
				'controller' => ucfirst($this->entityType),
				'action' => 'sendInvite',
				'userIdList' => [$this->profile_id],
				'data' => [
					$this->entityType . '_id' => $this->entity_id,
					'invite_from_id' => $this->user_id,
					'type' => $this->entityType,
				],
			],
		];

		if (!$wsClientComponent->sendData($wsData)) {
			$this->setError('', $wsClientComponent->error);
			$wsClientComponent->closeSocket();
			return false;
		}

		$wsClientComponent->closeSocket();

		return true;
	}
}