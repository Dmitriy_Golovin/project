<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\FileModel;
use Common\Models\EventGuestUserModel;
use Common\Models\UserModel;
use App\Libraries\DateAndTime\DateAndTime;
use Common\Components\WsClientComponent;

class SaveBaseEventForm extends BaseForm
{
	const TYPE_EVENT = 'event';
	const TYPE_MEETING = 'meeting';
	const TYPE_TASK = 'task';

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';

	public $user_id;
	public $type;
	public $name;
	public $date;
	public $time;
	public $duration;
	public $privacy;
	public $periodicity;
	public $description;
	public $reminder;
	public $location;
	public $assigned_user_id;
	public $guestUserIdList;
	public $timeOffsetSeconds;
	public $id;
	public $task_video;
	public $task_image;
	public $task_document;
	public $deleteTaskFileIdList;

	protected $validationRules =[
		'user_id' => 'required|integer',
		'type' => 'required|string',
		'name' => 'required|string|max_length[150]',
		'date' => 'required|integer',
		'time' => 'required|integer',
		'privacy' => 'required|integer|in_list[0,1]',
		'periodicity' => 'required|integer|in_list[0,1,2,3,4]',
		'timeOffsetSeconds' => 'required|integer',
		'duration' => 'permit_empty|integer',
		'description' => 'permit_empty|string',
		'location' => 'permit_empty|string',
		'reminder' => 'permit_empty|integer',
		'assigned_user_id' => 'permit_empty|integer',
		'guestUserIdList' => 'permit_empty|string',
		'id' => 'permit_empty|integer',
		'task_video' => [
			'uploaded[task_video]',
			'max_size[task_video,20000]',
			'ext_in[task_video,mp4]',
			'mime_in[task_video,video/mp4]',
			'permit_empty',
		],
		'task_image.*' => [
			'uploaded[task_image]',
			// 'max_size[task_image,3048]',
			'is_image[task_image]',
			'ext_in[task_image,png,jpeg,jpg,gif]',
			'mime_in[task_image,image/png,image/jpg,image/jpeg,image/gif]',
			'permit_empty',
		],
		'task_document.*' => [
			'uploaded[task_document]',
			'max_size[task_document,5000]',
			'ext_in[task_document,txt,doc,docx,pdf,ppt,pptx,csv,csv1,xls,xlsx,zip,rar]',
			'permit_empty',
		],
		'deleteTaskFileIdList' => 'permit_empty',
	];

	protected $validationMessages = [
        'name' => [
            'required' => 'Validation.The "name" field is required',
        ],
    ];

	protected $cleanValidationRules = false;

	private $_result;
	private $_id;
	private $_wsSendInviteUserIdList = [];
	private $_wsSendNewTaskUserId = null;
	private $_taskHistoryMainId;

	public function save()
	{
		if ($this->type === self::TYPE_EVENT) {
			$eventModel = new EventModel();
			$eventTableName = $eventModel->getTableName();
			$eventData = $this->eventMeetingPrepareData();

			if ($this->scenario === self::SCENARIO_UPDATE) {
				if (empty($this->id)) {
					$this->setError('', lang('Main.Required parameter id is missing'));
					return false;
				}

				$eventData[$this->type . '_id'] = $this->id;
			}

			$eventModel->db->transBegin();

			if (!$eventModel->save($eventData)) {
				$this->setError('', $eventModel->errors());
				$eventModel->db->transRollback();
				return false;
			}

			if ($this->scenario === self::SCENARIO_CREATE) {
				$this->_id = $eventModel->getInsertID();
			}

			if ($this->scenario === self::SCENARIO_UPDATE) {
				$this->_id = $this->id;
			}

			if (!$this->saveGuestUser($eventTableName, $this->_id)) {
				$eventModel->db->transRollback();
				return false;
			}

			if (count($this->_wsSendInviteUserIdList) > 0) {
				if (!$this->sendWsInvite()) {
					$eventModel->db->transRollback();
					return false;
				}
			}

			$eventModel->db->transCommit();
		}

		if ($this->type === self::TYPE_MEETING) {
			$meetingModel = new MeetingModel();
			$meetingTableName = $meetingModel->getTableName();
			$meetingData = $this->eventMeetingPrepareData();

			if ($this->scenario === self::SCENARIO_UPDATE) {
				if (empty($this->id)) {
					$this->setError('', lang('Main.Required parameter id is missing'));
					return false;
				}

				$meetingData[$this->type . '_id'] = $this->id;
			}

			$meetingModel->db->transBegin();

			if (!$meetingModel->save($meetingData)) {
				$this->setError('', $meetingModel->errors());
				$meetingModel->db->transRollback();
				return false;
			}

			if ($this->scenario === self::SCENARIO_CREATE) {
				$this->_id = $meetingModel->getInsertID();
			}

			if ($this->scenario === self::SCENARIO_UPDATE) {
				$this->_id = $this->id;
			}

			if (!$this->saveGuestUser($meetingTableName, $this->_id)) {
				$meetingModel->db->transRollback();
				return false;
			}

			if (count($this->_wsSendInviteUserIdList) > 0) {
				if (!$this->sendWsInvite()) {
					$eventModel->db->transRollback();
					return false;
				}
			}

			$meetingModel->db->transCommit();
		}

		if ($this->type === self::TYPE_TASK) {
			$taskModel = new TaskModel();
			$taskTableName = $taskModel->getTableName();
			$userModel = new UserModel();
			$fileModel = new FileModel();
			$taskHistoryFileModel = new TaskHistoryFileModel();

			if (!empty($this->assigned_user_id)) {
				$assignedUser = $userModel->getUserDataMin($this->assigned_user_id);

				if (empty($assignedUser)) {
					$this->setError('', lang('Main.User with id {0} not found', [$this->assigned_user_id]));
					return false;
				}
			}

			if ((int)$this->date + (int)$this->time < time()) {
				$this->setError('', lang('Task.Task date and time cannot be in the past'));
				return false;
			}

			$taskData = [
				'owner_user_id' => $this->user_id,
				'assigned_user_id' => (!empty($this->assigned_user_id)) ? $this->assigned_user_id : null,
				'date' => $this->date,
				'time' => $this->time,
				'name' => $this->name,
				'privacy' => $this->privacy,
				'periodicity' => $this->periodicity,
				'status' => (!empty($this->assigned_user_id)) ? TaskModel::STATUS_NOT_COMPLETED : TaskModel::STATUS_NO_EXECUTOR,
				'deleted_at' => null,
        		'time_offset_seconds' => $this->timeOffsetSeconds,
			];
			
			if ($this->scenario === self::SCENARIO_CREATE && !empty($this->assigned_user_id)) {
				$this->_wsSendNewTaskUserId = $this->assigned_user_id;
			}

			if ($this->scenario === self::SCENARIO_UPDATE) {
				if (empty($this->id)) {
					$this->setError('', lang('Main.Required parameter id is missing'));
					return false;
				}

				$task = $taskModel->find($this->id);

				if (empty($task)) {
					$this->setError('', lang('Main.Entity not found'));
					return false;
				}

				$taskModel->checkAndSaveFilePermission($task, $this->privacy, $this->assigned_user_id);

				if ((int)$task->assigned_user_id !== (int)$this->assigned_user_id && !empty($this->assigned_user_id)) {
					$this->_wsSendNewTaskUserId = $this->assigned_user_id;
				}

				$taskData[$this->type . '_id'] = $this->id;
			}

			if (!empty($this->description)) {
				$taskData['description'] = $this->description;
			}

			if (!empty($this->reminder)) {
				$taskData['reminder'] = $this->reminder;
			}

			if (!empty($this->duration)) {
				$taskData['duration'] = $this->duration;
			}

			$taskModel->db->transBegin();

			if (!$taskModel->save($taskData)) {
				$this->setError('', $taskModel->errors());
				$taskModel->db->transRollback();
				return false;
			}

			if ($this->scenario === self::SCENARIO_CREATE) {
				$taskHistoryModel = new TaskHistoryModel();
				$this->_id = $taskModel->getInsertID();
				$taskHistoryData = [
					'task_id' => $this->_id,
					'user_id' => $taskData['owner_user_id'],
					'type' =>TaskHistoryModel::TYPE_CREATE,
					'is_main' => TaskHistoryModel::IS_MAIN,
				];

				if (!$taskHistoryModel->createItem($taskHistoryData)) {
					$this->setError('', lang('Task.Failed to save task history'));
					$taskModel->db->transRollback();
					return false;
				}

				$this->_taskHistoryMainId = $taskHistoryModel->getInsertID();
			}

			if ($this->scenario === self::SCENARIO_UPDATE) {
				$taskHistoryModel = new TaskHistoryModel();
				$this->_id = $this->id;
				$taskHistoryData = [
					'task_id' => $this->_id,
					'user_id' => $taskData['owner_user_id'],
					'type' =>TaskHistoryModel::TYPE_EDIT,
				];

				if (!$taskHistoryModel->createItem($taskHistoryData)) {
					$this->setError('', lang('Task.Failed to save task history'));
					$taskModel->db->transRollback();
					return false;
				}

				$taskHistoryMain = $taskHistoryModel
					->where('task_id', (!empty($task->source_id)) ? $task->source_id : $task->task_id)
					->where('is_main', TaskHistoryModel::IS_MAIN)
					->first();

				$this->_taskHistoryMainId = $taskHistoryMain->task_history_id;

				$this->deleteTaskFileIdList = !empty($this->deleteTaskFileIdList)
					? explode(',', $this->deleteTaskFileIdList)
					: [];

				if (!empty($this->deleteTaskFileIdList)) {
					$deleteQuery = $fileModel
						->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id and taskHistoryFileTable.task_history_id = ' . $this->_taskHistoryMainId)
						->whereIn(FileModel::table() . '.file_id', $this->deleteTaskFileIdList)
						->get();

					foreach ($deleteQuery->getResultObject() as $item) {
						if (!$fileModel->delete($item->file_id)) {
							$this->setError('', lang('File.Failed to delete file'));
							$reportModel->db->transRollback();
							return false;
						}
					}
				}
			}

			if (!empty($this->task_video)) {
				$fileDataDisk = FileModel::saveFileOnDisk($this->task_video, FileModel::TYPE_VIDEO);
				$fileData = [
					'type' => FileModel::TYPE_VIDEO,
					'related_item' => 'user',
					'item_id' => $this->user_id,
					'url' => $fileDataDisk['fileName'],
					'privacy_status' => FileModel::getPrivacyByEvent()[$this->privacy],
					'mime_type' => $fileDataDisk['mimeType'],
					'original_name' => $fileDataDisk['originalName'],
					'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
					'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
				];

				if (!$fileModel->save($fileData)) {
					$this->setError('', $fileModel->errors());
					$taskModel->db->transRollback();
					return false;
				}

				$fileId = $fileModel->getInsertID();

				if (!empty($this->assigned_user_id) && (int)$this->privacy === TaskModel::PRIVATE_STATUS) {
					$fileModel->setFilePermission($fileId, $this->user_id, [$this->assigned_user_id]);
				}

				if (!$this->saveTaskHistoryFile($taskHistoryFileModel, $this->_taskHistoryMainId, $fileId)) {
					$this->setError('', lang('TaskHistory.Failed to save task history file'));
					$taskModel->db->transRollback();
					return false;
				}
			}

			if (!empty($this->task_image)) {
				if (count($this->task_image) > config('MainEventParamsConfig')->eventCreateMaxImage) {
					$this->setError('', lang('Main.Max images {0}', [config('MainEventParamsConfig')->eventCreateMaxImage]));
					$taskModel->db->transRollback();
					return false;
				}

				foreach ($this->task_image as $file) {
					if (empty($file)) {
						continue;
					}

					$fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_IMAGE);
					$fileData = [
						'type' => FileModel::TYPE_IMAGE,
						'related_item' => 'user',
						'item_id' => $this->user_id,
						'url' => $fileDataDisk['fileName'],
						'privacy_status' => FileModel::getPrivacyByEvent()[$this->privacy],
						'mime_type' => $fileDataDisk['mimeType'],
						'original_name' => $fileDataDisk['originalName'],
						'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
						'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
					];

					if (!$fileModel->save($fileData)) {
						$this->setError('', $fileModel->errors());
						$taskModel->db->transRollback();
						return false;
					}

					$fileId = $fileModel->getInsertID();

					if (!empty($this->assigned_user_id) && (int)$this->privacy === TaskModel::PRIVATE_STATUS) {
						$fileModel->setFilePermission($fileId, $this->user_id, [$this->assigned_user_id]);
					}

					if (!$this->saveTaskHistoryFile($taskHistoryFileModel, $this->_taskHistoryMainId, $fileId)) {
						$this->setError('', lang('TaskHistory.Failed to save task history file'));
						$taskModel->db->transRollback();
						return false;
					}
				}
			}

			if (!empty($this->task_document)) {
				if (count($this->task_document) > config('MainEventParamsConfig')->eventCreateMaxDocument) {
					$this->setError('', lang('Main.Max documents {0}', [config('MainEventParamsConfig')->eventCreateMaxDocument]));
					$taskModel->db->transRollback();
					return false;
				}

				foreach ($this->task_document as $file) {
					if (empty($file)) {
						continue;
					}

					$fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_DOCUMENT);
					$fileData = [
						'type' => FileModel::TYPE_DOCUMENT,
						'related_item' => 'user',
						'item_id' => $this->user_id,
						'url' => $fileDataDisk['fileName'],
						'privacy_status' => FileModel::getPrivacyByEvent()[$this->privacy],
						'mime_type' => $fileDataDisk['mimeType'],
						'original_name' => $fileDataDisk['originalName'],
						'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
						'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
					];

					if (!$fileModel->save($fileData)) {
						$this->setError('', $fileModel->errors());
						$taskModel->db->transRollback();
						return false;
					}

					$fileId = $fileModel->getInsertID();

					if (!empty($this->assigned_user_id) && (int)$this->privacy === TaskModel::PRIVATE_STATUS) {
						$fileModel->setFilePermission($fileId, $this->user_id, [$this->assigned_user_id]);
					}


					if (!$this->saveTaskHistoryFile($taskHistoryFileModel, $this->_taskHistoryMainId, $fileId)) {
						$this->setError('', lang('TaskHistory.Failed to save task history file'));
						$taskModel->db->transRollback();
						return false;
					}
				}
			}

			if (!is_null($this->_wsSendNewTaskUserId)) {
				if (!$this->sendWsNewTask()) {
					$taskModel->db->transRollback();
					return false;
				}
			}

			$taskModel->db->transCommit();
		}

		return true;
	}

	public function getResult()
	{
		$dateTimestamp = (int)$this->date + (int)$this->time + (int)$this->timeOffsetSeconds;
		$dateTime = DateAndTime::createFromTimestamp($dateTimestamp, '');

		return [
			'date' => $dateTime->toDateString(),
			'type' => $this->type,
			'typeSettings' => 'my_' . $this->type . 's_check',
			'id' => $this->_id,
		];
	}

	protected function eventMeetingPrepareData()
	{
		$result = [
			'user_id' => $this->user_id,
			'date' => $this->date,
			'time' => $this->time,
			'name' => $this->name,
			'privacy' => $this->privacy,
			'periodicity' => $this->periodicity,
			'deleted_at' => null,
			'time_offset_seconds' => $this->timeOffsetSeconds,
		];

		if (!empty($this->description)) {
			$result['description'] = $this->description;
		}

		if (!empty($this->location)) {
			$result['location'] = $this->location;
		}

		if (!empty($this->reminder)) {
			$result['reminder'] = $this->reminder;
		}

		if (!empty($this->duration)) {
			$result['duration'] = $this->duration;
		}

		if ((int)$this->date + (int)$this->time + (int)$this->duration > time()) {
			$result['is_completed'] = 0;
		} else {
			$result['is_completed'] = 1;
		}

		return $result;
	}

	protected function saveGuestUser($tableName, $id)
	{
		$notDeletedUserIdlist = [];
		$eventGuestUserModel = new EventGuestUserModel();

		if (!empty($this->guestUserIdList)) {
			$guestUserIdList = json_decode($this->guestUserIdList);
			$this->guestUserIdList = is_array($guestUserIdList)
				? $guestUserIdList
				: explode(',', $this->guestUserIdList);
			$userModel = new UserModel();

			foreach ($this->guestUserIdList as $guestUserId) {
				$existUser = $userModel->getUserDataMin($guestUserId);

				if (empty($existUser)) {
					$this->setError('', lang('User with id {0} not found', [$guestUserId]));
					return false;
				}

				$existGuest = $eventGuestUserModel
					->where('related_item', $tableName)
					->where('item_id', $id)
					->where('user_id', $guestUserId)
					->first();

				if (!empty($existGuest)) {
					if ($this->scenario === self::SCENARIO_CREATE) {
						$this->setError('', lang('Main.Guest with id {0} already exist', [$guestUserId]));
						return false;
					}

					if ($this->scenario === self::SCENARIO_UPDATE) {
						$notDeletedUserIdlist[] = $guestUserId;
						continue;
					}
				}

				$this->_wsSendInviteUserIdList[] = $guestUserId;

				$guestData = [
					'related_item' => $tableName,
					'item_id' => $id,
					'user_id' => $guestUserId,
					'status' => EventGuestUserModel::STATUS_NOT_VIEWED,
				];

				if (!$eventGuestUserModel->save($guestData)) {
					$this->setError('', $eventGuestUserModel->errors());
					return false;
				}

				$notDeletedUserIdlist[] = $guestUserId;
			}
		}

		if ($this->scenario === self::SCENARIO_UPDATE) {
			$eventGuestUserModel->where('related_item', $tableName)
				->where('item_id', $id);

			if (count($notDeletedUserIdlist) > 0) {
				$eventGuestUserModel->whereNotIn('user_id', $notDeletedUserIdlist);
			}
			
			$eventGuestUserModel->delete();
		}

		return true;
	}

	protected function sendWsInvite()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();

		$wsData = [
			'server' => [
				'controller' => ucfirst($this->type),
				'action' => 'sendInvite',
				'userIdList' => $this->_wsSendInviteUserIdList,
				'data' => [
					$this->type . '_id' => $this->_id,
					'invite_from_id' => $this->user_id,
					'type' => $this->type,
				],
			],
		];

		if (!$wsClientComponent->sendData($wsData)) {
			$this->setError('', $wsClientComponent->error);
			$wsClientComponent->closeSocket();
			return false;
		}

		$wsClientComponent->closeSocket();

		return true;
	}

	protected function sendWsNewTask()
	{
		$wsClientComponent = new WsClientComponent();
		$wsClientComponent->openSocket();
		$wsDataResponse = [
			'server' => [
				'controller' => 'MainDataUser',
				'action' => 'getTimeOffsetSeconds',
				'userId' => $this->_wsSendNewTaskUserId,
			]
		];

		$response = $wsClientComponent->sendDataWithResponse($wsDataResponse);

		if (!is_null($response->userId) && !is_null($response->timeOffsetSeconds)) {
			$startEntityDate = (int)$this->date + (int)$this->time;
			$endEntityDate = (int)$this->date + (int)$this->time + (int)$this->duration;
			$startDay = DateAndTime::getTimestampStartDay(time()) - $response->timeOffsetSeconds;
			$endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;

			if ((($startEntityDate <= time() && $endEntityDate >= time())
				|| ($startEntityDate >= time() && $startEntityDate <= $endDay))) {
				$subType = 'current';
			} else if ($startEntityDate > $endDay) {
				$subType = 'scheduled';
			} else if ($endEntityDate <= time()) {
				$subType = 'overdue';
			}

			$wsData = [
				'server' => [
					'controller' => 'Task',
					'action' => 'sendNewTask',
					'userId' => $this->_wsSendNewTaskUserId,
					'data' => [
						'subType' => $subType,
						'task_id' => $this->_id,
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$this->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();
				return false;
			}
		}

		$wsClientComponent->closeSocket();

		return true;
	}

	protected function saveTaskHistoryFile(TaskHistoryFileModel $taskHistoryFileModel, $taskHistoryId, $fileId)
	{
		$data = [
			'task_history_id' => $taskHistoryId,
			'file_id' => $fileId,
		];

		if (!$taskHistoryFileModel->save($data)) {
			return false;
		}

		return true;
	}
}