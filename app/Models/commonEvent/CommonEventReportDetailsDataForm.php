<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\ReportModel;
use Common\Models\ReportFileModel;
use Common\Models\FileModel;
use Common\Models\CommentModel;
use App\Libraries\DateAndTime\DateAndTime;

class CommonEventReportDetailsDataForm extends BaseForm
{
	const TYPE_EVENT = 1;
	const TYPE_MEETING = 2;

	public $user_id;
	public $entity_id;
	public $type;
	public $report_id;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2]',
		'report_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_entityModel;
	private $_result;

	protected function getTypeLabels()
	{
		return [
			self::TYPE_EVENT => 'event',
			self::TYPE_MEETING => 'meeting',
		];
	}

	public function getData()
	{
		if ((int)$this->type === self::TYPE_EVENT) {
			$this->_entityModel = new EventModel();
		}

		if ((int)$this->type === self::TYPE_MEETING) {
			$this->_entityModel = new MeetingModel();
		}

		$reportModel = new ReportModel();
		$commentModel = new CommentModel();
		$this->_result = $reportModel
			->select(ReportModel::table() . '.*')
			->select('entityTable.date, entityTable.time, entityTable.duration, entityTable.name')
			->join($this->_entityModel::table() . ' as entityTable', 'entityTable.' . self::getTypeLabels()[$this->type] . '_id = ' . ReportModel::table() . '.item_id and ' . ReportModel::table() . '.related_item = "' . $this->_entityModel::table() . '"')
			->where('report_id', $this->report_id)
			->where('entityTable.user_id', $this->user_id)
			->where('entityTable.' . self::getTypeLabels()[$this->type] . '_id', $this->entity_id)
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Report.Report not found'));
			return false;
		}

		$this->_result->fileList = $reportModel->getReportFileList($this->_result->report_id, $this->user_id, true);
		$this->_result->commentData = $commentModel->getCommentData(CommentModel::TYPE_TABLE_REPORT, $this->_result->report_id, $this->timeOffsetSeconds);

		return true;
	}

	public function getResult()
	{
		if (empty($this->_result->name)) {
			$this->_result->name = lang('Main.No title');
		}

		$startEntityDate = (int)$this->_result->date + (int)$this->_result->time;
		$endEntityDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$this->_result->date = DateAndTime::getDateTime($startEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->endDate = DateAndTime::getDateTime($endEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');

		return [
			'report' => $this->_result,
			'typeLabel' => self::getTypeLabels()[$this->type],
			'entityId' => $this->entity_id,
		];
	}
}