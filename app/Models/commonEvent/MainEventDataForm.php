<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\TaskModel;

class MainEventDataForm extends BaseForm
{
	const TYPE_MAIN = 1;
	const TYPE_EVENT = 2;
	const TYPE_MEETING = 3;
	const TYPE_TASK = 4;


	public $user_id;
	public $type;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2,3,4]',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_result;

	public function getData()
	{
		if ((int)$this->type === self::TYPE_MAIN) {
			$eventModel = new EventModel();
			$meetingModel = new MeetingModel();
			$taskModel = new TaskModel();
			$this->_result['eventDataList'] = $eventModel->getEventDataList($this->user_id, $this->timeOffsetSeconds);
			$this->_result['eventDataList']['invitations'] = $eventModel->getActiveInvite($this->user_id);
			$this->_result['meetingDataList'] = $meetingModel->getMeetingDataList($this->user_id, $this->timeOffsetSeconds);
			$this->_result['meetingDataList']['invitations'] = $meetingModel->getActiveInvite($this->user_id);
			$this->_result['taskDataList'] = $taskModel->getTaskDataList($this->user_id, $this->timeOffsetSeconds);
			$this->_result['taskNotViewedCountdata'] = $taskModel->getNotViewedTaskCountFullData($this->user_id, $this->timeOffsetSeconds);
		}

		if ((int)$this->type === self::TYPE_EVENT) {
			$eventModel = new EventModel();
			$this->_result['eventDataList'] = $eventModel->getEventDataList($this->user_id, $this->timeOffsetSeconds);
			$this->_result['eventDataList']['invitations'] = $eventModel->getActiveInvite($this->user_id);
		}

		if ((int)$this->type === self::TYPE_MEETING) {
			$meetingModel = new MeetingModel();
			$this->_result['meetingDataList'] = $meetingModel->getMeetingDataList($this->user_id, $this->timeOffsetSeconds);
			$this->_result['meetingDataList']['invitations'] = $meetingModel->getActiveInvite($this->user_id);
		}

		if ((int)$this->type === self::TYPE_TASK) {
			$taskModel = new TaskModel();
			$this->_result['taskDataList'] = $taskModel->getTaskDataList($this->user_id, $this->timeOffsetSeconds);
			$this->_result['taskNotViewedCountdata'] = $taskModel->getNotViewedTaskCountFullData($this->user_id, $this->timeOffsetSeconds);
		}

		return true;
	}

	public function getResult()
	{
		$this->_result['type'] = $this->type;
		return $this->_result;
	}
}