<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Models\FileModel;
use Common\Models\UserModel;
use App\Libraries\DateAndTime\DateAndTime;

class CommonEventInvitationDetailsForm extends BaseForm
{
	const TYPE_EVENT = 1;
	const TYPE_MEETING = 2;

	const TYPE_EVENT_LABEL = 'event';
	const TYPE_MEETING_LABEL = 'meeting';

	public $user_id;
	public $entity_id;
	public $type;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
		'type' => 'required|integer|in_list[1,2,3]',
	];

	protected $cleanValidationRules = false;

	private $_table;
	private $_type;
	private $_entity;
	private $_result;

	public static function getTypeLabels()
	{
		return [
			self::TYPE_EVENT => self::TYPE_EVENT_LABEL,
			self::TYPE_MEETING => self::TYPE_MEETING_LABEL,
		];
	}

	public function getDetails()
	{
		if ((int)$this->type === self::TYPE_EVENT) {
			$eventModel = new EventModel();
			$this->_entity = $eventModel;
			$this->_table = EventModel::table();
			$this->_type = self::getTypeLabels()[$this->type];	
		}

		if ((int)$this->type === self::TYPE_MEETING) {
			$meetingModel = new MeetingModel();
			$this->_entity = $meetingModel;
			$this->_table = MeetingModel::table();
			$this->_type = self::getTypeLabels()[$this->type];	
		}

		$this->_getEntity();

		if (empty($this->_entity)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		$this->_result['entity'] = $this->_entity;
		$this->_result['entity']->type = $this->_type;

		$this->_result['entity']->guestList = $this->_getGuestData();

		return true;
	}

	public function getResult()
	{
		$periodicityList = config('MainEventParamsConfig')->periodicity;
		$reminderList = config('MainEventParamsConfig')->duration;
		$startEntityDate = $this->_entity->date + $this->_entity->time;
		$endEntityDate = $this->_entity->date + $this->_entity->time + $this->_entity->duration;
		$this->_result['typeLabel'] = self::getTypeLabels()[$this->type];

		if ($endEntityDate <= time()) {
			$this->_result['entity']->statusEntity = COMPLETED;
		} else if ($startEntityDate <= time() && $endEntityDate >= time()) {
			$this->_result['entity']->statusEntity = CURRENT;
		} else if ($startEntityDate > time()) {
			$this->_result['entity']->statusEntity = SCHEDULED;
		}

		if (empty($this->_result['entity']->name)) {
			$this->_result['entity']->name = lang('Main.No title');
		}

		$this->_result['entity']->date = DateAndTime::getDateTime($startEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result['entity']->endDate = DateAndTime::getDateTime($endEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result['entity']->privacyValue = ((int)$this->_result['entity']->privacy === 1) ? lang('Event.private') : lang('Event.public');

		$this->_result['entity']->userAva = (!empty($this->_result['entity']->userAva))
				? FileModel::preparePathToAva($this->_result['entity']->userAva)
				: FileModel::DEFAULT_AVA_URL;

		return $this->_result;
	}

	private function _getEntity()
	{
		$this->_entity = $this->_entity
			->select($this->_table . '.*')
			->select('userTable.first_name, userTable.last_name')
			->select('fileTable.url as userAva')
			->select('selfGuest.status as selfGuestStatus')
			->join(EventGuestUserModel::table() . ' as selfGuest', 'selfGuest.item_id = ' . $this->_table . '.' . $this->_type . '_id and selfGuest.related_item = "' . $this->_table . '"')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . $this->_table . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where('selfGuest.user_id', $this->user_id)
			->where($this->_type . '_id', $this->entity_id)
			->first();
	}

	private function _getGuestData()
	{
		$eventGuestUserModel = new EventGuestUserModel();
		$result = [];
		$query = $eventGuestUserModel
			->select(EventGuestUserModel::table() . '.*')
			->select('concat(userTable.first_name, " ", userTable.last_name) as fullName')
			->select('fileTable.url as userAva')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . EventGuestUserModel::table() . '.user_id')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
			->where(EventGuestUserModel::table() . '.related_item', $this->_table)
			->where(EventGuestUserModel::table() . '.item_id', $this->_entity->{$this->_type . '_id'})
			->get();

		foreach ($query->getResultObject() as &$item) {
			$item->userAva = (!empty($item->userAva))
				? FileModel::preparePathToAva($item->userAva)
				: FileModel::DEFAULT_AVA_URL;

			$result[] = $item;
		}

		return $result;
	}
}