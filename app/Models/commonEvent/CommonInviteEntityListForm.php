<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\UserModel;
use Common\Models\EventGuestUserModel;
use App\Libraries\DateAndTime\DateAndTime;
use CodeIgniter\Database\BaseBuilder;

class CommonInviteEntityListForm extends BaseForm
{
	public $user_id;
	public $profile_id;
	public $type;
	public $timeOffsetSeconds;
	public $rowNum; // last row in list
	public $timestamp; // timestamp for first query
	public $limit = 20;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'profile_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2]',
		'rowNum' => 'permit_empty|integer',
		'timestamp' => 'required|integer',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	protected $entityModel;
	protected $entityFieldId;
	protected $entityType;

	private $_entityCount;
	private $_result;

	public function getList()
	{
		$mainEventParamsConfig = config('MainEventParamsConfig');

		if ((int)$this->type === (int)$mainEventParamsConfig->mainTypeEvent) {
			$this->entityModel = new EventModel();
			$this->entityFieldId = 'event_id';
			$this->entityType = 'event';
		} else if ((int)$this->type === (int)$mainEventParamsConfig->mainTypeMeeting) {
			$this->entityModel = new MeetingModel();
			$this->entityFieldId = 'meeting_id';
			$this->entityType = 'meeting';
		}

		$this->_result = $this->entityModel
			->select($this->entityModel::table() . '.*')
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . $this->entityModel::table() . '" and eventGuestTable.item_id = ' . $this->entityModel::table() . '.' . $this->entityFieldId . ' and eventGuestTable.user_id = ' . $this->profile_id, 'left')
			->where($this->entityModel::table() . '.user_id', $this->user_id)
			->where('(' . $this->entityModel::table() . '.date + ' . $this->entityModel::table() . '.time + ' . $this->entityModel::table() . '.duration) >', time())
			->where($this->entityModel::table() . '.created_at <=', $this->timestamp)
			->where('eventGuestTable.event_guest_user_id is null')
			->orderBy('(' . $this->entityModel::table() . '.date + ' . $this->entityModel::table() . '.time + ' . $this->entityModel::table() . '.duration)', 'ASC');

		$this->_entityCount = $this->_result->countAllResults(false);

		$this->_result = $this->_result->getCompiledSelect();

		$this->_result = 'select (@rowNum := @rowNum + 1) as rowNum, query.* from (' . $this->_result . ') as query, (select (@rowNum := 0)) as rowNum';

		if (!empty($this->rowNum)) {
			$this->_result = 'select query.* from (' . $this->_result . ') as query where rowNum > ' . $this->entityModel->db->escape($this->rowNum);
		}

		$this->_result = 'select query.* from (' . $this->_result . ') as query limit ' . $this->limit;
		$this->_result = $this->entityModel->db->query('select query.* from (' . $this->_result . ') as query');

		return true;
	}

	public function getResult()
	{
		$result = [];
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$result['inviteEntityResult'] = [];
		$result['insert_type'] = !empty($this->rowNum) ? 'add' : 'renew';
		$result['timestamp'] = $this->timestamp;
		$result['entityCount'] = $this->_entityCount;
		$result['profile_id'] = $this->profile_id;
		$result['type'] = $this->type;
		$result['entityType'] = $this->entityType;
		$result['entityFieldId'] = $this->entityFieldId;
		$result['title'] = $this->entityFieldId;

		if ((int)$this->type === (int)$mainEventParamsConfig->mainTypeEvent) {
			$title = lang('Main.invite to event');
		} else if ((int)$this->type === (int)$mainEventParamsConfig->mainTypeMeeting) {
			$title = lang('Main.invite to a meeting');;
		}

		$result['title'] = $title;

		foreach ($this->_result->getResultObject() as $item) {
			$startDate = (int)$item->date + (int)$item->time;
			$endDate = (int)$item->date + (int)$item->time + (int)$item->duration;
			$item->date = DateAndTime::getDateTime($startDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
			$item->endDate = DateAndTime::getDateTime($endDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');

			if (empty($item->name)) {
				$item->name = lang('Main.No title');
			}

			$result['inviteEntityResult'][] = $item;
		}
// var_dump($result);die;
		return $result;
	}
}