<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\ReportModel;
use Common\Models\ReportFileModel;
use Common\Models\FileModel;

class CommonEventSaveReportForm extends BaseForm
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_EDIT = 'edit';

	const TYPE_EVENT = 1;
	const TYPE_MEETING = 2;

	public $user_id;
	public $type;
	public $report_video;
	public $report_photo;
	public $note;
	public $report_id;
	public $entity_id;
	public $deleteFileIdList;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'type' => 'required|integer|in_list[1,2]',
		'report_video' => 'permit_empty|max_size[report_video,20000]|ext_in[report_video,mp4]|mime_in[report_video,video/mp4]',
		'report_photo.*' => 'permit_empty|uploaded[report_photo]|is_image[report_photo]|ext_in[report_photo,png,jpeg,jpg,gif]|mime_in[report_photo,image/png,image/jpg,image/jpeg,image/gif]',
		'note' => 'permit_empty|string|max_length[65000]',
		'report_id' => 'permit_empty|integer',
		'entity_id' => 'required|integer',
		'deleteFileIdList' => 'permit_empty',
	];

	protected $cleanValidationRules = false;

	private $_entityModel;
	private $_entity;
	private $_reportData;
	private $_reportFileModel;
	private $_existFileList = ['videoList' => [], 'imageList' => []];

	protected function getTypeLabels()
	{
		return [
			self::TYPE_EVENT => 'event',
			self::TYPE_MEETING => 'meeting',
		];
	}

	public function save()
	{
		$reportModel = new ReportModel();
		$fileModel = new FileModel();
		$this->_reportFileModel = new ReportFileModel();
		$this->getEntityModel();

		$this->_entity = $this->_entityModel
			->where(self::getTypeLabels()[$this->type] . '_id', $this->entity_id)
			->where('user_id', $this->user_id)
			->first();

		if (empty($this->_entity)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		if ($this->scenario === self::SCENARIO_CREATE) {
			$existReport = $reportModel
				->where('related_item', $this->_entityModel::table())
				->where('item_id', $this->entity_id)
				->first();

			if (!empty($existReport)) {
				$this->setError('', lang('Report.Report already exists'));
				return false;
			}

			if (empty($this->note) && empty($this->report_photo) && empty($this->report_video)) {
				$this->setError('', lang('Report.Unable to save report with blank data'));
				return false;
			}

			$this->_reportData = (object)[
				'related_item' => $this->_entityModel::table(),
				'item_id' => $this->_entity->{self::getTypeLabels()[$this->type] . '_id'},
				'note' => $this->note,
			];

			$reportModel->db->transBegin();

			if (!$reportModel->save($this->_reportData)) {
				$this->setError('', lang('Report.Failed to save report'));
				$reportModel->db->transRollback();
				return false;
			}

			$this->_reportData->report_id = $reportModel->getInsertID();
		}

		if ($this->scenario === self::SCENARIO_EDIT) {
			if (empty($this->report_id)) {
				$this->setError('', lang('Validation.The "report_id" field is required'));
				return false;
			}

			$this->_reportData = $reportModel
				->where('related_item', $this->_entityModel::table())
				->where('item_id', $this->_entity->{self::getTypeLabels()[$this->type] . '_id'})
				->where('report_id', $this->report_id)
				->first();

			if (empty($this->_reportData)) {
				$this->setError('', lang('Report.Report not found'));
				return false;
			}

			$reportFileListCount = $this->_reportFileModel
				->join($reportModel::table() . ' as reportTable', 'reportTable.report_id = ' . $this->_reportFileModel::table() . '.report_id')
				->where('reportTable.report_id', $this->_reportData->report_id)
				->countAllResults();

			$this->deleteFileIdList = !empty($this->deleteFileIdList)
				? explode(',', $this->deleteFileIdList)
				: [];

			if (empty($this->note) && empty($this->report_photo) && empty($this->report_video)
				&& count($this->deleteFileIdList) >= $reportFileListCount) {
				$this->setError('', lang('Report.Unable to save report with blank data'));
				return false;
			}

			$this->_reportData->note = $this->note;

			$reportModel->db->transBegin();

			if (!$reportModel->save($this->_reportData)) {
				$this->setError('', lang('Report.Failed to save report'));
				$reportModel->db->transRollback();
				return false;
			}

			if (!empty($this->deleteFileIdList)) {
				$deleteQuery = $fileModel
					->join(ReportFileModel::table() . ' as reportFileTable', 'reportFileTable.file_id = ' . FileModel::table() . '.file_id and reportFileTable.report_id = ' . $this->_reportData->report_id)
					->whereIn(FileModel::table() . '.file_id', $this->deleteFileIdList)
					->get();

				foreach ($deleteQuery->getResultObject() as $item) {
					if (!$fileModel->delete($item->file_id)) {
						$this->setError('', lang('File.Failed to delete file'));
						$reportModel->db->transRollback();
						return false;
					}
				}
			}

			$this->_existFileList = $reportModel->getReportFileList($this->_reportData->report_id, $this->user_id, true);
		}

		if ((int)$this->_entity->privacy === $this->_entityModel::PRIVATE_STATUS) {
			$permittedIdList = $this->_entityModel->getGuestList($this->entity_id)['userIdList'];
		}

		if (!empty($this->report_video)) {
			if ((1 + count($this->_existFileList['videoList'])) > config('MainEventParamsConfig')->reportMaxVideo) {
				$this->setError('', lang('Main.Max video files {0}', [config('MainEventParamsConfig')->reportMaxVideo]));
				$taskModel->db->transRollback();
				return false;
			}

			$fileDataDisk = FileModel::saveFileOnDisk($this->report_video, FileModel::TYPE_VIDEO);
			$fileData = [
				'type' => FileModel::TYPE_VIDEO,
				'related_item' => 'user',
				'item_id' => $this->user_id,
				'url' => $fileDataDisk['fileName'],
				'privacy_status' => FileModel::getPrivacyByEvent()[$this->_entity->privacy],
				'mime_type' => $fileDataDisk['mimeType'],
				'original_name' => $fileDataDisk['originalName'],
				'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
				'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
			];

			if (!$fileModel->save($fileData)) {
				$this->setError('', $fileModel->errors());
				$reportModel->db->transRollback();
				return false;
			}

			$fileId = $fileModel->getInsertID();

			if ((int)$this->_entity->privacy === $this->_entityModel::PRIVATE_STATUS) {
				$fileModel->setFilePermission($fileId, $this->user_id, $permittedIdList);
			}

			if (!$this->saveReportFile($this->_reportData->report_id, $fileId)) {
				$this->setError('', lang('Report.Failed to save report file'));
				$reportModel->db->transRollback();
				return false;
			}
		}

		if (!empty($this->report_photo)) {
			if ((count($this->report_photo) + count($this->_existFileList['imageList']))
				> config('MainEventParamsConfig')->reportMaxImage) {
				$this->setError('', lang('Main.Max images {0}', [config('MainEventParamsConfig')->reportMaxImage]));
				$taskModel->db->transRollback();
				return false;
			}

			foreach ($this->report_photo as $file) {
				$fileDataDisk = FileModel::saveFileOnDisk($file, FileModel::TYPE_IMAGE);
				$fileData = [
					'type' => FileModel::TYPE_IMAGE,
					'related_item' => 'user',
					'item_id' => $this->user_id,
					'url' => $fileDataDisk['fileName'],
					'privacy_status' => FileModel::getPrivacyByEvent()[$this->_entity->privacy],
					'mime_type' => $fileDataDisk['mimeType'],
					'original_name' => $fileDataDisk['originalName'],
					'preview_height' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewHeight'] : '',
					'preview_width' => isset($fileDataDisk['imageData']) ? $fileDataDisk['imageData']['previewWidth'] : '',
				];

				if (!$fileModel->save($fileData)) {
					$this->setError('', $fileModel->errors());
					$reportModel->db->transRollback();
					return false;
				}

				$fileId = $fileModel->getInsertID();

				if ((int)$this->_entity->privacy === $this->_entityModel::PRIVATE_STATUS) {
					$fileModel->setFilePermission($fileId, $this->user_id, $permittedIdList);
				}

				if (!$this->saveReportFile($this->_reportData->report_id, $fileId)) {
					$this->setError('', lang('Report.Failed to save report file'));
					$reportModel->db->transRollback();
					return false;
				}
			}
		}

		$reportModel->db->transCommit();
		return true;
	}

	public function getResult()
	{
		return $this->_reportData;
	}

	protected function getEntityModel()
	{
		if ((int)$this->type === self::TYPE_EVENT) {
			$this->_entityModel = new EventModel();
		}

		if ((int)$this->type === self::TYPE_MEETING) {
			$this->_entityModel = new MeetingModel();
		}
	}

	protected function saveReportFile($reportId, $fileId)
	{
		$data = [
			'report_id' => $reportId,
			'file_id' => $fileId,
		];

		if (!$this->_reportFileModel->save($data)) {
			return false;
		}

		return true;
	}
}