<?php

namespace App\Models\commonEvent;

use Common\Forms\BaseForm;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\ReportModel;
use App\Libraries\DateAndTime\DateAndTime;

class CommonEventSaveReportDataForm extends BaseForm
{
	const TYPE_EVENT = 1;
	const TYPE_MEETING = 2;

	const ACTION_CREATE = 'create';
	const ACTION_EDIT = 'edit';

	public $user_id;
	public $entity_id;
	public $report_id;
	public $type;
	public $action;
	public $timeOffsetSeconds;

	protected $validationRules = [
		'user_id' => 'required|integer',
		'entity_id' => 'required|integer',
		'report_id' => 'permit_empty|integer',
		'type' => 'required|integer|in_list[1,2]',
		'action' => 'required|string',
		'timeOffsetSeconds' => 'required|integer',
	];

	protected $cleanValidationRules = false;

	private $_entityModel;
	private $_report;
	private $_result;

	protected function getTypeLabels()
	{
		return [
			self::TYPE_EVENT => 'event',
			self::TYPE_MEETING => 'meeting',
		];
	}

	public function getData()
	{
		if ((int)$this->type === self::TYPE_EVENT) {
			$this->_entityModel = new EventModel();
		}

		if ((int)$this->type === self::TYPE_MEETING) {
			$this->_entityModel = new MeetingModel();
		}

		$this->_result = $this->_entityModel
			->where('user_id', $this->user_id)
			->where(self::getTypeLabels()[$this->type] . '_id', $this->entity_id)
			->first();

		if (empty($this->_result)) {
			$this->setError('', lang('Main.Entity not found'));
			return false;
		}

		if ($this->action === self::ACTION_EDIT) {
			$reportModel = new ReportModel();
			$this->_report = $reportModel
				->where('related_item', $this->_entityModel::table())
				->where('item_id', $this->_result->{self::getTypeLabels()[$this->type] . '_id'})
				->first();

			if (empty($this->_report)) {
				$this->setError('', lang('Report.Report not found'));
				return false;
			}

			$this->_report->fileList = $reportModel->getReportFileList($this->_report->report_id, $this->user_id, true);
		}

		return true;
	}

	public function getResult()
	{
		if (empty($this->_result->name)) {
			$this->_result->name = lang('Main.No title');
		}

		$startEntityDate = (int)$this->_result->date + (int)$this->_result->time;
		$endEntityDate = (int)$this->_result->date + (int)$this->_result->time + (int)$this->_result->duration;
		$this->_result->date = DateAndTime::getDateTime($startEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');
		$this->_result->endDate = DateAndTime::getDateTime($endEntityDate, $this->timeOffsetSeconds, 'genetive', 'onlyDate');

		return [
			'entity' => $this->_result,
			'entity_id' => $this->_result->{self::getTypeLabels()[$this->type] . '_id'},
			'formAction' => '/' . self::getTypeLabels()[$this->type] . '/' . $this->action . '-report/' . $this->entity_id,
			'typeLabel' => self::getTypeLabels()[$this->type],
			'action' => $this->action,
			'report' => $this->_report,
			'contentTitle' => ($this->action === self::ACTION_CREATE)
				? lang('Report.create ' . $this->getTypeLabels()[$this->type] . ' report')
				: lang('Report.edit ' . $this->getTypeLabels()[$this->type] . ' report'),
		];
	}
}