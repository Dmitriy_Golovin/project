<?php

$newInviteEvent = (count($cabinetData['event']) > 0)
	? '<span class="new_invite_amount">' . count($cabinetData['event']) . '</span>'
	: '';

$newInviteMeeting = (count($cabinetData['meeting']) > 0)
	? '<span class="new_invite_amount">' . count($cabinetData['meeting']) . '</span>'
	: '';

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/feedback_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box">
    		<?= $this->include('templates/error/single') ?>
			<div class="feedback_header"><?= lang('Main.feedback') ?></div>

			<?php if (!empty($successfullySent) && $successfullySent) : ?>

				<div class="feedback_successfully_sent"><?= lang('Main.Message successfully sent') ?></div>

			<?php endif ?>

			<form action="/feedback" method="post">
				<div class="feedback_field_row">
                    <div class="feedback_field_label">email</div>
                    <div class="feedback_field_value">
                        <input type="text" name="email" class="feedback_field" placeholder="<?= lang('Main.enter email') ?>" maxlength="150" value="<?= $model->email ?>">
                    </div>
                </div>
                <div class="feedback_field_row align_top">
                    <div class="feedback_field_label"><?= lang('Main.message text') ?></div>
                    <div class="feedback_field_value">
                        <textarea name="text" class="feedback_textarea" rows="16" placeholder="<?= lang('Main.enter your message text') ?>"><?= $model->text ?></textarea>
                    </div>
                </div>
                <div class="feedback_button_container">
                    <input type="submit" class="feedback_button_submit" value="<?= lang('Main.send') ?>">
                </div>
			</form>
		</div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <!-- <script src="/js/setNavTableHeaderContainerDimensions.js"></script> -->
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_feedback.js"></script>
    <script src="/js/keyCRForm.js"></script>
<?= $this->endSection() ?>