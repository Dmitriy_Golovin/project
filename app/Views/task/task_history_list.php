<?php if ($insert_type === 'renew') : ?>

	<div class="task_history_query_search" data-itemCount="<?= esc($itemCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($taskHistoryResult)) : ?>
		<h3 class="no_events_title"><?= esc(lang('Main.Nothing found')) ?></h3>
	<?php endif ?>

<?php endif ?>

<?php foreach ($taskHistoryResult as $item) : ?>

	<?php $this->setVar('item', $item) ?>
	<?= $this->include('task/task_history_item') ?>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>
	
<?php endif ?>