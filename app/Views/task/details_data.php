<?php

use Common\Models\TaskModel;

?>

<div class="back_to_event_list_container">
	<span class="back_to_event_list">
		<a href="/task/<?= $entity->backHrefList ?>"><?= lang('Main.back to list') ?></a>
	</span>
</div>
<div class="entity_header">
	<?= lang('Task.task') ?>
</div>
<div class="entity_button_container">
	<?= $this->include('/task/task_details_button') ?>
</div>
<div class="entity_name_container"><?= $entity->name ?></div>
<div class="entity_date_container">
	<div class="entity_date_box">
		<div class="entity_date_start">
			<?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
		</div>
		<div class="date_hyphen">-</div>
		<div class="entity_date_end">
			<?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
		</div>
	</div>
</div>
<div class="entity_data_list_container">
	<div class="entity_data_row entity_data_row_status">
		<div class="entity_data_row_item_label"><?= lang('Task.status') ?></div>
		<div class="entity_data_row_item_value<?= $entity->statusCssClass ?>"><?= $entity->statusValue ?></div>
	</div>
	<?php if (in_array($entity->entityStatus, [COMPLETED, CREATED])) : ?>

		<div class="entity_data_row entity_data_row_review_status">
			<div class="entity_data_row_item_label"><?= lang('Task.review status') ?></div>
			<div class="entity_data_row_item_value<?= TaskModel::getReviewStatusCssClass()[$entity->review_status] ?>"><?= TaskModel::getReviewStatusLabel()[$entity->review_status] ?></div>
		</div>

	<?php endif ?>

	<?php if (in_array($entity->entityStatus, [CREATED])) : ?>

		<div class="entity_data_row entity_data_row_view_by_assigned_status">
			<div class="entity_data_row_item_label"><?= lang('Task.viewed by executor') ?></div>
			<div class="entity_data_row_item_value<?= TaskModel::getViewedByExecutorCssClass()[$entity->is_viewed_assigned] ?>"><?= TaskModel::getViewedByExecutorStatusLabel()[$entity->is_viewed_assigned] ?></div>
		</div>

	<?php endif ?>
	<div class="entity_data_row entity_data_row_user">
		<div class="entity_data_row_item_label"><?= $entity->userDataLabel ?></div>
		<div class="entity_data_row_item_value">
			<div class="task_user_data_value_container">

				<?php if (!$entity->assignedUserDeleted) : ?>

					<img src="<?= $entity->userAva ?>" class="task_user_data_value_ava"></img>
					<div class="task_user_data_value_name"><?= $entity->first_name ?> <?= $entity->last_name ?></div>

				<?php endif ?>
				
			</div>
		</div>
	</div>
	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.privacy type') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->privacyValue ?></div>
	</div>
	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.reminder') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->reminderValue ?></div>
	</div>
	<?php if ($entity->entityStatus !== COMPLETED) : ?>

		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Event.repeat') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->periodicityValue ?></div>
		</div>

	<?php endif ?>

	<?php if (!empty($entity->description)) : ?>

	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Main.description') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->description ?></div>
	</div>

	<?php endif ?>
</div>
<div class="entity_fileList_container">

	<?php if (!empty($entity->fileList['videoList'])) : ?>

		<div class="task_history_item_video_container<?= (!empty($entity->fileList['imageList'])) ? ' mb_10' : '' ?>">
			<?php foreach ($entity->fileList['videoList'] as $file) : ?>
            
				<?php if (!empty($file->file_id)) : ?>

	                <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
	                    'file' => $file,
	                    'nameBlock' => false,
	                    'deleteBlock' => false,
	                ]) ?>

	            <?php else : ?>

	            	<?= view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []) ?>

	            <?php endif ?>

            <?php endforeach ?>
		</div>

	<?php endif ?>

	<?php if (!empty($entity->fileList['imageList'])) : ?>

		<div class="task_history_item_photo_container<?= (!empty($entity->fileList['documentList'])) ? ' mb_10' : '' ?>" data-task_history_id="<?= $entity->main_task_history_id ?>">
            <?php foreach ($entity->fileList['imageList'] as $file) : ?>
    
    			<?php if (!empty($file->file_id)) : ?>

	                <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
	                    'file' => $file,
	                    'deleteBlock' => false,
	                    'mainCssClass' => 'image_file_container_preview',
	                ]) ?>

	            <?php else : ?>

	            	<?= view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []) ?>

	            <?php endif ?>

            <?php endforeach ?>
		</div>

	<?php endif ?>

	<?php if (!empty($entity->fileList['documentList'])) : ?>

		<div class="task_history_item_document_container">
			<?php foreach ($entity->fileList['documentList'] as $file) : ?>
            
            	<?php if (!empty($file->file_id)) : ?>

	                <?= view_cell('\Common\Widgets\file\DocumentFileWidget::render', [
	                    'file' => $file,
	                    'deleteBlock' => false,
	                ]) ?>

	            <?php else : ?>

	            	<?= view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []) ?>

	            <?php endif ?>

            <?php endforeach ?>
		</div>

	<?php endif ?>

</div>
<div class="task_history_container">
	<div class="task_history_title_container">
		<span class="task_history_title"><?= lang('Task.history') ?></span>
		<span class="task_full_history_but">
			<a href="/task/history/<?= $entity->task_id ?>"><?= lang('TaskHistory.full history') ?></a>
		</span>
	</div>
	<div class="task_history_content">
		<?php foreach ($entity->history as $item) : ?>
			<?php $this->setVar('item', $item) ?>
			<?= $this->include('/task/task_history_item') ?>
		<?php endforeach ?>
	</div>
</div>
<?= $this->include('modal/notice') ?>