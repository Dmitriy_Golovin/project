<div class="event_filter_box">
	<div class="task_history_status_box">
		<label for="filter_task_history_status_field"><?= lang('TaskHistory.type') ?></label>
		<select id="filter_task_history_status_field">
			<option value=""><?= lang('TaskHistory.any') ?></option>

			<?php foreach ($historyStatusList as $index => $value) : ?>
				<option value="<?= $index ?>"><?= $value ?></option>
			<?php endforeach ?>

		</select>
	</div>
	<div class="filter_date_box">
		<span class="filter_date_label"><?= lang('Main.date') ?></span>
		<div class="filter_date_content">
			<div class="filter_date_item_label"><?= lang('Main.from') ?></div>
			<div class="filter_date_item">
				<div class="filter_date_item_value_from"></div>
				<img src="/img/icon_close_red.png" class="filter_date_clear_icon not_active">
				<img src="/img/icon_calendar.png" class="filter_date_icon">
			</div>
			<div class="filter_date_item_label"><?= lang('Main.to (inclusive)') ?></div>
			<div class="filter_date_item">
				<div class="filter_date_item_value_to"></div>
				<img src="/img/icon_close_red.png" class="filter_date_clear_icon not_active">
				<img src="/img/icon_calendar.png" class="filter_date_icon">
			</div>
		</div>
	</div>
	<div class="filter_button_box">
		<span class="filter_apply_button"><?= lang('Main.apply') ?></span>
	</div>
</div>