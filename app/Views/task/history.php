<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">
      	<div class="event_menu_container">
      		<div class="event_type_menu_container">
	      		<div class="event_type_block">
	      			<?= $contentTitle ?>
	      		</div>
	      		<div class="event_menu_close_container not_active">
		      		<div class="event_menu_close">
		      			<img alt="Close window" class="event_menu_close_icon" src="/img/icon_close_dark.png">
		      		</div>
		      	</div>
	      	</div>
      		
      		<?= $this->include('task/history-filter') ?>
      	</div>
      	<div class="event_content_container">
      		<span class="event_menu_icon_container">
      			<img class="event_menu_icon" src="/img/icon_burger_dark.png">
      		</span>
      		<div class="event_content_title"><?= $contentTitle ?></div>
      		<a href="<?= $previousUrl ?>">
      			<div class="task_name_container"><?= $taskName ?></div>
      		</a>
      		<div class="task_content_list" data-task_id="<?= $task_id ?>" data-source_id="<?= $source_id ?>">
      			
	      	</div>
      	</div>
	  </div>
    </div>
    <div class="calendar_container">
    	<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/Calendar/CalendarClass.js"></script>
    <script src="/js/fetch/fetchGetTaskHistoryList.js"></script>
    <script src="/js/main_task_history.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/showCommentWriteField.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchGetTaskHistoryListItem.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
<?= $this->endSection() ?>