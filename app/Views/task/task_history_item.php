<?php

$dataRowNum = (isset($item->rowNum)) ? 'data-rowNum="' . $item->rowNum . '"' : '';
$dataTaskHistoryId = (isset($item->task_history_id)) ? 'data-task_history_id="' . $item->task_history_id . '"' : '';

?>

<?php if ($item->scenario === 'create') : ?>

	<div class="task_history_item_container" data-task_history_id="<?= $item->task_history_id ?>" <?= $dataRowNum ?> <?= $dataTaskHistoryId ?>>

<?php endif ?>

	<div class="history_status_date_container">
		<?= $item->typeLabel ?> - <?= $item->date['date'] . ' ' . $item->date['time'] ?>
	</div>
	<?php if (!empty($item->user_id)) : ?>

		<div class="task_history_user_data_container">
			<a href="/profile/<?= $item->user_id ?>">
				<img src="<?= $item->userAva ?>" class="task_history_user_data_ava">
				<div class="task_history_user_data_name">
					<?= $item->first_name . ' ' . $item->last_name ?>
				</div>
			</a>
		</div>

	<?php endif ?>

	<?php if (!empty($item->fileList['videoList'])) : ?>

		<div class="task_history_item_video_container<?= (!empty($item->fileList['imageList'])) ? ' mb_10' : '' ?>">
			<?php foreach ($item->fileList['videoList'] as $file) : ?>
            
            	<?php if (!empty($file->file_id)) : ?>

	                <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
	                    'file' => $file,
	                    'nameBlock' => false,
	                    'deleteBlock' => false,
	                ]) ?>

	            <?php else : ?>

	            	<?= view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []) ?>

	            <?php endif ?>

            <?php endforeach ?>
		</div>

	<?php endif ?>

	<?php if (!empty($item->fileList['imageList'])) : ?>

		<div class="task_history_item_photo_container<?= (!empty($item->fileList['documentList'])) ? ' mb_10' : '' ?>">
            <?php foreach ($item->fileList['imageList'] as $file) : ?>
    
    			<?php if (!empty($file->file_id)) : ?>

	                <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
	                    'file' => $file,
	                    'deleteBlock' => false,
	                    'mainCssClass' => 'image_file_container_preview',
	                ]) ?>

	            <?php else : ?>

	            	<?= view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []) ?>

	            <?php endif ?>

            <?php endforeach ?>
		</div>

	<?php endif ?>

	<?php if (!empty($item->fileList['documentList'])) : ?>

		<div class="task_history_item_document_container">
			<?php foreach ($item->fileList['documentList'] as $file) : ?>
            
            	<?php if (!empty($file->file_id)) : ?>

	                <?= view_cell('\Common\Widgets\file\DocumentFileWidget::render', [
	                    'file' => $file,
	                    'deleteBlock' => false,
	                ]) ?>

	            <?php else : ?>

	            	<?= view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []) ?>

	            <?php endif ?>

            <?php endforeach ?>
		</div>

	<?php endif ?>

	<?php if (!empty($item->note)) : ?>

		<div class="task_history_note_container">
			<div class="task_history_note_label"><?= lang('TaskHistory.notes') ?>:</div>
			<div class=task_history_note_content_container>
				<?= $item->note ?>
			</div>
		</div>

	<?php endif ?>

<?php if ($item->scenario === 'create') : ?>

	</div>

<?php endif ?>