<?php

$mainEventParamsConfig = config('MainEventParamsConfig');
$taskHistoryMaxVideo = $mainEventParamsConfig->taskHistoryMaxVideo;
$taskHistoryMaxImage = $mainEventParamsConfig->taskHistoryMaxImage;
$taskHistoryMaxDocument = $mainEventParamsConfig->taskHistoryMaxDocument;

$hideLoadVideoLabelCssClass = '';
$hideLoadImageLabelCssClass = '';
$hideLoadDocumentLabelCssClass = '';
$photoContainerCssClass = '';
$documentContainerCssClass = '';
$taskHistoryNote = ($type === 'edit' && !empty($entity->taskHistory->note)) ? $entity->taskHistory->note : '';

if ($type === 'edit') {
    $hideLoadVideoLabelCssClass = (count($entity->taskHistory->fileList['videoList']) >= (int)$taskHistoryMaxVideo)
        ? ' not_active'
        : '';
    $hideLoadImageLabelCssClass = (count($entity->taskHistory->fileList['imageList']) >= (int)$taskHistoryMaxImage)
        ? ' not_active'
        : '';
    $hideLoadDocumentLabelCssClass = (count($entity->taskHistory->fileList['documentList']) >= (int)$taskHistoryMaxDocument)
        ? ' not_active'
        : '';
    $photoContainerCssClass = (count($entity->taskHistory->fileList['imageList']) > 0)
        ? ' mb_10'
        : '';
    $documentContainerCssClass = (count($entity->taskHistory->fileList['documentList']) > 0)
        ? ' mb_10'
        : '';
}

?>

<div class="entity_header">
    <?= $contentTitle ?>
</div>
<div class="entity_name_container"><?= $entity->name ?></div>
<div class="entity_date_container">
    <div class="entity_date_box">
        <div class="entity_date_start">
            <?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
        </div>
        <div class="date_hyphen">-</div>
        <div class="entity_date_end">
            <?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
        </div>
    </div>
</div>
<form id="task_history_action" action="<?= $formAction ?>" method="post" enctype="multipart/form-data">
    <fieldset class="fieldset_box mb_not">
        <input type="text" name="task_id" value="<?= $entity->task_id ?>" hidden>
        <input type="text" name="action" value="<?= $action ?>" hidden>
        <div class="create_task_history_field_row">
            <div class="create_task_history_field_label">
                <div class="note_label_container">
                    <?= lang('Main.video') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$taskHistoryMaxVideo]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="task_history_video_container">
                <?php if ($type === 'edit') : ?>

                    <?php foreach ($entity->taskHistory->fileList['videoList'] as $item) : ?>
            
                        <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
                            'file' => $item,
                            'nameBlock' => false,
                            'deleteBlock' => true,
                        ]) ?>

                    <?php endforeach ?>

                <?php endif ?>
            </div>
            <div class="create_task_history_field_value <?= $hideLoadVideoLabelCssClass ?>">
                <input type="file" id="load_task_history_video" name="task_history_video" class="load_file" data-error_max_value="<?= lang('Main.maximum {0}', [$taskHistoryMaxVideo]) ?>" hidden>
                <label for="load_task_history_video" class="create_task_history_load_video"><?= lang('Main.upload video') ?></label>
            </div>
            <div class="notice_elem"></div>
        </div>
        <div class="create_task_history_field_row">
            <div class="create_task_history_field_label create_task_history_field_label_photo">
                <div class="note_label_container">
                    <?= lang('Main.photo') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$taskHistoryMaxImage]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="task_history_photo_container<?= $photoContainerCssClass ?>">

                <?php if ($type === 'edit') : ?>

                    <?php foreach ($entity->taskHistory->fileList['imageList'] as $item) : ?>
            
                        <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
                            'file' => $item,
                            'deleteBlock' => true,
                            'mainCssClass' => 'image_file_container_preview',
                        ]) ?>

                    <?php endforeach ?>

                <?php endif ?>

            </div>
            <div class="create_task_history_field_value<?= $hideLoadImageLabelCssClass ?>">
                <input type="file" id="load_task_history_photo" name="task_history_photo[]" class="load_file" multiple="true" data-error_max_value="<?= lang('Main.maximum {0}', [$taskHistoryMaxImage]) ?>" hidden>
                <label for="load_task_history_photo" class="create_task_history_load_photo"><?= lang('Main.upload photo') ?></label>
            </div>
            <div class="notice_elem"></div>
        </div>
        <div class="create_task_history_field_row">
            <div class="create_task_history_field_label create_task_history_field_label_document">
                <div class="note_label_container">
                    <?= lang('Main.document') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$taskHistoryMaxDocument]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="task_history_document_container<?= $documentContainerCssClass ?>">

                <?php if ($type === 'edit') : ?>

                    <?php foreach ($entity->taskHistory->fileList['documentList'] as $item) : ?>
            
                        <?= view_cell('\Common\Widgets\file\DocumentFileWidget::render', [
                            'file' => $item,
                            'deleteBlock' => true,
                        ]) ?>

                    <?php endforeach ?>

                <?php endif ?>

            </div>
            <div class="create_task_history_field_value<?= $hideLoadDocumentLabelCssClass ?>">
                <input type="file" id="load_task_history_document" name="task_history_document[]" class="load_file" multiple="true" data-error_max_value="<?= lang('Main.maximum {0}', [$taskHistoryMaxDocument]) ?>" hidden>
                <label for="load_task_history_document" class="create_task_history_load_document"><?= lang('Main.upload document') ?></label>
            </div>
            <div class="notice_elem"></div>
        </div>
        <div class="create_task_history_field_row">
            <div contenteditable="true" class="task_history_note_field" data-placeholder="<?= lang('TaskHistory.add notes') ?>"><?= $taskHistoryNote ?></div>
        </div>
        <div class="create_task_history_button_box">
            <input type="button" class="create_task_history_button_save" value="<?= lang('Main.save') ?>">
        </div>
    </fieldset>
    <?php if ($type === 'edit') : ?>
        <input type="hidden" name="deleteFileIdList">
    <?php endif ?>
</form>