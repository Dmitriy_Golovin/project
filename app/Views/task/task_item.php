<?php

use Common\Models\TaskModel;

$notViewedCssClass = (isset($item->notViewed) && (bool)$item->notViewed) ? ' not_viewed_invite' : '';

?>

<div class="event_result_item<?= $notViewedCssClass ?>" <?= (isset($item->rowNum) ? 'data-rowNum="' . $item->rowNum . '"' : '') ?> data-task_id="<?= $item->task_id ?>" data-date_timestamp="<?= $item->dateTimestamp ?>">
	<div class="event_item_content">
		<a href="/task/<?= $item->detailsType ?>-details/<?= $item->task_id ?>">
			<div class="event_item_name"><?= $item->name ?></div>
		</a>
		<div class="event_item_content_block">
			<div class="event_item_date_start_container">
				<span class="event_item_label"><?= lang('Event.start') ?></span>
				<span class="event_item_value"><?= $item->date['date'] ?> <?= $item->date['time'] ?></span>
			</div>
			<div class="event_item_date_end_container">
				<span class="event_item_label"><?= lang('Event.end') ?></span>
				<span class="event_item_value"><?= $item->endDate['date'] ?> <?= $item->endDate['time'] ?></span>
			</div>
			<div class="event_item_privacy_container">
				<span class="event_item_label"><?= lang('Event.privacy type') ?></span>
				<span class="event_item_value"><?= $item->privacyValue ?></span>
			</div>

			<?php if (in_array($subType, ['scheduled', 'current', 'overdue', 'created'])) : ?>

				<div class="event_item_periodicity_container">
					<span class="event_item_label"><?= lang('Event.repeat') ?></span>
					<span class="event_item_value"><?= $item->periodicityValue ?></span>
				</div>

			<?php endif ?>

			<?php if (in_array($subType, ['scheduled', 'current', 'overdue', 'created'])) : ?>

				<div class="event_item_reminder_container">
					<span class="event_item_label"><?= lang('Event.reminder') ?></span>
					<span class="event_item_value"><?= $item->reminderValue ?></span>
				</div>

			<?php endif ?>

			<div class="task_item_user_container">
				<span class="event_item_label"><?= $item->ownerLabel ?></span>
				<div class="event_item_owner_value_container">

					<?php if (!$item->assignedUserDeleted) : ?>

						<img src="<?= $item->userAva ?>" class="event_item_owner_value_ava"></img>
						<div class="event_item_owner_value_name"><?= $item->first_name ?><br><?= $item->last_name ?></div>

					<?php endif ?>

				</div>
			</div>

			<?php if (in_array($subType, ['created'])) : ?>

				<div class="task_item_status_container">
					<span class="event_item_label"><?= lang('Task.task status') ?></span>
					<span class="event_item_value<?= TaskModel::getStatusCssClass()[$item->status] ?>"><?= TaskModel::getStatusLabel()[$item->status] ?></span>
				</div>

			<?php endif ?>

			<?php if (in_array($subType, ['completed', 'created'])) : ?>

				<div class="task_item_review_status_container">
					<span class="event_item_label"><?= lang('Task.review status') ?></span>
					<span class="event_item_value<?= TaskModel::getReviewStatusCssClass()[$item->review_status] ?>"><?= TaskModel::getReviewStatusLabel()[$item->review_status] ?></span>
				</div>

			<?php endif ?>
		</div>
	</div>
	<div class="event_item_menu">
		<div class="show_event_menu_container">
			<div class="show_event_menu_box">
				<span class="up_menu_box_item"></span>
				<span class="middle_menu_box_item"></span>
				<span class="bottom_menu_box_item"></span>
			</div>
		</div>
		<ul>
			<?php if (in_array($subType, ['current', 'scheduled', 'overdue'])) : ?>
				<li>
					<a href="/task/complete/<?= $item->task_id ?>">
						<img src="/img/selected_icon.png" title="<?= lang('Task.complete') ?>">
					</a>
				</li>
			<?php endif ?>

			<?php if (in_array($subType, ['completed'])) : ?>
				<li>
					<a href="/task/history/<?= $item->task_id ?>">
						<img src="/img/history_icon.png" title="<?= lang('TaskHistory.full history') ?>">
					</a>
				</li>
			<?php endif ?>

			<?php if (in_array($subType, ['created'])) : ?>
				<?php if ((int)$item->status === TaskModel::STATUS_COMPLETED) : ?>
					<?php if ((int)$item->review_status !== TaskModel::STATUS_ACCEPTED) : ?>
						<li class="entity_accept_task_button">
							<a href="/task/accept/<?= $item->task_id ?>">
								<img src="/img/selected_icon.png" title="<?= lang('Task.accept') ?>">
							</a>
						</li>
					<?php endif ?>
					<?php if ((int)$item->review_status === TaskModel::STATUS_NOT_REVIEWED || ((int)$item->review_status === TaskModel::STATUS_ACCEPTED && ($item->endEventTimestamp + DAY) > time())) : ?>
						<li class="entity_reject_task_button">
							<a href="/task/reject/<?= $item->task_id ?>">
								<img src="/img/icon_close_red_pad.png" title="<?= lang('Task.reject') ?>">
							</a>
						</li>
					<?php endif ?>
				<?php endif ?>
				<?php if ((int)$item->status !== TaskModel::STATUS_COMPLETED) : ?>
					<li>
						<a href="/task/edit/<?= $item->task_id ?>">
							<img src="/img/pencil_icon.png" title="<?= lang('Main.edit') ?>">
						</a>
					</li>
				<?php endif ?>
				<?php if ((int)$item->review_status !== TaskModel::STATUS_ACCEPTED) : ?>
					<li class="delete_event_item_icon_container">
						<img src="/img/cart_red_icon2.png" title="<?= lang('Main.delete') ?>">
					</li>
				<?php endif ?>
			<?php endif ?>
		</ul>
	</div>
</div>