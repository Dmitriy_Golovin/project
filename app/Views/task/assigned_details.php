<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_details_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box">
	    	<div class="event_data_container" data-type="<?= $type ?>" data-entity_id="<?= $entity_id ?>" data-details_type="<?= $detailsType ?>">
                <?php if (isset($model)) : ?>

                    <?= $this->include('templates/error/single') ?>

                <?php endif ?>
            </div>  	
		</div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/showTheResultsOfMatchingInputFromAvailableOnes.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetCommonEventDataDetails.js"></script>
    <script src="/js/main_event_details.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/showCommentWriteField.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchGetTaskHistoryListItem.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
    <script src="/js/fetch/fetchDeleteCommonEvent.js"></script>
<?= $this->endSection() ?>