<?php

$activeEmptyAssignedUserCssClass = (isset($entity) && !empty($entity->assigned_user_id))
    ? ' not_active'
    : '';

if (isset($entity) && !empty($entity->assigned_user_id)) {
    $guestUserIdListInputValue = $entity->assigned_user_id;
} else {
    $guestUserIdListInputValue = '';
}

$hideLoadVideoLabelCssClass = '';
$hideLoadImageLabelCssClass = '';
$hideLoadDocumentLabelCssClass = '';
$photoContainerCssClass = '';
$documentContainerCssClass = '';

if ($createType === 'edit') {
    $hideLoadVideoLabelCssClass = (count($entity->fileList['videoList']) >= (int)$maxVideo)
        ? ' not_active'
        : '';
    $hideLoadImageLabelCssClass = (count($entity->fileList['imageList']) >= (int)$maxImage)
        ? ' not_active'
        : '';
    $hideLoadDocumentLabelCssClass = (count($entity->fileList['documentList']) >= (int)$maxDocument)
        ? ' not_active'
        : '';
    $photoContainerCssClass = (count($entity->fileList['imageList']) > 0)
        ? ' mb_10'
        : '';
    $documentContainerCssClass = (count($entity->fileList['documentList']) > 0)
        ? ' mb_10'
        : '';
}

$dataAttrTaskHistoryId = ($createType === 'edit')
    ?'data-task_history_id="' . $entity->task_history_id . '"'
    : '';

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_details_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box" data-type="<?= $type ?>" data-create_type="<?= $createType ?>" <?= $dataAttrTaskHistoryId ?>>
            <?= $this->include('templates/error/single') ?>
	    	<div class="entity_header">
                <?= $contentTitle ?>
            </div>
            <form action="<?= $formAction ?>" method="post" enctype="multipart/form-data">
                <fieldset class="fieldset_box mb_not">
                    <div class="create_event_field_row">
                        <div class="create_event_field_label">
                            <?= lang('Main.name') ?>
                        </div>
                        <div class="create_event_field_value">
                            <input type="text" name="name" class="create_event_field" placeholder="<?= lang('Main.enter a name') ?>" maxlength="150" value="<?= (isset($entity) && !empty($entity->name)) ? $entity->name : '' ?>">
                        </div>
                    </div>
                    <div class="create_event_field_row align_top">
                        <div class="create_event_field_label">
                            <?= lang('Event.date and time') ?>
                        </div>
                        <div class="create_event_field_value">
                            <div class="create_event_date_field_container">
                                <div class="create_event_date_field"></div>
                                <input type="text" name="date" value="<?= (isset($entity) && !empty($entity->date)) ? $entity->date . '000' : '' ?>" hidden>
                                <div class="calendar_icon_container">
                                    <img src="/img/icon_calendar.png" class="calendar_icon">
                                </div>
                                <div class="create_event_time_field"></div>
                                <input type="text" name="time" value="<?= (isset($entity) && !empty($entity->time)) ? $entity->time . '000' : '' ?>" hidden>
                                <div class="time_icon_container">
                                    <img src="/img/icon_time.png" class="time_icon">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="create_event_field_row">
                        <div class="create_event_field_label">
                            <?= lang('Event.duration') ?>
                        </div>
                        <div class="create_event_field_value">
                            <select name="duration" class="create_event_field" value="<?= (isset($entity) && !empty($entity->duration)) ? $entity->duration : '' ?>">
                                <?php
                                foreach ($durationList as $index => $value) {
                                ?>
                                    <option class="option_create_new_event" value="<?= esc($index); ?>"<?= (isset($entity) && (int)$index === (int)$entity->duration) ? 'selected' : '' ?>><?= esc(lang('TimeAndDate.' . $value)); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="create_event_field_row align_top">
                        <div class="create_event_field_label">
                            <?= lang('Event.privacy type') ?>
                        </div>
                        <div class="create_event_field_value column_field_list">
                            <span>
                                <input type="radio" id="public_field" name="privacy" <?= (isset($entity) && (int)$entity->privacy === 0) ? 'checked' : 'checked' ?> value="0">
                                <label for="public_field"><?= lang('Event.public') ?></label>
                            </span>
                            <span>
                                <input type="radio" id="private_field" name="privacy" <?= (isset($entity) && (int)$entity->privacy === 1) ? 'checked' : '' ?> value="1">
                                <label for="private_field"><?= lang('Event.private') ?></label>
                            </span>
                        </div>
                    </div>
                    <div class="create_event_field_row align_top periodicity_container">
                        <div class="create_event_field_label">
                            <?= lang('Event.repeat') ?>
                        </div>
                        <div class="create_event_field_value">
                            <select name="periodicity" class="create_event_field">
                                <?php
                                foreach ($periodicityList as $index => $value) {
                                ?>
                                    <option class="option_create_new_event" value="<?= esc($index); ?>" <?= (isset($entity) && (int)$index === (int)$entity->periodicity) ? 'selected' : '' ?>><?= esc(lang('TimeAndDate.' . $value)); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="create_event_field_row reminder_container">
                        <div class="create_event_field_label">
                            <?= lang('Event.reminder') ?>
                        </div>
                        <div class="create_event_field_value row_field_list">
                            <span class="remind_label">
                                <?= lang('Event.remind in') ?>
                            </span>
                            <span>
                                <select name="reminder" class="create_event_field">
                                    <?php
                                    foreach ($reminderList as $index => $value) {
                                    ?>
                                        <option class="option_create_new_event" value="<?= esc($index); ?>" <?= (isset($entity) && (int)$index === (int)$entity->reminder) ? 'selected' : '' ?>><?= esc(lang('TimeAndDate.' . $value)); ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="create_event_field_row align_top">
                        <div class="create_event_field_label">
                            <?= lang('Main.description') ?>
                        </div>
                        <div class="create_event_field_value">
                            <textarea name="description" class="create_event_textarea" rows="16" placeholder="<?= lang('Main.enter description') ?>" ><?= (isset($entity) && !empty($entity->description)) ? $entity->description : '' ?></textarea>
                        </div>
                    </div>
                    <div class="create_event_file_field_row">
                        <div class="create_event_file_field_label">
                            <div class="note_label_container">
                                <?= lang('Main.video') ?>
                                <span class="notice_icon_container">
                                    <img class="notice_icon" src="/img/notice_icon_2.png">
                                    <div class="notice_label_message not_active not_active_opacity">
                                        <?= lang('Main.maximum {0}', [$maxVideo]) ?>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="create_event_video_container">
                            <?php if ($createType === 'edit') : ?>

                                <?php foreach ($entity->fileList['videoList'] as $item) : ?>
                        
                                    <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
                                        'file' => $item,
                                        'nameBlock' => false,
                                        'deleteBlock' => true,
                                    ]) ?>

                                <?php endforeach ?>

                            <?php endif ?>
                        </div>
                        <div class="create_event_file_field_value <?= $hideLoadVideoLabelCssClass ?>">
                            <input type="file" id="<?= $type ?>_load_file_video" name="<?= $type ?>_video" class="load_file" data-error_max_value="<?= lang('Main.maximum {0}', [$maxVideo]) ?>" data-max_value="<?= $maxVideo ?>" hidden>
                            <label for="<?= $type ?>_load_file_video" class="create_task_history_load_video"><?= lang('Main.upload video') ?></label>
                        </div>
                        <div class="notice_elem"></div>
                    </div>
                    <div class="create_event_file_field_row">
                        <div class="create_event_file_field_label create_task_history_field_label_photo">
                            <div class="note_label_container">
                                <?= lang('Main.photo') ?>
                                <span class="notice_icon_container">
                                    <img class="notice_icon" src="/img/notice_icon_2.png">
                                    <div class="notice_label_message not_active not_active_opacity">
                                        <?= lang('Main.maximum {0}', [$maxImage]) ?>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="create_event_image_container<?= $photoContainerCssClass ?>">

                            <?php if ($createType === 'edit') : ?>

                                <?php foreach ($entity->fileList['imageList'] as $item) : ?>
                        
                                    <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
                                        'file' => $item,
                                        'deleteBlock' => true,
                                        'mainCssClass' => 'image_file_container_preview',
                                    ]) ?>

                                <?php endforeach ?>

                            <?php endif ?>

                        </div>
                        <div class="create_event_file_field_value<?= $hideLoadImageLabelCssClass ?>">
                            <input type="file" id="<?= $type ?>_load_file_image" name="<?= $type ?>_image[]" class="load_file" multiple="true" data-error_max_value="<?= lang('Main.maximum {0}', [$maxImage]) ?>" data-max_value="<?= $maxImage ?>" hidden>
                            <label for="<?= $type ?>_load_file_image" class="create_task_history_load_photo"><?= lang('Main.upload photo') ?></label>
                        </div>
                        <div class="notice_elem"></div>
                    </div>
                    <div class="create_event_file_field_row">
                        <div class="create_event_file_field_label create_task_history_field_label_document">
                            <div class="note_label_container">
                                <?= lang('Main.document') ?>
                                <span class="notice_icon_container">
                                    <img class="notice_icon" src="/img/notice_icon_2.png">
                                    <div class="notice_label_message not_active not_active_opacity">
                                        <?= lang('Main.maximum {0}', [$maxDocument]) ?>
                                    </div>
                                </span>
                            </div>
                        </div>
                        <div class="create_event_document_container<?= $documentContainerCssClass ?>">

                            <?php if ($createType === 'edit') : ?>

                                <?php foreach ($entity->fileList['documentList'] as $item) : ?>
                        
                                    <?= view_cell('\Common\Widgets\file\DocumentFileWidget::render', [
                                        'file' => $item,
                                        'deleteBlock' => true,
                                    ]) ?>

                                <?php endforeach ?>

                            <?php endif ?>

                        </div>
                        <div class="create_event_file_field_value<?= $hideLoadDocumentLabelCssClass ?>">
                            <input type="file" id="task_load_file_document" name="<?= $type ?>_document[]" class="load_file" multiple="true" data-error_max_value="<?= lang('Main.maximum {0}', [$maxDocument]) ?>" data-max_value="<?= $maxDocument ?>" hidden>
                            <label for="<?= $type ?>_load_file_document" class="create_task_history_load_document"><?= lang('Main.upload document') ?></label>
                        </div>
                        <div class="notice_elem"></div>
                    </div>
                    <input type="text" name="assigned_user_id" hidden value="<?= $guestUserIdListInputValue ?>">
                    <input type="text" name="timeOffsetSeconds" hidden>
                </fieldset>
                <div class="entity_guest_container">
                    <div class="entity_guest_title">
                        <span><?= lang('Task.executor') ?></span>
                        <span class="add_guest_event_but<?= $activeEmptyAssignedUserCssClass ?>"><?= lang('Task.assign an executor') ?></span>
                    </div>
                    <div class="entity_guest_content">
                        <h3 class="no_events_title<?= $activeEmptyAssignedUserCssClass ?>"><?= esc(lang('Main.list is empty')) ?></h3>

                        <?php if (isset($entity) && !empty($entity->assigned_user_id)) : ?>
                            <?php $user = $userModel->getUserData($entity->assigned_user_id); ?>

                            <div class="box_bottom_border user_result_item" data-profile_id="<?= esc($user->user_id) ?>">
                                <div class="ava_box_result_item">

                                    <?= anchor('profile/' . $user->user_id, img(['src' => $user->path_to_ava, 'class' => 'ava_result_item'])) ?>

                                </div>
                                <div class="fullname_box_result_item">
                                    <a href="/profile/<?= $user->user_id ?>"><?= esc($user->first_name . ' ' . $user->last_name) ?></a>
                                </div>
                                <div class="button_box_result_item">
                                    <span class="task_cancel_user_button"><?= esc(lang('Task.cancel')) ?></span>
                                </div>
                            </div>

                        <?php endif ?>
                        
                    </div>
                </div>
                <div class="create_event_button_container">
                    <input type="button" class="create_event_button_submit" value="<?= lang('Main.save') ?>">
                </div>
                <?php if ($type === 'task' && $createType === 'edit') : ?>
                    <input type="hidden" name="deleteTaskFileIdList">
                <?php endif ?>
            </form>
		</div>
    </div>
    <div class="calendar_container">
        <?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
    </div>
    <?php echo view_cell('\Common\Widgets\select_time\SelectTimeWidget::render', []); ?>
    <?= $this->include('templates/self_result_item_assign_task') ?>
    <?php $this->setVar('taskAssignSelf', true) ?>
    <?= $this->include('modal/add_guest_user') ?>
    <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderTemplateForLoad', []) ?>
    <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderTemplateForLoad', []) ?>
    <?= view_cell('\Common\Widgets\file\DocumentFileWidget::renderTemplateForLoad', []) ?>
    <?= $this->include('modal/slider_image_modal') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/showTheResultsOfMatchingInputFromAvailableOnes.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetUserList.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/Calendar/CalendarClass.js"></script>
    <script src="/js/SelectTime/SelectTimeClass.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageClass.js"></script>
    <script src="/js/checkUploadFile.js"></script>

    <?php if ($createType === 'edit') : ?>

        <script src="/js/fetch/fetchGetCommentList.js"></script>
        <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
        <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
        <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
        <script src="/js/getScrollWidth.js"></script>
        <script src="/js/showCommentWriteField.js"></script>

    <?php endif ?>

    <script src="/js/main_event_create.js"></script>
    <script src="/js/keyCRForm.js"></script>
<?= $this->endSection() ?>