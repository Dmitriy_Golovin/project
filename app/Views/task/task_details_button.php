<?php if ($entity->canEditReport['state']) : ?>
	<a href="<?= $entity->canEditReport['url'] ?>">
		<span class="task_edit_report_button"><?= lang('Task.edit report') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canCompleteTask) : ?>
	<a href="/task/complete/<?= $entity->task_id ?>">
		<span class="task_complete_button"><?= lang('Task.complete') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canEditTask) : ?>
	<a href="/task/edit/<?= $entity->task_id ?>">
		<span class="entity_edit_button"><?= lang('Main.edit') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canAcceptTask) : ?>
	<a href="/task/accept/<?= $entity->task_id ?>">
		<span class="entity_accept_task_button"><?= lang('Task.accept') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canEditAcceptTask['state']) : ?>
	<a href="<?= $entity->canEditAcceptTask['url'] ?>">
		<span class="entity_accept_task_button"><?= lang('Task.accept (edit)') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canRejectTask) : ?>
	<a href="/task/reject/<?= $entity->task_id ?>">
		<span class="entity_reject_task_button"><?= lang('Task.reject') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canEditRejectTask['state']) : ?>
	<a href="<?= $entity->canEditRejectTask['url'] ?>">
		<span class="entity_reject_task_button"><?= lang('Task.reject (edit)') ?></span>
	</a>
<?php endif ?>

<?php if ($entity->canDeleteTask) : ?>
	<span class="entity_delete_button"><?= lang('Main.delete') ?></span>
<?php endif ?>