<div class="entity_header">
    <?= lang('Report.event report') ?>
</div>
<div class="entity_button_container">
    <a href="/<?= $typeLabel ?>/edit-report/<?= $entityId ?>">
        <span class="entity_edit_button"><?= lang('Main.edit') ?></span>
    </a>
    <span class="entity_delete_button"><?= lang('Main.delete') ?></span>
</div>
<div class="entity_name_container"><?= $report->name ?></div>
<div class="entity_date_container">
    <div class="entity_date_box">
        <div class="entity_date_start">
            <?= $report->date['dayOfWeek'] . ' ' . $report->date['date'] . ' ' . $report->date['time'] ?>
        </div>
        <div class="date_hyphen">-</div>
        <div class="entity_date_end">
            <?= $report->endDate['dayOfWeek'] . ' ' . $report->endDate['date'] . ' ' . $report->endDate['time'] ?>
        </div>
    </div>
</div>
<div class="report_details_content_container">
    <div class="report_details_note_container"><?= $report->note ?></div>
    <div class="report_details_video_container">
        <?php foreach ($report->fileList['videoList'] as $item) : ?>
            
            <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
                'file' => $item,
                'nameBlock' => false,
                'deleteBlock' => false,
            ]) ?>

        <?php endforeach ?>
    </div>
    <div class="report_details_photo_container">
        <?php foreach ($report->fileList['imageList'] as $item) : ?>
            
            <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
                'file' => $item,
                'deleteBlock' => false,
                'mainCssClass' => 'image_file_container_preview',
            ]) ?>

        <?php endforeach ?>
    </div>
</div>
<div class="report_details_comment_container">
    <?= view_cell('\Common\Widgets\comment\CommentWidget::render', [
        'commentData' => $report->commentData,
        'writeField' => true,
    ]) ?>
</div>