<?php

use Common\Models\EventGuestUserModel;

if ($entity->statusEntity === SCHEDULED) {
	$statusCssClass = ' entity_status_scheduled';
	$statusValue = lang(ucfirst($entity->type) . '.scheduled');
	$backHrefList = 'scheduled';
} else if ($entity->statusEntity === CURRENT) {
	$statusCssClass = ' entity_status_current';
	$statusValue = lang(ucfirst($entity->type) . '.current');
	$backHrefList = '';
} else if ($entity->statusEntity === COMPLETED) {
	$statusCssClass = ' entity_status_completed';
	$statusValue = lang(ucfirst($entity->type) . '.completed');
	$backHrefList = 'completed';
}

?>

<div class="back_to_event_list_container">
	<span class="back_to_event_list">
		<a href="/<?= $entity->type ?>/<?= $backHrefList ?>"><?= lang('Main.back to list') ?></a>
	</span>
</div>
<div class="entity_header">
	<?= lang('Event.' . $entity->type) ?>
</div>
<div class="entity_button_container">

	<?php if ($entity->statusEntity === COMPLETED) : ?>

		<?php if (!empty($entity->report_id)) : ?>
			<a href="/event/report/<?= $entity->report_id ?>">
				<span class="entity_show_report_button"><?= lang('Event.show report') ?></span>
			</a>
		<?php else : ?>
			<a href="/<?= $entity->type ?>/create-report/<?= $entity->{$entity->type . '_id'} ?>">
				<span class="entity_add_report_button"><?= lang('Event.add report') ?></span>
			</a>
		<?php endif ?>

	<?php endif ?>

	<?php if ($entity->statusEntity !== COMPLETED) : ?>

		<a href="/<?= $entity->type ?>/edit/<?= $entity->{$entity->type . '_id'} ?>">
			<span class="entity_edit_button"><?= lang('Main.edit') ?></span>
		</a>

	<?php endif ?>

	<span class="entity_delete_button"><?= lang('Main.delete') ?></span>
</div>
<div class="entity_name_container"><?= $entity->name ?></div>
<div class="entity_date_container">
	<div class="entity_date_box">
		<div class="entity_date_start">
			<?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
		</div>
		<div class="date_hyphen">-</div>
		<div class="entity_date_end">
			<?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
		</div>
	</div>
</div>
<div class="entity_data_list_container">
	<div class="entity_data_row entity_data_row_status">
		<div class="entity_data_row_item_label"><?= lang('Event.status') ?></div>
		<div class="entity_data_row_item_value<?= $statusCssClass ?>"><?= $statusValue ?></div>
	</div>

	<?php if ($entity->statusEntity !== COMPLETED) : ?>

		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Event.repeat') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->periodicityValue ?></div>
		</div>

	<?php endif ?>

	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.privacy type') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->privacyValue ?></div>
	</div>
	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.reminder') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->reminderValue ?></div>
	</div>

	<?php if (!empty($entity->location)) : ?>

		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Event.location') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->location ?></div>
		</div>

	<?php endif ?>

	<?php if (!empty($entity->description)) : ?>

		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Main.description') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->description ?></div>
		</div>

	<?php endif ?>
</div>

<div class="entity_guest_container">
	<div class="entity_guest_title"><?= lang('Event.participants') ?></div>
	<div class="entity_guest_content">
		<?php if (empty($entity->guestList)) : ?>
			<h3 class="no_events_title"><?= esc(lang('Main.list is empty')) ?></h3>
		<?php else : ?>
			<?php foreach ($entity->guestList as $item) : ?>

				<div class="box_bottom_border user_result_item" data-profile_id="<?= $item->user_id ?>">
					<div class="ava_box_result_item">
						<a href="/profile/<?= $item->user_id ?>">
							<img src="<?= $item->userAva ?>" class="ava_result_item" alt="">
						</a>
					</div>
					<div class="fullname_box_result_item">
						<a href="/profile/<?= $item->user_id ?>">
							<?= $item->fullName ?>
						</a>
					</div>

					<?php if ((int)$item->status === EventGuestUserModel::STATUS_ACCEPTED) {
						$statusGuestCssClass = ' invite_status_accept';
					} elseif ((int)$item->status === EventGuestUserModel::STATUS_DECLINED) {
						$statusGuestCssClass = ' invite_status_decline';
					} elseif ((int)$item->status === EventGuestUserModel::STATUS_VIEWED) {
						$statusGuestCssClass = ' invite_status_viewed';
					} else {
						$statusGuestCssClass = '';
					} ?>

					<div class="guest_status<?= $statusGuestCssClass ?>">
						<?= EventGuestUserModel::getStatusLabels()[$item->status] ?>
					</div>
				</div>

			<?php endforeach ?>
		<?php endif ?>
	</div>
</div>
<?= $this->include('modal/notice') ?>