<?php

use Common\Models\FriendModel;

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_self_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      	<div class="main_box" data-type="<?= $menuType ?>">
		 	 <div class="profile_submenu_button_block">

		 	 	<?php if ((int)$menuType !== FriendModel::TYPE_FRIENDS) : ?>
		 	 		<a href="/friends">
		 	 	<?php endif ?>

		 	 	<span class="profile_submenu_button profile_submenu_button_friends<?= ((int)$menuType === FriendModel::TYPE_FRIENDS) ? ' profile_submenu_button_active' : '' ?>"><?= lang('Main.friends') . ' ' . $friendsAmount->friends ?></span>

		 	 	<?php if ((int)$menuType !== FriendModel::TYPE_FRIENDS) : ?>
		 	 		</a>
		 	 	<?php endif ?>

		 	 	<?php if ((int)$menuType !== FriendModel::TYPE_SUBSCRIBERS) : ?>
		 	 		<a href="/friends-subscribers">
		 	 	<?php endif ?>

		 	 	<span class="profile_submenu_button profile_submenu_button_subscribers<?= ((int)$menuType === FriendModel::TYPE_SUBSCRIBERS) ? ' profile_submenu_button_active' : '' ?>">
		 	 		<span class="profile_submenu_button_subscriber">
		 	 			<?= lang('Friend.subscribers') . ' ' . $friendsAmount->subscribers ?>

		 	 			<?php if ((int)$friendsAmount->new_subscriber > 0) : ?>

		 	 				<span class="new_invite_amount"><?= $friendsAmount->new_subscriber ?></span>

		 	 			<?php endif ?>
		 	 		</span>
		 	 	</span>

		 	 	<?php if ((int)$menuType !== FriendModel::TYPE_SUBSCRIBERS) : ?>
		 	 		</a>
		 	 	<?php endif ?>

		 	 	<?php if ((int)$menuType !== FriendModel::TYPE_SUBSCRIPTIONS) : ?>
		 	 		<a href="/friends-subscriptions">
		 	 	<?php endif ?>

		 	 	<span class="profile_submenu_button profile_submenu_button_subscriptions profile_submenu_button_last_item<?= ((int)$menuType === FriendModel::TYPE_SUBSCRIPTIONS) ? ' profile_submenu_button_active' : '' ?>"><?= lang('Friend.subscriptions') . ' ' . $friendsAmount->subscriptions ?></span>

		 	 	<?php if ((int)$menuType !== FriendModel::TYPE_SUBSCRIPTIONS) : ?>
		 	 		</a>
		 	 	<?php endif ?>

		  	</div>
		  	<div class="profile_friend_search_block">
		  		<input type="text" name="search_string" class="search_friends_field" autocomplete="off" placeholder="<?= lang('Main.search') ?>">
		  	</div>
		  	<div class="friend_content_block"></div>
	  	</div>
    </div>
    <?php if ($statusUser === 'user'): ?>
		<?= $this->include('modal/write_message') ?>
		<?= $this->include('modal/event_meeting_invite') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/fetch/fetchGetFriendListSelf.js"></script>
    <script src="/js/fetch/fetchSendFriendRequest.js"></script>
    <script src="/js/main_friend_self.js"></script>
    <script src="/js/activateWriteNewmessageBox.js"></script>
    <script src="/js/ajaxGetUserPublicFiles.js"></script>
    <script src="/js/prepareFilesForMessage.js"></script>
    <script src="/js/ajaxLoadFile.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/addEmojiToMessageFieldModal.js"></script>
    <script src="/js/preloaderActionCircle.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/FileElement/AudioFileElementClass.js"></script>
    <script src="/js/FileElement/VideoFileElementClass.js"></script>
    <script src="/js/FileElement/DocumentFileElementClass.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/eventOrMeetingInvite.js"></script>
<?= $this->endSection() ?>