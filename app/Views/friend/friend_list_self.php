<?php if ($insert_type === 'renew') : ?>

	<div class="friend_query_search" data-itemCount="<?= esc($itemCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($friendResult)) : ?>
		<h3 class="no_events_title"><?= esc(lang('Main.Nothing found')) ?></h3>
	<?php endif ?>

<?php endif ?>

<?php foreach ($friendResult as $item) : ?>

	<?php $this->setVar('item', $item) ?>
	<?= $this->include('friend/friend_item_self') ?>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>
	
<?php endif ?>