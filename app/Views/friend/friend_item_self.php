<?php

use Common\Models\FriendModel;

$rowNumDataAttr = (isset($item->rowNum))
    ? 'data-rowNum="' . $item->rowNum . '"'
    : '';
?>

<div class="friend_result_item<?= (isset($item->newSubscriber) && $item->newSubscriber) ? ' new_subscriber_result_item' : '' ?>" data-profile_id="<?= $item->user_id ?>" <?= $rowNumDataAttr ?>>
	<div class="ava_box_friend_result_item">
        <a href="/profile/<?= $item->user_id ?>">
        	<img src="<?= $item->userAva ?>" class="ava_friend_result_item" alt="">
        </a>
    </div>
    <div class="user_name_button_block_friend_result_item">
    	<div class="user_name_block_friend_result_item">
    		<a href="/profile/<?= $item->user_id ?>">
    			<?= $item->userFullname ?>
    		</a>
    	</div>

        <?php if (!$item->selfItem) : ?>

        	<div class="button_block_friend_result_item">
        		<span class="button_friend_reslt_item send_message_button">
        			<?= (isset($item->locale)) ? lang('Main.Message', [], $item->locale) : lang('Main.Message') ?>
        		</span>
        		<span class="button_friend_reslt_item invite_button_friend_result_item">
        			<?= (isset($item->locale)) ? lang('Main.Invite', [], $item->locale) : lang('Main.Invite') ?>
        		</span>
        	</div>

            <?php if (!$selfProfile && isset($friendLinks[$item->user_id])) : ?>

                <span class="relation_with_user"><?= $friendLinks[$item->user_id]['relation_with_user'] ?></span>

            <?php endif ?>

        <?php endif ?>

    </div>

    <?php if ($selfProfile) : ?>

        <div class="menu_block_friend_result_item">
        	<img class="burger_menu_icon_friend_result_item" src="/img/icon_burger_menu.png">
        	<div class="burger_menu_friend_result_item">
        		<span class="burger_user_friend_menu_item" data-profile_id="<?= $item->user_id ?>" data-ws_tab_sync_to_do="<?= $friendLinks[$item->user_id]['ws_tab_sync_to_do'] ?>"><?= $friendLinks[$item->user_id]['button_value'] ?></span>

                <?php if ($friendLinks[$item->user_id]['user_relations'] === FriendModel::RELATION_NEW_SUBSCRIBER) : ?>

                    <span class="burger_user_friend_menu_item" data-profile_id="<?= $item->user_id ?>" data-ws_tab_sync_to_do="<?= $friendLinks[$item->user_id]['ws_tab_sync_to_do_reject'] ?>"><?= $friendLinks[$item->user_id]['button_value_reject'] ?></span>

                <?php endif ?>

        	</div>
        </div>

    <?php endif ?>

</div>