<?php

$dataRowNum = (!empty($item->rowNum))
	? 'data-rowNum="' . $item->rowNum . '"'
	: '';

?>

<div class="comment_result_item" data-comment_id="<?= esc($item->comment_id) ?>" <?= $dataRowNum ?>>
	<div class="comment_user_container" data-profile_id="<?= $item->user_id ?>">
		<div class="comment_user_ava_container">
			<a href="/profile/<?= $item->user_id ?>">
				<img class="comment_user_ava" src="<?= $item->userAva ?>">
			</a>
		</div>
		<div class="comment_user_name_date_container">
			<div class="comment_user_name">
				<a href="/profile/<?= $item->user_id ?>">
					<?= $item->userFullname ?>
				</a>
			</div>
			<div class="comment_date">
				<?= $item->date['date'] . ' ' . $item->date['time'] ?>
			</div>
		</div>
	</div>
	<div class="comment_text">
		<?= $item->text ?>
	</div>
</div>