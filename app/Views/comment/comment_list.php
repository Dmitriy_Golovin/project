<?php

$commentContainerPaddingStyle = (isset($commentData['writeField']) && $commentData['writeField'])
	? 'style="padding-bottom: 30px"'
	: '';

?>

<?php if ($commentData['insert_type'] === 'renew') : ?>

	<?php if ($commentData['showPrevious']) : ?>

		<div class="comment_show_previous"><?= lang('Main.show previous') ?></div>

	<?php endif ?>

	<div class="comment_query_search" <?= $commentContainerPaddingStyle ?> data-type="<?= $commentData['type'] ?>" data-entity_id="<?= $commentData['entityId'] ?>" data-comment_count="<?= esc($commentData['mainCommentAmount']) ?>" data-timestamp="<?= esc($commentData['timestamp']) ?>">

<?php endif ?>

<?php foreach ($commentData['commentList'] as $item) : ?>
	
	<?php $this->setVar('item', $item) ?>
	<?= $this->include('comment/comment_item') ?>

<?php endforeach ?>

<?php if ($commentData['insert_type'] === 'renew') : ?>

	</div>

<?php endif ?>