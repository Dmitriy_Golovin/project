<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('description') ?>

	<meta name="description" content="Универсум. Самое интересное в мире науки, история, мистические и таинственные проишествия." />

<?= $this->endSection() ?>

<?= $this->section('canonical') ?>

	<link rel="canonical" href="<?= site_url('universum') ?>" />

<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/universum_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php else : ?>
		<?= $this->include('templates/header_guest') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">

      	<?php foreach ($articleSectionList as $item) : ?>

      		<a class="article_section_container_anchor" href="/universum/<?= $item->title_transliteration ?>">
	      		<div class="article_section_container">
	      			<div class="article_section_image_container">
	      				<img class="article_section_image" src="<?= $item->url ?>" alt="<?= $item->title ?>" title="<?= $item->title ?>">
	      			</div>
	      			<div class="article_section_content">
	      				<h2><?= $item->title ?></h2>
	      				<div class="article_section_description">
	      					<?= $item->description ?>
	      				</div>
	      			</div>
	      		</div>
	      	</a>

      	<?php endforeach ?>

      </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/main_universum.js"></script>
<?= $this->endSection() ?>