<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('description') ?>

	<meta name="description" content="<?= $article->meta_description ?>" />

<?= $this->endSection() ?>

<?= $this->section('canonical') ?>

	<link rel="canonical" href="<?= site_url('universum/' . $articleSection->title_transliteration . '/' . $article->title_transliteration) ?>" />

<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/universum_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php else : ?>
		<?= $this->include('templates/header_guest') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box" data-article_section_id="<?= $articleSection->article_section_id ?>" data-article_id="<?= $article->article_id ?>">

      	<div class="back_to_list_container">
			<span class="back_to_list">
				<a href="/universum/<?= $articleSection->title_transliteration ?>"><?= lang('Main.back to article list') ?></a>
			</span>
		</div>

		<div class="aticle_details_block">
			<div class="article_content_container">
				<div class="article_title">
					<h1><?= $article->title ?></h1>
				</div>
				<div class="article_description">
					<?= $article->description ?>
				</div>
			</div>
	    </div>

      </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>

    <?php if ($statusUser === 'user'): ?>
    	<script src="/js/fetch/fetchSendComment.js"></script>
    <?php endif; ?>

    <script src="/js/showCommentWriteField.js"></script>
    <script src="/js/main_universum_article.js"></script>
<?= $this->endSection() ?>