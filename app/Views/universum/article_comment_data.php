<div class="article_details_comment_container">
    <?= view_cell('\Common\Widgets\comment\CommentWidget::render', [
        'commentData' => $commentData,
        'writeField' => ($commentData['statusUser'] === 'user') ? true : false,
    ]) ?>
</div>