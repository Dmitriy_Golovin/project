<?php if ($insert_type === 'renew') : ?>

	<div class="article_query_search" data-itemCount="<?= esc($itemCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($articleResult)) : ?>
		<h3 class="no_events_title"><?= esc(lang('Main.Nothing found')) ?></h3>
	<?php endif ?>

<?php endif ?>

<?php foreach ($articleResult as $item) : ?>

	<div class="" data-rowNum="<?= $item->rowNum ?>">
		<a class="article_section_container_anchor" href="/universum/<?= $articleSection->title_transliteration . '/' .  $item->title_transliteration ?>">
	  		<div class="article_section_container">
	  			<div class="article_section_image_container">
	  				<img class="article_section_image" src="<?= $item->fileUrl ?>" alt="<?= $item->fileTitle ?>" title="<?= $item->fileTitle ?>">
	  			</div>
	  			<div class="article_section_content">
	  				<h2><?= $item->title ?></h2>
	  				<div class="article_section_description">
      					<?= $item->descriptionPreview ?>
      				</div>
	  			</div>
	  		</div>
	  	</a>
	  </div>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>
	
<?php endif ?>