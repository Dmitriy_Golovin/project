<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('description') ?>

	<meta name="description" content="<?= $articleSection->title . '. ' . $articleSection->meta_description ?>" />

<?= $this->endSection() ?>

<?= $this->section('canonical') ?>

	<link rel="canonical" href="<?= site_url('universum/' . $articleSection->title_transliteration) ?>" />

<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/universum_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php else : ?>
		<?= $this->include('templates/header_guest') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box" data-article_section_id="<?= $articleSection->article_section_id ?>">

      	<div class="back_to_list_container">
			<span class="back_to_list">
				<a href="/universum/"><?= lang('Main.back to section list') ?></a>
			</span>
		</div>

	    <div class="article_section_title">
	    	<h1><?= $articleSection->title ?></h1>
	    </div>

	    <div class="search_article_block">
	      	<input type="text" name="search_string" class="search_article_field" autocomplete="off" placeholder="<?= lang('Main.search') ?>">
	      	<input type="submit" class="search_submit article_search_submit" value="">
	    </div>

	    <div class="aticle_content_block">

	    </div>

      </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetArticleList.js"></script>
    <script src="/js/main_universum_chapter.js"></script>
<?= $this->endSection() ?>