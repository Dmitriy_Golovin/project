<?php if ($insert_type === 'renew') : ?>

	<div class="holiday_query_search" data-holidayCount="<?= esc($holidayCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($holidayResult)) : ?>

		<h2 class="nothing_found"><?= esc(lang('Main.Nothing found')) ?></h2>

	<?php endif ?>

<?php endif ?>

<?php foreach ($holidayResult as $holiday) : ?>

	<div class="box_bottom_border search_main_result_item" data-rowNum="<?= esc($holiday->rowNum) ?>">
		<a href="/holiday/<?= esc($holiday->link) ?>">
			<h3 class="holiday_name_search_result"><?= esc($holiday->holiday_day) ?></h3>
		</a>
		<p class="holiday_date_search_result"><?= esc($holiday->holiday_date) ?></p>
	</div>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>

<?php endif ?>