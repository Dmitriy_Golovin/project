<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_search_page.css" type="text/css" />
	<link rel="stylesheet" href="/css/error_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php else : ?>
		<?= $this->include('templates/header_guest') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">
	    <div class="search_field_block">
	       <input type="search" name="search" class="" placeholder="поиск">
		  <!-- <input type="submit" class="search_submit search_elem" value=""> -->
	    </div>
	    <div class="search_options_block_main">
	      <div class="fields_block_search_options">
		    <div class="main_fields_search_options">
		      <select class="main_search_select field_for_search">
		        <option disabled selected value="">Что будем искать?</option>
		        <?php if ($userRole === 'user') : ?>
		        	<option value="f_users">люди</option>
		        	<option value="f_holidays">праздники</option>
		        <?php else : ?>
					<option value="f_holidays">праздники</option>
				<?php endif ?>
		      </select>
		    </div>
		    <div class="minor_fields_search_options">
			  <div class="f_users">
			    <select class="minor_field_for_search search_user_country">
		          <option data-countryId="" disabled selected value="">Выберите страну</option>

		          <?php foreach ($countryList as $country) : ?>
		          	<option data-countryId="<?= esc($country->country_id) ?>"><?= esc($country->title) ?></option>
		          <?php endforeach ?>
				  
		        </select>
			    <select name="city_for_search" class="minor_field_for_search search_user_city">
			    </select>
			  </div>
			  <div class="f_holidays">
			    <select class="minor_field_for_search search_holiday_country">
		          <option data-countryId="" disabled selected value="">Выберите страну</option>

		          <?php foreach ($countryList as $country) : ?>
		          	<option data-countryId="<?= esc($country->country_id) ?>"><?= esc($country->title) ?></option>
		          <?php endforeach ?>

		        </select>
			  </div>
		    </div>
		  </div>
	      <div class="button_wrapper_search_options">
		    <img class="search_options_left_arrow" src="/img/search_options_arrow.png">
		    <span class="search_options_button">показать параметры поиска</span>
		    <img class="search_options_right_arrow" src="/img/search_options_arrow.png">
		  </div>
		  <div class="start_search_block">
		    <input type="button" class="start_search_button" value="поиск">
		  </div>
	    </div>
	    <div class="search_result_block_main">
	    </div>
	  </div>
    </div>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('modal/write_message') ?>
		<?= $this->include('modal/event_meeting_invite') ?>
	<?php endif; ?>
	<?= $this->include('templates/error') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_search.js"></script>
    <script src="/js/ajaxGetCitiesForSearch.js"></script>
    <script src="/js/activateWriteNewmessageBox.js"></script>
    <script src="/js/ajaxSearchMain.js"></script>
    <script src="/js/ajaxSendFriendRequest.js"></script>
    <script src="/js/cutURLForController.js"></script>
	<script src="/js/screenNoiseErrorPage.js"></script>
	<script src="/js/ajaxGetUserPublicFiles.js"></script>
	<script src="/js/prepareFilesForMessage.js"></script>
	<script src="/js/addEmojiToMessageFieldModal.js"></script>
	<script src="/js/checkUploadFile.js"></script>
	<script src="/js/preloaderActionCircle.js"></script>
	<script src="/js/FileElement/FileElementClass.js"></script>
	<script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/FileElement/AudioFileElementClass.js"></script>
    <script src="/js/FileElement/VideoFileElementClass.js"></script>
    <script src="/js/FileElement/DocumentFileElementClass.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/eventOrMeetingInvite.js"></script>
<?= $this->endSection() ?>