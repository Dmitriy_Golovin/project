<div class="main_query_search">
	
	<?php if (!empty($userCount)) : ?>

		<div class="box_bottom_border search_main_result_item">
			<h3 class="main_query_h3_user_search_result" f="f_users"><?= esc('Люди: ' . $userCount) ?></h3>
		</div>

	<?php endif ?>

	<?php if (!empty($holidayCount)) : ?>

		<div class="box_bottom_border search_main_result_item">
			<h3 class="main_query_h3_holiday_search_result" f="f_holidays"><?= esc('Праздники: ' . $holidayCount) ?></h3>
		</div>

	<?php endif ?>

	<?php if (empty($userCount) && empty($holidayCount)) : ?>

		<h2 class="nothing_found"><?= esc(lang('Main.Nothing found')) ?></h2>

	<?php endif ?>

</div>