<?php helper('html');/*var_dump($friendLinks);die;*/ ?>

<?php if ($insert_type === 'renew') : ?>

	<div class="user_query_search" data-userCount="<?= esc($userCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($userResult)) : ?>

		<h2 class="nothing_found"><?= esc(lang('Main.Nothing found')) ?></h2>

	<?php endif ?>

<?php endif ?>

<?php foreach ($userResult as $user) : ?>

	<div class="friend_result_item" data-profile_id="<?= $user->user_id ?>" data-rowNum="<?= $user->rowNum ?>">
		<div class="ava_box_friend_result_item">
	        <a href="/profile/<?= $user->user_id ?>">
	        	<img src="<?= $user->userAva ?>" class="ava_friend_result_item" alt="">
	        </a>
	    </div>
	    <div class="user_name_button_block_friend_result_item">
	    	<div class="user_name_block_friend_result_item">
	    		<a href="/profile/<?= $user->user_id ?>">
	    			<?= $user->userFullname ?>
	    		</a>
	    	</div>

	    	<?php if (!$user->selfProfile) : ?>

		    	<div class="button_block_friend_result_item">
		    		<span class="button_friend_reslt_item send_message_button">
		    			<?= lang('Main.Message') ?>
		    		</span>
		    		<span class="button_friend_reslt_item invite_button_friend_result_item">
		    			<?= lang('Main.Invite') ?>
		    		</span>

		    		<?php if (isset($friendLinks[$user->user_id])) : ?>
			    		
			    		<span class="relation_with_user"><?= $friendLinks[$user->user_id]['relation_with_user'] ?></span>

			    	<?php endif ?>

		    	</div>

		    <?php endif ?>
	    </div>
	</div>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>

<?php endif ?>