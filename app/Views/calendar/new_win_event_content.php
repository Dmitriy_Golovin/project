<?php if (!empty($holidayList)) : ?>

	<div class="holidays_box">
		<h3 class="holiday_title"><?= lang('Holiday.Holidays') ?></h3>

		<?php foreach ($holidayList as $holiday) : ?>

			<div>
				<a href="<?= $holiday->link ?>" class="holiday_link">
					<p><?= $holiday->holiday_day ?></p>
				</a>
			</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>

<?php if (!empty($eventList)) : ?>

	<div class="event_box">
		<h3 class="event_title"><?= lang('Event.Events') ?></h3>

		<?php foreach ($eventList as $event) : ?>

			<div>
				<a href="<?= $event->link ?>" class="holiday_link">
					<p><span class="new_event_time_span"><?= $event->time ?></span><?= $event->name ?></p>
				</a>
			</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>

<?php if (!empty($meetingList)) : ?>

	<div class="meeting_box">
		<h3 class="meeting_title"><?= lang('Event.Meetings') ?></h3>

		<?php foreach ($meetingList as $meeting) : ?>

			<div>
				<a href="<?= $meeting->link ?>" class="holiday_link">
					<p><span class="new_event_time_span"><?= $meeting->time ?></span><?= $meeting->name ?></p>
				</a>
			</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>

<?php if (!empty($taskList)) : ?>

	<div class="task_assigned_box">
		<h3 class="task_assigned_title"><?= lang('Event.Tasks') ?></h3>

		<?php foreach ($taskList as $task) : ?>

			<div>
				<a href="<?= $task->link ?>" class="holiday_link">
					<p><span class="new_event_time_span"><?= $task->time ?></span><?= $task->name ?></p>
				</a>
			</div>

		<?php endforeach; ?>

	</div>

<?php endif; ?>