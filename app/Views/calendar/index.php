<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?= $this->include('templates/header_user') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
  <div class="content_wrapper user_page">
	<div class="calendar_settings_box">
	  <div class="calendar_menu_block">
	    <div class="calendar_menu_icon_block">
		  <img src="/img/icon_settings.png" class="calendar_menu_icon">
		  <div class="calendar_menu_item_block">
		    <ul class="calendar_menu_list">
			  <li>
			    <input type="checkbox" <?= $holidaySettings ?> id="holidays_check">
					<label for="holidays_check">праздники</label>
			  </li>
			  <li>
			    <input type="checkbox" <?= $myEventsSettings ?> id="my_events_check">
					<label for="my_events_check">мои события</label>
			  </li>
			  <li>
			    <input type="checkbox" <?= $myMeetingsSettings ?> id="my_meetings_check">
					<label for="my_meetings_check">мои встречи</label>
			  </li>
			  <li>
			    <input type="checkbox" <?= $myTasksSettings ?> id="my_tasks_check">
					<label for="my_tasks_check">мои задачи</label>
			  </li>
			</ul>
		  </div>
		</div>
	  </div>
	  <div class="change_month_block">
		<div class="change-month arrow_right_but"></div>
	    <span id="show-date"></span>
	    <div class="change-month arrow_left_but"></div>
	  </div>
	</div>
	<table data-month="<?= lang('TimeAndDate.monthListString') ?>">
	  <tr><th>пн</th><th>вт</th><th>ср</th><th>чт</th><th>пт</th><th>сб</th><th>вс</th></tr>
	  <tr>
	    <td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
	  </tr>
	  <tr>
	    <td>
	      <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
	  </tr>
	  <tr>
	    <td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
		    </div>
		  </div>
		</td>
	  </tr>
	  <tr>
	    <td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
		    <div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
	  </tr>
	  <tr>
	    <td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			  <div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
	  </tr>
	  <tr>
	    <td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td>
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
		<td class="sat_sun">
		  <div class="inside-cell">
		    <span class="day_date"></span>
			<div class="cell_events_box">
			  <span class="holiday_cell not_active"></span>
			  <span class="event_cell not_active"></span>
			  <span class="meeting_cell not_active"></span>
			  <span class="task_cell not_active"></span>
			</div>
		  </div>
		</td>
	  </tr>
	</table>
	<div class="add_new_win_container">
	  <div class="add_new_win inside_win">
	    <div id="event" class="container_create_new_event not_active">
		  <span class="span_close_win_add_new_event">
		    <img class="close_pic_event" src="/img/close_.png">
		  </span>
		  <div class="title_create_new_event title_create_new_event_transition">событие</div>
		  <div class="wrapper_create_new_event wrapper_create_new_event_transparent">
		    <fieldset class="fieldset_box fieldset_box_event_overflow">
			  <input type="text" class="input_create_new_event input_new_event_name" placeholder="добавить название" maxlength="150">
			  <div class="invite_box_create_new_event">
			    <div class="text_invite_create_new_event">пригласить</div>
					<div class="text_invite_create_new_event invite_item invite_user_buton" data-invite="users">пользователей</div>
			  </div>
			  <div class="date_box_create_new_event">
			    <select class="select_day">
				</select>
				<select class="select_month">
				  <option class="option_create_new_event" data-month="01">январь</option>
				  <option class="option_create_new_event" data-month="02">февраль</option>
				  <option class="option_create_new_event" data-month="03">март</option>
				  <option class="option_create_new_event" data-month="04">апрель</option>
				  <option class="option_create_new_event" data-month="05">май</option>
				  <option class="option_create_new_event" data-month="06">июнь</option>
				  <option class="option_create_new_event" data-month="07">июль</option>
				  <option class="option_create_new_event" data-month="08">август</option>
				  <option class="option_create_new_event" data-month="09">сентябрь</option>
				  <option class="option_create_new_event" data-month="10">октябрь</option>
				  <option class="option_create_new_event" data-month="11">ноябрь</option>
				  <option class="option_create_new_event" data-month="12">декабрь</option>
				</select>
				<select class="select_year">
				</select>
				<span class="day_of_week_create_new_event"></span>
			  </div>
			  <div class="time_box_create_new_event">
			  	<div>
			    	<span class="start_span_create_new_event">начало в</span>
						<select class="select_start_time_hour">

							<?php for ($i = 0; $i < 24; $i++) {
								  $hour = '';

								  if (strlen($i) == 1) {
								  	  $hour = '0' . $i;
								  } else {
								  	  $hour = $i;
								  } ?>

								  <option class="option_create_new_event"><?= esc($hour) ?></option>

							<?php } ?>
						  
						</select>

						<select class="select_start_time_minute">

							<?php for ($i = 0; $i < 60; $i++) {
								  $hour = '';

								  if (strlen($i) == 1) {
								  	  $hour = '0' . $i;
								  } else {
								  	  $hour = $i;
								  } ?>

								  <option class="option_create_new_event"><?= esc($hour) ?></option>

							<?php } ?>
						  
						</select>
					</div>
					<div>
						<span class="end_span_create_new_event">продолжительность</span>
						<select class="select_end_time">
							<?php
							foreach ($durationList as $index => $value) {
							?>
								<option class="option_create_new_event" data-duration="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
							<?php
							}
							?>
						</select>
					</div>
			  </div>
			  <input class="input_create_new_event input_new_event_location" type="text" placeholder="место" maxlength="150">
			  <div class="privacy_box_create_new_event">
			    <input type="radio" checked  id="public_el_event" name="privacy_event">
					<label for="public_el_event">публичное</label>
					<input type="radio" id="private_el_event" name="privacy_event">
					<label for="private_el_event">приватное</label>
			  	</div>
			  <div class="periodicity_box_create_new_event">
			    <span class="periodicity_span_create_new_event">периодичность</span>
					<select class="periodicity_select">
						<?php
						foreach ($periodicityList as $index => $value) {
						?>
							<option class="option_create_new_event" data-periodicity="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
						<?php
						}
						?>
					</select>
			  </div>
			  <div class="reminder_box_create_new_event">
			    <span class="reminder_span_create_new_event">напомнить за</span>
					<select class="reminder_select">
					  <?php
						foreach ($reminderList as $index => $value) {
						?>
							<option class="option_create_new_event" data-reminder="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
						<?php
						}
						?>
					</select>
			  </div>
			  <div class="description_box_create_new_event">
			    <textarea class="description_create_new_event" placeholder="добавить описание"></textarea>
			  </div>
			</fieldset>
		  </div>
		  <div class="add_button_block wrapper_create_new_event_transparent">
		  	<span class="event_form_error"></span>
		    <input type="button" class="add_button_event" value="сохранить">
		  </div>
		  <div class="invited_container not_active_transform_x not_active_opacity" data-active="0">
		  	<span class="back_button"><?= esc(lang('Main.back')) ?></span>
		  	<span class="invited_show_users_button not_active"><?= esc(lang('Event.show users')) ?></span>
		  	<div class="title_invited_container"><?= esc(lang('Event.invite users')) ?></div>
		  	<div class="title_invited_users_container not_active"><?= esc(lang('Event.invited users')) ?></div>
		  	<div class="invited_filter_and_content_container">
		  		<div class="invited_search_block">
		  			<input class="invited_search_field" type="text" placeholder="<?= lang('Main.enter first name or last name') ?>" auotocomplete="off">
		  		</div>
			  	<div class="invited_filter_block">
			  		<div class="invited_filter_item">
				  		<input class="invited_checkbox_input invited_only_friends" id="invited_friends_checkbox_event" type=checkbox name="only_friends">
				  		<label class="invited_checkbox_label" for="invited_friends_checkbox_event"><?= esc(lang('Main.friends')) ?></label>
				  	</div>
				  	<div class="invited_filter_item">
				  		<span class="invited_span_select_group"><?= esc(lang('Group.group')) ?></span>
				  		<!-- <select name="invited_select_group">
				  			<option data-group_id="7">wwwwwwwwwwwwwwwwwwwwwwwwwwww</option>
				  		</select> -->
				  	</div>
				  	<div class="invited_search_button_block">
				  		<span class="invited_added_users_button"><?= lang('Event.show invited') ?></span>
				  		<span class="invited_search_button"><?= lang('Main.search') ?></span>
				  	</div>
			  	</div>
			  	<div class="invited_content_block"></div>
			  	<div class="invited_added_users_container not_active"></div>
			  </div>
		  </div>
	  </div>
		<div id="meeting" class="container_create_new_event not_active">
		  <span class="span_close_win_add_new_event">
		    <img class="close_pic_event" src="/img/close_.png">
		  </span>
		  <div class="title_create_new_event title_create_new_event_transition">встреча</div>
		  <div class="wrapper_create_new_event wrapper_create_new_event_transparent">
		    <fieldset class="fieldset_box fieldset_box_event_overflow">
			  <input type="text" class="input_create_new_event input_new_event_name" placeholder="добавить название" maxlength="150">
			  <div class="invite_box_create_new_event">
			    <div class="text_invite_create_new_event">пригласить</div>
					<div class="text_invite_create_new_event invite_item invite_user_buton" data-invite="users">пользователей</div>
			  </div>
			  <div class="date_box_create_new_event">
			    <select class="select_day">
					</select>
					<select class="select_month">
					  <option class="option_create_new_event" data-month="01">январь</option>
					  <option class="option_create_new_event" data-month="02">февраль</option>
					  <option class="option_create_new_event" data-month="03">март</option>
					  <option class="option_create_new_event" data-month="04">апрель</option>
					  <option class="option_create_new_event" data-month="05">май</option>
					  <option class="option_create_new_event" data-month="06">июнь</option>
					  <option class="option_create_new_event" data-month="07">июль</option>
					  <option class="option_create_new_event" data-month="08">август</option>
					  <option class="option_create_new_event" data-month="09">сентябрь</option>
					  <option class="option_create_new_event" data-month="10">октябрь</option>
					  <option class="option_create_new_event" data-month="11">ноябрь</option>
					  <option class="option_create_new_event" data-month="12">декабрь</option>
					</select>
				<select class="select_year">
				</select>
				<span class="day_of_week_create_new_event"></span>
			  </div>
			  <div class="time_box_create_new_event">
			  	<div>
				    <span class="start_span_create_new_event">начало в</span>
						<select class="select_start_time_hour">

							<?php for ($i = 0; $i < 24; $i++) {
								  $hour = '';

								  if (strlen($i) == 1) {
								  	  $hour = '0' . $i;
								  } else {
								  	  $hour = $i;
								  } ?>

								  <option class="option_create_new_event"><?= esc($hour) ?></option>

							<?php } ?>
						  
						</select>

						<select class="select_start_time_minute">

							<?php for ($i = 0; $i < 60; $i++) {
								  $hour = '';

								  if (strlen($i) == 1) {
								  	  $hour = '0' . $i;
								  } else {
								  	  $hour = $i;
								  } ?>

								  <option class="option_create_new_event"><?= esc($hour) ?></option>

							<?php } ?>
						  
						</select>
					</div>
					<div>
						<span class="end_span_create_new_event">продолжительность</span>
						<select class="select_end_time">
						  <?php
							foreach ($durationList as $index => $value) {
							?>
								<option class="option_create_new_event" data-duration="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
							<?php
							}
							?>
						</select>
					</div>
			  </div>
			  <input class="input_create_new_event input_new_event_location" type="text" placeholder="место" maxlength="150">
			  <div class="privacy_box_create_new_event">
			    <input type="radio" id="public_el_meeting" name="privacy_meeting" checked>
				<label for="public_el_meeting">публичное</label>
				<input type="radio" id="private_el_meeting" name="privacy_meeting">
				<label for="private_el_meeting">приватное</label>
			  </div>
			  <div class="periodicity_box_create_new_event">
			    <span class="periodicity_span_create_new_event">периодичность</span>
					<select class="periodicity_select">
					  <?php
						foreach ($periodicityList as $index => $value) {
						?>
							<option class="option_create_new_event" data-periodicity="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
						<?php
						}
						?>
					</select>
			  </div>
			  <div class="reminder_box_create_new_event">
			    <span class="reminder_span_create_new_event">напомнить за</span>
					<select class="reminder_select">
					  <?php
						foreach ($reminderList as $index => $value) {
						?>
							<option class="option_create_new_event" data-reminder="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
						<?php
						}
						?>
					</select>
			  </div>
			  <div class="description_box_create_new_event">
			    <textarea class="description_create_new_event" placeholder="добавить описание"></textarea>
			  </div>
			</fieldset>
		  </div>
		  <div class="add_button_block wrapper_create_new_event_transparent">
		  	<span class="event_form_error"></span>
		    <input type="button" class="add_button_event" value="сохранить">
		  </div>
		  <div class="invited_container not_active_transform_x not_active_opacity" data-active="0">
		  	<span class="back_button"><?= esc(lang('Main.back')) ?></span>
		  	<span class="invited_show_users_button not_active"><?= esc(lang('Event.show users')) ?></span>
		  	<div class="title_invited_container"><?= esc(lang('Event.invite users')) ?></div>
		  	<div class="title_invited_users_container not_active"><?= esc(lang('Event.invited users')) ?></div>
		  	<div class="invited_filter_and_content_container">
		  		<div class="invited_search_block">
		  			<input class="invited_search_field" type="text" placeholder="<?= lang('Main.enter first name or last name') ?>" auotocomplete="off">
		  		</div>
			  	<div class="invited_filter_block">
			  		<div class="invited_filter_item">
				  		<input class="invited_checkbox_input invited_only_friends" id="invited_friends_checkbox_meeting" type=checkbox name="only_friends">
				  		<label class="invited_checkbox_label" for="invited_friends_checkbox_meeting"><?= esc(lang('Main.friends')) ?></label>
				  	</div>
				  	<div class="invited_filter_item">
				  		<span class="invited_span_select_group"><?= esc(lang('Group.group')) ?></span>
				  		<!-- <select name="invited_select_group">
				  			<option data-group_id="3">wwwwwwwwwwwwwwwwwwwwwwwwwwww</option>
				  		</select> -->
				  	</div>
				  	<div class="invited_search_button_block">
				  		<span class="invited_added_users_button"><?= lang('Event.show invited') ?></span>
				  		<span class="invited_search_button"><?= lang('Main.search') ?></span>
				  	</div>
			  	</div>
			  	<div class="invited_content_block"></div>
			  	<div class="invited_added_users_container not_active"></div>
			  </div>
		  </div>
	  </div>
		<div id="task" class="container_create_new_event not_active">
		  <span class="span_close_win_add_new_event">
		    <img class="close_pic_event" src="/img/close_.png">
		  </span>
		  <div class="title_create_new_event title_create_new_event_transition">задача</div>
		  <div class="wrapper_create_new_event wrapper_create_new_event_transparent">
		    <fieldset class="fieldset_box fieldset_box_event_overflow">
			  <input type="text" class="input_create_new_event input_new_event_name" placeholder="добавить название" maxlength="150">
			  <div class="invite_button_container">
			  	<div class="invite_button"><?= esc(lang('Task.assign an executor')) ?></div>
			  	<div class="assigned_user_container not_active">
			  		<div class="assigned_user_title"><?= lang('Task.executor') ?></div>
			  		<div class="assigned_user_block"></div>
			  	</div>
			  </div>
			  <div class="date_box_create_new_event">
			    <select class="select_day">
				</select>
				<select class="select_month">
				  <option class="option_create_new_event" data-month="01">январь</option>
				  <option class="option_create_new_event" data-month="02">февраль</option>
				  <option class="option_create_new_event" data-month="03">март</option>
				  <option class="option_create_new_event" data-month="04">апрель</option>
				  <option class="option_create_new_event" data-month="05">май</option>
				  <option class="option_create_new_event" data-month="06">июнь</option>
				  <option class="option_create_new_event" data-month="07">июль</option>
				  <option class="option_create_new_event" data-month="08">август</option>
				  <option class="option_create_new_event" data-month="09">сентябрь</option>
				  <option class="option_create_new_event" data-month="10">октябрь</option>
				  <option class="option_create_new_event" data-month="11">ноябрь</option>
				  <option class="option_create_new_event" data-month="12">декабрь</option>
				</select>
				<select class="select_year">
				</select>
				<span class="day_of_week_create_new_event"></span>
			  </div>
			  <div class="time_box_create_new_event">
			  	<div>
			    	<span class="start_span_create_new_event">начало в</span>
						<select class="select_start_time_hour">

							<?php for ($i = 0; $i < 24; $i++) {
								  $hour = '';

								  if (strlen($i) == 1) {
								  	  $hour = '0' . $i;
								  } else {
								  	  $hour = $i;
								  } ?>

								  <option class="option_create_new_event"><?= esc($hour) ?></option>

							<?php } ?>
						  
						</select>

						<select class="select_start_time_minute">

							<?php for ($i = 0; $i < 60; $i++) {
								  $hour = '';

								  if (strlen($i) == 1) {
								  	  $hour = '0' . $i;
								  } else {
								  	  $hour = $i;
								  } ?>

								  <option class="option_create_new_event"><?= esc($hour) ?></option>

							<?php } ?>
						  
						</select>
					</div>
					<div>
						<span class="end_span_create_new_event">продолжительность</span>
						<select class="select_end_time">
						  <?php
							foreach ($durationList as $index => $value) {
							?>
								<option class="option_create_new_event" data-duration="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
							<?php
							}
							?>
						</select>
					</div>
			  </div>
			  <div class="privacy_box_create_new_event">
			    <input type="radio" id="public_el_task" name="privacy_task" checked>
					<label for="public_el_task">публичное</label>
					<input type="radio" id="private_el_task" name="privacy_task">
					<label for="private_el_task">приватное</label>
			  </div>
			  <div class="periodicity_box_create_new_event">
			    <span class="periodicity_span_create_new_event">периодичность</span>
					<select class="periodicity_select">
						<?php
						foreach ($periodicityList as $index => $value) {
						?>
							<option class="option_create_new_event" data-periodicity="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
						<?php
						}
						?>
					</select>
			  </div>
			  <div class="reminder_box_create_new_event">
			    <span class="reminder_span_create_new_event">напомнить за</span>
					<select class="reminder_select">
					  <?php
						foreach ($reminderList as $index => $value) {
						?>
							<option class="option_create_new_event" data-reminder="<?= esc($index); ?>"><?= esc(lang('TimeAndDate.' . $value)); ?></option>
						<?php
						}
						?>
					</select>
			  </div>
			  <div class="file_box_create_new_event">
			  	<div class="file_button_create_new_event"><?= lang('Main.files') ?></div>
			  </div>
			  <div class="description_box_create_new_event">
			    <textarea class="description_create_new_event" placeholder="добавить описание"></textarea>
			  </div>
			</fieldset>
		  </div>
		  <div class="add_button_block wrapper_create_new_event_transparent">
		  	<span class="event_form_error"></span>
		    <input type="button" class="add_button_event" value="сохранить">
		  </div>
		  <div class="invited_container not_active_transform_x not_active_opacity" data-active="0">
		  	<span class="back_button"><?= esc(lang('Main.back')) ?></span>
		  	<div class="title_invited_container"><?= esc(lang('Task.assign an executor')) ?></div>
		  	<div class="invited_filter_and_content_container">
		  		<div class="invited_search_block">
		  			<input class="invited_search_field" type="text" placeholder="<?= lang('Main.enter first name or last name') ?>" auotocomplete="off">
		  		</div>
			  	<div class="invited_filter_block">
			  		<div class="invited_filter_item">
			  			<span class="invited_assign_self"><?= esc(lang('Task.make yourself an executor')) ?></span>
			  		</div>
			  		<div class="invited_filter_item">
				  		<input class="invited_checkbox_input invited_only_friends" id="invited_friends_checkbox_task" type=checkbox name="only_friends">
				  		<label class="invited_checkbox_label" for="invited_friends_checkbox_task"><?= esc(lang('Main.friends')) ?></label>
				  	</div>
				  	<div class="invited_filter_item">
				  		<span class="invited_span_select_group"><?= esc(lang('Group.group')) ?></span>
				  		<!-- <select name="invited_select_group">
				  			<option data-group_id="4">wwwwwwwwwwwwwwwwwwwwwwwwwwww</option>
				  		</select> -->
				  	</div>
				  	<div class="invited_search_button_block">
				  		<span class="invited_search_button"><?= lang('Main.search') ?></span>
				  	</div>
			  	</div>
			  	<div class="invited_content_block"></div>
			  </div>
		  </div>
		  <div class="add_file_container not_active_transform_x not_active_opacity" data-active="0">
		  	<span class="back_button"><?= esc(lang('Main.back')) ?></span>
		  	<div class="title_add_file_event"><?= lang('Main.task files') ?></div>
		  	<div class="event_add_file_block">
		  		<div class="create_event_file_field_row">
            <div class="create_event_file_field_label">
                <div class="note_label_container">
                    <?= lang('Main.video') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$maxVideo]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="create_event_video_container create_event_file_container">
                
            </div>
            <div class="create_event_file_field_value">
                <input type="file" id="task_load_file_video" name="task_video" class="load_file" data-error_max_value="<?= lang('Main.maximum {0}', [$maxVideo]) ?>" data-max_value="<?= $maxVideo ?>" hidden>
                <label for="task_load_file_video" class="create_event_file_load_label"><?= lang('Main.upload video') ?></label>
            </div>
            <div class="notice_elem"></div>
        	</div>
        	<div class="create_event_file_field_row">
            <div class="create_event_file_field_label">
                <div class="note_label_container">
                    <?= lang('Main.image') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$maxImage]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="create_event_image_container create_event_file_container">
                
            </div>
            <div class="create_event_file_field_value">
                <input type="file" id="task_load_file_image" name="task_image" class="load_file" data-error_max_value="<?= lang('Main.maximum {0}', [$maxImage]) ?>" data-max_value="<?= $maxImage ?>" multiple="true" hidden>
                <label for="task_load_file_image" class="create_event_file_load_label"><?= lang('Main.upload image') ?></label>
            </div>
            <div class="notice_elem"></div>
        	</div>
        	<div class="create_event_file_field_row">
            <div class="create_event_file_field_label">
                <div class="note_label_container">
                    <?= lang('Main.document') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$maxDocument]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="create_event_document_container create_event_file_container">
                
            </div>
            <div class="create_event_file_field_value">
                <input type="file" id="task_load_file_document" name="task_document" class="load_file" data-error_max_value="<?= lang('Main.maximum {0}', [$maxDocument]) ?>" data-max_value="<?= $maxDocument ?>" multiple="true" hidden>
                <label for="task_load_file_document" class="create_event_file_load_label"><?= lang('Main.upload document') ?></label>
            </div>
            <div class="notice_elem"></div>
        	</div>
	      </div>
		  </div>
	  </div>
		<div class="content_new_win_container inside_win">
		  <span class="close_win_add_new_event inside_win">
		  	<img alt="Close window" class="close_pic inside_win" src="/img/close_.png">
		  </span>
		  <div class="date_title_new_event inside_win"></div>
		  <div class="new_win_event_content_container"></div>
		  <div class="add_event_box inside_win">
		    <h3 class="inside_win">Добавить:</h3>
				<ul class="add_new_event_list inside_win">
		      <li class="add_new_event_item inside_win"><div class="inside_win" data-content="event">событие</div></li>
		      <li class="add_new_event_item inside_win"><div class="inside_win" data-content="meeting">встреча</div></li>
		      <li class="add_new_event_item inside_win"><div class="inside_win" data-content="task">задача</div></li>
		    </ul>
		  </div>
	   </div>
	  </div>
	  <div class="arrow inside_win"></div>
	</div>
  </div>
  <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderTemplateForLoad', []) ?>
  <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderTemplateForLoad', []) ?>
  <?= view_cell('\Common\Widgets\file\DocumentFileWidget::renderTemplateForLoad', []) ?>
  <?= $this->include('modal/slider_image_modal') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
	<script src="/js/changeButton.js"></script>
	<script src="/js/setGreetingContainerDimensions.js"></script>
	<script src="/js/calendarCreate.js"></script>
	<script src="/js/createNewEventAndShowEvents.js"></script>
	<script src="/js/personalMenu.js"></script>
	<script src="/js/closeModalCabinetPage.js"></script>
	<script src="/js/adaptive_user_menu_.js"></script>
	<script src="/js/checkUploadFile.js"></script>
	<script src="/js/FileElement/VideoFileExistElement.js"></script>
  <script src="/js/FileElement/MediaElementControlMixin.js"></script>
  <script src="/js/FileElement/FileElementClass.js"></script>
  <script src="/js/FileElement/ImageFileElementClass.js"></script>
  <script src="/js/Modal/ModalClass.js"></script>
  <script src="/js/Modal/ModalSliderImageClass.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/main_calendar.js"></script>
	<script src="/js/keyCRForm.js"></script>
	<script src="/js/ajaxCalendarSettings.js"></script>
	<script src="/js/showBlockToCreateANewEvent.js"></script>
	<script src="/js/ajaxSaveEvent.js"></script>
	<script src="/js/ajaxContentForCell.js"></script>
	<script src="/js/ajaxContentForEventWin.js"></script>
	<script src="/js/fetch/fetchGetUserList.js"></script>
	<script src="/js/fetch/fetchSaveBaseEvent.js"></script>
	<script src="/js/cutURLForController.js"></script>
<?= $this->endSection() ?>