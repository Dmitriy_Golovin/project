<?php

$mainEventParamsConfig = config('MainEventParamsConfig');

?>

<div class="modal_invite">
	<div class="modal_invite_box">
		<div class="modal_invite_event_meeting_box">
			<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
		  	<div class="invite_choose_entity">
		  		<span class="modal_invite_event_meeting_button modal_show_event_button" data-main_type="<?= $mainEventParamsConfig->mainTypeEvent ?>">
		  			<?= lang('Main.invite to event') ?>
		  		</span>
		  		<span class="modal_invite_event_meeting_button modal_show_meeting_button" data-main_type="<?= $mainEventParamsConfig->mainTypeMeeting ?>">
		  			<?= lang('Main.invite to a meeting') ?>
		  		</span>
		  	</div>
		  	<div class="invite_entity_box">
		  		<div class="invite_entity_title">
		  			
		  		</div>
		  		<div class="invite_entity_content">

		  		</div>
		  	</div>
		</div>
	</div>
</div>