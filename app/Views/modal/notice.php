<div class="modal modal_notice<?= (!empty($mainModalNoticeCssClass)) ? $mainModalNoticeCssClass : '' ?>">
  <div class="modal_box">
    <div class="modal_container_top">
	    <div class="notice_box_modal">
	    	<h3 class="notice_modal_title"><?= esc($titleNotice); ?></h3>
	    	<div class="notice_button_box">
	    		<?php if (isset($acceptNoticeAnchor)) : ?>
	    			<a href="<?= $acceptNoticeAnchor ?>">
	    		<?php endif ?>
	    			<span class="notice_button_accept"><?= (isset($acceptButtonText) && !empty($acceptButtonText)) ? $acceptButtonText : lang('Main.yes'); ?></span>
	    		<?php if (isset($acceptNoticeAnchor)) : ?>
	    			</a>
	    		<?php endif ?>
	    		<span class="notice_button_reject"><?= lang('Main.no'); ?></span>
	    	</div>
	    </div>
	  </div>
  </div>
</div>