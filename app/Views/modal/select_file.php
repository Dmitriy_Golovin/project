<div class="modal">
  <div class="modal_box">
    <div class="modal_container">
	    <?= $this->include('templates/select_picture_block') ?>
	    <?= $this->include('templates/select_video_block') ?>
	    <?= $this->include('templates/select_document_block') ?>
	    <div class="preloader_circle not_active">
		    <div class="preloader_circle_container">
		      <span class="preloader_item_1"></span>
		      <span class="preloader_item_2"></span>
		      <span class="preloader_item_3"></span>
		      <span class="preloader_item_4"></span>
		      <span class="preloader_item_5"></span>
		      <span class="preloader_item_6"></span>
		      <span class="preloader_item_7"></span>
		      <span class="preloader_item_8"></span>
		    </div>
		  </div>
	  </div>
  </div>
</div>