<div class="modal">
  <div class="modal_box">
    <div class="modal_container">
	    <div class="write_message_box_modal">
		    <span class="close_win_modal">
		      <img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		    </span>
		    <div class="message_to_user_block">
		      <span class="message_to_user_title">Сообщение для</span>
		      <a class="message_to_user_link" href="/profile/">
		        <span class="message_to_user_fullname"></span>
		      </a>
		    </div>
		    <div class="write_message_block">
	        <textarea class="write_message_field"></textarea>
	      </div>
	      <div class="count_of_files_block"></div>
	      <div class="available_file_message_block">
	      	<div class="available_image_message_block"></div>
	      	<div class="available_audio_message_block"></div>
	      	<div class="available_video_message_block"></div>
	      	<div class="available_document_message_block"></div>
	      </div>
		    <div class="message_button_block">
		      <div class="send_message_button_box">
		        <div class="emoji_block">
			        <img class="emoji_icon" src="/img/emj.png">
			        <?php $this->setVar('emojiBlockCssClass', 'emoji_box'); ?>
			        <?= $this->include('templates/emoji_box') ?>
			      </div>
			      <div class="add_file_block">
			      	<img class="add_file_icon" src="/img/add_picture_icon.png" data-block_modal="select_picture_box_modal" data-message_selected_file="available_image_message_block" data-file_type="1" title="Загрузить изображение">
			      </div>
			      <div class="add_file_block">
			      	<img class="add_file_icon" src="/img/add_video_icon.png" data-block_modal="select_video_box_modal" data-message_selected_file="available_video_message_block" data-file_type="3" title="Загрузить видеозапись">
			      </div>
			      <div class="add_file_block">
			        <img class="add_file_icon" src="/img/add_file_icon.png" data-block_modal="select_document_box_modal" data-message_selected_file="available_document_message_block" data-file_type="4" title="Загрузить документ">
			      </div>
			      <input type="button" class="send_message_to_user_button not_active_element" value="отправить">
		      </div>
		      <div class="waiting_icon waiting_succesfully_send_message not_active">
	          <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
			      <div class="succesfully_report not_active">отправлено</div>
	        </div>
	      </div>
	    </div>
	    <?= $this->include('templates/select_picture_block') ?>
	    <?= $this->include('templates/select_audio_block') ?>
	    <?= $this->include('templates/select_video_block') ?>
	    <?= $this->include('templates/select_document_block') ?>
	    <div class="preloader_circle not_active">
		    <div class="preloader_circle_container">
		      <span class="preloader_item_1"></span>
		      <span class="preloader_item_2"></span>
		      <span class="preloader_item_3"></span>
		      <span class="preloader_item_4"></span>
		      <span class="preloader_item_5"></span>
		      <span class="preloader_item_6"></span>
		      <span class="preloader_item_7"></span>
		      <span class="preloader_item_8"></span>
		    </div>
		  </div>
	  </div>
  </div>
</div>