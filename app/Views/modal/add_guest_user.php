<div class="modal">
	<div class="modal_box">
		<div class="modal_add_guest_user_box">
			<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
		  <div class="title_invited_container"><?= lang('Event.invite users') ?></div>
		  <div class="invited_filter_and_content_container">
		  	<div class="invited_search_block">
	  			<input class="invited_search_field" type="text" placeholder="<?= lang('Main.enter first or last name') ?>" auotocomplete="off">
	  		</div>
	  		<div class="invited_filter_block">

	  			<?php if (!empty($taskAssignSelf) && $taskAssignSelf) : ?>

	  				<div class="invited_filter_item">
	  					<span class="invited_assign_self"><?= lang('Task.make yourself an executor') ?></span>
	  				</div>

	  			<?php endif ?>

		  		<div class="invited_filter_item">
			  		<input class="invited_checkbox_input invited_only_friends" id="invited_friends_checkbox_event" type="checkbox" name="only_friends">
			  		<label class="invited_checkbox_label" for="invited_friends_checkbox_event"><?= lang('Main.friends') ?></label>
			  	</div>
			  	<div class="invited_search_button_block">
			  		<span class="invited_search_button"><?= lang('Main.search') ?></span>
			  	</div>
		  	</div>
		  	<div class="invited_content_block">
		  	</div>
		  </div>
		</div>
  </div>
</div>