<div class="modal">
  <div class="modal_box">
    <div class="modal_container">
    	<div class="container_load_avatar">
		  	<span class="close_win_modal">
			    <img alt="Close window" class="close_pic_modal" src="/img/close_.png">
			  </span>
	      <div class="current_ava_box ">
	    	  <div class="current_ava_container">
	          <img alt="Avatar" class="" src="<?= $pathToAva ?>">
	        </div>
	        <div class="notice_ava_extension"></div>
	      </div>
	      <div class="change_avatar_button_main">
	        <input type="file" id="load_new_ava" hidden>
		      <label for="load_new_ava" class="change_ava_load_menu_button">загрузить</label>
	        <span class="change_ava_select_menu_button">выбрать</span>
	      </div>
	    </div>
	    <div class="container_select_avatar modal_block_not_active not_active_opacity">
	    	<span class="close_win_modal">
			    <img alt="Close window" class="close_pic_modal" src="/img/close_.png">
			  </span>
	  	  <div class="container_available_image"></div>
	  	  <div class="select_avatar_button_box">
	  		  <span class="select_ava_cancel_button">отмена</span>
	  		  <span class="select_ava_accept_button not_active_element">подтвердить</span>
	  	  </div>
	    </div>
	    <div class="preloader_circle not_active">
		    <div class="preloader_circle_container">
		      <span class="preloader_item_1"></span>
		      <span class="preloader_item_2"></span>
		      <span class="preloader_item_3"></span>
		      <span class="preloader_item_4"></span>
		      <span class="preloader_item_5"></span>
		      <span class="preloader_item_6"></span>
		      <span class="preloader_item_7"></span>
		      <span class="preloader_item_8"></span>
		    </div>
		  </div>
		</div>
  </div>
</div>
