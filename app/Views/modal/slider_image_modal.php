<div class="modal modal_slider_image">
  <div class="modal_box">
    <div class="modal_container">
	    <div class="slider_image_box_modal not_active_opacity modal_block_not_active">
	    	<div class="close_slide_modal_container">
	    		<img src="/img/close_slide_modal.png">
	    	</div>
	    	
	    	<?php if (!isset($userAgentMobile)) { ?>
		    	<div class="slider_image_left_arrow_container">
		    		<img src="/img/slider_arrow_left.png">
		    	</div>
		    <?php } ?>

	    	<div class="<?= !isset($userAgentMobile) ? 'slider_image_container' : 'slider_image_container_mobile' ?>">

	    	</div>

	    	<?php if (!isset($userAgentMobile)) { ?>
		    	<div class="slider_image_right_arrow_container">
		    		<img src="/img/slider_arrow_right.png">
		    	</div>
		    <?php } ?>

	    </div>
	    <div class="preloader_circle not_active">
		    <div class="preloader_circle_container">
		      <span class="preloader_item_1"></span>
		      <span class="preloader_item_2"></span>
		      <span class="preloader_item_3"></span>
		      <span class="preloader_item_4"></span>
		      <span class="preloader_item_5"></span>
		      <span class="preloader_item_6"></span>
		      <span class="preloader_item_7"></span>
		      <span class="preloader_item_8"></span>
		    </div>
		  </div>
	  </div>
  </div>
</div>