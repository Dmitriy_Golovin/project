<div class="modal modal_show_guest">
	<div class="modal_box modal_show_guest_box">
		<div class="modal_show_guest_user_box">
			<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
		  	<div class="profile_entity_guest_content"></div>
		</div>
	</div>
</div>