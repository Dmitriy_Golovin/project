<div class="modal modal_notice_close">
  <div class="modal_box">
    <div class="modal_container_top">
	    <div class="notice_box_modal">
	    	<h3 class="notice_modal_title"><?= esc($titleNotice); ?></h3>
	    	<div class="notice_button_box">
	    		<span class="notice_button_close"><?= lang('Main.close'); ?></span>
	    	</div>
	    </div>
	  </div>
  </div>
</div>