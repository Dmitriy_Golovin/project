<div class="modal_notification">
	<div class="modal_notification_box">
		<div class="modal_notification_content_box">
			<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
			<h3 class="nothing_found"><?= lang('Main.No new notifications') ?></h3>
		</div>
  	</div>
</div>