<div class="modal_chat_member">
	<div class="modal_box">
		<div class="modal_chat_member_box">
			<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
			<div class="title_chat_member_container"><?= lang('ChatMember.chat members') ?></div>
			<div class="chat_member_content_block"></div>
		</div>
	</div>
</div>