<div class="self_assign_executor_container not_active">
	<div class="box_bottom_border user_result_item" data-profile_id="<?= $userID ?>">
		<div class="ava_box_result_item">
			<a href="/profile/<?= $userID ?>">
				<img src="<?= $pathToAva ?>" class="ava_result_item" alt="">
			</a>
		</div>
		<div class="fullname_box_result_item">
			<a href="http://project.loc/profile/<?= $userID ?>"><?= $firstName . ' ' . $lastName ?></a>
		</div>
		<div class="button_box_result_item">
			<span class="task_cancel_user_button"><?= lang('Task.cancel') ?></span>
		</div>
	</div>
</div>