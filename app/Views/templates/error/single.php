<?php helper('html') ?>

<?php if (isset($model) && $model->hasErrors()) : ?>
	<div class="errors" role="validation_errors">
		<span class="close_errors_block">
    		<img alt="Close validation block" class="" src="/img/close_.png">
  		</span>
		<ul>
			<li><?= esc($model->getFirstError()) ?></li>
		</ul>
	</div>

	<?= script_tag('/js/closeErrorValidationBlock.js') ?>
<?php endif ?>