<?php

$session = \Config\Services::session(); 
helper('html')

?>

<?php if (/*isset($model) && !empty($model->errors)*/!empty($session->getFlashdata('redirectError'))) : ?>
	<div class="errors" role="validation_errors">
		<span class="close_errors_block">
    		<img alt="Close validation block" class="" src="/img/close_.png">
  		</span>
		<ul>	
			<li><?= $session->getFlashdata('redirectError') ?></li>
		</ul>
	</div>

	<?php $session->remove('redirectError'); ?>
	<?php $session->destroy(); ?>
	<?= script_tag('/js/closeErrorValidationBlock.js') ?>
<?php endif ?>