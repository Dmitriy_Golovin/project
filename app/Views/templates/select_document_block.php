<div class="select_document_box_modal not_active_opacity modal_block_not_active">
	<span class="close_win_modal" data-close_modal_block="select_picture_box_modal">
    <img alt="Close window" class="close_pic_modal" src="/img/close_.png">
  </span>
  <div class="select_document_block">
    <div class="file_container"></div>
    <div class="notice_elem_modal_message"></div>
    <div class="select_avatar_button_box">
      <input type="file" id="load_file_document" class="load_file" multiple hidden>
      <span class="select_file_accept_button not_active_element" data-insert_block="available_document_message_block">подтвердить</span>
    </div>
  </div>
</div>