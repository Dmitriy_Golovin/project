<div class="menu_user_block header_footer_col">
  <ul class="menu_user_main">
    <li class="menu_user_item universum_menu_user_item">
      <a href="/universum"><img class="menu_user_icon" src="/img/icon_universum.png"><?= lang('Main.universum') ?></a>
    </li>
    <!-- <li class="menu_user_item books_menu_user_item">
      <a href="/books"><img class="menu_user_icon" src="/img/icon_books.png"><?= lang('Main.books') ?></a>
    </li> -->
    <li class="menu_user_item search_elem">
  	  <div class="search_block search_elem">
  	    <input type="search" name="search" class="search_field search_elem" placeholder="<?= lang('Main.search') ?>">
  	    <input type="submit" class="search_submit search_elem" value="">
  	  </div>
    </li>
    <li class="burger_menu not_active"><img class="burger_menu_icon" src="/img/icon_burger_menu.png"></li>
  </ul>
  <div class="burger_menu_header_box">
    <ul class="menu_user_burger">
    </ul>
  </div>
</div>
<div class="header_footer_col autorized_or_personal_block">
  <div class="header_footer_col"><a href="/login"><span class="login_button"><?= lang('Main.login') ?></span></a></div>
  <div class="header_footer_col"><a href="/register"><span class="registration_button"><?= lang('Main.registration') ?></span></a></div>
</div>
