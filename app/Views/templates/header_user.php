<?php

$newCabinetEventAmount = ($cabinetData['mainCount'] > 0)
		? '<span class="new_subscriber_amount">' . $cabinetData['mainCount'] . '</span>'
		: '';

?>

<div class="menu_user_block header_footer_col">
  <ul class="menu_user_main">
    <li class="menu_user_item notification_menu_user_item">
    	<img class="menu_user_icon" src="/img/icon_bell.png">
    	<?= $this->include('modal/notification') ?>
    </li>
	<li class="menu_user_item"><a href="<?= $logoUrl ?>"><img class="menu_user_icon" src="/img/icon_events.png">календарь</a></li>
	<li class="menu_user_item universum_menu_user_item">
    <a href="/universum"><img class="menu_user_icon" src="/img/icon_universum.png"><?= lang('Main.universum') ?></a>
  </li>
	<li class="menu_user_item cabinet_menu_user_item"><a href="/cabinet"><img class="menu_user_icon" src="/img/icon_cabinet.png">кабинет<?= $newCabinetEventAmount ?></a></li>
	<li class="menu_user_item friends_menu_user_item"><a href="/friends"><img class="menu_user_icon" src="/img/icon_friends.png">друзья<?=$newSubscriberAmount?></a></li>
	<li class="menu_user_item messages_menu_user_item"><a href="/messages"><img class="menu_user_icon" src="/img/icon_messages.png">сообщения<?=$newMessagesAmount?></a></li>
	<li class="menu_user_item search_elem">
	  <div class="search_block search_elem">
	    <input type="search" name="search" class="search_field search_elem" placeholder="<?= lang('Main.search') ?>">
		<input type="submit" class="search_submit search_elem" value="">
	  </div>
    </li>
	<li class="burger_menu not_active">
	  <img class="burger_menu_icon" src="/img/icon_burger_menu.png">
	  <span class="main_notification_amount not_active"></span>
	</li>
  </ul>
  <div class="burger_menu_header_box">
    <ul class="menu_user_burger">
    </ul>
  </div>
</div>
<div class="header_footer_col autorized_or_personal_block">
  <div class="header_footer_col personal_data_block">
		<p class="user_full_name" data-userID="<?= $userID ?>"><?= $firstName . ' ' . $lastName ?></p>
		<p class="box_pers_data_arrow"><img alt="menu_presonal_data" class="pers_data_arrow" src="/img/pers_data_arrow.png"></p>
		<div class="menu_box inside_menu_box">
		  <div class="menu_box_wrapper inside_menu_box">
			<ul class="menu_box_list inside_menu_box">
			  <a href="/profile/<?= $userID ?>"><li class="profile_but menu_box_item inside_menu_box">Мой профиль</li></a>
			  <a href="/profile/change_personal_data"><li class="person_data_but menu_box_item inside_menu_box">Личные данные</li></a>
			  <a href="/logout"><li class="menu_box_item inside_menu_box">Выход</li></a>
	        </ul>
		  </div>
		</div>
  </div>
  <div class="header_footer_col avatar_block"><img alt="Avatar" class="avatar" src="<?= $pathToAva ?>"></div>
</div>