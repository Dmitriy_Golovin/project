<?php

$newInviteEvent = (count($cabinetData['event']) > 0)
	? '<span class="new_invite_amount">' . count($cabinetData['event']) . '</span>'
	: '';

$newInviteMeeting = (count($cabinetData['meeting']) > 0)
	? '<span class="new_invite_amount">' . count($cabinetData['meeting']) . '</span>'
	: '';

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/cabinet_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">
		  <div class="cabinet_content">
		    <div class="list_cabinet_item">
		      <div class="self_events_item">
		      	<a href="/event">
				    <div class="name_item_block">
				      <h2><?= lang('Event.Events') ?></h2>
				    </div>
				    <div class="statistic_item_block">
				      <div class="statistic_item_container">
					    <div class="events_current_item_name"><?= lang('Event.statusDatePlural.current') ?>:</div>
					    <div class="events_current_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="events_scheduled_item_name"><?= lang('Event.statusDatePlural.scheduled') ?>:</div>
					    <div class="events_scheduled_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="events_completed_item_name"><?= lang('Event.statusDatePlural.completed') ?>:</div>
					    <div class="events_completed_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="invites_event_item_name"><?= lang('Event.statusDatePlural.invitations') ?>:</div>
					    <div class="invites_event_item_count"><?= $newInviteEvent ?></div>
					  </div>
				    </div>
				</a>
			  </div>
			  <div class="meeting_item">
			  	<a href="/meeting">
				    <div class="name_item_block">
				      <h2><?= lang('Event.Meetings') ?></h2>
				    </div>
				    <div class="statistic_item_block">
				      <div class="statistic_item_container">
					    <div class="meeting_current_item_name"><?= lang('Event.statusDatePlural.current') ?>:</div>
					    <div class="meeting_current_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="meeting_scheduled_item_name"><?= lang('Event.statusDatePlural.scheduled') ?>:</div>
					    <div class="meeting_scheduled_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="meeting_completed_item_name"><?= lang('Event.statusDatePlural.completed') ?>:</div>
					    <div class="meeting_completed_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="invites_meeting_item_name"><?= lang('Event.statusDatePlural.invitations') ?>:</div>
					    <div class="invites_meeting_item_count"><?= $newInviteMeeting ?></div>
					  </div>
				    </div>
				</a>
			  </div>
			  <div class="tasks_item">
			  	<a href="/task">
				    <div class="name_item_block">
				      <h2><?= lang('Event.Tasks') ?></h2>
				    </div>
				    <div class="statistic_item_block">
				      <div class="statistic_item_container">
					    <div class="tasks_current_item_name"><?= lang('Event.statusDatePlural.current') ?>:</div>
					    <div class="tasks_current_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="tasks_scheduled_item_name"><?= lang('Event.statusDatePlural.scheduled') ?>:</div>
					    <div class="tasks_scheduled_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="tasks_completed_item_name"><?= lang('Event.statusDatePlural.completed') ?>:</div>
					    <div class="tasks_completed_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="tasks_overdue_item_name"><?= lang('Task.statusDatePlural.overdue') ?>:</div>
					    <div class="tasks_overdue_item_count"></div>
					  </div>
					  <div class="statistic_item_container">
					    <div class="tasks_created_item_name"><?= lang('Task.statusDatePlural.created') ?>:</div>
					    <div class="tasks_created_item_count"></div>
					  </div>
				    </div>
				</a>
			  </div>
			  <div class="files_item">
		      	<a href="/file/photo">
				    <div class="name_item_block">
				      <h2><?= lang('Main.Files') ?></h2>
				    </div>
				    <div class="statistic_item_block">
				    	<div class="statistic_item_container">
						    <div class="image_scheduled_item_name"><?= lang('Main.photoPlural.photo') ?>:</div>
						    <div class="image_scheduled_item_count"><?= $fileCountOfUser['imageCount'] ?></div>
						</div>
					    <div class="statistic_item_container">
						  	<div class="video_current_item_name"><?= lang('Main.video') ?>:</div>
						    <div class="video_current_item_count"><?= $fileCountOfUser['videoCount'] ?></div>
						</div>
						<div class="statistic_item_container">
						    <div class="document_completed_item_name"><?= lang('Main.documents') ?>:</div>
						    <div class="document_completed_item_count"><?= $fileCountOfUser['documentCount'] ?></div>
						</div>
				    </div>
				</a>
			  </div>
		    </div>
		  </div>
	  </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/showTheResultsOfMatchingInputFromAvailableOnes.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetMainEventData.js"></script>
    <script src="/js/main_cabinet.js"></script>
<?= $this->endSection() ?>