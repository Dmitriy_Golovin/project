<?php

$sliderContainerCssClass = ($userAgentMobile) ? 'slider_image_comment_container_mobile' : 'slider_image_comment_container';
$sliderContentBoxCssClass = ($userAgentMobile) ? 'modal_image_slider_content_box_mobile' : 'modal_image_slider_content_box';
$leftArrowNotActiveCssClass = (!$leftArrowActive) ? 'not_active_element_arrow_pic' : '';
$rightArrowNotActiveCssClass = (!$rightArrowActive) ? 'not_active_element_arrow_pic' : '';
$dataImageCount = (isset($imageCount)) ? 'data-image_count="' . $imageCount . '"' : '';

?>

<?php if ($type === 1) : ?>

	<div class="modal_image_slider">
		<div class="modal_image_slider_box">
			<div class="slider_image_box_modal">
				<div class="close_container">
					<img class="close_container_pic" src="/img/close_slide_modal.png">
				</div>

				<?php if (!$userAgentMobile) : ?>
					<div class="slider_image_left_arrow_container">
			    		<img src="/img/slider_arrow_left.png" class="<?= $leftArrowNotActiveCssClass ?>">
			    	</div>
			    <?php endif ?>

				<div class="<?= $sliderContainerCssClass ?>" style="--sliderTranslate: <?= $sliderTranslate ?>%" <?= $dataImageCount ?>>

<?php endif ?>

<?php if (isset($imageData)) : ?>

	<?php foreach ($imageData as $file) : ?>
		
		<?php $notActiveCommentCssClass = ($file->current) ? ' ' : ' not_active' ?>
		<div class="<?= $sliderContentBoxCssClass ?>" data-current_image="<?= $file->current ? 1 : 0 ?>">
			<div class="modal_slider_image_container">
				<?= view_cell('\Common\Widgets\file\ImageFileWidget::renderInModal', ['file' => $file]); ?>
			</div>
			<div class="modal_slider_image_comment_container<?= $notActiveCommentCssClass ?>">
				<?= view_cell('\Common\Widgets\comment\CommentWidget::render', [
			        'commentData' => $file->commentData,
			        'writeField' => true,
			    ]) ?>
			</div>
		</div>

	<?php endforeach ?>

<?php endif ?>

<?php if ($type === 1) : ?>

				</div>

				<?php if (!$userAgentMobile) : ?>
					<div class="slider_image_right_arrow_container">
			    		<img src="/img/slider_arrow_right.png" class="<?= $rightArrowNotActiveCssClass ?>">
			    	</div>
			    <?php endif ?>

			</div>
		</div>
	</div>

<?php endif ?>