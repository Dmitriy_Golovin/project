<div class="modal_video">
	<div class="modal_video_box">
		<div class="close_container">
			<img class="close_container_pic" src="/img/close_slide_modal.png">
		</div>
		<div class="modal_video_content_box">
			<div class="modal_video_container">
				<?= view_cell('\Common\Widgets\file\VideoFileWidget::renderInModal', ['file' => $file]); ?>
			</div>
			<div class="comment_modal_container">
				<?= view_cell('\Common\Widgets\comment\CommentWidget::render', [
			        'commentData' => $file->commentData,
			        'writeField' => true,
			    ]) ?>
			</div>
		</div>
	</div>
</div>