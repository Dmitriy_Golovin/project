<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('description') ?>
	<meta name="description" content="Регистрация на сайте. Создайте профиль и делитесь с друзьями запоминающимися событиями, читайте интересные статьи. Календарь. Праздники. Праздники России. События, встречи, задачи." />
<?= $this->endSection() ?>

<?= $this->section('canonical') ?>

	<link rel="canonical" href="<?= site_url('register') ?>" />

<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper login_registration_page">
      <div class="login_reg_wrapper">
	    <div class="confirmation_box not_active"></div>
	    <div class="login_reg_box">
	      <?= $this->include('templates/error/single') ?>
	      <h3><?= lang('Main.Registering a new account') ?></h3>
		  <p><?= lang('Main.Already registered?') ?> <a href="/login"><?= lang('Main.Login') ?></a></p>
		  <form name="registration_form" method="post" action="/register">
			<!-- <input type="hidden" name="<?= csrf_token() ?>" value="<?= csrf_hash() ?>" /> -->
		    <fieldset class="fieldset_box">
		      <label class="label_reg_form"><?= lang('Main.Email') ?></label>
		      <input type="text" name="email" class="input_login_reg" tabindex="1">
			  <div class="notice_elem email"></div>
			  <label class="label_reg_form"><?= lang('Main.Password') ?></label>
			  <input type="password" name="password" class="input_login_reg" tabindex="2">
			  <div class="notice_elem password"></div>
			  <label class="label_reg_form"><?= lang('Main.Repeat password') ?></label>
			  <input type="password" name="repeat_password" class="input_login_reg" tabindex="3">
			  <div class="notice_elem repeat_password"></div>
			  <label class="label_reg_form"><?= lang('Main.Name') ?></label>
			  <input type="text" name="first_name" class="input_login_reg" tabindex="4">
			  <div class="notice_elem first_name"></div>
			  <label class="label_reg_form"><?= lang('Main.Surname') ?></label>
			  <input type="text" name="last_name" class="input_login_reg" tabindex="5">
			  <div class="notice_elem last_name"></div>
			  <input type="checkbox" name="accept_terms" id="accept_terms">
			  <label for="accept_terms" class="accept_terms_label">
			  	<?= lang('Main.I accept the terms of') ?>
			  	<a href="/termsofuse"><?= lang('Main.genetiveTermsOfUse.User agreement') ?></a>
			  	<?= lang('Main.and') ?>
			  	<a href="/privacy"><?= lang('Main.genetivePrivacyPolicy.Privacy policy') ?></a>
			  </label>
		    </fieldset>
		    <div class="button_box">
	          <input type="button" class="button_cancel_reg" value="<?= lang('Main.cancel') ?>" tabindex="7">
	          <input type="button" class="button_reg" value="<?= lang('Main.send') ?>" tabindex="6">
			  <div class="waiting_icon button_copy_reg not_active">
			    <span class="waiting_icon_span"></span>
			    <span class="waiting_icon_span"></span>
			    <span class="waiting_icon_span"></span>
			  </div>
		    </div>
		  </form>
	    </div>
	  </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main_login_registration.js"></script>
	<script src="/js/keyCRForm.js"></script>
    <script src="/js/checkFillingFormRegistration.js"></script>
    <script src="/js/ajaxCheckEmailExist.js"></script>
<?= $this->endSection() ?>