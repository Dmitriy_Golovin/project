<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('description') ?>
	<meta name="description" content="Календарь. Праздники. Праздники России. События, встречи, задачи. Создавайте события, встречи, задачи. Пригласить на событие друзей. Пригласить на встречу друзей. Назначить исполнителя на задачу. Обмен сообщениями. Чат. Calendar. Holidays. Holidays of Russia. Events, meetings, tasks. Create events, meetings, tasks. Invite friends to the event. Invite friends to a meeting. Assign an executor to a task. Message exchange. Chat." />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper pb_10">
			<div class="home_description_container">
				<h1><?= lang('Main.Your calendar. Events. Meetings. Tasks. Holidays.') ?></h1>
				<div class="description_content_block">
					<div class="description_image_container">
						<img class="description_image_s_1" src="/img/s-1.jpg">
						<img class="description_image_s_2" src="/img/s-2.jpg">
						<img class="description_image_s_3" src="/img/s-3.jpg">
					</div>
					<h1 class="description_text_container">
						<?= lang('Main.Create reminders for important and interesting events. Find new friends and invite them to a scheduled event or meeting. Share photo and video reports. Create tasks and assign an executor.') ?>
					</h1>
				</div>
			</div>
        <div class="calendar_settings_box start_page">
          <div class="change_month_block">
            <div class="change-month arrow_right_but"></div>
	        <span id="show-date"></span>
	        <div class="change-month arrow_left_but"></div>
	      </div>
        </div>
        <table class="start_page" data-month="<?= lang('TimeAndDate.monthListString') ?>">
          <tr>
		    <th>пн</th>
			<th>вт</th>
			<th>ср</th>
			<th>чт</th>
			<th>пт</th>
			<th>сб</th>
			<th>вс</th>
		  </tr>
	      <tr>
		    <td>
			  <div class="inside-cell">
			    <span class="day_date"></span>
		        <div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
			<td>
			  <div class="inside-cell">
			    <span class="day_date"></span>
				<div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
			<td>
			  <div class="inside-cell">
			    <span class="day_date"></span>
				<div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
			<td>
			  <div class="inside-cell">
			    <span class="day_date"></span>
				<div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
			<td>
			  <div class="inside-cell">
			    <span class="day_date"></span>
				<div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
			<td class="sat_sun">
			  <div class="inside-cell">
			    <span class="day_date"></span>
				<div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
			<td class="sat_sun">
			  <div class="inside-cell">
			    <span class="day_date"></span>
				<div class="cell_events_box">
				  <span class="holiday_cell not_active"></span>
				</div>
			  </div>
			</td>
		  </tr>
	      <tr><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td></tr>
	      <tr><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td></tr>
	      <tr><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td></tr>
	      <tr><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td></tr>
	      <tr><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td><td class="sat_sun"><div class="inside-cell"><span class="day_date"></span><div class="cell_events_box"><span class="holiday_cell not_active"></span></div></div></td></tr>
        </table>
	  <div class="add_new_win_container">
        <div class="add_new_win inside_win">
          <div class="content_new_win_container inside_win">
		    <span class="close_win_add_new_event inside_win"><img alt="Close window" class="close_pic inside_win" src="img/close_.png" /></span>
		    <div class="date_title_new_event inside_win"></div>
		    <div class="new_win_event_content_container"></div>
	        <div class="login_reg_box_small inside_win">
	          <h3 class="inside_win"><?= lang('Main.loginFuture.Login') ?></h3>
		      <p class="inside_win"><?= lang('Main.or') ?> <a href="/register"><?= lang('Main.register') ?></a></p>
		      <form name="login_form" method="post" action="/login" class="inside_win">
		        <fieldset class="fieldset_box inside_win">
	              <input type="text" name="email" class="input_login_reg_start_page inside_win" placeholder="email" tabindex="1">
	              <input type="password" name="password" class="input_login_reg_start_page inside_win" placeholder="<?= lang('Main.password') ?>" tabindex="2">
		        </fieldset>
		        <div class="notice_elem_start_page inside_win"></div>
		        <div class="button_box_start inside_win">
	              <input type="button" class="button_login_start_page inside_win" value="<?= lang('Main.sign in') ?>" tabindex="3">
		          <p class="lost_password_start_page inside_win"><a href="/send-restore-token"><?= lang('Main.Forgot password?') ?></a></p>
		        </div>
		      </form>
	        </div>
	      </div>
        </div>
        <div class="arrow inside_win"></div>
      </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/setGreetingContainerDimensions.js"></script>
    <script src="/js/calendarCreate.js"></script>
    <script src="/js/createNewEventAndShowEvents.js"></script>
    <script src="/js/checkFillingFormLogin.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main_start_page.js"></script>
	<script src="/js/keyCRForm.js"></script>
    <!-- <script src="/js/ajaxLogin.js"></script> -->
    <script src="/js/ajaxContentForCell.js"></script>
    <script src="/js/ajaxContentForEventWin.js"></script>
    <script src="/js/cutURLForController.js"></script>
<?= $this->endSection() ?>