<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper login_registration_page">
    <div class="reg_wrapper_confirm">
	  <div class="confirmation_box">
	    <h2><?= lang('Main.Registration completed successfully.') ?></h2>
		<p><?= lang('Main.We have sent you an email, please check your inbox.') ?></p>
	  </div>
	</div>
  </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main_login_registration.js"></script>
    <script src="/js/checkFillingFormRegistration.js"></script>
<?= $this->endSection() ?>