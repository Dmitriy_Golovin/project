<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper login_registration_page">
    <div class="reg_wrapper_confirm">
	  <div class="confirmation_box">
	    <h2><?= lang('Main.Your account has been successfully deleted') ?></h2>
	  </div>
	</div>
  </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
<?= $this->endSection() ?>