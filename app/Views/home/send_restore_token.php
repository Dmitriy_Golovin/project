<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('header') ?>
  <?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
  <div class="content_wrapper login_registration_page">
    <div class="login_reg_wrapper">
      <div class="login_reg_box">

        <?php if ($success) : ?>

            <h3><?= lang('Main.We have sent you an email, please check your inbox.') ?></h3>

        <?php else : ?>

          <?= $this->include('templates/error/single') ?>
          <form name="send_restore_token" method="post" action="/send-restore-token">
            <?= csrf_field() ?>
            <fieldset class="fieldset_box">
              <input id="email_field" type="text" name="email" class="input_login_reg" placeholder="<?= lang('Main.enter email') ?>" tabindex="1">
            </fieldset>
            <div class="button_box">
              <input type="submit" class="button_pass_recovery" value="<?= lang('Main.continue') ?>">
            </div>
          </form>

        <?php endif ?>

      </div>
    </div>
  </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
  <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main_login_registration.js"></script>
    <script src="/js/keyCRForm.js"></script>
    <script src="/js/checkFillingFormLogin.js"></script>
<?= $this->endSection() ?>