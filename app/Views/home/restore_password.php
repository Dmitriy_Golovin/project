<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('header') ?>
  <?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
  <div class="content_wrapper login_registration_page">
    <div class="login_reg_wrapper">
      <div class="login_reg_box">
        <?= $this->include('templates/error/single') ?>
        <form name="restore_password" method="post" action="/restore-password">
          <?= csrf_field() ?>
          <fieldset class="fieldset_box">
            <label class="label_reg_form"><?= lang('Main.enter a new password') ?></label>
            <input type="password" name="password" class="input_login_reg" tabindex="2">
            <div class="notice_elem password"></div>
            <label class="label_reg_form"><?= lang('Main.repeat password') ?></label>
            <input type="password" name="repeat_password" class="input_login_reg" tabindex="3">
            <div class="notice_elem repeat_password"></div>
          </fieldset>
          <div class="button_box">
              <input type="submit" class="button_new_pass" value="<?= lang('Main.submit') ?>" tabindex="6">
          </div>
        </form>
      </div>
    </div>
  </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
  <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main_login_registration.js"></script>
    <script src="/js/keyCRForm.js"></script>
    <script src="/js/checkFillingFormRegistration.js"></script>
<?= $this->endSection() ?>