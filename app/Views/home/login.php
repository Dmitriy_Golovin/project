<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('description') ?>
	<meta name="description" content="Авторизация на сайте. Календарь. Праздники. Праздники России. События, встречи, задачи." />
<?= $this->endSection() ?>

<?= $this->section('canonical') ?>

	<link rel="canonical" href="<?= site_url('login') ?>" />

<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?= $this->include('templates/header_guest') ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper login_registration_page">
      <div class="login_reg_wrapper">
	    <div class="login_reg_box">
	      <?= $this->include('templates/error/single') ?>
	      <h3><?= lang('Main.Login to your account') ?></h3>
		  <p><?= lang('Main.or') ?> <a href="/register"><?= lang('Main.register') ?></a></p>
		  <form name="login_form" method="post" action="/login">
		    <fieldset class="fieldset_box">
	          <input type="text" id="email_field" name="email" class="input_login_reg" placeholder="<?= lang('Main.enter email') ?>" tabindex="1">
	          <input type="password" id="password_field" name="password" class="input_login_reg" placeholder="<?= lang('Main.enter password') ?>" tabindex="2">
		    </fieldset>
		    <div class="notice_elem"></div>
		    <div class="button_box">
	          <input type="button" class="button_login" value="<?= lang('Main.sign in') ?>" tabindex="3">
		      <p class="lost_password"><a href="/send-restore-token"><?= lang('Main.Forgot password?') ?></a></p>
		    </div>
		  </form>
	    </div>
	  </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main_login_registration.js"></script>
    <script src="/js/keyCRForm.js"></script>
    <script src="/js/checkFillingFormLogin.js"></script>
<?= $this->endSection() ?>