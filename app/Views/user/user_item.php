<?php

use Common\Models\ChatMemberModel;

helper('html');

$dataRowNum = (!empty($user->rowNum))
	? 'data-rowNum="' . $user->rowNum . '"'
	: '';

?>

<div class="box_bottom_border user_result_item" data-profile_id="<?= esc($user->user_id) ?>" <?= $dataRowNum ?>>

	<div class="ava_box_result_item">
		<?= anchor('profile/' . $user->user_id, img(['src' => $user->path_to_ava, 'class' => 'ava_result_item'])) ?>
	</div>

	<div class="fullname_box_result_item">
		<?= anchor('profile/' . $user->user_id, $user->fullName) ?>
	</div>

	<div class="button_box_result_item">
		<?php if ($type === 'assign') : ?>

			<span class="task_cancel_user_button not_active"><?= esc(lang('Task.cancel')) ?></span>
			<span class="task_assign_user_button"><?= esc(lang('Task.assign')) ?></span>

		<?php endif; ?>

		<?php if ($type === 'invite') : ?>

			<span class="event_meeting_cancel_user_button not_active"><?= esc(lang('Task.cancel')) ?></span>
			<span class="event_meeting_invite_user_button"><?= esc(lang('Task.invite')) ?></span>

		<?php endif; ?>

		<?php if ($type === 'invite-chat') : ?>

			<div class="user_item_control_block">
				<div class="user_item_control_button_block">
					<span class="chat_cancel_user_button not_active"><?= esc(lang('Task.cancel')) ?></span>
					<span class="chat_invite_user_button"><?= esc(lang('Task.invite')) ?></span>
				</div>
				<div class="user_item_control_select_role_block not_active">
					<span class="user_item_role_label"><?= esc(lang('ChatMember.role')) ?></span>
			  		<select name="user_item_role_select">
			  			<?php foreach (ChatMemberModel::selectRoleLabels() as $key => $value) : ?>

			  				<option value="<?= $key ?>"><?= $value ?></option>

			  			<?php endforeach ?>
			  		</select>
				</div>
			</div>

		<?php endif; ?>

		<?php if ($type === 'chat-member-list') : ?>

			<div class="user_item_chat_role">
				<?= ChatMemberModel::getRoleLabels()[$user->role] ?>
			</div>

		<?php endif ?>

		<?php if ($type === 'file-permission-add') : ?>

			<span class="file_access_add_user_button"><?= esc(lang('Main.add')) ?></span>

		<?php endif ?>

		<?php if ($type === 'file-permission-cancel') : ?>

			<span class="file_access_cancel_user_button"><?= esc(lang('PrivateFilePermission.cancel')) ?></span>

		<?php endif ?>
	</div>
</div>