<?php if ($insert_type === 'renew') : ?>

	<div class="user_query_search" data-userCount="<?= esc($userCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($userResult)) : ?>

		<h2 class="nothing_found"><?= esc(lang('Main.Nothing found')) ?></h2>

	<?php endif ?>

<?php endif ?>

<?php foreach ($userResult as $user) : ?>

	<?php $this->setVar('user', $user) ?>
	<?= $this->include('user/user_item') ?>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>

<?php endif ?>