<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/error_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if (isset($statusUser) && $statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php else : ?>
		<?= $this->include('templates/header_guest') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
	  <div class="bottom_box">
	    <div class="tv_wrapper">
	      <canvas class="tv_screen"></canvas>
		  <canvas class="tv_screen_404"></canvas>
	    </div>
	  </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/screenNoiseErrorPage.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_errorPage.js"></script>
    <script src="/js/cutURLForController.js"></script> 
<?= $this->endSection() ?>