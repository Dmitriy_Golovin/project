<?php

use App\Libraries\DateAndTime\DateAndTime;

helper('html');

?>

<!DOCTYPE HTML>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <?= csrf_meta() ?>
  <?= $this->renderSection('viewport') ?>
  <title><?= esc($title); ?></title>
  <?= $this->renderSection('description') ?>
  <?= $this->renderSection('canonical') ?>
  <link rel="shortcut icon" href="<?= getenv('app.baseURL') ?>img/favicon.ico" type="image/x-icon">
  <?= link_tag('css/user_page.css', 'stylesheet', 'text/css'); ?>
  <?= $this->renderSection('style') ?>
</head>
<body>
  <header>
    <div class="header_container">
      <div class="logo_container"><a href="<?= $logoUrl ?>"><img alt="Home page" class="logo_img" src="/img/logo.png"></a></div>
	  <?= $this->renderSection('header') ?>
	</div>
  </header>
	<?= $this->renderSection('content') ?>
  <footer>
    <div class="footer_container">
      <!-- <div class="logo_container"><img alt="Home page" class="logo_img" src="/img/logo.png"></div> -->
      <div class="footer_menu_container">
        <div class="copyright footer_col"><p>&copy; <?= DateAndTime::now()->getYear() ?>, <?= getenv('siteUrl') ?></p></div>

        <?php if (!empty($statusUser) && $statusUser === 'user'): ?>
    	   
         <div class="contact_information footer_col"><p><a href="/feedback"><?= lang('Main.Feedback') ?></a></p></div>
         <div class="terms_of_use footer_col"><p><a href="/termsofuse"><?= lang('Main.Terms of use') ?></a></p></div>
         <div class="privacy_policy footer_col"><p><a href="/privacy"><?= lang('Main.Privacy policy') ?></a></p></div>

        <?php endif ?>

      </div>
    </div>
  </footer>
  <script src="/js/BroadcastLS/BroadcastLSClass.js"></script>
  <script src="/js/TabSync/TabSyncClass.js"></script>
  <?= $this->renderSection('script') ?>
</body>
</html>