<!DOCTYPE HTML>
<html lang="ru">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title><?= esc($title); ?></title>
  <link rel="shortcut icon" href="/img/favicon.ico" type="image/ico">
  <link rel="stylesheet" href="/css/user_page.css" type="text/css" />
  <link rel="stylesheet" href="/css/not_valid_link.css" type="text/css" />
</head>
<body>
  <header>
    <div class="header_container">
      <div class="logo_container"><a href="<?= $logoUrl ?>"><img alt="Home page" class="logo_img" src="/img/logo.png"></a></div>
	</div>
  </header>
  <div class="content_wrapper">
    <div class="main_box">
      <h1>Не рабочая ссылка</h1>
	  <p><a href="/">вернуться на главную</a></p>
	</div>
  </div>
  <footer>
    <div class="footer_container">
      <div class="logo_container"><img alt="Home page" class="logo_img" src="/img/logo.png"></div>
	  <div class="copyright header_footer_col"><p>&copy; 2018, calendaric.loc</p></div>
	  <div class="contact_information header_footer_col"><p><a href="">Контакты</a></p></div>
	</div>
  </footer>
  <script src="/js/BroadcastLS/BroadcastLSClass.js"></script>
  <script src="/js/TabSync/TabSyncClass.js"></script>
</body>
</html>