<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/not_valid_link.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper login_registration_page">
    <div class="reg_wrapper_confirm">
	  <div class="confirmation_box">
	    <h2>Email адрес подтвержден.</h2>
		<p><a href="/">Вернуться на главную</a>.</p>
	  </div>
	</div>
  </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/redirectToCabinet.js"></script>
<?= $this->endSection() ?>