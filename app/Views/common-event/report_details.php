<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_details_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box" data-type="<?= $type ?>" data-entity_id="<?= $entity_id ?>" data-report_id="<?= $report_id ?>">
            <div class="back_to_event_list_container">
                <span class="back_to_event_list">
                    <a href="<?= $previousUrl ?>"><?= lang('Main.back') ?></a>
                </span>
            </div>
		</div>
    </div>
    <?php
    $this->setVar('titleNotice', lang('Report.Are you sure you want to delete the report?'));
    $this->setVar('acceptNoticeAnchor', '/' . $typeLabel . '/delete-report/' . $entity_id . '/' . $report_id);
    ?>
    <?= $this->include('modal/notice') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/fetch/fetchGetCommonEventReportDetails.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/showCommentWriteField.js"></script>
    <script src="/js/main_report_details.js"></script>
<?= $this->endSection() ?>