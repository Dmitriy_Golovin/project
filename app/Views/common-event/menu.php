<?php

if ($type === 'event') {
	$newInviteAmount = (count($cabinetData['event']) > 0)
		? '<span class="new_invite_amount">' . count($cabinetData['event']) . '</span>'
		: '';
} else if ($type === 'meeting') {
	$newInviteAmount = (count($cabinetData['meeting']) > 0)
		? '<span class="new_invite_amount">' . count($cabinetData['meeting']) . '</span>'
		: '';
}

?>

<?php if ($type === 'event') : ?>
	<div class="event_menu_box">
		<ul>
			<li class="event_menu_item<?= ($subType === 'current') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="current"' ?>>

				<?php if ($subType !== 'current') : ?>
					<a href="/event">
				<?php endif ?>

				<?= lang('Event.events of the current day') ?>
				<span class="span_count current_event_span_count"></span>

				<?php if ($subType !== 'current') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'scheduled') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="scheduled"' ?>>

				<?php if ($subType !== 'scheduled') : ?>
					<a href="/event/scheduled">
				<?php endif ?>

				<?= lang('Event.scheduled events') ?>
				<span class="span_count scheduled_event_span_count"></span>

				<?php if ($subType !== 'scheduled') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'completed') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="completed"' ?>>

				<?php if ($subType !== 'completed') : ?>
					<a href="/event/completed">
				<?php endif ?>

				<?= lang('Event.completed events') ?>
				<span class="span_count completed_event_span_count"></span>

				<?php if ($subType !== 'completed') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'invitations') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="invitations"' ?>>

				<?php if ($subType !== 'invitations') : ?>
					<a href="/event/invitations">
				<?php endif ?>

				<?= lang('Event.invitations') ?>
				<span class="span_count invitations_span_count"><?= $newInviteAmount ?></span>

				<?php if ($subType !== 'invitations') : ?>
					</a>
				<?php endif ?>
			</li>
		</ul>
	</div>
<?php endif ?>

<?php if ($type === 'meeting') : ?>
	<div class="event_menu_box">
		<ul>
			<li class="event_menu_item<?= ($subType === 'current') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="current"' ?>>

				<?php if ($subType !== 'current') : ?>
					<a href="/meeting">
				<?php endif ?>

				<?= lang('Meeting.meetings of the current day') ?>
				<span class="span_count current_meeting_span_count"></span>

				<?php if ($subType !== 'current') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'scheduled') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="scheduled"' ?>>

				<?php if ($subType !== 'scheduled') : ?>
					<a href="/meeting/scheduled">
				<?php endif ?>

				<?= lang('Meeting.scheduled meetings') ?>
				<span class="span_count scheduled_meeting_span_count"></span>

				<?php if ($subType !== 'scheduled') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'completed') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="completed"' ?>>

				<?php if ($subType !== 'completed') : ?>
					<a href="/meeting/completed">
				<?php endif ?>

				<?= lang('Meeting.completed meetings') ?>
				<span class="span_count completed_meeting_span_count"></span>

				<?php if ($subType !== 'completed') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'invitations') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="invitations"' ?>>

				<?php if ($subType !== 'invitations') : ?>
					<a href="/meeting/invitations">
				<?php endif ?>

				<?= lang('Event.invitations') ?>
				<span class="span_count invitations_span_count"><?= $newInviteAmount ?></span>

				<?php if ($subType !== 'invitations') : ?>
					</a>
				<?php endif ?>
			</li>
		</ul>
	</div>
<?php endif ?>

<?php if ($type === 'task') : ?>
	<div class="event_menu_box">
		<ul>
			<li class="event_menu_item<?= ($subType === 'current') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="current"' ?>>

				<?php if ($subType !== 'current') : ?>
					<a href="/task">
				<?php endif ?>

				<?= lang('Task.tasks of the current day') ?>
				<span class="span_count current_task_span_count"></span>

				<?php if ($subType !== 'current') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'scheduled') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="scheduled"' ?>>

				<?php if ($subType !== 'scheduled') : ?>
					<a href="/task/scheduled">
				<?php endif ?>

				<?= lang('Task.scheduled tasks') ?>
				<span class="span_count scheduled_task_span_count"></span>

				<?php if ($subType !== 'scheduled') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'completed') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="completed"' ?>>

				<?php if ($subType !== 'completed') : ?>
					<a href="/task/completed">
				<?php endif ?>

				<?= lang('Task.completed tasks') ?>
				<span class="span_count completed_task_span_count"></span>

				<?php if ($subType !== 'completed') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'overdue') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="overdue"' ?>>

				<?php if ($subType !== 'overdue') : ?>
					<a href="/task/overdue">
				<?php endif ?>

				<?= lang('Task.overdue tasks') ?>
				<span class="span_count overdue_task_span_count"></span>

				<?php if ($subType !== 'overdue') : ?>
					</a>
				<?php endif ?>
			</li>
			<li class="event_menu_item<?= ($subType === 'created') ? ' active_event_menu_item' : '' ?>" <?= 'data-type_' . $type . '="created"' ?>>

				<?php if ($subType !== 'created') : ?>
					<a href="/task/created">
				<?php endif ?>

				<?= lang('Task.created tasks') ?>
				<span class="span_count created_task_span_count"></span>

				<?php if ($subType !== 'created') : ?>
					</a>
				<?php endif ?>
			</li>
		</ul>
	</div>
<?php endif ?>