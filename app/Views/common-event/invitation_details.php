<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_details_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box">
	    	<div class="event_data_container" data-type="<?= $type ?>" data-entity_id="<?= $entity_id ?>" data-entity_invite_status="<?= $inviteStatus ?>"></div>  	
		</div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/showTheResultsOfMatchingInputFromAvailableOnes.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetCommonEventDataInvitationDetails.js"></script>
    <script src="/js/fetch/fetchAcceptInviteCommonEvent.js"></script>
    <script src="/js/fetch/fetchDeclinetInviteCommonEvent.js"></script>
    <script src="/js/fetch/fetchSetViewedInviteStatus.js"></script>
    <script src="/js/main_invitation_details.js"></script>
<?= $this->endSection() ?>