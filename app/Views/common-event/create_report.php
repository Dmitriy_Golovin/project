<?php

$dataAttrReportId = '';

if ($action === $model::SCENARIO_EDIT) {
    $dataAttrReportId = 'data-report_id="' . $report->report_id . '"';
}

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_details_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box" data-type="<?= $type ?>" data-entity_id="<?= $id ?>" data-action="<?= $action ?>" data-max_video="<?= $reportMaxVideo ?>" data-max_image="<?= $reportMaxImage ?>" <?= $dataAttrReportId ?>>
            <div class="back_to_event_list_container">
                <span class="back_to_event_list">
                    <a href="<?= $previousUrl ?>"><?= lang('Main.back') ?></a>
                </span>
            </div>
            <?= $this->include('templates/error/single') ?>
		</div>
    </div>
    <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderTemplateForLoad', []) ?>
    <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderTemplateForLoad', []) ?>
    <?= $this->include('modal/slider_image_modal') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/fetch/fetchGetCommonEventDataSaveReport.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageClass.js"></script>
    <script src="/js/keyCRForm.js"></script>

    <?php if ($action === $model::SCENARIO_EDIT) : ?>

        <script src="/js/fetch/fetchGetCommentList.js"></script>
        <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
        <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
        <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
        <script src="/js/getScrollWidth.js"></script>
        <script src="/js/showCommentWriteField.js"></script>

    <?php endif ?>

    <script src="/js/main_create_report.js"></script>
<?= $this->endSection() ?>