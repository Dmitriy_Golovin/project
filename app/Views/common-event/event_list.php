<?php if ($insert_type === 'renew') : ?>

	<div class="event_query_search" data-itemCount="<?= esc($itemCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($eventResult)) : ?>
		<h3 class="no_events_title"><?= esc(lang('Main.Nothing found')) ?></h3>
	<?php endif ?>

<?php endif ?>

<?php foreach ($eventResult as $item) : ?>

	<?php $this->setVar('item', $item) ?>
	<?= $this->include($type . '/' . $type . '_item') ?>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>
	<?= $this->include('modal/notice') ?>
<?php endif ?>