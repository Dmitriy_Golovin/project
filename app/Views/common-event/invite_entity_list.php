<?php if ($insert_type === 'renew') : ?>

	<div class="invite_entity_query_search" data-entityCount="<?= esc($entityCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($inviteEntityResult)) : ?>

		<h2 class="nothing_found"><?= esc(lang('Main.Nothing found')) ?></h2>

	<?php endif ?>

<?php endif ?>

<?php foreach ($inviteEntityResult as $entity) : ?>

	<div class="invite_entity_result_item" data-entity_id="<?= $entity->{$entityFieldId} ?>" data-rowNum="<?= $entity->rowNum ?>" data-type="<?= $type ?>">
		<div class="invite_entity_name_container"><?= $entity->name ?></div>
		<div class="invite_entity_date_container">
			<div class="invite_entity_date_box">
				<div class="invite_entity_date_start">
					<?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
				</div>
				<div class="invite_date_hyphen">-</div>
				<div class="invite_entity_date_end">
					<?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
				</div>
			</div>
		</div>
		<div class="invite_entity_button_box">
			<span class="invite_entity_button"><?= lang('Main.invite') ?></span>
		</div>
	</div>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>

<?php endif ?>