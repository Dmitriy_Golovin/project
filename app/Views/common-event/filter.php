<?php

use Common\Models\EventGuestUserModel;

?>

<div class="event_filter_box">	
	<div class="filter_name_box">
		<label for="filter_name_field"><?= lang('Main.name') ?></label>
		<input type="text" id="filter_name_field">
	</div>
	<div class="filter_privacy_box">
		<label for="filter_privacy_type_field"><?= lang('Event.privacy type') ?></label>
		<select id="filter_privacy_type_field">

			<?php foreach ($privacyTypeList as $index => $value) : ?>
				<option value="<?= $index ?>"><?= lang('Event.privatePlural.' . $value) ?></option>
			<?php endforeach ?> 

		</select>
	</div>

	<?php if ($type === 'task' && in_array($subType, ['created'])) : ?>

		<div class="task_status_box">
			<label for="filter_task_status_field"><?= lang('Task.task status') ?></label>
			<select id="filter_task_status_field">

				<?php foreach ($taskStatusList as $index => $value) : ?>
					<option value="<?= $index ?>"><?= lang('Task.statusPlural.' . $value) ?></option>
				<?php endforeach ?>

			</select>
		</div>

	<?php endif ?>

	<?php if ($type === 'task' && in_array($subType, ['completed', 'created'])) : ?>

		<div class="review_status_box">
			<label for="filter_review_status_field"><?= lang('Task.review status') ?></label>
			<select id="filter_review_status_field">

				<?php foreach ($reviewStatusList as $index => $value) : ?>
					<option value="<?= $index ?>"><?= lang('Task.reviewStatusPlural.' . $value) ?></option>
				<?php endforeach ?>

			</select>
		</div>

	<?php endif ?>
	
	<?php if ($subType !== 'current') : ?>

		<div class="filter_date_box">
			<span class="filter_date_label"><?= lang('Main.date') ?></span>
			<div class="filter_date_content">
				<div class="filter_date_item_label"><?= lang('Main.from') ?></div>
				<div class="filter_date_item">
					<div class="filter_date_item_value_from"></div>
					<img src="/img/icon_close_red.png" class="filter_date_clear_icon not_active">
					<img src="/img/icon_calendar.png" class="filter_date_icon">
				</div>
				<div class="filter_date_item_label"><?= lang('Main.to (inclusive)') ?></div>
				<div class="filter_date_item">
					<div class="filter_date_item_value_to"></div>
					<img src="/img/icon_close_red.png" class="filter_date_clear_icon not_active">
					<img src="/img/icon_calendar.png" class="filter_date_icon">
				</div>
			</div>
		</div>

	<?php endif ?>

	<?php if (in_array($type, ['event', 'meeting']) && $subType === 'invitations') : ?>

		<div class="filter_invite_status_box">
			<label for="filter_invite_status_field"><?= lang('Event.invitation status') ?></label>
			<select id="filter_invite_status_field">
				<option value=""><?= lang('Event.any') ?></option>
				<?php foreach (EventGuestUserModel::getStatusLabels() as $index => $value) : ?>
					<option value="<?= $index ?>"><?= $value ?></option>
				<?php endforeach ?> 

			</select>
		</div>
		<div class="filter_invite_only_current_and_scheduled_box">
			<input type="checkbox" id="only_current_and_scheduled" checked>
			<label for="only_current_and_scheduled"><?= lang('Event.current and scheduled') ?></label>
		</div>

	<?php endif ?>

	<div class="filter_button_box">
		<span class="filter_apply_button"><?= lang('Main.apply') ?></span>
	</div>
</div>