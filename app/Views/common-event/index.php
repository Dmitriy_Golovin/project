<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">
      	<div class="event_menu_container">
      		<div class="event_type_menu_container">
      			<a href="<?= $createUrl ?>">
      				<div class="add_new_event_block">+</div>
      			</a>
	      		<div class="event_type_block">
	      			<?php if ($type === 'event') : ?>
	      				<?= lang('Event.Events') ?>
	      			<?php elseif ($type === 'meeting') : ?>
	      				<?= lang('Meeting.Meetings') ?>
	      			<?php elseif ($type === 'task') : ?>
	      				<?= lang('Task.Tasks') ?>
	      			<?php endif ?>
	      		</div>
	      		<div class="event_menu_close_container not_active">
		      		<div class="event_menu_close">
		      			<img alt="Close window" class="event_menu_close_icon" src="/img/icon_close_dark.png">
		      		</div>
		      	</div>
	      	</div>
      		
      		<?= $this->include('common-event/menu') ?>
      		<?= $this->include('common-event/filter') ?>
      	</div>
      	<div class="event_content_container" data-type="<?= $type ?>" data-subtype="<?= $subType ?>">
      		<div class="event_content_title">
      			<span class="event_menu_icon_container">
	      			<img class="event_menu_icon" src="/img/icon_burger_dark.png">
	      		</span>
      			<?= $contentTitle ?>
      		</div>
      		<div class="event_content_list">
      			
	      	</div>
      	</div>
	  </div>
    </div>
    <div class="calendar_container">
    	<?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/showTheResultsOfMatchingInputFromAvailableOnes.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/Calendar/CalendarClass.js"></script>
    <script src="/js/fetch/fetchGetEventList.js"></script>
    <script src="/js/fetch/fetchGetTaskList.js"></script>
    <script src="/js/fetch/fetchGetMainEventData.js"></script>
    <script src="/js/fetch/fetchDeleteCommonEvent.js"></script>
    <script src="/js/fetch/fetchAcceptInviteCommonEvent.js"></script>
    <script src="/js/fetch/fetchDeclinetInviteCommonEvent.js"></script>
    <script src="/js/main_event.js"></script>
<?= $this->endSection() ?>