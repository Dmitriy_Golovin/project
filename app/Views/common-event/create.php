<?php

$activeEmptyListGuestCssClass = ((isset($guestData) && !empty($guestData['list'])) || (isset($entity) && !empty($entity->guestUserIdList)))
    ? ' not_active'
    : '';

if (isset($guestData) && !empty($guestData['userIdList'])) {
    $guestUserIdListInputValue = implode(',', $guestData['userIdList']);
} else if (isset($entity) && !empty($entity->guestUserIdList)) {
    $guestUserIdListInputValue = $entity->guestUserIdList;
} else {
    $guestUserIdListInputValue = '';
}

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/event_details_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box" data-type="<?= $type ?>" data-create_type="<?= $createType ?>">
            <?= $this->include('templates/error/single') ?>
	    	<div class="entity_header">
                <?= $contentTitle ?>
            </div>
            <form action="<?= $formAction ?>" method="post">
                <fieldset class="fieldset_box mb_not">
                    <div class="create_event_field_row">
                        <div class="create_event_field_label">
                            <?= lang('Main.name') ?>
                        </div>
                        <div class="create_event_field_value">
                            <input type="text" name="name" class="create_event_field" placeholder="<?= lang('Main.enter a name') ?>" maxlength="150" value="<?= (isset($entity) && !empty($entity->name)) ? $entity->name : '' ?>">
                        </div>
                    </div>
                    <div class="create_event_field_row align_top">
                        <div class="create_event_field_label">
                            <?= lang('Event.date and time') ?>
                        </div>
                        <div class="create_event_field_value">
                            <div class="create_event_date_field_container">
                                <div class="create_event_date_field"></div>
                                <input type="text" name="date" value="<?= (isset($entity) && !empty($entity->date)) ? $entity->date . '000' : '' ?>" hidden>
                                <div class="calendar_icon_container">
                                    <img src="/img/icon_calendar.png" class="calendar_icon">
                                </div>
                                <div class="create_event_time_field"></div>
                                <input type="text" name="time" value="<?= (isset($entity) && !empty($entity->time)) ? $entity->time . '000' : '' ?>" hidden>
                                <div class="time_icon_container">
                                    <img src="/img/icon_time.png" class="time_icon">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="create_event_field_row">
                        <div class="create_event_field_label">
                            <?= lang('Event.duration') ?>
                        </div>
                        <div class="create_event_field_value">
                            <select name="duration" class="create_event_field" value="<?= (isset($entity) && !empty($entity->duration)) ? $entity->duration : '' ?>">
                                <?php
                                foreach ($durationList as $index => $value) {
                                ?>
                                    <option class="option_create_new_event" value="<?= esc($index); ?>"<?= (isset($entity) && (int)$index === (int)$entity->duration) ? 'selected' : '' ?>><?= esc(lang('TimeAndDate.' . $value)); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="create_event_field_row">
                        <div class="create_event_field_label">
                            <?= lang('Event.location') ?>
                        </div>
                        <div class="create_event_field_value">
                            <input type="text" name="location" class="create_event_field" placeholder="<?= lang('Event.enter location') ?>" maxlength="150" value="<?= (isset($entity) && !empty($entity->location)) ? $entity->location : '' ?>">
                        </div>
                    </div>
                    <div class="create_event_field_row align_top">
                        <div class="create_event_field_label">
                            <?= lang('Event.privacy type') ?>
                        </div>
                        <div class="create_event_field_value column_field_list">
                            <span>
                                <input type="radio" id="public_field" name="privacy" <?= (isset($entity) && (int)$entity->privacy === 0) ? 'checked' : 'checked' ?> value="0">
                                <label for="public_field"><?= lang('Event.public') ?></label>
                            </span>
                            <span>
                                <input type="radio" id="private_field" name="privacy" <?= (isset($entity) && (int)$entity->privacy === 1) ? 'checked' : '' ?> value="1">
                                <label for="private_field"><?= lang('Event.private') ?></label>
                            </span>
                        </div>
                    </div>
                    <div class="create_event_field_row align_top periodicity_container">
                        <div class="create_event_field_label">
                            <?= lang('Event.repeat') ?>
                        </div>
                        <div class="create_event_field_value">
                            <select name="periodicity" class="create_event_field">
                                <?php
                                foreach ($periodicityList as $index => $value) {
                                ?>
                                    <option class="option_create_new_event" value="<?= esc($index); ?>" <?= (isset($entity) && (int)$index === (int)$entity->periodicity) ? 'selected' : '' ?>><?= esc(lang('TimeAndDate.' . $value)); ?></option>
                                <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="create_event_field_row reminder_container">
                        <div class="create_event_field_label">
                            <?= lang('Event.reminder') ?>
                        </div>
                        <div class="create_event_field_value row_field_list">
                            <span class="remind_label">
                                <?= lang('Event.remind in') ?>
                            </span>
                            <span>
                                <select name="reminder" class="create_event_field">
                                    <?php
                                    foreach ($reminderList as $index => $value) {
                                    ?>
                                        <option class="option_create_new_event" value="<?= esc($index); ?>" <?= (isset($entity) && (int)$index === (int)$entity->reminder) ? 'selected' : '' ?>><?= esc(lang('TimeAndDate.' . $value)); ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="create_event_field_row align_top">
                        <div class="create_event_field_label">
                            <?= lang('Main.description') ?>
                        </div>
                        <div class="create_event_field_value">
                            <textarea name="description" class="create_event_textarea" rows="16" placeholder="<?= lang('Main.enter description') ?>" ><?= (isset($entity) && !empty($entity->description)) ? $entity->description : '' ?></textarea>
                        </div>
                    </div>
                    <input type="text" name="guestUserIdList" hidden value="<?= $guestUserIdListInputValue ?>">
                    <input type="text" name="timeOffsetSeconds" hidden>
                </fieldset>
                <div class="entity_guest_container">
                    <div class="entity_guest_title">
                        <span><?= lang('Event.participants') ?></span>
                        <span class="add_guest_event_but"><?= lang('Main.add') ?></span>
                    </div>
                    <div class="entity_guest_content">
                        <h3 class="no_events_title<?= $activeEmptyListGuestCssClass ?>"><?= esc(lang('Main.list is empty')) ?></h3>

                        <?php if (isset($guestData) && !empty($guestData['list'])) : ?>

                            <?php foreach ($guestData['list'] as $user) : ?>

                                <div class="box_bottom_border user_result_item" data-profile_id="<?= esc($user->user_id) ?>">
                                    <div class="ava_box_result_item">

                                        <?= anchor('profile/' . $user->user_id, img(['src' => $user->userAva, 'class' => 'ava_result_item'])) ?>

                                    </div>
                                    <div class="fullname_box_result_item">
                                        <a href="/profile/<?= $user->user_id ?>"><?= esc($user->userFullname) ?></a>
                                    </div>
                                    <div class="button_box_result_item">
                                        <span class="event_meeting_cancel_user_button"><?= esc(lang('Task.cancel')) ?></span>
                                    </div>
                                </div>

                            <?php endforeach ?>

                        <?php endif ?>

                        <?php if (isset($entity) && !empty($entity->guestUserIdList)) : ?>
                            <?php $entity->guestUserIdList = explode(',', $entity->guestUserIdList) ?>
                            <?php foreach ($entity->guestUserIdList as $userId) : ?>
                                <?php $user = $userModel->getUserData($userId); ?>


                                <div class="box_bottom_border user_result_item" data-profile_id="<?= esc($user->user_id) ?>">
                                    <div class="ava_box_result_item">

                                        <?= anchor('profile/' . $user->user_id, img(['src' => $user->path_to_ava, 'class' => 'ava_result_item'])) ?>

                                    </div>
                                    <div class="fullname_box_result_item">
                                        <a href="/profile/<?= $user->user_id ?>"><?= esc($user->first_name . ' ' . $user->last_name) ?></a>
                                    </div>
                                    <div class="button_box_result_item">
                                        <span class="event_meeting_cancel_user_button"><?= esc(lang('Task.cancel')) ?></span>
                                    </div>
                                </div>

                            <?php endforeach ?>
                        <?php endif ?>
                    </div>
                </div>
                <div class="create_event_button_container">
                    <input type="button" class="create_event_button_submit" value="<?= lang('Main.save') ?>">
                </div>
            </form>
		</div>
    </div>
    <div class="calendar_container">
        <?php echo view_cell('\Common\Widgets\calendar\CalendarWidget::render', []); ?>
    </div>
    <?php echo view_cell('\Common\Widgets\select_time\SelectTimeWidget::render', []); ?>
    <?= $this->include('modal/add_guest_user') ?>
    <?= $this->include('modal/slider_image_modal') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/showTheResultsOfMatchingInputFromAvailableOnes.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/fetch/fetchGetUserList.js"></script>
    <script src="/js/Calendar/CalendarClass.js"></script>
    <script src="/js/SelectTime/SelectTimeClass.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageClass.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/main_event_create.js"></script>
    <script src="/js/keyCRForm.js"></script>
<?= $this->endSection() ?>