<?php

$fileConfig = config('FileConfig');

$querySearchCssClass = '';

if ($type === $fileConfig->filePageTypeImage) {
	$querySearchCssClass = ' file_query_search_image';
} else if ($type === $fileConfig->filePageTypeVideo) {
	$querySearchCssClass = ' file_query_search_video';
} else if ($type === $fileConfig->filePageTypeDocument) {
	$querySearchCssClass = ' file_query_search_document';
}

?>

<?php if ($insert_type === 'renew') : ?>

	<div class="file_query_search<?= $querySearchCssClass ?>" data-itemCount="<?= esc($fileCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($fileResult)) : ?>
		<h3 class="no_events_title"><?= esc(lang('Main.Nothing found')) ?></h3>
	<?php endif ?>

<?php endif ?>

<?php foreach ($fileResult as $file) : ?>

	<?php if ($type === $fileConfig->filePageTypeImage) : ?>

		<?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
            'file' => $file,
            'deleteBlock' => false,
            'mainCssClass' => 'image_file_container_preview',
        ]) ?>

	<?php endif ?>

	<?php if ($type === $fileConfig->filePageTypeVideo) : ?>

		<?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
            'file' => $file,
            'nameBlock' => false,
            'deleteBlock' => false,
        ]) ?>

	<?php endif ?>

	<?php if ($type === $fileConfig->filePageTypeDocument) : ?>

		<?= view_cell('\Common\Widgets\file\DocumentFileWidget::render', [
            'file' => $file,
            'deleteBlock' => false,
        ]) ?>

	<?php endif ?>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>
	
<?php endif ?>