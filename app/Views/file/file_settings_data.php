<div class="file_settings_data_block">
	<div class="entity_settings_data_row">
		<div class="entity_settings_data_row_label"><?= lang('Main.added') ?></div>
		<div class="entity_settings_data_row_value">
			<?= $fileData->date['date'] . ' ' . $fileData->date['time'] ?>
		</div>
	</div>

	<?php if (!empty($fileData->entityData)) : ?>

		<div class="entity_settings_data_row">
			<div class="entity_settings_data_row_label"><?= $fileData->entityData['entity'] ?></div>
			<div class="entity_settings_data_row_value">
				<a href="<?= $fileData->entityData['href'] ?>">
					<span class="entity_settings_href"><?= $fileData->entityData['name'] ?></span>
				</a>
			</div>
		</div>

	<?php endif ?>

	<div class="entity_settings_data_row">
		<div class="entity_settings_data_row_label"><?= $fileData->privacyLabel ?></div>
		<div class="entity_settings_data_row_value">
			<a href="/file/toggle-permission/<?= $fileData->file_id ?>">
				<span class="entity_settings_privacy_action_button">
					<?= $fileData->privacyValueButton ?>
				</span>
			</a>
		</div>
	</div>
</div>