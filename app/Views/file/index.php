<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box" data-file_type="<?= $type ?>">
      	<div class="file_menu_container">
			<div class="close_menu_container not_active">
				<img alt="Close window" class="close_menu_icon" src="/img/icon_close_dark.png">
			</div>
			<ul>

				<?php if ($type !== $fileConfig->filePageTypeImage) : ?>

					<a href="/file/photo">

				<?php endif ?>

					<li class="<?= ($type === $fileConfig->filePageTypeImage) ? ' active_menu_item' : '' ?>"><?= lang('Main.photoPlural.photo') ?></li>

				<?php if ($type !== $fileConfig->filePageTypeImage) : ?>

					</a>

				<?php endif ?>

				<?php if ($type !== $fileConfig->filePageTypeVideo) : ?>

					<a href="/file/video">

				<?php endif ?>

					<li class="<?= ($type === $fileConfig->filePageTypeVideo) ? ' active_menu_item' : '' ?>"><?= lang('Main.video') ?></li>

				<?php if ($type !== $fileConfig->filePageTypeVideo) : ?>

					</a>

				<?php endif ?>

				<?php if ($type !== $fileConfig->filePageTypeDocument) : ?>

					<a href="/file/document">

				<?php endif ?>

					<li class="<?= ($type === $fileConfig->filePageTypeDocument) ? ' active_menu_item' : '' ?>"><?= lang('Main.documents') ?></li>

				<?php if ($type !== $fileConfig->filePageTypeDocument) : ?>

					</a>

				<?php endif ?>

			</ul>
		</div>
      	<div class="file_content_container">
      		<div class="content_title">
      			<div class="menu_icon_container">
      				<img src="/img/icon_burger_dark.png" class="menu_icon">
      			</div>
      			<?= $contentTitle ?>
      		</div>
      		<div class="content_block">
			  	<div class="file_content_block"></div>
	      	</div>
      	</div>
	  </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_file.js"></script>
    <script src="/js/fetch/fetchGetSelfFileList.js"></script>
    <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/fetch/fetchDeleteFile.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/showCommentWriteField.js"></script>
<?= $this->endSection() ?>