<?php

use Common\Models\FileModel;

$fileContainerCssClass = ((int)$file->type === FileModel::TYPE_DOCUMENT)
	? 'file_settings_file_container_document'
	: 'file_settings_file_container_image_video'

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/file_setting_page.css" type="text/css" />
<?= $this->endSection() ?>


<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>" data-file_privacy_status="<?= $file->privacy_status ?>">
      		<div class="file_settings_header"><?= $contentTitle ?></div>
      		<div class="file_settings_file_container">

	      		<?php if ((int)$file->type === FileModel::TYPE_IMAGE) : ?>

		      		<?= view_cell('\Common\Widgets\file\ImageFileWidget::renderSettings', [
			            'file' => $file,
			            'deleteBlock' => false,
			        ]) ?>

		      	<?php endif ?>

		      	<?php if ((int)$file->type === FileModel::TYPE_VIDEO) : ?>

		      		<?= view_cell('\Common\Widgets\file\VideoFileWidget::renderSettings', [
			            'file' => $file,
			            'deleteBlock' => false,
			        ]) ?>

		      	<?php endif ?>

		      	<?php if ((int)$file->type === FileModel::TYPE_DOCUMENT) : ?>

		      		<?= view_cell('\Common\Widgets\file\DocumentFileWidget::render', [
			            'file' => $file,
			            'deleteBlock' => false,
			        ]) ?>

		      	<?php endif ?>

		    </div>

		    <?php if ((int)$file->type !== FileModel::TYPE_DOCUMENT) : ?>

		    	<div class="file_settings_name_container"><?= $file->original_name ?></div>

		    <?php endif ?>

		    <div class="file_settings_data_container">

		    </div>

		    <?php if ((int)$file->privacy_status === FileModel::PRIVATE_STATUS) : ?>

		    	<div class="user_file_access_container">
		    		<div class="user_file_access_title">
	                    <span><?= lang('File.file access') ?></span>
	                    <span class="add_user_file_access_but"><?= lang('Main.add') ?></span>
	                </div>
	                <div class="user_file_access_content">

	                </div>
		    	</div>

		    <?php endif ?>

      	</div>
    </div>

    <?php if ((int)$file->type === FileModel::TYPE_IMAGE) : ?>

    	<?= $this->include('modal/slider_image_modal') ?>

    <?php endif ?>

    <?php if ((int)$file->privacy_status === FileModel::PRIVATE_STATUS) : ?>

    	<?= $this->include('modal/add_file_permission_user') ?>

    <?php endif ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_file_settings.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageClass.js"></script>
    <script src="/js/fetch/fetchGetFileSettingsData.js"></script>
    <script src="/js/fetch/fetchGetUserList.js"></script>
    <script src="/js/fetch/fetchGetFilePermissionUserList.js"></script>
<?= $this->endSection() ?>