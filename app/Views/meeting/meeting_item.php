<?php

use Common\Models\EventGuestUserModel;

$notViewedInviteCssClass = (isset($item->selfGuestStatus) && (int)$item->selfGuestStatus === EventGuestUserModel::STATUS_NOT_VIEWED && $item->scheduledInvite)
	? ' not_viewed_invite'
	: '';

?>

<div class="event_result_item<?= $notViewedInviteCssClass ?>" <?= (isset($item->rowNum) ? 'data-rowNum="' . $item->rowNum . '"' : '') ?> data-<?= $type ?>_id="<?= $item->entity_id ?>">
	<div class="event_item_content">

		<?php if (in_array($subType, ['current', 'scheduled', 'completed'])) : ?>
			<a href="/<?= $type ?>/details/<?= $item->entity_id ?>">
		<?php endif ?>

		<?php if (in_array($subType, ['invitations'])) : ?>
			<a href="/<?= $type ?>/invitation/<?= $item->entity_id ?>">
		<?php endif ?>

			<div class="event_item_name"><?= $item->name ?></div>
		</a>

		<div class="event_item_content_block">
			<div class="event_item_date_start_container">
				<span class="event_item_label"><?= lang('Event.start') ?></span>
				<span class="event_item_value"><?= $item->date['date'] ?> <?= $item->date['time'] ?></span>
			</div>
			<div class="event_item_date_end_container">
				<span class="event_item_label"><?= lang('Event.end') ?></span>
				<span class="event_item_value"><?= $item->endDate['date'] ?> <?= $item->endDate['time'] ?></span>
			</div>
			<div class="event_item_privacy_container">
				<span class="event_item_label"><?= lang('Event.privacy type') ?></span>
				<span class="event_item_value"><?= $item->privacyValue ?></span>
			</div>

			<?php if (($type === 'event' || $type === 'meeting') && in_array($subType, ['scheduled', 'current'])) : ?>

				<div class="event_item_periodicity_container">
					<span class="event_item_label"><?= lang('Event.repeat') ?></span>
					<span class="event_item_value"><?= $item->periodicityValue ?></span>
				</div>

			<?php endif ?>

			<?php if (($type === 'event' || $type === 'meeting') && in_array($subType, ['scheduled', 'current'])) : ?>

				<div class="event_item_reminder_container">
					<span class="event_item_label"><?= lang('Event.reminder') ?></span>
					<span class="event_item_value"><?= $item->reminderValue ?></span>
				</div>

			<?php endif ?>

			<?php if ($subType === 'invitations') : ?>

				<div class="event_item_owner_container">
					<span class="event_item_label"><?= lang('Event.invitation from') ?></span>
					<div class="event_item_owner_value_container">
						<img src="<?= $item->userAva ?>" class="event_item_owner_value_ava"></img>
						<div class="event_item_owner_value_name"><?= $item->first_name ?><br><?= $item->last_name ?></div>
					</div>
				</div>

				<div class="event_item_invite_status_container">
					<span class="event_item_label"><?= lang('Event.status') ?></span>
					<div class="event_item_accepted_value_container">

						<?php if ((int)$item->selfGuestStatus === EventGuestUserModel::STATUS_ACCEPTED) {
							$statusCssClass = ' invite_status_accept';
						} elseif ((int)$item->selfGuestStatus === EventGuestUserModel::STATUS_DECLINED) {
							$statusCssClass = ' invite_status_decline';
						} elseif ((int)$item->selfGuestStatus === EventGuestUserModel::STATUS_VIEWED) {
							$statusCssClass = ' invite_status_viewed';
						} else {
							$statusCssClass = '';
						} ?>

						<span class="event_item_value invite_status_value<?= $statusCssClass ?>"><?= EventGuestUserModel::getStatusLabels()[$item->selfGuestStatus] ?></span>
					</div>
				</div>

			<?php endif ?>

			<div class="event_item_guest_container">
				<span class="event_item_label"><?= $item->userGuestLabel ?></span>
				<span class="event_item_value"><?= $item->userGuestCount ?></span>
			</div>

			<?php if ($subType === 'completed') : ?>
				<div class="event_item_report_container">
					<span class="event_item_label"><?= lang('Main.report') ?></span>
					<span class="event_item_value">
					<?php if (!empty($item->report_id)): ?>
						<a href="/<?= $type ?>/report/<?= $item->report_id ?>">
							<span class="item_view_report_but"><?= lang('Main.view') ?></span>
						</a>
					<?php else : ?>
						<a href="/<?= $type ?>/create-report/<?= $item->entity_id ?>">
							<span class="item_add_report_but"><?= lang('Main.add') ?></span>
						</a>
					<?php endif ?>
					</span>
				</div>

			<?php endif ?>
		</div>
	</div>
	<div class="event_item_menu">
		<div class="show_event_menu_container">
			<div class="show_event_menu_box">
				<span class="up_menu_box_item"></span>
				<span class="middle_menu_box_item"></span>
				<span class="bottom_menu_box_item"></span>
			</div>
		</div>
		<ul>
			<?php if (($type === 'event' || $type === 'meeting') && $subType !== 'invitations') : ?>
				<?php if ($subType !== 'completed') : ?>
					<li>
						<a href="/<?= $type ?>/edit/<?= $item->{$type . '_id'} ?>">
							<img src="/img/pencil_icon.png">
						</a>
					</li>
				<?php endif ?>
				<li class="delete_event_item_icon_container">
					<img src="/img/cart_red_icon2.png">
				</li>
			<?php endif ?>

			<?php if (($type === 'event' || $type === 'meeting') && $subType === 'invitations' && $item->scheduledInvite) : ?>
				<?php if ((int)$item->selfGuestStatus !== EventGuestUserModel::STATUS_ACCEPTED) : ?>
					<li class="entity_accept_invite_button">
						<img src="/img/selected_icon.png">
					</li>
				<?php endif ?>
				<?php if ((int)$item->selfGuestStatus !== EventGuestUserModel::STATUS_DECLINED) : ?>
					<li class="entity_decline_invite_button">
						<img src="/img/icon_close_red_pad.png">
					</li>
				<?php endif ?>
			<?php endif ?>
		</ul>
	</div>
</div>