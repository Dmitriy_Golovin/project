<?php

$mainEventParamsConfig = config('MainEventParamsConfig');
$reportMaxVideo = $mainEventParamsConfig->reportMaxVideo;
$reportMaxImage = $mainEventParamsConfig->reportMaxImage;

$hideLoadVideoLabelCssClass = '';
$hideLoadImageLabelCssClass = '';
$photoContainerCssClass = '';
$reportNote = ($action === 'edit' && !empty($report->note)) ? $report->note : '';

if ($action === 'edit') {
    $hideLoadVideoLabelCssClass = (count($report->fileList['videoList']) >= (int)$reportMaxVideo)
        ? ' not_active'
        : '';
    $hideLoadImageLabelCssClass = (count($report->fileList['imageList']) >= (int)$reportMaxImage)
        ? ' not_active'
        : '';
    $photoContainerCssClass = (count($report->fileList['imageList']) > 0)
        ? ' mb_10'
        : '';
}

?>

<div class="entity_header">
    <?= $contentTitle ?>
</div>
<div class="entity_name_container"><?= $entity->name ?></div>
<div class="entity_date_container">
    <div class="entity_date_box">
        <div class="entity_date_start">
            <?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
        </div>
        <div class="date_hyphen">-</div>
        <div class="entity_date_end">
            <?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
        </div>
    </div>
</div>
<form id="save_report" action="<?= $formAction ?>" method="post" enctype="multipart/form-data">
    <fieldset class="fieldset_box mb_not">
        <input type="text" name="entity_id" value="<?= $entity_id ?>" hidden>
        <div class="create_report_field_row">
            <div class="create_report_field_label">
                <div class="note_label_container">
                    <?= lang('Main.video') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$reportMaxVideo]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="report_video_container">
                <?php if ($action === 'edit') : ?>

                    <?php foreach ($report->fileList['videoList'] as $item) : ?>
            
                        <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
                            'file' => $item,
                            'nameBlock' => false,
                            'deleteBlock' => true,
                        ]) ?>

                    <?php endforeach ?>

                <?php endif ?>
            </div>
            <div class="create_report_field_value <?= $hideLoadVideoLabelCssClass ?>">
                <input type="file" id="load_report_video" name="report_video" class="load_file" data-error_max_value="<?= lang('Main.maximum {0}', [$reportMaxVideo]) ?>" hidden>
                <label for="load_report_video" class="create_report_load_video"><?= lang('Main.upload video') ?></label>
            </div>
            <div class="notice_elem"></div>
        </div>
        <div class="create_report_field_row">
            <div class="create_report_field_label create_report_field_label_photo">
                <div class="note_label_container">
                    <?= lang('Main.photo') ?>
                    <span class="notice_icon_container">
                        <img class="notice_icon" src="/img/notice_icon_2.png">
                        <div class="notice_label_message not_active not_active_opacity">
                            <?= lang('Main.maximum {0}', [$reportMaxImage]) ?>
                        </div>
                    </span>
                </div>
            </div>
            <div class="report_photo_container<?= $photoContainerCssClass ?>">

                <?php if ($action === 'edit') : ?>

                    <?php foreach ($report->fileList['imageList'] as $item) : ?>
            
                        <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
                            'file' => $item,
                            'deleteBlock' => true,
                            'mainCssClass' => 'image_file_container_preview',
                        ]) ?>

                    <?php endforeach ?>

                <?php endif ?>

            </div>
            <div class="create_report_field_value<?= $hideLoadImageLabelCssClass ?>">
                <input type="file" id="load_report_photo" name="report_photo[]" class="load_file" multiple="true" data-error_max_value="<?= lang('Main.maximum {0}', [$reportMaxImage]) ?>" hidden>
                <label for="load_report_photo" class="create_report_load_photo"><?= lang('Main.upload photo') ?></label>
            </div>
            <div class="notice_elem"></div>
        </div>
        <div class="create_report_field_row">
            <div contenteditable="true" class="report_note_field" data-placeholder="<?= lang('Report.add notes to the report') ?>"><?= $reportNote ?></div>
        </div>
        <div class="create_report_button_box">
            <input type="button" class="create_report_button_save" value="<?= lang('Main.save') ?>">
        </div>
    </fieldset>
    <?php if ($action === 'edit') : ?>
        <input type="hidden" name="deleteFileIdList">
        <input type="text" name="report_id" value="<?= $report->report_id ?>" hidden>
    <?php endif ?>
</form>