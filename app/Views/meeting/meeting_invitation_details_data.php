<?php

use Common\Models\EventGuestUserModel;

if ($entity->statusEntity === SCHEDULED) {
	$statusCssClass = ' entity_status_scheduled';
	$statusValue = lang(ucfirst($entity->type) . '.scheduled');
} else if ($entity->statusEntity === CURRENT) {
	$statusCssClass = ' entity_status_current';
	$statusValue = lang(ucfirst($entity->type) . '.current');
} else if ($entity->statusEntity === COMPLETED) {
	$statusCssClass = ' entity_status_completed';
	$statusValue = lang(ucfirst($entity->type) . '.completed');
}

?>

<div class="back_to_event_list_container">
	<span class="back_to_event_list">
		<a href="/<?= $entity->type ?>/invitations"><?= lang('Main.back to list') ?></a>
	</span>
</div>
<div class="entity_header">
	<?= lang('Meeting.meeting invitation') ?>
</div>
<div class="entity_button_container">

	<?php if ($entity->statusEntity === COMPLETED) : ?>

		<?php if (!empty($entity->report_id)) : ?>
			<a href="">
				<span class="entity_show_report_button"><?= lang('Event.show report') ?></span>
			</a>
		<?php endif ?>

	<?php endif ?>

	<?php if ($entity->statusEntity !== COMPLETED) : ?>
		<?php if (!in_array($entity->selfGuestStatus, [EventGuestUserModel::STATUS_ACCEPTED, EventGuestUserModel::STATUS_DECLINED])) : ?>

			<span class="entity_accept_invite_button"><?= lang('Main.accept') ?></span>
			<span class="entity_decline_invite_button"><?= lang('Main.decline') ?></span>

		<?php endif ?>

		<?php if ((int)$entity->selfGuestStatus === (int)EventGuestUserModel::STATUS_ACCEPTED) : ?>

			<span class="entity_decline_invite_button"><?= lang('Main.decline') ?></span>

		<?php endif ?>

		<?php if ((int)$entity->selfGuestStatus === (int)EventGuestUserModel::STATUS_DECLINED) : ?>

			<span class="entity_accept_invite_button"><?= lang('Main.accept') ?></span>

		<?php endif ?>
	<?php endif ?>
</div>
<div class="entity_name_container"><?= $entity->name ?></div>
<div class="entity_date_container">
	<div class="entity_date_box">
		<div class="entity_date_start">
			<?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
		</div>
		<div class="date_hyphen">-</div>
		<div class="entity_date_end">
			<?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
		</div>
	</div>
</div>
<div class="entity_data_list_container">
	<div class="entity_data_row entity_data_row_status">
		<div class="entity_data_row_item_label"><?= lang('Event.invitation from') ?></div>
		<div class="entity_data_row_item_value">
			<div class="entity_data_owner_value_container">
				<img src="<?= $entity->userAva ?>" class="entity_data_owner_value_ava">
				<div class="entity_data_owner_value_name">
					<?= $entity->first_name ?>
					<br>
					<?= $entity->last_name ?>
				</div>
			</div>
		</div>
	</div>
	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.invitation status') ?></div>

		<?php if ((int)$entity->selfGuestStatus === EventGuestUserModel::STATUS_ACCEPTED) {
			$selfGuestStatusCssClass = ' invite_status_accept';
		} elseif ((int)$entity->selfGuestStatus === EventGuestUserModel::STATUS_DECLINED) {
			$selfGuestStatusCssClass = ' invite_status_decline';
		} elseif ((int)$entity->selfGuestStatus === EventGuestUserModel::STATUS_VIEWED) {
			$selfGuestStatusCssClass = ' invite_status_viewed';
		} else {
			$selfGuestStatusCssClass = '';
		} ?>

		<div class="entity_data_row_item_value invite_status_value<?= $selfGuestStatusCssClass ?>">
			<?= EventGuestUserModel::getStatusLabels()[$entity->selfGuestStatus] ?>
		</div>
	</div>
	<div class="entity_data_row entity_data_row_status">
		<div class="entity_data_row_item_label"><?= lang('Event.event status') ?></div>
		<div class="entity_data_row_item_value<?= $statusCssClass ?>"><?= $statusValue ?></div>
	</div>
	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.privacy type') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->privacyValue ?></div>
	</div>
	<?php if (!empty($entity->location)) : ?>
		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Event.location') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->location ?></div>
		</div>

	<?php endif ?>

	<?php if (!empty($entity->description)) : ?>

	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Main.description') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->description ?></div>
	</div>

	<?php endif ?>
</div>
<div class="entity_guest_container">
	<div class="entity_guest_title"><?= lang('Event.participants') ?></div>
	<div class="entity_guest_content">
		<?php if (empty($entity->guestList)) : ?>
			<h3 class="no_events_title"><?= esc(lang('Main.list is empty')) ?></h3>
		<?php else : ?>
			<?php foreach ($entity->guestList as $item) : ?>

				<div class="box_bottom_border user_result_item" data-profile_id="<?= $item->user_id ?>">
					<div class="ava_box_result_item">
						<a href="/profile/<?= $item->user_id ?>">
							<img src="<?= $item->userAva ?>" class="ava_result_item" alt="">
						</a>
					</div>
					<div class="fullname_box_result_item">
						<a href="/profile/<?= $item->user_id ?>">
							<?= $item->fullName ?>
						</a>
					</div>
				</div>

			<?php endforeach ?>
		<?php endif ?>
	</div>
</div>