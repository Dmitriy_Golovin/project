<?php

use Common\Models\ChatMemberModel;

$activeEmptyListMemberCssClass = (isset($chat) && !empty($chat->chatMember))
	? ' not_active'
	: '';

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/create_chat_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
    	<div class="main_box">
    		<?= $this->include('templates/error/single') ?>
    		<div class="create_chat_title"><?= $contentTitle ?></div>
    		<?php if ($model->scenario === $model::SCENARIO_UPDATE) : ?>
    			<div class="delete_chat_container">
    				<span class="delete_chat_button"><?= lang('Main.delete') ?></span>
    			</div>
    		<?php endif ?>
    		<form id="create_chat_form" action="<?= $formAction ?>" method="post" enctype="multipart/form-data">
    			<fieldset class="fieldset_box mb_not">
    				<div class="create_chat_field_row">
                        <div class="create_chat_field_label">
                            <?= lang('Main.name') ?>
                        </div>
                        <div class="create_chat_field_value">
                            <input type="text" name="chat_name" class="input_create_chat_name" placeholder="<?= lang('Main.enter a name') ?>" maxlength="150" value="<?= ($model->scenario === $model::SCENARIO_UPDATE) ? $chat->title : '' ?>">
                        </div>
                    </div>
                    <div class="create_chat_field_row align_top">
                    	<div class="create_chat_field_label">
                            <?= lang('Main.image') ?>
                        </div>
                        <div class="create_chat_field_value column_field_list">
                        	<input type="file" id="load_file_picture" name="chat_avatar" class="load_file" hidden>
                        	<label for="load_file_picture" class="create_chat_load_file  <?= ($model->scenario === $model::SCENARIO_UPDATE && !empty($chat->chatAva) ? 'not_active' : '') ?>"><?= lang('Main.select image') ?></label>
                        	<div class="notice_elem"></div>
                        	<?php if ($model->scenario === $model::SCENARIO_UPDATE && !empty($chat->chatAva)) : ?>
			    				<div class="create_chat_image_container">
			    					<div class="create_chat_file_control_block">
			    						<span class="create_chat_remove_file_block">
			    							<img class="create_chat_remove_file_pic remove_available_avatar_chat" src="/img/close_.png">
			    						</span>
			    					</div>
			    					<div class="create_chat_image_box">
			    						<img class="create_chat_upload_image" src="<?= $chat->chatAva ?>">
			    					</div>
			    				</div>
			    			<?php endif ?>
                        </div>
                    </div>
    			</fieldset>
    			<?php if ($model->scenario === $model::SCENARIO_UPDATE && !empty($chat->chat_id)) : ?>
		    		<input type="text" name="chat_id" value="<?= $chat->chat_id ?>" hidden>
		    		<input type="text" name="remove_avatar" value="0" hidden>
		    	<?php endif ?>
		    	<input type="text" name="chat_member" hidden>
    		</form>
    		<div class="chat_member_container">
    			<div class="chat_member_title">
                    <span><?= lang('ChatMember.members') ?></span>
                    <span class="add_chat_member_but"><?= lang('Main.add') ?></span>
                </div>
                <div class="chat_member_content">
                	<h3 class="no_events_title<?= $activeEmptyListMemberCssClass ?>"><?= esc(lang('Main.list is empty')) ?></h3>
                	<?php if ($model->scenario === $model::SCENARIO_UPDATE && !empty($chat->chatMember)) : ?>
    					<?php foreach ($chat->chatMember as $member) : ?>
    						<div class="box_bottom_border user_result_item" data-profile_id="<?= $member->user_id ?>">
    							<div class="ava_box_result_item">
    								<a href="/profile/<?= $member->user_id ?>">
    									<img class="ava_result_item" src="<?= $member->userAva ?>">
    								</a>
    							</div>
    							<div class="fullname_box_result_item"><?= $member->fullName ?></div>
    							<div class="button_box_result_item">
    								<div class="user_item_control_block">
    									<?php if ((int)$member->role !== ChatMemberModel::ROLE_CREATER && (int)$member->user_id !== (int)$selfId) : ?>
    										<span class="chat_cancel_user_button"><?= esc(lang('Task.cancel')) ?></span>
    									<?php endif ?>
    								</div>
    								<div class="user_item_control_select_role_block">
    									<span class="user_item_role_label"><?= esc(lang('ChatMember.role')) ?></span>
								  		<select name="user_item_role_select" class="<?= ((int)$member->role === ChatMemberModel::ROLE_CREATER || (int)$member->user_id === (int)$selfId) ? 'not_active_element' : '' ?>">
								  			<?php if ((int)$member->role === ChatMemberModel::ROLE_CREATER) : ?>
								  				<option value="<?= ChatMemberModel::ROLE_CREATER ?>"><?= lang('ChatMember.' .ChatMemberModel::ROLE_CREATER_LABEL) ?></option>
								  			<?php else : ?>
									  			<?php foreach (ChatMemberModel::selectRoleLabels() as $key => $value) : ?>

									  				<option <?= ((int)$member->role === (int)$key) ? 'selected' : '' ?> value="<?= $key ?>"><?= $value ?></option>

									  			<?php endforeach ?>
									  		<?php endif ?>
								  		</select>
    								</div>
    							</div>
    						</div>
    					<?php endforeach ?>
    				<?php endif ?>
                </div>
    		</div>
    		<div class="chat_create_button_box">
	    		<input type="button" class="chat_create_button_save" value="<?= lang('Main.save') ?>">
	    	</div>
		</div>
    </div>
    <?php if ($model->scenario === $model::SCENARIO_UPDATE) : ?>
    	<?= $this->include('modal/notice') ?>
    <?php endif ?>
    <?= $this->include('modal/add_guest_user') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
	<script src="/js/adaptive_user_menu_.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/main_create_chat.js"></script>
	<script src="/js/checkUploadFile.js"></script>
	<script src="/js/fetch/fetchGetUserList.js"></script>
	<script src="/js/keyCRForm.js"></script>
<?= $this->endSection() ?>