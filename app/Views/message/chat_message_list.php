<?php

use Common\Models\FileModel;

$firstNotRead = false;
?>

<?php if ($messageResult['insert_type'] === 'renew') : ?>

	<div class="message_query_search" data-messageCount="<?= esc($messageResult['messageCount']) ?>" data-timestamp="<?= esc($messageResult['timestamp']) ?>">

	<?php if (empty($messageResult['messageResult'])) : ?>

		<div class="welcome_message_block">
			<h4>
				<?= $messageResult['chatCreater']->fullName . ' ' . lang('Chat.created a chat') ?><br>
				<?= ($messageResult['chat']->created_at['date'] . ' ' . $messageResult['chat']->created_at['time']) ?>
			</h4>
		</div>

	<?php endif ?>

<?php endif ?>

<?php foreach($messageResult['messageResult'] as $message) : ?>
	<div class="message_item_box<?= ($message->notReadMessage) ? ' dialog_chat_item_not_read_message' : '' ?><?= ($message->notReadMessage && !$firstNotRead) ? ' first_not_read_message' : '' ?>" data-profile_id="<?= $message->user_id ?>" data-chat_message_id="<?= $message->chat_message_id ?>" data-important_message="<?= $message->importantMessage ?>" data-rowNum="<?= $message->rowNum ?>">
		<?php if ($message->notReadMessage && !$firstNotRead) {
			$firstNotRead = true;
		} ?>
		<div class="message_title">
			<div class="message_ava_box">
				<a href="/profile/<?= $message->user_id ?>">
					<img class="message_ava" src="<?= $message->userAva ?>">
				</a>
			</div>
			<div class="user_name_time_box">
				<a href="/profile/<?= $message->user_id ?>">
					<span><?= $message->fullName ?></span>
				</a>
				<span><?= ($message->date['date'] . ' ' . $message->date['time']) ?></span>
			</div>
			<?php if ($message->importantMessage) : ?>

				<div class="important_message_container">
					<img class="important_message_icon" src="/img/important_message_icon_exp.png">
				</div>

			<?php endif ?>
			<div class="menu_message_container">
				<div class="menu_message_list_container">
					<ul class="menu_message_list">
						<li class="menu_message_item menu_message_important_item"><?= ($message->importantMessage) ? lang('Message.remove from important') : lang('Message.mark as important') ?></li>
						<li class="menu_message_item menu_message_delete_item"><?= lang('Main.delete') ?></li>
					</ul>
				</div>
				<img class="menu_message_icon" src="/img/icon_burger_message_menu.png">
			</div>
		</div>
		<div class="message_box">
			<div class="message_text"><?= $message->text ?></div>
			<?php if (!empty($message->file_list)) : ?>
				<div class="massage_file_container">
					<?php foreach ($message->file_list as $index => $file) {
						$widgetData = ['file' => $file];
						
						if ($file === 'deleted') {
							echo view_cell('\Common\Widgets\file\DeletedFileWidget::render', []);
							continue;
						} else if ($file === 'notAllowed') {
							echo view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []);
							continue;
						} else if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_IMAGE) {
							echo view_cell('\Common\Widgets\file\ImageFileWidget::renderInMessage', $widgetData);
						} else if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_AUDIO) {
							if (isset($message->file_list[(int)$index + 1]) && in_array($message->file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
								$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
							}
							echo view_cell('\Common\Widgets\file\AudioFileWidget::renderInMessage', $widgetData);
						} else if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_VIDEO) {
							echo view_cell('\Common\Widgets\file\VideoFileWidget::renderInMessage', $widgetData);
						} else if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_DOCUMENT) {
							if (isset($message->file_list[(int)$index + 1]) && isset($message->file_list[(int)$index + 1]->type) && in_array($message->file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
								$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
							}
							echo view_cell('\Common\Widgets\file\DocumentFileWidget::renderInMessage', $widgetData);
						}
					} ?>
				</div>
			<?php endif ?>
		</div>
		<div class="chat_message_statistic_block">
			<div class="reaction_message_icon_container">
				<img class="reaction_message_icon" src="/img/emj.png">
			</div>
			<div class="reaction_message_block">
				<?php $this->setVar('reactionList', $message->reactionList) ?>
				<?= $this->include('message/chat_message_reaction_list') ?>
			</div>
			<div class="message_view_statistic">
				<img class="view_statistic_icon" src="/img/eye_icon.png">
				<span class="view_statistic_count"><?= $message->viewCount ?></span>
			</div>
		</div>
	</div>
<?php endforeach ?>

<?php if ($messageResult['insert_type'] === 'renew') : ?>

	</div>

<?php endif ?>