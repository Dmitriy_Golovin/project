<div class="dialog_chat_border dialog_chat_item input_search_item" data-profile_id="<?= $item->user_from ?>"  data-type="dialog">
	<span class="delete_correspondence not_active_opacity">
		<img class="delete_correspondence_pic" src="/img/close_.png">
	</span>
	<a href="/messages/d/<?= $item->user_from ?>">
		<div class="ava_box_dialog_chat">
			<img src="<?= $item->fromAva ?>" class="ava_dialog_chat">
		</div>
		<div class="user_name_and_date_box_dialog_chat">
			<div class="user_name_box_dialog_chat">
				<span><?= $item->first_name ?></span>
				<span><?= $item->last_name ?></span>
			</div>
			<div class="date_box_dialog_chat">
				<span><?= $item->date['date'] ?></span>
				<span><?= $item->date['time'] ?></span>
			</div>
		</div>
		<div class="text_box_dialog_chat <?= ($item->notReadMessage) ? 'dialog_chat_item_not_read_message' : '' ?>">
			<?php if ($item->selfMessage) : ?>
				<img class="ava_self_message_dialog_chat" src="<?= $item->selfAva ?>">
			<?php endif ?>
			<div class="<?= ($item->selfMessage) ? 'text_self_dialog_chat_container' : 'text_not_self_dialog_chat_container' ?>">
				<div class="text_self_dialog_chat">
					<?php if ($item->newFileCount > 0) : ?>
						<span class="new_attachments_count"><?= lang('Message.attachments') . ': ' . $item->newFileCount ?></span><br>
					<?php endif ?>
					<?= $item->text ?>
				</div>
			</div>
		</div>
		<div class="new_count_container">
			<?php if ($item->new_messages_from > 0) : ?>
				<span class="new_messages_count">+<?= $item->new_messages_from ?></span>
			<?php endif ?>
		</div>
	</a>
</div>