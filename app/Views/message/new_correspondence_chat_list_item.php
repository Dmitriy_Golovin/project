<div class="dialog_chat_border dialog_chat_item input_search_item" data-chat_id="<?= $modelResult->chat_id ?>" data-type="chat">
	<span class="delete_correspondence not_active_opacity">
		<img class="delete_correspondence_pic" src="/img/close_.png">
	</span>
	<a href="/messages/cr/<?= $modelResult->chat_id ?>">
		<div class="ava_box_dialog_chat">
			<img src="<?= $modelResult->chatAva ?>" class="ava_dialog_chat">
		</div>
		<div class="user_name_and_date_box_dialog_chat">
			<div class="user_name_box_dialog_chat">
				<span><?= $modelResult->title ?></span>
			</div>
			<div class="date_box_dialog_chat">
				<span><?= $modelResult->date['date'] ?></span>
				<span><?= $modelResult->date['time'] ?></span>
			</div>
		</div>
		<div class="text_box_dialog_chat <?= ($modelResult->notReadMessage) ? 'dialog_chat_item_not_read_message' : '' ?>">
			<img class="ava_self_message_dialog_chat" src="<?= $modelResult->userAva ?>">
			<div class="text_self_dialog_chat_container">
				<div class="text_self_dialog_chat">
					<?php if ($modelResult->fileCount > 0) : ?>
						<span class="new_attachments_count"><?= lang('Message.attachments') . ': ' . $modelResult->fileCount ?></span><br>
					<?php endif ?>
					<?= $modelResult->text ?>
				</div>
			</div>
		</div>
		<div class="new_count_container">
			<?php if ($modelResult->newMessageCount > 0) : ?>
				<span class="new_messages_count">+<?= $modelResult->newMessageCount ?></span>
			<?php endif ?>
		</div>
	</a>
</div>