<?php if (!empty($reactionList)) : ?>
		<ul>
		<?php foreach ($reactionList as $reaction) : ?>
			<li class="chat_message_reaction_item_container">
				<span class="chat_message_reaction_pic"><?= esc($reaction->reaction) ?></span>
				<span class="chat_message_reaction_count"><?= esc($reaction->count) ?></span>
			</li>
		<?php endforeach ?>
		</ul>
<?php endif ?>