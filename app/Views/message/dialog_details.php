<?php

use Common\Models\ChatMemberModel;

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/chat_room_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      	<div class="main_box">
      		<div class="title_messages_block">
      			<div class="back_to_all_correspondence_button">
				  	<a href="/messages"><?= lang('Main.go back') ?></a>
				</div>
				<div class="correspondence_fullname_or_chat_room" data-profile_id="<?= $id ?>"><?= $userWithFullname ?></div>
      		</div>
      		<div class="messages_block">
      		</div>
      		<div class="write_message_box">
      			<div class="write_message_text_box">
		      		<div class="text_message" contenteditable="true"></div>
			  	</div>
			  	<div class="count_of_files_block"></div>
				<div class="available_file_message_block">
			      	<div class="available_image_message_block"></div>
			      	<div class="available_audio_message_block"></div>
			      	<div class="available_video_message_block"></div>
			      	<div class="available_document_message_block"></div>
			    </div>
			    <?php echo view_cell('\Common\Widgets\emoji\EmojiWidget::render', ['cssBoxClass' => 'emoji_relative_box']); ?>
			    <div class="write_message_button_box">
			    	<div class="send_message_button_box">
			    		<div class="emoji_block file_icon_for_message_message_page">
		        			<img class="emoji_icon" src="/img/emj.png">
		      			</div>
		      			<div class="add_file_block file_icon_for_message_message_page">
		      				<img class="add_file_icon" src="/img/add_picture_icon.png" data-block_modal="select_picture_box_modal" data-message_selected_file="available_image_message_block" data-file_type="1" title="Загрузить изображение">
		      			</div>
		      			<div class="add_file_block file_icon_for_message_message_page">
		      				<img class="add_file_icon" src="/img/add_video_icon.png" data-block_modal="select_video_box_modal" data-message_selected_file="available_video_message_block" data-file_type="3" title="Загрузить видеозапись">
		      			</div>
		      			<div class="add_file_block file_icon_for_message_message_page">
		        			<img class="add_file_icon" src="/img/add_file_icon.png" data-block_modal="select_document_box_modal" data-message_selected_file="available_document_message_block" data-file_type="4" title="Загрузить документ">
		      			</div>
		      			<input type="button" class="write_message_send_message_button" value="отправить">
			    	</div>
			    </div>
      		</div>
      	</div>
    </div>
	<?= $this->include('modal/select_file') ?>
    <?= $this->include('modal/slider_image_modal') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
	<script src="/js/adaptive_user_menu_.js"></script>
	<script src="/js/main.js"></script>
    <script src="/js/activateFileToMessageBlockModal.js"></script>
    <script src="/js/ajaxGetUserPublicFiles.js"></script>
    <script src="/js/prepareFilesForMessage.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/preloaderActionCircle.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/FileElement/AudioFileElementClass.js"></script>
    <script src="/js/FileElement/VideoFileElementClass.js"></script>
    <script src="/js/FileElement/DocumentFileElementClass.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/FileElement/AudioFileExistElement.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageClass.js"></script>
    <script src="/js/fetch/fetchToggleImportantMessage.js"></script>
    <script src="/js/fetch/fetchDeleteDialogMessage.js"></script>
    <script src="/js/fetch/fetchMarkDialogMessageAsRead.js"></script>
    <script src="/js/main_dialog_details.js"></script>
<?= $this->endSection() ?>