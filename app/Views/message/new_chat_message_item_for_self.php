<?php

use Common\Models\FileModel;

$importantMessageIdList = !empty($message->important_chat_member_ids) ? json_decode($message->important_chat_member_ids) : [];
$importantMessage = in_array($sender->chat_member_id, $importantMessageIdList) ? 1 : 0;
$viewMessageIdList = !empty($message->who_viewed_chat_member_ids) ? json_decode($message->who_viewed_chat_member_ids) : [];
$viewCount = !empty($viewMessageIdList) ? count($viewMessageIdList) : 0;

?>

<div class="message_item_box <?= ($message->notReadMessage) ? 'dialog_chat_item_not_read_message' : '' ?>" data-profile_id="<?= $sender->user_id ?>" data-chat_message_id="<?= $message->chat_message_id ?>" data-important_message="<?= $importantMessage ?>">
	<div class="message_title">
		<div class="message_ava_box">
			<a href="/profile/<?= $sender->user_id ?>">
				<img class="message_ava" src="<?= $sender->path_to_ava ?>">
			</a>
		</div>
		<div class="user_name_time_box">
			<a href="/profile/<?= $sender->user_id ?>">
				<span><?= $sender->fullName ?></span>
			</a>
			<span><?= ($message->date['date'] . ' ' . $message->date['time']) ?></span>
		</div>
		<div class="menu_message_container">
			<div class="menu_message_list_container">
				<ul class="menu_message_list">
					<li class="menu_message_item menu_message_important_item"><?= ($importantMessage) ? lang('Message.remove from important') : lang('Message.mark as important') ?></li>
					<li class="menu_message_item menu_message_delete_item"><?= lang('Main.delete') ?></li>
				</ul>
			</div>
			<img class="menu_message_icon" src="/img/icon_burger_message_menu.png">
		</div>

		<?php if ($importantMessage) : ?>

			<div class="important_message_container">
				<img class="important_message_icon" src="/img/important_message_icon_exp.png">
			</div>

		<?php endif ?>

	</div>
	<div class="message_box">
		<div class="message_text"><?= $message->text ?></div>
		<?php if (!empty($file_list)) : ?>
			<div class="massage_file_container">
				<?php foreach ($file_list as $index => $file) {
					$widgetData = ['file' => $file];

					if ((int)$file->type === FileModel::TYPE_IMAGE) {
						echo view_cell('\Common\Widgets\file\ImageFileWidget::renderInMessage', $widgetData);
					}

					if ((int)$file->type === FileModel::TYPE_AUDIO) {
						if (isset($file_list[(int)$index + 1]) && in_array($file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
							$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
						}
						echo view_cell('\Common\Widgets\file\AudioFileWidget::renderInMessage', $widgetData);
					}

					if ((int)$file->type === FileModel::TYPE_VIDEO) {
						echo view_cell('\Common\Widgets\file\VideoFileWidget::renderInMessage', $widgetData);
					}

					if ((int)$file->type === FileModel::TYPE_DOCUMENT) {
						if (isset($file_list[(int)$index + 1]) && in_array($file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
							$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
						}
						echo view_cell('\Common\Widgets\file\DocumentFileWidget::renderInMessage', $widgetData);
					}
				} ?>
			</div>
		<?php endif ?>
	</div>
	<div class="chat_message_statistic_block">
		<img class="reaction_message_icon" src="/img/emj.png">
		<div class="reaction_message_block"></div>
		<div class="message_view_statistic">
			<img class="view_statistic_icon" src="/img/eye_icon.png">
			<span class="view_statistic_count"><?= $viewCount ?></span>
		</div>
	</div>
</div>