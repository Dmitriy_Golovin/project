<?php

use Common\Models\FileModel;

?>

<div class="message_item_box <?= ($message->notReadMessage) ? 'dialog_chat_item_not_read_message' : '' ?>" data-profile_id="<?= $message->user_id ?>" data-chat_message_id="<?= $message->chat_message_id ?>" data-important_message="<?= $message->importantMessage ?>">
	<div class="message_title">
		<div class="message_ava_box">
			<a href="/profile/<?= $message->user_id ?>">
				<img class="message_ava" src="<?= $message->userAva ?>">
			</a>
		</div>
		<div class="user_name_time_box">
			<a href="/profile/<?= $message->user_id ?>">
				<span><?= $message->fullName ?></span>
			</a>
			<span><?= ($message->date['date'] . ' ' . $message->date['time']) ?></span>
		</div>
		<div class="menu_message_container">
			<div class="menu_message_list_container">
				<ul class="menu_message_list">
					<li class="menu_message_item menu_message_important_item"><?= ($message->importantMessage) ? lang('Message.remove from important') : lang('Message.mark as important') ?></li>
					<li class="menu_message_item menu_message_delete_item"><?= lang('Main.delete') ?></li>
				</ul>
			</div>
			<img class="menu_message_icon" src="/img/icon_burger_message_menu.png">
		</div>

		<?php if ($message->importantMessage) : ?>

			<div class="important_message_container">
				<img class="important_message_icon" src="/img/important_message_icon_exp.png">
			</div>

		<?php endif ?>

	</div>
	<div class="message_box">
		<div class="message_text"><?= $message->text ?></div>
		<?php if (!empty($message->file_list)) : ?>
			<div class="massage_file_container">
				<?php foreach ($message->file_list as $index => $file) {
					$widgetData = ['file' => $file];

					if ($file === 'deleted') {
						echo view_cell('\Common\Widgets\file\DeletedFileWidget::render', []);
					}

					if ($file === 'notAllowed') {
						echo 'notAllowed';
					}

					if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_IMAGE) {
						echo view_cell('\Common\Widgets\file\ImageFileWidget::renderInMessage', $widgetData);
					}

					if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_AUDIO) {
						if (isset($message->file_list[(int)$index + 1]) && in_array($message->file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
							$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
						}
						echo view_cell('\Common\Widgets\file\AudioFileWidget::renderInMessage', $widgetData);
					}

					if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_VIDEO) {
						echo view_cell('\Common\Widgets\file\VideoFileWidget::renderInMessage', $widgetData);
					}

					if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_DOCUMENT) {
						if (isset($message->file_list[(int)$index + 1]) && in_array($message->file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
							$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
						}
						echo view_cell('\Common\Widgets\file\DocumentFileWidget::renderInMessage', $widgetData);
					}
				} ?>
			</div>
		<?php endif ?>
	</div>
	<div class="chat_message_statistic_block">
		<img class="reaction_message_icon" src="/img/emj.png">
		<div class="reaction_message_block"></div>
		<div class="message_view_statistic">
			<img class="view_statistic_icon" src="/img/eye_icon.png">
			<span class="view_statistic_count"><?= $message->viewCount ?></span>
		</div>
	</div>
</div>