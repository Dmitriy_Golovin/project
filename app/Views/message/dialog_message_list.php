<?php

use Common\Models\FileModel;

$firstNotRead = false;
?>

<?php if ($messageResult['insert_type'] === 'renew') : ?>

	<div class="message_query_search" data-messageCount="<?= esc($messageResult['messageCount']) ?>" data-timestamp="<?= esc($messageResult['timestamp']) ?>">

	<?php if (empty($messageResult['messageResult'])) : ?>

		<div class="welcome_message_block">
			<h4>
				<?= lang('Message.No messages') ?>
			</h4>
		</div>

	<?php endif ?>

<?php endif ?>

<?php foreach($messageResult['messageResult'] as $message) : ?>
	<?php $this->setVar('message', $message); ?>
	<?= $this->include('message/new_dialog_message_item') ?>
<?php endforeach ?>

<?php if ($messageResult['insert_type'] === 'renew') : ?>

	</div>

<?php endif ?>