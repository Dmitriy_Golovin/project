<?php foreach ($modelResult as $item) : ?>
	<?php if ($item->messageType === 'dialog') : ?>
		<?php $this->setVar('item', $item); ?>
		<?= $this->include('message/new_correspondence_dialog_list_item') ?>
	<?php endif ?>
	<?php if ($item->messageType === 'chat') : ?>
		<div class="dialog_chat_border dialog_chat_item input_search_item" data-chat_id="<?= $item->chat_id ?>" data-type="chat">
			<span class="delete_correspondence not_active_opacity">
				<img class="delete_correspondence_pic" src="/img/close_.png">
			</span>
			<a href="/messages/cr/<?= $item->chat_id ?>">
				<div class="ava_box_dialog_chat">
					<img src="<?= $item->chatAva ?>" class="ava_dialog_chat">
				</div>
				<div class="user_name_and_date_box_dialog_chat">
					<div class="user_name_box_dialog_chat">
						<span><?= $item->title ?></span>
					</div>
					<div class="date_box_dialog_chat">
						<span><?= $item->date['date'] ?></span>
						<span><?= $item->date['time'] ?></span>
					</div>
				</div>
				<div class="text_box_dialog_chat <?= ($item->notReadMessage) ? 'dialog_chat_item_not_read_message' : '' ?>">
					<img class="ava_self_message_dialog_chat" src="<?= $item->userAva ?>">
					<div class="text_self_dialog_chat_container">
						<div class="text_self_dialog_chat">
							<?php if ($item->fileCount > 0) : ?>
								<span class="new_attachments_count"><?= lang('Message.attachments') . ': ' . $item->fileCount ?></span><br>
							<?php endif ?>
							<?= $item->text ?>
						</div>
					</div>
				</div>
				<div class="new_count_container">
					<?php if ($item->newMessageCount > 0) : ?>
						<span class="new_messages_count">+<?= $item->newMessageCount ?></span>
					<?php endif ?>
				</div>
			</a>
		</div>
	<?php endif ?>
<?php endforeach; ?>