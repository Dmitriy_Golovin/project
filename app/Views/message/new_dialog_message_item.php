<?php

use Common\Models\FileModel;

$firstNotRead = false;

if ((int)$message->status === 1 && !$firstNotRead && !$message->selfMessage) {
	$firstNotReadCssClass = ' first_not_read_message';
	$firstNotRead = true;
} else {
	$firstNotReadCssClass = '';
} 

$dataRowNum = (isset($message->rowNum))
	? 'data-rowNum="' . $message->rowNum . '"'
	: '';
?>

<div class="message_item_box<?= ($message->self_message_not_read) ? ' self_message_not_read' : '' ?><?= $firstNotReadCssClass ?><?= ((int)$message->status === 1 && !$message->selfMessage) ? ' dialog_chat_item_not_read_message' : '' ?>" data-profile_id="<?= $message->user_from ?>" data-message_id="<?= $message->message_id ?>" data-important_message="<?= $message->important ?>" <?= $dataRowNum ?>>
	
	<div class="message_title">
		<div class="message_ava_box">
			<a href="/profile/<?= $message->user_from ?>">
				<img class="message_ava" src="<?= $message->user_from_ava ?>">
			</a>
		</div>
		<div class="user_name_time_box">
			<a href="/profile/<?= $message->user_from ?>">
				<span><?= $message->user_from_fullname ?></span>
			</a>
			<span><?= ($message->date['date'] . ' ' . $message->date['time']) ?></span>
		</div>
		<?php if ($message->important) : ?>

			<div class="important_message_container">
				<img class="important_message_icon" src="/img/important_message_icon_exp.png">
			</div>

		<?php endif ?>
		<div class="menu_message_container">
			<div class="menu_message_list_container">
				<ul class="menu_message_list">
					<li class="menu_message_item menu_message_important_item"><?= ($message->important) ? lang('Message.remove from important') : lang('Message.mark as important') ?></li>
					<li class="menu_message_item menu_message_delete_item"><?= lang('Main.delete') ?></li>
				</ul>
			</div>
			<img class="menu_message_icon" src="/img/icon_burger_message_menu.png">
		</div>
	</div>
	<div class="message_box">
		<div class="message_text"><?= $message->text ?></div>
		<?php if (!empty($message->file_list)) : ?>
			<div class="massage_file_container">
				<?php foreach ($message->file_list as $index => $file) {
				$widgetData = ['file' => $file];

				if ($file === 'deleted') {
					echo view_cell('\Common\Widgets\file\DeletedFileWidget::render', []);
					continue;
				}

				if ($file === 'notAllowed') {
					echo view_cell('\Common\Widgets\file\AccessDeniedFileWidget::render', []);
					continue;
				}

				if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_IMAGE) {
					echo view_cell('\Common\Widgets\file\ImageFileWidget::renderInMessage', $widgetData);
				}

				if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_AUDIO) {
					if (isset($message->file_list[(int)$index + 1]) && in_array($message->file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
						$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
					}
					echo view_cell('\Common\Widgets\file\AudioFileWidget::renderInMessage', $widgetData);
				}

				if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_VIDEO) {
					echo view_cell('\Common\Widgets\file\VideoFileWidget::renderInMessage', $widgetData);
				}

				if (isset($file->file_id) && (int)$file->type === FileModel::TYPE_DOCUMENT) {
					if (isset($message->file_list[(int)$index + 1]) && in_array($message->file_list[(int)$index + 1]->type, [FileModel::TYPE_AUDIO, FileModel::TYPE_DOCUMENT])) {
						$widgetData['containerCssBorderBottom'] = 'dialog_chat_border';
					}
					echo view_cell('\Common\Widgets\file\DocumentFileWidget::renderInMessage', $widgetData);
				}
			} ?>
			</div>
		<?php endif ?>
	</div>
</div>