<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_event_meeting_details_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
        <div class="main_box" data-profile_id="<?= $pageId ?>" data-type="<?= $type ?>" data-entity_id="<?= $entity->{$entityFieldId} ?>" data-report_id="<?= $entity->report_id ?>">
            <div class="event_content_block"></div>
        </div>
	</div>
    <?= $this->include('modal/show_guest_user') ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_profile_report_details.js"></script>
    <script src="/js/fetch/fetchGetParticipantsProfileMainEventList.js"></script>
    <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/Modal/ModalClass.js"></script>
    <script src="/js/Modal/ModalSliderImageWithCommentClass.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/showCommentWriteField.js"></script>
<?= $this->endSection() ?>