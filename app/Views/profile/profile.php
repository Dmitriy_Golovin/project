<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">
      	<?= $this->include('profile/menu') ?>
      	<div class="profile_content_container">
      		<div class="content_title">
      			<div class="menu_icon_container">
      				<img src="/img/icon_burger_dark.png" class="menu_icon">
      			</div>
      			<?= lang('Main.profile') ?>
      		</div>
      		<div class="content_block">
      			<div class="profile_data_box">
		      		<div class="block_avatar">
		      			<img src="<?= $pathToAvaPage ?>" class="avatar_image">
		      		</div>
		      		<div class="profile_data">
					  	<div class="profile_name">
					  		<?= $profileFullName ?>
					  	</div>
					  	<div class="profile_country_city_box">
					  		<span class="profile_country_city_caption">страна:</span>
					  		<span class="profile_country_city_value"><?= $profileCountry ?></span>
					  	</div>
					  	<div class="profile_country_city_box">
					  		<span class="profile_country_city_caption">город:</span>
					  		<span class="profile_country_city_value"><?= $profileCity ?></span>
					  	</div>
					</div>
					<div class="profile_button_box">
				  		<span data-profile_ID="<?=$pageID?>" class="profile_button message_button"><?= lang('Main.Message') ?></span>
				  		<span data-profile_ID="<?=$pageID?>" class="profile_button invite_button"><?= lang('Main.Invite') ?></span>

				  		<?php if (isset($friendLinks[$pageID])) : ?>

				  			<span data-profile_ID="<?= $pageID ?>" data-user_relations="<?= $friendLinks[$pageID]['user_relations'] ?>" data-ws_tab_sync_to_do="<?= $friendLinks[$pageID]['ws_tab_sync_to_do'] ?>" class="main_profile_button friend_button<?= $friendLinks[$pageID]['main_profile_button_css'] ?>"><?= $friendLinks[$pageID]['button_value'] ?></span>

					  	<?php endif ?>
				  	</div>
				</div>
			  	<div class="profile_content_box">
			  		<div class="about_self"><?= $profileAboutSelf ?></div>
			  	</div>
	      	</div>
      	</div>
	  </div>
	</div>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('modal/write_message') ?>
		<?= $this->include('modal/event_meeting_invite') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_profile.js"></script>
    <script src="/js/activateWriteNewmessageBox.js"></script>
    <script src="/js/fetch/fetchSendFriendRequest.js"></script>
    <script src="/js/ajaxGetUserPublicFiles.js"></script>
    <script src="/js/prepareFilesForMessage.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/addEmojiToMessageFieldModal.js"></script>
    <script src="/js/preloaderActionCircle.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/FileElement/AudioFileElementClass.js"></script>
    <script src="/js/FileElement/VideoFileElementClass.js"></script>
    <script src="/js/FileElement/DocumentFileElementClass.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/eventOrMeetingInvite.js"></script>
<?= $this->endSection() ?>