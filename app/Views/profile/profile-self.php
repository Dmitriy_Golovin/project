<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_self_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      	<div class="main_box">
  			<div class="profile_data_box">
	      		<div class="block_avatar">
	      			<img src="<?= $pathToAvaPage ?>" class="avatar_image">
	      		</div>
	      		<div class="profile_data">
				  	<div class="profile_name">
				  		<?= $profileFullName ?>
				  	</div>
				  	<div class="profile_country_city_box">
				  		<span class="profile_country_city_caption">страна:</span>
				  		<span class="profile_country_city_value"><?= $profileCountry ?></span>
				  	</div>
				  	<div class="profile_country_city_box">
				  		<span class="profile_country_city_caption">город:</span>
				  		<span class="profile_country_city_value"><?= $profileCity ?></span>
				  	</div>
				</div>
			</div>
		  	<div class="profile_content_box">
		  		<span class="add_button_about_self"><?= $profileChangeAboutSelf ?></span>
		  		<div class="about_self"><?= $profileAboutSelf ?></div>
		  		<div class="block_add_about_self not_active">
					<textarea class="field_add_about_self"></textarea>
					<div class="button_block_about_self">
				  		<div class="button_block_about_self">
							<span class="save_button_about_self"><?= lang('Main.save') ?></span>
							<span class="cancel_button_about_self"><?= lang('Main.cancel') ?></span>
				  		</div>
					</div>
			  	</div>
		  	</div>
  		</div>
  	</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_profile_self.js"></script>
    <script src="/js/ajaxProfileAddAboutSelf.js"></script>
    <script src="/js/activateWriteNewmessageBox.js"></script>
    <script src="/js/ajaxSendFriendRequest.js"></script>
    <script src="/js/ajaxGetUserPublicFiles.js"></script>
    <script src="/js/prepareFilesForMessage.js"></script>
    <script src="/js/ajaxLoadFile.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/addEmojiToMessageFieldModal.js"></script>
    <script src="/js/preloaderActionCircle.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/FileElement/AudioFileElementClass.js"></script>
    <script src="/js/FileElement/VideoFileElementClass.js"></script>
    <script src="/js/FileElement/DocumentFileElementClass.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
<?= $this->endSection() ?>