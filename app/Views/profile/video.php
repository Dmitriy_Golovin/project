<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box" data-page_id="<?= $pageId ?>" data-profile_id="<?= $pageId ?>" data-file_type="<?= $type ?>">
      	<?= $this->include('profile/menu') ?>
      	<div class="profile_content_container profile_file_content">
            <div class="profile_user_block">
                <div class="profile_user_ava_box">
                    <img src="<?= $user->path_to_ava ?>" class="profile_user_ava" alt="">
                </div>
                <div class="profile_user_fullname_box">
                    <?= $user->first_name . ' ' . $user->last_name ?>
                </div>
            </div>
      		<div class="content_title">
      			<div class="menu_icon_container">
      				<img src="/img/icon_burger_dark.png" class="menu_icon">
      			</div>
      			<?= $contentTitle ?>
      		</div>
      		<div class="content_block">
      			
	      	</div>
      	</div>
	  </div>
	</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_profile_file.js"></script>
    <script src="/js/fetch/fetchGetFileList.js"></script>
    <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/showCommentWriteField.js"></script>
<?= $this->endSection() ?>