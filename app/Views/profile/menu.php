<div class="profile_menu_container">
	<div class="close_menu_container not_active">
		<img alt="Close window" class="close_menu_icon" src="/img/icon_close_dark.png">
	</div>
	<ul>

		<?php if ($activeMenu !== $profileConfig->profileActiveMenu) : ?>

			<a href="/profile/<?= $pageId ?>">

		<?php endif ?>

			<li class="<?= ($activeMenu === $profileConfig->profileActiveMenu) ? ' active_menu_item' : '' ?>"><?= lang('Main.profile') ?></li>

		<?php if ($activeMenu !== $profileConfig->profileActiveMenu) : ?>

			</a>

		<?php endif ?>

		<?php if ($activeMenu !== $profileConfig->friendsActiveMenu) : ?>

			<a href="/profile/<?= $pageId ?>/friends">

		<?php endif ?>

			<li class="<?= ($activeMenu === $profileConfig->friendsActiveMenu) ? ' active_menu_item' : '' ?>"><?= lang('Main.friends') ?></li>

		<?php if ($activeMenu !== $profileConfig->friendsActiveMenu) : ?>

			</a>

		<?php endif ?>

		<?php if ($activeMenu !== $profileConfig->eventsActiveMenu) : ?>

			<a href="/profile/<?= $pageId ?>/events">

		<?php endif ?>

			<li class="<?= ($activeMenu === $profileConfig->eventsActiveMenu) ? ' active_menu_item' : '' ?>"><?= lang('Main.events') ?></li>

		<?php if ($activeMenu !== $profileConfig->eventsActiveMenu) : ?>

			</a>

		<?php endif ?>

		<?php if ($activeMenu !== $profileConfig->meetingsActiveMenu) : ?>

			<a href="/profile/<?= $pageId ?>/meetings">

		<?php endif ?>

			<li class="<?= ($activeMenu === $profileConfig->meetingsActiveMenu) ? ' active_menu_item' : '' ?>"><?= lang('Main.meetings') ?></li>

		<?php if ($activeMenu !== $profileConfig->meetingsActiveMenu) : ?>

			</a>

		<?php endif ?>

		<?php if ($activeMenu !== $profileConfig->photoActiveMenu) : ?>

			<a href="/profile/<?= $pageId ?>/photo">

		<?php endif ?>

			<li class="<?= ($activeMenu === $profileConfig->photoActiveMenu) ? ' active_menu_item' : '' ?>"><?= lang('Main.photoPlural.photo') ?></li>

		<?php if ($activeMenu !== $profileConfig->photoActiveMenu) : ?>

			</a>

		<?php endif ?>

		<?php if ($activeMenu !== $profileConfig->videoActiveMenu) : ?>

			<a href="/profile/<?= $pageId ?>/video">

		<?php endif ?>

			<li class="<?= ($activeMenu === $profileConfig->videoActiveMenu) ? ' active_menu_item' : '' ?>"><?= lang('Main.video') ?></li>

		<?php if ($activeMenu !== $profileConfig->videoActiveMenu) : ?>

			</a>

		<?php endif ?>

	</ul>
</div>