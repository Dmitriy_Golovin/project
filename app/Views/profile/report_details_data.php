<?php

use Common\Models\EventGuestUserModel;

if ($entity->statusEntity === SCHEDULED) {
	$statusCssClass = ' entity_status_scheduled';
	$statusValue = lang(ucfirst($entity->type) . '.scheduled');
	$backHrefList = 'scheduled';
} else if ($entity->statusEntity === CURRENT) {
	$statusCssClass = ' entity_status_current';
	$statusValue = lang(ucfirst($entity->type) . '.current');
	$backHrefList = '';
} else if ($entity->statusEntity === COMPLETED) {
	$statusCssClass = ' entity_status_completed';
	$statusValue = lang(ucfirst($entity->type) . '.completed');
	$backHrefList = 'completed';
}

?>

<div class="profile_user_block">
    <div class="profile_user_ava_box">
        <img src="<?= $entity->userAva ?>" class="profile_user_ava" alt="">
    </div>
    <div class="profile_user_fullname_box">
    	<?= $entity->first_name . ' ' . $entity->last_name ?>
    </div>
</div>
<div class="profile_entity_name_container"><?= lang('Event.' . $entity->type) . ' - ' . $entity->name ?></div>
<div class="entity_date_container">
	<div class="entity_date_box">
		<div class="entity_date_start">
			<?= $entity->date['dayOfWeek'] . ' ' . $entity->date['date'] . ' ' . $entity->date['time'] ?>
		</div>
		<div class="date_hyphen">-</div>
		<div class="entity_date_end">
			<?= $entity->endDate['dayOfWeek'] . ' ' . $entity->endDate['date'] . ' ' . $entity->endDate['time'] ?>
		</div>
	</div>
</div>
<div class="entity_data_list_container">
	<div class="entity_data_row entity_data_row_status">
		<div class="entity_data_row_item_label"><?= lang('Event.status') ?></div>
		<div class="entity_data_row_item_value<?= $statusCssClass ?>"><?= $statusValue ?></div>
	</div>
	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.privacy type') ?></div>
		<div class="entity_data_row_item_value"><?= $entity->privacyValue ?></div>
	</div>

	<?php if (!empty($entity->location)) : ?>
		
		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Event.location') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->location ?></div>
		</div>

	<?php endif ?>

	<?php if (!empty($entity->description)) : ?>

		<div class="entity_data_row">
			<div class="entity_data_row_item_label"><?= lang('Main.description') ?></div>
			<div class="entity_data_row_item_value"><?= $entity->description ?></div>
		</div>

	<?php endif ?>

	<div class="entity_data_row">
		<div class="entity_data_row_item_label"><?= lang('Event.participants') ?></div>
		<div class="entity_data_row_item_value">
			<?= $entity->userGuestCount ?>

			<?php if ((int)$entity->userGuestCount > 0) : ?>

				<span class="profile_entity_show_guest_button">
					<?= lang('Main.show') ?>
				</span>

			<?php endif ?>
		</div>
	</div>
</div>
<div class="profile_report_details_video_container<?= (!empty($entity->fileList['videoList'])) ? ' mb_5' : '' ?>">

	<?php foreach ($entity->fileList['videoList'] as $item) : ?>

        <?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
            'file' => $item,
            'nameBlock' => false,
            'deleteBlock' => false,
        ]) ?>

    <?php endforeach ?>

</div>

<div class="profile_report_details_photo_container<?= (!empty($entity->fileList['imageList'])) ? ' mb_5' : '' ?>">

	<?php foreach ($entity->fileList['imageList'] as $item) : ?>
            
        <?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
            'file' => $item,
            'deleteBlock' => false,
            'mainCssClass' => 'image_file_container_preview',
        ]) ?>

    <?php endforeach ?>

</div>
<div class="profile_report_details_comment_container">
    <?= view_cell('\Common\Widgets\comment\CommentWidget::render', [
        'commentData' => $entity->commentData,
        'writeField' => true,
    ]) ?>
</div>