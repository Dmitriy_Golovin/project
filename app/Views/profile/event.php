<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box" data-page_id="<?= $pageId ?>" data-profile_id="<?= $pageId ?>" data-type="<?= $type ?>" data-subType="<?= $subType ?>">
      	<?= $this->include('profile/menu') ?>
      	<div class="profile_content_container">
            <div class="profile_user_block">
                <div class="profile_user_ava_box">
                    <img src="<?= $user->path_to_ava ?>" class="profile_user_ava" alt="">
                </div>
                <div class="profile_user_fullname_box">
                    <?= $user->first_name . ' ' . $user->last_name ?>
                </div>
            </div>
      		<div class="content_title">
      			<div class="menu_icon_container">
      				<img src="/img/icon_burger_dark.png" class="menu_icon">
      			</div>
      			<?= $contentTitle ?>
      		</div>
      		<div class="content_block">
      			<div class="profile_submenu_button_block profile_submenu_event_button_block">

                    <?php if ((int)$subType !== $mainEventParamsConfig->subTypeProfilePageCurrentCompleted) : ?>
                        <a href="/profile/<?= $pageId ?>/events">
                    <?php endif ?>

                    <span class="profile_submenu_button <?= ((int)$subType === $mainEventParamsConfig->subTypeProfilePageCurrentCompleted) ? ' profile_submenu_button_active' : '' ?>"><?= lang('Main.current and complete') ?></span>

                    <?php if ((int)$subType !== $mainEventParamsConfig->subTypeProfilePageCurrentCompleted) : ?>
                        </a>
                    <?php endif ?>

                    <?php if ((int)$subType !== $mainEventParamsConfig->subTypeProfilePageScheduled) : ?>
                        <a href="/profile/<?= $pageId ?>/events-scheduled">
                    <?php endif ?>

                    <span class="profile_submenu_button profile_submenu_button_last_item<?= ((int)$subType === $mainEventParamsConfig->subTypeProfilePageScheduled) ? ' profile_submenu_button_active' : '' ?>"><?= lang('Main.scheduled') ?></span>

                    <?php if ((int)$subType !== $mainEventParamsConfig->subTypeProfilePageScheduled) : ?>
                        </a>
                    <?php endif ?>

                </div>
                <div class="event_content_block"></div>
	      	</div>
      	</div>
	  </div>
	</div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_profile_event.js"></script>
    <script src="/js/fetch/fetchGetProfileMainEventList.js"></script>

    <!-- <script src="/js/fetch/fetchGetVideoViewModal.js"></script>
    <script src="/js/fetch/fetchGetImageSliderViewModal.js"></script>
    <script src="/js/fetch/fetchSendComment.js"></script>
    <script src="/js/fetch/fetchGetCommentList.js"></script>
    <script src="/js/FileElement/VideoFileExistElement.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/getScrollWidth.js"></script>
    <script src="/js/showCommentWriteField.js"></script> -->
<?= $this->endSection() ?>