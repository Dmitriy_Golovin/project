<?php if ($insert_type === 'renew') : ?>

	<div class="profile_event_query_search" data-itemCount="<?= esc($profileEventCount) ?>" data-timestamp="<?= esc($timestamp) ?>">

	<?php if (empty($profileEventResult)) : ?>
		<h3 class="no_events_title"><?= esc(lang('Main.Nothing found')) ?></h3>
	<?php endif ?>

<?php endif ?>

<?php foreach ($profileEventResult as $item) : ?>

	<div class="profile_event_result_item" data-entity_id="<?= $item->{$entityFieldId} ?>" data-rowNum="<?= $item->rowNum ?>">
		<div class="profile_event_title"><?= esc($item->name) ?></div>
		<div class="profile_event_date_box">
			<div class="profile_event_date_start">
				<?= $item->date['dayOfWeek'] . ' ' . $item->date['date'] . ' ' . $item->date['time'] ?>
			</div>
			<div class="date_hyphen">-</div>
			<div class="profile_event_date_end">
				<?= $item->endDate['dayOfWeek'] . ' ' . $item->endDate['date'] . ' ' . $item->endDate['time'] ?>
			</div>
		</div>
		<div class="profile_event_participants_box"><?= lang('Event.participants') . ': ' . $item->userGuestCount ?></div>

		<?php if (!empty($item->location)) : ?>

			<div class="profile_event_location_box"><?= esc($item->location) ?></div>

		<?php endif ?>

		<?php if (!empty($item->description)) : ?>

			<div class="profile_event_description_box"><?= esc($item->description) ?></div>

		<?php endif ?>

		<?php if (empty($item->report_id) && $item->isComplete) : ?>

			<div class="profile_event_no_report_box"><?= lang('Report.No report') ?></div>

		<?php else : ?>

			<?php if ($item->isComplete) : ?>

				<div class="profile_event_no_report_box"><?= lang('Main.Report') ?></div>

			<?php endif ?>

			<div class="profile_event_report_box">

				<?php if (!empty($item->reportFileList['videoList'])) : ?>

					<div class="profile_event_video_container">

						<?php foreach ($item->reportFileList['videoList'] as $file) : ?>

							<?= view_cell('\Common\Widgets\file\VideoFileWidget::renderPreview', [
				                'file' => $file,
				                'nameBlock' => false,
				                'deleteBlock' => false,
				                'videoElMaxheight' => '200px !important',
				            ]) ?>

						<?php endforeach ?>

					</div>

				<?php endif ?>

				<?php if (!empty($item->reportFileList['imageList'])) : ?>

					<div class="profile_event_image_container">

						<?php foreach ($item->reportFileList['imageList'] as $file) : ?>

							<?= view_cell('\Common\Widgets\file\ImageFileWidget::renderPreview', [
				                'file' => $file,
				                'deleteBlock' => false,
				                'mainCssClass' => 'image_file_container_preview_small',
				            ]) ?>

						<?php endforeach ?>

					</div>

				<?php endif ?>

				<?php if (!empty($item->reportNote)) : ?>

					<div class="profile_event_report_note_box"><?= $item->reportNote ?></div>

				<?php endif ?>

			</div>

		<?php endif ?>

		<div class="profile_event_comment_button_box">

			<?php if (!empty($item->report_id) && $item->isComplete) : ?>

				<div class="comment_icon_container">
					<img class="comment_icon" src="/img/comment_icon.png">
				</div>
				<div class="main_comment_amount"><?= $item->commentCount ?></div>

			<?php endif ?>

			<span class="profile_event_details_button"><a href="/profile/<?= $profile_id ?>/<?= $entityType ?>/<?= $item->{$entityFieldId} ?>"><?= lang('Main.more') ?></a></span>
		</div>
	</div>

<?php endforeach ?>

<?php if ($insert_type === 'renew') : ?>

	</div>
	
<?php endif ?>