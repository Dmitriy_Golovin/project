<?php

use Common\Models\FriendModel;

?>

<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/profile_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box" data-page_id="<?= $pageId ?>" data-type="<?= $menuType ?>">
      	<?= $this->include('profile/menu') ?>
      	<div class="profile_content_container">
      		<div class="profile_user_block">
                <div class="profile_user_ava_box">
                    <img src="<?= $user->path_to_ava ?>" class="profile_user_ava" alt="">
                </div>
                <div class="profile_user_fullname_box">
                    <?= $user->first_name . ' ' . $user->last_name ?>
                </div>
            </div>
      		<div class="content_title">
      			<div class="menu_icon_container">
      				<img src="/img/icon_burger_dark.png" class="menu_icon">
      			</div>
      			<?= $contentTitle ?>
      		</div>
      		<div class="content_block">
      			<div class="profile_submenu_button_block">

			 	 	<?php if ((int)$menuType !== FriendModel::TYPE_FRIENDS) : ?>
			 	 		<a href="/profile/<?= $pageId ?>/friends">
			 	 	<?php endif ?>

			 	 	<span class="profile_submenu_button profile_submenu_button_friends<?= ((int)$menuType === FriendModel::TYPE_FRIENDS) ? ' profile_submenu_button_active' : '' ?>"><?= lang('Main.friends') . ' ' . $friendsAmount->friends ?></span>

			 	 	<?php if ((int)$menuType !== FriendModel::TYPE_FRIENDS) : ?>
			 	 		</a>
			 	 	<?php endif ?>

			 	 	<?php if ((int)$menuType !== FriendModel::TYPE_SUBSCRIBERS) : ?>
			 	 		<a href="/profile/<?= $pageId ?>/friends-subscribers">
			 	 	<?php endif ?>

			 	 	<span class="profile_submenu_button profile_submenu_button_subscribers profile_submenu_button_last_item<?= ((int)$menuType === FriendModel::TYPE_SUBSCRIBERS) ? ' profile_submenu_button_active' : '' ?>"><?= lang('Friend.subscribers') . ' ' . $friendsAmount->subscribers ?></span>

			 	 	<?php if ((int)$menuType !== FriendModel::TYPE_SUBSCRIBERS) : ?>
			 	 		</a>
			 	 	<?php endif ?>

			  	</div>
			  	<div class="profile_friend_search_block">
			  		<input type="text" name="search_string" class="search_friends_field" autocomplete="off" placeholder="<?= lang('Main.search') ?>">
			  	</div>
			  	<div class="friend_content_block"></div>
	      	</div>
      	</div>
	  </div>
	</div>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('modal/write_message') ?>
		<?= $this->include('modal/event_meeting_invite') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/editAboutSelf.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_friends.js"></script>
    <script src="/js/fetch/fetchGetFriendList.js"></script>
    <script src="/js/activateWriteNewmessageBox.js"></script>
    <script src="/js/ajaxSendFriendRequest.js"></script>
    <script src="/js/ajaxGetUserPublicFiles.js"></script>
    <script src="/js/prepareFilesForMessage.js"></script>
    <script src="/js/ajaxLoadFile.js"></script>
    <script src="/js/checkUploadFile.js"></script>
    <script src="/js/cutURLForController.js"></script>
    <script src="/js/addEmojiToMessageFieldModal.js"></script>
    <script src="/js/preloaderActionCircle.js"></script>
    <script src="/js/FileElement/FileElementClass.js"></script>
    <script src="/js/FileElement/ImageFileElementClass.js"></script>
    <script src="/js/FileElement/AudioFileElementClass.js"></script>
    <script src="/js/FileElement/VideoFileElementClass.js"></script>
    <script src="/js/FileElement/DocumentFileElementClass.js"></script>
    <script src="/js/FileElement/MediaElementControlMixin.js"></script>
    <script src="/js/eventOrMeetingInvite.js"></script>
<?= $this->endSection() ?>