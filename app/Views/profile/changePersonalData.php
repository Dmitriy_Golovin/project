<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/change_personal_data.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
      <div class="main_box">
		<div class="personal_data_title">
		  <h2><?= lang('Main.Personal data') ?></h2>
		</div>
		<div class="change_user_data_box personal_data">
		  <div class="firstname_elem">
			<p class="item_pers_data title_for_item">Имя</p>
			<p class="show_hide_firstname title_for_item qqq not_empty"><?=$firstName?></p>
			<div class="edit_profile_inp show_hide_firstname not_active">
			  <input type="text" name="reg_firstname" class="input_edit">
		    </div>
			<img alt="edit" class="edit_icon" src="/img/edit_icon.png" data-show-hide="show_hide_firstname">
		  </div>
		  <div class="lastname_elem">
	        <p class="item_pers_data title_for_item">Фамилия</p>
	        <p class="show_hide_lastname title_for_item qqq not_empty"><?=$lastName?></p>
		    <div class="edit_profile_inp show_hide_lastname not_active">
		      <input type="text" name="reg_lastname" class="input_edit">
		    </div>
		    <img alt="edit" class="edit_icon" src="/img/edit_icon.png" data-show-hide="show_hide_lastname">
	      </div>
		  <div class="country_elem">
	        <p class="item_pers_data title_for_item">Страна</p>
		    <p class="show_hide_country title_for_item qqq" <?php if (!empty($country_id)) echo 'data-countryId=' . $country_id; ?>><?=$country?></p>
		    <div class="edit_profile_inp show_hide_country not_active">
		      <select name="reg_country" class="input_edit">
		        <option data-countryId="" disabled selected>Выберите страну</option>

		        <?php foreach ($countryList as $country) : ?>
	          	  <option data-countryId="<?= esc($country->country_id) ?>"><?= esc($country->title) ?></option>
	          	<?php endforeach ?>

		      <select>
		    </div>
		    <img alt="edit" class="edit_icon" src="/img/edit_icon.png" data-show-hide="show_hide_country">
	      </div>
		  <div class="city_elem">
			<p class="item_pers_data title_for_item">Город</p>
			<p id="city_value" class="show_hide_city title_for_item qqq" <?php if (!empty($city_id)) echo 'data-cityId=' . $city_id; ?>><?=$city?></p>
			<div class="edit_profile_inp show_hide_city not_active">
			  <select name="reg_city" class="input_edit">
		      </select>
			</div>
			<img alt="edit" class="edit_icon" src="/img/edit_icon.png" data-show-hide="show_hide_city">
		  </div>
		  <div class="avatar_elem">
			<p class="item_pers_data title_for_item qqq">Аватар</p>
			<!--<input type="file" name="avatar" id="files" class="input_edit_ava">-->
			<p class="title_for_item">Изменить аватар</p>
			<span id="elem_avatar_id" data-avatar_id="" hidden></span>
			<img alt="edit" class="edit_icon avatar_edit_icon" src="/img/edit_icon.png">
		  </div>
		  <div class="notice_elem_ava"></div>
		  <div class="button_box_edit_profile">
			<input type="button" class="button_edit_profile_submit" value="Сохранить изменения" tabindex="3">
			<div class="waiting_icon button_edit_profile_submit not_active">
			  <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
			</div>
		  </div>
		</div>
		<div class="personal_data_title">
		  <h2><?= lang('Main.Settings') ?></h2>
		</div>
		<div class="change_user_data_box user_settings">
			<div class="work_day_start">
				<p class="item_pers_data title_for_item"><?= lang('Main.Start of the working week') ?></p>
				<p class="show_hide_work_day_start title_for_item qqq not_empty"><?= lang('TimeAndDate.' . $dayOfWeekList[$userSetting->work_week_start]) ?></p>
				<div class="edit_profile_inp show_hide_work_day_start not_active">
				  <select name="work_week_start" class="input_edit">

			        <?php foreach ($dayOfWeekList as $index => $day) : ?>
		          	  <option data-day_value="<?= $index ?>"<?= ((int)$index === (int)$userSetting->work_week_start) ? 'selected' : '' ?>><?= lang('TimeAndDate.' . $day) ?></option>
		          	<?php endforeach ?>

			      <select>
			    </div>
				<img alt="edit" class="edit_icon" src="/img/edit_icon.png" data-show-hide="show_hide_work_day_start">
		  	</div>
		  	<div class="work_days_amount">
				<p class="item_pers_data title_for_item"><?= lang('Main.Work days amount') ?></p>
				<p class="show_hide_work_days_amount title_for_item qqq not_empty"><?= $userSetting->work_days_amount ?></p>
				<div class="edit_profile_inp show_hide_work_days_amount not_active">
				  <input type="text" name="work_days_amount" class="input_edit" value="<?= $userSetting->work_days_amount ?>">
			    </div>
				<img alt="edit" class="edit_icon" src="/img/edit_icon.png" data-show-hide="show_hide_work_days_amount">
		  	</div>
		  	<div class="button_box_edit_profile">
				<input type="button" class="button_user_setting_submit" value="Сохранить изменения" tabindex="3">
				<div id="waitingBoxSettingSendData" class="waiting_icon button_user_setting_submit not_active">
				  <span class="waiting_icon_span"></span>
		          <span class="waiting_icon_span"></span>
		          <span class="waiting_icon_span"></span>
				</div>
			 </div>
		</div>
		<div class="personal_data_title">
		  <h2><?= lang('Main.Safety') ?></h2>
		</div>
		<div class="change_user_data_box password_and_safety_edit">
	      <div class="current_password">
	        <p class="item_pers_data title_for_item">Введите текущий пароль:</p>
			<div class="current_password_inp_container">
		      <input type="password" class="input_edit current_password_inp" readonly>
			  <input type="submit" class="search_submit current_password_submit" value="">
		    </div>
			<div class="notice_elem">неверный пароль</div>
	      </div>
		  <div class="edit_email_elem not_active_element email_change pass_saf_active_edit_box">
	        <p class="item_pers_data title_for_item">Email</p>
	        <p class="show_hide_email title_for_item not_empty"><?=$email?></p>
		    <img alt="edit" class="edit_icon pass_saf_icon_button" src="/img/edit_icon.png" data-pass-saf-show="email_change">
	      </div>
		  <div class="new_email_elem email_change pass_saf_change">
			<p class="title_change_pass_saf hide_elem" data-default-content="Введите новый email:">Введите новый email:</p>
			<input type="text" name="new_email" class="input_edit hide_elem">
			<div class="button_change_email_left button_change_email pass_saf_cancel_button hide_elem" data-pass-saf-show="email_change"></div>
		    <div class="button_change_email_right button_change_email hide_elem" data-pass-saf-show="email_change"></div>
			<div class="button_change_right waiting_icon not_active">
			  <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
			</div>
		  </div>
		  <div class="edit_password_elem not_active_element password_change pass_saf_active_edit_box">
	        <p class="item_pers_data title_for_item">Пароль</p>
	        <p class="show_hide_edit_password title_for_item qqq">Изменить пароль</p>
		    <img alt="edit" class="edit_icon pass_saf_icon_button" src="/img/edit_icon.png" data-pass-saf-show="password_change">
	      </div>
		  <div class="new_password_elem password_change pass_saf_change">
	        <p class="title_change_pass_saf new_pass hide_elem" data-default-content="Новый пароль:">Новый пароль:</p>
		    <input type="password" name="new_password" class="input_edit hide_elem">
		    <p class="title_change_pass_saf new_pass_again hide_elem" data-default-content="Новый пароль повторно:">Новый пароль повторно:</p>
		    <input type="password" name="repeat_new_password" class="input_edit hide_elem">
		    <div class="button_change_password_left button_change_password pass_saf_cancel_button hide_elem" data-pass-saf-show="password_change"></div>
		    <div class="button_change_password_right button_change_password hide_elem" data-pass-saf-show="password_change"></div>
			<div class="button_change_right waiting_icon not_active">
			  <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
	          <span class="waiting_icon_span"></span>
			</div>
	      </div>
	      <div class="delete_profile_box not_active_element">
	      	<span class="delete_profile_button"><?= lang('Main.delete account') ?></span>
	      </div>
		</div>
	  </div>
	</div>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('modal/change_avatar') ?>
		<?= $this->include('modal/notice') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
	<script src="/js/setNavTableHeaderContainerDimensions.js"></script>
	<script src="/js/ajaxGetCitiesForSearch.js"></script>
	<script src="/js/personalMenu.js"></script>
	<script src="/js/adaptive_user_menu_.js"></script>
	<script src="/js/activateChangeAvatarModalBox.js"></script>
	<script src="/js/main.js"></script>
	<script src="/js/editProfile.js"></script>
	<script src="/js/modalWinTools.js"></script>
	<script src="/js/changeButton.js"></script>
	<script src="/js/main_changePersonalData.js"></script>
	<script src="/js/ajaxSendEditData.js"></script>
	<script src="/js/ajaxModalPasswordAndSafety.js"></script>
	<script src="/js/ajaxLoadAvatar.js"></script>
	<script src="/js/ajaxSelectAvatar.js"></script>
	<script src="/js/ajaxGetUserPublicFiles.js"></script>
	<script src="/js/checkUploadFile.js"></script>
	<script src="/js/preloaderActionCircle.js"></script>
	<script src="/js/FileElement/FileElementClass.js"></script>
<?= $this->endSection() ?>