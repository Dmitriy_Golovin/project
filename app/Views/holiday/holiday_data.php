<h1 class="holiday_name"><?= esc($modelResult->holiday_day) ?></h1>
<p class="holiday_date"><?= esc($modelResult->holiday_date) ?></p>
<div class="holiday_description">

	<?php if ($modelResult->imageUrl) : ?>

		<div class="holiday_image_container">
			<img alt="<?= $modelResult->fileTitle ?>" title="<?= $modelResult->fileTitle ?>" class="holiday_image" src="<?= $modelResult->imageUrl ?>">
		</div>

	<?php endif ?>

	<?= $modelResult->description ?>
</div>

<?php if (!empty($modelResult->link)) : ?>

	<p class="origin"><span><?= esc(lang('Main.The material is taken from the site')) ?></span> <a class="origin_link" href="<?= esc($modelResult->link) ?>"><?= $modelResult->linkLabel ?></a></p>

<?php endif ?>