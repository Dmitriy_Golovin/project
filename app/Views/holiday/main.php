<?= $this->extend('layouts/main') ?>

<?= $this->section('viewport') ?>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?= $this->endSection() ?>

<?php if (!empty($modelResult)) : ?>

	<?= $this->section('description') ?>

		<?php if (!empty($modelResult->holiday_day)) : ?>

			<meta name="description" content="<?= $modelResult->holiday_day ?>" />

		<?php endif ?>

	<?= $this->endSection() ?>

<?php endif ?>

<?= $this->section('canonical') ?>

	<link rel="canonical" href="<?= site_url($canonical) ?>" />

<?= $this->endSection() ?>


<?= $this->section('style') ?>
	<link rel="stylesheet" href="/css/holiday_page.css" type="text/css" />
<?= $this->endSection() ?>

<?= $this->section('header') ?>
	<?php if ($statusUser === 'user'): ?>
		<?= $this->include('templates/header_user') ?>
	<?php else : ?>
		<?= $this->include('templates/header_guest') ?>
	<?php endif; ?>
<?= $this->endSection() ?>

<?= $this->section('content') ?>
	<div class="content_wrapper">
	  <div class="left_box">
	    <div class="holiday_search_box">
	      <h2>Поиск праздников</h2>
		  <select class="holiday_search_select">
		    <option disabled selected>Выберите страну</option>
		    <option data-countryCode="ru">Россия</option>
		  </select>
		  <input type="text" name="" class="holiday_search_input not_active_element" placeholder="Название праздника">
		  <input type="button" class="holiday_search_submit not_active_element" value="Поиск">
	    </div>
	  </div>
	  <div class="main_box">
	  	<?= $this->include('templates/error/single') ?>
	  	
	    <?php if ($content !== null): ?>
			<?= $this->include('holiday/' . $content) ?>
		<?php endif ?>
		
	  </div>
	  <div class="right_box">
	  </div>
    </div>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
    <script src="/js/setNavTableHeaderContainerDimensions.js"></script>
    <script src="/js/changeButton.js"></script>
    <script src="/js/checkFillingFormLogin.js"></script>
    <script src="/js/ajaxLogin.js"></script>
    <script src="/js/personalMenu.js"></script>
    <script src="/js/closeModalCabinetPage.js"></script>
    <script src="/js/adaptive_user_menu_.js"></script>
    <script src="/js/main.js"></script>
    <script src="/js/main_holiday.js"></script>
    <script src="/js/cutURLForController.js"></script>
<?= $this->endSection() ?>