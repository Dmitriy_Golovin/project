<?php if (empty($modelResult)) : ?>

	<h2 class="nothing_found"><?= esc(lang('Main.Nothing found')) ?></h2>

<?php else : ?>

	<?php foreach ($modelResult as $holiday) : ?>

		<div class="holiday_box_search search_result_item">

			<h3 class="holiday_name_search">

				<?= anchor('/holiday/' . $holiday->link, esc($holiday->holiday_day)) ?>

			</h3>

			<p class="holiday_date"><?= esc($holiday->holiday_date) ?></p>

		</div>

	<?php endforeach ?>

<?php endif ?>