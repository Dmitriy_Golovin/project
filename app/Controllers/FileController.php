<?php

namespace App\Controllers;

use Common\Models\FileModel;
use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\file\FileListByTypeForm;
use App\Models\file\FileListForm;
use App\Models\file\FileSettingsDataForm;
use App\Models\file\FilePermissionUserListForm;
use App\Models\file\AddUserFilePermissionForm;
use App\Models\file\RemoveUserFilePermissionForm;
use App\Models\file\DeleteFileForm;
use App\Libraries\DateAndTime\DateAndTime;
use Common\Components\nothingFoundList\NothingFoundListComponent;

class FileController extends BaseController
{
	use ResponseTrait;

	public function getFileListByType()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new FileListByTypeForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			return $this->respond([
				'res' => true,
				'fileList' => $model->resultList,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getFileList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new FileListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('file/file_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function photo()
	{
		$filterData = $this->request->data;
		$fileConfig = config('FileConfig');

		$data = [
			'title' => lang('Main.photoPlural.Photo'),
			'contentTitle' => lang('Main.photoPlural.photo'),
			'fileConfig' => $fileConfig,
			'type' => $fileConfig->filePageTypeImage,
		];
		
		$data = array_merge($data, $filterData);

		return view('file/index', $data);
	}

	public function video()
	{
		$filterData = $this->request->data;
		$fileConfig = config('FileConfig');

		$data = [
			'title' => lang('Main.Video'),
			'contentTitle' => lang('Main.video'),
			'fileConfig' => $fileConfig,
			'type' => $fileConfig->filePageTypeVideo,
		];
		
		$data = array_merge($data, $filterData);

		return view('file/index', $data);
	}

	public function document()
	{
		$filterData = $this->request->data;
		$fileConfig = config('FileConfig');

		$data = [
			'title' => lang('Main.Documents'),
			'contentTitle' => lang('Main.documents'),
			'fileConfig' => $fileConfig,
			'type' => $fileConfig->filePageTypeDocument,
		];
		
		$data = array_merge($data, $filterData);

		return view('file/index', $data);
	}

	public function settings($fileId)
	{
		$filterData = $this->request->data;
		$fileModel = new FileModel();
		$file = $fileModel
			->where('file_id', $fileId)
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where('item_id', $filterData['userID'])
			->first();

		if (empty($file)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$file->url = FileModel::preparePathToUserFile($file->url, $file->type);

		$data = [
			'title' => lang('Main.File settings'),
			'contentTitle' => lang('Main.file settings'),
			'file' => $file,
		];
		
		$data = array_merge($data, $filterData);

		return view('file/settings', $data);
	}

	public function getFileSettingsData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new FileSettingsDataForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getData()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('file/file_settings_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getFilePermissionUserList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new FilePermissionUserListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('user/list_for_event', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function addUserFilePermission()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new AddUserFilePermissionForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->add()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('user/user_item', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function removeUserFilePermission()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new RemoveUserFilePermissionForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->remove()) {

			return $this->respond([
				'res' => true,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function togglePermission($fileId)
	{
		$filterData = $this->request->data;
		$fileModel = new FileModel();
		$file = $fileModel
			->where('file_id', $fileId)
			->where('related_item', FileModel::RELATED_ITEM_USER)
			->where('item_id', $filterData['userID'])
			->first();

		if (empty($file)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$file->privacy_status = FileModel::getOppositePrivacyStatus()[$file->privacy_status];
		$fileModel->save($file);
		
		return redirect()->to('/file/settings/' . $file->file_id);
	}

	public function delete()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new DeleteFileForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->delete()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'nothingFound' => NothingFoundListComponent::render(),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
}
