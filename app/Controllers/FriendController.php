<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\CalendarModel;
use Common\Models\FriendModel;
use App\Models\friend\FriendListSelfForm;
use App\Models\friend\FriendListForm;
use App\Models\friend\SubscribeFriendForm;
use App\Models\friend\UnubscribeFriendForm;
use App\Models\friend\AcceptFriendshipFriendForm;
use App\Models\friend\RejectFriendshipFriendForm;
use App\Models\friend\RemoveFriendForm;

class FriendController extends BaseController
{
	use ResponseTrait;

	public function index() {
		$friendModel = new FriendModel();
		$filterData = $this->request->data;

		$data = [
			'title' => lang('Main.Friends'),
			'friendsAmount' => $friendModel->getAmountOfUsersInSpecificRelationships($filterData['userID']),
			'menuType' => FriendModel::TYPE_FRIENDS,
		];
		
		$data = array_merge($data, $filterData);

		return view('friend/index', $data);
	}

	public function subscribers() {
		$friendModel = new FriendModel();
		$filterData = $this->request->data;

		$data = [
			'title' => lang('Main.Friends'),
			'friendsAmount' => $friendModel->getAmountOfUsersInSpecificRelationships($filterData['userID']),
			'menuType' => FriendModel::TYPE_SUBSCRIBERS,
		];
		
		$data = array_merge($data, $filterData);

		return view('friend/index', $data);
	}

	public function subscriptions() {
		$friendModel = new FriendModel();
		$filterData = $this->request->data;

		$data = [
			'title' => lang('Main.Friends'),
			'friendsAmount' => $friendModel->getAmountOfUsersInSpecificRelationships($filterData['userID']),
			'menuType' => FriendModel::TYPE_SUBSCRIPTIONS,
		];
		
		$data = array_merge($data, $filterData);

		return view('friend/index', $data);
	}

	public function getFriendListSelf()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new FriendListSelfForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('friend/friend_list_self', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getFriendList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new FriendListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('friend/friend_list_self', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function subscribe()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SubscribeFriendForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->subscribe()) {
			$modelResult = $model->getResult();

			if (!empty($modelResult['noticeData'])) {
				return $this->respond([
					'res' => true,
					'htmlNotice' => $modelResult['noticeData'],
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'htmlData' => $modelResult['htmlData'],
				'userFromId' => $modelResult['userFromId'],
				'userToId' => $modelResult['userToId'],
				'newSubscriberAction' => $modelResult['newSubscriberAction'],
				'action' => $modelResult['action'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function unsubscribe()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new UnubscribeFriendForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->unsubscribe()) {
			$modelResult = $model->getResult();

			if (!empty($modelResult['noticeData'])) {
				return $this->respond([
					'res' => true,
					'htmlNotice' => $modelResult['noticeData'],
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'htmlData' => $modelResult['htmlData'],
				'userFromId' => $modelResult['userFromId'],
				'userToId' => $modelResult['userToId'],
				'newSubscriberAction' => $modelResult['newSubscriberAction'],
				'action' => $modelResult['action'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function acceptFriendship()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new AcceptFriendshipFriendForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->accept()) {
			$modelResult = $model->getResult();

			if (!empty($modelResult['noticeData'])) {
				return $this->respond([
					'res' => true,
					'htmlNotice' => $modelResult['noticeData'],
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'htmlData' => $modelResult['htmlData'],
				'userFromId' => $modelResult['userFromId'],
				'userToId' => $modelResult['userToId'],
				'newSubscriberAction' => $modelResult['newSubscriberAction'],
				'action' => $modelResult['action'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function rejectFriendship()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new RejectFriendshipFriendForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->reject()) {
			$modelResult = $model->getResult();

			if (!empty($modelResult['noticeData'])) {
				return $this->respond([
					'res' => true,
					'htmlNotice' => $modelResult['noticeData'],
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'htmlData' => $modelResult['htmlData'],
				'userFromId' => $modelResult['userFromId'],
				'userToId' => $modelResult['userToId'],
				'newSubscriberAction' => $modelResult['newSubscriberAction'],
				'action' => $modelResult['action'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function removeFriend()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new RemoveFriendForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->remove()) {
			$modelResult = $model->getResult();

			if (!empty($modelResult['noticeData'])) {
				return $this->respond([
					'res' => true,
					'htmlNotice' => $modelResult['noticeData'],
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'htmlData' => $modelResult['htmlData'],
				'userFromId' => $modelResult['userFromId'],
				'userToId' => $modelResult['userToId'],
				'newSubscriberAction' => $modelResult['newSubscriberAction'],
				'action' => $modelResult['action'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
}