<?php 
namespace App\Controllers;

use Common\Models\FileModel;

class OpenFileController extends BaseController
{
	public function getImage($imgName) {
		$type = FileModel::TYPE_IMAGE;
		$path = FileModel::getUploadFileDir($type) . '/' . $imgName;
		$img = new \CodeIgniter\Files\File($path);

		if (!empty($img)) {
			$this->response->setHeader('Content-Type', $img->getMimeType());
			$file = file_get_contents($path, true);
			echo $file;
		}
	}
}