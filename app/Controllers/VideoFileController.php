<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\videoFile\VideoFileDataForm;

class VideoFileController extends BaseController
{
    use ResponseTrait;

    public function details()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new VideoFileDataForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getData()) {
            $modelResult = $model->getResult();

            return $this->respond([
                'res' => true,
                'html' => view('video-file/modal-details', $modelResult),
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'notice_html' => view('modal/notice_close', ['titleNotice' => $model->getFirstError()]),
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }
}
