<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Models\UserModel;
use Common\Models\ReportModel;
use App\Models\commonEvent\CommonAcceptInviteForm;
use App\Models\commonEvent\CommonDeclineInviteForm;
use App\Models\commonEvent\CommonEventDeleteForm;
use App\Models\commonEvent\SaveBaseEventForm;
use App\Models\commonEvent\SetViewedInviteStatusForm;
use App\Models\commonEvent\CommonEventSaveReportForm;

class MeetingController extends BaseController
{
	use ResponseTrait;

	public function index() {
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Meeting.Meetings of the current day'),
			'contentTitle' => lang('Meeting.meetings of the current day'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'meeting',
			'subType' => 'current',
			'createUrl' => '/meeting/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function scheduled()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Meeting.Scheduled meetings'),
			'contentTitle' => lang('Meeting.scheduled meetings'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'meeting',
			'subType' => 'scheduled',
			'createUrl' => '/meeting/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function completed()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Meeting.Completed meetings'),
			'contentTitle' => lang('Meeting.completed meetings'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'meeting',
			'subType' => 'completed',
			'createUrl' => '/meeting/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function invitations()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Meeting.Invitations'),
			'contentTitle' => lang('Meeting.invitations'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'meeting',
			'subType' => 'invitations',
			'createUrl' => '/meeting/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function details($id)
	{
		$filterData = $this->request->data;
		$meetingModel = new MeetingModel();
		$meeting = $meetingModel
			->where('user_id', $filterData['userID'])
			->where('meeting_id', $id)
			->first();

		if (empty($meeting)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($meeting->name)) {
			$meeting->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Meeting.Meeting') . ' - ' . $meeting->name,
			'entity_id' => $meeting->meeting_id,
			'type' => 2,
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/event_details', $data);
	}

	public function invitation($id)
	{
		$filterData = $this->request->data;
		$meetingModel = new MeetingModel();
		$meeting = $meetingModel
			->select(meetingModel::table() . '.*')
			->select('selfGuest.status as inviteStatus')
			->join(EventGuestUserModel::table() . ' as selfGuest', 'selfGuest.item_id = ' . MeetingModel::table() . '.meeting_id and selfGuest.related_item = "' . MeetingModel::table() . '"')
			->where(MeetingModel::table() . '.meeting_id', $id)
			->where('selfGuest.user_id', $filterData['userID'])
			->first();

		if (empty($meeting)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($meeting->name)) {
			$meeting->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Meeting.Meeting invitation') . ' - ' . $meeting->name,
			'entity_id' => $meeting->meeting_id,
			'type' => 2,
			'inviteStatus' => $meeting->inviteStatus,
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/invitation_details', $data);
	}

	public function create()
	{
		$model = new SaveBaseEventForm();

		$filterData = $this->request->data;
		$data = array_merge($filterData, [
			'title' => lang('Meeting.Create meeting'),
			'contentTitle' => lang('Meeting.create meeting'),
			'model' => $model,
			'durationList' => config('MainEventParamsConfig')->duration,
			'reminderList' => config('MainEventParamsConfig')->reminder,
			'periodicityList' => config('MainEventParamsConfig')->periodicity,
			'formAction' => '/meeting/create',
			'type' => 'meeting',
			'createType' => 'create',
		]);

		if ($this->request->getMethod() == 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_MEETING]);
			$model->fill($this->request->requestData);
			$model->scenario = $model::SCENARIO_CREATE;

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/meeting/details/' . $modelResult['id']);
			} else {
				$data['userModel'] = new UserModel();
				$data['entity'] = $model;
			}
		}

		return view('common-event/create', $data);
	}

	public function edit($id)
	{
		$model = new SaveBaseEventForm();
		$filterData = $this->request->data;
		$meetingModel = new MeetingModel();
		$entity = $meetingModel
			->where('meeting_id', $id)
			->where('user_id', $filterData['userID'])
			->first();

		if (empty($entity)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = array_merge($filterData, [
			'title' => lang('Meeting.Edit meeting'),
			'contentTitle' => lang('Meeting.edit meeting'),
			'model' => $model,
			'entity' => $entity,
			'guestData' => $meetingModel->getGuestList($entity->meeting_id),
			'durationList' => config('MainEventParamsConfig')->duration,
			'reminderList' => config('MainEventParamsConfig')->reminder,
			'periodicityList' => config('MainEventParamsConfig')->periodicity,
			'formAction' => '/meeting/edit/' . $id,
			'type' => 'meeting',
			'createType' => 'edit',
		]);

		if ($this->request->getMethod() == 'post') {
			$this->request->requestData = array_merge($this->request->requestData, [
				'type' => $model::TYPE_MEETING,
				'id' => $id,
			]);
			$model->fill($this->request->requestData);
			$model->scenario = $model::SCENARIO_UPDATE;

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/meeting/details/' . $modelResult['id']);
			}
		}
		
		return view('common-event/create', $data);
	}

	public function delete()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventDeleteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_MEETING;

		if ($model->validate() && $model->deleteItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function acceptInvite()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonAcceptInviteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_MEETING;

		if ($model->validate() && $model->accept()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function declineInvite()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonDeclineInviteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_MEETING;

		if ($model->validate() && $model->accept()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function setViewedInviteStatus()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SetViewedInviteStatusForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_MEETING;

		if ($model->validate() && $model->setStatus()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function createReport($meetingId)
	{
		helper('previousUrl');
		$model = new CommonEventSaveReportForm();
		$model->scenario = CommonEventSaveReportForm::SCENARIO_CREATE;
		$filterData = $this->request->data;
		$meetingModel = new MeetingModel();
		$previousUrl = previous_url();
		$meeting = $meetingModel
			->where('user_id', $filterData['userID'])
			->where('meeting_id', $meetingId)
			->first();

		if (empty($meeting)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('Report.Create meeting report'),
			'type' => $model::TYPE_MEETING,
			'id' => $meetingId,
			'action' => $model->scenario,
			'model' => $model,
			'reportMaxVideo' => config('MainEventParamsConfig')->reportMaxVideo,
			'reportMaxImage' => config('MainEventParamsConfig')->reportMaxImage,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['details', 'completed']], '/meeting/details/' . $meeting->meeting_id),
		];

		if ($this->request->getMethod() === 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_MEETING]);
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/meeting/report/' . $modelResult->report_id);
			}
		}
		
		$data = array_merge($data, $filterData);

		return view('common-event/create_report', $data);
	}

	public function editReport($meetingId)
	{
		helper('previousUrl');
		$model = new CommonEventSaveReportForm();
		$model->scenario = CommonEventSaveReportForm::SCENARIO_EDIT;
		$filterData = $this->request->data;
		$meetingModel = new meetingModel();
		$previousUrl = previous_url();
		$meeting = $meetingModel
			->where('user_id', $filterData['userID'])
			->where('meeting_id', $meetingId)
			->first();

		if (empty($meeting)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$reportModel = new ReportModel();
		$report = $reportModel
			->where('related_item', $meetingModel::table())
			->where('item_id', $meeting->meeting_id)
			->first();

		if (empty($report)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('Report.Edit meeting report'),
			'type' => $model::TYPE_MEETING,
			'id' => $meetingId,
			'action' => $model->scenario,
			'model' => $model,
			'report' => $report,
			'reportMaxVideo' => config('MainEventParamsConfig')->reportMaxVideo,
			'reportMaxImage' => config('MainEventParamsConfig')->reportMaxImage,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['details', 'report', 'completed']], '/meeting/report/' . $report->report_id),
		];

		if ($this->request->getMethod() === 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_MEETING]);
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/meeting/report/' . $modelResult->report_id);
			}
		}
		
		$data = array_merge($data, $filterData);

		return view('common-event/create_report', $data);
	}

	public function reportDetails($reportId)
	{
		helper('previousUrl');
		$filterData = $this->request->data;
		$reportModel = new ReportModel();
		$previousUrl = previous_url();
		$report = $reportModel
			->select(ReportModel::table() . '.*')
			->select('meetingTable.name')
			->select('meetingTable.meeting_id')
			->join(MeetingModel::table() . ' as meetingTable', 'meetingTable.meeting_id = ' . ReportModel::table() . '.item_id and ' . ReportModel::table() . '.related_item = "' . MeetingModel::table() . '"')
			->where('report_id', $reportId)
			->where('meetingTable.user_id', $filterData['userID'])
			->first();

		if (empty($report)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($report->name)) {
			$report->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Report.Meeting report') . ' - ' . $report->name,
			'entity_id' => $report->item_id,
			'report_id' => $report->report_id,
			'type' => 2,
			'typeLabel' => 'meeting',
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['details', 'completed']], '/meeting/details/' . $report->meeting_id),
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/report_details', $data);
	}

	public function deleteReport($meetingId, $reportId)
	{
		$filterData = $this->request->data;
		$reportModel = new ReportModel();
		$report = $reportModel
			->select(ReportModel::table() . '.*')
			->select('meetingTable.meeting_id')
			->join(MeetingModel::table() . ' as meetingTable', 'meetingTable.meeting_id = ' . ReportModel::table() . '.item_id and ' . ReportModel::table() . '.related_item = "' . MeetingModel::table() . '"')
			->where('report_id', $reportId)
			->where('meetingTable.meeting_id', $meetingId)
			->where('meetingTable.user_id', $filterData['userID'])
			->first();

		if (empty($report)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (!$reportModel->delete($report->report_id)) {
			return redirect()->back();
		}

		return redirect()->to('/meeting/details/' . $report->meeting_id);
	}
}