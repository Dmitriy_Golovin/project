<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Libraries\AuthJWT\AuthController;
use App\Libraries\AuthJWT\AuthModel;
use App\Models\UserModel;
use App\Models\auth\ActivationForm;
use App\Models\auth\ConfirmChangeEmailForm;
use App\Models\auth\EmailExistsForm;

class AuthCheck extends BaseController
{
	use ResponseTrait;

	public function checkRefreshToken() {
		$key = $this->request->getCookie('uprtk');

		if (empty($key)) {
			return redirect()->to('/cabinet');
		}
		
		if ($key == '/') {
			$key = '';
		}
		
		$auth = new AuthController();

		return $auth->upgradeTokens($this->request, $key);
	}
	
	public function strUpgrade() {
		if ($this->request->getHeader('X-DATA-STR') == null) {
			return redirect()->to('/');
		}

		$auth = new AuthController();
		$result = $auth->upgradeTokens($this->request, 'X-DATA-STR');
		echo json_encode(['res' => $result]);
	}
	
	public function activation($token)
	{
		if (empty($token)) {
			return redirect()->to('/');
		}

		$model = new ActivationForm();
		$model->token = $token;

		if ($model->validate() && $model->activate()) {
			$userId = $model->getResult();
			$auth = new AuthController();
			$auth->activateRegister($this->request, $this->response, $userId);
			$data = [
				'title' => lang('Main.Successfully'),
				'logoUrl' => '/'
			];
		
			return view('successfulUpdateEmail', $data);
		} else {
			$data = [
				'title' => lang('Main.Not working link'),
				'logoUrl' => '/'
			];
			
			return view('notValidToken', $data);
		}
	}

	public function confirm_change_email($token = null)
	{
		if (empty($token)) {
			return redirect()->to('/');
		}

		$model = new ConfirmChangeEmailForm();
		$model->token = $token;

		if ($model->validate() && $model->confirmChange()) {
			$data = [
				'title' => 'Успешное изменение e-mail адреса',
				'logoUrl' => '/',
				'statusUser' => $this->request->data['statusUser']
			];
			
			return view('successfulChangeEmail', $data);
		} else {
			$data = [
				'title' => lang('Main.Not working link'),
				'logoUrl' => '/'
			];
			
			return view('notValidToken', $data);
		}
	}

	public function emailExists()
	{
		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new EmailExistsForm();
		$model->fill($this->request->getPost());

		if ($model->validate() && $model->checkEmail()) {
			return $this->respond([
				'res' => true,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}
	
	public function logout() {
		$access = $this->request->getCookie('sta');
		
		if ($access == null) return redirect()->to('/');
		
		$auth = new AuthController();
		$accessDecode = $auth->decodeAccess($access);
		
		$db = db_connect();
		$authModel = new AuthModel($db);
		$authModel->deleteTokensFromDB($accessDecode['payload']['jti']);
		
		return $auth->destroyTokens();
	}
	
	public function authWS() {
		return json_encode(['res' => $this->request->result]);
	}

}