<?php namespace App\Controllers;

use App\Models\home\RegistrationForm;
use App\Models\home\LoginForm;
use App\Models\home\SendRestoreTokenForm;
use App\Models\home\RestorePasswordForm;
use App\Libraries\AuthJWT\AuthController;

class HomeController extends BaseController
{
	public function index()
	{
		$data = [
			'title' => 'Календарик',
		];

		$filterData = $this->request->data;
		$data = array_merge($data, $filterData);

		echo view('home/home', $data);

	}

	public function register()
	{
		$model = new RegistrationForm();
		$data = [
			'title' => 'Регистрация',
			'model' => $model,
		];
		$data = array_merge($data, $this->request->data);
		
		if ($this->request->getMethod() == 'post') {
			$model->fill($this->request->getPost());

			if ($model->validate() && $model->registration()) {
				$session = \Config\Services::session();
				$session->set('confr', '1');
				return redirect()->to('/confirm');
			}
		}

		return view('home/register', $data);
	}
	
	public function login()
	{
		$model = new LoginForm();
		$data = [
			'title' => 'Вход',
			'model' => $model,
		];
		$data = array_merge($data, $this->request->data);

		if ($this->request->getMethod() == 'post') {
			$model->fill($this->request->getPost());
			
			if ($model->validate() && $model->login()) {
				$auth = new AuthController();
				return $auth->activate($this->request, $model->user_id);
			}
		}

		return view('home/login', $data);
	}

	public function sendRestoreToken()
	{
		$model = new SendRestoreTokenForm();
		$data = [
			'title' => 'Восстановление пароля',
			'model' => $model,
			'success' => false,
		];
		$data = array_merge($data, $this->request->data);

		if ($this->request->getMethod() == 'post') {
			$model->fill($this->request->getPost());
			
			if ($model->validate() && $model->send()) {
				$data['success'] = true;
				
			}
		}

		return view('home/send_restore_token', $data);
	}

	public function restorePassword($token = null)
	{
		$model = new RestorePasswordForm();
		$data = [
			'title' => 'Восстановление пароля',
			'model' => $model,
		];
		$data = array_merge($data, $this->request->data);

		if ($this->request->getMethod() == 'post') {
			$model->fill($this->request->getPost());
			$model->setAttribute('token_change_safety_data', $token);

			if ($model->validate() && $model->createNewPassword()) {
				return redirect()->to('/login');
			}
		}

		return view('home/restore_password', $data);
	}
	
	public function confirm()
	{
		$session = \Config\Services::session();
		
		if ($session->get('confr') != 1) {
			$session->destroy();
			return redirect()->to('/');
		}
		
		$session->destroy();
		
		$data = [
			'title' => 'Подтвердите регистрацию',
		];
		
		$filterData = $this->request->data;
		$data = array_merge($data, $filterData);
		
		return view('home/confirm', $data);
	}
	
	
	public function cs()
	{
		if ($this->request->getHeader('X-DATA-CS') == NULL) return redirect()->to('/');
		
		echo json_encode(['token' => csrf_hash()]);
	}
}
