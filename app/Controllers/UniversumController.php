<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use Common\Models\ArticleSectionModel;
use Common\Models\ArticleModel;
use Common\Models\FileModel;
use Common\Models\CommentModel;
use App\Models\universum\ArticleListForm;

class UniversumController extends BaseController
{
    use ResponseTrait;
    
    public function index()
    {
        $filterData = $this->request->data;
        $articleSectionModel = new ArticleSectionModel();

        $data = [
            'title' => lang('Main.Universum'),
            'articleSectionList' => $articleSectionModel->getArticleSectionList(),
        ];

        return view('universum/index', array_merge($data, $filterData));
    }

    public function chapter($chapter)
    {
        $filterData = $this->request->data;
        $articleSectionModel = new ArticleSectionModel();
        $articleSection = $articleSectionModel
            ->where('title_transliteration', $chapter)
            ->first();

        if (empty($articleSection)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $data = [
            'title' => lang('Main.Universum') .  ' - ' . $articleSection->title,
            'articleSection' => $articleSection,
        ];

        return view('universum/chapter', array_merge($data, $filterData));
    }

    public function article($chapter, $article)
    {
        $filterData = $this->request->data;
        $articleSectionModel = new ArticleSectionModel();
        $articleSection = $articleSectionModel
            ->where('title_transliteration', $chapter)
            ->first();

        if (empty($articleSection)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $articleModel = new ArticleModel();
        $article = $articleModel
            ->where('article_section_id', $articleSection->article_section_id)
            ->where('title_transliteration', $article)
            ->first();

        if (empty($article)) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $article->description = ArticleModel::prepareDescriptionForDetailsClient($article->description);

        $data = [
            'title' => $article->title,
            'article' => $article,
            'articleSection' => $articleSection,
        ];

        return view('universum/article_details', array_merge($data, $filterData));
    }

    public function getArticleList()
    {
        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new ArticleListForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getList()) {
            $modelResult = $model->getResult();

            return $this->respond([
                'res' => true,
                'html' => view('universum/article_list', $modelResult),
                'insert_type' => $modelResult['insert_type'],
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }

    public function getArticleCommentData()
    {
        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $guest = ($this->request->requestData['statusUser'] === 'guest') ? true : false;

        $commentModel = new CommentModel();
        $commentData = $commentModel->getCommentData(CommentModel::TYPE_TABLE_ARTICLE, $this->request->getVar('article_id'), $this->request->getVar('timeOffsetSeconds'), null, null, null, $guest);
        $commentData['statusUser'] = $this->request->requestData['statusUser'];


        return $this->respond([
            'res' => true,
            'html' => view('universum/article_comment_data', ['commentData' => $commentData]),
            'csrf' => csrf_hash(),
        ]);
    }
}
