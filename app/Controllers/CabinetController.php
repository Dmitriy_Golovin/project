<?php namespace App\Controllers;

use App\Models\CalendarModel;
use Common\Models\FileModel;

class CabinetController extends BaseController
{
	public function index() {
		$filterData = $this->request->data;
		$fileModel = new FileModel();
		$fileCountOfUser = $fileModel->getFileCountOfUser($filterData['userID']);
		$data = [
			'title' => lang('Main.Cabinet'),
			'fileCountOfUser' => $fileCountOfUser,
		];
		
		$data = array_merge($data, $filterData);

		return view('cabinet/index', $data);
	}
}