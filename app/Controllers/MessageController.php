<?php namespace App\Controllers;

use App\Libraries\MainLib\MainLib;
use Common\Components\WsClientComponent;
use App\Libraries\DateAndTime\DateAndTime;
use CodeIgniter\API\ResponseTrait;
use Common\Models\ChatModel;
use Common\Models\ChatMemberModel;
use Common\Models\MessageModel;
use Common\Models\UserModel;
use App\Models\message\CorrespondenceListForm;
use App\Models\message\DialogMessagesDetailsForm;
use App\Models\message\MessageImportantForm;
use App\Models\message\MessageDeleteOneMessageDialogForm;
use App\Models\message\MessageDeleteAllMessagesDialogForm;
use App\Models\message\SaveChatForm;
use App\Models\message\SendMessageChatForm;
use App\Models\message\SendMessageDialogForm;
use App\Models\message\ChatMessageListItemForm;
use App\Models\message\DialogMessageListItemForm;
use App\Models\message\ChatMessagesDetailsForm;
use App\Models\message\ChatMessageDetailsItemForm;
use App\Models\message\DialogMessageDetailsItemForm;
use App\Models\message\LeaveChatForm;
use App\Models\message\MarkChatMessageAsReadForm;
use App\Models\message\MarkDialogMessageAsReadForm;
use App\Models\message\ChatMessageImportantForm;
use App\Models\message\MessageDeleteOneMessageChatForm;
use App\Models\message\ChatMessageReactionForm;
use App\Models\message\ChatMemberListForm;
use App\Models\message\ChatMessageViewMemberListForm;

class MessageController extends BaseController
{
	use ResponseTrait;

	const DEFAULT_AVA_URL = '/img/no_ava.png';

	public function index() {
		$filterData = $this->request->data;

		$data = [
			'title' => lang('Message.Messages'),
		];
		
		$data = array_merge($data, $filterData);
		
		return view('message/index', $data);
	}

	public function createChat()
	{
		$model = new SaveChatForm();
		$model->scenario = SaveChatForm::SCENARIO_CREATE;
		$filterData = $this->request->data;
		$data = array_merge($filterData, [
			'title' => lang('Message.Create chat'),
			'contentTitle' => lang('Message.create chat'),
			'model' => $model,
			'formAction' => '/messages/create-chat',
		]);

		if ($this->request->getMethod() == 'post') {
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/messages/cr/' . $modelResult->chat_id);
			}
		}
		
		return view('message/create-chat', $data);
	}

	public function editChat($chatId)
	{
		$model = new SaveChatForm();
		$chatMemberModel = new ChatMemberModel();
		$filterData = $this->request->data;
		$chatMember = $chatMemberModel
			->where('chat_id', $chatId)
			->where('user_id', $filterData['user_id'])
			->whereIn('role', [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN])
			->first();

		if (empty($chatMember)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$chatModel = new ChatModel();
		$chat = $chatModel->getChatData($chatId, $chatMember->chat_member_id);
		$model->scenario = SaveChatForm::SCENARIO_UPDATE;
		$data = array_merge($filterData, [
			'title' => lang('Message.Edit chat'),
			'contentTitle' => lang('Message.edit chat'),
			'model' => $model,
			'chat' => $chat,
			'selfId' => $filterData['user_id'],
			'formAction' => '/messages/edit-chat/' . $chatId,
			'titleNotice' => ($chat->importantMessageCount > 0)
				? lang('Notice.Some messages are marked "important". Are you sure you want to delete all the correspondence?')
				: lang('Notice.Are you sure you want to delete all the correspondence?'),
			'acceptNoticeAnchor' => '/messages/delete-chat/' . $chat->chat_id,
		]);

		if ($this->request->getMethod() == 'post') {
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/messages/cr/' . $modelResult->chat_id);
			}
		}
		
		return view('message/create-chat', $data);
	}

	public function deleteChat($chatId)
	{
		$chatModel = new ChatModel();
		$chat = $chatModel->find($chatId);

		if (empty($chat)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$chatMemberModel = new ChatMemberModel();
		$chatMember = $chatMemberModel
			->where('chat_id', $chat->chat_id)
			->where('user_id', $filterData['userID'])
			->whereIn('role', [ChatMemberModel::ROLE_CREATER, ChatMemberModel::ROLE_ADMIN])
			->first();

		if (empty($chatMember)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$chatModel->delete($chat->chat_id);
		return redirect()->to('/messages');
	}

	public function chatRoom($chatId)
	{
		$chatModel = new ChatModel();
		$chat = $chatModel->find($chatId);

		if (empty($chat)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$chatMemberModel = new ChatMemberModel();
		$chatMember = $chatMemberModel
			->where('chat_id', $chat->chat_id)
			->where('user_id', $filterData['userID'])
			->first();

		if (empty($chatMember)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$chatMemberCount = $chatMemberModel
			->where('chat_id', $chat->chat_id)
			->countAllResults();

		$data = [
			'title' => lang('Message.Messages'),
			'chat' => $chat,
			'chatMemberCount' => $chatMemberCount,
			'chatMember' => $chatMember,
		];
		
		$data = array_merge($data, $filterData);
		
		return view('message/chat_room_details', $data);
	}

	public function dialog($id)
	{
		$messageModel = new MessageModel();
		$userModel = new UserModel();
		$filterData = $this->request->data;
		$message = $messageModel
			->select($messageModel->getTablename() . '.*')
			->select('(select concat(first_name, " ", last_name) from ' . $userModel->getTableName() . ' where user_id = ' . $id . ') as user_with_fullname')
			->groupStart()
				->where('user_from', $filterData['userID'])
				->where('user_to', $id)
				->where('delete_from', MessageModel::MESSAGE_NOT_DELETED)
			->groupEnd()
			->orGroupStart()
				->where('user_from', $id)
				->where('user_to', $filterData['userID'])
				->where('delete_to', MessageModel::MESSAGE_NOT_DELETED)
			->groupEnd()
			->first();

		if (empty($message)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('Message.Messages'),
			'userWithFullname' => $message->user_with_fullname,
			'id' => $id,
		];

		$data = array_merge($data, $filterData);
		
		return view('message/dialog_details', $data);
	}

	public function leaveChat()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new LeaveChatForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->leave()) {
			$modelResult = $model->getResult();

			if (!empty($modelResult['notice'])) {
				if ($modelResult['notice'] === $model::EXIST_IMPORTANT) {
					$noticeData = [
						'titleNotice' => lang('Notice.Some messages are marked "important". Do you really want to leave the chat?')
					];
				}

				if ($modelResult['notice'] === $model::NO_ADMIN) {
					$noticeData = [
						'titleNotice' => lang('Notice.You are the only administrator in the chat. Assign one of the chat members as an administrator so that you can leave the chat'),
						'acceptNoticeAnchor' => '/messages/edit-chat/' . $modelResult['chat']->chat_id,
						'acceptButtonText' => lang('Task.assign'),
					];
				}

				return $this->respond([
					'res' => 'notice',
					'notice_html' => view('modal/notice', $noticeData),
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function getCorrespondence()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$act = $this->request->getVar('act');

		if ($act === 'messages') {
			$model = new CorrespondenceListForm();
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->getList()) {
				$modelResult = $model->getResult();
				$dataResponse = [
					'res' => true,
					'action' => 'list',
	                'html' => view('message/correspondence_list', $modelResult),
					'csrf' => csrf_hash(),
				];

				if (empty($modelResult)) {
					$dataResponse['empty'] = lang('Main.Nothing found');
				}

				return $this->respond($dataResponse);
			} else {
				return $this->respond([
					'res' => false,
					'errors' => $model->getFirstError(),
					'csrf' => csrf_hash(),
				]);
			}
		}
	}

	public function sendDialogMessage()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SendMessageDialogForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->send()) {
			$modelResult = $model->getResult();
			$wsClientComponent = new WsClientComponent();

			$wsClientComponent->openSocket();

			$wsData = [
				'server' => [
					'controller' => 'DialogMessage',
					'action' => 'sendMessage',
					'userId' => $modelResult['message']->user_to,
					'data' => [
						'message_id' => $modelResult['message']->message_id,
						'user_from' => $modelResult['message']->user_from,
						'user_to' => $modelResult['message']->user_to,
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$model->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();

				return $this->respond([
					'res' => false,
					'errors' => $model->getErrors(),
					'csrf' => csrf_hash()
				]);
			}

			$wsClientComponent->closeSocket();

			return $this->respond([
				'res' => true,
				'html' => view('message/new_dialog_message_item', [
					'message' => $modelResult['message'],
				]),
				'user_to' => $modelResult['message']->user_to,
				'user_from' => $modelResult['message']->user_from,
				'message_id' => $modelResult['message']->message_id,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function sendChatMessage()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SendMessageChatForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->send()) {
			$modelResult = $model->getResult();
			$wsClientComponent = new WsClientComponent();;

			$wsClientComponent->openSocket();

			$wsData = [
				'server' => [
					'controller' => 'ChatMessage',
					'action' => 'sendMessage',
					'userIdList' => $modelResult['memberIdList'],
					'data' => [
						'chat_id' => $modelResult['chat']->chat_id,
						'chat_message_id' => $modelResult['chatMessage']->chat_message_id,
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$model->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();

				return $this->respond([
					'res' => false,
					'errors' => $model->getErrors(),
					'csrf' => csrf_hash()
				]);
			}

			$wsClientComponent->closeSocket();

			return $this->respond([
				'res' => true,
				'html' => view('message/new_chat_message_item_for_self', [
					'message' => $modelResult['chatMessage'],
					'sender' => $modelResult['sender'],
					'file_list' => $modelResult['file_list'],
				]),
				'chatId' => $modelResult['chat']->chat_id,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function markDialogMessageAsRead()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MarkDialogMessageAsReadForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->markMessage()) {
			$modelResult = $model->getResult();
			$wsClientComponent = new WsClientComponent();;

			$wsClientComponent->openSocket();

			$wsData = [
				'server' => [
					'controller' => 'DialogMessage',
					'action' => 'markMessageAsReadForSender',
					'userId' => $modelResult['dialogId'],
					'data' => [
						'messageDataList' => $modelResult['messageDataList'],
						'newMessageForSenderCount' => $modelResult['newMessageForSenderCount'],
						'userId' => $modelResult['dialogId'],
						'dialogId' => $modelResult['userId'],
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$model->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();

				return $this->respond([
					'res' => false,
					'errors' => $model->getErrors(),
					'csrf' => csrf_hash()
				]);
			}

			$wsClientComponent->closeSocket();

			return $this->respond([
				'res' => true,
				'messageDataList' => $modelResult['messageDataList'],
				'userId' => $modelResult['userId'],
				'dialogId' => $modelResult['dialogId'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function markChatMessageAsRead()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MarkChatMessageAsReadForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->markMessage()) {
			$modelResult = $model->getResult();
			$wsClientComponent = new WsClientComponent();;

			$wsClientComponent->openSocket();

			$wsData = [
				'server' => [
					'controller' => 'ChatMessage',
					'action' => 'updateViewMessageCount',
					'userIdList' => $modelResult['chatMemberList'],
					'data' => [
						'chat_id' => $modelResult['chatId'],
						'messageDataList' => $modelResult['messageDataList'],
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$model->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();

				return $this->respond([
					'res' => false,
					'errors' => $model->getErrors(),
					'csrf' => csrf_hash()
				]);
			}

			$wsClientComponent->closeSocket();

			return $this->respond([
				'res' => true,
				'messageDataList' => $modelResult['messageDataList'],
				'userId' => $modelResult['userId'],
				'chatId' => $modelResult['chatId'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getDialogMessageListItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new DialogMessageListItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('message/new_correspondence_dialog_list_item', ['item' => $modelResult]),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getChatMessageListItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMessageListItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('message/new_correspondence_chat_list_item', ['modelResult' => $modelResult]),
				'chatId' => $modelResult->chat_id,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getDialogDetails()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new DialogMessagesDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();
			
			return $this->respond([
				'res' => true,
				'html' =>view('message/dialog_message_list', ['messageResult' => $modelResult]),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getChatDetails()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMessagesDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();
			
			return $this->respond([
				'res' => true,
				'html' =>view('message/chat_message_list', ['messageResult' => $modelResult]),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getDialogMessageDetailsItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new DialogMessageDetailsItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();
			
			return $this->respond([
				'res' => true,
				'html' => view('message/new_dialog_message_item', [
					'message' => $modelResult,
				]),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getChatMessageDetailsItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMessageDetailsItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();
			
			return $this->respond([
				'res' => true,
				'html' =>view('message/new_chat_message_item', ['message' => $modelResult]),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function addChatMessageReaction()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMessageReactionForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->toggleReaction()) {
			$modelResult = $model->getResult();
			$view = view('message/chat_message_reaction_list', ['reactionList' => $modelResult['reactionList']]);

			$wsClientComponent = new WsClientComponent();;

			$wsClientComponent->openSocket();

			$wsData = [
				'server' => [
					'controller' => 'ChatMessage',
					'action' => 'updateMessageReaction',
					'userIdList' => $modelResult['userIdList'],
					'data' => [
						'chat_id' => $modelResult['chatId'],
						'chat_message_id' => $modelResult['chatMessageId'],
						'reactionHtml' => $view,
					],
				],
			];

			if (!$wsClientComponent->sendData($wsData)) {
				$model->setError('', $wsClientComponent->error);
				$wsClientComponent->closeSocket();

				return $this->respond([
					'res' => false,
					'errors' => $model->getErrors(),
					'csrf' => csrf_hash()
				]);
			}

			$wsClientComponent->closeSocket();

			return $this->respond([
				'res' => true,
				'html' => $view,
				'chatId' => $modelResult['chatId'],
				'chatMessageId' => $modelResult['chatMessageId'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function toggleImportantMessage() {
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MessageImportantForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->toggleImportant()) {
			return $this->respond([
				'res' => true,
				'important' => $model->important,
				'buttonText' => $model->important ? lang('Message.remove from important') : lang('Message.mark as important'),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function toggleImportantChatMessage()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMessageImportantForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->toggleImportant()) {
			return $this->respond([
				'res' => true,
				'important' => $model->important,
				'buttonText' => $model->important ? lang('Message.remove from important') : lang('Message.mark as important'),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function deleteDialogMessage() {
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MessageDeleteOneMessageDialogForm();
		$model->fill($this->request->requestData);
		$modelResult = $model->delete();

		if (!$modelResult) {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash(),
			]);
		}

		if ($modelResult === 'asImportant') {
			$noticeData = [
				'titleNotice' => lang('Notice.The message is marked "important". Are you sure you want to delete the message?')
			];

			return $this->respond([
				'res' => 'notice',
				'notice_html' => view('modal/notice', $noticeData),
				'csrf' => csrf_hash(),
			]);
		}

		return $this->respond([
			'res' => true,
			'csrf' => csrf_hash(),
		]);
	}

	public function deleteChatMessage()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MessageDeleteOneMessageChatForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->delete()) {
			$modelResult = $model->getResult();

			if (isset($modelResult['asImportant'])) {
				$noticeData = [
					'titleNotice' => lang('Notice.The message is marked "important". Are you sure you want to delete the message?')
				];

				return $this->respond([
					'res' => 'notice',
					'notice_html' => view('modal/notice', $noticeData),
					'messageId' => $modelResult['messageId'],
					'csrf' => csrf_hash(),
				]);
			}

			return $this->respond([
				'res' => true,
				'messageId' => $modelResult['messageId'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function deleteDialogAllMesages() {
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}
		
		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MessageDeleteAllMessagesDialogForm();
		$model->fill($this->request->requestData);
		$modelResult = $model->delete();

		if (!$modelResult) {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash(),
			]);
		}

		if ($modelResult === 'asImportant') {
			$noticeData = [
				'titleNotice' => lang('Notice.Some messages are marked "important". Are you sure you want to delete all the correspondence?')
			];

			return $this->respond([
				'res' => 'notice',
				'notice_html' => view('modal/notice', $noticeData),
				'csrf' => csrf_hash(),
			]);
		}

		return $this->respond([
			'res' => true,
			'csrf' => csrf_hash(),
		]);
	}

	public function getChatMemberList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMemberListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'insert_type' => $modelResult['insert_type'],
                'html' => view('user/list_for_event', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getChatMessageViewMemberList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChatMessageViewMemberListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'insert_type' => $modelResult['insert_type'],
                'html' => view('user/list_for_event', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
}