<?php namespace App\Controllers;

class Error404Controller extends BaseController
{

	public function show404()
	{
		$filterData = null;

		if (isset($this->request->data)) {
			$filterData = $this->request->data;
		}
		
		$data = [
			'title' => '404',
		];

		if (!is_null($filterData)) {
			$data = array_merge($data, $filterData);
		} else {
			$data = array_merge($data, [
				'logoUrl' => '/'
			]);
		}

		$this->response->setStatusCode('404');

		echo view('404/index', $data);
	}

}