<?php namespace App\Controllers;

class SitemapController extends BaseController
{
	public function index() {
		$this->response->setHeader('Content-Type', 'text/xml');
		$xml = file_get_contents($_SERVER['DOCUMENT_ROOT'] . '/sitemap.xml');

		echo $xml;
	}
}