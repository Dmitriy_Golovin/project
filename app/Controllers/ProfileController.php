<?php namespace App\Controllers;

use App\Libraries\MainLib\MainLib;
use App\Models\profile\ProfileDataForm;
use App\Models\profile\ProfileFileListForm;
use App\Models\profile\ProfileMainEventListForm;
use App\Models\profile\ProfileMainEventDetailsForm;
use App\Models\profile\ProfileMainEventParticipantsListForm;
use App\Models\profile\ProfileReportDetailsForm;
use Common\Models\CountryTranslationModel;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Models\UserSettingModel;
use Common\Models\EventModel;
use Common\Models\MeetingModel;
use Common\Models\EventGuestUserModel;
use Common\Models\ReportModel;
use App\Libraries\DateAndTime\DateAndTime;
use CodeIgniter\API\ResponseTrait;

class ProfileController extends BaseController
{
	use ResponseTrait;

	public function index($page_id)
	{
		$model = new ProfileDataForm();
		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$filterData['activeMenu'] = $profileConfig->profileActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['pageId'] = $page_id;
		$model->page_id = $page_id;
		$model->user = $filterData;

		if ($model->validate() && $model->getData()) {
			$modelResult = $model->getResult();

			if ($modelResult === $model::USER_NOT_FOUND) {
				throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
			}

			if ($modelResult === $model::USER_BLOCKED) {
				return view('profile/user_blocked', array_merge($filterData, [
					'title' => lang('Main.Profile blocked'),
				]));
			}

			if ($modelResult === $model::USER_DELETED) {
				return view('profile/user_deleted', array_merge($filterData, [
					'title' => lang('Main.Profile deleted'),
				]));
			}

			$viewFile = ($modelResult['selfPage'])
				? 'profile/profile-self'
				: 'profile/profile';
			return view($viewFile, array_merge($filterData, $modelResult));
		} else {
			return $model->getFirstError();
		}
	}

	public function friends($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$friendModel = new FriendModel();
		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->friendsActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['pageId'] = $page_id;
		$filterData['title'] = lang('Main.Friends') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.friends') . ' - ' . $user->first_name . ' ' . $user->last_name;
		$filterData['menuType'] = FriendModel::TYPE_FRIENDS;
		$filterData['friendsAmount'] = $friendModel->getAmountOfUsersInSpecificRelationships($page_id);

		return view('profile/friends', $filterData);
	}

	public function subscribers($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$friendModel = new FriendModel();
		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->friendsActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['pageId'] = $page_id;
		$filterData['title'] = lang('Main.Friends') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.friends') . ' - ' . $user->first_name . ' ' . $user->last_name;
		$filterData['menuType'] = FriendModel::TYPE_SUBSCRIBERS;
		$filterData['friendsAmount'] = $friendModel->getAmountOfUsersInSpecificRelationships($page_id);

		return view('profile/friends', $filterData);
	}

	public function photo($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$fileConfig = config('FileConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->photoActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['pageId'] = $page_id;
		$filterData['title'] = lang('Main.photoPlural.Photo') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.photoPlural.photo') . ' - ' . $user->first_name . ' ' . $user->last_name;
		$filterData['type'] = $fileConfig->filePageTypeImage;

		return view('profile/photo', $filterData);
	}

	public function video($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$fileConfig = config('FileConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->videoActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['pageId'] = $page_id;
		$filterData['title'] = lang('Main.Video') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.video') . ' - ' . $user->first_name . ' ' . $user->last_name;
		$filterData['type'] = $fileConfig->filePageTypeVideo;

		return view('profile/video', $filterData);
	}

	public function getFileList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ProfileFileListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('file/file_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function event($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->eventsActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['mainEventParamsConfig'] = $mainEventParamsConfig;
		$filterData['pageId'] = $page_id;
		$filterData['type'] = $mainEventParamsConfig->typeProfilePageEvent;
		$filterData['subType'] = $mainEventParamsConfig->subTypeProfilePageCurrentCompleted;
		$filterData['title'] = lang('Main.Events') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.events') . ' - ' . $user->first_name . ' ' . $user->last_name;

		return view('profile/event', $filterData);
	}

	public function eventScheduled($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->eventsActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['mainEventParamsConfig'] = $mainEventParamsConfig;
		$filterData['pageId'] = $page_id;
		$filterData['type'] = $mainEventParamsConfig->typeProfilePageEvent;
		$filterData['subType'] = $mainEventParamsConfig->subTypeProfilePageScheduled;
		$filterData['title'] = lang('Main.Events') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.events') . ' - ' . $user->first_name . ' ' . $user->last_name;

		return view('profile/event', $filterData);
	}

	public function meeting($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->meetingsActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['mainEventParamsConfig'] = $mainEventParamsConfig;
		$filterData['pageId'] = $page_id;
		$filterData['type'] = $mainEventParamsConfig->typeProfilePageMeeting;
		$filterData['subType'] = $mainEventParamsConfig->subTypeProfilePageCurrentCompleted;
		$filterData['title'] = lang('Main.Meetings') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.meetings') . ' - ' . $user->first_name . ' ' . $user->last_name;

		return view('profile/meeting', $filterData);
	}

	public function meetingScheduled($page_id)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$profileConfig = config('ProfileConfig');
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData['user'] = $user;
		$filterData['activeMenu'] = $profileConfig->meetingsActiveMenu;
		$filterData['profileConfig'] = $profileConfig;
		$filterData['mainEventParamsConfig'] = $mainEventParamsConfig;
		$filterData['pageId'] = $page_id;
		$filterData['type'] = $mainEventParamsConfig->typeProfilePageMeeting;
		$filterData['subType'] = $mainEventParamsConfig->subTypeProfilePageScheduled;
		$filterData['title'] = lang('Main.Meetings') . ' ' . $user->first_name . ' ' . $user->last_name;
		$filterData['contentTitle'] = lang('Main.meetings') . ' - ' . $user->first_name . ' ' . $user->last_name;

		return view('profile/meeting', $filterData);
	}

	public function getMainEventList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ProfileMainEventListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('profile/main_event_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function eventDetails($page_id, $eventId)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$eventModel = new EventModel();
		$event = $eventModel
			->select(EventModel::table() . '.*')
			->select('reportTable.report_id')
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . EventModel::table() . '" and eventGuestTable.item_id = ' . EventModel::table() . '.event_id', 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.related_item = "' . EventModel::table() . '" and reportTable.item_id = ' . EventModel::table() . '.event_id', 'left')
			->where(EventModel::table() . '.event_id', $eventId)
			->groupStart()
				->groupStart()
					->where(EventModel::table() . '.privacy', EventModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where(EventModel::table() . '.privacy', EventModel::PRIVATE_STATUS)
					->where('eventGuestTable.user_id', $filterData['userID'])
				->groupEnd()
			->groupEnd()
			->first();

		if (empty($event)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($event->name)) {
			$event->name = lang('Main.No title');
		}

		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData['title'] = lang('Event.Event') . ' - ' . $event->name;
		$filterData['type'] = $mainEventParamsConfig->typeProfilePageEvent;
		$filterData['pageId'] = $page_id;
		$filterData['entity'] = $event;
		$filterData['entityFieldId'] = 'event_id';
		$viewFile = (!empty($event->report_id))
			? 'profile/report_details'
			: 'profile/event_meeting_details';

		return view($viewFile, $filterData);
	}

	public function meetingDetails($page_id, $meetingId)
	{
		$userModel = new UserModel();
		$user = $userModel->getUserDataMin($page_id);

		if (empty($user)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$filterData = $this->request->data;
		$meetingModel = new MeetingModel();
		$meeting = $meetingModel
			->select(MeetingModel::table() . '.*')
			->select('reportTable.report_id')
			->join(EventGuestUserModel::table() . ' as eventGuestTable', 'eventGuestTable.related_item = "' . MeetingModel::table() . '" and eventGuestTable.item_id = ' . MeetingModel::table() . '.meeting_id', 'left')
			->join(ReportModel::table() . ' as reportTable', 'reportTable.related_item = "' . MeetingModel::table() . '" and reportTable.item_id = ' . MeetingModel::table() . '.meeting_id', 'left')
			->where(MeetingModel::table() . '.meeting_id', $meetingId)
			->groupStart()
				->groupStart()
					->where(MeetingModel::table() . '.privacy', MeetingModel::PUBLIC_STATUS)
				->groupEnd()
				->orGroupStart()
					->where(MeetingModel::table() . '.privacy', MeetingModel::PRIVATE_STATUS)
					->where('eventGuestTable.user_id', $filterData['userID'])
				->groupEnd()
			->groupEnd()
			->first();

		if (empty($meeting)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($meeting->name)) {
			$meeting->name = lang('Main.No title');
		}

		$filterData = $this->request->data;
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData['title'] = lang('Meeting.Meeting') . ' - ' . $meeting->name;
		$filterData['type'] = $mainEventParamsConfig->typeProfilePageMeeting;
		$filterData['pageId'] = $page_id;
		$filterData['entity'] = $meeting;
		$filterData['entityFieldId'] = 'meeting_id';
		$viewFile = (!empty($meeting->report_id))
					? 'profile/report_details'
					: 'profile/event_meeting_details';

		return view($viewFile, $filterData);
	}

	public function getMainEventDetails()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ProfileMainEventDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('profile/event_meeting_details_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getReportDetails()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ProfileReportDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('profile/report_details_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getMainEventParticipantsList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ProfileMainEventParticipantsListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('user/list_for_event', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
	
	public function changePersonalData() {
		$filterData = $this->request->data;
		$negotiate = \Config\Services::negotiator();
		$language = $negotiate->language(config('app')->supportedLocales);
		$countryTranslationModel = new CountryTranslationModel();
		$userSettingModel = new UserSettingModel();
		$countryList = $countryTranslationModel
			->where('lang_code', $language)
			->findAll();

		if (empty($countryList)) {
			$countryList = $countryTranslationModel
				->where('lang_code', 'ru')
				->findAll();
		}

		$userSetting = $userSettingModel
			->where('user_id', $filterData['userID'])
			->first();
		
		if (empty($filterData['country'])) {
			$filterData['country'] = lang('Main.Choose a country');
		}

		if (empty($filterData['city'])) {
			$filterData['city'] = lang('Main.Choose city');
		}
		
		$data = [
			'title' => $filterData['firstName'] . ' ' . $filterData['lastName'],
			'countryList' => $countryList,
			'dayOfWeekList' => DateAndTime::DAY_OF_WEEK,
			'userSetting' => $userSetting,
			'mainModalNoticeCssClass' => ' modal_notice_delete_profile',
			'titleNotice' => lang('Main.Confirm your account deletion'),
			'acceptNoticeAnchor' => '/user/delete',
			'acceptButtonText' => lang('Main.delete'),
		];
		
		$data = array_merge($data, $filterData);

		return view('profile/changePersonalData', $data);
	}
}