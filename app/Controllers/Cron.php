<?php namespace App\Controllers;

use App\Models\UserModel;

class Cron extends BaseController {
	
	public function deleteNotActivatedUser() {
		$db = db_connect();
		$userModel = new UserModel($db);
		
		$userModel->deleteNotActivatedUserFromDB();
	}
}
