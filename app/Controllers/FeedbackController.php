<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Models\feedback\FeedbackSaveForm;

class FeedbackController extends BaseController
{
    public function index()
    {
        $model = new FeedbackSaveForm();

        $filterData = $this->request->data;

        $data = [
            'title' => lang('Main.Feedback'),
            'model' => $model,
        ];
        
        $data = array_merge($data, $filterData);

        if ($this->request->getMethod() == 'post') {
            $model->fill($this->request->requestData);
            
            if ($model->validate() && $model->save()) {
                $data['successfullySent'] = true;
            }
        }

        return view('feedback/index', $data);
    }
}
