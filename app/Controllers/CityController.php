<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use CodeIgniter\API\ResponseTrait;
use App\Models\search\GetCityForm;

class CityController extends BaseController
{
	use ResponseTrait;

	public function getCitiesForThisCountry() {
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] == 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new GetCityForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			return $this->respond([
				'res' => true,
				'html' => view('templates/city_list_select', ['cityList' => $model->getResult()]),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
}
