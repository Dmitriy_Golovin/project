<?php namespace App\Controllers;

use App\Models\CalendarModel;

class AgreementController extends BaseController
{
	public function termsOfUse() {
		$filterData = $this->request->data;
		
		$data = array_merge($filterData, [
			'title' => lang('Main.Terms of use'),
		]);

		return view('agreement/terms_of_use', $data);
	}

	public function privacy() {
		$filterData = $this->request->data;
		
		$data = array_merge($filterData, [
			'title' => lang('Main.Privacy policy'),
		]);

		return view('agreement/privacy', $data);
	}
}