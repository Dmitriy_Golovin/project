<?php

namespace App\Controllers;

use App\Controllers\BaseController;
use App\Libraries\MainLib\MainLib;
use Common\Models\CountryTranslationModel;
use CodeIgniter\API\ResponseTrait;
use App\Models\search\SearchMainForm;

class SearchController extends BaseController
{
    use ResponseTrait;

    public function index()
    {
        $negotiate = \Config\Services::negotiator();
        $filterData = $this->request->data;
        $language = $negotiate->language(config('app')->supportedLocales);
        $countryTranslationModel = new CountryTranslationModel();
        $countryList = $countryTranslationModel
            ->where('lang_code', $language)
            ->findAll();

        $data = [
            'title' => 'Поиск',
            'userRole' => $filterData['statusUser'],
            'countryList' => $countryList,
        ];
        
        $data = array_merge($data, $filterData);

        return view('search/search', $data);
    }

    public function searchRequest()
    {
        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new SearchMainForm();
        $model->fill($this->request->requestData);

        if ($this->request->data['statusUser'] == 'guest') {
            if (!empty($this->request->requestData['m_v']) && $this->request->requestData['m_v'] === $model::QUERY_USER) {
                return $this->respond([
                    'res' => false,
                    'errors' => lang('Main.You are not authorized'),
                    'csrf' => csrf_hash()
                ]);
            }

            if ($model->validate() && $model->search()) {
                $modelResult = $model->getResult();
                return $this->respond([
                    'res' => true,
                    'insert_type' => $modelResult['insert_type'],
                    'html' => view('search/' . $modelResult['template'], $modelResult),
                    'csrf' => csrf_hash()
                ]);
            } else {
                return $this->respond([
                    'res' => false,
                    'errors' => $model->getFirstError(),
                    'csrf' => csrf_hash()
                ]);
            }
        }

        if ($this->request->data['statusUser'] == 'user') {
            if ($model->validate() && $model->search()) {
                $modelResult = $model->getResult();
                return $this->respond([
                    'res' => true,
                    'insert_type' => $modelResult['insert_type'],
                    'html' => view('search/' . $modelResult['template'], $modelResult),
                    'csrf' => csrf_hash()
                ]);
            } else {
                return $this->respond([
                    'res' => false,
                    'errors' => $model->getFirstError(),
                    'csrf' => csrf_hash()
                ]);
            }
        }
    }
}
