<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use Common\Models\EventModel;
use Common\Models\UserModel;
use Common\Models\EventGuestUserModel;
use Common\Models\ReportModel;
use App\Models\commonEvent\SaveBaseEventForm;
use App\Models\commonEvent\CommonEventDeleteForm;
use App\Models\commonEvent\CommonAcceptInviteForm;
use App\Models\commonEvent\CommonDeclineInviteForm;
use App\Models\commonEvent\SetViewedInviteStatusForm;
use App\Models\commonEvent\CommonEventSaveReportForm;

class EventController extends BaseController
{
	use ResponseTrait;

	public function index() {
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Event.Events of the current day'),
			'contentTitle' => lang('Event.events of the current day'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'event',
			'subType' => 'current',
			'createUrl' => '/event/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}
	
	public function scheduled()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Event.Scheduled events'),
			'contentTitle' => lang('Event.scheduled events'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'event',
			'subType' => 'scheduled',
			'createUrl' => '/event/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function completed()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Event.Completed events'),
			'contentTitle' => lang('Event.completed events'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'event',
			'subType' => 'completed',
			'createUrl' => '/event/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function invitations()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Event.Invitations'),
			'contentTitle' => lang('Event.invitations'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'event',
			'subType' => 'invitations',
			'createUrl' => '/event/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function details($id)
	{
		$filterData = $this->request->data;
		$eventModel = new EventModel();
		$event = $eventModel
			->where('user_id', $filterData['userID'])
			->where('event_id', $id)
			->first();

		if (empty($event)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($event->name)) {
			$event->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Event.Event') . ' - ' . $event->name,
			'entity_id' => $event->event_id,
			'type' => 1,
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/event_details', $data);
	}

	public function invitation($id)
	{
		$filterData = $this->request->data;
		$eventModel = new EventModel();
		$event = $eventModel
			->select(EventModel::table() . '.*')
			->select('selfGuest.status as inviteStatus')
			->join(EventGuestUserModel::table() . ' as selfGuest', 'selfGuest.item_id = ' . EventModel::table() . '.event_id and selfGuest.related_item = "' . EventModel::table() . '"')
			->where(EventModel::table() . '.event_id', $id)
			->where('selfGuest.user_id', $filterData['userID'])
			->first();

		if (empty($event)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($event->name)) {
			$event->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Event.Event invitation') . ' - ' . $event->name,
			'entity_id' => $event->event_id,
			'type' => 1,
			'inviteStatus' => $event->inviteStatus,
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/invitation_details', $data);
	}

	public function create()
	{
		$model = new SaveBaseEventForm();

		$filterData = $this->request->data;
		$data = array_merge($filterData, [
			'title' => lang('Event.Create event'),
			'contentTitle' => lang('Event.create event'),
			'model' => $model,
			'durationList' => config('MainEventParamsConfig')->duration,
			'reminderList' => config('MainEventParamsConfig')->reminder,
			'periodicityList' => config('MainEventParamsConfig')->periodicity,
			'formAction' => '/event/create',
			'type' => 'event',
			'createType' => 'create',
		]);

		if ($this->request->getMethod() == 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_EVENT]);
			$model->fill($this->request->requestData);
			$model->scenario = $model::SCENARIO_CREATE;

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/event/details/' . $modelResult['id']);
			} else {
				$data['userModel'] = new UserModel();
				$data['entity'] = $model;
			}
		}

		return view('common-event/create', $data);
	}

	public function edit($id)
	{
		$model = new SaveBaseEventForm();
		$filterData = $this->request->data;
		$eventModel = new EventModel();
		$entity = $eventModel
			->where('event_id', $id)
			->where('user_id', $filterData['userID'])
			->first();

		if (empty($entity)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = array_merge($filterData, [
			'title' => lang('Event.Edit event'),
			'contentTitle' => lang('Event.edit event'),
			'model' => $model,
			'entity' => $entity,
			'guestData' => $eventModel->getGuestList($entity->event_id),
			'durationList' => config('MainEventParamsConfig')->duration,
			'reminderList' => config('MainEventParamsConfig')->reminder,
			'periodicityList' => config('MainEventParamsConfig')->periodicity,
			'formAction' => '/event/edit/' . $id,
			'type' => 'event',
			'createType' => 'edit',
		]);

		if ($this->request->getMethod() == 'post') {
			$this->request->requestData = array_merge($this->request->requestData, [
				'type' => $model::TYPE_EVENT,
				'id' => $id,
			]);
			$model->fill($this->request->requestData);
			$model->scenario = $model::SCENARIO_UPDATE;

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/event/details/' . $modelResult['id']);
			}
		}
		
		return view('common-event/create', $data);
	}

	public function delete()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventDeleteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_EVENT;

		if ($model->validate() && $model->deleteItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function acceptInvite()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonAcceptInviteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_EVENT;

		if ($model->validate() && $model->accept()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function declineInvite()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonDeclineInviteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_EVENT;

		if ($model->validate() && $model->accept()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function setViewedInviteStatus()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SetViewedInviteStatusForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_EVENT;

		if ($model->validate() && $model->setStatus()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function createReport($eventId)
	{
		helper('previousUrl');
		$model = new CommonEventSaveReportForm();
		$model->scenario = CommonEventSaveReportForm::SCENARIO_CREATE;
		$filterData = $this->request->data;
		$eventModel = new EventModel();
		$previousUrl = previous_url();
		$event = $eventModel
			->where('user_id', $filterData['userID'])
			->where('event_id', $eventId)
			->first();

		if (empty($event)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('Report.Create event report'),
			'type' => $model::TYPE_EVENT,
			'id' => $eventId,
			'action' => $model->scenario,
			'model' => $model,
			'reportMaxVideo' => config('MainEventParamsConfig')->reportMaxVideo,
			'reportMaxImage' => config('MainEventParamsConfig')->reportMaxImage,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['details', 'completed']], '/event/details/' . $event->event_id)
		];

		if ($this->request->getMethod() === 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_EVENT]);
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/event/report/' . $modelResult->report_id);
			}
		}
		
		$data = array_merge($data, $filterData);

		return view('common-event/create_report', $data);
	}

	public function editReport($eventId)
	{
		helper('previousUrl');
		$model = new CommonEventSaveReportForm();
		$model->scenario = CommonEventSaveReportForm::SCENARIO_EDIT;
		$filterData = $this->request->data;
		$eventModel = new EventModel();
		$previousUrl = previous_url();
		$event = $eventModel
			->where('user_id', $filterData['userID'])
			->where('event_id', $eventId)
			->first();

		if (empty($event)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$reportModel = new ReportModel();
		$report = $reportModel
			->where('related_item', $eventModel::table())
			->where('item_id', $event->event_id)
			->first();

		if (empty($report)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('Report.Edit event report'),
			'type' => $model::TYPE_EVENT,
			'id' => $eventId,
			'action' => $model->scenario,
			'model' => $model,
			'report' => $report,
			'reportMaxVideo' => config('MainEventParamsConfig')->reportMaxVideo,
			'reportMaxImage' => config('MainEventParamsConfig')->reportMaxImage,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['details', 'report', 'completed']], '/event/report/' . $report->report_id),
		];

		if ($this->request->getMethod() === 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_EVENT]);
			$model->fill($this->request->requestData);

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/event/report/' . $modelResult->report_id);
			}
		}
		
		$data = array_merge($data, $filterData);

		return view('common-event/create_report', $data);
	}

	public function reportDetails($reportId)
	{
		helper('previousUrl');
		$filterData = $this->request->data;
		$reportModel = new ReportModel();
		$previousUrl = previous_url();
		$report = $reportModel
			->select(ReportModel::table() . '.*')
			->select('eventTable.name')
			->select('eventTable.event_id')
			->join(EventModel::table() . ' as eventTable', 'eventTable.event_id = ' . ReportModel::table() . '.item_id and ' . ReportModel::table() . '.related_item = "' . EventModel::table() . '"')
			->where('report_id', $reportId)
			->where('eventTable.user_id', $filterData['userID'])
			->first();

		if (empty($report)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($report->name)) {
			$report->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Report.Event report') . ' - ' . $report->name,
			'entity_id' => $report->item_id,
			'report_id' => $report->report_id,
			'type' => 1,
			'typeLabel' => 'event',
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['details', 'completed']], '/event/details/' . $report->event_id),
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/report_details', $data);
	}

	public function deleteReport($eventId, $reportId)
	{
		$filterData = $this->request->data;
		$reportModel = new ReportModel();
		$report = $reportModel
			->select(ReportModel::table() . '.*')
			->select('eventTable.event_id')
			->join(EventModel::table() . ' as eventTable', 'eventTable.event_id = ' . ReportModel::table() . '.item_id and ' . ReportModel::table() . '.related_item = "' . EventModel::table() . '"')
			->where('report_id', $reportId)
			->where('eventTable.event_id', $eventId)
			->where('eventTable.user_id', $filterData['userID'])
			->first();

		if (empty($report)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (!$reportModel->delete($report->report_id)) {
			return redirect()->back();
		}

		return redirect()->to('/event/details/' . $report->event_id);
	}
}