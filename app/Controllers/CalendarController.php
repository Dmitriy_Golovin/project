<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\CalendarModel;
use CodeIgniter\I18n\Time;
use Common\Models\CountryModel;
use Common\Models\UserSettingModel;
use App\Models\calendar\CalendarContentForm;
use App\Models\calendar\CalendarSettingChangeForm;
use App\Models\calendar\CalendarEventWinContentForm;
use App\Models\commonEvent\SaveBaseEventForm;

class CalendarController extends BaseController
{
	use ResponseTrait;

	public function index() {
		$db = db_connect();
		$userSettingModel = new UserSettingModel($db);
		$filterData = $this->request->data;
		$userSettings = $userSettingModel->getUserSettings($filterData['user_id']);
		$mainEventParamsConfig = config('MainEventParamsConfig');

		$data = [
			'title' => 'Календарь',
			'holidaySettings' => ($userSettings->holidays == 1) ? "checked" : "",
			'myEventsSettings' => ($userSettings->my_events == 1) ? "checked" : "",
			'myMeetingsSettings' => ($userSettings->my_meetings == 1) ? "checked" : "",
			'myTasksSettings' => ($userSettings->my_tasks == 1) ? "checked" : "",
			'friendsEventsSettings' => ($userSettings->friends_events == 1) ? "checked" : "",
			'durationList' => config('MainEventParamsConfig')->duration,
			'periodicityList' => config('MainEventParamsConfig')->periodicity,
			'reminderList' => config('MainEventParamsConfig')->reminder,
			'maxVideo' => $mainEventParamsConfig->eventCreateMaxVideo,
			'maxImage' => $mainEventParamsConfig->eventCreateMaxImage,
			'maxDocument' => $mainEventParamsConfig->eventCreateMaxDocument,
		];
		
		$data = array_merge($data, $filterData);

		return view('calendar/index', $data);
	}

	public function getContentInCell()
	{
		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CalendarContentForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->get()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function getContentForEventWin()
	{
		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CalendarEventWinContentForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->get()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'html' => view('calendar/new_win_event_content', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function changeSettings()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CalendarSettingChangeForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->change()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}
	
	public function saveBaseEvent() {
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SaveBaseEventForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_CREATE;

		if ($model->validate() && $model->save()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}
}