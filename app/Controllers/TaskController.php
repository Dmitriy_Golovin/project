<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use CodeIgniter\Database\BaseBuilder;
use Common\Models\TaskModel;
use Common\Models\TaskHistoryModel;
use Common\Models\TaskHistoryFileModel;
use Common\Models\UserModel;
use App\Models\commonEvent\SaveBaseEventForm;
use App\Models\commonEvent\CommonEventDeleteForm;
use App\Models\task\TaskListForm;
use App\Models\task\TaskDetailsForm;
use App\Models\task\TaskHistoryForm;
use App\Models\task\TaskListItemForm;
use App\Models\task\TaskHistoryListItemForm;
use App\Models\task\setStatusViewedByAssignedForm;
use App\Models\task\TaskActionSaveForm;
use App\Models\task\TaskActionDataForm;

class TaskController extends BaseController
{
	use ResponseTrait;

	public function index() {
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Task.Tasks of the current day'),
			'contentTitle' => lang('Task.tasks of the current day'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'task',
			'subType' => 'current',
			'createUrl' => '/task/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function scheduled()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Task.Scheduled tasks'),
			'contentTitle' => lang('Task.scheduled tasks'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'task',
			'subType' => 'scheduled',
			'createUrl' => '/task/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function completed()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Task.Completed tasks'),
			'contentTitle' => lang('Task.completed tasks'),
			'privacyTypeList' => $eventConfig->privacy,
			'reviewStatusList' => $eventConfig->reviewStatusWithoutRejected,
			'type' => 'task',
			'subType' => 'completed',
			'createUrl' => '/task/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function overdue()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Task.Overdue tasks'),
			'contentTitle' => lang('Task.overdue tasks'),
			'privacyTypeList' => $eventConfig->privacy,
			'type' => 'task',
			'subType' => 'overdue',
			'createUrl' => '/task/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function created()
	{
		$filterData = $this->request->data;
		$eventConfig = config('MainEventParamsConfig');

		$data = [
			'title' => lang('Task.Created tasks'),
			'contentTitle' => lang('Task.created tasks'),
			'privacyTypeList' => $eventConfig->privacy,
			'taskStatusList' => $eventConfig->taskStatus,
			'reviewStatusList' => $eventConfig->reviewStatus,
			'type' => 'task',
			'subType' => 'created',
			'createUrl' => '/task/create',
		];
		
		$data = array_merge($data, $filterData);

		return view('common-event/index', $data);
	}

	public function getList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new TaskListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('task/task_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function assignedDetails($id)
	{
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$task = $taskModel
			->where('assigned_user_id', $filterData['userID'])
			->where('task_id', $id)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($task->name)) {
			$task->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Task.Task') . ' - ' . $task->name,
			'entity_id' => $task->task_id,
			'detailsType' => TaskDetailsForm::DETAILS_TYPE_ASSIGNED,
			'type' => 3,
		];

		if ((int)$task->is_viewed_assigned === TaskModel::STATUS_NOT_VIEW_ASSIGNED) {
			$model = new setStatusViewedByAssignedForm();
			$model->fill([
				'user_id' => $filterData['userID'],
				'task_id' => $id,
				'owner_user_id' => $task->owner_user_id,
			]);

			if (!$model->validate() || !$model->setStatusViewed()) {
				$data['model'] = $model;
			}
		}
		
		$data = array_merge($data, $filterData);

		return view('task/assigned_details', $data);
	}

	public function ownerDetails($id)
	{
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$task = $taskModel
			->where('owner_user_id', $filterData['userID'])
			->where('task_id', $id)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($task->name)) {
			$task->name = lang('Main.No title');
		}

		$data = [
			'title' => lang('Task.Task') . ' - ' . $task->name,
			'entity_id' => $task->task_id,
			'detailsType' => TaskDetailsForm::DETAILS_TYPE_OWNER,
			'type' => 3,
		];
		
		$data = array_merge($data, $filterData);

		return view('task/assigned_details', $data);
	}

	public function getTaskDetailsData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new TaskDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('task/details_data', $modelResult),
				// 'qqq' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function create()
	{
		$model = new SaveBaseEventForm();
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$filterData = $this->request->data;
		$data = array_merge($filterData, [
			'title' => lang('Task.Create task'),
			'contentTitle' => lang('Task.create task'),
			'model' => $model,
			'durationList' => $mainEventParamsConfig->duration,
			'reminderList' => $mainEventParamsConfig->reminder,
			'periodicityList' => $mainEventParamsConfig->periodicity,
			'maxVideo' => $mainEventParamsConfig->eventCreateMaxVideo,
			'maxImage' => $mainEventParamsConfig->eventCreateMaxImage,
			'maxDocument' => $mainEventParamsConfig->eventCreateMaxDocument,
			'formAction' => '/task/create',
			'type' => 'task',
			'createType' => 'create',
		]);

		if ($this->request->getMethod() == 'post') {
			$this->request->requestData = array_merge($this->request->requestData, ['type' => $model::TYPE_TASK]);
			$model->fill($this->request->requestData);
			$model->scenario = $model::SCENARIO_CREATE;

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/owner-details/' . $modelResult['id']);
			} else {
				$data['userModel'] = new UserModel();
				$data['entity'] = $model;
			}
		}

		return view('task/create', $data);
	}

	public function edit($id)
	{
		$model = new SaveBaseEventForm();
		$filterData = $this->request->data;
		$mainEventParamsConfig = config('MainEventParamsConfig');
		$taskModel = new TaskModel();
		$taskHistoryFileModel = new TaskHistoryFileModel();
		$entity = $taskModel
			->select(TaskModel::table() . '.*')
			->select('taskHistoryTable.task_history_id')
			->join(TaskHistoryModel::table() . ' as taskHistoryTable', '(taskHistoryTable.task_id = ' . TaskModel::table() . '.task_id or taskHistoryTable.task_id = ' . TaskModel::table() . '.source_id) and taskHistoryTable.type in (' . TaskHistoryModel::TYPE_CREATE . ') and taskHistoryTable.is_main = ' . TaskHistoryModel::IS_MAIN)
			->where(TaskModel::table() . '.task_id', $id)
			->where('owner_user_id', $filterData['userID'])
			->where('status !=', TaskModel::STATUS_COMPLETED)
			->first();

		if (empty($entity)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$entity->fileList = $taskHistoryFileModel->getFileList($entity->task_history_id, $filterData['userID']);

		$data = array_merge($filterData, [
			'title' => lang('Task.Edit task'),
			'contentTitle' => lang('Task.edit task'),
			'model' => $model,
			'entity' => $entity,
			'userModel' => new UserModel(),
			'durationList' => $mainEventParamsConfig->duration,
			'reminderList' => $mainEventParamsConfig->reminder,
			'periodicityList' => $mainEventParamsConfig->periodicity,
			'maxVideo' => $mainEventParamsConfig->eventCreateMaxVideo,
			'maxImage' => $mainEventParamsConfig->eventCreateMaxImage,
			'maxDocument' => $mainEventParamsConfig->eventCreateMaxDocument,
			'formAction' => '/task/edit/' . $id,
			'type' => 'task',
			'createType' => 'edit',
		]);

		if ($this->request->getMethod() == 'post') {
			$this->request->requestData = array_merge($this->request->requestData, [
				'type' => $model::TYPE_TASK,
				'id' => $id,
			]);
			$model->fill($this->request->requestData);
			$model->scenario = $model::SCENARIO_UPDATE;

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/owner-details/' . $modelResult['id']);
			}
		}
		
		return view('task/create', $data);
	}

	public function history($id)
	{
		helper('previousUrl');
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('task_id', $id)
			->groupStart()
				->where('assigned_user_id', $filterData['userID'])
				->orWhere('owner_user_id', $filterData['userID'])
			->groupEnd()
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Task history'),
			'contentTitle' => lang('TaskHistory.task history'),
			'historyStatusList' => TaskHistoryModel::getStatusLabels(),
			'task_id' => $id,
			'source_id' => $task->source_id,
			'taskName' => (!empty($task->name))
				? $task->name
				: $task->name = lang('Main.No title'),
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['assigned-details', 'owner-details', 'completed']], '/task'),
		];
		
		$data = array_merge($data, $filterData);

		return view('task/history', $data);
	}

	public function getHistoryList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new TaskHistoryForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('task/task_history_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getTaskListItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new TaskListItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('task/task_item', $modelResult),
				'entity_id' => $modelResult['item']->task_id,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function complete($taskId)
	{
		helper('previousUrl');
		$model = new TaskActionSaveForm();
		$model->scenario = TaskActionSaveForm::SCENARIO_CREATE;
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('assigned_user_id', $filterData['userID'])
			->where('task_id', $taskId)
			->where('status !=', TaskModel::STATUS_COMPLETED)
			->where('review_status !=', TaskModel::STATUS_ACCEPTED)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Complete task'),
			'id' => $taskId,
			'model' => $model,
			'type' => $model->scenario,
			'action' => TaskHistoryModel::ACTION_TYPE_COMPLETE,
			'taskHistoryMaxVideo' => config('MainEventParamsConfig')->taskHistoryMaxVideo,
			'taskHistoryMaxImage' => config('MainEventParamsConfig')->taskHistoryMaxImage,
			'taskHistoryMaxDocument' => config('MainEventParamsConfig')->taskHistoryMaxDocument,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['assigned-details', 'scheduled', 'overdue']], '/task/assigned-details/' . $task->task_id)
		];

		if ($this->request->getMethod() == 'post') {
			$model->fill(array_merge($this->request->requestData, ['task' => $task]));

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/assigned-details/' . $modelResult['taskId']);
			}
		}

		$data = array_merge($data, $filterData);

		return view('task/complete', $data);
	}

	public function completeEdit($taskId, $taskHistoryId)
	{
		helper('previousUrl');
		$model = new TaskActionSaveForm();
		$model->scenario = TaskActionSaveForm::SCENARIO_EDIT;
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('assigned_user_id', $filterData['userID'])
			->where('task_id', $taskId)
			->where('status', TaskModel::STATUS_COMPLETED)
			->where('review_status', TaskModel::STATUS_NOT_REVIEWED)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$taskHistoryModel = new TaskHistoryModel();
		$taskHistory = $taskHistoryModel
			->where('task_history_id', $taskHistoryId)
			->where('task_id', $taskId)
			->where('user_id', $filterData['userID'])
			->first();

		if (empty($taskHistory)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Complete task (edit)'),
			'id' => $taskId,
			'model' => $model,
			'type' => $model->scenario,
			'action' => TaskHistoryModel::ACTION_TYPE_COMPLETE,
			'taskHistory' => $taskHistory,
			'taskHistoryMaxVideo' => config('MainEventParamsConfig')->taskHistoryMaxVideo,
			'taskHistoryMaxImage' => config('MainEventParamsConfig')->taskHistoryMaxImage,
			'taskHistoryMaxDocument' => config('MainEventParamsConfig')->taskHistoryMaxDocument,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['assigned-details', 'scheduled', 'overdue']], '/task/assigned-details/' . $task->task_id)
		];

		if ($this->request->getMethod() == 'post') {
			$model->fill(array_merge($this->request->requestData, [
				'task' => $task,
				'task_history_id' => $taskHistoryId,
			]));

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/assigned-details/' . $modelResult['taskId']);
			}
		}

		$data = array_merge($data, $filterData);

		return view('task/complete', $data);
	}

	public function accept($taskId)
	{
		helper('previousUrl');
		$model = new TaskActionSaveForm();
		$model->scenario = TaskActionSaveForm::SCENARIO_CREATE;
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('owner_user_id', $filterData['userID'])
			->where('task_id', $taskId)
			->where('status', TaskModel::STATUS_COMPLETED)
			->where('review_status !=', TaskModel::STATUS_ACCEPTED)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Accept task completion'),
			'id' => $taskId,
			'model' => $model,
			'type' => $model->scenario,
			'action' => TaskHistoryModel::ACTION_TYPE_ACCEPT,
			'taskHistoryMaxVideo' => config('MainEventParamsConfig')->taskHistoryMaxVideo,
			'taskHistoryMaxImage' => config('MainEventParamsConfig')->taskHistoryMaxImage,
			'taskHistoryMaxDocument' => config('MainEventParamsConfig')->taskHistoryMaxDocument,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['owner-details', 'created']], '/task/owner-details/' . $task->task_id)
		];

		if ($this->request->getMethod() == 'post') {
			$model->fill(array_merge($this->request->requestData, ['task' => $task]));

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/owner-details/' . $modelResult['taskId']);
			}
		}

		$data = array_merge($data, $filterData);

		return view('task/accept', $data);
	}

	public function acceptEdit($taskId, $taskHistoryId)
	{
		helper('previousUrl');
		$model = new TaskActionSaveForm();
		$model->scenario = TaskActionSaveForm::SCENARIO_EDIT;
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('owner_user_id', $filterData['userID'])
			->where('task_id', $taskId)
			->where('status', TaskModel::STATUS_COMPLETED)
			->where('review_status', TaskModel::STATUS_ACCEPTED)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$taskHistoryModel = new TaskHistoryModel();
		$taskHistory = $taskHistoryModel
			->where('task_history_id', $taskHistoryId)
			->where('task_id', $taskId)
			->where('user_id', $filterData['userID'])
			->first();

		if (empty($taskHistory)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Accept task completion (edit)'),
			'id' => $taskId,
			'model' => $model,
			'type' => $model->scenario,
			'action' => TaskHistoryModel::ACTION_TYPE_ACCEPT,
			'taskHistory' => $taskHistory,
			'taskHistoryMaxVideo' => config('MainEventParamsConfig')->taskHistoryMaxVideo,
			'taskHistoryMaxImage' => config('MainEventParamsConfig')->taskHistoryMaxImage,
			'taskHistoryMaxDocument' => config('MainEventParamsConfig')->taskHistoryMaxDocument,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['owner-details', 'created']], '/task/owner-details/' . $task->task_id)
		];

		if ($this->request->getMethod() == 'post') {
			$model->fill(array_merge($this->request->requestData, [
				'task' => $task,
				'task_history_id' => $taskHistoryId,
			]));

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/owner-details/' . $modelResult['taskId']);
			}
		}

		$data = array_merge($data, $filterData);

		return view('task/accept', $data);
	}

	public function reject($taskId)
	{
		helper('previousUrl');
		$model = new TaskActionSaveForm();
		$model->scenario = TaskActionSaveForm::SCENARIO_CREATE;
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('owner_user_id', $filterData['userID'])
			->where('task_id', $taskId)
			->where('status', TaskModel::STATUS_COMPLETED)
			->where('review_status !=', TaskModel::STATUS_DECLINED)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Reject task completion'),
			'id' => $taskId,
			'model' => $model,
			'type' => $model->scenario,
			'action' => TaskHistoryModel::ACTION_TYPE_REJECT,
			'taskHistoryMaxVideo' => config('MainEventParamsConfig')->taskHistoryMaxVideo,
			'taskHistoryMaxImage' => config('MainEventParamsConfig')->taskHistoryMaxImage,
			'taskHistoryMaxDocument' => config('MainEventParamsConfig')->taskHistoryMaxDocument,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['owner-details', 'created']], '/task/owner-details/' . $task->task_id)
		];

		if ($this->request->getMethod() == 'post') {
			$model->fill(array_merge($this->request->requestData, ['task' => $task]));

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/owner-details/' . $modelResult['taskId']);
			}
		}

		$data = array_merge($data, $filterData);

		return view('task/reject', $data);
	}

	public function rejectEdit($taskId, $taskHistoryId)
	{
		helper('previousUrl');
		$model = new TaskActionSaveForm();
		$model->scenario = TaskActionSaveForm::SCENARIO_EDIT;
		$filterData = $this->request->data;
		$taskModel = new TaskModel();
		$previousUrl = previous_url();
		$task = $taskModel
			->where('owner_user_id', $filterData['userID'])
			->where('task_id', $taskId)
			->where('review_status', TaskModel::STATUS_DECLINED)
			->first();

		if (empty($task)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$taskHistoryModel = new TaskHistoryModel();
		$taskHistory = $taskHistoryModel
			->where('task_history_id', $taskHistoryId)
			->where('task_id', $taskId)
			->where('user_id', $filterData['userID'])
			->first();

		if (empty($taskHistory)) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$data = [
			'title' => lang('TaskHistory.Reject task completion (edit)'),
			'id' => $taskId,
			'model' => $model,
			'type' => $model->scenario,
			'action' => TaskHistoryModel::ACTION_TYPE_REJECT,
			'taskHistory' => $taskHistory,
			'taskHistoryMaxVideo' => config('MainEventParamsConfig')->taskHistoryMaxVideo,
			'taskHistoryMaxImage' => config('MainEventParamsConfig')->taskHistoryMaxImage,
			'taskHistoryMaxDocument' => config('MainEventParamsConfig')->taskHistoryMaxDocument,
			'previousUrl' => getPreviousUrl($previousUrl, ['segment' => '4', 'variant' => ['owner-details', 'created']], '/task/owner-details/' . $task->task_id)
		];

		if ($this->request->getMethod() == 'post') {
			$model->fill(array_merge($this->request->requestData, [
				'task' => $task,
				'task_history_id' => $taskHistoryId,
			]));

			if ($model->validate() && $model->save()) {
				$modelResult = $model->getResult();
				return redirect()->to('/task/owner-details/' . $modelResult['taskId']);
			}
		}

		$data = array_merge($data, $filterData);

		return view('task/reject', $data);
	}

	public function getTaskActionData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new TaskActionDataForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getData()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('task/task_history_action_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getTaskHistoryListItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new TaskHistoryListItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('task/task_history_item', $modelResult),
				'taskDetailsButton' => view('task/task_details_button', $modelResult),
				'task_history_id' => $modelResult['item']->task_history_id,
				'scenario' => $modelResult['item']->scenario,
				'statusDetailsData' => $modelResult['statusDetailsData'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function delete()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventDeleteForm();
		$model->fill($this->request->requestData);
		$model->scenario = $model::SCENARIO_TASK;

		if ($model->validate() && $model->deleteItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
}