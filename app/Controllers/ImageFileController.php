<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\imageFile\ImageSliderReportDataForm;
use App\Models\imageFile\ImageSliderTaskHistoryDataForm;
use App\Models\imageFile\ImageSliderSelfUserDataForm;
use App\Models\imageFile\ImageSliderUserDataForm;

class ImageFileController extends BaseController
{
    use ResponseTrait;

    public function reportSlider()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new ImageSliderReportDataForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getData()) {
            $modelResult = $model->getResult();
            $filterData = $this->request->data;
            $modelResult['userAgentMobile'] = (isset($filterData['userAgentMobile']) && $filterData['userAgentMobile'])
                ? true
                : false;

            return $this->respond([
                'res' => true,
                'html' => view('image-file/modal-slider', $modelResult),
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'notice_html' => view('modal/notice_close', ['titleNotice' => $model->getFirstError()]),
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }

    public function taskHistorySlider()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new ImageSliderTaskHistoryDataForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getData()) {
            $modelResult = $model->getResult();
            $filterData = $this->request->data;
            $modelResult['userAgentMobile'] = (isset($filterData['userAgentMobile']) && $filterData['userAgentMobile'])
                ? true
                : false;

            return $this->respond([
                'res' => true,
                'html' => view('image-file/modal-slider', $modelResult),
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'notice_html' => view('modal/notice_close', ['titleNotice' => $model->getFirstError()]),
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }

    public function selfUserSlider()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new ImageSliderSelfUserDataForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getData()) {
            $modelResult = $model->getResult();
            $filterData = $this->request->data;
            $modelResult['userAgentMobile'] = (isset($filterData['userAgentMobile']) && $filterData['userAgentMobile'])
                ? true
                : false;

            return $this->respond([
                'res' => true,
                'html' => view('image-file/modal-slider', $modelResult),
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'notice_html' => view('modal/notice_close', ['titleNotice' => $model->getFirstError()]),
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }

    public function userSlider()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new ImageSliderUserDataForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getData()) {
            $modelResult = $model->getResult();
            $filterData = $this->request->data;
            $modelResult['userAgentMobile'] = (isset($filterData['userAgentMobile']) && $filterData['userAgentMobile'])
                ? true
                : false;

            return $this->respond([
                'res' => true,
                'html' => view('image-file/modal-slider', $modelResult),
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'notice_html' => view('modal/notice_close', ['titleNotice' => $model->getFirstError()]),
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }
}
