<?php namespace App\Controllers;

use App\Models\HolidayModel;
use App\Models\holiday\HoldayDataForm;
use App\Models\holiday\HolidaySearchForm;

class HolidayController extends BaseController
{
	public function index() {
		$filterData = $this->request->data;

		$data = [
			'title' => lang('Main.Holidays'),
			'content' => 'home_content',
			'canonical' => 'holiday',
		];
		
		$data = array_merge($data, $filterData);

		return view('holiday/main', $data);
	}
	
	public function holidayCountry($holiday_id = null) {
		$uri = service('uri');
		$data = [];
		$data = array_merge($data, $this->request->data);

		if (empty(lang('Holiday.' . config('HolidayConfig')->title[$uri->getSegment(2)]))) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		if (empty($holiday_id)) {
			$data['title'] = lang('Holiday.' . config('HolidayConfig')->title[$uri->getSegment(2)]);
			$data['content'] = 'country_' . $uri->getSegment(2);
			$data['canonical'] = 'holiday/ru';

			return view('holiday/main', $data);
		}

		$model = new HoldayDataForm();
		$model->countryCode = $uri->getSegment(2);
		$model->holiday_id = $holiday_id;

		if ($model->getHolidayData()) {
			$modelResult = $model->getResult();
			$data['title'] = $modelResult->holiday_day;
			$data['modelResult'] = $modelResult;
			$data['content'] = 'holiday_data';
			$data['canonical'] = 'holiday/ru/' . $modelResult->holiday_id;

			return view('holiday/main', $data);
		} else {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}
	}

	public function holidaySearch($country_code = null, $query = null) {
		$model = new HolidaySearchForm();
		$model->country_code = $country_code;
		$model->query = $query;
		
		$data = array_merge($this->request->data, [
			'title' => lang('Main.Holidays search'),
			'content' => 'holiday_search',
			'model' => $model,
			'modelResult' => null,
			'canonical' => 'holiday/holiday-search/ru'
		]);

		if ($model->search()) {
			$data['modelResult'] = $model->getResult();
		}

		return view('holiday/main', $data);
	}
}