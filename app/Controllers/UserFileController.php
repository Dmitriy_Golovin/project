<?php 
namespace App\Controllers;

use Common\Models\FileModel;

class UserFileController extends BaseController
{
	public function getAvatar($imgName) {
		$type = FileModel::TYPE_IMAGE;
		$path = FileModel::getUploadFileDir($type) . '/' . $imgName;
		$img = new \CodeIgniter\Files\File($path);

		if (!empty($img)) {
			$this->response->setHeader('Content-Type', $img->getMimeType());
			$file = file_get_contents($path, true);
			echo $file;
		}
	}

	public function getImage($imgName) {
		$type = FileModel::TYPE_IMAGE;
		$path = FileModel::getUploadFileDir($type) . '/' . $imgName;
		$img = new \CodeIgniter\Files\File($path);

		if (!empty($img)) {
			$this->response->setHeader('Content-Type', $img->getMimeType());
			$file = file_get_contents($path, true);
			echo $file;
		}
	}

	public function getAudio($imgName) {
		$type = FileModel::TYPE_AUDIO;
		$path = FileModel::getUploadFileDir($type) . '/' . $imgName;
		$file = new \CodeIgniter\Files\File($path);

		if (!empty($file)) {
			$this->response->setHeader('Content-Type', $file->getMimeType());
			$this->response->setHeader('Content-Length', (string)$file->getSize());
			$this->response->setHeader('Accept-Ranges', 'bytes');
			$this->response->setHeader('Content-Range', 'bytes 0-' . $file->getSize() . '/' . $file->getSize());
			$file = file_get_contents($path, true);
			echo $file;
		}
	}

	public function getVideo($imgName) {
		$type = FileModel::TYPE_VIDEO;
		$path = FileModel::getUploadFileDir($type) . '/' . $imgName;
		$file = new \CodeIgniter\Files\File($path);

		if (!empty($file)) {
			$this->response->setHeader('Content-Type', $file->getMimeType());
			//$this->response->setHeader('Content-Disposition', 'inline');
			$this->response->setHeader('Content-Length', (string)$file->getSize());
			$this->response->setHeader('Accept-Ranges', 'bytes');
			$this->response->setHeader('Content-Range', 'bytes 0-' . $file->getSize() . '/' . $file->getSize());
			$file = file_get_contents($path, true);
			echo $file;
		}
	}

	public function getDocument($fileName) {
		$type = FileModel::TYPE_DOCUMENT;
		$path = FileModel::getUploadFileDir($type) . '/' . $fileName;
		$file = new \CodeIgniter\Files\File($path);

		if (!empty($file)) {
			return $this->response->download($path, null, true);
		}
	}
}