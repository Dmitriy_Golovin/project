<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Libraries\MainLib\MainLib;
use App\Models\UserModel;
use App\Models\FileModel;
use App\Models\user\EditPersonalDataForm;
use App\Models\user\CheckPasswordForm;
use App\Models\user\CreateTempEmailForm;
use App\Models\user\ChangePasswordForm;
use App\Models\user\ChangeAboutSelfForm;
use App\Models\user\FriendshipUserListForm;
use App\Models\user\FriendsListForm;
use App\Models\user\SearchUsersWithoutFriendsForm;
use App\Models\user\LoadAvatarForm;
use App\Models\user\LoadFileForm;
use App\Models\user\SetAvatarForm;
use App\Models\user\UserListForm;
use App\Models\user\UserChangeSettingForm;
use App\Models\user\DeleteAccountForm;

class UserController extends BaseController
{
	use ResponseTrait;

	public function editPersonalData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new EditPersonalDataForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->edit()) {
			return $this->respond([
				'res' => true,
				'user' => $model->getResult(),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function checkPassword()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CheckPasswordForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->check()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'tcsd' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function createTempEmail()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CreateTempEmailForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->create()) {
			return $this->respond([
				'res' => true,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}
	
	public function changePassword()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChangePasswordForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->change()) {
			return $this->respond([
				'res' => true,
				'message' => ['result' => lang('Main.New password saved'), 'default' => lang('Main.Change Password')],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function changeAboutSelf()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new ChangeAboutSelfForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->change()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	// public function getFriendshipUserList()
	// {
	// 	if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
	// 		$this->failUnauthorized(lang('Main.Authorization error'));
	// 	}

	// 	if (!$this->request->isAJAX()) {
	// 		throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	// 	}

	// 	$model = new FriendshipUserListForm();
	// 	$model->fill($this->request->requestData);

	// 	if ($model->validate() && $model->getList()) {
	// 		$modelResult = $model->getResult();

	// 		return $this->respond([
	// 			'res' => true,
	// 			'data' => $modelResult,
	// 			'csrf' => csrf_hash(),
	// 		]);
	// 	} else {
	// 		return $this->respond([
	// 			'res' => false,
	// 			'errors' => $model->getFirstError(),
	// 			'csrf' => csrf_hash(),
	// 		]);
	// 	}
	// }

	// public function getFriendsList()
	// {
	// 	if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
	// 		$this->failUnauthorized(lang('Main.Authorization error'));
	// 	}

	// 	if (!$this->request->isAJAX()) {
	// 		throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	// 	}

	// 	$model = new FriendsListForm();
	// 	$model->fill($this->request->requestData);

	// 	if ($model->validate() && $model->getList()) {
	// 		$modelResult = $model->getResult();

	// 		return $this->respond([
	// 			'res' => true,
	// 			'data' => $modelResult,
	// 			'csrf' => csrf_hash(),
	// 		]);
	// 	} else {
	// 		return $this->respond([
	// 			'res' => false,
	// 			'errors' => $model->getFirstError(),
	// 			'csrf' => csrf_hash(),
	// 		]);
	// 	}
	// }

	// public function searchUsersWithoutFriends()
	// {
	// 	if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
	// 		$this->failUnauthorized(lang('Main.Authorization error'));
	// 	}

	// 	if (!$this->request->isAJAX()) {
	// 		throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
	// 	}

	// 	$model = new SearchUsersWithoutFriendsForm();
	// 	$model->fill($this->request->requestData);

	// 	if ($model->validate() && $model->getList()) {
	// 		$modelResult = $model->getResult();

	// 		return $this->respond([
	// 			'res' => true,
	// 			'data' => $modelResult,
	// 			'csrf' => csrf_hash(),
	// 		]);
	// 	} else {
	// 		return $this->respond([
	// 			'res' => false,
	// 			'errors' => $model->getFirstError(),
	// 			'csrf' => csrf_hash(),
	// 		]);
	// 	}
	// }

	public function loadAvatar()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new LoadAvatarForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->load()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function loadFile()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new LoadFileForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->load()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function selectAvatar()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new SetAvatarForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->set()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function list()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new UserListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'insert_type' => $modelResult['insert_type'],
                'html' => view('user/list_for_event', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function changeSetting()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new UserChangeSettingForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->change()) {

			return $this->respond([
				'res' => true,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getFirstError(),
				'csrf' => csrf_hash(),
			]);
		}
	}

	public function delete($tcsd)
	{
		$filterData = $this->request->data;
		$model = new DeleteAccountForm();
		$model->fill([
			'user_id' => $filterData['userID'],
			'tcsd' => $tcsd,
			'access' => $this->request->getCookie('sta'),
		]);

		if ($model->validate() && $model->delete()) {
			return view('home/success_deleted', array_merge([
				'title' => lang('Main.Successful account deletion'),
			], $filterData));
		} else {
			return view('profile/not_success_deleted', array_merge([
				'title' => '',
				'model' => $model,
			], $filterData));
		}
	}
}