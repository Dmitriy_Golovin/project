<?php

namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\comment\CommentListForm;
use App\Models\comment\SendCommentForm;

class CommentController extends BaseController
{
    use ResponseTrait;

    public function getList()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new CommentListForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->getList()) {
            $modelResult = $model->getResult();

            return $this->respond([
                'res' => true,
                'html' => view('comment/comment_list', $modelResult),
                'showPrevious' => $modelResult['commentData']['showPrevious'],
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }

    public function sendComment()
    {
        if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
            $this->failUnauthorized(lang('Main.Authorization error'));
        }

        if (!$this->request->isAJAX()) {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }

        $model = new SendCommentForm();
        $model->fill($this->request->requestData);

        if ($model->validate() && $model->send()) {
            $modelResult = $model->getResult();

            return $this->respond([
                'res' => true,
                'html' => view('comment/comment_item', $modelResult),
                'csrf' => csrf_hash(),
            ]);
        } else {
            return $this->respond([
                'res' => false,
                'errors' => $model->getErrors(),
                'csrf' => csrf_hash()
            ]);
        }
    }
}
