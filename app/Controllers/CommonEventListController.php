<?php namespace App\Controllers;

use CodeIgniter\API\ResponseTrait;
use App\Models\commonEvent\CommonEventListForm;
use App\Models\commonEvent\MainEventDataForm;
use App\Models\commonEvent\CommonEventDetailsForm;
use App\Models\commonEvent\CommonEventInvitationDetailsForm;
use App\Models\commonEvent\CommonInviteListItemForm;
use App\Models\commonEvent\CommonEventSaveReportDataForm;
use App\Models\commonEvent\CommonEventReportDetailsDataForm;
use App\Models\commonEvent\CommonInviteEntityListForm;
use App\Models\commonEvent\CommonEntityInviteUserForm;

class CommonEventListController extends BaseController
{
	use ResponseTrait;

	public function getList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('common-event/event_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
	
	public function getMainEventData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new MainEventDataForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getData()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'data' => $modelResult,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getCommonEventDetailsData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view($modelResult['typeLabel'] . '/' . $modelResult['typeLabel'] . '_details_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getCommonEventSaveReportData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventSaveReportDataForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getData()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view($modelResult['typeLabel'] . '/create_report_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getCommonEventReportDetailsData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventReportDetailsDataForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getData()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view($modelResult['typeLabel'] . '/report_details_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getCommonEventInvitationDetailsData()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEventInvitationDetailsForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getDetails()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view($modelResult['typeLabel'] . '/' . $modelResult['typeLabel'] . '_invitation_details_data', $modelResult),
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getInviteListItem()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonInviteListItemForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getItem()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view($modelResult['type'] . '/' . $modelResult['type'] . '_item', $modelResult),
				'entity_id' => $modelResult['item']->entity_id,
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function getInviteEntityList()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonInviteEntityListForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->getList()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'htmlTitle' => $modelResult['title'],
				'htmlContent' => view('common-event/invite_entity_list', $modelResult),
				'insert_type' => $modelResult['insert_type'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}

	public function entityInviteUser()
	{
		if (empty($this->request->data['statusUser']) || $this->request->data['statusUser'] === 'guest') {
			$this->failUnauthorized(lang('Main.Authorization error'));
		}

		if (!$this->request->isAJAX()) {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}

		$model = new CommonEntityInviteUserForm();
		$model->fill($this->request->requestData);

		if ($model->validate() && $model->invite()) {
			$modelResult = $model->getResult();

			return $this->respond([
				'res' => true,
				'html' => view('common-event/invite_entity_successfull_sent', []),
				'entityId' => $modelResult['entityId'],
				'csrf' => csrf_hash(),
			]);
		} else {
			return $this->respond([
				'res' => false,
				'errors' => $model->getErrors(),
				'csrf' => csrf_hash()
			]);
		}
	}
}