<?php

namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use App\Libraries\AuthJWT\AuthController;

class AuthorizationWSFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
		$host = null;
		$authorization = null;
		$userAgent = null;
		
		if ($request->hasHeader('X-DATA-WShost')) {
			$host = $request->getHeader('X-DATA-WShost')->getValue();
		}

		if ($request->hasHeader('Authorization')) {
			$authorization = $request->getHeader('Authorization')->getValue();
		}
		
		if ($request->hasHeader('User-Agent')) {
			$userAgent = $request->getHeader('User-Agent')->getValue();
		}
		
		if ($host !== null && $authorization !== null && $userAgent !== null && $host === 'serverWS') {
			$auth = new AuthController();
			preg_match('/<(.*?)>/', $authorization, $matches);
			$accessToken = $matches[1];
			//$request->result = 'resssss' . $accessToken;
			$result = $auth->authorizationWS($request, $accessToken);
			$request->result = $result;
		} else {
			$request->result = false;
		}
    }

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}