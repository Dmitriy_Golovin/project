<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use CodeIgniter\Filters\FilterInterface;
use App\Libraries\AuthJWT\AuthController;
use Common\Models\UserModel;
use Common\Models\FriendModel;
use Common\Models\MessageModel;
use Common\Models\ChatMessageModel;
use Common\Models\EventGuestUserModel;
use Common\Models\TaskModel;
use Common\Models\VisitLogModel;

class AuthorizationFilter implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
    	$visitLogModel = new VisitLogModel();
    	$visitLogModel->createLog($request);
		$uri = service('uri');
		$auth = new AuthController();
		$request->data['permission'] = $request->getCookie('sta');
		$statusUser = $auth->authorization($request, $uri->getPath());
		$langHeader = (!empty($request->getHeader('Accept-Language')))
			? $request->getHeader('Accept-Language')->getValue()
			: '';
		
		if (!empty($langHeader)) {
			$localeStr = (mb_strpos($langHeader, ',') !== false)
				? mb_substr($langHeader, 0, mb_strpos($langHeader, ','))
				: false;
			$country_abbr = ($localeStr && mb_strpos($localeStr, '-') !== false)
				? mb_substr($localeStr, mb_strpos($localeStr, '-') + 1)
				: false;
		} else {
			$country_abbr = 'ru';
		}

		if ($request->getUserAgent()->isMobile()) {
			$request->data['userAgentMobile'] = true;
		}

		if ($request->getMethod() === 'post') {
			$request->requestData = $request->getPost();
		}

		$request->requestData['country_abbr'] = $country_abbr;

		if (is_string($statusUser)) {
			$request->requestData['statusUser'] = $statusUser;
			$resourse = $uri->getSegments();
			$request->data['statusUser'] = $statusUser;
			$request->data['logoUrl'] = '/';
			if ($statusUser == 'user') {
				$userID = $auth->getUserIDByAccessToken($request);
				$request->data['userID'] = $userID;
				$request->data['logoUrl'] = '/' . strtolower(date('Fo'));
				$this->getUserDataInHeader($request, $userID);

				if ($request->getMethod() === 'post') {
					$files = $request->getFiles();

					if (!empty($files)) {
						foreach ($files as $fileIndex => $file) {
							if (is_array($file)) {
								foreach ($file as $fileItem) {
									if (!empty($fileItem->getError())) {
										continue 2;
									}
								}
							} else {
								if (!empty($file->getError())) {
									continue;
								}
							}
							
							$request->requestData[$fileIndex] = $file;
						}
					}

					$request->requestData['user_id'] = $userID;
					$request->requestData['user_country_id'] = $request->data['country_id'];
					$request->requestData['selfAvatar'] = $request->data['pathToAva'];
					$request->requestData['firstName'] = $request->data['firstName'];
					$request->requestData['lastName'] = $request->data['lastName'];
				}
			}

			$permission = $this->checkResourse($resourse);

			return $this->makeRedirect($permission, $statusUser);
		} else {
			return $statusUser;
		}
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
		// Do something here
    }
	
	protected $guestResourse = [
		'/', 
		'login',
		'register',
		'confirm',
		'activation',
		'send-restore-token',
		'restore-password',
	];
	
	protected $userResourse = [
		'cabinet',
		'profile',
		'change_personal_data',
		'messages',
		'message',
		'event',
		'meeting',
		'task',
		'friends',
		'friends-subscribers',
		'friends-subscriptions',
		'file',
		'imageFile',
		'videoFile',
		'city',
		'commonEventList',
		'user',
		'userFile',
		'feedback',
	];
	
	protected $userResourseRegular = [
		'/^(january|february|march|april|may|june|july|august|september|october|november|december)[0-9]{1,6}/'
	];

	protected $questAndUserResourse = [
		'search',
		'holiday',
		'termsofuse',
		'privacy',
		'openFile',
		'comment',
	];
	
	private function checkResourse($resourse) {
		$result = ['guest' => 0, 'user' => 0, 'guestAndUser' => 0];
		
		if (empty($resourse)) {
			$result['guest'] = true;
			return $result;
		}
		
		foreach ($this->questAndUserResourse as $seg1) {
			if ($resourse[0] == $seg1) {
				$result['guestAndUser'] = true;
				break;
				return $result;
			}
		}
		
		foreach ($this->guestResourse as $seg1) {
			if ($resourse[0] == $seg1) {
				$result['guest'] = true;
				break;
			}
		}
		
		foreach ($this->userResourseRegular as $seg1) {
			if (preg_match($seg1, $resourse[0])) {
				$result['user'] = true;
				break;
			}
		}
		
		foreach ($this->userResourse as $seg1) {
			if ($resourse[0] == $seg1) {
				$result['user'] = true;
				break;
			}
		}
		
		return $result;
	}
	
	private function checkResourseForUser($permission, $statusUser) {
		if ($permission['guestAndUser'] == true) {
			return 'no';
		} else if ($statusUser == 'guest' && $permission['user'] == 1) {
			return '/';
		} else if ($statusUser == 'user' && $permission['guest'] == 1) {
			return '/cabinet';
		}
	}
	
	private function makeRedirect($permission, $statusUser) {
		$redirectTo = $this->checkResourseForUser($permission, $statusUser);

		if ($redirectTo !== 'no' && $redirectTo !== null) {
			return redirect()->to($redirectTo);
		}
	}
	
	private function getUserDataInHeader($request, $userID) {
		$db = db_connect();
		$userModel = new UserModel($db);
		$friendModel = new FriendModel();
		$messageModel = new MessageModel();
		$chatMessageModel = new ChatMessageModel();
		$eventGuestUserModel = new EventGuestUserModel();
		$taskModel = new TaskModel();
		$userData = $userModel->getUserData($userID);
		$friendAmont = $friendModel->getAmountOfUsersInSpecificRelationships($userID);
		$mainNewMessageAmount = 0;
		$mainNewMessageAmount += $messageModel->getNewMessagesAmountDB($userID)->status;
		$mainNewMessageAmount += $chatMessageModel->getNewMessagesAmountDB($userID);
		$eventMeetingNewActiveInvite = $eventGuestUserModel->getAllNewActiveInviteList($userID);
		$notViewedTaskCount = $taskModel->getNotViewedTaskCount($userID);
		$eventMeetingNewActiveInvite['mainCount'] += (int)$notViewedTaskCount;

		if (!empty($friendAmont) && $friendAmont->new_subscriber > 0) {
			$newSubscriberAmount = '<span class="new_subscriber_amount">' . $friendAmont->new_subscriber . '</span>';
		} else {
			$newSubscriberAmount = '';
		}
		
		if ($mainNewMessageAmount > 0) {
			$newMessagesAmount = '<span class="new_subscriber_amount">' . $mainNewMessageAmount . '</span>';
		} else {
			$newMessagesAmount = '';
		}
		
		$request->data['user_id'] = $userData->user_id;
		$request->data['firstName'] = $userData->first_name;
		$request->data['lastName'] = $userData->last_name;
		$request->data['pathToAva'] = $userData->path_to_ava;
		$request->data['country'] = $userData->country;
		$request->data['city'] = $userData->city;
		$request->data['country_id'] = $userData->country_id;
		$request->data['city_id'] = $userData->city_id;
		$request->data['numPhone'] = $userData->number_phone;
		$request->data['email'] = $userData->email;
		$request->data['about_self'] = $userData->about_self;
		$request->data['newSubscriberAmount'] = $newSubscriberAmount;
		$request->data['newMessagesAmount'] = $newMessagesAmount;
		$request->data['cabinetData'] = $eventMeetingNewActiveInvite;
	}
}