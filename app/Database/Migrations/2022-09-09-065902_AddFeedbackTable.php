<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFeedbackTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'feedback_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'email' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'text' => [
                'type' => 'TEXT',
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('feedback_id', true);
        $this->forge->addForeignKey('user_id', 'user', 'user_id', 'set null', 'set null');
        $this->forge->createTable('feedback');
    }

    public function down()
    {
        $this->forge->dropTable('feedback');
    }
}
