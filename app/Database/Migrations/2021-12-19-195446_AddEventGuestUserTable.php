<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddEventGuestUserTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'event_guest_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'related_item' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'item_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'status' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'is_visited' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => null,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('event_guest_user_id', true);
		$this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
		$this->forge->createTable('event_guest_user');
	}

	public function down()
	{
		$this->forge->dropTable('event_guest_user');
	}
}
