<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use Common\Models\UserModel;
use Common\Models\HolidayModel;
use Common\Models\CountryModel;
use Common\Models\CityModel;

class AddCityTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'city_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'external_country_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'external_region_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'external_city_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('city_id', true);
		$this->forge->addForeignKey('country_id', 'country', 'country_id', 'cascade', 'cascade');
		$this->forge->createTable('city');

		$holidayModel = new HolidayModel();
		$userModel = new UserModel();
		$countryModel = new CountryModel();
		$cityModel = new CityModel();

		$this->db->query('ALTER TABLE ' . $holidayModel->getTableName() . ' ADD CONSTRAINT `23_xxiii_legion_holiday_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES ' . $countryModel->getTableName() . ' (`country_id`) ON DELETE SET NULL ON UPDATE SET NULL;');

		$this->db->query('ALTER TABLE ' . $userModel->getTableName() . ' ADD CONSTRAINT `23_xxiii_legion_user_country_id_foreign` FOREIGN KEY (`country_id`) REFERENCES ' . $countryModel->getTableName() . ' (`country_id`) ON DELETE SET NULL ON UPDATE SET NULL;');

		$this->db->query('ALTER TABLE ' . $userModel->getTableName() . ' ADD CONSTRAINT `23_xxiii_legion_user_city_id_foreign` FOREIGN KEY (`city_id`) REFERENCES ' . $cityModel->getTableName() . ' (`city_id`) ON DELETE SET NULL ON UPDATE SET NULL;');
	}

	public function down()
	{
		$this->forge->dropTable('city');
	}
}
