<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddPrivateFilePermissionTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'private_file_permission_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'owner_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null
			],
			'permitted_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null
			],
			'file_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('private_file_permission_id', true);
		$this->forge->addForeignKey('owner_id', 'user', 'user_id', 'set null', 'set null');
		$this->forge->addForeignKey('permitted_id', 'user', 'user_id', 'set null', 'set null');
		$this->forge->addForeignKey('file_id', 'file', 'file_id', 'set null', 'set null');
		$this->forge->createTable('private_file_permission');
	}

	public function down()
	{
		$this->forge->dropTable('private_file_permission');
	}
}
