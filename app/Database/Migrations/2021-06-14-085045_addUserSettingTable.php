<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use App\Database\Seeds\AdminUserSeeder;
use Config\Database;

class AddUserSettingTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'user_setting_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'holidays' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'my_events' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'my_meetings' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'my_tasks' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'friends_events' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'work_week_start' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 2,
			],
			'work_days_amount' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 5,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('user_setting_id', true);
		$this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
		$this->forge->createTable('user_setting');

		$config = new Database();
		$adminUserSeeder = new AdminUserSeeder($config);
		$adminUserSeeder->run();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user_setting');
	}
}
