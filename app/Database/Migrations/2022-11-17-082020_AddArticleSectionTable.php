<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddArticleSectionTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'article_section_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'title_transliteration' => [
                'type' => 'VARCHAR',
                'constraint' => 355,
            ],
            'description' => [
                'type' => 'TEXT',
                'default' => null,
            ],
            'meta_description' => [
                'type' => 'TEXT',
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('article_section_id', true);
        $this->forge->createTable('article_section');
    }

    public function down()
    {
        $this->forge->dropTable('article_section');
    }
}
