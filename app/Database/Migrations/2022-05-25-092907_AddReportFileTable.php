<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddReportFileTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'report_file_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'report_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('report_file_id', true);
        $this->forge->addForeignKey('report_id', 'report', 'report_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('file_id', 'file', 'file_id', 'cascade', 'cascade');
        $this->forge->createTable('report_file');
    }

    public function down()
    {
        $this->forge->dropTable('report_file');
    }
}
