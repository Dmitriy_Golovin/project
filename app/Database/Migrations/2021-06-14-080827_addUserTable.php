<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddUserTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'first_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'last_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'city_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'about_self' => [
				'type' => 'TEXT',
				'default' => null,
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'password' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'accept_terms' => [
				'type' => 'TINYINT',
				'constraint' => 1,
			],
			'veryfication_token' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'default' => null
			],
			'number_phone' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'default' => null,
			],
			'file_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null
			],
			'is_blocked' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			],
			'role' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 1,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null
			],

		]);

		$this->forge->addKey('user_id', true);
		$this->forge->addForeignKey('file_id', 'file', 'file_id', 'set null', 'set null');
		// $this->forge->addForeignKey('city_id', 'city', 'city_id', 'set null', 'set null');
		// $this->forge->addForeignKey('country_id', 'country', 'country_id', 'set null', 'set null');
		$this->forge->createTable('user');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('user');
	}
}
