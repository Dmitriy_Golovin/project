<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddMessageTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'message_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'text' => [
				'type' => 'TEXT',
				'default' => null,
			],
			'user_from' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'user_to' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'delete_from' => [
				'type' => 'TINYINT',
				'constraint' => 11,
				'default' => 0
			],
			'delete_to' => [
				'type' => 'TINYINT',
				'constraint' => 11,
				'default' => 0
			],
			'status' => [
				'type' => 'TINYINT',
				'constraint' => 11,
				'default' => 1
			],
			'important_from' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			],
			'important_to' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('message_id', true);
		$this->forge->createTable('message');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('message');
	}
}
