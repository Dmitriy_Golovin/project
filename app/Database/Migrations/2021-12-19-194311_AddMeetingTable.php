<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddMeetingTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'meeting_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'description' => [
				'type' => 'TEXT',
			],
			'location' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'name' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'duration' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'periodicity' => [
				'type' => 'TINYINT',
				'constraint' => 3
			],
			'reminder' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'time' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'date' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'privacy' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'is_completed' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'is_failed' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => null,
			],
			'is_cloned' => [
				'type' => 'TINYINT',
				'constraint' => 1,
			],
			'time_offset_seconds' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('meeting_id', true);
		$this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
		$this->forge->createTable('meeting');
	}

	public function down()
	{
		$this->forge->dropTable('meeting');
	}
}
