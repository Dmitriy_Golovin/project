<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddHolidayTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'holiday_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'description' => [
				'type' => 'TEXT',
			],
			'holiday_day' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'link' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'default' => null,
			],
			'file_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'file_link' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'default' => null,
			],
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('holiday_id', true);
		// $this->forge->addForeignKey('country_id', 'country', 'country_id', 'set null', 'set null');
		$this->forge->addForeignKey('file_id', 'file', 'file_id', 'set null', 'set null');
		$this->forge->createTable('holiday');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('holiday');
	}
}
