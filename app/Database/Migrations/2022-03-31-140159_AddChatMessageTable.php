<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddChatMessageTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'chat_message_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'chat_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'chat_member_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'text' => [
                'type' => 'TEXT',
                'default' => null,
            ],
            'quoted_chat_message_Id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'recipient_chat_member_ids' => [
                'type' => 'JSON',
                'default' => null,
            ],
            'who_viewed_chat_member_ids' => [
                'type' => 'JSON',
                'default' => null,
            ],
            'important_chat_member_ids' => [
                'type' => 'JSON',
                'default' => null,
            ],
            'deleted_chat_member_ids' => [
                'type' => 'JSON',
                'default' => null,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null
            ],
        ]);

        $this->forge->addKey('chat_message_id', true);
        $this->forge->addForeignKey('chat_id', 'chat', 'chat_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('chat_member_id', 'chat_member', 'chat_member_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('quoted_chat_message_Id', 'chat_message', 'chat_message_id', 'cascade', 'cascade');
        $this->forge->createTable('chat_message');
    }

    public function down()
    {
        $this->forge->dropTable('chat_message');
    }
}
