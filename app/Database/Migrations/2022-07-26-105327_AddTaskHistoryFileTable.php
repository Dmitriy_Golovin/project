<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTaskHistoryFileTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'task_history_file_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'task_history_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('task_history_file_id', true);
        $this->forge->addForeignKey('task_history_id', 'task_history', 'task_history_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('file_id', 'file', 'file_id', 'cascade', 'cascade');
        $this->forge->createTable('task_history_file');
    }

    public function down()
    {
        $this->forge->dropTable('task_history_file');
    }
}
