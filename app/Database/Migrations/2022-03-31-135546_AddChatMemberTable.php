<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddChatMemberTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'chat_member_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'chat_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'role' => [
                'type' => 'TINYINT',
                'constraint' => 3,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null
            ],
        ]);

        $this->forge->addKey('chat_member_id', true);
        $this->forge->addForeignKey('chat_id', 'chat', 'chat_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
        $this->forge->createTable('chat_member');
    }

    public function down()
    {
        $this->forge->dropTable('chat_member');
    }
}
