<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use App\Database\Seeds\CountrySeeder;
use Config\Database;

class AddCountryTranslationTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'country_translation_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'lang_code' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'title' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('country_translation_id', true);
		$this->forge->addForeignKey('country_id', 'country', 'country_id', 'cascade', 'cascade');
		$this->forge->createTable('country_translation');

		$config = new Database();
		$countrySeeder = new CountrySeeder($config);
		$countrySeeder->run();
	}

	public function down()
	{
		$this->forge->dropTable('country_translation');
	}
}
