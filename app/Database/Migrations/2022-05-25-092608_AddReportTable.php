<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddReportTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'report_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'related_item' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'item_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'note' => [
                'type' => 'TEXT',
                'default' => null,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('report_id', true);
        $this->forge->createTable('report');
    }

    public function down()
    {
        $this->forge->dropTable('report');
    }
}
