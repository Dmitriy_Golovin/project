<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class Addfriendstable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'friend_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'id_request_recipient' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'id_request_sender' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'status' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('friend_id', true);

		$this->forge->createTable('friend');
	}

	public function down()
	{
		$this->forge->dropTable('friend');
	}
}
