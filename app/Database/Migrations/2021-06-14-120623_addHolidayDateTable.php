<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use App\Database\Seeds\HolidaySeeder;
use Config\Database;

class AddHolidayDateTable extends Migration
{
	public function up()
	{	
		$config = new Database();
		$holidaySeeder = new HolidaySeeder($config);

		$this->forge->addField([
			'holiday_date_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'holiday_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'holiday_date' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('holiday_date_id', true);
		$this->forge->addForeignKey('holiday_id', 'holiday', 'holiday_id', 'cascade', 'cascade');
		$this->forge->createTable('holiday_date');

		$holidaySeeder->run();
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('holiday_date');
	}
}
