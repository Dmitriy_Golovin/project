<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddChatTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'chat_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'pinned_message_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null
            ],
        ]);

        $this->forge->addKey('chat_id', true);
        $this->forge->addForeignKey('file_id', 'file', 'file_id', 'set null', 'set null');
        $this->forge->createTable('chat');
    }

    public function down()
    {
        $this->forge->dropTable('chat');
    }
}
