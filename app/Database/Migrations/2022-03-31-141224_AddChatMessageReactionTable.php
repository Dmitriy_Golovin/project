<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddChatMessageReactionTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'chat_message_reaction_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'chat_message_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'chat_member_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'reaction' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null
            ],
        ]);

        $this->forge->addKey('chat_message_reaction_id', true);
        $this->forge->addForeignKey('chat_message_id', 'chat_message', 'chat_message_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('chat_member_id', 'chat_member', 'chat_member_id', 'cascade', 'cascade');
        $this->forge->createTable('chat_message_reaction');
    }

    public function down()
    {
        $this->forge->dropTable('chat_message_reaction');
    }
}
