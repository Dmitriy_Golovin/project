<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddAccessTokenTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'access_token_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'access_id' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'ip' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'secret' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'user_agent' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('access_token_id', true);
		$this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
		$this->forge->createTable('access_token');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('access_token');
	}
}
