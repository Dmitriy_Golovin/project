<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFileChatMessageTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'file_chat_message_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'chat_message_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'file_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null
            ],
        ]);

        $this->forge->addKey('file_chat_message_id', true);
        $this->forge->addForeignKey('chat_message_id', 'chat_message', 'chat_message_id', 'cascade', 'cascade');
        $this->forge->addForeignKey('file_id', 'file', 'file_id', 'set null', 'set null');
        $this->forge->createTable('file_chat_message');
    }

    public function down()
    {
        $this->forge->dropTable('file_chat_message');
    }
}
