<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddCommentTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'comment_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'related_item' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'item_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'text' => [
                'type' => 'TEXT',
                'default' => null,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('comment_id', true);
        $this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
        $this->forge->createTable('comment');
    }

    public function down()
    {
        $this->forge->dropTable('comment');
    }
}
