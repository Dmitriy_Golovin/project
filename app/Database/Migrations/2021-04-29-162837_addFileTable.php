<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddFileTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'file_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'type' => [
				'type' => 'TINYINT',
				'constraint' => 2,
			],
			'related_item' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'item_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'url' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'mime_type' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'original_name' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'privacy_status' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'preview_height' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'preview_width' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('file_id', true);
		$this->forge->createTable('file');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('file');
	}
}
