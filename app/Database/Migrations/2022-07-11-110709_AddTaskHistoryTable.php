<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTaskHistoryTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'task_history_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'task_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'user_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
            'type' => [
                'type' => 'TINYINT',
                'constraint' => 1,
            ],
            'note' => [
                'type' => 'TEXT',
                'default' => null,
            ],
            'is_main' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 0,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('task_history_id', true);
        $this->forge->addForeignKey('task_id', 'task', 'task_id', 'set null', 'set null');
        $this->forge->addForeignKey('user_id', 'user', 'user_id', 'set null', 'set null');
        $this->forge->createTable('task_history');
    }

    public function down()
    {
        $this->forge->dropTable('task_history');
    }
}
