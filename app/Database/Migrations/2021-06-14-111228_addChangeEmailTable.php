<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddChangeEmailTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'change_email_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'email_tmp' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'token' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
				'default' => null
			],
			'changed' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('change_email_id', true);
		$this->forge->addForeignKey('user_id', 'user', 'user_id', 'cascade', 'cascade');
		$this->forge->createTable('change_email');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('change_email');
	}
}
