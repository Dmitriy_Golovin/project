<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddVisitLogTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'visit_log_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'ip' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'user_agent' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'uri_path' => [
                'type' => 'VARCHAR',
                'constraint' => 255
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('visit_log_id', true);
        $this->forge->createTable('visit_log');
    }

    public function down()
    {
        $this->forge->dropTable('visit_log');
    }
}
