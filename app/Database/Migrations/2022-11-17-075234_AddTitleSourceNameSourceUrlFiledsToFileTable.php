<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTitleSourceNameSourceUrlFiledsToFileTable extends Migration
{
    public function up()
    {
        $fields = [
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => null,
            ],
            'source' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
                'default' => null,
            ],
        ];

        $this->forge->addColumn('file', $fields);
    }

    public function down()
    {
        $this->forge->dropColumn('file', ['title', 'sourceName', 'sourceUrl']);
    }
}
