<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddArticleTable extends Migration
{
    public function up()
    {
        $this->forge->addField([
            'article_id' => [
                'type' => 'INT',
                'constraint' => 11,
                'auto_increment' => true,
            ],
            'article_section_id' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'title' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'title_transliteration' => [
                'type' => 'VARCHAR',
                'constraint' => 355,
            ],
            'description' => [
                'type' => 'TEXT',
                'default' => null,
            ],
            'meta_description' => [
                'type' => 'VARCHAR',
                'constraint' => 255,
            ],
            'status' => [
                'type' => 'TINYINT',
                'constraint' => 1,
                'default' => 1,
            ],
            'created_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'updated_at' => [
                'type' => 'INT',
                'constraint' => 11,
            ],
            'deleted_at' => [
                'type' => 'INT',
                'constraint' => 11,
                'default' => null,
            ],
        ]);

        $this->forge->addKey('article_id', true);
        $this->forge->addForeignKey('article_section_id', 'article_section', 'article_section_id');
        $this->forge->createTable('article');
    }

    public function down()
    {
        $this->forge->dropTable('article');
    }
}
