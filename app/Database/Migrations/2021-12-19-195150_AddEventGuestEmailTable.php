<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddEventGuestEmailTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'event_guest_email_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'related_item' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'item_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'email' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('event_guest_email_id', true);
		$this->forge->createTable('event_guest_email');
	}

	public function down()
	{
		$this->forge->dropTable('event_guest_email');
	}
}
