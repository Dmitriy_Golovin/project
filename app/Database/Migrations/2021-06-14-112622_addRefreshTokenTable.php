<?php namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddRefreshTokenTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'refresh_token_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'access_token_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'refresh_token' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'user_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'expire' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('refresh_token_id', true);
		$this->forge->addForeignKey('access_token_id', 'access_token', 'access_token_id', 'cascade', 'cascade');
		$this->forge->createTable('refresh_token');
	}

	//--------------------------------------------------------------------

	public function down()
	{
		$this->forge->dropTable('refresh_token');
	}
}
