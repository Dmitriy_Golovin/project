<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddAddressTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'address_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'related_item' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'item_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'address' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			['latitude' => [
				'type' => 'DECIMAL',
				'constraint' => 12,8
			]],
			['longitude' => [
				'type' => 'DECIMAL',
				'constraint' => 12,8
			]],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('address_id', true);
		$this->forge->createTable('address');
	}

	public function down()
	{
		$this->forge->dropTable('address');
	}
}
