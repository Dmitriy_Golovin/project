<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddTaskTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'task_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'source_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'owner_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null
			],
			'assigned_user_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'description' => [
				'type' => 'TEXT',
			],
			'name' => [
				'type' => 'VARCHAR',
				'constraint' => 255
			],
			'duration' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'periodicity' => [
				'type' => 'TINYINT',
				'constraint' => 3
			],
			'reminder' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
			'time' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'date' => [
				'type' => 'INT',
				'constraint' => 11
			],
			'privacy' => [
				'type' => 'TINYINT',
				'constraint' => 1
			],
			'status' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 1,
			],
			'is_cloned' => [
				'type' => 'TINYINT',
				'constraint' => 1,
			],
			'review_status' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			],
			'time_offset_seconds' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'is_viewed_assigned' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			],
			'is_viewed_owner' => [
				'type' => 'TINYINT',
				'constraint' => 1,
				'default' => 0,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('task_id', true);
		$this->forge->addForeignKey('owner_user_id', 'user', 'user_id', 'set null', 'set null');
		$this->forge->addForeignKey('assigned_user_id', 'user', 'user_id', 'set null', 'set null');
		$this->forge->addForeignKey('source_id', 'task', 'task_id', 'set null', 'set null');
		$this->forge->createTable('task');
	}

	public function down()
	{
		$this->forge->dropTable('task');
	}
}
