<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;
use App\Database\Seeds\CitySeeder;
use Config\Database;

class AddCityTranslationTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'city_translation_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'city_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'lang_code' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'title' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('city_translation_id', true);
		$this->forge->addForeignKey('city_id', 'city', 'city_id', 'cascade', 'cascade');
		$this->forge->createTable('city_translation');

		$config = new Database();
		$citySeeder = new CitySeeder($config);
		$citySeeder->run();
	}

	public function down()
	{
		$this->forge->dropTable('city_translation');
	}
}
