<?php

namespace App\Database\Migrations;

use CodeIgniter\Database\Migration;

class AddCountryTable extends Migration
{
	public function up()
	{
		$this->forge->addField([
			'country_id' => [
				'type' => 'INT',
				'constraint' => 11,
				'auto_increment' => true,
			],
			'external_country_id' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'code' => [
				'type' => 'VARCHAR',
				'constraint' => 255,
			],
			'created_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'updated_at' => [
				'type' => 'INT',
				'constraint' => 11,
			],
			'deleted_at' => [
				'type' => 'INT',
				'constraint' => 11,
				'default' => null,
			],
		]);

		$this->forge->addKey('country_id', true);
		$this->forge->createTable('country');
	}

	public function down()
	{
		$this->forge->dropTable('country');
	}
}
