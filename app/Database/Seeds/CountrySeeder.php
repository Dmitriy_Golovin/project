<?php 
namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CountrySeeder extends Seeder
{
	public function run()
	{
		$this->db = \Config\Database::connect();
		$country = $this->db->prefixTable('country');
		$countryTranslation = $this->db->prefixTable('country_translation');
		$handle = fopen('../public/documents/country.csv', 'r');
		$firstStr = false;

		while (($data = fgetcsv($handle, 1000)) !== FALSE) {
			if (!$firstStr) {
				$firstStr = true;
				continue;
			}

			$itemData = str_getcsv($data[0], ';');

			$countryData = [
				'external_country_id' => $itemData[0],
				'code' => $itemData[3],
				'created_at' => time(),
				'updated_at' => time(),
				'deleted_at' => 0,
			];

			$this->db->table($country)->insert($countryData);

			$countryTranslationData = [
				'country_id' => $this->db->insertId(),
				'lang_code' => 'ru',
				'title' => $itemData[2],
				'created_at' => time(),
				'updated_at' => time(),
				'deleted_at' => 0,
			];

			$this->db->table($countryTranslation)->insert($countryTranslationData);
		}

		fclose($handle);
	}
}