<?php

namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;
use Common\Models\UserModel;
use Common\Models\UserSettingModel;

class AdminUserSeeder extends Seeder
{
    public function run()
    {
        $this->db = \Config\Database::connect();
        $userTable = $this->db->prefixTable('user');

        $user = [
            'first_name' => 'Yago',
            'last_name' => 'Sevatarion',
            'country_id' => 1,
            'email' => config('AdminConfig')->adminEmail,
            'password' => password_hash(config('AdminConfig')->adminPassword, PASSWORD_DEFAULT),
            'accept_terms' => UserModel::ACCEPT_TERMS,
            'veryfication_token' => null,
            'is_blocked' => UserModel::STATUS_NOT_BLOCKED,
            'role' => UserModel::ROLE_ADMIN,
        ];

        $this->db->table($userTable)->insert($user);
        $userId = $this->db->insertID();

        $userSettingTable = $this->db->prefixTable('user_setting');

        $userSetting = [
            'user_id' => $userId,
            'holidays' => UserSettingModel::HOLIDAYS_ACTIVE,
            'my_events' => UserSettingModel::MY_EVENTS_ACTIVE,
            'my_meetings' => UserSettingModel::MY_MEETINGS_ACTIVE,
            'my_tasks' => UserSettingModel::MY_TASKS_ACTIVE,
            'work_week_start' => 2,
            'work_days_amount' => 5,
        ];

        $this->db->table($userSettingTable)->insert($userSetting);
    }
}
