<?php 
namespace App\Database\Seeds;

use CodeIgniter\Database\Seeder;

class CitySeeder extends Seeder
{
	public function run()
	{
		$this->db = \Config\Database::connect();
		$city = $this->db->prefixTable('city');
		$cityTranslation = $this->db->prefixTable('city_translation');
		$country = $this->db->prefixTable('country');

		$countryData = [];
		$countryQuery = $this->db->table($country)
			->select('country_id, external_country_id');

		foreach ($countryQuery->get()->getResultArray() as $row) {
			$countryData[$row['external_country_id']] = $row['country_id'];
		}

		$handle = fopen('../public/documents/city.csv', 'r');
		$firstStr = false;

		while (($data = fgetcsv($handle, 1000)) !== FALSE) {
			if (!$firstStr) {
				$firstStr = true;
				continue;
			}

			$itemData = str_getcsv($data[0], ';');

			$cityData = [
				'country_id' => $countryData[$itemData[1]],
				'external_city_id' => $itemData[0],
				'external_country_id' => $itemData[1],
				'external_region_id' => $itemData[2],
				'created_at' => time(),
				'updated_at' => time(),
				'deleted_at' => 0,
			];

			$this->db->table($city)->insert($cityData);

			$cityTranslationData = [
				'city_id' => $this->db->insertId(),
				'lang_code' => 'ru',
				'title' => $itemData[3],
				'created_at' => time(),
				'updated_at' => time(),
				'deleted_at' => 0,
			];

			$this->db->table($cityTranslation)->insert($cityTranslationData);
		}

		fclose($handle);
	}
}