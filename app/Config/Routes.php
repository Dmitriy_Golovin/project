<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('');
$routes->setDefaultController('HomeController');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(true);
$routes->set404Override('App\Controllers\Error404Controller::show404');
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'App\Controllers\HomeController::index');
$routes->match(['get', 'post'], '/login', 'App\Controllers\HomeController::login');
$routes->match(['get', 'post'], '/register', 'App\Controllers\HomeController::register');
$routes->match(['get', 'post'], '/send-restore-token', 'App\Controllers\HomeController::sendRestoreToken');
$routes->match(['get', 'post'], '/restore-password/(:segment)', 'App\Controllers\HomeController::restorePassword/$1');
$routes->get('/confirm', 'App\Controllers\HomeController::confirm');
$routes->get('/activation/(:segment)', 'App\Controllers\AuthCheck::activation/$1');
$routes->get('/confirm_change_email/(:segment)', 'App\Controllers\AuthCheck::confirm_change_email/$1');
$routes->post('/authCheck/authWS', 'App\Controllers\AuthCheck::authWS');
$routes->post('/auth/emailExists', 'App\Controllers\AuthCheck::emailExists');
$routes->get('/cabinet', 'App\Controllers\CabinetController::index');
$routes->get('/auth', 'App\Controllers\AuthCheck::checkRefreshToken');
$routes->get('/auth/strUpgrade', 'App\Controllers\AuthCheck::strUpgrade');
$routes->get('/logout', 'App\Controllers\AuthCheck::logout');
$routes->get('/cs', 'App\Controllers\HomeController::cs');
$routes->get('/^(january|february|march|april|may|june|july|august|september|october|november|december)[0-9]{1,6}', 'App\Controllers\CalendarController::index');
$routes->post('/calendar/getContentInCell', 'App\Controllers\CalendarController::getContentInCell');
$routes->post('/calendar/changeSettings', 'App\Controllers\CalendarController::changeSettings');
$routes->post('/calendar/getContentForEventWin', 'App\Controllers\CalendarController::getContentForEventWin');
$routes->post('/calendar/saveBaseEvent', 'App\Controllers\CalendarController::saveBaseEvent');
$routes->get('/holiday', 'App\Controllers\HolidayController::index');
$routes->get('/holiday/((ru))', 'App\Controllers\HolidayController::holidayCountry');
$routes->get('/holiday/((ru))/(:num)', 'App\Controllers\HolidayController::holidayCountry/$3');
$routes->get('/holiday/holiday-search/((ru))', 'App\Controllers\HolidayController::holidaySearch/$2');
$routes->get('/holiday/holiday-search/((ru))/(:segment)', 'App\Controllers\HolidayController::holidaySearch/$2/$3');
$routes->get('/messages', 'App\Controllers\MessageController::index');
$routes->get('/messages/d(:num)', 'App\Controllers\MessageController::index');
$routes->get('/messages/d/(:num)', 'App\Controllers\MessageController::dialog/$1');
$routes->get('/messages/cr/(:num)', 'App\Controllers\MessageController::chatRoom/$1');
$routes->match(['get', 'post'], '/messages/create-chat', 'App\Controllers\MessageController::createChat');
$routes->match(['get', 'post'], '/messages/edit-chat/(:num)', 'App\Controllers\MessageController::editChat/$1');
$routes->get('/messages/delete-chat/(:num)', 'App\Controllers\MessageController::deleteChat/$1');
$routes->post('/message/toggleImportantMessage', 'App\Controllers\MessageController::toggleImportantMessage');
$routes->post('/message/deleteDialogMessage', 'App\Controllers\MessageController::deleteDialogMessage');
$routes->post('/message/deleteDialogAllMesages', 'App\Controllers\MessageController::deleteDialogAllMesages');
$routes->post('/message/getCorrespondence', 'App\Controllers\MessageController::getCorrespondence');
$routes->post('/message/sendChatMessage', 'App\Controllers\MessageController::sendChatMessage');
$routes->post('/message/sendDialogMessage', 'App\Controllers\MessageController::sendDialogMessage');
$routes->post('/message/getChatMessageListItem', 'App\Controllers\MessageController::getChatMessageListItem');
$routes->post('/message/getDialogMessageListItem', 'App\Controllers\MessageController::getDialogMessageListItem');
$routes->post('/message/getChatDetails', 'App\Controllers\MessageController::getChatDetails');
$routes->post('/message/getChatMessageDetailsItem', 'App\Controllers\MessageController::getChatMessageDetailsItem');
$routes->post('/message/getDialogMessageDetailsItem', 'App\Controllers\MessageController::getDialogMessageDetailsItem');
$routes->post('/message/leaveChat', 'App\Controllers\MessageController::leaveChat');
$routes->post('/message/markChatMessageAsRead', 'App\Controllers\MessageController::markChatMessageAsRead');
$routes->post('/message/markDialogMessageAsRead', 'App\Controllers\MessageController::markDialogMessageAsRead');
$routes->post('/message/toggleImportantChatMessage', 'App\Controllers\MessageController::toggleImportantChatMessage');
$routes->post('/message/deleteChatMessage', 'App\Controllers\MessageController::deleteChatMessage');
$routes->post('/message/addChatMessageReaction', 'App\Controllers\MessageController::addChatMessageReaction');
$routes->post('/message/getDialogDetails', 'App\Controllers\MessageController::getDialogDetails');
$routes->post('/message/getChatMemberList', 'App\Controllers\MessageController::getChatMemberList');
$routes->post('/message/getChatMessageViewMemberList', 'App\Controllers\MessageController::getChatMessageViewMemberList');
$routes->post('/city/getCitiesForThisCountry', 'App\Controllers\CityController::getCitiesForThisCountry');

// get file

$routes->get('/userFile/ava/(:segment)', 'App\Controllers\UserFileController::getAvatar/$1');
$routes->get('/userFile/img/(:segment)', 'App\Controllers\UserFileController::getImage/$1');
$routes->get('/userFile/audio/(:segment)', 'App\Controllers\UserFileController::getAudio/$1');
$routes->get('/userFile/video/(:segment)', 'App\Controllers\UserFileController::getVideo/$1');
$routes->get('/userFile/document/(:segment)', 'App\Controllers\UserFileController::getDocument/$1');
$routes->get('/openFile/img/(:segment)', 'App\Controllers\OpenFileController::getImage/$1');

//routes for working with your files

$routes->post('/file/getFileListByType', 'App\Controllers\FileController::getFileListByType');
$routes->get('/file/photo', 'App\Controllers\FileController::photo');
$routes->get('/file/video', 'App\Controllers\FileController::video');
$routes->get('/file/document', 'App\Controllers\FileController::document');
$routes->post('/file/getFileList', 'App\Controllers\FileController::getFileList');
$routes->get('/file/settings/(:num)', 'App\Controllers\FileController::settings/$1');
$routes->post('/file/getFileSettingsData', 'App\Controllers\FileController::getFileSettingsData');
$routes->post('/file/getFilePermissionUserList', 'App\Controllers\FileController::getFilePermissionUserList');
$routes->post('/file/addUserFilePermission', 'App\Controllers\FileController::addUserFilePermission');
$routes->post('/file/removeUserFilePermission', 'App\Controllers\FileController::removeUserFilePermission');
$routes->get('/file/toggle-permission/(:num)', 'App\Controllers\FileController::togglePermission/$1');
$routes->post('/file/delete', 'App\Controllers\FileController::delete');

$routes->get('/search', 'App\Controllers\SearchController::index');
$routes->post('/search/searchRequest', 'App\Controllers\SearchController::searchRequest');
$routes->post('/user/editPersonalData', 'App\Controllers\UserController::editPersonalData');
$routes->post('/user/checkPassword', 'App\Controllers\UserController::checkPassword');
$routes->post('/user/createTempEmail', 'App\Controllers\UserController::createTempEmail');
$routes->post('/user/changePassword', 'App\Controllers\UserController::changePassword');
$routes->post('/user/sendFriendRequest', 'App\Controllers\UserController::sendFriendRequest');
$routes->post('/user/unsubscribe', 'App\Controllers\UserController::unsubscribe');
$routes->post('/user/deleteFriendFromList', 'App\Controllers\UserController::deleteFriendFromList');
$routes->post('/user/rejectFriendRequest', 'App\Controllers\UserController::rejectFriendRequest');
$routes->post('/user/changeAboutSelf', 'App\Controllers\UserController::changeAboutSelf');
$routes->post('/user/getFriendshipUserList', 'App\Controllers\UserController::getFriendshipUserList');
$routes->post('/user/getFriendsList', 'App\Controllers\UserController::getFriendsList');
$routes->post('/user/searchUsersWithoutFriends', 'App\Controllers\UserController::searchUsersWithoutFriends');
$routes->post('/user/loadAvatar', 'App\Controllers\UserController::loadAvatar');
// $routes->post('/user/loadFile', 'App\Controllers\UserController::loadFile');
$routes->post('/user/selectAvatar', 'App\Controllers\UserController::selectAvatar');
$routes->post('/user/list', 'App\Controllers\UserController::list');
$routes->post('/user/changeSetting', 'App\Controllers\UserController::changeSetting');
$routes->get('/user/delete/(:segment)', 'App\Controllers\UserController::delete/$1');

$routes->cli('/console-user/run', 'Console\Controllers\UserController::run');
$routes->cli('/console-event-common/run', 'Console\Controllers\EventCommonController::run');
$routes->get('/event', 'App\Controllers\EventController::index');
$routes->get('/event/scheduled', 'App\Controllers\EventController::scheduled');
$routes->get('/event/completed', 'App\Controllers\EventController::completed');
$routes->get('/event/invitations', 'App\Controllers\EventController::invitations');
$routes->get('/event/details/(:num)', 'App\Controllers\EventController::details/$1');
$routes->match(['get', 'post'], '/event/create', 'App\Controllers\EventController::create');
$routes->match(['get', 'post'], '/event/edit/(:num)', 'App\Controllers\EventController::edit/$1');
$routes->post('/commonEventList/getList', 'App\Controllers\CommonEventListController::getList');
$routes->post('/commonEventList/getMainEventData', 'App\Controllers\CommonEventListController::getMainEventData');
$routes->post('/commonEventList/getCommonEventDetailsData', 'App\Controllers\CommonEventListController::getCommonEventDetailsData');
$routes->get('/event/invitation/(:num)', 'App\Controllers\EventController::invitation/$1');
$routes->post('/commonEventList/getCommonEventInvitationDetailsData', 'App\Controllers\CommonEventListController::getCommonEventInvitationDetailsData');
$routes->post('/commonEventList/getInviteListItem', 'App\Controllers\CommonEventListController::getInviteListItem');
$routes->post('/commonEventList/entityInviteUser', 'App\Controllers\CommonEventListController::entityInviteUser');
$routes->post('/event/delete', 'App\Controllers\EventController::delete');
$routes->post('/event/acceptInvite', 'App\Controllers\EventController::acceptInvite');
$routes->post('/event/declineInvite', 'App\Controllers\EventController::declineInvite');
$routes->post('/event/setViewedInviteStatus', 'App\Controllers\EventController::setViewedInviteStatus');
$routes->match(['get', 'post'], '/event/create-report/(:num)', 'App\Controllers\EventController::createReport/$1');
$routes->match(['get', 'post'], '/event/edit-report/(:num)', 'App\Controllers\EventController::editReport/$1');
$routes->post('/commonEventList/getCommonEventSaveReportData', 'App\Controllers\CommonEventListController::getCommonEventSaveReportData');
$routes->post('/commonEventList/getCommonEventReportDetailsData', 'App\Controllers\CommonEventListController::getCommonEventReportDetailsData');
$routes->post('/commonEventList/getInviteEntityList', 'App\Controllers\CommonEventListController::getInviteEntityList');
$routes->post('/videoFile/details', 'App\Controllers\VideoFileController::details');
$routes->get('/event/report/(:num)', 'App\Controllers\EventController::reportDetails/$1');
$routes->post('/comment/getList', 'App\Controllers\CommentController::getList');
$routes->post('/comment/sendComment', 'App\Controllers\CommentController::sendComment');

//image slider routes

$routes->post('/imageFile/reportSlider', 'App\Controllers\ImageFileController::reportSlider');
$routes->post('/imageFile/taskHistorySlider', 'App\Controllers\ImageFileController::taskHistorySlider');
$routes->post('/imageFile/selfUserSlider', 'App\Controllers\ImageFileController::selfUserSlider');
$routes->post('/imageFile/userSlider', 'App\Controllers\ImageFileController::userSlider');

$routes->get('/event/delete-report/(:num)/(:num)', 'App\Controllers\EventController::deleteReport/$1/$2');
$routes->get('/meeting', 'App\Controllers\MeetingController::index');
$routes->get('/meeting/scheduled', 'App\Controllers\MeetingController::scheduled');
$routes->get('/meeting/completed', 'App\Controllers\MeetingController::completed');
$routes->get('/meeting/invitations', 'App\Controllers\MeetingController::invitations');
$routes->match(['get', 'post'], '/meeting/create', 'App\Controllers\MeetingController::create');
$routes->match(['get', 'post'], '/meeting/edit/(:num)', 'App\Controllers\MeetingController::edit/$1');
$routes->get('/meeting/details/(:num)', 'App\Controllers\MeetingController::details/$1');
$routes->get('/meeting/invitation/(:num)', 'App\Controllers\MeetingController::invitation/$1');
$routes->post('/meeting/setViewedInviteStatus', 'App\Controllers\MeetingController::setViewedInviteStatus');
$routes->post('/meeting/acceptInvite', 'App\Controllers\MeetingController::acceptInvite');
$routes->post('/meeting/declineInvite', 'App\Controllers\MeetingController::declineInvite');
$routes->post('/meeting/delete', 'App\Controllers\MeetingController::delete');
$routes->match(['get', 'post'], '/meeting/create-report/(:num)', 'App\Controllers\MeetingController::createReport/$1');
$routes->match(['get', 'post'], '/meeting/edit-report/(:num)', 'App\Controllers\MeetingController::editReport/$1');
$routes->get('/meeting/report/(:num)', 'App\Controllers\MeetingController::reportDetails/$1');
$routes->get('/meeting/delete-report/(:num)/(:num)', 'App\Controllers\MeetingController::deleteReport/$1/$2');
$routes->get('/task', 'App\Controllers\TaskController::index');
$routes->post('/task/getList', 'App\Controllers\TaskController::getList');
$routes->get('/task/scheduled', 'App\Controllers\TaskController::scheduled');
$routes->get('/task/completed', 'App\Controllers\TaskController::completed');
$routes->get('/task/overdue', 'App\Controllers\TaskController::overdue');
$routes->get('/task/created', 'App\Controllers\TaskController::created');
$routes->match(['get', 'post'], '/task/create', 'App\Controllers\TaskController::create');
$routes->match(['get', 'post'], '/task/edit/(:num)', 'App\Controllers\TaskController::edit/$1');
$routes->get('/task/assigned-details/(:num)', 'App\Controllers\TaskController::assignedDetails/$1');
$routes->get('/task/owner-details/(:num)', 'App\Controllers\TaskController::ownerDetails/$1');
$routes->post('/task/getTaskDetailsData', 'App\Controllers\TaskController::getTaskDetailsData');
$routes->get('/task/history/(:num)', 'App\Controllers\TaskController::history/$1');
$routes->post('/task/getHistoryList', 'App\Controllers\TaskController::getHistoryList');
$routes->post('/task/getTaskListItem', 'App\Controllers\TaskController::getTaskListItem');
$routes->match(['get', 'post'], '/task/complete/(:num)', 'App\Controllers\TaskController::complete/$1');
$routes->match(['get', 'post'], '/task/complete-edit/(:num)/(:num)', 'App\Controllers\TaskController::completeEdit/$1/$2');
$routes->match(['get', 'post'], '/task/accept/(:num)', 'App\Controllers\TaskController::accept/$1');
$routes->match(['get', 'post'], '/task/accept-edit/(:num)/(:num)', 'App\Controllers\TaskController::acceptEdit/$1/$2');
$routes->match(['get', 'post'], '/task/reject/(:num)', 'App\Controllers\TaskController::reject/$1');
$routes->match(['get', 'post'], '/task/reject-edit/(:num)/(:num)', 'App\Controllers\TaskController::rejectEdit/$1/$2');
$routes->post('/task/getTaskActionData', 'App\Controllers\TaskController::getTaskActionData');
$routes->post('/task/getTaskHistoryListItem', 'App\Controllers\TaskController::getTaskHistoryListItem');
$routes->post('/task/delete', 'App\Controllers\TaskController::delete');

//friend routes

$routes->get('/friends', 'App\Controllers\FriendController::index');
$routes->get('/friends-subscribers', 'App\Controllers\FriendController::subscribers');
$routes->get('/friends-subscriptions', 'App\Controllers\FriendController::subscriptions');
$routes->post('/friends/getFriendListSelf', 'App\Controllers\FriendController::getFriendListSelf');
$routes->post('/friends/getFriendList', 'App\Controllers\FriendController::getFriendList');
$routes->post('/friends/subscribe', 'App\Controllers\FriendController::subscribe');
$routes->post('/friends/unsubscribe', 'App\Controllers\FriendController::unsubscribe');
$routes->post('/friends/acceptFriendship', 'App\Controllers\FriendController::acceptFriendship');
$routes->post('/friends/rejectFriendship', 'App\Controllers\FriendController::rejectFriendship');
$routes->post('/friends/removeFriend', 'App\Controllers\FriendController::removeFriend');

//profile routes

$routes->get('/profile/(:num)', 'App\Controllers\ProfileController::index/$1');
$routes->get('/profile/(:num)/friends', 'App\Controllers\ProfileController::friends/$1');
$routes->get('/profile/(:num)/friends-subscribers', 'App\Controllers\ProfileController::subscribers/$1');
$routes->get('/profile/change_personal_data', 'App\Controllers\ProfileController::changePersonalData');
$routes->get('/profile/(:num)/photo', 'App\Controllers\ProfileController::photo/$1');
$routes->get('/profile/(:num)/video', 'App\Controllers\ProfileController::video/$1');
$routes->post('/profile/getFileList', 'App\Controllers\ProfileController::getFileList');
$routes->get('/profile/(:num)/events', 'App\Controllers\ProfileController::event/$1');
$routes->get('/profile/(:num)/events-scheduled', 'App\Controllers\ProfileController::eventScheduled/$1');
$routes->get('/profile/(:num)/meetings', 'App\Controllers\ProfileController::meeting/$1');
$routes->get('/profile/(:num)/meetings-scheduled', 'App\Controllers\ProfileController::meetingScheduled/$1');
$routes->post('/profile/getMainEventList', 'App\Controllers\ProfileController::getMainEventList');
$routes->get('/profile/(:num)/event/(:num)', 'App\Controllers\ProfileController::eventDetails/$1/$2');
$routes->get('/profile/(:num)/meeting/(:num)', 'App\Controllers\ProfileController::meetingDetails/$1/$2');
$routes->post('/profile/getMainEventDetails', 'App\Controllers\ProfileController::getMainEventDetails');
$routes->post('/profile/getMainEventParticipantsList', 'App\Controllers\ProfileController::getMainEventParticipantsList');
$routes->post('/profile/getReportDetails', 'App\Controllers\ProfileController::getReportDetails');

//unversum routes

$routes->group('/universum', ['namespace' => 'App\Controllers'], static function ($routes) {
    $routes->get('/', 'UniversumController::index');
    $routes->get('(:segment)', 'UniversumController::chapter/$1');
    $routes->post('getArticleList', 'UniversumController::getArticleList/');
    $routes->get('(:segment)/(:segment)', 'UniversumController::article/$1/$2');
    $routes->post('getArticleCommentData', 'UniversumController::getArticleCommentData/');
});

//feedback routes

$routes->match(['get', 'post'], '/feedback', 'App\Controllers\FeedbackController::index');

//agreement routes

$routes->get('/termsofuse', 'App\Controllers\AgreementController::termsOfUse');
$routes->get('/privacy', 'App\Controllers\AgreementController::privacy');

// site map routes

$routes->get('/sitemap', 'App\Controllers\SitemapController::index');

//admin routes

$routes->group('/admin', ['namespace' => 'Admin\Controllers'], static function ($routes) {
    $routes->get('/', 'HomeController::index');

    $routes->get('userFile/ava/(:segment)', 'UserFileController::getAvatar/$1');
    $routes->get('userFile/img/(:segment)', 'UserFileController::getImage/$1');
    $routes->get('userFile/video/(:segment)', 'UserFileController::getVideo/$1');
    $routes->get('userFile/document/(:segment)', 'UserFileController::getDocument/$1');

    $routes->post('city/getCityList', 'CityController::getCityList');

    $routes->get('users', 'UserController::index');
    $routes->get('users/details/(:num)', 'UserController::details/$1');
    $routes->post('users/upload-avatar/(:num)', 'UserController::uploadAvatar/$1');
    $routes->get('users/delete-avatar/(:num)/(:num)', 'UserController::deleteAvatar/$1/$2');
    $routes->get('users/block/(:num)', 'UserController::block/$1');
    $routes->get('users/unblock/(:num)', 'UserController::unblock/$1');
    $routes->get('users/delete/(:num)', 'UserController::delete/$1');
    $routes->get('users/restore/(:num)', 'UserController::restore/$1');
    $routes->match(['get', 'post'], 'users/create', 'UserController::create');
    $routes->match(['get', 'post'], 'users/edit/(:num)', 'UserController::edit/$1');

    $routes->get('holidays', 'HolidayController::index');
    $routes->get('holidays/details/(:num)', 'HolidayController::details/$1');
    $routes->match(['get', 'post'], 'holidays/create', 'HolidayController::create');
    $routes->match(['get', 'post'], 'holidays/edit/(:num)', 'HolidayController::edit/$1');
    $routes->get('holidays/delete/(:num)', 'HolidayController::delete/$1');

    $routes->get('visit-logs', 'VisitLogController::index');

    $routes->get('article-sections', 'ArticleSectionController::index');
    $routes->get('article-sections/details/(:num)', 'ArticleSectionController::details/$1');
    $routes->match(['get', 'post'], 'article-sections/create', 'ArticleSectionController::create');
    $routes->match(['get', 'post'], 'article-sections/edit/(:num)', 'ArticleSectionController::edit/$1');
    $routes->get('article-sections/delete/(:num)', 'ArticleSectionController::delete/$1');

    $routes->get('articles', 'ArticleController::index');
    $routes->get('articles/details/(:num)', 'ArticleController::details/$1');
    $routes->match(['get', 'post'], 'articles/create', 'ArticleController::create');
    $routes->match(['get', 'post'], 'articles/edit/(:num)', 'ArticleController::edit/$1');
    $routes->match(['get', 'post'], 'articles/add-paragraph/(:num)', 'ArticleController::addParagraph/$1');
    $routes->match(['get', 'post'], 'articles/add-image/(:num)', 'ArticleController::addImage/$1');
    $routes->post('articles/delete-paragraph', 'ArticleController::deleteParagraph');
    $routes->get('articles/delete/(:num)', 'ArticleController::delete/$1');
    $routes->get('articles/publish/(:num)', 'ArticleController::publish/$1');
    $routes->get('articles/unpublish/(:num)', 'ArticleController::unpublish/$1');
});

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
