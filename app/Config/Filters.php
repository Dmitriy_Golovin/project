<?php namespace Config;

use CodeIgniter\Config\BaseConfig;

class Filters extends BaseConfig
{
	// Makes reading things below nicer,
	// and simpler to change out script that's used.
	public $aliases = [
		'csrf'     => \CodeIgniter\Filters\CSRF::class,
		'toolbar'  => \CodeIgniter\Filters\DebugToolbar::class,
		'honeypot' => \CodeIgniter\Filters\Honeypot::class,
		'uricheckstart' => \App\Filters\UriCheckStart::class,
		'authorizationfilter' => \App\Filters\AuthorizationFilter::class,
		'authorizationwsfilter' => \App\Filters\AuthorizationWSFilter::class,
		'filepermissionfilter' => \App\Filters\FilePermissionFilter::class,
		'adminauthorizationfilter' => \Admin\Filters\AdminAuthorizationFilter::class,
	];

	// Always applied before every request
	public $globals = [
		'before' => [
			//'honeypot'
			'authorizationfilter' => ['except' => ['/auth', '/auth/strUpgrade', '/logout', '/cs', '/authCheck/authWS', '/console-user/run', 'console-event-common/run', '/admin', '/admin/*', '/sitemap']],
			'csrf' => ['except' => ['/authCheck/authWS']],
			//'uricheckstart'
		],
		'after'  => [
			// 'toolbar',
			//'honeypot'
		],
	];

	// Works on all of a particular HTTP method
	// (GET, POST, etc) as BEFORE filters only
	//     like: 'post' => ['CSRF', 'throttle'],
	public $methods = [];

	// List filter aliases and any before/after uri patterns
	// that they should run on, like:
	//    'isLoggedIn' => ['before' => ['account/*', 'profiles/*']],
	public $filters = [
		'uricheckstart' => ['before' => ['/', '/*']],
		'authorizationwsfilter' => ['before' =>['/authCheck/authWS']],
		'filepermissionfilter' => ['before' => ['userFile/img/*', '/userFile/audio/*', '/userFile/video/*', '/userFile/document/*']],
		'adminauthorizationfilter' => ['before' => ['/admin', '/admin/*']],
	];
}
