<?php

namespace App\Libraries\DateAndTime;

use CodeIgniter\I18n\Time;

class DateAndTime extends Time {

	const SECONDS_IN_DAY = 86400;
	
	const SUCH_MONTH = [
		1 => 'january',
		2 => 'february',
		3 => 'march',
		4 => 'april',
		5 => 'may',
		6 => 'june',
		7 => 'july',
		8 => 'august',
		9 => 'september',
		10 => 'october',
		11 => 'november',
		12 => 'december',
	];

	const DAY_OF_WEEK = [
		2 => 'monday',
		3 => 'tuesday',
		4 => 'wednesday',
		5 => 'thursday',
		6 => 'friday',
		7 => 'saturday',
		1 => 'sunday',
	];
	
	public static function getDateTime($dateTime, $timeOffsetSeconds, $case, $type = null)
	{
		$dateTime = (string)((int)$dateTime + $timeOffsetSeconds);
		$now = Time::now();
		$yesterday = self::yesterday();
		$tomorrow = self::tomorrow();
		$today = self::today();
		$dateTimeYear = self::createFromTimestamp($dateTime)->getYear();
		$nowYear = self::createFromTimestamp(time() + $timeOffsetSeconds)->getYear();
		$yesterdayStartDiff = abs($now->difference($yesterday)->getSeconds()) - $timeOffsetSeconds;
		$todayStartDiff = abs($now->difference($today)->getSeconds()) - $timeOffsetSeconds;
		$diffSeconds = abs($now->difference(self::createFromTimestamp($dateTime))->getSeconds()) - $timeOffsetSeconds;
		$result = '';
		
		if ((int)$dateTime >= $today->getTimestamp() && (int)$dateTime < $tomorrow->getTimestamp()) {
			if ($type === 'onlyDate') {
				$result = self::getLastInThisYeardateAndTime($dateTime, $case);
			} else if (is_null($type)) {
				$result = self::getTodayAndTime($dateTime);
			}
		}

		if ((int)$dateTime >= $tomorrow->getTimestamp()
			&& (int)$dateTime < $tomorrow->getTimestamp() + self::SECONDS_IN_DAY) {
			if ($type === 'onlyDate') {
				$result = self::getLastInThisYeardateAndTime($dateTime, $case);
			} else if (is_null($type)) {
				$result = self::getTommorowAndTime($dateTime);
			}
		}

		if ($diffSeconds <= $yesterdayStartDiff && $diffSeconds > $todayStartDiff
			&& (int)$dateTime < $today->getTimestamp()) {
			if ($type === 'onlyDate') {
				$result = self::getLastInThisYeardateAndTime($dateTime, $case);
			} else if (is_null($type)) {
				$result = self::getYesterdayAndTime($dateTime);
			}
		}

		if (((int)$dateTime < $yesterday->getTimestamp() || (int)$dateTime >= $tomorrow->getTimestamp() + self::SECONDS_IN_DAY) && $diffSeconds < YEAR) {
			$result = self::getLastInThisYeardateAndTime($dateTime, $case);
		}

		if ((int)$dateTimeYear !== (int)$nowYear) {
			$result = self::getLastBeforeThisYeardateAndTime($dateTime, $case);
		}

		return $result;
	}

	public static function getFullDate($timestamp, $timeOffsetSeconds = null)
	{
		if (is_string($timestamp)) {
			$timestamp = (int)$timestamp;
		}

		if (is_object($timestamp)) {
			$timestamp = (int)$timestamp->getTimestamp();
		}

		$timestamp = (!empty($timeOffsetSeconds))
			? $timestamp + (int)$timeOffsetSeconds
			: $timestamp;

		$date = self::createFromTimestamp($timestamp);
		$day = $date->getDay();
		$month = $date->getMonth();
		$day = (strlen((string)$day) < 2)
			? '0' . $day
			: $day;
		$month = (strlen((string)$month) < 2)
			? '0' . $month
			: $month;

		return $day . '-' . $month . '-' . $date->getYear();
	}

	public static function getFullDateTime($timestamp, $timeOffsetSeconds = null)
	{
		if (is_string($timestamp)) {
			$timestamp = (int)$timestamp;
		}

		if (is_object($timestamp)) {
			$timestamp = (int)$timestamp->getTimestamp();
		}

		$timestamp = (!empty($timeOffsetSeconds))
			? $timestamp + (int)$timeOffsetSeconds
			: $timestamp;

		$date = self::createFromTimestamp($timestamp);
		$day = $date->getDay();
		$month = $date->getMonth();
		$hour = $date->getHour();
		$minute = $date->getMinute();
		$second = $date->getSecond();
		$day = (strlen((string)$day) < 2)
			? '0' . $day
			: $day;
		$month = (strlen((string)$month) < 2)
			? '0' . $month
			: $month;
		$hour = (strlen((string)$hour) < 2)
			? '0' . $hour
			: $hour;
		$minute = (strlen((string)$minute) < 2)
			? '0' . $minute
			: $minute;
		$second = (strlen((string)$second) < 2)
			? '0' . $second
			: $second;

		return $day . '-' . $month . '-' . $date->getYear() . ' ' . $hour . ':' . $minute . ':' . $second;
	}

	public static function getYesterdayAndTime($dateTime)
	{
		$date = self::createFromTimestamp($dateTime);
		return [
			'date' => lang('TimeAndDate.yesterday'),
			'time' => $date->toLocalizedString('H:mm')
		];
	}

	public static function getTommorowAndTime($dateTime)
	{
		$date = self::createFromTimestamp($dateTime);
		return [
			'date' => lang('TimeAndDate.tomorrow'),
			'time' => $date->toLocalizedString('H:mm')
		];
	}

	public static function getTodayAndTime($dateTime)
	{
		$date = self::createFromTimestamp($dateTime);
		return [
			'date' => lang('TimeAndDate.today'),
			'time' => $date->toLocalizedString('H:mm')
		];
	}

	public static function getLastInThisYeardateAndTime($dateTime, $case)
	{
		$date = self::createFromTimestamp($dateTime);
		return [
			'date' => $date->getDay() . ' ' . lang('TimeAndDate.' . $case . 'Month.' . self::SUCH_MONTH[$date->getMonth()]),
			'time' => $date->toLocalizedString('H:mm'),
			'dayOfWeek' => lang('TimeAndDate.' . self::DAY_OF_WEEK[$date->getDayOfWeek()]),
		];
	}

	public static function getLastBeforeThisYeardateAndTime($dateTime, $case)
	{
		$date = self::createFromTimestamp($dateTime);
		return [
			'date' => $date->getDay() . ' ' . lang('TimeAndDate.' . $case . 'Month.' . self::SUCH_MONTH[$date->getMonth()]) . ' ' . $date->getYear(),
			'time' => $date->toLocalizedString('H:mm'),
			'dayOfWeek' => lang('TimeAndDate.' . self::DAY_OF_WEEK[$date->getDayOfWeek()]),
		];
	}

    public static function getHours($dateTime): string
    {
    	$date = self::createFromTimestamp($dateTime);
        return $date->toLocalizedString('HH');
    }

    public static function getMinutes($dateTime): string
    {
    	$date = self::createFromTimestamp($dateTime);
        return $date->toLocalizedString('mm');
    }

    public static function getTimestampStartDay($timestamp)
    {	
    	return $timestamp - ($timestamp % self::SECONDS_IN_DAY);
    }
}