<?php namespace App\Libraries\AuthJWT;

use App\Libraries\AuthJWT\AuthModel;

class AuthController {
	
	public function activate($request, $userID) {
		$config = config('App');
		$access = $this->createAccessToken($request, $userID);
		$refresh = $this->createRefreshToken($access);
		
		if (!empty($access) && !empty($refresh)) {
			return redirect()->to('/cabinet')->setCookie(
				'sta',
				$access['access'],
				7 * MONTH - 30,
				$config->cookieDomain,
				'/',
				$config->cookiePrefix,
				$config->cookieSecure,
				$config->cookieHTTPOnly,
				'lax'
			)->setCookie(
				'str',
				$refresh,
				7 * MONTH - 30,
				$config->cookieDomain,
				'/auth',
				$config->cookiePrefix,
				$config->cookieSecure,
				true,
				'lax'
			);
		}
	}
	
	public function activateRegister($request, $response, $userID) {
		$config = config('App');
		$access = $this->createAccessToken($request, $userID);
		$refresh = $this->createRefreshToken($access);
		
		$response->setCookie(
			'sta',
			$access['access'],
			7 * MONTH - 30,
			$config->cookieDomain,
			'/',
			$config->cookiePrefix,
			$config->cookieSecure,
			$config->cookieHTTPOnly,
			'lax'
		)->setCookie(
			'str',
			$refresh,
			7 * MONTH - 30,
			$config->cookieDomain,
			'/auth',
			$config->cookiePrefix,
			$config->cookieSecure,
			true,
			'lax'
		);
	}
	
	protected function createAccessToken($request, $userID) {
		helper('date');
		helper('text');
		
		$expTime = HOUR + now();
		$jwtID = random_string('alnum', 10);
		$secret = random_string('alnum', 8);
		
		$header = json_encode([
			'alg' => 'HS256',
			'typ' => 'JWT'
		]);
		
		$payload = json_encode([
			'userID' => $userID,
			'exp' => $expTime,
			'jti' => $jwtID,
		]);

		$data = base64_encode($header) . '.' . base64_encode($payload);
		$signature = hash_hmac('sha256', $data, $secret);

		$dataDB = [
			'user_id' => $userID,
			'access_id' => $jwtID,
			'secret' => $secret,
			'user_agent' => $request->getUserAgent(),
			'ip' => $request->getIPAddress(),
		];
		
		$db = db_connect();
		$authModel = new AuthModel($db);
		$authModel->removeAnalogToken($userID, $request->getUserAgent(), $request->getIPAddress());
		$countTokens = $authModel->getCountTokens($userID);
		
		if ($countTokens >= 3) {
			$authModel->removeAllUserTokens($userID);
		}
		
		$resultDB = $authModel->saveToken($dataDB, 'access_token');
		
		if ($resultDB) {
			return ['access_token_id' => $resultDB, 'userID' => $userID, 'access' => $data . '.' . $signature];
		}
	}
	
	protected function createRefreshToken($access) {
		$data = bin2hex(openssl_random_pseudo_bytes(5, $cstrong));
		$secret = bin2hex(openssl_random_pseudo_bytes(5, $cstrong));
		$refresh = hash_hmac('sha256', $data, $secret) . substr($access['access'], -7);
		
		$dataDB = [
			'user_id' => $access['userID'],
			'expire' => MONTH + now(),
			'access_token_id' => $access['access_token_id'],
			'refresh_token' => $refresh
		];
		
		$db = db_connect();
		$authModel = new AuthModel($db);
		$resultDB = $authModel->saveToken($dataDB, 'refresh_token');
		
		if ($resultDB) return $refresh;
	}
	
	public function authorization($request, $uri) {
		$config = config('App');
		$access = $request->getCookie('sta');

		if (empty($access)) {
			return 'guest';
		}

		$resultCheck = $this->checkAccess($access, $request, 'browser');

		if (!$resultCheck) {
			return redirect()->to('/auth')->setCookie(
				'uprtk',
				$uri,
				2 * MINUTE,
				$config->cookieDomain,
				'/auth',
				$config->cookiePrefix,
				$config->cookieSecure,
				$config->cookieHTTPOnly,
				'lax'
			);
		} else {
			return 'user';
		}
	}
	
	public function authorizationWS($request, $access) {
		return $this->checkAccess($access, $request, 'ws');
	}
	
	public function upgradeTokens($request, $uri = null) {
		$refresh = $request->getCookie('str');
		$access = $request->getCookie('sta');
		
		if (empty($refresh) || empty($access)) {
			if ($uri === 'X-DATA-STR') {
				$this->destroyTokensAjax();
				return 'logout';
			}

			return $this->destroyTokens();
		}
		
		$accessDecode = $this->decodeAccess($access);

		if (!$accessDecode) {
			if ($uri === 'X-DATA-STR') {
				$this->destroyTokensAjax();
				return 'logout';
			}

			return $this->destroyTokens();
		}
		
		helper('cookie');
		$db = db_connect();
		$authModel = new AuthModel($db);
		$tokenInfoDB = $authModel->getTokensFromDB($accessDecode['payload']['jti']);

		if (empty($tokenInfoDB)) {
			if ($uri === 'X-DATA-STR') {
				$this->destroyTokensAjax();
				return 'logout';
			}

			return $this->destroyTokens();
		}
		
		if (!empty($tokenInfoDB) && $tokenInfoDB['refresh_token'] == $refresh && $this->checkExpired($tokenInfoDB['expire'])) {
			$authModel->deleteTokensFromDB($accessDecode['payload']['jti']);
			$config = config('App');
			$access = $this->createAccessToken($request, $accessDecode['payload']['userID']);
			$refresh = $this->createRefreshToken($access);
			
			if (!empty($access) && !empty($refresh)) {
				if ($uri != 'X-DATA-STR') {
					return redirect()->to('/' . $uri)->setCookie(
						'sta',
						$access['access'],
						7 * MONTH - 30,
						$config->cookieDomain,
						'/',
						$config->cookiePrefix,
						$config->cookieSecure,
						$config->cookieHTTPOnly,
						'lax'
					)->setCookie(
						'str',
						$refresh,
						7 * MONTH - 30,
						$config->cookieDomain,
						'/auth',
						$config->cookiePrefix,
						$config->cookieSecure,
						true,
						'lax'
					);
				} else {
					set_cookie(
						'sta',
						$access['access'],
						7 * MONTH - 30,
						$config->cookieDomain,
						'/',
						$config->cookiePrefix,
						$config->cookieSecure,
						$config->cookieHTTPOnly,
						'lax'
					);
					set_cookie(
						'str',
						$refresh,
						7 * MONTH - 30,
						$config->cookieDomain,
						'/auth',
						$config->cookiePrefix,
						$config->cookieSecure,
						true,
						'lax'
					);
					return 'good';
				}
			}
		} else if (!$this->checkExpired($tokenInfoDB['expire'])) {
			$authModel->deleteTokensFromDB($accessDecode['payload']['jti']);
			if ($uri != 'X-DATA-STR') {
				log_message('debug', 'refresh token {userID} - expired', ['userID' => $accessDecode['payload']['userID']]);
				return $this->destroyTokens();
			} else {
				log_message('debug', 'refresh token {userID} - expired', ['userID' => $accessDecode['payload']['userID']]);
				$this->destroyTokensAjax();
				return 'logout';
			}
		} else {
			$authModel->deleteTokensFromDB($accessDecode['payload']['jti']);
			if ($uri != 'X-DATA-STR') {
				log_message('debug', 'authorization by refresh {userID} - failed', ['userID' => $accessDecode['payload']['userID']]);
				return $this->destroyTokens();
			} else {
				log_message('debug', 'authorization by refresh {userID} - failed', ['userID' => $accessDecode['payload']['userID']]);
				$this->destroyTokensAjax();
				return 'logout';
			}
		}

	}
	
	public function destroyTokens() {
		$config = config('App');
		return redirect('/')->deleteCookie('sta')->deleteCookie('str');
	}
	
	public function destroyTokensAjax() {
		$config = config('App');
		helper('cookie');
		delete_cookie('sta');
		delete_cookie('str');
	}
	
	public function decodeAccess($access) {
		$result = [];
		$access = explode('.', $access);

		if (count($access) < 3) {
			return false;
		}

		$access[0] = base64_decode($access[0]);
		$access[0] = json_decode($access[0]);
		$access[1] = base64_decode($access[1]);
		$access[1] = json_decode($access[1]);

		if (!isset($access[0]->alg)) {
			return false;
		}

		if (!isset($access[0]->typ)) {
			return false;
		}

		if (!isset($access[1]->userID)) {
			return false;
		}

		if (!isset($access[1]->exp)) {
			return false;
		}

		if (!isset($access[1]->jti)) {
			return false;
		}

		$result['header']['alg'] = $access[0]->alg;
		$result['header']['typ'] = $access[0]->typ;
		$result['payload']['userID'] = $access[1]->userID;
		$result['payload']['exp'] = $access[1]->exp;
		$result['payload']['jti'] = $access[1]->jti;
		$result['signature'] = $access[2];
		return $result;
	}
	
	protected function checkExpired($exp) {
		helper('date');
		if (now() > $exp) return false;
		else return true;
	}
	
	protected function checkAccess($access, $request, $client) {
		helper('date');
		$resultArr = [];

		if ($client === 'browser') {
			$resultArr = ['signature' => false, 'ip' => false, 'userAgent' => false, 'isLive' => false];
			$ip = $request->getIPAddress();
		} else if ($client === 'ws') {
			$resultArr = ['signature' => false, 'userAgent' => false, 'isLive' => false];
		}

		$accessDecode = $this->decodeAccess($access);

		if (!$accessDecode) {
			return false;
		}

		$userAgent = $request->getUserAgent()->getAgentString();
		
		$db = db_connect();
		$authModel = new AuthModel($db);
		$acTokenInfoDB = $authModel->getAccessTokenInfo($accessDecode['payload']['userID'], $accessDecode['payload']['jti']);

		if (empty($acTokenInfoDB)) {
			return false;
		}
		
		$data = explode('.', $access);
		$signature = hash_hmac('sha256', $data[0] . '.' . $data[1], $acTokenInfoDB['secret']);
		
		if ($signature === $accessDecode['signature']) $resultArr['signature'] = true;
		
		if ($client === 'browser') {
			if ($ip === $acTokenInfoDB['ip']) $resultArr['ip'] = true;
		}
		
		if ($userAgent === $acTokenInfoDB['user_agent']) $resultArr['userAgent'] = true;
		if (now() < $accessDecode['payload']['exp']) $resultArr['isLive'] = true;

		foreach ($resultArr as $item) {
			if ($item == false) {
				log_message('debug', 'check access token {userID}, wrong {item}', ['userID' => $accessDecode['payload']['userID'], 'item' => $item]);
				return false;
				break;
			}
		}
		
		return true;
	}
	
	public function getUserIDByAccessToken($request) {
		$access = $request->getCookie('sta');
		$accessDecode = $this->decodeAccess($access);
		return $accessDecode['payload']['userID'];
	}
	
	public function qqq($access) {
		$result = [];
		$access = explode('.', $access);
		$access[0] = base64_decode($access[0]);
		$access[0] = json_decode($access[0]);
		$access[1] = base64_decode(substr($access[1], 0, -6));
		$access[1] = json_decode($access[1]);
		//$result['header']['alg'] = $access[0]->alg;
		//$result['header']['typ'] = $access[0]->typ;
		//$result['payload']['userID'] = $access[1]->userID;
		//$result['payload']['exp'] = $access[1]->exp;
		//$result['payload']['jti'] = $access[1]->jti;
		//$result['signature'] = $access[2];
		return $access[1];
	}

}