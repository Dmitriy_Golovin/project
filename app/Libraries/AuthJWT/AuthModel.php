<?php namespace App\Libraries\AuthJWT;

use CodeIgniter\Database\ConnectionInterface;
use Common\Models\UserModel;

class AuthModel {
	
	protected $db;
	
	public function __construct(ConnectionInterface &$db) {
		$this->db =& $db;
	}
	
	public function removeAnalogToken($userID, $userAgent, $userIPAddress) {
		$this->db->table($this->db->prefixTable('access_token'))
			->delete(['user_id' => $userID, 'user_agent' => $userAgent, 'ip' => $userIPAddress]);
					
		return 	$this->db->affectedRows();
	}
	
	public function getCountTokens($userID) {
		return $this->db->table($this->db->prefixTable('access_token'))
			->where('user_id', $userID)
			->countAllResults();
	}
	
	public function removeAllUserTokens($userID) {
		$this->db->table($this->db->prefixTable('access_token'))
			->delete(['user_id' => $userID]);
					
		return 	$this->db->affectedRows();
	}
	
	public function saveToken($dataDB, $table) {
		$this->db->table($this->db->prefixTable($table))
					->insert($dataDB);
					
		if (!empty($this->db->affectedRows())) {
			return $this->db->insertId();
		}
	}
	
	public function getTokensFromDB($accessID) {
		$resultDB = $this->db->table($this->db->prefixTable('access_token'))
			->select('*')
			->join($this->db->prefixTable('refresh_token'), $this->db->prefixTable('access_token') . '.access_token_id = ' . $this->db->prefixTable('refresh_token') . '.access_token_id')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . $this->db->prefixTable('access_token') . '.user_id')
			->where($this->db->prefixTable('access_token') . '.access_id', $accessID)
			->where('userTable.deleted_at', null)
			->where('userTable.is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->where('userTable.veryfication_token', null)
			->get()->getRowArray();

		return $resultDB;
	}
	
	public function getAccessTokenInfo($userID, $jti) {
		$result = $this->db->table($this->db->prefixTable('access_token'))
			->select($this->db->prefixTable('access_token') . '.*')
			->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . $this->db->prefixTable('access_token') . '.user_id')
			->where($this->db->prefixTable('access_token') . '.user_id', $userID)
			->where('access_id', $jti)
			->where('userTable.deleted_at', null)
			->where('userTable.is_blocked', UserModel::STATUS_NOT_BLOCKED)
			->where('userTable.veryfication_token', null)
			->get()->getResultArray();
						
		if (!empty($result)) return $result[0];
		else return false;
	}
	
	public function deleteTokensFromDB($accessID) {
		$this->db->table($this->db->prefixTable('access_token'))
					->delete(['access_id' => $accessID]);
					
		return 	$this->db->affectedRows();
	}
}