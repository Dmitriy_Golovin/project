<?php

return [
	'create a chat' => 'создать чат',
	'Messages' => 'Сообщения',
	'mark as important' => 'отметить важным',
	'remove from important' => 'убрать из важных',
	'attachments' => 'вложения',
	'create chat' => 'создать чат',
	'edit chat' => 'редактировать чат',
	'Add users' => 'Добавить пользователей',
	'Chat name' => 'Название чата',
	'Messages' => 'Сообщения',
	'Create chat' => 'Создать чат',
	'Edit chat' => 'Редактировать чат',
	'Failed to save message file' => 'Не удалось сохранить файл сообщения',
	'Failed to save message' => 'Не удалось сохранить сообщение',
	'message already deleted' => 'сообщение уже удалено',
	'No messages' => 'Сообщений нет',
];