<?php

return [
	'Chat not found' => 'Чат не найден',
	'Failed to save chat' => 'Не удалось сохранить чат',
	'created a chat' => 'создал(а) чат',
	'The {0} parameter is required' => 'Параметр {0} обязателен для заполнения',
	'Members who viewed the message' => 'Участники просмотревшие сообщение',
	'viewed' => 'просмотрено',
];