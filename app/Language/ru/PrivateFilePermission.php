<?php

return [
	'Failed to save permissions to view the file' => 'Не удалось сохранить права на просмотр файла',
	'cancel' => 'отменить',
	'add users' => 'добавить пользователей',
	'The user already has access to the file' => 'У пользователя уже есть доступ к файлу',
	'The user does not have access to the file' => 'У пользователя нет доступа к файлу',
	'Failed to remove file permissions' => 'Не удалось удалить права доступа к файлу',
];