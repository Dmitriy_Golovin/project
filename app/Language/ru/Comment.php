<?php

return [
	'Failed to save comment' => 'Не удалось сохранить комментарий',
	'Comment not found' => 'Комментарий не найден',
];