<?php

// override core en language system validation or define your own en language validation message
return [
	'The "name" field is required' => 'Поле "название" обязательно для заполнения',
	'The "report_id" field is required' => 'Поле "report_id" обязательно для заполнения',
	'The "task_history_id" field is required' => 'Поле "task_history_id" обязательно для заполнения',
	'The "country_id" field is required' => 'Поле "country_id" обязательно для заполнения',
	'The "city_id" field is required' => 'Поле "city_id" обязательно для заполнения',
	'Invalid file extension' => 'Не допустимое расширение файла',
	'Invalid file mime type' => 'Недопустимый mime тип файла',
	'Maximum number of characters 255' => 'Максимальное количество символов 255',
	'The {0} parameter is required' => 'Параметр {0} обязателен',
	'You do not have permission to do this' => 'У вас нет прав на это действие',
	'Terms must be accepted' => 'Необходимо принять условия',
];