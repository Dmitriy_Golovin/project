<?php

return [
	'role' => 'роль',
	'creater' => 'создатель',
	'admin' => 'админ',
	'member' => 'участник',
	'members' => 'участники',
	'Member role not valid' => 'Роль участника недействительна',
	'Failed to save chat member' => 'Не удалось сохранить участника чата',
	'The user is already a member of this chat' => 'Пользователь уже является участником данного чата',
	'You are not a member of this chat' => 'Вы не являйтесь участником данного чата',
	'chat members' => 'участники чата',
];