<?php

return [
	'Limit on the number of files to upload at one time - {0}' => 'Лимит на количество файлов для загрузки за один раз - {0}',
	'Image file not found' => 'Файл изображения не найден',
	'File not found' => 'Файл не найден',
	'File access denied' => 'Доступ к файлу закрыт',
	'Failed to delete file' => 'Не удалось удалить Файл',
	'Failed to save file' =>'Не удалось сохранить Файл',
	'File deleted' => 'Файл удален',
	'File does not exist, it may have been deleted' => 'Файл не существует, возможно он был удален',
	'private' => 'приватный',
	'public' => 'публичный',
	'make private' => 'сделать приватным',
	'make public' => 'сделать публичным',
	'file access' => 'доступ к файлу',
	'File not found' => 'Файл не найден',
];