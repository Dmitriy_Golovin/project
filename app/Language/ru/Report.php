<?php

return [
	'Create event report' => 'Создать отчет о событии',
	'create event report' => 'создать отчет о событии',
	'Create meeting report' => 'Создать отчет о встрече',
	'create meeting report' => 'создать отчет о встрече',
	'Edit event report' => 'Редактировать отчет о событии',
	'edit event report' => 'редактировать отчет о событии',
	'Edit meeting report' => 'Редактировать отчет о встрече',
	'edit meeting report' => 'редактировать отчет о встрече',
	'add notes to the report' => 'добавить примечания к отчету',
	'Unable to save report with blank data' => 'Невозможно сохранить отчет с незаполненными данными',
	'Failed to save report' => 'Не удалось сохранить отчет',
	'Failed to save report file' => 'Не удалось сохранить файл отчета',
	'Event report' => 'Очтет о событии',
	'event report' => 'очтет о событии',
	'Meeting report' => 'Очтет о встрече',
	'meeting report' => 'очтет о встрече',
	'Report not found' => 'Отчет не найден',
	'Are you sure you want to delete the report?' => 'Вы действительно хотите удалить отчет?',
	'Report already exists' => 'Отчет уже существует',
	'No report' => 'Отчет отсутсвует',
];