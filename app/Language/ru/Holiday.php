<?php

return [
	'Russian holidays' => 'Праздники России',
	'Holiday search' => 'Поиск праздников',
	'Holidays' => 'Праздники',
	'Holiday with id {0} not found' => 'Праздник с id {0} не найден',
	'Failed to save holiday' => 'Не удалось сохранить праздник',
	'Failed to delete holiday' => 'Не удалось удалить праздник',
	'Failed to save holiday date' => 'Не удалось сохранить дату праздника',
];