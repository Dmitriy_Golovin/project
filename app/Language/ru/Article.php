<?php

return [
	'draft' => 'черновик',
	'not published' => 'не опубликовано',
	'published' => 'опубликовано',
	'Failed to save article' => 'Не удалось сохранить статью',
	'Article with id {0} not found' => 'Статья с id {0} не найдена',
	'Failed to delete article' => 'Не удалось удалить статью',
];