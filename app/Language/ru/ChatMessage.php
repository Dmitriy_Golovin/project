<?php

return [
	'Failed to save chat message' => 'Не удалось сохранить сообщение чата',
	'Message not found' => 'Сообщение не найдено',
	'Failed to save reaction to message' => 'Не удалось сохранить реакцию на сообщение',
];