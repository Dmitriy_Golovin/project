<?php

return [
	'Failed to save article section' => 'Не удалось сохранить раздел для статей',
	'Article section with id {0} not found' => 'Раздел статьи с id {0} не найден',
	'Failed to delete article section' => 'Не удалось удалить раздел статьи',
];