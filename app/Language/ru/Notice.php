<?php

return [
	'The message is marked "important". Are you sure you want to delete the message?' => 'Сообщение имеет пометку "важное". Вы действительно хотите удалить сообщение?',
	'Some messages are marked "important". Are you sure you want to delete all the correspondence?' => 'Некоторые сообщения имеют пометку "важное". Вы действительно хотите удалить всю переписку?',
	'Are you sure you want to delete all the correspondence?' => 'Вы уверены, что хотите удалить всю переписку?',
	'Some messages are marked "important". Do you really want to leave the chat?' => 'Некоторые сообщения имеют пометку "важное". Вы действительно хотите покинуть чат?',
	'You are the only administrator in the chat. Assign one of the chat members as an administrator so that you can leave the chat' => 'Вы единственный администратор в чате. Назначьте одного из участников чата администратором, чтобы вы могли покинуть чат.',
];