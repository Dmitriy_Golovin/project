<?php

return [
	'Create event report' => 'Create event report',
	'Edit event report' => 'Edit event report',
	'create event report' => 'create event report',
	'edit event report' => 'edit event report',
	'Create meeting report' => 'Create meeting report',
	'create meeting report' => 'create meeting reportе',
	'Edit meeting report' => 'Edit meeting report',
	'edit meeting report' => 'edit meeting reportе',
	'add notes to the report' => 'add notes to the report',
	'Unable to save report with blank data' => 'Unable to save report with blank data',
	'Failed to save report' => 'Failed to save report',
	'Failed to save report file' => 'Failed to save report file',
	'Event report' => 'Event report',
	'event report' => 'event report',
	'Meeting report' => 'Meeting report',
	'meeting report' => 'meeting report',
	'Report not found' => 'Report not found',
	'Are you sure you want to delete the report?' => 'Are you sure you want to delete the report?',
	'Report already exists' => 'Report already exists',
	'No report' => 'No report',
];