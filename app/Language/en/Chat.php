<?php

return [
	'Chat not found' => 'Chat not found',
	'Failed to save chat' => 'Failed to save chat',
	'created a chat' => 'created a chat',
	'The {0} parameter is required' => 'The {0} parameter is required',
	'Members who viewed the message' => 'Members who viewed the message',
	'viewed' => 'viewed',
];