<?php

return [
	'invite users' => 'invite users',
	'show invited' => 'show invited',
	'show users' => 'show users',
	'invited users' => 'invited users',
	'Events' => 'Events',
	'Meetings' => 'Meetings',
	'Tasks' => 'Tasks',
	'event' => 'event',
	'Event' => 'Event',
	'meeting' => 'meeting',
	'task' => 'task',
	'invitations' => 'invitations',
	'Events of the current day' => 'Events of the current day',
	'Scheduled events' => 'Scheduled events',
	'Completed events' => 'Completed events',
	'Invitations' => 'Invitations',
	'events of the current day' => 'events of the current day',
	'scheduled events' => 'scheduled events',
	'completed events' => 'completed events',
	'any' => 'any',
	'public' => 'public',
	'private' => 'private',
	'privacy type' => 'privacy type',
	'You don\'t have current events' => 'You don\'t have current events',
	'You have no scheduled events' => 'You have no scheduled events',
	'You have no completed events' => 'You have no completed events',
	'start' => 'start',
	'end' => 'end',
	'repeat' => 'repeat',
	'without repeat' => 'without repeat',
	'reminder' => 'reminder',
	'without reminder' => 'without reminder',
	'participants' => 'participants',
	'guests' => 'guests',
	'invitation from' => 'invitation from',
	'accept' => 'accept',
	'decline' => 'decline',
	'status' => 'status',
	'invitation status' => 'invitation status',
	'not specified' => 'not specified',
	'location' => 'location',
	'show report' => 'show report',
	'add report' => 'add report',
	'current' => 'current',
	'scheduled' => 'scheduled',
	'completed' => 'completed',
	'create event' => 'create event',
	'edit event' => 'edit event',
	'Create event' => 'Create event',
	'Edit event' => 'Edit event',
	'duration' => 'duration',
	'location' => 'location',
	'enter location' => 'enter location',
	'remind in' => 'remind in',
	'date and time' => 'date and time',
	'event invitation' => 'event invitation',
	'Event invitation' => 'Event invitation',
	'event status' => 'event status',
	'Are you sure you want to delete the event?' => 'Are you sure you want to delete the event?',
	'current and scheduled' => 'current and scheduled',
	'Invitation not found' => 'Invitation not found',
	'back to event' => 'back to event',

	'privatePlural' => [
		'any' => 'any',
		'public' => 'public',
		'private' => 'private',
	],

	'statusDatePlural' => [
		'current' => 'current',
		'scheduled' => 'scheduled',
		'completed' => 'completed',
		'invitations' => 'invitations',
	],
];