<?php

// override core en language system validation or define your own en language validation message
return [
	'The "name" field is required' => 'The "name" field is required',
	'The "report_id" field is required' => 'The "report_id" field is required',
	'The "task_history_id" field is required' => 'The "task_history_id" field is required',
	'The "country_id" field is required' => 'The "country_id" field is required',
	'The "city_id" field is required' => 'The "city_id" field is required',
	'Invalid file extension' => 'Invalid file extension',
	'Invalid file mime type' => 'Invalid file mime type',
	'Maximum number of characters 255' => 'Maximum number of characters 255',
	'The {0} parameter is required' => 'The {0} parameter is required',
	'You do not have permission to do this' => 'You do not have permission to do this',
	'Terms must be accepted' => 'Terms must be accepted',
];
