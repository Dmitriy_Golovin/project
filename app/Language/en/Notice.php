<?php

return [
	'The message is marked "important". Are you sure you want to delete the message?' => 'The message is marked "important". Are you sure you want to delete the message?',
	'Some messages are marked "important". Are you sure you want to delete all the correspondence?' => 'Some messages are marked "important". Are you sure you want to delete all the correspondence?',
	'Are you sure you want to delete all the correspondence?' => 'Are you sure you want to delete all the correspondence?',
	'Some messages are marked "important". Do you really want to leave the chat?' => 'Some messages are marked "important". Do you really want to leave the chat?',
	'You are the only administrator in the chat. Assign one of the chat members as an administrator so that you can leave the chat' => 'You are the only administrator in the chat. Assign one of the chat members as an administrator so that you can leave the chat',
];