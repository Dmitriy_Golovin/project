<?php

return [
	'role' => 'role',
	'creater' => 'creater',
	'admin' => 'admin',
	'member' => 'member',
	'members' => 'members',
	'Member role not valid' => 'Member role not valid',
	'Failed to save chat member' => 'Failed to save chat member',
	'The user is already a member of this chat' => 'The user is already a member of this chat',
	'You are not a member of this chat' => 'You are not a member of this chat',
	'chat members' => 'chat members',
];