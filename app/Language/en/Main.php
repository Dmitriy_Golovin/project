<?php

return [
	'Login' => 'Login',
	'login' => 'login',
	'registration' => 'registration',

	'loginFuture' => [
		'Login' => 'Login',
	],

	'registerFutureAction' => [
		'register' => 'register'
	],

	'or' => 'or',
	'register' => 'register',
	'sign in' => 'sign in',
	'Password' => 'Password',
	'password' => 'password',
	'Forgot password?' => 'Forgot password?',
	'Login to your account' => 'Login to your account',
	'enter email' => 'enter email',
	'enter password' => 'enter password',
	'Registering a new account' => 'Registering a new account',
	'Already registered?' => 'Already registered?',
	'Email' => 'Электронная почта',
	'Repeat password' => 'Repeat password',
	'Name' => 'Name',
	'Surname' => 'Surname',
	'continue' => 'continue',
	'We have sent you an email, please check your inbox.' => 'We have sent you an email, please check your inbox.',
	'submit' => 'submit',
	'enter a new password' => 'enter a new password',
	'repeat password' => 'repeat password',
	'Registration completed successfully.' => 'Registration completed successfully.',
	'Terms of use' => 'Terms of use',
	'Privacy policy' => 'Privacy policy',
	'I accept the terms of' => 'I accept the terms of',
	'Feedback' => 'Feedback',
	'feedback' => 'feedback',
	'message text' => 'message text',
	'enter your message text' => 'enter your message text',
	'send' => 'send',
	'Message successfully sent' => 'Message successfully sent',
	'Failed to send message' => 'Failed to send message',
	'delete account' => 'delete account',
	'Confirm your account deletion' => 'Confirm your account deletion',
	'Failed to delete account' => 'Failed to delete account',
	'Your account has been successfully deleted' => 'Your account has been successfully deleted',
	'Successful account deletion' => 'Successful account deletion',
	'Profile blocked' => 'Profile blocked',
	'Profile deleted' => 'Profile deleted',
	'Profile' => 'Profile',
	'deleted' => 'deleted',
	'Added' => 'Added',
	'added' => 'added',
	'yes' => 'yes',
	'no' => 'no',
	'No' => 'No',
	'Nothing found' => 'Nothing found',
	'Authorization error' => 'Authorization error',
	'You are not authorized' => 'You are not authorized',
	'Message' => 'Message',
	'Invite' => 'Invite',
	'invite' => 'invite',
	'Add as friend' => 'Add as friend',
	'Unsubscribe' => 'Unsubscribe',
	'Remove from friends' => 'Remove from friends',
	'The material is taken from the site' => 'The material is taken from the site',
	'Email already registered' => 'Email already registered',
	'Invalid password' => 'Invalid password',
	'Confirm previous email change' => 'Confirm previous email change',
	'New password saved' => 'New password saved',
	'Change Password' => 'Change Password',
	'User with id {0} not found' => 'User with id {0} not found',
	'Failed to change the description about yourself' => 'Failed to change the description about yourself',
	'Failed to send email' => 'Failed to send email',
	'Failed to set avatar' => 'Failed to set avatar',
	'write something about yourself...' => 'write something about yourself...',
	'change' => 'change',
	'Choose a country' => 'Choose a country',
	'Choose city' => 'Choose city',
	'back' => 'back',
	'friends' => 'friends',
	'Friends' => 'Friends',
	'search' => 'seacrh',
	'delete' => 'delete',
	'Not working link' => 'Not working link',
	'Successfully' => 'Successfully',
	'Successful change of e-mail address' => 'Successful change of e-mail address',
	'go back' =>'go back',
	'enter first name or last name' => 'enter first name or last name',
	'enter a name' => 'enter a name',
	'image' => 'image',
	'select image' => 'select image',
	'edit' => 'edit',
	'an error has occurred' => 'an error has occurred',
	'Cabinet' => 'Cabinet',
	'Holidays search' => 'Holidays search',
	'Holidays' => 'Holidays',
	'Holiday' => 'Holiday',
	'name' => 'name',
	'apply' => 'apply',
	'date' => 'date',
	'from' => 'from',
	'to (inclusive)' => 'to (inclusive)',
	'sun' => 'sun',
	'mon' => 'mon',
	'tue' => 'tue',
	'wed' => 'wed',
	'thu' => 'thu',
	'fri' => 'fri',
	'sat' => 'sat',
	'accepted' => 'accepted',
	'declined' => 'declined',
	'viewed' => 'viewed',
	'not viewed' => 'not viewed',
	'Entity not found' => 'Entity not found',
	'report' => 'report',
	'Report' => 'Report',
	'add' => 'add',
	'view' => 'view',
	'No title' => 'No title',
	'not specified' => 'not specified',
	'list is empty' => 'list is empty',
	'enter description' => 'enter description',
	'enter first or last name' => 'enter first or last name',
	'save' => 'save',
	'cancel' => 'cancel',
	'Guest with id {0} already exist' => 'Guest with id {0} already exist',
	'Required parameter id is missing' => 'Required parameter id is missing',
	'description' => 'description',
	'accept' => 'accept',
	'decline' => 'decline',
	'Settings' => 'Settings',
	'settings' => 'settings',
	'Personal data' => 'Personal data',
	'Safety' => 'Safety',
	'Start of the working week' => 'Start of the working week',
	'Work days amount' => 'Work days amount',
	'Failed to save user settings' => 'Failed to save user settings',
	'Failed to save token' => 'Failed to save token',
	'Invalid token' => 'Invalid token',
	'back to list' => 'back to list',
	'Failed to delete entity' => 'Failed to delete entity',
	'An error occurred, please try again later' => 'An error occurred, please try again later',
	'Failed to save entity' => 'Failed to save entity',
	'Video' => 'Video',
	'video' => 'video',
	'photo' => 'photo',
	'image' => 'image',
	'document' =>'document',
	'videos' => 'videos',
	'photos' => 'photos',
	'images' => 'images',
	'documents' =>'documents',
	'Documents' =>'Documents',
	'select a video' => 'select a video',
	'select a photo' => 'select a photo',
	'upload video' => 'upload video',
	'upload photo' => 'upload photo',
	'upload image' => 'upload image',
	'upload document' => 'upload document',
	'maximum {0}' => 'maximum {0}',
	'comments' => 'comments',
	'write a comment' => 'write a comment',
	'commenting is not possible' => 'commenting is not possible',
	'show previous' => 'show previous',
	'files' => 'files',
	'Files' => 'Files',
	'event files' => 'event files',
	'meeting files' => 'meeting files',
	'task files' => 'task files',
	'Max video files {0}' => 'Max video files {0}',
	'Max images {0}' => 'Max images {0}',
	'Max documents {0}' => 'Max documents {0}',
	'No new notifications' => 'No new notifications',
	'Events' => 'Events',
	'events' => 'events',
	'Meetings' => 'Meetings',
	'meetings' => 'meetings',
	'profile' => 'profile',
	'close' => 'close',
	'File settings' => 'File settings',
	'file settings' => 'file settings',
	'current and complete' => 'current and complete',
	'scheduled' => 'scheduled',
	'more' => 'more',
	'show' => 'show',
	'invite to event' => 'invite to event',
	'invite to a meeting' => 'invite to a meeting',
	'Failed to send invitation' => 'Failed to send invitation',
	'the invitation is sent' => 'the invitation is sent',
	'and' => 'and',
	'clear' => 'clear',
	'select' => 'select',
	'Active' => 'Active',
	'Blocked' => 'Blocked',
	'Deleted' => 'Deleted',
	'Users' => 'Users',
	'User' => 'User',
	'Load' => 'Load',
	'Failed to save avatar' => 'Failed to save avatar',
	'Failed to save image' => 'Failed to save image',
	'Failed to save user' => 'Failed to save user',
	'Are you sure you want to delete your avatar?' => 'Are you sure you want to delete your avatar?',
	'Add user' => 'Add user',
	'Edit user' => 'Edit user',
	'Are you sure you want to delete holiday?' => 'Are you sure you want to delete holiday?',
	'Are you sure you want to delete section of articles?' => 'Are you sure you want to delete section of articles?',
	'Add holiday' => 'Add holiday',
	'Edit holiday' => 'Edit holiday',
	'Visit logs' => 'Visit logs',
	'Your calendar. Events. Meetings. Tasks. Holidays.' => 'Your calendar. Events. Meetings. Tasks. Holidays.',
	'Create reminders for important and interesting events. Find new friends and invite them to a scheduled event or meeting. Share photo and video reports. Create tasks and assign an executor.' => 'Create reminders for important and interesting events. Find new friends and invite them to a scheduled event or meeting. Share photo and video reports. Create tasks and assign an executor.',
	'Article sections' => 'Article sections',
	'Add a section for articles' => 'Add a section for articles',
	'Edit a section for articles' => 'Edit a section for articles',
	'Articles' => 'Articles',
	'Article' => 'Article',
	'Add an article' => 'Add an article',
	'Edit an article' => 'Edit an article',
	'complete' => 'complete',
	'Are you sure you want to delete article?' => 'Are you sure you want to delete article?',
	'Add paragraph' => 'Add paragraph',
	'Add image' => 'Add image',
	'universum' => 'universum',
	'Universum' => 'Universum',
	'books' => 'books',
	'back to section list' => 'back to section list',
	'back to article list' => 'back to article list',

	'role' => [
		'Admin' => 'Admin',
		'User' => 'User',
	],

	'photoPlural' => [
		'photo' => 'photo',
		'Photo' => 'Photo',
	],

	'genetiveTermsOfUse' => [
		'User agreement' => 'User agreement',
	],

	'genetivePrivacyPolicy' => [
		'Privacy policy' => 'Privacy policy',
	],
];