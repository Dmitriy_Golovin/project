<?php

return [
	'Russian holidays' => 'Russian holidays',
	'Holiday search' => 'Holiday search',
	'Holidays' => 'Holidays',
	'Holiday with id {0} not found' => 'Holiday with id {0} not found',
	'Failed to save holiday' => 'Failed to save holiday',
	'Failed to delete holiday' => 'Failed to delete holiday',
	'Failed to save holiday date' => 'Failed to save holiday date',
];