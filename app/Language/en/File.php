<?php

return [
	'Limit on the number of files to upload at one time - {0}' => 'Limit on the number of files to upload at one time - {0}',
	'Image file not found' => 'Image file not found',
	'File not found' => 'File not found',
	'File access denied' => 'File access denied',
	'Failed to delete file' =>'Failed to delete file',
	'Failed to save file' =>'Failed to save file',
	'File deleted' => 'File deleted',
	'File does not exist, it may have been deleted' => 'File does not exist, it may have been deleted',
	'private' => 'private',
	'public' => 'public',
	'make private' => 'make private',
	'make public' => 'make public',
	'file access' => 'file access',
	'File not found' => 'File not found',
];