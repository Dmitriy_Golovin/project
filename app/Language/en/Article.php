<?php

return [
	'draft' => 'draft',
	'not published' => 'not published',
	'published' => 'published',
	'Failed to save article' => 'Failed to save article',
	'Article with id {0} not found' => 'Article with id {0} not found',
	'Failed to delete article' => 'Failed to delete article',
];