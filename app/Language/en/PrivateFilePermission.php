<?php

return [
	'Failed to save permissions to view the file' => 'Failed to save permissions to view the file',
	'cancel' => 'cancel',
	'add users' => 'add users',
	'The user already has access to the file' => 'The user already has access to the file',
	'The user does not have access to the file' => 'The user does not have access to the file',
	'Failed to remove file permissions' => 'Failed to remove file permissions',
];