<?php

return [
	'create a chat' => 'create a chat',
	'Messages' => 'Messages',
	'mark as important' => 'mark as important',
	'remove from important' => 'remove from important',
	'attachments' => 'attachments',
	'create chat' => 'create chat',
	'edit chat' => 'edit chat',
	'Add users' => 'Add users',
	'Chat name' => 'Chat name',
	'Messages' => 'Messages',
	'Create chat' => 'Create chat',
	'Edit chat' => 'Edit chat',
	'Failed to save message file' => 'Failed to save message file',
	'Failed to save message' => 'Failed to save message',
	'message already deleted' => 'message already deleted',
	'No messages' => 'No messages',
];