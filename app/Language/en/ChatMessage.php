<?php

return [
	'Failed to save chat message' => 'Failed to save chat message',
	'Message not found' => 'Message not found',
	'Failed to save reaction to message' =>'Failed to save reaction to message',
];