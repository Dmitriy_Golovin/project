<?php

return [
	'assign an executor' => 'assign an executor',
	'make yourself an executor' => 'make yourself an executor',
	'assign' => 'assign',
	'invite' => 'invite',
	'cancel' => 'cancel',
	'executor' => 'executor',
	'executor not selected' => 'executor not selected',
	'current' => 'current',
	'scheduled' => 'scheduled',
	'completed' => 'completed',
	'not completed' => 'not completed',
	'overdue' => 'overdue',
	'Tasks' => 'Tasks',
	'Task date and time cannot be in the past' => 'Task date and time cannot be in the past',
	'Failed to save task history' => 'Failed to save task history',
	'Failed to save task' => 'Failed to save task',
	'Tasks of the current day' => 'Tasks of the current day',
	'tasks of the current day' => 'tasks of the current day',
	'Scheduled tasks' => 'Scheduled tasks',
	'scheduled tasks' => 'scheduled tasks',
	'Completed tasks' => 'Completed tasks',
	'completed tasks' => 'completed tasks',
	'Overdue tasks' => 'Overdue tasks',
	'overdue tasks' => 'overdue tasks',
	'Created tasks' => 'Created tasks',
	'created tasks' => 'created tasks',
	'Are you sure you want to delete the task?' => 'Are you sure you want to delete the task?',
	'task created by' => 'task created by',
	'review status' => 'review status',
	'task status' => 'task status',
	'accepted' => 'accepted',
	'rejected' => 'rejected',
	'not reviewed' => 'not reviewed',
	'Create task' => 'Create task',
	'create task' => 'create task',
	'Edit task' => 'Edit task',
	'edit task' => 'edit task',
	'Task' => 'Task',
	'task' => 'task',
	'edit report' => 'edit report',
	'complete' => 'complete',
	'status' => 'status',
	'history' => 'history',
	'viewed by executor' => 'viewed by executor',
	'Failed to set status as "viewed by executor"' => 'Failed to set status as "viewed by executor"',
	'accept' => 'accept',
	'reject' => 'reject',
	'accept (edit)' => 'accept (edit)',
	'reject (edit)' => 'reject (edit)',
	'The task cannot be with empty data' => 'The task cannot be with empty data',
	'completed the task' => 'completed the task',
	'accepted the result of completing the task' => 'accepted the result of completing the task',
	'rejected the result of completing the task' => 'rejected the result of completing the task',
	'completed the task (edit)' => 'completed the task (edit)',
	'accepted the result of completing the task (edit)' => 'accepted the result of completing the task (edit)',
	'rejected the result of completing the task (edit)' => 'rejected the result of completing the task (edit)',
	'no task executor' => 'no task executor',

	'statusDatePlural' => [
		'overdue' => 'overdue',
		'created' => 'created',
	],

	'statusPlural' => [
		'any' => 'any',
		'not completed' => 'not completed',
		'completed' => 'completed',
		'overdue' => 'overdue',
	],

	'reviewStatusPlural' => [
		'any' => 'any',
		'not reviewed' => 'not reviewed',
		'rejected' => 'rejected',
		'accepted' => 'accepted',
	],
];