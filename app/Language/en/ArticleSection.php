<?php

return [
	'Failed to save article section' => 'Failed to save article section',
	'Article section with id {0} not found' => 'Article section with id {0} not found',
	'Failed to delete article section' => 'Failed to delete article section',
];