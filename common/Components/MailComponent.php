<?php

namespace Common\Components;

class MailComponent
{
	public static function sendActivationToken($emailTo, $verifyToken) {
		$email = \Config\Services::email();
		
		$email->setFrom(config('MailConfig')->adminEmail, 'Calendaric');
		$email->setTo($emailTo);

		$message = '<html>
					  <head>
					  </head>
					  <body>
					    <h2>Добро пожаловать на сайт ' . getenv('baseUrl') . '</h2>
						<p>Активируйте вашу учетную запись: <a href="' . base_url('activation/' . $verifyToken) . '">нажмите здесь.</a></p>
						<p>С уважением, администрация сайта ' . getenv('siteUrl') . '.</p>
					  </body>
					</html>';
		$email->setSubject('Активация аккаунта');
		$email->setMessage($message);

		return $email->send();
	}

	public static function sendRestoreToken($emailTo, $token) {
		$email = \Config\Services::email();
		
		$email->setFrom(config('MailConfig')->adminEmail, 'Calendaric');
		$email->setTo($emailTo);

		$message = '<html>
					  <head>
					  </head>
					  <body>
					    <h2>Мы рады, что Вы с нами!</h2>
						<p>Ссылка для восстановления пароля: <a href="' . base_url('restore-password/' . $token) . '">нажмите здесь.</a></p>
						<p>С уважением, администрация сайта ' . getenv('siteUrl') . '.</p>
					  </body>
					</html>';
		$email->setSubject('Восстановление пароля');
		$email->setMessage($message);

		return $email->send();
	}

	public static function sendTokenChangeEmail($emailTo, $token) {
		$email = \Config\Services::email();
		
		$email->setFrom(config('MailConfig')->adminEmail, 'Calendaric');
		$email->setTo($emailTo);

		$message = '<html>
					  <head>
					  </head>
					  <body>
					    <h2>Добро пожаловать на сайт ' . getenv('baseUrl') . '</h2>
						<p>Пожалуйста, подтвердите изменение адреса электронной почты: <a href="http://project1.loc/confirm_change_email/' . $token . '">нажмите здесь.</a></p>
						<p>С уважением, администрация сайта ' . getenv('siteUrl') . '.</p>
					  </body>
					</html>';
		$email->setSubject('Подтвердите изменение e-mail адреса');
		$email->setMessage($message);

		return $email->send();
	}
}