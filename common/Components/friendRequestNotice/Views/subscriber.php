<?php

use Common\Models\FriendModel;

?>

<div class="modal modal_notice">
  <div class="modal_box">
    <div class="modal_container_top">
	    <div class="notice_box_modal">
	    	<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
	    	<h3 class="notice_modal_title_friend">
	    		<?= lang('Friend.User') ?>
	    		<a href="/profile/<?= $user->user_id ?>">
	    			<span class="notice_user_name"><?= ' ' . $user->userFullname . ' ' ?></span>
	    		</a>
	    		<?= lang('Friend.has already sent a friend request') ?>
	    	</h3>
	    	<div class="notice_button_box">
	    		<span data-profile_id="<?= $user->user_id ?>" data-ws_tab_sync_to_do="<?= FriendModel::WS_ACTION_ACCEPT_FRIENDSHIP ?>" class="main_profile_button friend_button profile_button"><?= lang('Main.accept') ?></span>
	    	</div>
	    </div>
	  </div>
  </div>
</div>