<?php

use Common\Models\FriendModel;

?>

<div class="modal modal_notice">
  <div class="modal_box">
    <div class="modal_container_top">
	    <div class="notice_box_modal">
	    	<span class="close_win_modal">
		    	<img alt="Close window" class="close_pic_modal" src="/img/close_.png">
		  	</span>
	    	<h3 class="notice_modal_title_friend">
	    		<?= lang('Friend.You didn\'t send the user') ?>
	    		<a href="/profile/<?= $user->user_id ?>">
	    			<span class="notice_user_name"><?= ' ' . $user->userFullname . ' ' ?></span>
	    		</a>
	    		<?= lang('Friend.a friend request') ?>
	    	</h3>
	    </div>
	  </div>
  </div>
</div>