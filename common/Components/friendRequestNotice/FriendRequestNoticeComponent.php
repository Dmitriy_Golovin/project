<?php

namespace Common\Components\friendRequestNotice;

use Common\Models\FriendModel;

class FriendRequestNoticeComponent
{
	const ACTION_SUBSCRIBE = 'subscribe';
	const ACTION_UNSUBSCRIBE = 'unsubscribe';
	const ACTION_ACCEPT_FRIENDSHIP = 'acceptFriendship';
	const ACTION_REJECT_FRIENDSHIP = 'rejectFriendship';
	const ACTION_REMOVE_FRIEND = 'removeFriend';

	public static function getNotice(array $data)
	{
		$viewFile = self::getViewPath()[$data['action']][$data['relationNoticeData'][$data['user']->user_id]['user_relations']];
		return view($viewFile, $data);
	}

	protected static function getViewPath()
	{
		return [
			self::ACTION_SUBSCRIBE => [
				FriendModel::RELATION_YOU_ARE_SUBSCRIBED => 'Common\Components\friendRequestNotice\Views\you_are_subscribed',
				FriendModel::RELATION_SUBSCRIBER => 'Common\Components\friendRequestNotice\Views\subscriber',
				FriendModel::RELATION_NEW_SUBSCRIBER => 'Common\Components\friendRequestNotice\Views\subscriber',
				FriendModel::RELATION_FRIEND => 'Common\Components\friendRequestNotice\Views\friend',
			],
			self::ACTION_UNSUBSCRIBE => [
				FriendModel::RELATION_NO_RELATION => 'Common\Components\friendRequestNotice\Views\unsubscribe_no_relation',
				FriendModel::RELATION_SUBSCRIBER => 'Common\Components\friendRequestNotice\Views\subscriber',
				FriendModel::RELATION_NEW_SUBSCRIBER => 'Common\Components\friendRequestNotice\Views\subscriber',
				FriendModel::RELATION_FRIEND => 'Common\Components\friendRequestNotice\Views\friend',
			],
			self::ACTION_ACCEPT_FRIENDSHIP => [
				FriendModel::RELATION_NO_RELATION => 'Common\Components\friendRequestNotice\Views\accept_friendship_no_relation',
				FriendModel::RELATION_YOU_ARE_SUBSCRIBED => 'Common\Components\friendRequestNotice\Views\accept_friendship_no_relation',
				FriendModel::RELATION_FRIEND => 'Common\Components\friendRequestNotice\Views\friend',
			],
			self::ACTION_REJECT_FRIENDSHIP => [
				FriendModel::RELATION_NO_RELATION => 'Common\Components\friendRequestNotice\Views\accept_friendship_no_relation',
				FriendModel::RELATION_YOU_ARE_SUBSCRIBED => 'Common\Components\friendRequestNotice\Views\accept_friendship_no_relation',
				FriendModel::RELATION_FRIEND => 'Common\Components\friendRequestNotice\Views\friend',
			],
			self::ACTION_REMOVE_FRIEND => [
				FriendModel::RELATION_NO_RELATION => 'Common\Components\friendRequestNotice\Views\remove_friend',
				FriendModel::RELATION_YOU_ARE_SUBSCRIBED => 'Common\Components\friendRequestNotice\Views\remove_friend',
				FriendModel::RELATION_SUBSCRIBER => 'Common\Components\friendRequestNotice\Views\remove_friend',
				FriendModel::RELATION_NEW_SUBSCRIBER => 'Common\Components\friendRequestNotice\Views\remove_friend',
			],
		];
	}
}