<?php

namespace Common\Components\friendRequest;

class FriendRequestComponent
{
	public static function getFriendButtonUserProfile(array $data)
	{
		return view('Common\Components\friendRequest\Views\friend_button_user_profile', $data);
	}

	public static function getFriendUserItem(array $data)
	{
		return view('friend/friend_item_self', $data);
	}
}