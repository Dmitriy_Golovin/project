<?php

namespace Common\Components;

class WsClientComponent
{
	private $_socket;
	public $error;

	public function openSocket()
	{
		helper('text');
        $key = base64_encode(random_string('alnum', 16));
        $token = config('WsServerConfig')->tokenWsServer;
        $host = config('WsServerConfig')->host;
        $port = config('WsServerConfig')->port;

        $header = "GET /server HTTP/1.1\r\n";
        $header .= "Host: " . $host . ":" . $port . "\r\n";
        $header .= "Upgrade: websocket\r\n";
        $header .= "Connection: keep-alive, upgrade\r\n";
        $header .= "Sec-WebSocket-Key: " . $key . "\r\n";
        $header .= "Sec-WebSocket-Version: 13\r\n";
        $header .= "Origin: https://calendaric.fun\r\n";
        $header .= "Auth-Token: " . $token . "\r\n";
        $header .= "\r\n";

        $this->_socket = stream_socket_client('ssl://calendaric.fun:' . $port, $errno, $errstr, 30);

        if (!fwrite($this->_socket, $header)) {
        	$this->error = $errno;
        	return false;
        }

        $response = fread($this->_socket, 2000);

        return true;
	}

	public function closeSocket()
	{
		fclose($this->_socket);
	}

	public function sendData($data)
	{
        if (!fwrite($this->_socket, $this->_wsDataEncode(json_encode($data)))) {
        	$this->error = $errno;
        	return false;
        }

        /**
         * @todo: check response here. Currently not implemented cause "2 key handshake" is already deprecated.
         * See: http://en.wikipedia.org/wiki/WebSocket#WebSocket_Protocol_Handshake
         */
        return true;
    }

    public function sendDataWithResponse($data)
	{
        if (!fwrite($this->_socket, $this->_wsDataEncode(json_encode($data)))) {
        	$this->error = $errno;
        	return false;
        }
        
        $response = '';
        $i =0;

        while(($buff = fread($this->_socket, 1024)) !== false) {
			$response .= $buff;
			$i++;
			
			if ((int)$i === 2) {
				break;
			}
		}

        /**
         * @todo: check response here. Currently not implemented cause "2 key handshake" is already deprecated.
         * See: http://en.wikipedia.org/wiki/WebSocket#WebSocket_Protocol_Handshake
         */
        return json_decode($this->_wsDataDecode($response));
    }

    private function _wsDataEncode($payload, $type = 'text', $masked = true) {
	    $frameHead = array();
	    $frame = '';
	    $payloadLength = strlen($payload);

	    switch ($type) {
	        case 'text':
	            // first byte indicates FIN, Text-Frame (10000001):
	            $frameHead[0] = 129;
	            break;

	        case 'close':
	            // first byte indicates FIN, Close Frame(10001000):
	            $frameHead[0] = 136;
	            break;

	        case 'ping':
	            // first byte indicates FIN, Ping frame (10001001):
	            $frameHead[0] = 137;
	            break;

	        case 'pong':
	            // first byte indicates FIN, Pong frame (10001010):
	            $frameHead[0] = 138;
	            break;
	    }

	    // set mask and payload length (using 1, 3 or 9 bytes)
	    if ($payloadLength > 65535) {
	        $payloadLengthBin = str_split(sprintf('%064b', $payloadLength), 8);
	        $frameHead[1] = ($masked === true) ? 255 : 127;
	        for ($i = 0; $i < 8; $i++) {
	            $frameHead[$i + 2] = bindec($payloadLengthBin[$i]);
	        }

	        // most significant bit MUST be 0 (close connection if frame too big)
	        if ($frameHead[2] > 127) {
	            $this->close(1004);
	            return false;
	        }
	    } elseif ($payloadLength > 125) {
	        $payloadLengthBin = str_split(sprintf('%016b', $payloadLength), 8);
	        $frameHead[1] = ($masked === true) ? 254 : 126;
	        $frameHead[2] = bindec($payloadLengthBin[0]);
	        $frameHead[3] = bindec($payloadLengthBin[1]);
	    } else {
	        $frameHead[1] = ($masked === true) ? $payloadLength + 128 : $payloadLength;
	    }

	    // convert frame-head to string:
	    foreach (array_keys($frameHead) as $i) {
	        $frameHead[$i] = chr($frameHead[$i]);
	    }

	    if ($masked === true) {
	        // generate a random mask:
	        $mask = array();
	        for ($i = 0; $i < 4; $i++) {
	            $mask[$i] = chr(rand(0, 255));
	        }

	        $frameHead = array_merge($frameHead, $mask);
	    }
	    $frame = implode('', $frameHead);
	    // append payload to frame:
	    for ($i = 0; $i < $payloadLength; $i++) {
	        $frame .= ($masked === true) ? $payload[$i] ^ $mask[$i % 4] : $payload[$i];
	    }

	    return $frame;
	}

	private function _wsDataDecode($data)
	{
	    $bytes = $data;
	    $dataLength = '';
	    $mask = '';
	    $coded_data = '';
	    $decodedData = '';
	    $secondByte = sprintf('%08b', ord($bytes[1]));
	    $masked = ($secondByte[0] == '1') ? true : false;
	    $dataLength = ($masked === true) ? ord($bytes[1]) & 127 : ord($bytes[1]);

	    if ($masked === true) {
	        if ($dataLength === 126) {
	           $mask = substr($bytes, 4, 4);
	           $coded_data = substr($bytes, 8);
	        } elseif ($dataLength === 127) {
	            $mask = substr($bytes, 10, 4);
	            $coded_data = substr($bytes, 14);
	        } else {
	            $mask = substr($bytes, 2, 4);       
	            $coded_data = substr($bytes, 6);        
	        }   
	        
	        for($i = 0; $i < strlen($coded_data); $i++) {       
	            $decodedData .= $coded_data[$i] ^ $mask[$i % 4];
	        }
	    } else {
	        if ($dataLength === 126)
	        {          
	           $decodedData = substr($bytes, 4);
	        } elseif ($dataLength === 127) {           
	            $decodedData = substr($bytes, 10);
	        } else {               
	            $decodedData = substr($bytes, 2);       
	        }       
	    }   

	    return $decodedData;
	}
}