<?php

namespace Common\Components\nothingFoundList;

class NothingFoundListComponent
{
	public static function render($locale = '')
	{
		return view('Common\Components\nothingFoundList\Views\nothing_found', ['locale' => $locale]);
	}
}