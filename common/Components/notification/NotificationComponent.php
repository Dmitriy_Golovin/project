<?php

namespace Common\Components\notification;

use Common\Models\TaskHistoryModel;

class NotificationComponent
{
	public function getTaskActionNotification(array $data)
	{
		if (empty($data['task']->name)) {
			$data['task']->name = lang('Main.No title');
		}

		$url = '/task/assigned-details/' . $data['task']->task_id;

		if ($data['fromUserData']->user_id === $data['task']->assigned_user_id) {
			$url = '/task/owner-details/' . $data['task']->task_id;
		}

		return view('Common\Components\notification\Views\task_action_notification', [
			'user' => $data['fromUserData'],
			'task' => $data['task'],
			'textInfo' => $data['textInfo'],
			'url' => $url,
		]);
	}
}