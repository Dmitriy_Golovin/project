<div class="notification_container">
	<div class="notification_user_container">
		<a href="/profile/<?= $user->user_id ?>">
			<img class="notification_user_data_ava" src="<?= $user->path_to_ava ?>">
			<div class="notification_user_data_name">
				<?= $user->first_name . ' ' . $user->last_name ?>
			</div>
		</a>
	</div>
	<div class="notification_text_container">
		<?= $textInfo ?>
		<a href="<?= $url ?>">
			<span class="notification_url_span"><?= $task->name ?></span>
		</a>
	</div>
</div>