<?php

namespace Common\Config;

use CodeIgniter\Config\BaseConfig;

class MailConfig extends BaseConfig
{
	public $adminEmail = 'noreply@calendaric.fun';
}