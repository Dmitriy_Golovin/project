<?php

namespace Common\Config;

use CodeIgniter\Config\BaseConfig;

class MainEventParamsConfig extends BaseConfig
{
	public $duration = [
		3600 => '1 hour',
		7200 => '2 hours',
		10800 => '3 hours',
		14400 => '4 hours',
		18000 => '5 hours',
		21600 => '6 hours',
		25200 => '7 hours',
		28800 => '8 hours',
		32400 => '9 hours',
		36000 => '10 hours',
		39600 => '11 hours',
		43200 => '12 hours',
		46800 => '13 hours',
		50400 => '14 hours',
		54000 => '15 hours',
		57600 => '16 hours',
		61200 => '17 hours',
		64800 => '18 hours',
		68400 => '19 hours',
		72000 => '20 hours',
		75600 => '21 hours',
		79200 => '22 hours',
		82800 => '23 hours',
		86400 => '1 day',
		172800 => '2 days',
		259200 => '3 days',
		345600 => '4 days',
		432000 => '5 days',
		518400 => '6 days',
		604800 => 'week',
	];

	public $periodicity = [
		0 => 'none',
		1 => 'working day',
		2 => 'every day',
		3 => 'every week',
		4 => 'every month',
		5 => 'yearly',
	];

	public $reminder = [
		'' => '-',
		3600 => '1 hour',
		7200 => '2 hours',
		10800 => '3 hours',
		14400 => '4 hours',
		18000 => '5 hours',
		21600 => '6 hours',
		28800 => '8 hours',
		43200 => '12 hours',
		86400 => '1 day',
		172800 => '2 days',
		259200 => '3 days',
		604800 => 'week',
	];

	public $privacy = [
		'' => 'any',
		0 => 'public',
		1 => 'private',
	];

	public $reviewStatusWithoutRejected = [
		'' => 'any',
		0 => 'not reviewed',
		2 => 'accepted',
	];

	public $taskStatus = [
		'' => 'any',
		1 => 'not completed',
		2 => 'completed',
		3 => 'overdue',
	];

	public $reviewStatus = [
		'' => 'any',
		0 => 'not reviewed',
		1 => 'rejected',
		2 => 'accepted',
	];

	public $eventCreateMaxVideo = 1;
	public $eventCreateMaxImage = 3;
	public $eventCreateMaxDocument = 3;

	public $reportMaxVideo = 1;
	public $reportMaxImage = 10;

	public $taskHistoryMaxVideo = 1;
	public $taskHistoryMaxImage = 10;
	public $taskHistoryMaxDocument = 5;

	public $typeProfilePageEvent = 1;
	public $typeProfilePageMeeting = 2;

	public $subTypeProfilePageCurrentCompleted = 1;
	public $subTypeProfilePageScheduled = 2;

	public $mainTypeEvent = 1;
	public $mainTypeMeeting = 2;
}
