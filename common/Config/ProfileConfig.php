<?php

namespace Common\Config;

use CodeIgniter\Config\BaseConfig;

class ProfileConfig extends BaseConfig
{
	public $profileActiveMenu = 'profile';
	public $friendsActiveMenu = 'friends';
	public $eventsActiveMenu = 'event';
	public $meetingsActiveMenu = 'meeting';
	public $photoActiveMenu = 'photo';
	public $videoActiveMenu = 'video';
}