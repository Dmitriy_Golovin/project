<?php

namespace Common\Config;

use CodeIgniter\Config\BaseConfig;

class FileConfig extends BaseConfig
{
	public $maxUploadFileCount = 10;

	public $filePageTypeImage = 'image';
	public $filePageTypeVideo = 'video';
	public $filePageTypeDocument = 'document';
}