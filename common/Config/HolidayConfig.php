<?php

namespace Common\Config;

use CodeIgniter\Config\BaseConfig;

class HolidayConfig extends BaseConfig
{
	public $title = [
		'ru' => 'Russian holidays',
	];
}
