<?php

namespace Common\Widgets\comment;

class CommentWidget
{
	public function render(array $params = [])
	{
		return view('Common\Widgets\comment\Views\comment', $params);
	}
}