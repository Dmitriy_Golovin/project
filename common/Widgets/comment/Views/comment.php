<div class="comment_container">
	<div class="comment_header">
		<div class="comment_icon_container">
			<img class="comment_icon" src="/img/comment_icon.png">
		</div>
		<div class="main_comment_amount">
			<?= $commentData['mainCommentAmount'] ?>
		</div>
	</div>
	
	<?php $commentData['writeField'] = $writeField ?>
	<?php $this->setVar('commentData', $commentData) ?>
	<?= $this->include('comment/comment_list') ?>
	<?php if ($writeField) : ?>
		<div class="comment_field_container">
			<div contenteditable="true" class="comment_field" data-placeholder="<?= lang('Main.write a comment') ?>"></div>
			<img class="send_comment_icon" src="/img/send_icon.png">
		</div>
	<?php endif ?>
</div>