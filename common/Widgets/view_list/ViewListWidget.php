<?php

namespace Common\Widgets\view_list;

use Common\Models\FileModel;

class ViewListWidget
{
	const BUTTON_ACTION_LABEL = 'buttonActions';

	public $result;
	public $breadCrumbs;
	public $title;
	public $actions = [];
	public $columns = [];
	public $pager;
	public $model;
	public $supportElement = [];

	public function __construct(array $params)
	{
		$this->result = $params['result'];
		$this->breadCrumbs = (!empty($params['breadCrumbs']))
			? $params['breadCrumbs']
			: '';
		$this->title = $params['title'];
		$this->columns = $params['columns'];
		$this->actions = (!empty($params['actions'])) ? $params['actions'] : [];
		$this->pager = $params['pager'];
		$this->model = $params['model'];

		$this->run();
	}

	public function run()
	{
		$title = $this->getTitle();
		$action = $this->getActions();
		$filter = $this->getFilters();
		$table = $this->getTable();
		$pager = $this->getPager();

		echo '<div class="admin_list_box">'
			. $this->breadCrumbs . $title . $action . $filter . $table . $pager
			. '</div>';
	}

	public function getTitle()
	{
		return '<h2 class="admin_list_title">' . $this->title . '</h2>';
	}

	public function getActions()
	{
		if (empty($this->actions)) {
			return '';
		}

		$result = '<div class="admin_list_action_block">';

		foreach ($this->actions as $action) {
			$result .= self::prepareActionItem($action);
		}

		$result .= '</div>';

		return $result;
	}

	public function getFilters()
	{
		$filterContent = '';

		foreach ($this->columns as $column) {
			if (!is_array($column) || empty($column['filter'])) {
				continue;
			}
			// var_dump($column['label']);
			$filterContent .= $column['filter'];
		}

		$active = (!empty($filterContent))
			? ''
			: 'not_active';
		$result = '<div class="admin_filter_block ' . $active . '">';
		$result .= '<form action="" method="get">';
		$result .= $filterContent;
		$result .= '<div class="admin_filter_button_block">';
		$result .= '<input type="button" class="admin_filter_clear_button" value="' . lang('Main.clear') . '">';
		$result .= '<input type="submit" class="admin_filter_submit_button" value="' . lang('Main.search') . '">';
		$result .= '</div>';
		$result .= '</form>';
		$result .= '</div>';

		return $result;
	}

	public function getTable()
	{
		$result = '<table class="admin_list_table">';
		$result .= $this->getTableHead();
		$result .= $this->getTableContent();
		$result .= '</table>';

		return $result;
	}

	public function getPager()
	{
		return $this->pager->links('default', 'admin_base');
	}

	public static function prepareActionItem($action)
	{
		if (empty($action)) {
			return '';
		}

		$result = '<a href="' . $action['url'] . '"><span class="' . $action['cssClass'] . '">'
			. $action['label'] . '</span></a>';

		return $result;
	}

	public function getTableHead()
	{
		$result = '<tr>';

		foreach ($this->columns as $column) {
			$label = (is_array($column))
				? $column['label']
				: $column;

			if (is_array($column) && $column['label'] === self::BUTTON_ACTION_LABEL) {
				$label = '';
			} else {
				$label = $this->model::getLabels()[$label] ;
			}

			$result .= '<th>' . $label . '</th>';
		}

		$result .= '</tr>';

		return $result;
	}

	public function getTableContent()
	{
		if (empty($this->result)) {
			return '<tr><td class="nothing_found" colspan="' . count($this->columns) . '">' . lang('Main.Nothing found') . '</td></tr>';
		}

		$result = '';

		foreach ($this->result as $item) {
			$result .= '<tr>';

			foreach ($this->columns as $column) {
				$label = (is_array($column))
					? $column['label']
					: $column;
				$value = (is_array($column) && $column['label'] === self::BUTTON_ACTION_LABEL)
					? ''
					: $item->{$label};
				$style = '';

				if (is_array($column)) {
					if (!empty($column['style'])) {
						$style = 'style="' . $column['style'] . '"';
					}

					if (!empty($column['value'])) {
						$value = $column['value']($item);
					}

					$column['value'] = null;
				}


				if (is_null($value)) {
					$value = '<span style="font-style: italic; font-size: 11pt; color: #848484">Не задано</span>';
				}

				$result .= '<td ' . $style . '>' . $value . '</td>';
			}

			$result .= '</tr>';
		}

		if (empty($result)) {
			$result = '<tr><td class="nothing_found">' . lang('Main.Nothing found') . '</td></tr>';
		}

		return $result;
	}
}