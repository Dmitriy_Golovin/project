<?php

namespace Common\Widgets\calendar;

class CalendarWidget
{
	public function render(array $params = [])
	{
		return view('Common\Widgets\calendar\Views\calendar', $params);
	}
}