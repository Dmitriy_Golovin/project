<div class="calendar_box">
	<span class="calendar_container_close">
		<img src="/img/close_.png" class="calendar_container_close_icon">
	</span>
	<div class="calendar_header_box">
		<div class="calendar_left_arrow"><</div>
		<div class="calendar_date_title"></div>
		<div class="calendar_right_arrow">></div>
	</div>
	<table>
		<tr>
			<th><?= lang('Main.mon') ?></th>
			<th><?= lang('Main.tue') ?></th>
			<th><?= lang('Main.wed') ?></th>
			<th><?= lang('Main.thu') ?></th>
			<th><?= lang('Main.fri') ?></th>
			<th><?= lang('Main.sat') ?></th>
			<th><?= lang('Main.sun') ?></th>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>
</div>