<?php

namespace Common\Widgets\bread_crumbs;

class BreadCrumbsWidget
{
	public function renderBase(array $params)
	{
		return view('Common\Widgets\bread_crumbs\Views\base', $params);
	}
}