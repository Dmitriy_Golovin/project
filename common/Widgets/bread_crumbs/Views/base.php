<ul class="admin_bread_crumbs_container">

	<?php foreach ($items as $item) : ?>
		<?php $active = (!empty($item['active']) && $item['active'])
			? true
			: false;
		?>

		<li class="<?= ($active) ? 'active' : '' ?>">

			<?php if (!$active) : ?>

				<a href="<?= $item['url'] ?>">

			<?php endif ?>

				<?= $item['label'] ?>

			<?php if (!$active) : ?>

				</a>

			<?php endif ?>

		</li>

	<?php endforeach ?>

</ul>