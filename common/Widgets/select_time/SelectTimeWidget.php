<?php

namespace Common\Widgets\select_time;

class SelectTimeWidget
{
	public function render(array $params = [])
	{
		return view('Common\Widgets\select_time\Views\select_time', $params);
	}
}