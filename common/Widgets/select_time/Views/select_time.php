<div class="select_time_container">
	<div class="select_time_box">
		<span class="select_time_container_close">
			<img src="/img/close_.png" class="select_time_container_close_icon">
		</span>
		<ul class="select_time_hours_box">

			<?php for ($i = 0; $i < 24; $i++) {
				$value = $i;

				if (strlen($value . '') < 2) {
					$value = '0' . $value;
				}

				echo '<li data-hours="' . $value .'">' . $value . '</li>';
			} ?>

		</ul>
		<ul class="select_time_minutes_box">

			<?php for ($y = 0; $y < 60; $y++) {
				$value = $y;

				if (strlen($value . '') < 2) {
					$value = '0' . $value;
				}

				echo '<li data-minutes="' . $value .'">' . $value . '</li>';
			} ?>

		</ul>
	</div>
</div>