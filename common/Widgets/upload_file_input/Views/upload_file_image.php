<?php

use Common\Models\FileModel;

?>

<div id="upload_file_image_field_container" class="text_container_base">
	<div class="text_title_container"><?= $model->labels[$fieldName] ?></div>
	<div class="field_image_container<?= (!empty($existFileId)) ? ' not_active' : '' ?>">
		<label for="admin_upload_file_image" class="admin_upload_file_label"><?= lang('Main.Load') ?></label>
		<input id="admin_upload_file_image" type="file" name="<?= $fieldName ?>" hidden>
		<input id="admin_delete_file_image" type="text" name="delete_file" value="0" hidden>
	</div>

	<div class="image_preview_container<?= (empty($existFileId)) ? ' not_active' : '' ?>">

		<?php if (!empty($existFileId)) : ?>

			<?= view_cell('\Common\Widgets\file\ImageFileWidget::renderAdminPreview', [
					'file' => (object)[
						'file_id' => $existFileId,
						'url' => FileModel::preparePathAdminToUserFile($existUrl, FileModel::TYPE_IMAGE),
						'type' => FileModel::TYPE_IMAGE,
					],
					'deleteBlock' => true,
					'cssClass' => ' admin_details_delete_file_holiday'
				]) ?>

		<?php endif ?>

	</div>

	<?= view_cell('\Common\Widgets\file\ImageFileWidget::renderTemplateForLoad', []) ?>

	<script src="/js/Admin/upload_file_image.js"></script>
</div>