<form action="<?= $action ?>" class="upload_file_widget_form" method="post" enctype="multipart/form-data">
	<label for="admin_upload_file" class="admin_upload_file_label"><?= lang('Main.Load') ?></label>
	<input id="admin_upload_file" type="file" name="<?= $fieldName ?>" hidden>
</form>