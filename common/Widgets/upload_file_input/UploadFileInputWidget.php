<?php

namespace Common\Widgets\upload_file_input;

class UploadFileInputWidget
{
	public function renderBase(array $params)
	{
		return view('Common\Widgets\upload_file_input\Views\upload_file_base', $params);
	}

	public function renderImage(array $params)
	{
		return view('Common\Widgets\upload_file_input\Views\upload_file_image', $params);
	}
}