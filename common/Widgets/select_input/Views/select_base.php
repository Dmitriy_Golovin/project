<div class="select_container_base">
	<div class="text_title_container"><?= $model->labels[$fieldName] ?></div>
	<select name="<?= $fieldName ?>" class="admin_base_field" value="<?= $model->{$fieldName} ?>" data-selected_index="<?= (!empty($selectedIndex)) ? $selectedIndex : '' ?>">
		<option class="admin_select_default_value" value=""><?= lang('Main.select') ?></option>

		<?php foreach ($data as $index => $item) : ?>

			<option class="" value="<?= $index ?>" <?= ((int)$model->{$fieldName} === (int)$index) ? 'selected' : '' ?>><?= $item ?></option>

		<?php endforeach ?>

	</select>
</div>