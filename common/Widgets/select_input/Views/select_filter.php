<div class="select_container">
	<div class="text_title_container"><?= $labelValue ?></div>
	<select name="<?= $fieldName ?>" class="admin_filter_field" value="<?= $form->{$fieldName} ?>">
		<option class="" value=""><?= lang('Main.select') ?></option>

		<?php foreach ($data as $index => $item) : ?>

			<option class="" value="<?= $index ?>" <?= ((int)$form->{$fieldName} === (int)$index) ? 'selected' : '' ?>><?= $item ?></option>

		<?php endforeach ?>

	</select>
</div>