<?php

namespace Common\Widgets\select_input;

class SelectInputWidget
{
	public function renderBase(array $params)
	{
		return view('Common\Widgets\select_input\Views\select_base', $params);
	}

	public function renderFilter(array $params)
	{
		return view('Common\Widgets\select_input\Views\select_filter', $params);
	}
}