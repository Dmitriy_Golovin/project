<?php

namespace Common\Widgets\textarea_input;

class TextareaInputWidget
{
	public function renderBase(array $params)
	{
		return view('Common\Widgets\textarea_input\Views\textarea_base', $params);
	}
}