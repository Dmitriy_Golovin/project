<div class="text_container_base">
	<div class="text_title_container"><?= $model->labels[$fieldName] ?></div>
	<textarea name="<?= $fieldName ?>" class="admin_base_textarea_field" rows="<?= (!empty($rows)) ? $rows : '6' ?>"><?= $model->{$fieldName} ?></textarea>
</div>