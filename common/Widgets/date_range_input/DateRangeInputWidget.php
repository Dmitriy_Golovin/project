<?php

namespace Common\Widgets\date_range_input;

class DateRangeInputWidget
{
	public function renderBase(array $params)
	{
		return view('Common\Widgets\date_range_input\Views\date_range_base', $params);
	}
}