<div class="date_range_container">
	<div class="date_range_title_container"><?= $labelValue ?></div>
	<div class="date_range_field_container">
		<div class="date_range_from_container date_range_main_container">
			<label>от</label>
			<input type="text" name="<?= $fromFieldName ?>" class="admin_filter_date_range_field" value="<?= $from ?>">
			<img src="/img/icon_close_red.png" class="admin_filter_date_range_clear_icon not_active">
			<img src="/img/icon_calendar.png" class="admin_filter_date_range_icon">
		</div>
		<div class="date_range_to_container date_range_main_container">
			<label>до</label>
			<input type="text" name="<?= $toFieldName ?>" class="admin_filter_date_range_field" value="<?= $to ?>">
			<img src="/img/icon_close_red.png" class="admin_filter_date_range_clear_icon not_active">
			<img src="/img/icon_calendar.png" class="admin_filter_date_range_icon">
		</div>
	</div>
</div>