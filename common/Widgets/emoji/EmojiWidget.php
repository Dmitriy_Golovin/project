<?php

namespace Common\Widgets\emoji;

class EmojiWidget
{
	public function render(array $params = [])
	{
		return view('Common\Widgets\emoji\Views\emoji', $params);
	}
}