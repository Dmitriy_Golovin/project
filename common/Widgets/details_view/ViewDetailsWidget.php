<?php

namespace Common\Widgets\details_view;

class ViewDetailsWidget
{
	public $result;
	public $breadCrumbs;
	public $title;
	public $actions = [];
	public $items = [];
	public $model;
	public $supportElement = [];

	public function __construct(array $params)
	{
		$this->result = $params['result'];
		$this->breadCrumbs = (!empty($params['breadCrumbs']))
			? $params['breadCrumbs']
			: '';
		$this->title = $params['title'];
		$this->items = $params['items'];
		$this->actions = $params['actions'];
		$this->model = $params['model'];
		$this->supportElement = (!empty($params['supportElement']))
			? $params['supportElement']
			: '';

		$this->run();
	}

	public function run()
	{
		$title = $this->getTitle();
		$action = $this->getActions();
		$listData = $this->getListData();

		echo '<div class="admin_details_box">'
			. $this->breadCrumbs . $title . $action . $listData
			. '</div>';
	}

	public function getTitle()
	{
		return '<h2 class="admin_list_title">' . $this->title . '</h2>';
	}

	public function getActions()
	{
		if (empty($this->actions)) {
			return '';
		}

		$result = '<div class="admin_list_action_block">';

		foreach ($this->actions as $action) {
			$result .= self::prepareActionItem($action);
		}

		$result .= '</div>';

		return $result;
	}

	public function getListData()
	{
		$result = '<div class="admin_view_content_block">';

		foreach ($this->items as $item) {
			$label = (is_array($item))
				? $item['label']
				: $item;

			$result .= '<div class="admin_view_row">';
			$result .= '<div class="admin_view_row_label">' . $this->model::getLabels()[$label] . '</div>';

			if (is_array($item)) {
				if (!empty($item['value'])) {
					$value = (is_callable($item['value']))
						? $item['value']($this->result)
						: $item['value'];
				}
			} else {
				$value = $this->result->{$label};
			}

			if (is_null($value)) {
				$value = '<span style="font-style: italic; font-size: 11pt; color: #848484">Не задано</span>';
			}

			$result .= '<div class="admin_view_row_value">' . $value . '</div>';

			$result .= '</div>';
		}

		$result .= '</div>';

		return $result;
	}

	public static function prepareActionItem($action)
	{
		if (empty($action)) {
			return '';
		}

		$result = '';

		if (!empty($action['url'])) {
			$result .= '<a href="' . $action['url'] . '">';
		}

		$result .= '<span class="' . $action['cssClass'] . '">'
			. $action['label'] . '</span>';

		if (!empty($action['url'])) {
			$result .= '</a>';
		}

		return $result;
	}
}