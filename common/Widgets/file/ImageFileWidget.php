<?php

namespace Common\Widgets\file;

class ImageFileWidget
{
	public function renderInMessage(array $params = [])
	{
		return view('Common\Widgets\file\Views\image\image_file_in_message', $params);
	}

	public function renderTemplateForLoad(array $params = [])
	{
		return view('Common\Widgets\file\Views\image\image_file_template_load', $params);
	}

	public function renderPreview(array $params = [])
	{
		return view('Common\Widgets\file\Views\image\image_file_preview', $params);
	}

	public function renderInModal(array $params = [])
	{
		return view('Common\Widgets\file\Views\image\image_file_modal', $params);
	}

	public function renderSettings(array $params = [])
	{
		return view('Common\Widgets\file\Views\image\image_file_settings', $params);
	}

	public function renderAdminPreview(array $params = [])
	{
		return view('Common\Widgets\file\Views\image\image_file_admin_preview', $params);
	}
}