<?php

namespace Common\Widgets\file;

class DeletedFileWidget
{
	public function render(array $params = [])
	{
		return view('Common\Widgets\file\Views\deleted\deleted', $params);
	}
}