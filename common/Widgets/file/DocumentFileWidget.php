<?php

namespace Common\Widgets\file;

class DocumentFileWidget
{
	public function renderInMessage(array $params = [])
	{
		return view('Common\Widgets\file\Views\document\document_file_in_message', $params);
	}

	public function renderTemplateForLoad(array $params = [])
	{
		return view('Common\Widgets\file\Views\document\document_file_template', $params);
	}

	public function render(array $params = [])
	{
		return view('Common\Widgets\file\Views\document\document_file', $params);
	}

	// public function renderSettings(array $params = [])
	// {
	// 	return view('Common\Widgets\file\Views\document\document_file', $params);
	// }
}