<?php

use Common\Models\FileModel;

?>

<div class="image_file_template_container">
	<div class="image_file_container_template main_file_image_container image_template_container thumbnail_image_container" data-file_type="<?= FileModel::TYPE_IMAGE ?>">
		<img class="image_file" src="">
		<div class="image_template_delete_file_container">
			<img class="image_template_delete_file_icon" src="/img/close_slide_modal.png">
		</div>
	</div>
</div>