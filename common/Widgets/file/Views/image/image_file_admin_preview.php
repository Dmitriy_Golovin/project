<div class="image_file_container_admin_preview main_file_image_container image_preview_container thumbnail_image_container<?= (!empty($cssClass)) ? $cssClass : '' ?>" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>" data-load_type="exist">
	<img class="image_file" src="<?= $file->url ?>">
	<?php if ($deleteBlock) : ?>
		<div class="image_template_delete_file_container">
			<img class="image_template_delete_file_icon" src="/img/close_slide_modal.png">
		</div>
	<?php endif ?>
</div>