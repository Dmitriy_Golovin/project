<div class="file_container_in_message main_file_image_container" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>">
	<div class="image_file_in_message_block">
		<img src="<?= $file->url ?>" class="image_file_in_message" style="height: <?= $file->preview_height ?>px;">
	</div>
</div>