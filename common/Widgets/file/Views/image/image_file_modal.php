<div class="image_file_container_slider main_file_image_container" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>" data-rowNum="<?= $file->rowNum ?>" data-slider_position="<?= $file->position ?>">
	<img src="<?= $file->url ?>" class="image_file_slider">
	<div class="image_date_block">
		<?= lang('Main.Added') . ' ' . $file->date['date'] . ' ' . $file->date['time'] ?>

		<?php if ($file->selfFile) : ?>

			<div class="dot_menu_container">
				<ul class="file_menu_list">
					<li><a href="/file/settings/<?= $file->file_id ?>"><?= lang('Main.settings') ?></a></li>

					<?php if ($file->canDelete) : ?>

						<li class="delete_file_button"><?= lang('Main.delete') ?></li>

					<?php endif ?>

				</ul>
				<img class="dot_menu_container_pic" src="/img/menu_dot_icon.png">
			</div>

		<?php endif ?>
	</div>

	<?php if (!empty($file->entityData)) : ?>

		<div class="image_entity_block">
			<span><?= $file->entityData['entity'] ?>: </span>
			<a href="<?= $file->entityData['href'] ?>"><?= $file->entityData['name'] ?></a>
		</div>

	<?php endif ?>
</div>