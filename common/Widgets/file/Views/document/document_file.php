<?php

use Common\Models\FileModel;

$dataRowNum = (isset($file->rowNum))
	? 'data-rowNum="' . $file->rowNum . '"'
	: '';

?>

<div class="document_file_container_preview main_file_document_container" data-file_id="<?= $file->file_id ?>" data-file_type="<?= FileModel::TYPE_DOCUMENT ?>" <?= $dataRowNum ?>>
	<div class="document_file_template_block">
		<div class="document_icon_block_template">
			<img class="document_icon_el_template" src="/img/document_icon.png">
		</div>
		<div class="document_content_block">
			<a class="document_download_anchor" href="<?= $file->url ?>">
				<div class="document_name_block_template"><?= $file->original_name ?></div>
			</a>

			<?php if (!empty($file->documentDate)) : ?>

				<div class="document_date_block">
					<?= lang('Main.Added') . ' ' . $file->documentDate['date'] . ' ' . $file->documentDate['time'] ?>
				</div>

			<?php endif ?>

			<?php if (!empty($file->entityData)) : ?>

				<div class="document_entity_block">
					<span><?= $file->entityData['entity'] ?>: </span><a href="<?= $file->entityData['href'] ?>"><?= $file->entityData['name'] ?></a>
				</div>

			<?php endif ?>

		</div>
	</div>
	<?php if ($deleteBlock) : ?>
		<div class="document_template_delete_file_container">
			<img class="document_template_delete_file_icon" src="/img/close_slide_modal.png">
		</div>
	<?php endif ?>

	<?php if (isset($file->selfFile) && $file->selfFile) : ?>

		<div class="dot_menu_container">
			<ul class="file_menu_list">
				<li><a href="/file/settings/<?= $file->file_id ?>"><?= lang('Main.settings') ?></a></li>

				<?php if ($file->canDelete) : ?>

					<li class="delete_file_button"><?= lang('Main.delete') ?></li>

				<?php endif ?>

			</ul>
			<img class="dot_menu_container_pic" src="/img/menu_dot_icon.png">
		</div>

	<?php endif ?>

</div>