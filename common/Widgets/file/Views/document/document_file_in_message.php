<div class="file_container_in_message main_file_document_container document_file_in_message_container <?= isset($containerCssBorderBottom) ? $containerCssBorderBottom : '' ?>" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>">
	<a class="document_download_anchor" href="<?= $file->url ?>">
		<div class="document_file_message_block">
			<div class="document_icon_block_in_message">
				<img class="document_icon_el_in_message" src="/img/document_icon.png">
			</div>
			<div class="document_name_block_in_message"><?= $file->name ?></div>
		</div>
	</a>
</div>