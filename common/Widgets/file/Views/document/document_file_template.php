<?php

use Common\Models\FileModel;

?>

<div class="document_file_template_container">
	<div class="document_file_container_template main_file_document_container" data-file_type="<?= FileModel::TYPE_DOCUMENT ?>">
		<div class="document_file_template_block">
			<div class="document_icon_block_template">
				<img class="document_icon_el_template" src="/img/document_icon.png">
			</div>
			<div class="document_name_block_template"></div>
		</div>
		<div class="document_template_delete_file_container">
			<img class="document_template_delete_file_icon" src="/img/close_slide_modal.png">
		</div>
	</div>
</div>