<?php

use Common\Models\FileModel;

?>

<div class="video_file_template_container">
	<div class="file_container_preload main_file_video_container video_file_in_message_container" data-file_type="<?= FileModel::TYPE_VIDEO ?>" media_init="0">
		<div class="video_template_delete_file_container">
			<img class="video_template_delete_file_icon" src="/img/close_slide_modal.png">
		</div>
		<div class="video_in_message_block">
			<video src="" controlslist="nodownload" class="video_el media_el"></video>
			<div class="video_control_media_block">
				<div class="video_play_block">
					<img class="video_play_icon" src="/img/play_video_icon.png" data-action_media_icon="play">
				</div>
				<div class="video_state_block">
					<div class="video_time_bar">
						<div class="video_time_bar_inner"></div>
					</div>
				</div>
				<div class="video_current_time_block"></div>
				<div class="fullscreen_video_block">
					<img class="fullscreen_video_icon" src="/img/fullscreen_video_icon.png" data-action_video_fullsreen_icon="not_full">
				</div>
			</div>
		</div>
	</div>
</div>


