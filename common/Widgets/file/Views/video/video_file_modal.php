<div class="file_container_in_modal main_file_video_container video_file_in_modal_container" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>" media_init="0">
	<div class="video_in_message_block">
		<video src="<?= $file->url ?>" controlslist="nodownload" class="video_el media_el"></video>
		<div class="video_control_media_block">
			<div class="video_play_block">
				<img class="video_play_icon" src="/img/play_video_icon.png" data-action_media_icon="play">
			</div>
			<div class="video_state_block">
				<div class="video_time_bar">
					<div class="video_time_bar_inner"></div>
				</div>
			</div>
			<div class="video_current_time_block"></div>
			<div class="fullscreen_video_block">
				<img class="fullscreen_video_icon" src="/img/fullscreen_video_icon.png" data-action_video_fullsreen_icon="not_full">
			</div>
		</div>
	</div>
	<div class="video_date_block">
		<?= lang('Main.Added') . ' ' . $file->date['date'] . ' ' . $file->date['time'] ?>

		<?php if ($file->selfFile) : ?>

			<div class="dot_menu_container">
				<ul class="file_menu_list">
					<li><a href="/file/settings/<?= $file->file_id ?>"><?= lang('Main.settings') ?></a></li>

					<?php if ($file->canDelete) : ?>

						<li class="delete_file_button"><?= lang('Main.delete') ?></li>

					<?php endif ?>

				</ul>
				<img class="dot_menu_container_pic" src="/img/menu_dot_icon.png">
			</div>

		<?php endif ?>
	</div>

	<?php if (!empty($file->entityData)) : ?>

		<div class="video_entity_block">
			<span><?= $file->entityData['entity'] ?>: </span><a href="<?= $file->entityData['href'] ?>"><?= $file->entityData['name'] ?></a>
		</div>

	<?php endif ?>

</div>