<?php

$dataRowNum = (isset($file->rowNum))
	? 'data-rowNum="' . $file->rowNum . '"'
	: '';

?>

<div class="file_container_preview main_file_video_container video_file_preview_container" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>" <?= $dataRowNum ?>>
	<?php if ($deleteBlock) : ?>
		<div class="video_template_delete_file_container">
			<img class="video_template_delete_file_icon" src="/img/close_slide_modal.png">
		</div>
	<?php endif ?>
	<div class="video_preview_block">
		<video src="<?= $file->url ?>" controlslist="nodownload" class="video_el media_el" <?= (isset($videoElMaxheight)) ? 'style="max-height:' . $videoElMaxheight . '"' : '' ?>></video>
		<div class="video_preview_play_icon_container">
			<img src="/img/play_video_icon.png" class="video_preview_play_icon">
		</div>
	</div>
	<?php if ($nameBlock) : ?>
		<div class="video_name_block"><?= $file->name ?></div>
	<?php endif ?>
</div>