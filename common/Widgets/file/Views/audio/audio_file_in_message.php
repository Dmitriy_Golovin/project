<div class="file_container_in_message main_file_audio_container audio_file_in_message_container <?= isset($containerCssBorderBottom) ? $containerCssBorderBottom : '' ?>" data-file_id="<?= $file->file_id ?>" data-file_type="<?= $file->type ?>" media_init="0">
	<div class="select_audio_message_block">
		<div class="audio_play_block">
			<img src="/img/play_icon.png" data-action_media_icon="play" class="audio_play_icon">
		</div>
		<div class="audio_content_block">
			<div class="audio_name_block" data-file_url="<?= $file->url ?>"><?= $file->name ?></div>
			<div class="audio_state_block">
				<div class="audio_time_bar">
					<div class="audio_time_bar_inner"></div>
				</div>
			</div>
			<div class="audio_current_time_block"></div>
		</div>
		<audio class="media_el" preload="metadata" src="<?= $file->url ?>"></audio>
	</div>
</div>