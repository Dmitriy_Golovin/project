<?php

namespace Common\Widgets\file;

class AudioFileWidget
{
	public function renderInMessage(array $params = [])
	{
		return view('Common\Widgets\file\Views\audio\audio_file_in_message', $params);
	}
}