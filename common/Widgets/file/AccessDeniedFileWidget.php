<?php

namespace Common\Widgets\file;

class AccessDeniedFileWidget
{
	public function render(array $params = [])
	{
		return view('Common\Widgets\file\Views\accessDenied\access-denied', $params);
	}
}