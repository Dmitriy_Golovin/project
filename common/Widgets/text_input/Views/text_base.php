<div class="text_container_base">
	<div class="text_title_container"><?= $model->labels[$fieldName] ?></div>
	<input type="<?= (!empty($type)) ? $type : 'text' ?>" name="<?= $fieldName ?>" class="admin_base_field" value="<?= $model->{$fieldName} ?>">
</div>