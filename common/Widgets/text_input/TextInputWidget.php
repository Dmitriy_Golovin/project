<?php

namespace Common\Widgets\text_input;

class TextInputWidget
{
	public function renderBase(array $params)
	{
		return view('Common\Widgets\text_input\Views\text_base', $params);
	}

	public function renderFilter(array $params)
	{
		return view('Common\Widgets\text_input\Views\text_filter', $params);
	}
}