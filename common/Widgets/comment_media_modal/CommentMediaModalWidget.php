<?php

namespace Common\Widgets\comment_media_modal;

class CommentMediaModalWidget
{
	public function renderMediaModal(array $params = [])
	{
		return view('Common\Widgets\comment_media_modal\Views\comment_media_modal', $params);
	}
}