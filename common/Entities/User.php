<?php

namespace Common\Entities;

use CodeIgniter\Entity\Entity;

class User extends Entity
{
    public $labels = [
        'user_id' => 'ID',
        'first_name' => 'Имя',
        'last_name' => 'Фамилия',
        'country' => 'Страна',
        'city' => 'Город',
        'country_id' => 'Страна',
        'city_id' => 'Город',
        'email' => 'Email',
        'password' => 'Пароль',
        'repeat_password' => 'Повторите пароль',
        'about_self' => 'О себе',
        'accept_terms' => 'Условия приняты',
        'path_to_ava' => 'Аватар',
        'veryfication_token' => 'Статус профиля',
        'is_blocked' => 'Статус',
        'role' => 'Роль',
        'created_at' => 'Дата регистрации',
        'deleted_at' => 'Профиль удален',
        'userDeleted' => 'Профиль удален',
    ];

    protected $attributes = [
        'first_name' => null,
        'last_name' => null,
        'city_id' => null,
        'country_id' => null,
        'about_self' => null,
        'email' => null,
        'password' => null,
        'accept_terms' => null,
        'veryfication_token' => null,
        'number_phone' => null,
        'file_id' => null,
        'token_change_safety_data' => null,
        'is_blocked' => null,
        'role' => null,
        'restore_password_token' => null,
    ];

    protected $datamap = [];

    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
}
