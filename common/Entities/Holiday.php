<?php

namespace Common\Entities;

use CodeIgniter\Entity\Entity;

class Holiday extends Entity
{
    public $labels = [
        'holiday_id' => 'ID',
        'holiday_day' => 'Название',
        'description' => 'Описание',
        'holiday_date' => 'Дата',
        'country' => 'Страна',
        'country_id' => 'Страна',
        'link' => 'Источник',
        'file_id' => 'Изображение',
        'file' => 'Изображение',
        'file_link' => 'Источник изображения',
        'created_at' => 'Дата создания',
        'deleted_at' => 'Профиль удален',
    ];

    protected $attributes = [
        'holiday_day' => null,
        'description' => null,
        'holiday_date' => null,
        'fileUrl' => null,
        'country_id' => null,
        'file_id' => null,
        'file_link' => '',
        'link' => '',
    ];

    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
}
