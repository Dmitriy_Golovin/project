<?php

namespace Common\Entities;

use CodeIgniter\Entity\Entity;

class File extends Entity
{
    protected $datamap = [];
    protected $dates   = ['created_at', 'updated_at', 'deleted_at'];
    protected $casts   = [];
}
