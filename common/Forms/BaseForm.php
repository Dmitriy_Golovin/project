<?php

namespace Common\Forms;

use CodeIgniter\Validation\ValidationInterface;
use CodeIgniter\View\RendererInterface;
use Config\Services;

class BaseForm
{
	/**
	 * Holds the current values of all class vars.
	 *
	 * @var array
	 */
	protected $attributes = [];

	/**
	 * Our validator instance.
	 *
	 * @var ValidationInterface
	 */
	protected $validation;

	/**
	 * Rules used to validate data in insert, update, and save methods.
	 * The array must match the format of data passed to the Validation
	 * library.
	 *
	 * @var array|string
	 */
	protected $validationRules = [];

	/**
	 * Contains any custom error messages to be
	 * used during data validation.
	 *
	 * @var array
	 */
	protected $validationMessages = [];

	/**
	 * Whether rules should be removed that do not exist
	 * in the passed in data. Used between inserts/updates.
	 *
	 * @var boolean
	 */
	protected $cleanValidationRules = true;

	/**
	 * The view renderer used to render error messages.
	 *
	 * @var RendererInterface
	 */
	protected $view;

	protected $errors = [];

	public $scenario = 'default';

	/**
	 * BaseForm constructor.
	 *
	 * @param ValidationInterface|null $validation Validation
	 */
	public function __construct(RendererInterface $view = null, ValidationInterface $validation = null)
	{
		$this->view = $view ?? Services::renderer(__DIR__ . '/Views', null, true);

		$this->validation = $validation ?? Services::validation(null, false);
	}

	/**
	 * Takes an array of key/value pairs and sets them as
	 * class properties, using any `setCamelCasedProperty()` methods
	 * that may or may not exist.
	 *
	 * @param array $data
	 *
	 * @return $this
	 */
	public function fill(array $data = null)
	{
		if (!is_array($data)) {
			return $this;
		}

		$this->setAttributes($data);

		foreach ($data as $key => $value) {
			if (property_exists(get_class($this), $key)) {
				$this->$key = $value;
			}
		}
	}

	public function getAttributes()
	{
		return $this->attributes;
	}

	public function setAttribute($attribute, $value)
	{
		$this->attributes[$attribute] = $value;
		$this->$attribute = $value;
	}

	/**
	 * Validate the data against the validation rules (or the validation group)
	 * specified in the class property, $validationRules.
	 *
	 * @param array|object $data Data
	 *
	 * @return boolean
	 */
	public function validate(): bool
	{
		$data = $this->attributes;
		$rules = $this->getValidationRules();

		if (empty($rules) || empty($data)) {
			return true;
		}

		//Validation requires array, so cast away.
		if (is_object($data)) {
			$data = (array) $data;
		}

		$rules = $this->cleanValidationRules ? $this->cleanValidationRules($rules, $data) : $rules;

		// If no data existed that needs validation
		// our job is done here.
		if (empty($rules)) {
			return true;
		}

		$result = $this->validation->setRules($rules, $this->validationMessages)->run($data);
		$this->errors = $this->validation->getErrors();

		return $result;
	}

	/**
	 * Returns the model's defined validation rules so that they
	 * can be used elsewhere, if needed.
	 *
	 * @param array $options Options
	 *
	 * @return array
	 */
	public function getValidationRules(array $options = []): array
	{
		$rules = $this->validationRules;

		// ValidationRules can be either a string, which is the group name,
		// or an array of rules.
		if (is_string($rules)) {
			// @phpstan-ignore-next-line
			$rules = $this->validation->loadRuleGroup($rules);
		}

		if (isset($options['except'])) {
			$rules = array_diff_key($rules, array_flip($options['except']));
		} elseif (isset($options['only'])) {
			$rules = array_intersect_key($rules, array_flip($options['only']));
		}

		return $rules;
	}

	/**
	 * Returns the model's define validation messages so they
	 * can be used elsewhere, if needed.
	 *
	 * @return array
	 */
	public function getValidationMessages(): array
	{
		return $this->validationMessages;
	}

	/**
	 * Removes any rules that apply to fields that have not been set
	 * currently so that rules don't block updating when only updating
	 * a partial row.
	 *
	 * @param array      $rules Array containing field name and rule
	 * @param array|null $data  Data
	 *
	 * @return array
	 */
	protected function cleanValidationRules(array $rules, array $data = null): array
	{
		if (empty($data))
		{
			return [];
		}

		foreach ($rules as $field => $rule)
		{
			if (! array_key_exists($field, $data))
			{
				unset($rules[$field]);
			}
		}

		return $rules;
	}

	/**
	 * Grabs the last error(s) that occurred. If data was validated,
	 * it will first check for errors there, otherwise will try to
	 * grab the last error from the Database connection.
	 *
	 * @return array|null
	 */
	public function getErrors()
	{
		return $this->errors;
	}

	public function getError($attribute)
	{
		if (!empty($this->errors[$attribute])) {
			return $this->errors[$attribute];
		}

		return null;
	}

	public function getFirstError()
	{
		if (!empty($this->errors)) {
			$firstError = current($this->errors);
            if (!empty($firstError)) {
                return $firstError;
            }
		}

		return null;
	}

	public function hasErrors()
	{
		if (!empty($this->errors)) {
			return true;
		}

		return false;
	}

	public function hasError($attribute)
	{
		return !empty($this->errors[$attribute]) ? true : false;
	}

	/**
	 * Sets the error for a specific field. Used by custom validation methods.
	 *
	 * @param string $field
	 * @param string $error
	 */
	public function setError(string $field, string $error)
	{
		$this->errors[$field] = $error;
	}

	/**
	 * Set raw data array without any mutations
	 *
	 * @param  array $data
	 */
	protected function setAttributes(array $data)
	{
		foreach ($data as $key => $value) {
			if (property_exists(get_class($this), $key)) {
				$this->attributes[$key] = $value;
			}
		}
	}

}