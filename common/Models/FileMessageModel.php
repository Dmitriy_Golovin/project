<?php

namespace Common\Models;

use CodeIgniter\Model;

class FileMessageModel extends Model
{
    protected $table = 'file_message';
    protected $primaryKey = 'file_message_id';

    protected $useAutoIncrement = true;

    protected $returnType = 'object';
    protected $useSoftDeletes = false;

    protected $allowedFields = ['message_id', 'file_id'];

    protected $useTimestamps = true;
    protected $createdField = 'created_at';
    protected $updatedField = 'updated_at';
    protected $deletedField = 'deleted_at';

    protected $dateFormat = 'int';

    protected $validationRules    = [];
    
    protected $validationMessages = [];
    protected $skipValidation     = false;
    protected $cleanValidationRules = false;

    public static function table()
    {
        return db_connect()->prefixTable('file_message');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }
}