<?php

namespace Common\Models;

use CodeIgniter\Model;

class UserSettingModel extends Model
{
    const HOLIDAYS_NOT_ACTIVE = 0;
    const HOLIDAYS_ACTIVE = 1;

    const MY_EVENTS_NOT_ACTIVE = 0;
    const MY_EVENTS_ACTIVE = 1;

    const MY_MEETINGS_NOT_ACTIVE = 0;
    const MY_MEETINGS_ACTIVE = 1;

    const MY_TASKS_NOT_ACTIVE = 0;
    const MY_TASKS_ACTIVE = 1;

    const FRIENDS_EVENTS_NOT_ACTIVE = 0;
    const FRIENDS_EVENTS_ACTIVE = 1;

    protected $DBGroup          = 'default';
    protected $table            = 'user_setting';
    protected $primaryKey       = 'user_setting_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'user_id',
        'holidays',
        'my_events',
        'my_meetings',
        'my_tasks',
        'friends_events',
        'work_week_start',
        'work_days_amount',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('user_setting');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public function getUserSettings($user_id)
    {
        return $this
            ->where('user_id', $user_id)
            ->first();
    }
}
