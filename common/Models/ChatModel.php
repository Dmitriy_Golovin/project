<?php

namespace Common\Models;

use CodeIgniter\Model;
use Common\Models\FileModel;
use Common\Models\ChatMemberModel;
use Common\Models\ChatMessageModel;
use Common\Models\UserModel;

class ChatModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'chat';
    protected $primaryKey       = 'chat_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['file_id', 'pinned_message_id', 'title'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('chat');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public function getChatData($chatId, $memberId)
    {
        $fileModel = new FileModel();
        $chatMessageModel = new ChatMessageModel();
        $chatTableName = $this->getTableName();
        $fileTableName = $fileModel->getTableName();
        $chatMessageTableName = $chatMessageModel->getTableName();

        $chat = $this->select($chatTableName . '.*')
            ->select('url as chatAva, type')
            ->select('count(' . $chatMessageTableName . '.chat_message_id) as importantMessageCount')
            ->join($fileTableName, $fileTableName . '.file_id = ' .$chatTableName . '.file_id', 'left')
            ->join($chatMessageTableName, $chatMessageTableName . '.chat_id = ' . $chatTableName . '.chat_id and json_contains(important_chat_member_ids, "' . $memberId . '", "$") = 1 and json_contains(deleted_chat_member_ids, "' . $memberId . '", "$") = 0', 'left')
            ->where($chatTableName . '.chat_id', $chatId)
            ->first();

        $chat->chatAva = !empty($chat->chatAva) ? FileModel::preparePathToUserFile($chat->chatAva, $chat->type) : null;
        $chat->chatMember = [];
        $chatMemberModel = new ChatMemberModel();
        $userModel = new UserModel();
        $chatMemberTableName = $chatMemberModel->getTableName();
        $userTableName = $userModel->getTableName();

        $chatMemberQuery = $chatMemberModel
            ->select($chatMemberTableName . '.*')
            ->select('concat(first_name, " ", last_name) as fullName, url as userAva')
            ->join($userTableName, $userTableName . '.user_id = ' . $chatMemberTableName . '.user_id')
            ->join($fileTableName, $fileTableName . '.file_id = ' . $userTableName . '.file_id', 'left')
            ->where($chatMemberTableName . '.chat_id', $chatId)
            ->where($chatMemberTableName . '.deleted_at', null)
            ->orderBy('role', 'asc')
            ->get();

        foreach ($chatMemberQuery->getResultObject() as $member) {
            $member->userAva = !empty($member->userAva)
                ? FileModel::preparePathToAva($member->userAva)
                : FileModel::DEFAULT_AVA_URL;
            $chat->chatMember[] = $member;
        }

        return $chat;
    }
}
