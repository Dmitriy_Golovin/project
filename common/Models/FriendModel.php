<?php

namespace Common\Models;

use CodeIgniter\Model;

class FriendModel extends Model
{
    const NEW_SUBSCRIBER_STATUS = 0;
    const SUBSCRIBER_STATUS = 1;
    const FRIEND_STATUS = 2;

    const TYPE_FRIENDS = 1;
    const TYPE_SUBSCRIBERS = 2;
    const TYPE_SUBSCRIPTIONS = 3;

    const WS_ACTION_SUBSCRIBE = 'subscribe';
    const WS_ACTION_UNSUBSCRIBE = 'unsubscribe';
    const WS_ACTION_ACCEPT_FRIENDSHIP = 'acceptFriendship';
    const WS_ACTION_REJECT_FRIENDSHIP = 'rejectFriendship';
    const WS_ACTION_REMOVE_FRIEND = 'removeFriend';

    const RELATION_NO_RELATION = 'no_relation';
    const RELATION_YOU_ARE_SUBSCRIBED = 'you_are_subscribed';
    const RELATION_SUBSCRIBER = 'subscriber';
    const RELATION_NEW_SUBSCRIBER = 'new_subscriber';
    const RELATION_FRIEND = 'friend';

    protected $DBGroup          = 'default';
    protected $table            = 'friend';
    protected $primaryKey       = 'friend_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['id_request_recipient', 'id_request_sender', 'status'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('friend');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public static function getNoLinksDataLabel($locale = '')
    {
        return [
            'ws_tab_sync_to_do' => self::WS_ACTION_SUBSCRIBE,
            'button_value' => lang('Main.Add as friend', [], $locale),
            'main_profile_button_css' => ' profile_button',
            'relation_with_user' => '',
            'user_relations' => self::RELATION_NO_RELATION,
        ];
    }

    public function getUserRelationships($listId, $selfId, $locale = '')
    {
        $resultList = [];

        if (empty($listId)) {
            return $resultList;
        }
        
        $query = $this->select('friend_id, id_request_sender, id_request_recipient, status')
            ->groupStart()
                ->where('id_request_sender', $selfId)
                ->whereIn('id_request_recipient', $listId)
            ->groupEnd()
            ->orGroupStart()
                ->where('id_request_recipient', $selfId)
                ->whereIn('id_request_sender', $listId)
            ->groupEnd()
            ->get();
        
        foreach ($query->getResultObject() as $entity) {
            if ($entity->id_request_sender == $selfId && ($entity->status == 0 || $entity->status == 1)) {
                $resultList[$entity->id_request_recipient] = [
                    'friend_id' => $entity->friend_id,
                    'ws_tab_sync_to_do' => self::WS_ACTION_UNSUBSCRIBE,
                    'button_value' => lang('Main.Unsubscribe', [], $locale),
                    'main_profile_button_css' => ' unsubscribe_button',
                    'relation_with_user' => lang('Friend.you subscribed', [], $locale),
                    'user_relations' => self::RELATION_YOU_ARE_SUBSCRIBED
                ];
            } else if ($entity->id_request_recipient == $selfId && $entity->status == 1) {
                $resultList[$entity->id_request_sender] = [
                    'friend_id' => $entity->friend_id,
                    'ws_tab_sync_to_do' => self::WS_ACTION_ACCEPT_FRIENDSHIP,
                    'button_value' => lang('Main.Add as friend', [], $locale),
                    'main_profile_button_css' => ' profile_button',
                    'relation_with_user' => lang('Friend.user subscribed', [], $locale),
                    'user_relations' => self::RELATION_SUBSCRIBER,
                ];
            } else if ($entity->id_request_recipient == $selfId && $entity->status == 0) {
                $resultList[$entity->id_request_sender] = [
                    'friend_id' => $entity->friend_id,
                    'ws_tab_sync_to_do' => self::WS_ACTION_ACCEPT_FRIENDSHIP,
                    'ws_tab_sync_to_do_reject' => self::WS_ACTION_REJECT_FRIENDSHIP,
                    'button_value' => lang('Main.Add as friend', [], $locale),
                    'button_value_reject' => lang('Friend.Leave in subscribers', [], $locale),
                    'main_profile_button_css' => ' profile_button',
                    'relation_with_user' => lang('Friend.user subscribed', [], $locale),
                    'user_relations' => self::RELATION_NEW_SUBSCRIBER,
                ];
            } else if ($entity->id_request_sender == $selfId && $entity->status == 2) {
                $resultList[$entity->id_request_recipient] = [
                    'friend_id' => $entity->friend_id,
                    'ws_tab_sync_to_do' => self::WS_ACTION_REMOVE_FRIEND,
                    'button_value' => lang('Main.Remove from friends', [], $locale),
                    'main_profile_button_css' => ' delete_friend_button',
                    'relation_with_user' => lang('Friend.you are friends', [], $locale),
                    'user_relations' => self::RELATION_FRIEND,
                ];
            } else if ($entity->id_request_recipient == $selfId && $entity->status == 2) {
                $resultList[$entity->id_request_sender] = [
                    'friend_id' => $entity->friend_id,
                    'ws_tab_sync_to_do' => self::WS_ACTION_REMOVE_FRIEND,
                    'button_value' => lang('Main.Remove from friends', [], $locale),
                    'main_profile_button_css' => ' delete_friend_button',
                    'relation_with_user' => lang('Friend.you are friends', [], $locale),
                    'user_relations' => self::RELATION_FRIEND,
                ];
            }
        }

        return $resultList;
    }

    public function getAmountOfUsersInSpecificRelationships($user_id)
    {
        $friendQuery = $this
            ->select('count(*)')
            ->join(UserModel::table() . ' as userTableRecipient', 'userTableRecipient.user_id = ' . self::table() . '.id_request_recipient')
            ->join(UserModel::table() . ' as userTableSender', 'userTableSender.user_id = ' . self::table() . '.id_request_sender')
            ->groupStart()
                ->where('id_request_recipient', $user_id)
                ->where('status', self::FRIEND_STATUS)
                ->where('userTableSender.deleted_at', null)
                ->where('userTableSender.is_blocked', UserModel::STATUS_NOT_BLOCKED)
                ->where('userTableSender.veryfication_token', null)
            ->groupEnd()
            ->orGroupStart()
                ->where('id_request_sender', $user_id)
                ->where('status', self::FRIEND_STATUS)
                ->where('userTableRecipient.deleted_at', null)
                ->where('userTableRecipient.is_blocked', UserModel::STATUS_NOT_BLOCKED)
                ->where('userTableRecipient.veryfication_token', null)
            ->groupEnd()
            ->getCompiledSelect();

        $subscriberQuery = $this
            ->select('count(*)')
            ->join(UserModel::table() . ' as userTableSender', 'userTableSender.user_id = ' . self::table() . '.id_request_sender')
            ->where('id_request_recipient', $user_id)
            ->whereIn('status', [self::NEW_SUBSCRIBER_STATUS, self::SUBSCRIBER_STATUS])
            ->where('userTableSender.deleted_at', null)
            ->where('userTableSender.is_blocked', UserModel::STATUS_NOT_BLOCKED)
            ->where('userTableSender.veryfication_token', null)
            ->getCompiledSelect();

        $newSubscriberQuery = $this
            ->select('count(*)')
            ->join(UserModel::table() . ' as userTableSender', 'userTableSender.user_id = ' . self::table() . '.id_request_sender')
            ->where('id_request_recipient', $user_id)
            ->where('status', self::NEW_SUBSCRIBER_STATUS)
            ->where('userTableSender.deleted_at', null)
            ->where('userTableSender.is_blocked', UserModel::STATUS_NOT_BLOCKED)
            ->where('userTableSender.veryfication_token', null)
            ->getCompiledSelect();

        $subscriptionQuery = $this
            ->select('count(*)')
            ->join(UserModel::table() . ' as userTableRecipient', 'userTableRecipient.user_id = ' . self::table() . '.id_request_recipient')
            ->where('id_request_sender', $user_id)
            ->whereIn('status', [self::NEW_SUBSCRIBER_STATUS, self::SUBSCRIBER_STATUS])
            ->where('userTableRecipient.deleted_at', null)
            ->where('userTableRecipient.is_blocked', UserModel::STATUS_NOT_BLOCKED)
            ->where('userTableRecipient.veryfication_token', null)
            ->getCompiledSelect();
        
        $result = $this
            ->select('(' . $friendQuery . ') as friends')
            ->select('(' . $subscriberQuery . ') as subscribers')
            ->select('(' . $newSubscriberQuery . ') as new_subscriber')
            ->select('(' . $subscriptionQuery . ') as subscriptions')
            ->first();


        return (!is_null($result))
            ? $result
            : (object)[
               'friends' => 0, 
               'subscribers' => 0, 
               'new_subscriber' => 0, 
               'subscriptions' => 0, 
            ];
    }
}
