<?php

namespace Common\Models;

use CodeIgniter\Model;

class ChatMemberModel extends Model
{
    const ROLE_CREATER = 1;
    const ROLE_ADMIN = 2;
    const ROLE_MEMBER = 3;

    const ROLE_CREATER_LABEL = 'creater';
    const ROLE_MEMBER_LABEL = 'member';
    const ROLE_ADMIN_LABEL = 'admin';

    protected $DBGroup          = 'default';
    protected $table            = 'chat_member';
    protected $primaryKey       = 'chat_member_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = ['chat_id', 'user_id', 'role'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('chat_member');
    }

    public static function selectRoleLabels()
    {
        return [
            self::ROLE_MEMBER => lang('ChatMember.' . self::ROLE_MEMBER_LABEL),
            self::ROLE_ADMIN => lang('ChatMember.' . self::ROLE_ADMIN_LABEL),
        ];
    }

    public static function getRoleLabels()
    {
        return [
            self::ROLE_CREATER => lang('ChatMember.' . self::ROLE_CREATER_LABEL),
            self::ROLE_MEMBER => lang('ChatMember.' . self::ROLE_MEMBER_LABEL),
            self::ROLE_ADMIN => lang('ChatMember.' . self::ROLE_ADMIN_LABEL),
        ];
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public function getMemberList($chatId)
    {
        $result = [];

        $query = $this->where('chat_id', $chatId)->get();

        foreach ($query->getResultObject() as $item) {
            $result[] = $item;
        }

        return $result;
    }

    public function getMemberUserIdListWithoutSelf($chatId, $userId)
    {
        $result = [];

        $query = $this
            ->where('chat_id', $chatId)
            ->where('user_id !=', $userId)
            ->get();

        foreach ($query->getResultObject() as $item) {
            $result[] = $item->user_id;
        }

        return $result;
    }
}
