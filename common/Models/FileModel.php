<?php

namespace Common\Models;

use CodeIgniter\Model;
use Common\Models\PrivateFilePermissionModel;

class FileModel extends Model
{
	const DEFAULT_AVA_URL = '/img/no_ava.png';
	const DEFAULT_CHAT_AVA_URL = '/img/chat_icon.png';
	
	const TYPE_IMAGE = 1;
	const TYPE_AUDIO = 2;
	const TYPE_VIDEO = 3;
	const TYPE_DOCUMENT = 4;

	const TYPE_IMAGE_URL_LABEL = 'img';
	const TYPE_AUDIO_URL_LABEL = 'audio';
	const TYPE_VIDEO_URL_LABEL = 'video';
	const TYPE_DOCUMENT_URL_LABEL = 'document';
	
	const PUBLIC_STATUS = 1;
	const PRIVATE_STATUS = 2;

	const RELATED_ITEM_USER = 'user';
	const RELATED_ITEM_HOLIDAY = 'holiday';
	const RELATED_ITEM_ARTICLE_SECTION = 'article_section';
	const RELATED_ITEM_ARTICLE = 'article';

	const MIME_JPEG = 'image/jpeg';
	const MIME_PNG = 'image/png';
	const MIME_GIF = 'image/gif';

	protected $DBGroup              = 'default';
	protected $table                = 'file';
	protected $primaryKey           = 'file_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['type', 'related_item', 'item_id', 'url', 'privacy_status', 'mime_type', 'original_name', 'preview_height', 'preview_width', 'created_at', 'title', 'source'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'int';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = true;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = ['deleteFileFromDisk'];
	protected $afterDelete          = [];

	public static function table()
	{
		return db_connect()->prefixTable('file');
	}

	public function getTableName()
	{
		$db = db_connect();
		return $db->prefixTable($this->table);
	}

	public static function typeLabel() {
		return [
			self::TYPE_IMAGE => self::TYPE_IMAGE_URL_LABEL,
			self::TYPE_AUDIO => self::TYPE_AUDIO_URL_LABEL,
			self::TYPE_VIDEO => self::TYPE_VIDEO_URL_LABEL,
			self::TYPE_DOCUMENT => self::TYPE_DOCUMENT_URL_LABEL,
		];
	}

	public static function getPrivacyByEvent()
	{
		return [
			0 => self::PUBLIC_STATUS,
			1 => self::PRIVATE_STATUS,
		];
	}

	public static function getTypeByFileConfig()
	{
		$fileConfig = config('FileConfig');

		return [
			"$fileConfig->filePageTypeImage" => self::TYPE_IMAGE,
			"$fileConfig->filePageTypeVideo" => self::TYPE_VIDEO,
			"$fileConfig->filePageTypeDocument" => self::TYPE_DOCUMENT,
		];
	}

	public static function getPrivacyStatusLabel()
	{
		return [
			self::PUBLIC_STATUS => lang('File.public'),
			self::PRIVATE_STATUS => lang('File.private'),
		];
	}

	public static function getPrivacyStatusValueButton()
	{
		return [
			self::PUBLIC_STATUS => lang('File.make private'),
			self::PRIVATE_STATUS => lang('File.make public'),
		];
	}

	public static function getOppositePrivacyStatus()
	{
		return [
			self::PUBLIC_STATUS => self::PRIVATE_STATUS,
			self::PRIVATE_STATUS => self::PUBLIC_STATUS
		];
	}

	public static function getFileNameWithoutExec($name)
	{
		if (empty($name)) {
			return $name;
		}

		$dotPosition = strrpos($name, '.');

		if ($dotPosition !== false) {
			return substr($name, 0, $dotPosition);
		} else {
			return $name;
		}
	}

	public function getOriginalFileName($url) {
		$name = preg_replace('/(\d+)_([0-9a-z]+)_/', '', $url);
		return $name;
	}

	public static function getUploadFileDir($fileType) {
		$dir = WRITEPATH;

		if ($fileType == self::TYPE_IMAGE) {
			$dir = $dir . 'uploads/image';
		} else if ($fileType == self::TYPE_AUDIO) {
			$dir = $dir . 'uploads/audio';
		} else if ($fileType == self::TYPE_VIDEO) {
			$dir = $dir . 'uploads/video';
		} else if ($fileType == self::TYPE_DOCUMENT) {
			$dir = $dir . 'uploads/document';
		}

		if (!is_dir($dir)) {
			return $dir;
		}

		return $dir;
	}

	public static function createImageFromPath($imagePath, $mimeType)
	{
		if ($mimeType === self::MIME_JPEG) {
			return imagecreatefromjpeg($imagePath);
		} else if ($mimeType === self::MIME_PNG) {
			return imagecreatefrompng($imagePath);
		} else if ($mimeType === self::MIME_GIF) {
			return imagecreatefromgif($imagePath);
		}
	}

	public static function compressImage($imagePath, $mimeType)
	{
		if ($mimeType === self::MIME_JPEG) {
			$im = new \Imagick($imagePath);
			$im->setImageCompressionQuality(50);
			$im->writeImage($imagePath);
			// $im = imagecreatefromjpeg($imagePath);
			// imagejpeg($im, $imagePath, 50);
		} else if ($mimeType === self::MIME_PNG) {
			$im = imagecreatefrompng($imagePath);
			imagepng($im, $imagePath, 5);
		} else if ($mimeType === self::MIME_GIF) {
			$im = imagecreatefromgif($imagePath);
			imagetruecolortopalette($im, true, 256);
			imagegif($im, $imagePath);
			
			// $imagick = new \Imagick($imagePath);
			// $frameDataList = [];
			// $width = $imagick->getImageWidth();
			// $height = $imagick->getImageheight();
			// $image = $imagick->coalesceImages();
			// $count = 0;

			// foreach ($image as $frame) {
			// 	$delay = $frame->getImageDelay();
			// 	$newFrame = new \Imagick();
			// 	$newFrame->newImage($width, $height, new \ImagickPixel("black"));
			// 	$newFrame->setImageFormat("png");
			// 	$newFrame->compositeImage($frame, \Imagick::COMPOSITE_OVER, 0, 10);
			// 	$path = self::getUploadFileDir(self::TYPE_IMAGE) . '/tmp_gif_' . $count . '.png';
			// 	$newFrame->writeImage($path);
			// 	$dataItem = ['path' => $path, 'delay' =>$delay];
			// 	$frameDataList[] = $dataItem;
			// 	$count++;
			// }

			// $image = $image->deconstructImages();
			// $image->writeImages($imagePath, true);
			// unlink($imagePath);

			// $newGif = new \Imagick();
			// $newGif->setFormat('gif');

			// foreach ($frameDataList as $frame) {
			// 	$im = imagecreatefrompng($frame['path']);
			// 	imagepng($im, $frame['path'], 1);
			// 	$im = new \Imagick($frame['path']);
			// 	$im->setImageDispose(1);
			// 	$im->scaleImage($width, $height);
			//     $newGif->addImage($im);
			//     $newGif->setImageDelay($frame['delay']);
			//     // unlink($frame['path']);
			// }

			// $newGif->setImageIterations(0);
			// $newGif = $newGif->deconstructImages();
			// $newGif->writeImages($imagePath, true);
		}

			$img = new \Imagick($imagePath);
			$orientation = $img->getImageOrientation();

			switch($orientation) { 
			   case \Imagick::ORIENTATION_BOTTOMRIGHT: 
			      $img->rotateimage("#000", 180); // rotate 180 degrees 
			    	break; 
			   case \Imagick::ORIENTATION_RIGHTTOP: 
			      $img->rotateimage("#000", 90); // rotate 90 degrees CW 
			    	break; 
			    case \Imagick::ORIENTATION_LEFTBOTTOM: 
			      $img->rotateimage("#000", -90); // rotate 90 degrees CCW 
			    	break; 
			}

			$img->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
			$img->writeImage($imagePath);
			$img->clear();
			$img->destroy();
	}

	public static function prepareImageData($imagePath, $mimeType, $fileSize) {
		if ((int)$fileSize > 350000) {
			self::compressImage($imagePath, $mimeType);
		}

		$image = \Config\Services::image()->withFile($imagePath);
		$scaleWidth = ($image->getWidth() > 300) ? (300 / $image->getWidth()) : 1;
     	$scaleHeight = ($image->getHeight() > 300) ? (300 / $image->getHeight()) : 1;
     	$scale = min($scaleWidth, $scaleHeight);

     	$previewWidth = round($image->getWidth() * $scale);
     	$previewHeight = round($image->getHeight() * $scale);

     	return [
     		'previewHeight' => $previewHeight,
     		'previewWidth' => $previewWidth,
     ];
	}

	public static function saveFileOnDisk($file, $fileType) {
		$imageData = null;
		$fileSize = $file->getSize();
		$userDir = self::getUploadFileDir($fileType);
		$originalFileName = $file->getName();
		$originalImgName = str_replace(" ", "_", $file->getName());
		$imgRandomName = $file->getRandomName();
		$mimeType = $file->getMimeType();
		$imgRandomNameWithoutExec = substr($imgRandomName, 0, strrpos($imgRandomName, '.'));
		$fileName = $imgRandomNameWithoutExec . '_' . $originalImgName;
		$file->move($userDir, $fileName);

		if ($fileType == FileModel::TYPE_IMAGE) {
			$imageData = self::prepareImageData(self::getUploadFileDir($fileType) . '/' . $fileName, $mimeType, $fileSize);
		}

		return ['fileName' => $fileName, 'mimeType' => $mimeType, 'originalName' => $originalFileName, 'imageData' => $imageData];
	}

	public static function preparePathToAva($path) {
		
		if ($path !== '/img/no_ava.png') {
			return '/userFile/ava/' . $path;
		}

		return $path;
	}

	public static function preparePathToUserFile($path, $type) {
		return '/userFile/' . FileModel::typeLabel()[$type] . '/' . $path;
	}

	public static function preparePathToOpenFile($path, $type) {
		return '/openFile/' . FileModel::typeLabel()[$type] . '/' . $path;
	}

	public static function preparePathAdminToUserFile($path, $type) {
		return '/admin/userFile/' . FileModel::typeLabel()[$type] . '/' . $path;
	}

	public function getfileByUrl($fileUrl) {
		return $this
			->where('url', $fileUrl)
			->first();
	}

	public function getFilePermission($dataFile, $userId) {
		if ((int)$dataFile->privacy_status === self::PUBLIC_STATUS) {
			return true;
		}

		if ($dataFile->related_item == 'user' && $dataFile->item_id == $userId) {
			return true;
		}

		$filePermissionModel = new PrivateFilePermissionModel();
		$filePermission = $filePermissionModel
			->where([
				'file_id' => $dataFile->file_id,
				'permitted_id' => $userId
			])
			->first();

		return !empty($filePermission) ? true : false;
	}

	public function setFilePermission($fileId, $ownerId, array $permittedIdList)
	{
		$filePermissionModel = new PrivateFilePermissionModel();

		foreach ($permittedIdList as $permittedId) {
			if ((int)$permittedId === (int)$ownerId) {
				continue;
			}

			$filePermission = $filePermissionModel
				->where([
					'file_id' => $fileId,
					'permitted_id' => $permittedId
				])
				->first();

			if (empty($filePermission)) {
				$filePermission = [
					'owner_id' => $ownerId,
					'permitted_id' => $permittedId,
					'file_id' => $fileId,
				];

				$filePermissionModel->save($filePermission);
			}
		}
	}

	protected function deleteFileFromDisk(array $data)
	{
		$fileList = $this->find($data['id']);

		foreach ($fileList as $file) {
			$fileOnDisk = unlink(self::getUploadFileDir($file->type) . '/' . $file->url);
		}
	}

	public function getFileCountOfUser($userId)
	{
		$imageCountQuery = $this
			->select('count(file_id)')
			->where('related_item', self::RELATED_ITEM_USER)
			->where('item_id', $userId)
			->where('type', self::TYPE_IMAGE)
			->getCompiledSelect();

		$videoCountQuery = $this
			->select('count(file_id)')
			->where('related_item', self::RELATED_ITEM_USER)
			->where('item_id', $userId)
			->where('type', self::TYPE_VIDEO)
			->getCompiledSelect();

		$documentCountQuery = $this
			->select('count(file_id)')
			->where('related_item', self::RELATED_ITEM_USER)
			->where('item_id', $userId)
			->where('type', self::TYPE_DOCUMENT)
			->getCompiledSelect();

		$result = $this
			->select('(' . $imageCountQuery . ') as imageCount')
			->select('(' . $videoCountQuery . ') as videoCount')
			->select('(' . $documentCountQuery . ') as documentCount')
			->get()
            ->getRowArray();

        return (!is_null($result))
            ? $result
            : [
               'imageCount' => 0, 
               'videoCount' => 0, 
               'documentCount' => 0, 
            ];
	}
}
