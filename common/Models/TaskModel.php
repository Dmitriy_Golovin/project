<?php

namespace Common\Models;

use CodeIgniter\Model;
use App\Libraries\DateAndTime\DateAndTime;

class TaskModel extends Model
{
    const STATUS_NOT_COMPLETED = 1;
    const STATUS_COMPLETED = 2;
    const STATUS_OVERDUE = 3;
    const STATUS_NO_EXECUTOR = 4;

    const STATUS_NOT_REVIEWED = 0;
    const STATUS_DECLINED = 1;
    const STATUS_ACCEPTED = 2;

    const PERIODICITY_NONE = 0;
    const PERIODICITY_WORKING_DAY = 1;
    const PERIODICITY_DAILY = 2;
    const PERIODICITY_WEEKLY = 3;
    const PERIODICITY_MONTHLY = 4;
    const PERIODICITY_YEARLY = 5;

    const STATUS_NOT_CLONED = 0;
    const STATUS_CLONED = 1;

    const STATUS_NOT_VIEW_ASSIGNED = 0;
    const STATUS_VIEW_ASSIGNED = 1;

    const STATUS_NOT_VIEW_OWNER = 0;
    const STATUS_VIEW_OWNER = 1;

    const PUBLIC_STATUS = 0;
    const PRIVATE_STATUS = 1;

    protected $DBGroup          = 'default';
    protected $table            = 'task';
    protected $primaryKey       = 'task_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'source_id',
        'name',
        'owner_user_id',
        'assigned_user_id',
        'description',
        'duration',
        'periodicity',
        'reminder',
        'time',
        'date',
        'privacy',
        'is_cloned',
        'status',
        'time_offset_seconds',
        'review_status',
        'is_viewed_assigned',
        'is_viewed_owner',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('task');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public static function getReviewStatusLabel()
    {
        return [
            self::STATUS_NOT_REVIEWED => lang('Task.not reviewed'),
            self::STATUS_DECLINED => lang('Task.rejected'),
            self::STATUS_ACCEPTED => lang('Task.accepted'),
        ];
    }

    public static function getStatusLabel()
    {
        return [
            self::STATUS_NOT_COMPLETED => lang('Task.not completed'),
            self::STATUS_COMPLETED => lang('Task.completed'),
            self::STATUS_OVERDUE => lang('Task.overdue'),
            self::STATUS_NO_EXECUTOR => lang('Task.no task executor'),
        ];
    }

    public static function getViewedByExecutorStatusLabel($locale = '')
    {
        return [
            self::STATUS_NOT_VIEW_ASSIGNED => lang('Main.not viewed', [], $locale),
            self::STATUS_VIEW_ASSIGNED => lang('Main.viewed', [], $locale),
        ];
    } 

    public static function getViewedByExecutorCssClass()
    {
        return [
            self::STATUS_NOT_VIEW_ASSIGNED => ' task_not_viewed_status',
            self::STATUS_VIEW_ASSIGNED => ' task_viewed_status',
        ];
    } 

    public static function getReviewStatusCssClass()
    {
        return [
            self::STATUS_NOT_REVIEWED => ' task_not_reviewed_status',
            self::STATUS_DECLINED => ' task_rejected_status',
            self::STATUS_ACCEPTED => ' task_accepted_status',
        ];
    }

    public static function getStatusCssClass()
    {
        return [
            self::STATUS_NOT_COMPLETED => ' task_not_completed_status',
            self::STATUS_COMPLETED => ' task_completed_status',
            self::STATUS_OVERDUE => ' task_overdue_status',
            self::STATUS_NO_EXECUTOR => ' task_overdue_status',
        ];
    }

    public static function getStatusDetailsCssClass()
    {
        return [
            self::STATUS_NOT_COMPLETED => ' entity_status_scheduled',
            self::STATUS_COMPLETED => ' entity_status_completed',
            self::STATUS_OVERDUE => ' entity_status_overdue',
            self::STATUS_NO_EXECUTOR => ' entity_status_overdue',
        ];
    }

    public function getTaskDataList($userId, $timeOffsetSeconds)
    {
        $endDay = DateAndTime::tomorrow()->getTimestamp() - $timeOffsetSeconds;
        $timeNow = time();
        
        $queryCurrentAmount = $this
            ->select('count(task_id)')
            ->where('assigned_user_id', $userId)
            ->where('status', self::STATUS_NOT_COMPLETED)
            ->where('deleted_at', null)
            ->groupStart()
                ->groupStart()
                    ->where('(date + time) <=', $timeNow)
                    ->where('(date + time + duration) >=', $timeNow)
                ->groupEnd()
                ->orGroupStart()
                    ->where('(date + time) <', $endDay)
                    ->where('(date + time) >=', $timeNow)
                ->groupEnd()
            ->groupEnd()
            ->getCompiledSelect();

        $querySheduledAmount = $this
            ->select('count(task_id)')
            ->where('(date + time) >', $endDay)
            ->where('assigned_user_id', $userId)
            ->where('status', self::STATUS_NOT_COMPLETED)
            ->where('deleted_at', null)
            ->getCompiledSelect();

        $queryCompletedAmount = $this
            ->select('count(task_id)')
            ->where('status', self::STATUS_COMPLETED)
            ->where('assigned_user_id', $userId)
            ->where('deleted_at', null)
            ->getCompiledSelect();

        $queryOverdueAmount = $this
            ->select('count(task_id)')
            ->where('assigned_user_id', $userId)
            ->where('deleted_at', null)
            ->groupStart()
                ->groupStart()
                    ->where('status', self::STATUS_OVERDUE)
                ->groupEnd()
                ->orGroupStart()
                    ->where('(date + time + duration) <', $timeNow)
                    ->where('status !=', self::STATUS_COMPLETED)
                ->groupEnd()
            ->groupEnd()
            ->getCompiledSelect();

        $queryAssignedBySelfAmount = $this
            ->select('count(task_id)')
            ->where('owner_user_id', $userId)
            // ->where('is_cloned', self::STATUS_NOT_CLONED)
            ->where('deleted_at', null)
            ->groupStart()
                ->groupStart()
                    ->where('is_cloned', TaskModel::STATUS_CLONED)
                    ->where('periodicity !=', TaskModel::PERIODICITY_NONE)
                    ->where('review_status !=', TaskModel::STATUS_ACCEPTED)
                ->groupEnd()
                ->orGroupStart()
                    ->where(TaskModel::table() . '.is_cloned', TaskModel::STATUS_NOT_CLONED)
                ->groupEnd()
            ->groupEnd()
            ->getCompiledSelect();

        $result = $this
            ->select('(' . $queryCurrentAmount . ') as current')
            ->select('(' . $querySheduledAmount . ') as scheduled')
            ->select('(' . $queryCompletedAmount . ') as completed')
            ->select('(' . $queryOverdueAmount . ') as overdue')
            ->select('(' . $queryAssignedBySelfAmount . ') as created')
            ->get()
            ->getRowArray();

        return (!is_null($result))
            ? $result
            : [
               'current' => 0, 
               'scheduled' => 0, 
               'completed' => 0, 
               'overdue' => 0, 
               'created' => 0, 
            ];
    }

    public function getNotViewedTaskCount($userId)
    {
        $taskCount = $this
            ->groupStart()
                ->where('assigned_user_id', $userId)
                ->where('is_viewed_assigned', self::STATUS_NOT_VIEW_ASSIGNED)
                ->where('status !=', self::STATUS_COMPLETED)
                ->where('deleted_at', null)
            ->groupEnd()
            ->orGroupStart()
                ->where('owner_user_id', $userId)
                ->where('status', self::STATUS_COMPLETED)
                ->where('review_status', self::STATUS_NOT_REVIEWED)
                ->where('deleted_at', null)
            ->groupEnd()
            ->countAllResults();

        return $taskCount;
    }

    public function getNotViewedTaskCountFullData($userId, $timeOffsetSeconds)
    {
        $startDay = DateAndTime::getTimestampStartDay(time()) - $this->timeOffsetSeconds;
        $endDay = $startDay + DateAndTime::SECONDS_IN_DAY - 1;

        $queryCurrent = $this
            ->selectCount('task_id')
            ->where('assigned_user_id', $userId)
            ->where('is_viewed_assigned', self::STATUS_NOT_VIEW_ASSIGNED)
            ->where('status', self::STATUS_NOT_COMPLETED)
            ->where('deleted_at', null)
            ->groupStart()
                ->groupStart()
                    ->where('(date + time) <=', time())
                    ->where('(date + time + duration) >=', time())
                ->groupEnd()
                ->orGroupStart()
                    ->where('(date + time) >=', time())
                    ->where('(date + time) <=', $endDay)
                ->groupEnd()
            ->groupEnd()
            ->getCompiledSelect();

        $queryScheduled = $this
            ->selectCount('task_id')
            ->where('assigned_user_id', $userId)
            ->where('is_viewed_assigned', self::STATUS_NOT_VIEW_ASSIGNED)
            ->where('status', TaskModel::STATUS_NOT_COMPLETED)
            ->where('(date + time) >', $endDay)
            ->where('deleted_at', null)
            ->getCompiledSelect();

        $queryOverdue = $this
            ->selectCount('task_id')
            ->where('assigned_user_id', $userId)
            ->where('is_viewed_assigned', self::STATUS_NOT_VIEW_ASSIGNED)
            ->where('deleted_at', null)
            ->groupStart()
                ->groupStart()
                    ->where('status', TaskModel::STATUS_OVERDUE)
                ->groupEnd()
                ->orGroupStart()
                    ->where('(date + time + duration) <=', time())
                    ->where('status', TaskModel::STATUS_NOT_COMPLETED)
                ->groupEnd()
            ->groupEnd()
            ->getCompiledSelect();

        $queryCreated = $this
            ->selectCount('task_id')
            ->where('owner_user_id', $userId)
            ->where('status', self::STATUS_COMPLETED)
            ->where('review_status', self::STATUS_NOT_REVIEWED)
            ->where('deleted_at', null)
            ->getCompiledSelect();

        $result = $this
            ->select('(' . $queryCurrent . ') as currentCount')
            ->select('(' . $queryScheduled . ') as scheduledCount')
            ->select('(' . $queryOverdue . ') as overdueCount')
            ->select('(' . $queryCreated . ') as createdCount')
            ->get()
            ->getRowArray();

        return (!is_null($result))
            ? $result
            : [
               'currentCount' => 0, 
               'scheduledCount' => 0, 
               'overdueCount' => 0, 
               'createdCount' => 0, 
            ];
    }

    public function checkAndSaveFilePermission($task, $privacy, $assignedUserId)
    {
        $fileModel = new FileModel();
        $filePermissionModel = new PrivateFilePermissionModel();
        $fileList = $this->getTaskFileList($task->task_id, $task->source_id, $task->owner_user_id, $fileModel);

        if ((int)$privacy === self::PRIVATE_STATUS) {
            if ((int)$task->privacy === self::PUBLIC_STATUS) {
                foreach ($fileList as $file) {
                    $file->privacy_status = FileModel::PRIVATE_STATUS;
                    $fileModel->save($file);
                }
            }

            if ((empty($task->assigned_user_id) || (int)$task->assigned_user_id !== (int)$assignedUserId)
                && !empty($assignedUserId)) {

                foreach ($fileList as $file) {
                    $fileModel->setFilePermission($file->file_id, $task->owner_user_id, [$assignedUserId]);
                }
            }

            if (!empty($task->assigned_user_id)
                && (empty($assignedUserId || (int)$assignedUserId !== (int)$task->assigned_user_id))) {

                foreach ($fileList as $file) {
                    $filePermissionModel->where('file_id', $file->file_id)
                        ->where('owner_id', $task->owner_user_id)
                        ->where('permitted_id', $task->assigned_user_id)
                        ->delete();
                }
            }
        } else if ((int)$privacy === self::PUBLIC_STATUS) {
            if ((int)$task->privacy === self::PRIVATE_STATUS) {
                foreach ($fileList as $file) {
                    $file->privacy_status = FileModel::PUBLIC_STATUS;
                    $fileModel->save($file);
                }
            }
        }
    }

    public function getTaskFileList($taskId, $sourceId, $ownerId, $fileModel)
    {
        $result = [];
        $query = $fileModel
            ->select(FileModel::table() . '.*')
            ->join(TaskHistoryFileModel::table() . ' as taskHistoryFileTable', 'taskHistoryFileTable.file_id = ' . FileModel::table() . '.file_id')
            ->join(TaskHistoryModel::table() . ' as taskHistoryTable', 'taskHistoryTable.task_history_id = taskHistoryFileTable.task_history_id')
            ->join(self::table() . ' as taskTable', 'taskTable.task_id = taskHistoryTable.task_id')
            ->whereIn('taskTable.task_id', [$taskId, $sourceId])
            ->where('taskTable.owner_user_id', $ownerId)
            ->get();

        foreach ($query->getResultObject() as $file) {
            $result[] = $file;
        }

        return $result;
    }
}
