<?php

namespace Common\Models;

use CodeIgniter\Model;

class ChangeEmailModel extends Model
{
    const EMAIL_NOT_CHANGE = 0;
    const EMAIL_CHANGE = 1;

    protected $DBGroup          = 'default';
    protected $table            = 'change_email';
    protected $primaryKey       = 'change_email_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['email_tmp', 'user_id', 'token', 'changed'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }
}
