<?php

namespace Common\Models;

use CodeIgniter\Model;

class MessageModel extends Model
{
	const MESSAGE_NOT_DELETED = 0;
	const MESSAGE_DELETED = 1;

	const MESSAGE_NOT_IMPORTANT = 0;
	const MESSAGE_IMPORTANT = 1;

	protected $table                = 'message';
	protected $primaryKey           = 'message_id';
	protected $useAutoIncrement     = true;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['text', 'user_from', 'user_to', 'delete_from', 'delete_to', 'status', 'important_from', 'important_to'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'int';
	protected $createdField = 'created_at';
	protected $updatedField = 'updated_at';
	protected $deletedField = 'deleted_at';


	// Validation
	protected $validationRules      = [];

	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = false;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public static function table()
	{
		return db_connect()->prefixTable('message');
	}

	public function getTableName()
	{
		$db = db_connect();
		return $db->prefixTable($this->table);
	}

	public function getMessageByIdAndUserId($messageId, $selfId) {
		return $this->groupStart()
			->where([
				'message_id' => $messageId,
			 	'user_to' => $selfId
			])
			->groupEnd()
			->orGroupStart()
			->where([
				'user_from' => $selfId,
			 	'message_id' => $messageId,
			])
			->groupEnd()
			->first();
	}

	public function getDialogMessages($withUserId, $userId) {
		$result = [];

		$query = $this->groupStart()
			->where([
				'user_to' => $withUserId,
				'user_from' => $userId,
			])->groupEnd()
			->orGroupStart()
			->where([
				'user_to' => $userId,
				'user_from' => $withUserId,
			])
			->groupEnd();

		foreach ($query->get()->getResult() as $message) {
			$result[] = $message;
		}

		return $result;
	}

	public function getNewMessagesAmountDB($userId) {
		$newMessagesAmount = $this->selectSum('status')
			->where('user_to', $userId)
			->where('delete_to', 0)
			->where('status', 1)
			->get()
			->getRowObject();
			
		return $newMessagesAmount;
	}
}
