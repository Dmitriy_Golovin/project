<?php

namespace Common\Models;

use CodeIgniter\Model;

class ArticleModel extends Model
{
    const DEFAULT_ARTICLE_IMAGE = '/img/icon_article_pic_stub.png';
    const DEFAULT_ARTICLE_IMAGE_TITLE = 'Article';

    const PARAGRAPH_DELIMITER = '<--paragraph_delimiter-->';

    const STATUS_DRAFT = 1;
    const STATUS_NOT_PUBLISHED = 2;
    const STATUS_PUBLISHED = 3;

    const STATUS_DRAFT_LABEL = 'draft';
    const STATUS_NOT_PUBLISHED_LABEL = 'not published';
    const STATUS_PUBLISHED_LABEL = 'published';

    protected $DBGroup          = 'default';
    protected $table            = 'article';
    protected $primaryKey       = 'article_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'article_section_id',
        'title',
        'title_transliteration',
        'description',
        'meta_description',
        'status',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function getLabels()
    {
        return [
            'article_id' => 'ID',
            'article_section_id' => 'Раздел статьи',
            'title' => 'Название',
            'title_transliteration' => 'Транслитерация',
            'status' => 'Статус',
            'description' => 'Описание',
            'meta_description' => 'Содержимое метатега description',
            'file' => 'Изображение',
            'file_link' => 'Источник изображения',
            'file_title' => 'Название изображения',
            'created_at' => 'Дата создания',
        ];
    }

    public static function getStatuslabels()
    {
        return [
            self::STATUS_DRAFT => lang('Article.' . self::STATUS_DRAFT_LABEL),
            self::STATUS_NOT_PUBLISHED => lang('Article.' . self::STATUS_NOT_PUBLISHED_LABEL),
            self::STATUS_PUBLISHED => lang('Article.' . self::STATUS_PUBLISHED_LABEL),
        ];
    }

    public static function table()
    {
        return db_connect()->prefixTable('article');
    }

    public static function getArticleSectionIdTitle()
    {
        $result = [];
        $articleSectionModel = new ArticleSectionModel();
        $query = $articleSectionModel
            ->get();

        foreach ($query->getResultObject() as $item) {
            $resultItem = ['article_section_id' => $item->article_section_id, 'title' => $item->title];
            $result[] = $resultItem;
        }

        return $result;
    }

    public static function prepareParagraphText($text, $position)
    {
        return '<div data-type="text" --adattr-->'
            . $text
            . '</div>';
    }

    public static function prepareParagraphImage($fileData, $position)
    {
        $image = '<div data-type="image" data-file_id="' . $fileData['file_id'] . '" --adattr--><div class="article_img_container">'
            . '<div>'
            . '<img src="' . FileModel::preparePathToOpenFile($fileData['url'], $fileData['type']) . '" alt="' . $fileData['title'] . '" title="' . $fileData['title'] . '">'
            . '<div class="img_title">' . $fileData['title'] . '</div>';

        if (!empty($fileData['source'])) {
            $image .= '<div class="img_source"><a href="' . $fileData['source'] . '">Источник изображения</a></div>';
        }

        $image .= '</div></div></div>';
        return $image;
    }

    public static function prepareDescription($description, $text, $position, $type)
    {
        if ($type === 'text') {
            $text = self::prepareParagraphText($text, $position);
        }

        if ($type === 'image') {
            $text = self::prepareParagraphImage($text, $position);
        }

        if (empty($description)) {
            return $text;
        } else {
            $descriptionArr = explode(self::PARAGRAPH_DELIMITER, $description);
            array_splice($descriptionArr, $position, 0, $text);

            return implode(self::PARAGRAPH_DELIMITER, $descriptionArr);
        }
    }

    public static function prepareDescriptionForDetailsAdmin($description)
    {
        $descriptionArr = explode(self::PARAGRAPH_DELIMITER, $description);

        foreach ($descriptionArr as $index => &$item) {
            $item = preg_replace('/--adattr-->/', '><span class="p_count">' . ($index + 1) . '</span><span class="p_delete" data-position="' . $index . '">удалить</span>', $item);
        }

        $description = implode(self::PARAGRAPH_DELIMITER, $descriptionArr);
        return preg_replace('/' . self::PARAGRAPH_DELIMITER . '/', '', $description );
    }

     public static function prepareDescriptionForDetailsClient($description)
    {
        $descriptionArr = explode(self::PARAGRAPH_DELIMITER, $description);

        foreach ($descriptionArr as $index => &$item) {
            $item = preg_replace('/--adattr-->/', '>', $item);
        }

        $description = implode(self::PARAGRAPH_DELIMITER, $descriptionArr);
        return preg_replace('/' . self::PARAGRAPH_DELIMITER . '/', '', $description );
    }

    public static function prepareDescriptionForPreview($description)
    {
        $descriptionArr = explode(self::PARAGRAPH_DELIMITER, $description);

        foreach ($descriptionArr as $index => &$item) {
            $item = preg_replace('/ --adattr-->/', '>', $item);

            if (stristr($item, 'data-type="text"') !== false) {
                $item = preg_replace('/<div data-type="text">/', '', $item);
                $item = preg_replace('/<\/div>/', '', $item);
                $item = (mb_strlen($item) > 200)
                    ? mb_substr($item, 0, 200) . ' ...'
                    : $item;

                return $item;
            }
        }
    }

    public static function deleteParagraph($description, $position)
    {
        $descriptionArr = explode(self::PARAGRAPH_DELIMITER, $description);
        array_splice($descriptionArr, $position, 1);

        return !empty($descriptionArr)
            ? implode(self::PARAGRAPH_DELIMITER, $descriptionArr)
            : null;
    }

    public function deleteFiles($articleId)
    {
        $fileModel = new FileModel();

        $deleteQuery = $fileModel
            ->where('related_item', FileModel::RELATED_ITEM_ARTICLE)
            ->where('item_id', $articleId)
            ->get();

        foreach ($deleteQuery->getResultObject() as $file) {
            if (!$fileModel->delete($file->file_id)) {
                return false;
            }
        }

        return true;
    }
}
