<?php

namespace Common\Models;

use CodeIgniter\Model;

class FileChatMessageModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'file_chat_message';
    protected $primaryKey       = 'file_chat_message_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['chat_message_id', 'file_id'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('file_chat_message');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }
}
