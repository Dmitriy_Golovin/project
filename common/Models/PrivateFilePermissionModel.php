<?php

namespace Common\Models;

use CodeIgniter\Model;

class PrivateFilePermissionModel extends Model
{
	protected $table                = 'private_file_permission';
	protected $primaryKey           = 'private_file_permission_id';
	protected $useAutoIncrement     = true;
	protected $returnType           = 'object';
	protected $useSoftDelete        = false;
	protected $allowedFields        = ['owner_id', 'permitted_id', 'file_id'];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'int';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [
		'owner_id' => 'required',
		'permitted_id' => 'required',
		'file_id' => 'required',
	];

	protected $validationMessages   = [];
	protected $cleanValidationRules = false;

	public static function table()
	{
		return db_connect()->prefixTable('private_file_permission');
	}

	public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }
}