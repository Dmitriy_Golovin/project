<?php

namespace Common\Models;

use CodeIgniter\Model;
use App\Libraries\DateAndTime\DateAndTime;

class MeetingModel extends Model
{
    const STATUS_NOT_COMPLETED = 0;
    const STATUS_COMPLETED = 1;

    const STATUS_NOT_FAILED = 0;
    const STATUS_FAILED = 1;

    const PERIODICITY_NONE = 0;
    const PERIODICITY_WORKING_DAY = 1;
    const PERIODICITY_DAILY = 2;
    const PERIODICITY_WEEKLY = 3;
    const PERIODICITY_MONTHLY = 4;
    const PERIODICITY_YEARLY = 5;

    const STATUS_NOT_CLONED = 0;
    const STATUS_CLONED = 1;

    const PUBLIC_STATUS = 0;
    const PRIVATE_STATUS = 1;

    protected $DBGroup          = 'default';
    protected $table            = 'meeting';
    protected $primaryKey       = 'meeting_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'user_id',
        'description',
        'location',
        'name',
        'duration',
        'periodicity',
        'reminder',
        'time',
        'date',
        'privacy',
        'is_completed',
        'is_failed',
        'is_cloned',
        'time_offset_seconds',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = ['deleteUserGuest'];

    public static function table()
    {
        return db_connect()->prefixTable('meeting');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public function getMeetingDataList($userId, $timeOffsetSeconds)
    {
        $endDay = DateAndTime::tomorrow()->getTimestamp() - $timeOffsetSeconds;
        $current = [];
        $scheduled = [];
        $completed = [];
        $query = $this
            ->where('user_id', $userId)
            ->where('deleted_at', null)
            ->get();

        foreach ($query->getResultObject() as $item) {
            if (((int)$item->date + (int)$item->time <= time()
                && (int)$item->date + (int)$item->time + (int)$item->duration >= time()
                && (int)$item->is_completed === self::STATUS_NOT_COMPLETED)
                || ((int)$item->is_completed === self::STATUS_NOT_COMPLETED
                && (int)$item->date + (int)$item->time >= time()
                && (int)$item->date + (int)$item->time < $endDay)) {
                $current[] = $item;
            }

            if ((int)$item->date + (int)$item->time >= $endDay && (int)$item->is_completed === self::STATUS_NOT_COMPLETED) {
                $scheduled[] = $item;
            }

            if ((int)$item->is_completed === self::STATUS_COMPLETED
                || (int)$item->date + (int)$item->time + (int)$item->duration <= time()) {
                $completed[] = $item;
            }
        }

        return ['current' => $current, 'scheduled' => $scheduled, 'completed' => $completed];
    }

    public function getActiveInvite($userId)
    {
        $result = [];
        $eventGuestUserModel = new EventGuestUserModel();
        $query = $this
            ->join(EventGuestUserModel::table() . ' as userGuest', 'userGuest.item_id = ' . self::table() . '.meeting_id and userGuest.related_item = "' . self::table() . '"')
            ->where('userGuest.user_id', $userId)
            // ->where('userGuest.status !=', EventGuestUserModel::STATUS_DECLINED)
            ->where('is_completed', self::STATUS_NOT_COMPLETED)
            ->where('(date + time + duration) >', time())
            ->where(self::table() . '.deleted_at', null)
            ->get();

        foreach ($query->getResultObject() as $item) {
            $result[] = $item;
        }

        return $result;
    }

    public function getGuestList($eventId)
    {
        $eventGuestUserModel = new EventGuestUserModel();
        $result = [];
        $userIdList = [];

        $query = $eventGuestUserModel
            ->select(EventGuestUserModel::table() . '.*')
            ->select('concat(userTable.first_name, " ", userTable.last_name) as userFullname')
            ->select('fileTable.url as userAva')
            ->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . EventGuestUserModel::table() . '.user_id')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
            ->where(EventGuestUserModel::table() . '.related_item', self::table())
            ->where(EventGuestUserModel::table() . '.item_id', $eventId)
            ->get();

        foreach ($query->getResultObject() as $item) {
            $item->userAva = (!empty($item->userAva))
                ? FileModel::preparePathToAva($item->userAva)
                : FileModel::DEFAULT_AVA_URL;

            $result[] = $item;
            $userIdList[] = $item->user_id;
        }

        return ['list' => $result, 'userIdList' => $userIdList];
    }

    protected function deleteUserGuest(array $data)
    {
        $eventGuestUserModel = new EventGuestUserModel();
        $eventGuestUserModel->where('item_id', $data['id'])
            ->where('related_item', self::table())
            ->delete();
    }
}
