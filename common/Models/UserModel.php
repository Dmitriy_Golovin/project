<?php

namespace Common\Models;

use CodeIgniter\Model;
use Common\Models\CityTranslationModel;
use Common\Models\CountryTranslationModel;
use Common\Models\FileModel;
use Common\Models\UserSettingModel;

class UserModel extends Model
{
	const STATUS_NOT_BLOCKED = 1;
	const STATUS_BLOCKED = 2;

	const ROLE_USER = 1;
	const ROLE_ADMIN = 2;

	const NOT_ACCEPT_TERMS = 0;
	const ACCEPT_TERMS = 1;

	const ACCOUNT_IS_NOT_DELETED = 1;
	const ACCOUNT_IS_DELETED = 2;

	protected $DBGroup              = 'default';
	protected $table                = 'user';
	protected $primaryKey           = 'user_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = \Common\Entities\User::class;
	// protected $returnType           = 'object';
	protected $useSoftDeletes       = true;
	protected $protectFields        = true;
	protected $allowedFields        = [
		'first_name',
		'last_name',
		'city_id',
		'country_id',
		'about_self',
		'email',
		'password',
		'accept_terms',
		'veryfication_token',
		'number_phone',
		'file_id',
		'token_change_safety_data',
		'restore_password_token',
		'is_blocked',
		'role',
	];

	// Dates
	protected $useTimestamps        = true;
	protected $dateFormat           = 'int';
	protected $createdField         = 'created_at';
	protected $updatedField         = 'updated_at';
	protected $deletedField         = 'deleted_at';

	// Validation
	protected $validationRules      = [];
	
	protected $validationMessages   = [];
	protected $skipValidation       = false;
	protected $cleanValidationRules = false;

	// Callbacks
	protected $allowCallbacks       = true;
	protected $beforeInsert         = [];
	protected $afterInsert          = [];
	protected $beforeUpdate         = [];
	protected $afterUpdate          = [];
	protected $beforeFind           = [];
	protected $afterFind            = [];
	protected $beforeDelete         = [];
	protected $afterDelete          = [];

	public static function table()
	{
		return db_connect()->prefixTable('user');
	}

	public static function getLabels()
	{
		return [
			'user_id' => 'ID',
			'first_name' => 'Имя',
			'last_name' => 'Фамилия',
			'country' => 'Страна',
			'city' => 'Город',
			'country_id' => 'Страна',
			'city_id' => 'Город',
			'email' => 'email',
			'about_self' => 'О себе',
			'accept_terms' => 'Условия приняты',
			'path_to_ava' => 'Аватар',
			'veryfication_token' => 'Статус профиля',
			'is_blocked' => 'Статус',
			'role' => 'Роль',
			'created_at' => 'Дата регистрации',
			'deleted_at' => 'Профиль удален',
			'userDeleted' => 'Профиль удален',
		];
	}

	public static function getRoleLabels()
	{
		return [
			self::ROLE_USER => lang('Main.role.User'),
			self::ROLE_ADMIN => lang('Main.role.Admin'),
		];
	}

	public static function getBlockedStatusLabels()
	{
		return [
			self::STATUS_NOT_BLOCKED => lang('Main.Active'),
			self::STATUS_BLOCKED => lang('Main.Blocked'),
		];
	}

	public static function getAccountDeletedLabel()
	{
		return [
			self::ACCOUNT_IS_NOT_DELETED => lang('Main.No'),
			self::ACCOUNT_IS_DELETED => lang('Main.Deleted'),
		];
	}

	public function getTableName()
	{
		$db = db_connect();
		return $db->prefixTable($this->table);
	}

	public function setSettingsUserDefault($userId) {
		$userSettingModel = new UserSettingModel();
		$data = [
			'user_id' => $userId,
			'holidays' => UserSettingModel::HOLIDAYS_ACTIVE,
			'my_events' => UserSettingModel::MY_EVENTS_ACTIVE,
			'my_meetings' => UserSettingModel::MY_MEETINGS_ACTIVE,
			'my_tasks' => UserSettingModel::MY_TASKS_ACTIVE,
			'friends_events' => UserSettingModel::FRIENDS_EVENTS_ACTIVE,
			'work_week_start' => 2,
            'work_days_amount' => 5,
		];

		if (!$userSettingModel->save($data)) {
			return false;
		}

		return true;
	}

	public function getUserData($userId, bool $withDeleted = false, bool $withBlocked = false, bool $withNotActive = false)
	{
		$negotiate = \Config\Services::negotiator();
		$language = $negotiate->language(config('app')->supportedLocales);
		$countryTranslationModel = new CountryTranslationModel();
		$cityTranslationModel = new CityTranslationModel();
		$fileModel = new FileModel();
		$userTableName = $this->getTableName();
		$countryTranslationTableName = $countryTranslationModel->getTableName();
		$cityTranslationTableName = $cityTranslationModel->getTableName();
		$fileTableName = $fileModel->getTableName();

		$user = $this->select('user_id, first_name, last_name, about_self, number_phone, email, ' . $userTableName . '.country_id , ' . $userTableName . '.city_id, ' . $userTableName . '.is_blocked, ' . $userTableName . '.deleted_at as userDeleted, ' . $fileTableName . '.url as path_to_ava, ' .$fileTableName . '.url, ' . $userTableName . '. file_id, ' .$userTableName . '.role, ' . $userTableName . '.is_blocked, ' . $userTableName . '.created_at, ' . $countryTranslationTableName . '.title as countryTitle , ' . $cityTranslationTableName . '.title as cityTitle')
			->select($countryTranslationTableName . '.title as country')
			->select($cityTranslationTableName . '.title as city')
			->join($fileTableName, $userTableName . '.file_id = ' . $fileTableName . '.file_id', 'left')
			->join($countryTranslationTableName, $userTableName . '.country_id = ' . $countryTranslationTableName . '.country_id and ' . $countryTranslationTableName . '.lang_code = ' . '"' . $this->db->escapeString($language). '"', 'left')
			->join($cityTranslationTableName, $userTableName . '.city_id = ' . $cityTranslationTableName . '.city_id and ' . $cityTranslationTableName . '.lang_code = ' . '"' . $this->db->escapeString($language). '"', 'left')
			->where(['user_id' => $userId]);

		if (!$withNotActive) {
			$user->where(self::table() . '.veryfication_token', null);
		}

		if ($withDeleted) {
			$user->withDeleted();
		} else {
			$user->where(self::table() . '.deleted_at', null);
		}

		if (!$withBlocked) {
			$user->where(self::table() . '.is_blocked', self::STATUS_NOT_BLOCKED);
		}
			
		$user = $user->first();

		if (empty($user)) {
			return false;
		}

		if (empty($user->path_to_ava)) {
			$user->path_to_ava = FileModel::DEFAULT_AVA_URL;
		} else {
			$user->path_to_ava = FileModel::preparePathToAva($user->path_to_ava);
		}

		return $user;
	}

	public function getUserDataMin($userId)
	{
		$user = $this
			->select('user_id, first_name, last_name')
			->select('fileTable.url as path_to_ava')
			->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . self::table() . '.file_id', 'left')
			->where('user_id', $userId)
			->where(self::table() . '.deleted_at', null)
			->where(self::table() . '.is_blocked', self::STATUS_NOT_BLOCKED)
			->where(self::table() . '.veryfication_token', null)
			->first();

		if (empty($user)) {
			return $user;
		}

		if (empty($user->path_to_ava)) {
			$user->path_to_ava = FileModel::DEFAULT_AVA_URL;
		} else {
			$user->path_to_ava = FileModel::preparePathToAva($user->path_to_ava);
		}

		return $user;
	}
}
