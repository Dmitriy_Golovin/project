<?php

namespace Common\Models;

use CodeIgniter\Model;
use Common\Models\ChatMemberModel;
use Common\Models\ChatModel;

class ChatMessageModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'chat_message';
    protected $primaryKey       = 'chat_message_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'chat_id',
        'chat_member_id',
        'text',
        'quoted_chat_message_Id',
        'recipient_chat_member_ids',
        'who_viewed_chat_member_ids',
        'important_chat_member_ids',
        'deleted_chat_member_ids',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('chat_message');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public function getNewMessagesAmountDB($userId) {
        $chatModel = new ChatModel();
        $chatMemberModel = new ChatMemberModel();
        $newMessagesAmount = $this
            ->join($chatModel->table() . ' as chat', 'chat.chat_id = ' . self::table() . '.chat_id and chat.deleted_at is null')
            ->join($chatMemberModel->table() . ' as chatMember', 'chatMember.chat_id = chat.chat_id and chatMember.user_id = ' . $userId . ' and chatMember.deleted_at is null')
            ->where('not json_contains(who_viewed_chat_member_ids, cast(chatMember.chat_member_id as json), "$")')
            ->where('not json_contains(deleted_chat_member_ids, cast(chatMember.chat_member_id as json), "$")')
            ->where(self::table() . '.chat_member_id != chatMember.chat_member_id')
            ->where(self::table() . '.created_at >= chatMember.created_at')
            ->countAllResults();
            
        return $newMessagesAmount;
    }
}
