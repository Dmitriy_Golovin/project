<?php

namespace Common\Models;

use CodeIgniter\Model;
use App\Libraries\DateAndTime\DateAndTime;

class CommentModel extends Model
{
    const TYPE_TABLE_REPORT = 1;
    const TYPE_TABLE_FILE = 2;
    const TYPE_TABLE_ARTICLE = 3;

    protected $DBGroup          = 'default';
    protected $table            = 'comment';
    protected $primaryKey       = 'comment_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['related_item', 'item_id', 'user_id', 'text'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    protected $limit = 10;

    public static function getTableByType()
    {
        return [
            self::TYPE_TABLE_REPORT => ReportModel::table(),
            self::TYPE_TABLE_FILE => FileModel::table(),
            self::TYPE_TABLE_ARTICLE => ArticleModel::table(),
        ];
    }

    public static function getModelByType()
    {
        return [
            self::TYPE_TABLE_REPORT => new ReportModel(),
            self::TYPE_TABLE_FILE => new FileModel(),
            self::TYPE_TABLE_ARTICLE => new ArticleModel(),
        ];
    }

    public static function getPrimaryKeyByType()
    {
        return [
            self::TYPE_TABLE_REPORT => 'report_id',
            self::TYPE_TABLE_FILE => 'file_id',
            self::TYPE_TABLE_ARTICLE => 'article_id',
        ];
    }

    public static function table()
    {
        return db_connect()->prefixTable('comment');
    }

    public function getCommentData($type, $entityId, $timeOffsetSeconds, $lastId = null, $timestamp = null, $existCount = null, $guest = false)
    {
        $commentModel = new CommentModel();
        $commentList = [];
        $timestamp = !empty($timestamp) ? $timestamp : time();
        $rowNumStart = (!is_null($existCount))
            ? (int)$existCount
            : 0;

        $query = $commentModel
            ->select(CommentModel::table() . '.*')
            ->select('concat(userTable.first_name, " ", userTable.last_name) as userFullname, userTable.deleted_at as userDeleted')
            ->select('fileTable.url as userAva')
            ->join(UserModel::table() . ' as userTable', 'userTable.user_id = ' . CommentModel::table() . '.user_id')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = userTable.file_id', 'left')
            ->where(CommentModel::table() . '.related_item', self::getTableByType()[$type])
            ->where(CommentModel::table() . '.item_id', $entityId)
            ->where(CommentModel::table() . '.created_at <=', $timestamp)
            ->orderBy(CommentModel::table() . '.created_at', 'DESC');

        $mainCommentAmount = $query->countAllResults(false);

        if (!empty($lastId)) {
            $query->where('comment_id <', $lastId);
        }

        $query = $query
            ->limit($this->limit)
            ->getCompiledSelect();

        $query = 'select (@rowNum := @rowNum + 1) as rowNum, userQuery.* from (' . $query . ') as userQuery, (select (@rowNum := ' . $rowNumStart . ')) as rowNum';

        $query = $commentModel->db->query('select query.* from (' . $query . ') as query order by  query.created_at ASC');

        foreach($query->getResultObject() as &$comment) {
            $comment->date = DateAndTime::getDateTime($comment->created_at, $timeOffsetSeconds, 'genetive');

            if (is_null($comment->userDeleted)) {
                $comment->userAva = !empty($comment->userAva)
                    ? ($guest)
                        ? FileModel::preparePathToOpenFile($comment->userAva, FileModel::TYPE_IMAGE)
                        : FileModel::preparePathToAva($comment->userAva)
                    : FileModel::DEFAULT_AVA_URL;
            } else {
                $comment->userFullname = lang('Main.Profile deleted');
                $comment->userAva = FileModel::DEFAULT_AVA_URL;
            }

            $commentList[] = $comment;
        }

        if (is_null($existCount)) {
            $existCount = $this->limit;
        } else {
            (int)$existCount += (int)count($commentList);
        }

        return [
            'commentList' => $commentList,
            'mainCommentAmount' => $mainCommentAmount,
            'timestamp' => $timestamp,
            'insert_type' => empty($lastId) ? 'renew' : 'add',
            'type' => $type,
            'entityId' => $entityId,
            'showPrevious' => ($mainCommentAmount > $existCount) ? true : false,
        ];
    }
}
