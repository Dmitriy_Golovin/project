<?php

namespace Common\Models;

use CodeIgniter\Model;

class EventGuestUserModel extends Model
{
    const STATUS_NOT_VIEWED = 0;
    const STATUS_VIEWED = 1;
    const STATUS_ACCEPTED = 2;
    const STATUS_DECLINED = 3;

    const STATUS_NOT_VIEWED_LABEL = 'not viewed';
    const STATUS_VIEWED_LABEL = 'viewed';
    const STATUS_ACCEPTED_LABEL = 'accepted';
    const STATUS_DECLINED_LABEL = 'declined';

    protected $DBGroup          = 'default';
    protected $table            = 'event_guest_user';
    protected $primaryKey       = 'event_guest_user_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'related_item',
        'item_id',
        'user_id',
        'status',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('event_guest_user');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public static function getStatusLabels($locale = '')
    {
        return [
            self::STATUS_NOT_VIEWED => lang('Main.' . self::STATUS_NOT_VIEWED_LABEL, [], $locale),
            self::STATUS_VIEWED => lang('Main.' . self::STATUS_VIEWED_LABEL, [], $locale),
            self::STATUS_ACCEPTED => lang('Main.' . self::STATUS_ACCEPTED_LABEL, [], $locale),
            self::STATUS_DECLINED => lang('Main.' . self::STATUS_DECLINED_LABEL, [], $locale),
        ];
    }

    public function getAllNewActiveInviteList($userId)
    {
        $result = ['event' => [], 'meeting' => []];

        $eventQuery = $this
            ->join(EventModel::table() . ' as eventTable', 'eventTable.event_id = ' . self::table() . '.item_id and ' . self::table() . '.related_item = "' . EventModel::table() . '"')
            ->where(self::table() . '.user_id', $userId)
            ->where(self::table() . '.status', self::STATUS_NOT_VIEWED)
            ->where('eventTable.deleted_at', null)
            ->where('eventTable.is_completed', EventModel::STATUS_NOT_COMPLETED)
            ->get();

        foreach ($eventQuery->getResultObject() as $item) {
            $result['event'][] = $item;
        }

        $meetingQuery = $this
            ->join(MeetingModel::table() . ' as meetingTable', 'meetingTable.meeting_id = ' . self::table() . '.item_id and ' . self::table() . '.related_item = "' . MeetingModel::table() . '"')
            ->where(self::table() . '.user_id', $userId)
            ->where(self::table() . '.status', self::STATUS_NOT_VIEWED)
            ->where('meetingTable.deleted_at', null)
            ->where('meetingTable.is_completed', MeetingModel::STATUS_NOT_COMPLETED)
            ->get();

        foreach ($meetingQuery->getResultObject() as $item) {
            $result['meeting'][] = $item;
        }

        $result['mainCount'] = count($result['event']) + count($result['meeting']);

        return $result;
    }
}
