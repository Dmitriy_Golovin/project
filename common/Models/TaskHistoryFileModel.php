<?php

namespace Common\Models;

use CodeIgniter\Model;

class TaskHistoryFileModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'task_history_file';
    protected $primaryKey       = 'task_history_file_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'task_history_id',
        'file_id',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('task_history_file');
    }

    public function getFileList($taskHistoryId, $userId)
    {
        $fileModel = new FileModel();
        $result = [
            'videoList' => [],
            'imageList' => [],
            'documentList' => [],
        ];

        $query = $this
            ->select(TaskHistoryFileModel::table() . '.task_history_file_id')
            ->select('fileTable.*')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . TaskHistoryFileModel::table() . '.file_id')
            ->where('task_history_id', $taskHistoryId)
            ->get();

        foreach ($query->getResultObject() as $file) {
            $file->url = FileModel::preparePathToUserFile($file->url, $file->type);
            $filePermission = $fileModel->getFilePermission($file, $userId);

            if ((int)$file->type === FileModel::TYPE_VIDEO) {
                $file->name = FileModel::getFileNameWithoutExec($file->original_name);
                $file = ($filePermission) ? $file : 'notAllowed';
                $result['videoList'][] = $file;
            } else if ((int)$file->type === FileModel::TYPE_IMAGE) {
                $file->name = FileModel::getFileNameWithoutExec($file->original_name);
                $file = ($filePermission) ? $file : 'notAllowed';
                $result['imageList'][] = $file;
            } else if ((int)$file->type === FileModel::TYPE_DOCUMENT) {
                $file->name = $file->original_name;
                $file = ($filePermission) ? $file : 'notAllowed';
                $result['documentList'][] = $file;
            }
        }

        return $result;
    }
}
