<?php

namespace Common\Models;

use CodeIgniter\Model;

class VisitLogModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'visit_log';
    protected $primaryKey       = 'visit_log_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'ip',
        'user_agent',
        'uri_path',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function getLabels()
    {
        return [
            'visit_log_id' => 'ID',
            'ip' => 'Ip',
            'user_agent' => 'Пользовательский агент',
            'uri_path' => 'Uri',
            'created_at' => 'Дата создания',
        ];
    }

    public static function table()
    {
        return db_connect()->prefixTable('visit_log');
    }

    public function createLog($request)
    {
        $uri = service('uri');

        if (in_array($uri->getSegment(1), ['userFile'])/* || $request->getMethod() == 'post'*/) {
            return;
        }

        $data = [
            'ip' => $request->getIPAddress(),
            'user_agent' => $request->getUserAgent()->getAgentString(),
            'uri_path' => $uri->getPath(),
        ];

        $this->save($data);
    }
}
