<?php

namespace Common\Models;

use CodeIgniter\Model;

class TaskHistoryModel extends Model
{
    const TYPE_CREATE = 1;
    const TYPE_EDIT = 2;
    const TYPE_CLONE = 3;
    const TYPE_COMPLETE = 4;
    const TYPE_OVERDUE = 5;
    const TYPE_ACCEPT = 6;
    const TYPE_REJECT = 7;

    const ACTION_TYPE_COMPLETE = 'complete';
    const ACTION_TYPE_ACCEPT = 'accept';
    const ACTION_TYPE_REJECT = 'reject';

    const IS_NOT_MAIN = 0;
    const IS_MAIN = 1;

    protected $DBGroup          = 'default';
    protected $table            = 'task_history';
    protected $primaryKey       = 'task_history_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = true;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'task_id',
        'user_id',
        'type',
        'note',
        'is_main',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('task_history');
    }

    public static function getStatusLabels()
    {
        return [
            self::TYPE_CREATE => lang('TaskHistory.created'),
            self::TYPE_EDIT => lang('TaskHistory.edited'),
            self::TYPE_CLONE => lang('TaskHistory.replay created'),
            self::TYPE_COMPLETE => lang('TaskHistory.completed'),
            self::TYPE_OVERDUE => lang('TaskHistory.overdue'),
            self::TYPE_ACCEPT => lang('TaskHistory.result accepted'),
            self::TYPE_REJECT => lang('TaskHistory.result rejected'),
        ];
    }

    public static function getUserActionType()
    {
        return [
            'create' => self::TYPE_CREATE,
            'edit' => self::TYPE_EDIT,
            'complete' => self::TYPE_COMPLETE,
            'accept' => self::TYPE_ACCEPT,
            'reject' => self::TYPE_REJECT,
        ];
    }

    public static function getActionContentTitle()
    {
        return [
            self::ACTION_TYPE_COMPLETE => [
                'create' => lang('TaskHistory.complete task'),
                'edit' => lang('TaskHistory.complete task (edit)'),
            ],
            self::ACTION_TYPE_ACCEPT => [
                'create' => lang('TaskHistory.accept task completion'),
                'edit' => lang('TaskHistory.accept task completion (edit)'),
            ],
            self::ACTION_TYPE_REJECT => [
                'create' => lang('TaskHistory.reject task completion'),
                'edit' => lang('TaskHistory.reject task completion (edit)'),
            ],
        ];
    }

    public static function getTaskActionText($locale = '')
    {
        return [
            self::ACTION_TYPE_COMPLETE => [
                'create' => lang('Task.completed the task', [], $locale),
                'edit' => lang('Task.completed the task (edit)', [], $locale),
            ],
            self::ACTION_TYPE_ACCEPT => [
                'create' => lang('Task.accepted the result of completing the task', [], $locale),
                'edit' => lang('Task.accepted the result of completing the task (edit)', [], $locale),
            ],
            self::ACTION_TYPE_REJECT => [
                'create' => lang('Task.rejected the result of completing the task', [], $locale),
                'edit' => lang('Task.rejected the result of completing the task (edit)', [], $locale),
            ],
        ];
    }

    public function createItem($data)
    {
        if (!$this->save($data)) {
            return false;
        }

        return true;
    }
}
