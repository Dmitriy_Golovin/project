<?php

namespace Common\Models;

use CodeIgniter\Model;

class ArticleSectionModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'article_section';
    protected $primaryKey       = 'article_section_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'title',
        'title_transliteration',
        'description',
        'meta_description',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function getLabels()
    {
        return [
            'article_section_id' => 'ID',
            'title' => 'Название',
            'title_transliteration' => 'Транслитерация',
            'description' => 'Описание',
            'meta_description' => 'Содержимое метатега description',
            'file' => 'Изображение',
            'file_link' => 'Источник изображения',
            'file_title' => 'Название изображения',
            'created_at' => 'Дата создания',
        ];
    }

    public static function table()
    {
        return db_connect()->prefixTable('article_section');
    }

    public function getArticleSectionList()
    {
        $result = [];
        $query = $this
            ->select(self::table() . '.*')
            ->select('fileTable.url, fileTable.type, fileTable.source, fileTable.title as fileTitle')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.related_item = "' . FileModel::RELATED_ITEM_ARTICLE_SECTION . '" and fileTable.item_id = ' . self::table() . '.article_section_id', 'left')
            ->get();

        foreach ($query->getResultObject() as $item) {
            $item->url = FileModel::preparePathToOpenFile($item->url, $item->type);
            $result[] = $item;
        }

        return $result;
    }

    public function deleteFiles($articleSectionId)
    {
        $fileModel = new FileModel();

        $deleteQuery = $fileModel
            ->where('related_item', FileModel::RELATED_ITEM_ARTICLE_SECTION)
            ->where('item_id', $articleSectionId)
            ->get();

        foreach ($deleteQuery->getResultObject() as $file) {
            if (!$fileModel->delete($file->file_id)) {
                return false;
            }
        }

        return true;
    }
}
