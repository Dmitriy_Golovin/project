<?php

namespace Common\Models;

use CodeIgniter\Model;

class HolidayModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'holiday';
    protected $primaryKey       = 'holiday_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'holiday_day',
        'description',
        'country_id',
        'file_id',
        'file_link',
        'link',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public static function getLabels()
    {
        return [
            'holiday_id' => 'ID',
            'holiday_day' => 'Название',
            'description' => 'Описание',
            'link' => 'Источник',
            'file_id' => 'Изображение',
            'file' => 'Изображение',
            'file_link' => 'Источник изображения',
            'file_title' => 'Название изображения',
            'holiday_date' => 'Дата',
            'country' => 'Страна',
            'country_id' => 'Страна',
            'created_at' => 'Дата создания',
            'deleted_at' => 'Профиль удален',
        ];
    }

    public static function table()
    {
        return db_connect()->prefixTable('holiday');
    }

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public static function getHolidayDay($day) {
        
        $monthList = ['01' =>'января', '02' =>'февраля', '03' => 'марта', '04' =>'апреля', '05' => 'мая', '06' => 'июня', '07' =>'июля', '08' => 'августа', '09' => 'сентября', '10' => 'октября', '11' => 'ноября', '12' => 'декабря'];
        $dyaOfWeekWithPrefixLast = ['mon' => 'последний понедельник', 'tue' => 'последний вторник', 'wed' => 'последнюю среду', 'thu' => 'последний четверг', 'fri' => 'последнюю пятницу', 'sat' => 'последнюю субботу', 'sun' => 'последнее воскресенье'];
        $dyaOfWeekWithPrefixCount = ['mon' => ['в 1-й понедельник', 'во 2-й понедельник', 'в 3-й понедельник', 'в 4-й понедельник', 'в 5-й понедельник'],
                                     'tue' => ['в 1-й вторник', 'во 2-й вторник', 'в 3-й вторник', 'в 4-й вторник', 'в 5-й вторник'],
                                     'wed' => ['в 1-ю среду', 'во 2-ю среду', 'в 3-ю среду', 'в 4-ю среду', 'в 5-ю среду'],
                                     'thu' => ['в 1-й четверг', 'во 2-й четверг', 'в 3-й четверг', 'в 4-й четверг', 'в 5-й четверг'],
                                     'fri' => ['в 1-ю пятницу', 'во 2-ю пятницу', 'в 3-ю пятницу', 'в 4-ю пятницу', 'в 5-ю пятницу'],
                                     'sat' => ['в 1-ю субботу', 'во 2-ю субботу', 'в 3-ю субботу', 'в 4-ю субботу', 'в 5-ю субботу'],
                                     'sun' => ['в 1-е воскресенье', 'во 2-е воскресенье', 'в 3-е воскресенье', 'в 4-е воскресенье', 'в 5-е воскресенье']];
        $day = explode('-', $day);

        if (count($day) == 2) {
            return 'Отмечается ' . $day[0] . ' ' . $monthList[$day[1]] . '.';
        } else if ($day[2] == 'l') {
            return 'Отмечается в ' . $dyaOfWeekWithPrefixLast[$day[0]] . ' ' . $monthList[$day[1]] . '.';
        } else if (count($day) == 3 && strlen($day[1]) == 3) {
            return 'Отмечается ' . $dyaOfWeekWithPrefixCount[$day[1]][$day[0] - 1] . ' ' . $monthList[$day[2]] . '.';
        } else {
            return 'Уникальная дата для каждого года. В ' . $day[2] . ' году праздник отмечают ' . $day[0] . ' ' . $monthList[$day[1]] . '.';
        }
    }
}
