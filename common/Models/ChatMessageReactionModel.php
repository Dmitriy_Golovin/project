<?php

namespace Common\Models;

use CodeIgniter\Model;

class ChatMessageReactionModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'chat_message_reaction';
    protected $primaryKey       = 'chat_message_reaction_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = ['chat_message_id', 'chat_member_id', 'reaction'];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = [];
    protected $afterDelete    = [];

    public function getTableName()
    {
        $db = db_connect();
        return $db->prefixTable($this->table);
    }

    public function getMessageReactionList($chatMessageId)
    {
        $result = [];
        $reactionMessageQuery = $this
            ->where('chat_message_id', $chatMessageId)
            ->get();

        foreach ($reactionMessageQuery->getResultObject() as &$reaction) {
            if (empty($result)) {
                $reaction->count = 1;
                $result[] = $reaction;
                continue;
            }

            foreach ($result as &$item) {
                if ($reaction->reaction === $item->reaction) {
                    $item->count += 1;
                }

                if ($reaction->reaction !== $item->reaction && end($result)) {
                    $reaction->count = 1;
                    $result[] = $reaction;
                    continue 2;
                }
            }
        }

        return $result;
    }
}
