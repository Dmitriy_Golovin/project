<?php

namespace Common\Models;

use CodeIgniter\Model;
use App\Libraries\DateAndTime\DateAndTime;

class ReportModel extends Model
{
    protected $DBGroup          = 'default';
    protected $table            = 'report';
    protected $primaryKey       = 'report_id';
    protected $useAutoIncrement = true;
    protected $insertID         = 0;
    protected $returnType       = 'object';
    protected $useSoftDeletes   = false;
    protected $protectFields    = true;
    protected $allowedFields    = [
        'related_item',
        'item_id',
        'note',
    ];

    // Dates
    protected $useTimestamps = true;
    protected $dateFormat    = 'int';
    protected $createdField  = 'created_at';
    protected $updatedField  = 'updated_at';
    protected $deletedField  = 'deleted_at';

    // Validation
    protected $validationRules      = [];
    protected $validationMessages   = [];
    protected $skipValidation       = false;
    protected $cleanValidationRules = true;

    // Callbacks
    protected $allowCallbacks = true;
    protected $beforeInsert   = [];
    protected $afterInsert    = [];
    protected $beforeUpdate   = [];
    protected $afterUpdate    = [];
    protected $beforeFind     = [];
    protected $afterFind      = [];
    protected $beforeDelete   = ['deleteFiles'];
    protected $afterDelete    = [];

    public static function table()
    {
        return db_connect()->prefixTable('report');
    }

    public function getReportFileList($reportId, $userId, bool $addNotAllowed = false)
    {
        $reportFileModel = new ReportFileModel();
        $fileModel = new FileModel();
        $videoList = [];
        $imageList = [];

        $query = $reportFileModel
            ->select('fileTable.*')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . ReportFileModel::table() . '.file_id')
            ->where('report_id', $reportId)
            ->get();

        foreach ($query->getResultObject() as &$file) {
            $file->url = FileModel::preparePathToUserFile($file->url, $file->type);
            $filePermission = $fileModel->getFilePermission($file, $userId);
            $file->name = FileModel::getFileNameWithoutExec($file->original_name);
            
            if ((int)$file->type === FileModel::TYPE_VIDEO) {
                $file = ($filePermission) ? $file : 'notAllowed';

                if (!$filePermission && !$addNotAllowed) {
                    continue;
                }

                $videoList[] = $file;
            }

            if ((int)$file->type === FileModel::TYPE_IMAGE) {
                $file = ($filePermission) ? $file : 'notAllowed';

                if (!$filePermission && !$addNotAllowed) {
                    continue;
                }
                
                $imageList[] = $file;
            }
        }

        return ['videoList' => $videoList, 'imageList' => $imageList];
    }

    public static function getPublicReportFileAndWithPermission($reportId, $userId)
    {
        $reportFileModel = new ReportFileModel();
        $videoList = [];
        $imageList = [];

        $query = $reportFileModel
            ->select('fileTable.*')
            ->join(FileModel::table() . ' as fileTable', 'fileTable.file_id = ' . ReportFileModel::table() . '.file_id')
            ->join(PrivateFilePermissionModel::table() . ' as filePermissionTable', 'filePermissionTable.file_id = fileTable.file_id', 'left')
            ->where('report_id', $reportId)
            ->groupStart()
                ->groupStart()
                    ->where('fileTable.privacy_status', FileModel::PUBLIC_STATUS)
                ->groupEnd()
                ->orGroupStart()
                    ->where('fileTable.privacy_status', FileModel::PRIVATE_STATUS)
                    ->where('filePermissionTable.permitted_id', $userId)
                ->groupEnd()
            ->groupEnd()
            ->get();

        foreach ($query->getResultObject() as &$file) {
            $file->url = FileModel::preparePathToUserFile($file->url, $file->type);
            
            if ((int)$file->type === FileModel::TYPE_VIDEO) {
                $videoList[] = $file;
            }

            if ((int)$file->type === FileModel::TYPE_IMAGE) {
                $imageList[] = $file;
            }
        }

        return ['videoList' => $videoList, 'imageList' => $imageList];
    }

    public function deleteFiles(array $data)
    {
        $fileModel = new FileModel();
        $query = $fileModel
            ->select(FileModel::table() . '.*')
            ->join(ReportFileModel::table() . ' as reportFileTable', 'reportFileTable.file_id = ' . FileModel::table() . '.file_id and reportFileTable.report_id = ' . $data['id'][0])
            ->get();

        foreach ($query->getResultObject() as $item) {
            $fileModel->delete($item->file_id);
        }
    }
}
